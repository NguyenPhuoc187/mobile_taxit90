/**
 * Created by user on 04/08/2016.
 */

/// <reference path="tstyping/gulp/gulp.d.ts" />
/// <reference path="tstyping/main-bower-files/main-bower-files.d.ts" />
/// <reference path="tstyping/browser-sync/browser-sync.d.ts" />
/// <reference path="tstyping/connect-livereload/connect-livereload.d.ts" />

//version up trang dev sẽ tăng số sau cùng -> sau khi test ok sẽ tăng số kề trước để up lên host chính
var VERSION='2.0.20';

import gulp = require('gulp');
import {Stream} from "stream";
var $ = require('gulp-load-plugins')({lazy: true});
/*lấy các plugin của gulp .*/
import bowerFiles = require('main-bower-files');
import express = require('express');
import connectLr = require('connect-livereload');
var lrServer = require('tiny-lr')();
import browserSync = require('browser-sync');
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-minify-css');
var rename = require('gulp-rename');
var filter = require('gulp-filter');
var file = require('gulp-concat');
var through = require('through2');
var injectString = require('gulp-inject-string');
gulp.task('build',['zip-route', 'zip-controller', 'zip-lib', 'zip-service', 'zip-directive','zip-other'], ()=> {
    log('task build assets....');
    injectAssets();

});
gulp.task('dev', ['build'], function () {
    log('task dev start....');

    gulp.watch(['front-end//*.orgi.html', 'front-end//**//*.ts'], ['build', browserSync.reload])
        .on('change', function (evt:any) {
            log(evt);
        });
    serve(true);

});
gulp.task('zip-controller', ()=> {
    return gulp.src(['front-end/**/*.ctrl.js', '.front-end/*.ctrl.js'])
        .pipe($.print())
        .pipe(uglify())
        .pipe(file('zip-controller.js'))
        .pipe(gulp.dest("./front-end"));
});
gulp.task('zip-route', ()=> {
    return gulp.src(['front-end/**/*.route.js', '.front-end/*.route.js'])
        .pipe($.print())
        .pipe(uglify())
        .pipe(file('zip-route.js'))
        .pipe(gulp.dest("./front-end"));
});
gulp.task('zip-lib', ()=> {
    return gulp.src(['front-end/**/*.lib.js', '.front-end/*.lib.js'])
        .pipe($.print())
        .pipe(uglify())
        .pipe(file('zip-lib.js'))
        .pipe(gulp.dest("./front-end"));
});
gulp.task('zip-directive', ()=> {
    return gulp.src(['front-end/**/*.directive.js', '.front-end/*.directive.js'])
        .pipe($.print())
        .pipe(file('zip-directive.js'))
        .pipe(gulp.dest("./front-end"));
});
gulp.task('zip-service', ()=> {
    return gulp.src(['front-end/**/*.service.js', '.front-end/*.service.js'])
        .pipe($.print())
        .pipe(uglify())
        .pipe(file('zip-service.js'))
        .pipe(gulp.dest("./front-end"));
});


gulp.task('zip-other', ()=> {
    return gulp.src(
        [
            'bower_components/jquery/dist/jquery.min.js',
            'bower_components/signalr/jquery.signalR.min.js',
            'bower_components/bootstrap/dist/js/bootstrap.min.js',
            'bower_components/angular/angular.js',
            'bower_components/angular-ui-router/release/angular-ui-router.min.js',
            'bower_components/angular-route/angular-route.min.js',
            'bower_components/angular-cookies/angular-cookies.min.js',
            'bower_components/angular-sanitize/angular-sanitize.min.js',
            'bower_components/angular-dom-events/angular-dom-events.js',
            'bower_components/jqwidgets/jqwidgets/jqxcore.js',
            'bower_components/jqwidgets/jqwidgets/jqxbuttons.js',
            'bower_components/jqwidgets/jqwidgets/jqxscrollbar.js',
            'bower_components/jqwidgets/jqwidgets/jqxmenu.js',
            'bower_components/jqwidgets/jqwidgets/jqxdatatable.js',
            'bower_components/jqwidgets/jqwidgets/jqxcalendar.js',
            'bower_components/jqwidgets/jqwidgets/jqxdatetimeinput.js',
            'bower_components/jqwidgets/jqwidgets/jqxgauge.js',
            'bower_components/jqwidgets/jqwidgets/jqxgrid.js',
            'bower_components/jqwidgets/jqwidgets/jqxgrid.aggregates.js',
            'bower_components/jqwidgets/jqwidgets/jqxgrid.columnsreorder.js',
            'bower_components/jqwidgets/jqwidgets/jqxgrid.columnsresize.js',
            'bower_components/jqwidgets/jqwidgets/jqxgrid.edit.js',
            'bower_components/jqwidgets/jqwidgets/jqxgrid.export.js',
            'bower_components/jqwidgets/jqwidgets/jqxgrid.filter.js',
            'bower_components/jqwidgets/jqwidgets/jqxgrid.grouping.js',
            'bower_components/jqwidgets/jqwidgets/jqxgrid.pager.js',
            'bower_components/jqwidgets/jqwidgets/jqxgrid.selection.js',
            'bower_components/jqwidgets/jqwidgets/jqxgrid.storage.js',
            'bower_components/jqwidgets/jqwidgets/jqxgrid.sort.js',
            'bower_components/jqwidgets/jqwidgets/jqxdata.js',
            'bower_components/jqwidgets/jqwidgets/jqxcombobox.js',
            'bower_components/jqwidgets/jqwidgets/jqxcheckbox.js',
            'bower_components/jqwidgets/jqwidgets/jqxlistbox.js',
            'bower_components/jqwidgets/jqwidgets/jqxdropdownlist.js',
            'bower_components/jqwidgets/jqwidgets/jqxwindow.js',



            'bower_components/bootbox.js/bootbox.js',

            'bower_components/jqwidgets/jqwidgets/jqxdraw.js',
            'bower_components/jqwidgets/jqwidgets/jqxfileupload.js',
            'bower_components/jqwidgets/jqwidgets/jqxtooltip.js',
            'bower_components/jqwidgets/jqwidgets/jqxangular.js',
            'bower_components/angular-google-places-autocomplete/src/autocomplete.js',
            'bower_components/ng-flow/dist/ng-flow-standalone.js',
            'bower_components/flow.js/dist/flowjs',
            'bower_components/ng-flow/dist/ng-flow.s',
            'bower_components/moment/min/moment.min.js',


            'bower_components/jquery-watch-dom/jquery-watch.js',
            'bower_components/js-xlsx/dist/xlsx.full.js',
            'bower_components/jquery/dist/jquery.easing.1.3.js',
            'front-end/assets/js/plugins/autocomplete/jquery.autocomplete.min.js',
            'front-end/assets/js/plugins/autocomplete/currency-autocomplete.js',
            'front-end/assets/js/adminlte.js',

            'bower_components/ng-material-floating-button/src/mfb-directive.js',
            'bower_components/ng-material-floating-button/mfb/dist/mfb.min.js',
            'bower_components/bootstrap-daterangepicker/daterangepicker.js',
            'bower_components/FileSaver/FileSaver.min.js',
            'bower_components/FileSaver/jquery.wordexport.js',
            'front-end/assets/js/notify.min.js',
        ])
        .pipe($.print())//.pipe(uglify())
        .pipe(file('zip-other.js'))
        .pipe(gulp.dest("./front-end"));
});


gulp.task('zip-theme', ()=> {
    return gulp.src(['front-end/assets/themes/*/assets/**/*.js', '.front-end/assets/themes/*/assets/*.js'])
        .pipe($.print())//.pipe(uglify())
        .pipe(file('zip-theme.js'))
        .pipe(gulp.dest("./front-end"));
});
function buildLoadScript(dev:Boolean):string {

    var tmp=`var version='${VERSION}';
        var scriptRequest=[
            {
                name:'main',
                version:version,
                url:'zip-other.js'
            }
            ${!dev?`,
            {
                name:'app',
                version:version,
                url:'app.js'
            },{
                name:'router.provider',
                version:version,
                url:'shared/router.provider.js'
            },{
                name:'zip-lib',
                version:version,
                url:'zip-lib.js'
            },{
                name:'zip-directive',
                version:version,
                url:'zip-directive.js'
            },{
                name:'zip-service',
                version:version,
                url:'zip-service.js'
            },{
                name:'zip-route',
                version:version,
                url:'zip-route.js'
            },{
                name:'zip-controller',
                version:version,
                url:'zip-controller.js'
            }`:''}
        ];
        var _downServer=false;
        function injectRange(begin,end,callbak){
            function building(i){
                if(i>=end||i>=scriptRequest.length) {
                    t=true;
                    callbak();
                    return;
                }
                requireScript(scriptRequest[i].name, scriptRequest[i].version,scriptRequest[i].url,function(isServer){
                   _downServer=isServer;
                    building(i+1);
                });
            }
            building(begin);
        }
        var range=parseInt(scriptRequest.length/3);
        var step=0;
        function splitTaskInject(){
            injectRange(step,step+range,function(){
                step+=range;
                if(step>=scriptRequest.length) return;
                splitTaskInject();
            });
        }
        ${!dev?`window.onload = function(e) {
        for(var i=0;i<scriptRequest.length;i++){
             requireScript(scriptRequest[i].name, scriptRequest[i].version,scriptRequest[i].url,function(isServer){
                   _downServer=isServer;
                });
        }
        //if(_downServer &&!(/Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor))) location.reload();
                };`:`for(var i=0;i<scriptRequest.length;i++){
             requireScript(scriptRequest[i].name, scriptRequest[i].version,scriptRequest[i].url,function(isServer){
                   _downServer=isServer;
                });
        }`}
            console.log('load : '+(_downServer?'Server':'Cache'));
        
                `;
    return tmp;
}
gulp.task('build-release', ()=> {
    /*inject thông tin vào file index.html -. coppy ra thư mục release*/
    var target = gulp.src('.front-end/index.orgi.html');
    var provider = gulp.src([
        '.front-end/shared/*provider.js',
        '.front-end/shared/**/*provider.js'
    ], {read: false});
    var directive = gulp.src(['.front-end/shared/*directive.js', '.front-end/shared/**/*directive.js'], {read: false});
    var services=gulp.src(['.front-end/shared/*service.js','.front-end/shared/**/*service.js'], {read: false});
    var modules=gulp.src(['.front-end/*.module.js','.front-end/**/*.module.js'], {read: false});
    var router=gulp.src(['.front-end/*.route.js','.front-end/**/*.route.js'], {read: false});
    var lib=gulp.src(['.front-end/shared/*lib.js','.front-end/shared/**/*lib.js'], {read: false});
    var controller=gulp.src(['.front-end/*.ctrl.js','.front-end/**/*.ctrl.js'], {read: false});
    // zip tất cả cacf file component lại

    var app = gulp.src(['.front-end/*.js'], {read: false});
    var theme = gulp.src(['.front-end/assets/themes/*.js'], {read: false});
    // var app=gulp.src(['app/app.js'], {read: false});

    target
    //.pipe(inject(app, 'app'))/*inject all js file to index.html*/
    ////.pipe()
    //.pipe(inject(theme, 'theme'))/*inject all js file to index.html*/
    ////.pipe(inject(lib,'lib'))/*inject all js file to index.html*/
    //.pipe(inject(provider, 'provider'))/*inject all js file to index.html*/
    ////.pipe(inject(directive,'directive'))/*inject all js file to index.html*/
    //.pipe(inject(services,'services'))/*inject all js file to index.html*/
    //.pipe(inject(modules,'module'))/*inject all js file to index.html*/
    //.pipe(inject(router,'router'))/*inject all js file to index.html*/
    //.pipe(inject(controller,'controller'))/*inject all js file to index.html*/
    ////.pipe(inject(app,'app'))/*inject all js file to index.html*/
    //.pipe($.inject(gulp.src(bowerFiles(), {
    //    base: 'bower_components',
    //    read: false
    //}), {name: 'bower'}))/*inject all js and css lib file to index.html*/
    //.pipe(inject(gulp.src(['app/.temp/*.css','app/.temp/**/*.css'],{read:false}),'app'))/*inject stylesheet to index.html*/
        .pipe($.rename('index.html'))
        // .pipe(injectString.before('<aaa',`<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places,geometry&language=vi&key=AIzaSyBTKCCuBqOul_55ZOEFDMIkVum6aWoy5H0"></script>`))
        .pipe(injectString.before('<bbb',`<script src="zip-other.js?v=${VERSION}"></script>`))
        .pipe(injectString.before('<bbb',`<script src="app.js?v=${VERSION}"></script>`))
        .pipe(injectString.before('<bbb',`<script src="shared/router.provider.js?v=${VERSION}"></script>`))
        .pipe(injectString.before('<bbb',`<script src="zip-lib.js?v=${VERSION}"></script>`))
        .pipe(injectString.before('<bbb',`<script src="zip-directive.js?v=${VERSION}"></script>`))
        .pipe(injectString.before('<bbb',`<script src="zip-service.js?v=${VERSION}"></script>`))
        .pipe(injectString.before('<bbb',`<script src="zip-route.js?v=${VERSION}"></script>`))
        .pipe(injectString.before('<bbb',`<script src="zip-controller.js?v=${VERSION}"></script>`))

        .pipe(gulp.dest('./.front-end'));

    //this.buildLoadScript(target,true);
});
gulp.task('coppy-release', ['clean'], ()=> {
    function numHex(s:number) {
        var a = s.toString(16);
        if ((a.length % 2) > 0) {
            a = "0" + a;
        }
        return a;
    }

    /*coppy các file js và html ra thư mục release*/
    gulp.src(['front-end/**/*.js']) .pipe($.print())
    //.pipe(filter(['*','!front-end/assets/**/*.js']))
    //.pipe(uglify())
    // .pipe(rename((path:any)=>{
    //     var tmp=path.basename.split('.');
    //     for(var i=0;i<tmp.length;i++)
    //     {
    //         if(i==0)
    //         {
    //             path.basename=tmp[0]+numHex(Math.round(Math.random()*100000000)).toUpperCase();
    //         }
    //         else
    //             path.basename+='.'+tmp[i];
    //     }
    //
    // }))
        .pipe(gulp.dest('.front-end'));

    gulp.src(['front-end/assets/**/*.js'])
        .pipe(gulp.dest('.front-end/assets'));

    gulp.src(['front-end/**/*.css'])
        .pipe(minifyCSS({processImport: false}))
        .pipe(gulp.dest('.front-end'));

    gulp.src([
        'front-end/**/*.html',
        'front-end/**/*.ico',
        'front-end/**/*.otf',
        'front-end/**/*.eot',
        'front-end/**/*.svg',
        'front-end/**/*.ttf',
        'front-end/**/*.psd',
        'front-end/**/*.woff',
        'front-end/**/*.png',
        'front-end/**/*.jpg',
        'front-end/**/*.directive.js'])
        .pipe(gulp.dest('.front-end'));

});
gulp.task('clean', ()=> {
    gulp.src(['.front-end']).pipe($.clean({force: true}));
});
function injectAssets() {
    try {
        var target = gulp.src('front-end/index.orgi.html');
        var provider = gulp.src([
            'front-end/shared/*provider.js',
            'front-end/shared/**/*provider.js'
        ], {read: false});
        var directive = gulp.src(['front-end/shared/*directive.js', 'front-end/shared/**/*directive.js'], {read: false});
        var services = gulp.src(['front-end/shared/*service.js', 'front-end/shared/**/*service.js'], {read: false});
        var modules = gulp.src(['front-end/*.module.js', 'front-end/**/*.module.js'], {read: false});
        var router = gulp.src(['front-end/*.route.js', 'front-end/**/*.route.js'], {read: false});
        var lib = gulp.src(['front-end/shared/*lib.js', 'front-end/shared/**/*lib.js'], {read: false});
        var controller = gulp.src(['front-end/*.ctrl.js', 'front-end/**/*.ctrl.js'], {read: false});
        var app = gulp.src(['front-end/app.js'], {read: false});
        var theme = gulp.src(['front-end/assets/themes/*.js'], {read: false});

        target
            .pipe(inject(app,'app'))/*inject all js file to index.html*/
            .pipe(inject(theme, 'theme'))/*inject all js file to index.html*/
            .pipe(inject(lib, 'lib'))/*inject all js file to index.html*/
            .pipe(inject(provider, 'provider'))/*inject all js file to index.html*/
            .pipe(inject(directive, 'directive'))/*inject all js file to index.html*/
            .pipe(inject(services, 'services'))/*inject all js file to index.html*/
            .pipe(inject(modules, 'module'))/*inject all js file to index.html*/
            .pipe(inject(router, 'router'))/*inject all js file to index.html*/
            .pipe(inject(controller, 'controller'))/*inject all js file to index.html*/
            //.pipe($.inject(gulp.src(bowerFiles(), {
            //    base: 'bower_components',
            //    read: false
            //}), {name: 'bower'}))/*inject all js and css lib file to index.html*/
            //.pipe(inject(gulp.src(['app/.temp/*.css','app/.temp/**/*.css'],{read:false}),'app'))/*inject stylesheet to index.html*/
            // .pipe(injectString.before('<aaa','<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places,geometry&language=vi&key=AIzaSyDDvlmJeYSMu6BZT9_XDzIi6RmMGGVDWeU"></script>'))
            .pipe(injectString.before('<aaa',`<script>${buildLoadScript(true)}</script>`))
            .pipe($.rename('index.html'))
            .pipe(gulp.dest('./front-end'));

        //buildLoadScript(target,true);
    }
    catch (e) {
        console.log(e);
    }

}

function serve(isDev:boolean) {
    var nodeOptions = {
        script: 'server.js',
        delayTime: 100,
        ext: 'ts html',
        env: {
            'PORT': 3800,
            'PROXY_PORT': 8800,
            'NODE_ENV': isDev ? 'dev' : 'build'
        }//,
        // watch: ['./front-end']
    };


    //addWatchForFileReload(isDev);
    var isReload = false;
    return $.nodemon(nodeOptions)
        .on('start', function () {
            startBrowserSync(nodeOptions.env.PORT, nodeOptions.env.PROXY_PORT);
        })
        .on('restart', function () {
            if (isReload) return;
            isReload = true;
            setTimeout(function () {
                log('restarted!');
                browserSync.notify('reloading now ...');
                browserSync.reload({stream: false});
                isReload = false;
            }, 3000);
        });
}

function startBrowserSync(mainPort:number, proxyPort:number) {
    if (browserSync.active) {
        return;
    }

    log('Starting BrowserSync on port ' + (mainPort + 1));

    var options = {
        proxy: 'localhost:' + mainPort,
        port: proxyPort,
        ghostMode: { // these are the defaults t,f,t,t
            clicks: true,
            location: false,
            forms: true,
            scroll: true
        },
        injectChanges: true,
        logFileChanges: true,
        logLevel: 'error',
        logPrefix: 'gulp-patterns',
        notify: true,
        reloadDelay: 1000
    };
    browserSync(options);
}
function inject(src:any, tag:any) {
    src.pipe($.print());
    return $.inject(
        src, {
            starttag: '<!-- inject:' + tag + ':{{ext}} -->'
        }
    );
}
function log(msg:any) {
    if (typeof(msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.green(msg[item]));
            }
        }
    } else {
        $.util.log($.util.colors.green(msg));
    }
}
