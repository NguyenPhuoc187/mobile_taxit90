/**
 * Created by user on 04/11/2016.
 */

/// <reference path="../tstyping/angularjs/angular.d.ts" />
/// <reference path="shared/Entity/account.e.ts" />

/*Create adsun module*/
module App{
    import IRole = App.Shared.Entity.IRole;
    export var AdModule:ng.IModule = angular.module("adsun",['ui.router', 'ngRoute','jqwidgets','ngCookies','ngDomEvents','ngSanitize','google.places','flow','ng-mfb']);
    AdModule.run(['global','$http','$cookieStore','orderByFilter',(global:any,$http:ng.IHttpService, $cookieStore: ng.cookies.ICookieStoreService)=> {
        global.Token.SetValue($cookieStore.get('Token'));
        $http.get(global.AuthHost + 'User/GetRole').success((data:any)=> {
            //console.log(data);
            var acc = {
                Level: data.Level,
                Roles: Array<IRole>(),
                Username: data.Username,
                GroupUser: data.GroupUser,
                DisplayName: data.DisplayName,
                Phone: data.Phone,
                Theme: data.Theme,
                CauHinhDoi:data.CauHinhDoi
            };
            if (data.Funcs != null && data.Funcs != undefined)
                $.each(data.Funcs, (indx:number, val:any)=> {
                    acc.Roles.push({
                        Name: val.Name,
                        Description: val.Description,
                        Access: val.Access
                    })
                });


            global.Account.SetValue(acc);
        });
    }]);
}