/**
 * Created by phamn on 25/10/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export class DetailreportRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper,global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'detailreport',
                            config: {
                                controller: Taximanage.Detailreport.DetailreportController,
                                templateUrl: '/components/taximanage/detailreport/detailreport.html'+`?v=${global.Version}`,
                                url: '/bao-cao-tong-quat',
                                tag:{
                                    title:'Báo cáo tổng quát',
                                    icon:'icon-archive',
                                    groupId:'taxi',
                                    index:2
                                },
                                show:false
                            }
                        });
                        sysRouter.configureStates({
                            name: 'detailreport.zonetaxi',
                            config: {
                                controller: Taximanage.Detailreport.Zonetaxi.ZonetaxiController,
                                templateUrl: '/components/taximanage/detailreport/zonetaxi/zonetaxi.html'+`?v=${global.Version}`,
                                url: '/so-cuoc-xe-trong-vung',
                                parent:'detailreport',
                                tag:{
                                    title:'Cuốc xe trong vùng',
                                    icon:' icon-location4',
                                    groupId:'taxi',
                                    index:7
                                }
                            }
                        });
                        sysRouter.configureStates({
                            name: 'detailreport.pointtaxi',
                            config: {
                                controller: Taximanage.Detailreport.Pointtaxi.PointtaxiController,
                                templateUrl: '/components/taximanage/detailreport/pointtaxi/pointtaxi.html'+`?v=${global.Version}`,
                                url: '/so-cuoc-xe-trong-diem',
                                parent:'detailreport',
                                tag:{
                                    title:'Cuốc xe trong điểm',
                                    icon:'icon-location3',
                                    groupId:'taxi',
                                    index:8
                                }
                            }
                        });
                        // sysRouter.configureStates({
                        //     name: 'detailreport.sessionlog',
                        //     config: {
                        //         controller: Taximanage.Detailreport.Sessionlog.SessionlogController,
                        //         templateUrl: '/components/taximanage/detailreport/sessionlog/sessionlog.html'+`?v=${global.Version}`,
                        //         url: '/chi-tiet-cuoc-xe',
                        //         parent:'detailreport',
                        //         tag:{
                        //             title:'Chi tiết cuốc xe',
                        //             icon:'icon-clipboard3',
                        //             groupId:'taxi',
                        //             index:0
                        //         }
                        //     }
                        // });
                        sysRouter.configureStates({
                            name: 'detailreport.chotcataxi',
                            config: {
                                controller: Taximanage.Detailreport.Chotcataxi.ChotcataxiController,
                                templateUrl: '/components/taximanage/detailreport/chotcataxi/chotcataxi.html'+`?v=${global.Version}`,
                                url: '/chot-ca-dong-ho',
                                parent:'detailreport',
                                tag:{
                                    title:'Chốt ca đồng hồ',
                                    icon:'icon-files-empty',
                                    groupId:'taxi',
                                    index:2
                                }
                            }
                        });
                        sysRouter.configureStates({
                            name: 'detailreport.sessionlogerror',
                            config: {
                                controller: Taximanage.Detailreport.Sessionlogerror.SessionlogerrorController,
                                templateUrl: '/components/taximanage/detailreport/sessionlogerror/sessionlogerror.html'+`?v=${global.Version}`,
                                url: '/cuoc-bat-thuong',
                                parent:'detailreport',
                                tag:{
                                    title:'Cuốc bất thường',
                                    icon:'icon-warning',
                                    groupId:'taxi',
                                    index:6
                                }
                            }
                        });
                        sysRouter.configureStates({
                            name: 'detailreport.customzone',
                            config: {
                                controller: Taximanage.Detailreport.Customzone.CustomzoneController,
                                templateUrl: '/components/taximanage/detailreport/customzone/customzone.html'+`?v=${global.Version}`,
                                url: '/thong-ke-cuoc-vung-tuy-chon',
                                parent:'detailreport',
                                tag:{
                                    title:'Cuốc vùng tùy chọn',
                                    icon:'icon-files-empty',
                                    groupId:'taxi',
                                    index:9
                                }
                            }
                        });
                        sysRouter.configureStates({
                            name: 'detailreport.cuochongngoai',
                            config: {
                                controller: Taximanage.Detailreport.Cuochongngoai.CuochongngoaiController,
                                templateUrl: '/components/taximanage/detailreport/cuochongngoai/cuochongngoai.html'+`?v=${global.Version}`,
                                url: '/cuoc-hong-ngoai',
                                parent:'detailreport',
                                tag:{
                                    title:'Cuốc hồng ngoại',
                                    icon:'icon-files-empty',
                                    groupId:'taxi',
                                    index:5
                                }
                            }
                        });
                        sysRouter.configureStates({
                            name: 'detailreport.tatmaythatroi',
                            config: {
                                controller: Taximanage.Detailreport.Tatmaythatroi.TatmaythatroiController,
                                templateUrl: '/components/taximanage/detailreport/tatmaythatroi/tatmaythatroi.html'+`?v=${global.Version}`,
                                url: '/thong-ke-tat-may-tha-troi',
                                parent:'detailreport',
                                tag:{
                                    title:'Thống kê tắt máy thả trôi',
                                    icon:'icon-file-stats2',
                                    groupId:'taxi',
                                    index:10
                                }
                            }
                        });
                        // sysRouter.configureStates({
                        //     name: 'detailreport.thongtindongho',
                        //     config: {
                        //         controller: Taximanage.Detailreport.Thongtindongho.ThongtindonghoController,
                        //         templateUrl: '/components/taximanage/detailreport/thongtindongho/thongtindongho.html'+`?v=${global.Version}`,
                        //         url: '/thong-tin-dong-ho',
                        //         parent:'detailreport',
                        //         tag:{
                        //             title:'Thông tin đồng hồ',
                        //             icon:'icon-file-presentation',
                        //             groupId:'taxi',
                        //             index:1
                        //         }
                        //     }
                        // });
                        sysRouter.configureStates({
                            name: 'detailreport.ketoanca',
                            config: {
                                controller: Taximanage.Detailreport.Ketoanca.KetoancaController,
                                templateUrl: '/components/taximanage/detailreport/ketoanca/ketoanca.html'+`?v=${global.Version}`,
                                url: '/ket-toan-ca',
                                parent:'detailreport',
                                tag:{
                                    title:'Kết toán ca',
                                    icon:'icon-paste2',
                                    groupId:'taxi',
                                    index:3
                                }
                            }
                        });
                        sysRouter.configureStates({
                            name: 'detailreport.lichsukettoanca',
                            config: {
                                controller: Taximanage.Detailreport.Lichsukettoanca.LichsukettoancaController,
                                templateUrl: '/components/taximanage/detailreport/lichsukettoanca/lichsukettoanca.html'+`?v=${global.Version}`,
                                url: '/lich-su-ket-toan-ca',
                                parent:'detailreport',
                                tag:{
                                    title:'Lịch sử kết toán ca',
                                    icon:'icon-history',
                                    groupId:'taxi',
                                    index:4
                                }
                            }
                        });
                        sysRouter.configureStates({
                            name: 'detailreport.doanhthungay',
                            config: {
                                controller: Taximanage.Detailreport.Doanhthungay.DoanhthungayController,
                                templateUrl: '/components/taximanage/detailreport/doanhthungay/doanhthungay.html'+`?v=${global.Version}`,
                                url: '/doanh-thu-ngay',
                                parent:'detailreport',
                                tag:{
                                    title:'Tổng hợp doanh thu ngày',
                                    icon:'icon-drawer',
                                    groupId:'taxi',
                                    index:11
                                }
                            }
                        });
                        sysRouter.configureStates({
                            name: 'detailreport.kettoancatudong',
                            config: {
                                controller: Taximanage.Detailreport.Kettoancatudong.KettoancatudongController,
                                templateUrl: '/components/taximanage/detailreport/kettoancatudong/kettoancatudong.html'+`?v=${global.Version}`,
                                url: '/lich-su-ket-toan-ca-tu-dong',
                                parent:'detailreport',
                                tag:{
                                    title:'Lịch sử kết toán ca tự động',
                                    icon:'icon-drawer',
                                    groupId:'taxi',
                                    index:12
                                }
                            }
                        });
                        // sysRouter.configureStates({
                        //     name: 'detailreport.timdothatlac',
                        //     config: {
                        //         controller: Taximanage.Detailreport.Timdothatlac.TimdothatlacController,
                        //         templateUrl: '/components/taximanage/detailreport/timdothatlac/timdothatlac.html',
                        //         url: '/tim-do-that-lac',
                        //         parent:'detailreport',
                        //         tag:{
                        //             title:'Tìm đồ thất lạc',
                        //             icon:'icon icon-files-empty',
                        //             groupId:'taxi',
                        //             index:12
                        //         }
                        //     }
                        // });
                        //note: danh mục menu ko hiển thị ở trên thanh menu chính,
                        sysRouter.configureStates({
                            name: 'zonetaxidetail',
                            config: {
                                controller: Taximanage.Detailreport.Zonetaxidetail.ZonetaxidetailController,
                                templateUrl: '/components/taximanage/detailreport/zonetaxidetail/zonetaxidetail.html'+`?v=${global.Version}`,
                                url: '/chi-tiet-cuoc-xe/:param1/:param2/:param3/:param4/:param5/:param6',
                                tag:{
                                    title:'Chi tiết cuốc xe trong vùng',
                                    icon:'icon-files-empty-new',
                                    groupId:'taxi',
                                },
                                params: {
                                    param1: {
                                        value: null,
                                        squash: true
                                    },
                                    param2: {
                                        value: null,
                                        squash: true
                                    },
                                    param3: {
                                        value: null,
                                        squash: true
                                    },
                                    param4: {
                                        value: null,
                                        squash: true
                                    },
                                    param5: {
                                        value: null,
                                        squash: true
                                    },
                                    param6: {
                                        value: null,
                                        squash: true
                                    }

                                },
                                show:false
                            }
                        });
                        sysRouter.configureStates({
                            name: 'pointtaxidetail',
                            config: {
                                controller: Taximanage.Detailreport.Pointtaxidetail.PointtaxidetailController,
                                templateUrl: '/components/taximanage/detailreport/pointtaxidetail/pointtaxidetail.html'+`?v=${global.Version}`,
                                url: '/chi-tiet-cuoc-xe-diem/:param1/:param2/:param3/:param4/:param5/:param6',
                                tag:{
                                    title:'Chi tiết cuốc xe trong điểm',
                                    icon:'icon-files-empty-new',
                                    groupId:'taxi',
                                },
                                params: {
                                    param1: {
                                        value: null,
                                        squash: true
                                    },
                                    param2: {
                                        value: null,
                                        squash: true
                                    },
                                    param3: {
                                        value: null,
                                        squash: true
                                    },
                                    param4: {
                                        value: null,
                                        squash: true
                                    },
                                    param5: {
                                        value: null,
                                        squash: true
                                    },
                                    param6: {
                                        value: null,
                                        squash: true
                                    }

                                },
                                show:false
                            }
                        });

                        sysRouter.configureStates({
                            name: 'dthuxetheochotca',
                            config: {
                                controller: Taximanage.Detailreport.Dthuxetheochotca.DthuxetheochotcaController,
                                templateUrl: '/components/taximanage/detailreport/dthuxetheochotca/dthuxetheochotca.html'+`?v=${global.Version}`,
                                url: '/doanh-thu-xe-chot-ca/:param1/:param2/:param3',
                                tag:{
                                    title:'Doanh thu xe chốt ca',
                                    icon:'icon-files-empty-new',
                                    groupId:'taxi',
                                },
                                params: {
                                    param1: {
                                        value: null,
                                        squash: true
                                    },
                                    param2: {
                                        value: null,
                                        squash: true
                                    },
                                    param3: {
                                        value: null,
                                        squash: true
                                    },
                                },
                                show:false
                            }
                        });
                        sysRouter.configureStates({
                            name: 'dthuxetheokettoan',
                            config: {
                                controller: Taximanage.Detailreport.Dthuxetheokettoan.DthuxetheokettoanController,
                                templateUrl: '/components/taximanage/detailreport/dthuxetheokettoan/dthuxetheokettoan.html'+`?v=${global.Version}`,
                                url: '/doanh-thu-xe-ket-toan/:param1/:param2/:param3/:param4/:param5',
                                tag:{
                                    title:'Doanh thu xe kết toán ca',
                                    icon:'icon-files-empty-new',
                                    groupId:'taxi',
                                },
                                params: {
                                    //id công ty
                                    param1: {
                                        value: null,
                                        squash: true
                                    },
                                    //serial
                                    param2: {
                                        value: null,
                                        squash: true
                                    },
                                    //ngày bắt đầu
                                    param3: {
                                        value: null,
                                        squash: true
                                    },
                                    //ngày kết th
                                    param4: {
                                        value: null,
                                        squash: true
                                    },
                                    //đội xe
                                    param5: {
                                        value: null,
                                        squash: true
                                    },

                                },
                                show:false
                            }
                        });
                    }
                }
                AdModule.run(DetailreportRouteModel);
            }
        }
    }
}