/**
 * Created by phamn on 31/10/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Chotcataxi{
                    export interface IChotcataxiScope extends ng.IScope{
                        gridChotcataxi: App.Shared.Directives.GridControl;
                        $parent: IDetailreportScope;

                    }
                    export class ChotcataxiController{
                        public static $inject = ['$scope','language','global', '$http'];
                        constructor($scope: IChotcataxiScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService, $http: ng.IHttpService){
                            $scope.$parent.viewData = language.gridLg().viewData;
                            $scope.$parent.hideComboboxCar = false;
                            $scope.$parent.checkboxes_combobox = false;
                            $scope.$parent.hideComboboxGroupCar = false;
                            $scope.$parent.showlocationandpoint = false;
                            $scope.$parent.hidecalendar = false;
                            $scope.$parent.hidesinglecalendar = true;
                            $scope.gridChotcataxi = new App.Shared.Directives.GridControl("gridChotcataxi");
                            $scope.gridChotcataxi.selectionModeinfo = 'singlerows'; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridChotcataxi.filename_excel="BÁO CÁO SỐ LƯỢNG CHỐT CA ĐỒNG HỒ";
                            $scope.gridChotcataxi.columns = [];
                            $scope.gridChotcataxi.height = window.innerHeight - 200;
                            $scope.gridChotcataxi.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            $scope.gridChotcataxi.isshowAggregates = true;
                            $scope.gridChotcataxi.isAltRows=true;
                            $scope.gridChotcataxi.isFilter=true;
                            $scope.gridChotcataxi.groupable = true;
                            $scope.gridChotcataxi.groups = [];


                            $scope.gridChotcataxi.columns.push({ text: 'STT', dataField: 'STT', type: 'number', align: 'center', width:'40px',pinned: true });
                            $scope.gridChotcataxi.columns.push({ text: 'TG tính doanh thu', dataField: 'LastChotCa', type: 'string', width:'150px', align: 'center',filterable:true,pinned: true });
                            // $scope.gridChotcataxi.columns.push({ text: 'Thời gian chốt', dataField: 'UpdateTime', type: 'string', width:'100px', align: 'center',filterable:true,pinned: true });
                            $scope.gridChotcataxi.columns.push({ text: 'Số tài - Biển số', dataField: 'Display', type: 'string', align: 'center',width:'130px',filterable:true, pinned: true });
                            $scope.gridChotcataxi.columns.push({ text: 'Đội xe', dataField: 'GroupName', type: 'string', align: 'center',width:'80px',filterable:true });
                            $scope.gridChotcataxi.columns.push({ text: 'Tên tài xế', dataField: 'DriverName', type: 'string', align: 'center',width:'130px',filterable:true,  });
                            $scope.gridChotcataxi.columns.push({ text: 'Tổng tiền', dataField: 'TongTien', type: 'number', width:'125px', align: 'center',cellsformat: 'n',filterable:true,
                                aggregates: ['sum'],
                            });
                            $scope.gridChotcataxi.columns.push({ text: 'Km trong ca', dataField: 'TongKm', type: 'float', align: 'center',width:'65px',filterable:true,cellsformat: 'F1',className: 'gridtaxidetail_height40_2row',
                                aggregates: ['sum'],
                            });
                            $scope.gridChotcataxi.columns.push({ text: 'Km có khách', dataField: 'TongKmKhach', type: 'float', align: 'center',width:'65px',filterable:true,cellsformat: 'F1',className: 'gridtaxidetail_height40_2row',
                                aggregates: ['sum'],
                            });
                            $scope.gridChotcataxi.columns.push({ text: 'Mất nguồn', dataField: 'TongMatNguon', type: 'number', align: 'center',width:'63px',filterable:true,cellsformat: 'n',className: 'gridtaxidetail_height40_2row',
                                aggregates: ['sum'],
                            });
                            $scope.gridChotcataxi.columns.push({ text: 'Tổng cuốc', dataField: 'TongCuoc', type: 'number', align: 'center',width:'63px',filterable:true,cellsformat: 'n',className: 'gridtaxidetail_height40_2row',
                                aggregates: ['sum'],
                            });
                            $scope.gridChotcataxi.columns.push({ text: 'Số cuốc 0km', dataField: 'TongCuoc0Km', type: 'number', align: 'center',width:'63px',filterable:true,cellsformat: 'n',className: 'gridtaxidetail_height40_2row',
                                aggregates: ['sum'],
                            });
                            $scope.gridChotcataxi.columns.push({ text: 'Chích xung', dataField: 'ChichXung', type: 'number', align: 'center',width:'63px',filterable:true,cellsformat: 'n',className: 'gridtaxidetail_height40_2row',
                                aggregates: ['sum'],
                            });

                            $scope.gridChotcataxi.columns.push({ text: 'Tiền tích lũy cũ', dataField: 'TichLuyCu', type: 'number', align: 'center',width:'120px',filterable:true,cellsformat: 'n',});
                            $scope.gridChotcataxi.columns.push({ text: 'Tiền tích lũy mới', dataField: 'TichLuyMoi', type: 'number', align: 'center',width:'auto',filterable:true, cellsformat: 'n',});
                            $scope.gridChotcataxi.columns.push({ text: 'Ghi chú', dataField: 'GhiChu', type: 'string', align: 'center',width:'auto'});

                            //note: xử lý sự kiện nhấn nút xem
                            $scope.$parent.btViewClick = () => {

                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined){
                                    if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                        //note: nếu chọn đội xe là tất cả
                                        if($scope.$parent.modelSelectSettings.itemSelect() == undefined  || $scope.$parent.modelSelectSettings.itemSelect().Serial == '-1'){
                                            GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                        }else{
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.modelSelectSettings.itemSelect().Serial,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end())
                                        }
                                    }else{
                                        //note: nếu chọn đội xe là các giá trị khác
                                        if($scope.$parent.modelSelectSettings.itemSelect() == undefined  || $scope.$parent.modelSelectSettings.itemSelect().Serial == '-1'){
                                            GetDataWithGroup($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.groupdeviceSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                        }else{
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.modelSelectSettings.itemSelect().Serial,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end())
                                        }
                                    }
                                }
                            };


                            function GetDataWithCompany(companyId:number,beginTime:string,endTime:string){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.get(global.ReportHost + `api/ChotCaTaxi/ChotCaLogByCompany?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        if(rep.data.ChotCaLogTranfers.length > 0){
                                            $scope.gridChotcataxi.data = rep.data.ChotCaLogTranfers;
                                            ConvertData($scope.gridChotcataxi.data);
                                            $scope.gridChotcataxi.refresh();
                                        }else{
                                            $scope.gridChotcataxi.clear();
                                            $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                header: 'Thông báo!',
                                                theme: 'bg-info',
                                                position: 'center'
                                            });
                                        }
                                    }else{
                                        $scope.$parent.viewData= 'Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridChotcataxi.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                        // console.debug(rep.data.Description);
                                    }
                                });
                            }
                            function GetDataWithGroup(companyId:number,groupId:number,beginTime:string,endTime:string){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.get(global.ReportHost + `api/ChotCaTaxi/ChotCaLogByGroup?companyId=${companyId}&groupId=${groupId}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        if(rep.data.ChotCaLogTranfers.length > 0){
                                            $scope.gridChotcataxi.data = rep.data.ChotCaLogTranfers;
                                            ConvertData($scope.gridChotcataxi.data);
                                            $scope.gridChotcataxi.refresh();
                                        }else{
                                            $scope.gridChotcataxi.clear();
                                            $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                header: 'Thông báo!',
                                                theme: 'bg-info',
                                                position: 'center'
                                            });
                                        }
                                    }else{
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridChotcataxi.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                        // console.debug(rep.data.Description);

                                    }
                                });
                            }
                            function GetDataWithSerial(companyId:number,serial:string,beginTime:string,endTime:string){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.get(global.ReportHost + `api/ChotCaTaxi/ChotCaLogLamda?companyId=${companyId}&serial=${serial}&driverId=0&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        if(rep.data.ChotCaLogTranfers.length > 0){
                                            $scope.gridChotcataxi.data = rep.data.ChotCaLogTranfers;
                                            ConvertData($scope.gridChotcataxi.data);
                                            $scope.gridChotcataxi.refresh();
                                        }else{
                                            $scope.gridChotcataxi.clear();
                                            $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                header: 'Thông báo!',
                                                theme: 'bg-info',
                                                position: 'center'
                                            });
                                        }


                                    }else{
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridChotcataxi.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                        console.debug(rep.data.Description);

                                    }
                                });
                            }

                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    val.LastChotCa = `${global.AdapterDate(val.LastChotCa)} - ${global.AdapterDate(val.UpdateTime)}`;
                                    val.Display = `<span class="text-danger">${val.SoHieu} - ${val.Bs}</span>`;
                                    val.TongKm = (val.TongKm/1000).toFixed(1);
                                    val.TongKmKhach = (val.TongKmKhach/1000).toFixed(1);
                                    // val.TongCuoc0Km = (val.TongCuoc0Km/1000).toFixed(1);
                                });
                            }


                        }
                    }
                    AdModule.controller('ChotcataxiController', ChotcataxiController);
                }
            }
        }
    }
}