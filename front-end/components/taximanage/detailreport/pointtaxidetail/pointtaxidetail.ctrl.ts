/**
 * Created by phamn on 27/10/2016.
 */

module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Pointtaxidetail{
                    export interface IPointtaxidetailScope extends ng.IScope{
                        gridDetailpointtaxi: App.Shared.Directives.GridviewControl;

                        companyId:any;
                        params:any;
                        beginday:string;
                        endday:string;
                        PointName:string;
                    }
                    export class PointtaxidetailController{
                        public static $inject = ['$scope','$http', 'global','language','api','$location','$stateParams'];
                        constructor ($scope: IPointtaxidetailScope, public $http: ng.IHttpService, public global: App.Shared.GlobalService,public language:App.Shared.LanguageService,
                                     public api: App.Shared.Api.IApi,public $location: ng.ILocationService, public stateParams:ng.ui.IStateParamsService){
                            $scope.gridDetailpointtaxi = new App.Shared.Directives.GridviewControl("gridDetailpointtaxi");
                            $scope.gridDetailpointtaxi.filename_excel="BÁO CÁO CHI TIẾT CUỐC XE TRONG ĐIỂM";
                            $scope.gridDetailpointtaxi.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridDetailpointtaxi.columns = [];
                            $scope.gridDetailpointtaxi.height = window.innerHeight - 150;
                            $scope.gridDetailpointtaxi.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            $scope.gridDetailpointtaxi.isAltRows=true;
                            $scope.gridDetailpointtaxi.isFilter=true;
                            $scope.gridDetailpointtaxi.isFilterMode= 'advanced';
                            $scope.gridDetailpointtaxi.isshowAggregates = true;






                            $scope.gridDetailpointtaxi.columns.push({ text: 'STT', dataField: 'STT', type: 'number', align: 'center',width:'35px',pinned: true  });
                            $scope.gridDetailpointtaxi.columns.push({ text: 'Serial', dataField: 'Serial', type: 'string', align: 'center',width:'50px',hidden:true });
                            // $scope.gridDetailpointtaxi.columns.push({ text: 'ID Cuốc', dataField: 'SessionTaxiId', type: 'number', align: 'center',width:'60px',filterable:true,pinned: true });
                            $scope.gridDetailpointtaxi.columns.push({ text: 'ID Cuốc', dataField: 'SessionTaxiId', type: 'number', align: 'center',width:'50px',filterable:true,pinned: true });
                            $scope.gridDetailpointtaxi.columns.push({ text: 'Số tài - Biển số', dataField: 'Display', type: 'string', align: 'center',width:'114px',filterable:true,pinned: true});
                            // $scope.gridDetailpointtaxi.columns.push({ text: 'Tài xế', dataField: 'TenTaiXe', type: 'string', align: 'center',width:'150px',filterable:true,columngroup:'thongtinxe' });
                            // $scope.gridDetailpointtaxi.columns.push({ text: 'Đội xe', dataField: 'TenDoi', type: 'string', align: 'center',width:'auto',filterable:true,columngroup:'thongtinxe' });
                            $scope.gridDetailpointtaxi.columns.push({ text: 'TG đón trả khách', dataField: 'TimeTotal', type: 'string', align: 'center',width:'164px'  });
                            $scope.gridDetailpointtaxi.columns.push({ text: 'Địa điểm đón trả khách', dataField: 'Address', type: 'string', align: 'center',width:'237px'  });
                            // $scope.gridDetailpointtaxi.columns.push({ text: 'Thời gian', dataField: 'EndTime', type: 'string', align: 'center',width:'80px',columngroup:'diemtra'  });
                            // $scope.gridDetailpointtaxi.columns.push({ text: 'Địa điểm', dataField: 'AdrressEnd', type: 'string', align: 'center',width:'220px',columngroup:'diemtra'  });
                            $scope.gridDetailpointtaxi.columns.push({ text: 'Thời gian chờ', dataField: 'ThoiGianCho', type: 'string', align: 'center',width:'63px',columngroup:'thongtincuoc',className:'gridtaxidetail_height40_2row' });
                            $scope.gridDetailpointtaxi.columns.push({ text: 'Tổng tiền', dataField: 'TongTien', type: 'number', align: 'center',width:'107px',columngroup:'thongtincuoc',cellsFormat: 'n',
                                aggregates: ['sum'],
                            });
                            $scope.gridDetailpointtaxi.columns.push({ text: 'Km rỗng', dataField: 'KmRong', type: 'float', align: 'center',width:'83px',columngroup:'thongtincuoc' ,cellsFormat: 'F1',
                                aggregates: ['sum'],
                            });
                            $scope.gridDetailpointtaxi.columns.push({ text: 'Km GPS', dataField: 'TongKmGps', type: 'float', align: 'center',width:'79px',cellsFormat: 'F1',filterable:true,columngroup:'thongtincuoc',
                                aggregates: ['sum'],
                            });
                            $scope.gridDetailpointtaxi.columns.push({ text: 'Km(Đồng hồ)', dataField: 'TongKmDongHo', type: 'float', align: 'center',width:'71px',filterable:true,columngroup:'thongtincuoc',className:'gridtaxidetail_height40_2row',cellsFormat: 'F1',
                                aggregates: ['sum'],
                            });
                            $scope.gridDetailpointtaxi.columns.push({ text: 'Tỉ lệ km(dh)/km(gps)', dataField: 'TyLeKm_show', type: 'number', align: 'center',width:'104px',filterable:true,columngroup:'thongtincuoc', cellsFormat: 'p',className:'gridtaxidetail_height40_2row' });
                            $scope.gridDetailpointtaxi.columns.push({ text: 'Cuốc 0km', dataField: 'CuocKhongKm_show', type: 'string', align: 'center',width:'44px',filterable:true,columngroup:'thongtincuoc',className:'gridtaxidetail_height40_2row' });
                            $scope.gridDetailpointtaxi.columns.push({ text: 'Cuốc hợp đồng', dataField: 'IsCuocHopDong_show', type: 'string', align: 'center',width:'63px',filterable:true,columngroup:'thongtincuoc',className:'gridtaxidetail_height40_2row' });
                            $scope.gridDetailpointtaxi.columns.push({ text: 'Cuốc vãng lai', dataField: 'CuocVangLai_show', type: 'string', align: 'center',width:'auto',filterable:true,columngroup:'thongtincuoc',className:'gridtaxidetail_height40_2row' });
                            $scope.gridDetailpointtaxi.columnGroups.push({ text: 'Điểm đón khách', name: 'diemdon'});
                            $scope.gridDetailpointtaxi.columnGroups.push({ text: 'Điểm trả khách', name: 'diemtra'});
                            $scope.gridDetailpointtaxi.columnGroups.push({ text: 'Thông tin cuốc', name: 'thongtincuoc'});
                            $scope.gridDetailpointtaxi.columnGroups.push({ text: 'Thông tin xe', name: 'thongtinxe'});
                            
                            



                            $scope.params = {};
                            $scope.params  = stateParams;

                            $scope.beginday = ChangeTime($scope.params.param3);
                            $scope.endday = ChangeTime($scope.params.param4);
                            $scope.PointName = $scope.params.param6;
                            console.log($scope.params.param1);

                            if($scope.params !== undefined){
                                $http.get(global.ReportHost + `api/ZonePointReport/PointTaxiDetailReport?companyId=${$scope.params.param1}&beginTime=${$scope.params.param3}&endTime=${$scope.params.param4}&gpsCheckPointId=${$scope.params.param2}&groupId=${$scope.params.param5}`).then((rep:any)=>{
                                    $scope.gridDetailpointtaxi.data = rep.data.SessionTaxiLogList;
                                    ConvertData($scope.gridDetailpointtaxi.data);
                                    $scope.gridDetailpointtaxi.refresh();
                                });
                            }else{
                                return;
                            }


                            function ChangeTime(time:any){
                                var year:string = time.slice(12);
                                var month:string = time.slice(6,8);
                                var date:string = time.slice(9,11);

                                var hour:string = time.slice(0,5);

                                var tt:any = date + '-' + month + '-' + year ;
                                return tt;
                            }
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    if(val.BeginLocation !== null && val.EndLocation !== null){
                                        if(val.BeginLocation.Address !== null && val.EndLocation.Address !== null){
                                            val.Address = `<strong>Đón:</strong> ${val.BeginLocation.Address} </br> <strong>Trả : </strong> ${val.EndLocation.Address}`;
                                        }else{
                                            val.Address = '';
                                        }
                                    }
                                    val.TimeTotal = `<strong>Đón:</strong> ${global.AdapterDate(val.BeginTime)} </br> <strong>Trả :</strong> ${global.AdapterDate(val.EndTime)}`;
                                    if(val.CuocKhongKm){
                                        val.CuocKhongKm_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.CuocKhongKm_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(val.IsCuocHopDong){
                                        val.IsCuocHopDong_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.IsCuocHopDong_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(val.CuocVangLai){
                                        val.CuocVangLai_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.CuocVangLai_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    val.ThoiGianCho = val.ThoiGianCho.slice(0,5);
                                    val.TongKmGps = (val.TongKmGps/1000).toFixed(1);
                                    val.KmRong = (val.KmRong/1000).toFixed(1);
                                    val.TongKmDongHo = (val.TongKmDongHo/1000).toFixed(1);
                                    // val.showBs = `${val.Bs} - ${val.Id}`;
                                    val.Display = `<span class="text-danger">${val.SoHieu} - ${val.Bs}</span>`;
                                });

                            }
                        }
                    }
                    AdModule.controller('PointtaxidetailController',PointtaxidetailController);
                }
            }
        }
    }
}