/**
 * Created by phamn on 08/11/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Cuochongngoai{
                    export interface ICuochongngoaiScope extends ng.IScope{
                        gridCuochongngoai: App.Shared.Directives.GridviewControl;
                        $parent: IDetailreportScope;
                        viewchoice: number;
                    }
                    export class CuochongngoaiController{
                        public static $inject = ['$scope','language','global', '$http'];
                        constructor($scope: ICuochongngoaiScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService, $http: ng.IHttpService){
                            $scope.$parent.viewData = language.gridLg().viewData;
                            $scope.viewchoice = 0;
                            $scope.$parent.hideComboboxCar = false;
                            $scope.$parent.checkboxes_combobox = true;
                            $scope.$parent.showlocationandpoint = false;
                            $scope.$parent.hidecalendar = false;
                            $scope.$parent.hidesinglecalendar = true;
                            $scope.$parent.hideComboboxGroupCar = false;


                            $scope.gridCuochongngoai = new App.Shared.Directives.GridviewControl("gridCuochongngoai");
                            $scope.gridCuochongngoai.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridCuochongngoai.filename_excel="BÁO CÁO SỐ LƯỢNG CUỐC HỒNG NGOẠI";
                            $scope.gridCuochongngoai.columns = [];
                            $scope.gridCuochongngoai.height = window.innerHeight - 150;
                            $scope.gridCuochongngoai.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            $scope.gridCuochongngoai.isshowAggregates = true;
                            $scope.gridCuochongngoai.isAltRows=true;
                            $scope.gridCuochongngoai.isFilter=true;
                            $scope.gridCuochongngoai.isFilterMode= 'advanced';

                            $scope.gridCuochongngoai.columns.push({ text: 'STT', dataField: 'STT', type: 'string', align: 'center', width:'50px',pinned: true });
                            $scope.gridCuochongngoai.columns.push({ text: 'Số tài - Biển số', dataField: 'Display', type: 'string', width:'170px', align: 'center',filterable:true,pinned: true });
                            $scope.gridCuochongngoai.columns.push({ text: 'Serial', dataField: 'Serial', type: 'string', align: 'center',width:'50px',hidden:true });
                            $scope.gridCuochongngoai.columns.push({ text: 'Begintime', dataField: 'BeginTime', type: 'string', align: 'center',width:'50px',hidden:true });
                            $scope.gridCuochongngoai.columns.push({ text: 'EndTime', dataField: 'EndTime', type: 'string', align: 'center',width:'50px',hidden:true });
                            // $scope.gridCuochongngoai.columns.push({ text: 'Số hiệu xe', dataField: 'Id', type: 'string', width:'100px', align: 'center',filterable:true,columngroup:'thongtinxe' });
                            $scope.gridCuochongngoai.columns.push({ text: 'Đội xe', dataField: 'TenDoi', type: 'string', width:'100px', align: 'center',filterable:true });
                            $scope.gridCuochongngoai.columns.push({ text: 'Tên tài xế', dataField: 'TenTaiXe', type: 'string', width:'150px', align: 'center',filterable:true});
                            $scope.gridCuochongngoai.columns.push({ text: 'Số điện thoại', dataField: 'DtTaiXe', type: 'string', width:'90px', align: 'center',filterable:true});
                            $scope.gridCuochongngoai.columns.push({ text: 'TG đón trả khách', dataField: 'TimeTotal', type: 'string', align: 'center',width:'180px',filterable:true  });
                            $scope.gridCuochongngoai.columns.push({ text: 'Địa điểm đón trả khách', dataField: 'Address', type: 'string', align: 'center',width:'250px',filterable:true  });

                            $scope.gridCuochongngoai.columns.push({ text: 'Tổng Km', dataField: 'TongKmGps', type: 'float', width:'auto', align: 'center',filterable:true,cellsFormat: 'F1',
                                aggregates: ['sum'],
                            });


                            /**
                             * Hàm xử lý sự kiện click đúp vào grid - view
                             */
                            $scope.gridCuochongngoai.event.onDoubleClick = (row:any) =>{
                                console.log(row);
                                window.open(`/hanh-trinh-taxi/${$scope.$parent.cpnSelectSettings.itemSelect().Id}/${row.Serial}/${ChangeTime(row.BeginTime)}/${ChangeTime(row.EndTime)}`, '_blank');
                            };

                            /**
                             * Hàm xử lý sự kiện nhấn nút xem
                             */
                            $scope.$parent.btViewClick = ()=>{
                                if($scope.$parent.cpnSelectSettings !== undefined){
                                    if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                        //note: trường hợp chọn đội xe là tất cả
                                        if($scope.$parent.modelSelectSettings_multi.valueSelects() == undefined  || $scope.$parent.modelSelectSettings_multi.valueSelects()[0] == '-1'){
                                            // lấy thông tin theo công ty
                                            GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                        }else{
                                            // lấy thông tin theo serial
                                            PostDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end(),$scope.$parent.modelSelectSettings_multi.valueSelects())
                                        }
                                    }else{
                                        //note: trường hợp chọn đội xe không phải là tất cả
                                        if($scope.$parent.modelSelectSettings_multi.valueSelects() == undefined  || $scope.$parent.modelSelectSettings_multi.valueSelects()[0] == '-1'){

                                            // lấy thông tin theo đội xe
                                            GetDataWithGroup($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.groupdeviceSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                        }else{
                                            // lấy thông tin theo serial
                                            PostDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end(),$scope.$parent.modelSelectSettings_multi.valueSelects())
                                        }
                                    }

                                }
                            };

                            /**
                             * Hàm lấy thông tin cuốc hồng ngoại theo công ty
                             * @param {number} companyId: id công ty
                             * @param {string} begindate: thời gian bắt đầu
                             * @param {string} enddate: thời gian kết thúc
                             * @constructor
                             */
                            function GetDataWithCompany(companyId:number, begindate:string, enddate:string){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.get(global.ReportHost + `api/CuocHongNgoai/CuocHongNgoai?companyId=${companyId}&beginTime=${begindate}&endTime=${enddate}`).then((rep:any)=>{
                                    // nếu server có dữ liệu trả về thành công
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            if(rep.data.CuocHongNgoaiTranfers.length > 0){
                                                $scope.gridCuochongngoai.data = ViewChoice($scope.viewchoice,rep.data.CuocHongNgoaiTranfers);
                                                ConvertData($scope.gridCuochongngoai.data);
                                                $scope.gridCuochongngoai.refresh();
                                            }else{
                                                $scope.gridCuochongngoai.clear();
                                                $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                    header: 'Thông báo!',
                                                    theme: 'bg-info',
                                                    position: 'center'
                                                });
                                            }
                                        }else{
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            $scope.gridSessionlogerror.clear();
                                            $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                                header: 'Thông báo!',
                                                theme: 'bg-danger',
                                                position: 'center'
                                            });
                                        }
                                    }
                                    // nếu server bị lỗi hoặc
                                    else{
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridSessionlogerror.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                    }

                                });
                            }

                            /**
                             * Hàm lấy thông tin cuốc hồng ngoại theo đội xe
                             * @param {number} companyId: id công ty
                             * @param {number} groupId: id đội xe
                             * @param {string} beginTime: thời gian bắt đầu
                             * @param {string} endTime: thời gian kết thúc
                             * @constructor
                             */
                            function GetDataWithGroup(companyId:number,groupId:number,beginTime:string,endTime:string){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.get(global.ReportHost + `api/CuocHongNgoai/CuocHongNgoaiByGroup?companyId=${companyId}&group=${groupId}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            if(rep.data.CuocHongNgoaiTranfers.length > 0){
                                                $scope.gridCuochongngoai.data = ViewChoice($scope.viewchoice,rep.data.CuocHongNgoaiTranfers);
                                                ConvertData($scope.gridCuochongngoai.data);
                                                $scope.gridCuochongngoai.refresh();
                                            }else{
                                                $scope.gridSessionlogerror.clear();
                                                $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                    header: 'Thông báo!',
                                                    theme: 'bg-info',
                                                    position: 'center'
                                                });
                                            }

                                        }else{
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            $scope.gridSessionlogerror.clear();
                                            $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                header: 'Thông báo!',
                                                theme: 'bg-info',
                                                position: 'center'
                                            });
                                        }
                                    }else{
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridSessionlogerror.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                    }

                                });
                            }
                            // function GetDataWithSerial(companyId:number,beginTime:string,endTime:string,serial:string){
                            //     $scope.$parent.viewData='Đang tải';
                            //     $scope.$parent.loading = true;
                            //     $http.get(global.ReportHost + `/api/CuocHongNgoai/CuocHongNgoaiBySerial?companyId=${companyId}&serial=${serial}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{
                            //         if(rep !== undefined){
                            //             if(rep.data.Status == 1){
                            //                 $scope.$parent.viewData='Xem';
                            //                 $scope.$parent.loading = false;
                            //                 if(rep.data.CuocHongNgoaiTranfers.length > 0){
                            //                     $scope.gridCuochongngoai.data = ViewChoice($scope.viewchoice,rep.data.CuocHongNgoaiTranfers);
                            //                     // $scope.gridCuochongngoai.data = rep.data.CuocHongNgoaiTranfers;
                            //                     ConvertData($scope.gridCuochongngoai.data);
                            //                     $scope.gridCuochongngoai.refresh();
                            //                 }else{
                            //                     $scope.gridCuochongngoai.clear();
                            //                     $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                            //                         header: 'Thông báo!',
                            //                         theme: 'bg-info',
                            //                         position: 'center'
                            //                     });
                            //                 }
                            //
                            //             }else{
                            //                 $scope.$parent.viewData='Xem';
                            //                 $scope.$parent.loading = false;
                            //                 $scope.gridCuochongngoai.clear();
                            //                 $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                            //                     header: 'Thông báo!',
                            //                     theme: 'bg-danger',
                            //                     position: 'center'
                            //                 });
                            //             }
                            //         }else{
                            //             $scope.$parent.viewData='Xem';
                            //             $scope.$parent.loading = false;
                            //             $scope.gridCuochongngoai.clear();
                            //             $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                            //                 header: 'Thông báo!',
                            //                 theme: 'bg-danger',
                            //                 position: 'center'
                            //             });
                            //         }
                            //
                            //     });
                            // }

                            /**
                             * Hàm lấy thông tin cuốc hồng ngoại khi chọn một hoặc nhiều xe
                             * @param {number} companyId: id công ty
                             * @param {string} beginTime: thời gian bắt đầu
                             * @param {string} endTime: thời gian kết thúc
                             * @param listserial mảng các serial khi chọn ở combobox
                             * @constructor
                             */
                            function PostDataWithSerial(companyId:number,beginTime:string,endTime:string,listserial:any){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.post(global.ReportHost + `api/CuocHongNgoai/CuocHongNgoaiBySerials?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`,listserial).then((rep:any)=>{
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            // trường hợp có giá trị khi lấy về
                                            if(rep.data.CuocHongNgoaiTranfers.length > 0){
                                                $scope.gridCuochongngoai.data = ViewChoice($scope.viewchoice,rep.data.CuocHongNgoaiTranfers);
                                                // $scope.gridCuochongngoai.data = rep.data.CuocHongNgoaiTranfers;
                                                ConvertData($scope.gridCuochongngoai.data);
                                                $scope.gridCuochongngoai.refresh();
                                            }
                                            // trường hợp lấy về rỗng
                                            else{
                                                $scope.gridCuochongngoai.clear();
                                                $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                    header: 'Thông báo!',
                                                    theme: 'bg-info',
                                                    position: 'center'
                                                });
                                            }

                                        }else{
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            $scope.gridCuochongngoai.clear();
                                            $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                                header: 'Thông báo!',
                                                theme: 'bg-danger',
                                                position: 'center'
                                            });
                                        }
                                    }else{
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridCuochongngoai.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                    }
                                });
                            }

                            /**
                             * hàm convert giá trị lấy về cần xử lý
                             * @param value: giá trị mảng dữ liệu lấy về từ server
                             * @constructor
                             */
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind +1;
                                    val.TimeTotal = `<strong>Đón:</strong> ${global.AdapterDate(val.BeginTime)} </br> <strong>Trả :</strong> ${global.AdapterDate(val.EndTime)}`;
                                    if(val.BeginLocation !== null && val.EndLocation !== null){
                                        if(val.BeginLocation.Address !== null && val.EndLocation.Address !== null){
                                            val.Address = `<strong>Đón:</strong> ${val.BeginLocation.Address} </br> <strong>Trả : </strong> ${val.EndLocation.Address}`;
                                        }else{
                                            val.Address = '';
                                        }
                                    }
                                    // if(val.BeginLocation !== null){
                                    //     if(val.BeginLocation.Address !== null){
                                    //         val.AdrressStart = val.BeginLocation.Address;
                                    //     }else{
                                    //         val.AdrressStart = '';
                                    //     }
                                    // }
                                    // if(val.EndLocation !== null){
                                    //     if(val.EndLocation.Address !== null){
                                    //         val.AdrressEnd = val.EndLocation.Address;
                                    //     }else{
                                    //         val.AdrressEnd = '';
                                    //     }
                                    // }else{
                                    //     val.AdrressEnd = val.AdrressStart;
                                    // }
                                    val.BeginTime = global.AdapterDate(val.BeginTime);
                                    val.EndTime = global.AdapterDate(val.EndTime);
                                    val.Display = `<span class="text-danger">${val.SoHieu} - ${val.Bs}</span>`;
                                    val.TongKmGps = (val.TongKmGps/1000).toFixed(1);
                                });
                            }

                            /**
                             * Hàm chuyển định dạng thời gian để chuyển lên param chạy qua trang hành trình để vẽ
                             * @param time: giá trị thời gian theo format đã được định dạng 07:02:23 06/09/2017
                             * @returns {any} trả ra giá trị 07:02:23 09-06-2017
                             * @constructor
                             */
                            function ChangeTime(time:any){
                                var year:string = time.slice(15,19);
                                var month:string = time.slice(12,14);
                                var date:string = time.slice(9,11);

                                var hour:string = time.slice(0,8);

                                var tt:any = hour +' ' +month + '-' + date + '-' + year ;
                                return tt;
                            }

                            /**
                             * Hàm lấy dữ liệu theo trạng thái lọc
                             * @param value: chọn từ radio button
                             * @param data: giá trị dữ liệu lấy từ server về
                             * @returns {any} trả về mảng dữ liệu đã được lọc
                             * @constructor
                             */
                            function ViewChoice(value:any, data:any){
                                switch (value){
                                    case 0:
                                        // lấy trường hợp tất cả
                                        return data;
                                    case 1:
                                        // trường hợp chích xung không sụp cò
                                        let result_chichxung:any = [];
                                        $.each(data,(ind:number,val:any)=>{
                                            if(val.ChichXung){
                                                result_chichxung.push(val);
                                            }
                                        });
                                        return result_chichxung;

                                    case 2:
                                        //trường hợp mất kết nối
                                        let result_matketnoi:any = [];
                                        $.each(data,(ind:number,val:any)=>{
                                            if(val.MatKetNoiDongHo){
                                                result_chichxung.push(val);
                                            }
                                        });
                                        return result_matketnoi;

                                    case 3:
                                        let result_taixe:any = [];
                                        $.each(data,(ind:number,val:any)=>{
                                            if(val.TaiXeTacDong){
                                                result_taixe.push(val);
                                            }
                                        });
                                        return result_taixe;
                                }
                            }

                        }
                    }
                    AdModule.controller('CuochongngoaiController', CuochongngoaiController);
                }
            }
        }
    }
}