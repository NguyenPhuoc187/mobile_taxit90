/**
 * Created by phamn on 26/10/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Pointtaxi{
                    export interface IPointtaxiScope extends ng.IScope{
                        gridPointtaxi: App.Shared.Directives.GridviewControl;
                        $parent: IDetailreportScope;
                    }
                    export class PointtaxiController{
                        public static $inject = ['$scope','language','global', '$http'];
                        constructor($scope: IPointtaxiScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService, $http: ng.IHttpService){
                            //note: cấu hình trang master
                            $scope.$parent.viewData = language.gridLg().viewData;
                            $scope.$parent.checkboxes_combobox = false;
                            $scope.$parent.checkzonelocation = false;
                            $scope.$parent.showlocationandpoint = true;
                            $scope.$parent.hideComboboxCar = true;
                            $scope.$parent.hidecalendar = false;
                            $scope.$parent.hidesinglecalendar = true;
                            $scope.$parent.hideComboboxGroupCar = false;
                            // note: cấu hình trang child
                            $scope.gridPointtaxi = new App.Shared.Directives.GridviewControl("gridPointtaxi");
                            $scope.gridPointtaxi.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridPointtaxi.filename_excel="BÁO CÁO SỐ LƯỢNG CUỐC XE TRONG ĐIỂM";
                            $scope.gridPointtaxi.columns = [];
                            $scope.gridPointtaxi.height = window.innerHeight - 150;
                            $scope.gridPointtaxi.columnsHeight = 30;
                            //bật tính năng lọc dữ liệu
                            $scope.gridPointtaxi.isshowAggregates = true;
                            $scope.gridPointtaxi.isAltRows=true;
                            $scope.gridPointtaxi.isFilter=true;
                            $scope.gridPointtaxi.isFilterMode= 'advanced';

                            $scope.gridPointtaxi.columns.push({ text: 'STT', dataField: 'STT', type: 'string', align: 'center', width:'50px',pinned: true });
                            $scope.gridPointtaxi.columns.push({ text: 'Tên điểm', dataField: 'PointName', type: 'string', width:'200px', align: 'center',filterable:true,pinned: true });
                            $scope.gridPointtaxi.columns.push({ text: 'Id', dataField: 'PointId', type: 'string', width:'200px', align: 'center',hidden:true });
                            $scope.gridPointtaxi.columns.push({ text: 'Số lượng', dataField: 'BeginCount', type: 'number', align: 'center',width:'100px',filterable:true,columngroup:'cuocbatdau'  });
                            $scope.gridPointtaxi.columns.push({ text: 'Tổng tiền', dataField: 'BeginTotalMoney', type: 'number', align: 'center',width:'150px',filterable:true,columngroup:'cuocbatdau',cellsFormat: 'n',
                                aggregates: ['sum'],
                            });
                            $scope.gridPointtaxi.columns.push({ text: 'Tổng km', dataField: 'BeginTotalKm', type: 'float', align: 'center',width:'150px',filterable:true,columngroup:'cuocbatdau',cellsFormat: 'F1',
                                aggregates: ['sum'],
                            });
                            $scope.gridPointtaxi.columns.push({ text: 'Số lượng', dataField: 'EndCount', type: 'number', align: 'center',width:'100px',filterable:true,columngroup:'cuocketthuc'  });
                            $scope.gridPointtaxi.columns.push({ text: 'Tổng tiền', dataField: 'EndTotalMoney', type: 'number', align: 'center',width:'150px',filterable:true,columngroup:'cuocketthuc',cellsFormat: 'n',
                                aggregates: ['sum'],

                            });
                            $scope.gridPointtaxi.columns.push({ text: 'Tổng km', dataField: 'EndTotalKm', type: 'float', align: 'center',width:'auto',filterable:true,columngroup:'cuocketthuc',cellsFormat: 'F1',
                                aggregates: ['sum'],
                            });

                            $scope.gridPointtaxi.columnGroups.push({ text: 'Cuốc bắt đầu', name: 'cuocbatdau'});
                            $scope.gridPointtaxi.columnGroups.push({ text: 'Cuốc kết thúc', name: 'cuocketthuc'});


                            /**
                             * Xử lý sự kiện khi nhấn nút xem
                             */
                            $scope.$parent.btViewClick =() => {
                                //todo: kiểm tra xem có chọn công ty chưa

                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined){
                                    //note: nếu chọn công ty và ko chọn đội xe hoặc chọn đội xe là tất cả
                                    if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                        //note: nếu có chọn điểm
                                       if($scope.$parent.pointSelectSetting.valueSelects().length !== 0 && $scope.$parent.pointSelectSetting.valueSelects() !== undefined){
                                           //todo: thêm api chọn điểm
                                           if($scope.$parent.pointSelectSetting.valueSelects()[0].Id == '-1'){
                                               GetDataWithCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                           }else{
                                               GetDataGroupAndPoint($scope.$parent.pointSelectSetting.valueSelects(),null,$scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                           }
                                       }else{
                                           //todo: lấy api chọn tất cả điểm theo công ty
                                           GetDataWithCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                       }
                                    }else{
                                        //note: có chọn đội
                                        if($scope.$parent.pointSelectSetting.valueSelects().length !== 0 && $scope.$parent.pointSelectSetting.valueSelects() !== undefined){
                                            //todo: thêm api chọn điểm theo đội
                                            if($scope.$parent.pointSelectSetting.valueSelects()[0].Id == '-1'){
                                                GetDataGroupAndPoint(null,[$scope.$parent.groupdeviceSelectSettings.itemSelect().Id],$scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                            }else{
                                                GetDataGroupAndPoint($scope.$parent.pointSelectSetting.valueSelects(),[$scope.$parent.groupdeviceSelectSettings.itemSelect().Id],$scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                            }
                                        }else{
                                            //todo: api chọn theo đội khi chọn vùng là tất cả
                                            GetDataGroupAndPoint(null,[$scope.$parent.groupdeviceSelectSettings.itemSelect().Id],$scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                        }
                                    }

                                    // if($scope.$parent.modelSelectSettings.itemSelect() !== undefined){
                                    //     //nếu chọn xe và chọn vùng không thuộc trường hợp tất cả
                                    //     if($scope.$parent.modelSelectSettings.itemSelect().Serial !== '-1'){
                                    //         //nếu không chọn điểm
                                    //         if($scope.$parent.pointSelectSetting.valueSelects().length !== 0 && $scope.$parent.pointSelectSetting.valueSelects() !== undefined){
                                    //             GetDataSerialAndPoint($scope.$parent.pointSelectSetting.valueSelects(),$scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end(),$scope.$parent.modelSelectSettings.itemSelect().Serial);
                                    //         }else{
                                    //             bootbox.dialog({
                                    //                 title: "Thông Báo",
                                    //                 message: "Vui lòng chọn điểm!!!",
                                    //                 buttons: {
                                    //                     confirm: {
                                    //                         label: "Quay lại",
                                    //                         className: "btn-default",
                                    //                     }
                                    //                 }
                                    //             });
                                    //         }
                                    //     }else{
                                    //         if($scope.$parent.pointSelectSetting.valueSelects()[0].Id == '-1'){
                                    //             //note: lấy theo đội xe
                                    //             GetDataGroupAndPoint(null,[$scope.$parent.groupdeviceSelectSettings.itemSelect().Id],$scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                    //         }else{
                                    //             //note: lấy theo đội xe và vùng
                                    //             GetDataGroupAndPoint($scope.$parent.pointSelectSetting.valueSelects(),[$scope.$parent.groupdeviceSelectSettings.itemSelect().Id],$scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                    //         }
                                    //     }
                                    // }else{
                                    //     bootbox.dialog({
                                    //         title: "Thông Báo",
                                    //         message: "Vui lòng chọn xe!!!",
                                    //         buttons: {
                                    //             confirm: {
                                    //                 label: "Quay lại",
                                    //                 className: "btn-default",
                                    //             }
                                    //         }
                                    //     });
                                    // }


                                }

                            };


                            /**
                             *  Chi tiết cuốc xe
                             */
                            $scope.gridPointtaxi.event.onDoubleClick = (row:any)=>{
                                if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                    var groupId = 0;
                                }else{
                                    groupId = $scope.$parent.groupdeviceSelectSettings.itemSelect().Id;
                                }
                                console.log(row);
                                window.open(`/chi-tiet-cuoc-xe-diem/${$scope.$parent.cpnSelectSettings.itemSelect().Id}/${row.PointId}/${global.ChangeTimeGetLink($scope.$parent.dtTimeSelect.begin())}/${global.ChangeTimeGetLink($scope.$parent.dtTimeSelect.end())}/${groupId}/${row.PointName}`, '_blank');

                            };


                            /**
                             * Get data GroupAndPoint
                             */

                            function GetDataGroupAndPoint(pointId:any,groupId:any,companyId:number, begindate:string, enddate:string){
                                language.showLoading(false,"");
                                var req:any = {
                                    gpsCheckPointId: pointId,
                                    groupId: groupId
                                };
                                $http.post(global.ReportHost + `api/ZonePointReport/PointTaxiReportLamda?companyId=${companyId}&beginTime=${begindate}&endTime=${enddate}`,req).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridPointtaxi.data = rep.data.PointTaxiReports;
                                        ConvertData($scope.gridPointtaxi.data);
                                        $scope.gridPointtaxi.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }

                            function GetDataWithCompanyId(companyId:number, begindate:string,enddate:string){
                                language.showLoading(false,"");
                                $http.get(global.ReportHost + `api/ZonePointReport/PointTaxiReport?companyId=${companyId}&beginTime=${begindate}&endTime=${enddate}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridPointtaxi.data = rep.data.PointTaxiReports;
                                        ConvertData($scope.gridPointtaxi.data);
                                        $scope.gridPointtaxi.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }


                            function GetDataSerialAndPoint(pointId:any,companyId:number,begindate:string,enddate:string,serial:string){
                                // var mangpoint:any = pointId;
                                if(pointId[0] == -1){
                                    pointId.shift();

                                }
                                language.showLoading(false,"");
                                $http.post(global.ReportHost + `api/ZonePointReport/PointTaxiReportBySerial?companyId=${companyId}&beginTime=${begindate}&endTime=${enddate}&serial=${serial}`,pointId).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridPointtaxi.data = rep.data.PointTaxiReports;
                                        ConvertData($scope.gridPointtaxi.data);
                                        $scope.gridPointtaxi.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }

                            /**
                             * Convert Data
                             */
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                   val.STT = ind + 1;
                                   val.BeginTotalKm = (val.BeginTotalKm/1000).toFixed(1);
                                   val.EndTotalKm = (val.EndTotalKm/1000).toFixed(1);
                                });
                            }

                        }
                    }
                    AdModule.controller('PointtaxiController', PointtaxiController);
                }
            }
        }
    }
}