/**
 * Created by phamn on 25/10/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export interface IDetailreportScope extends ng.IScope{
                    cpnSelectSettings:App.Shared.Gui.IDropDown<App.Shared.Entity.ICompany>;
                    groupdeviceSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IGroup>;
                    modelSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IDevice>;
                    modelSelectSettings_multi: App.Shared.Gui.IDropDown<App.Shared.Entity.IDevice>;
                    driverSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IDriver>;
                    zoneSelectSetting: App.Shared.Gui.IDropDown<any>;
                    pointSelectSetting: App.Shared.Gui.IDropDown<any>;
                    dtTimeSelect:App.Shared.Gui.IDateTimeControl;
                    dtSingleSelect:App.Shared.Gui.IDateTimeControl;
                    checkboxes_combobox:boolean;
                    showComboboxGroupCar:any;
                    hideComboboxGroupCar:boolean;
                    viewData:string;
                    btViewClick:() => any;
                    checkzonelocation:boolean;
                    hideDriver:boolean;
                    hideComboboxCar:boolean;
                    showlocationandpoint:boolean;
                    hidecalendar:boolean;
                    hidesinglecalendar:boolean;
                    init:() =>any;
                    dtsignleDay:any;
                    loading:boolean;
                    dtTimeStartSelect:any;
                    dtTimeEndSelect:any;
                    dtTimeSingleSelect:any;
                }
                export class DetailreportController{
                    public static $inject = ['$scope','language','global', '$http','api','$q'];
                    constructor ($scope: IDetailreportScope,public language:App.Shared.LanguageService, public global: App.Shared.GlobalService, public $http: ng.IHttpService,
                                 public api:App.Shared.Api.IApi, public $q: ng.IQService){
                        $scope.dtsignleDay = {
                            Daymacdinh:global.getnowsingleday(),
                            getdulieu: global.getnowsingleday(),
                        };
                        $scope.init = () => {

                            var datepicker = {
                                singleDatePicker: true,
                                showDropdowns: true,
                                format: 'DD/MM/YYYY',
                                opens: 'left',
                                drops: 'down',
                                locale: {
                                    cancelLabel: 'Hủy',
                                    applyLabel: 'Đồng ý',
                                    fromLabel: 'Từ ngày',
                                    toLabel: 'Đến ngày',
                                    daysOfWeek: ['<label class="text-danger">CN</label>', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
                                    monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
                                    firstDay: 1

                                },
                            };
                            $('#dtSingleSelect').daterangepicker(datepicker);
                            $scope.dtsignleDay.Daymacdinh = global.getnowsingleday();
                        };

                        $scope.viewData=language.gridLg().viewData;
                        $scope.loading = false;
                        // Khai báo mới combobox truyền vào 3 thông số : tên định dạng của combobox, hiển thị tên trường, giá trị chọn
                        $scope.cpnSelectSettings =new App.Shared.Gui.DropDown<App.Shared.Entity.ICompany>('cbCompanyDetailTaxiMaster','Display','Id');
                        // Tiêu đề trên combobox
                        $scope.cpnSelectSettings.title= language.gridLg().company;
                        // Set chiều dài cho combobox
                        $scope.cpnSelectSettings.setting.width = '100%';
                        $scope.cpnSelectSettings.setting.checkboxes = false;
                        $scope.cpnSelectSettings.setting.filterPlaceHolder = 'Tìm công ty...';

                        // Khai báo combobox đội xe
                        $scope.groupdeviceSelectSettings=new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('dropdownGroupDeivesDetailTaxiMaster','Name','Id');
                        $scope.groupdeviceSelectSettings.title=language.gridLg().group;
                        $scope.groupdeviceSelectSettings.setting.width = '100%';
                        $scope.groupdeviceSelectSettings.setting.checkboxes = false;
                        $scope.groupdeviceSelectSettings.setting.filterPlaceHolder = 'Tìm đội xe...';

                        // Khai báo combobox xe
                        $scope.modelSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IDevice>('cbDeviceDetailsTaxiMaster','DisplayId','Serial');
                        $scope.modelSelectSettings.title=language.gridLg().car;
                        $scope.modelSelectSettings.setting.width = '100%';
                        $scope.modelSelectSettings.setting.placeHolder = 'Chọn xe';
                        $scope.modelSelectSettings.setting.checkboxes = false;

                        //Khai báo combobox xe multi
                        $scope.modelSelectSettings_multi = new App.Shared.Gui.DropDown<App.Shared.Entity.IDevice>('cbDeviceMutiDetailTaxiMaster','DisplayId','Serial');
                        $scope.modelSelectSettings_multi.title=language.gridLg().car;
                        $scope.modelSelectSettings_multi.setting.width = '100%';
                        $scope.modelSelectSettings_multi.setting.placeHolder = 'Chọn xe';
                        // $scope.modelSelectSettings_multi.setting.checkboxes = true;
                        // $scope.modelSelectSettings_multi.setting.showArrow = true;


                        $scope.zoneSelectSetting = new App.Shared.Gui.DropDown<any>('drZonelocationTaxiMaster','Name','Id');
                        $scope.zoneSelectSetting.title='Vùng';
                        $scope.zoneSelectSetting.setting.width = '100%';
                        $scope.zoneSelectSetting.setting.placeHolder = 'Chọn vùng';
                        $scope.zoneSelectSetting.setting.filterPlaceHolder = 'Tìm vùng...';


                        $scope.pointSelectSetting = new App.Shared.Gui.DropDown<any>('drPointTaxiMaster','Name','Id');
                        $scope.pointSelectSetting.title='Điểm';
                        $scope.pointSelectSetting.setting.width = '100%';
                        $scope.pointSelectSetting.setting.placeHolder = 'Chọn điểm';
                        $scope.pointSelectSetting.setting.filterPlaceHolder = 'Tìm điểm...';


                        $scope.dtTimeStartSelect= new App.Shared.Gui.DateTimeControl('dtTimeSelectStartMobileReportTaxi');
                        $scope.dtTimeStartSelect.titleTime='';
                        $scope.dtTimeStartSelect.minday = -3;
                        // var min = $scope.dtTimeEndSelect.GetPickerDate();

                        $scope.dtTimeEndSelect=new App.Shared.Gui.DateTimeControl('dtTimeSelectEndMobileReportTaxi');
                        $scope.dtTimeEndSelect.titleTime='';
                        $scope.dtTimeEndSelect.minday = -3;
                        //hiển thị button quay về
                        $scope.dtTimeStartSelect.valuetime_default = '00:00';
                        $scope.dtTimeEndSelect.valuetime_default = '23:59';
                        $scope.dtTimeStartSelect.onSelect = () =>{

                            $scope.dtTimeEndSelect.SetMinDatePicker($scope.dtTimeStartSelect.GetPickerDate_setmin());
                        };

                        //Khai báo lịch
                        // $scope.dtTimeSelect=new App.Shared.Gui.DateTimeControl('dtTimeSelectdetailtaxiMaster');
                        // $scope.dtTimeSelect.titleTime=language.gridLg().titleTime;
                        // // $scope.dtTimeSelect.open = 'right';
                        // //Khai báo lịch
                        // $scope.dtSingleSelect=new App.Shared.Gui.DateTimeControl('dtSingleDaySelectdetailtaxiMaster');
                        // $scope.dtSingleSelect.titleTime=language.gridLg().titleTime;


                        //Khai báo tài xế
                        $scope.driverSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IDriver>('cbDriverDetailtaixiMaster','Name','Id');
                        $scope.driverSelectSettings.setting.width = '100%';
                        $scope.driverSelectSettings.setting.placeHolder = 'Chọn tài xế';
                        $scope.driverSelectSettings.setting.filterPlaceHolder = 'Tìm tài xế...';
                        $scope.driverSelectSettings.title = language.gridLg().titleDriver;
                        $scope.driverSelectSettings.setting.checkboxes = false;

                        $scope.checkboxes_combobox = false;
                        $scope.checkzonelocation = true;
                        $scope.hideDriver = true; // ẩn dropdown tài xế
                        $scope.hideComboboxCar = true; // ẩn dropdown xe
                        $scope.showlocationandpoint = true; // hiển thị vùng và điểm
                        $scope.hidecalendar = false;
                        $scope.hidesinglecalendar = true;

                        // api.GetAllCompany().then((data:any)=>{
                        //     var result_promise=$q.defer();
                        //     //todo: thêm dạng promise để lấy dữ liệu được
                        //     var result:any = [];
                        //     $.each(data,(ind,val)=>{
                        //         if(val.Id !== 18){
                        //             result.push(val);
                        //         }
                        //     });
                        //     $scope.cpnSelectSettings.loaddataarraywithapi(result_promise.promise,$q,true);
                        //     result_promise.resolve(result);
                        // });
                        // load công ty 2 thông số truyền vào : dữ liệu lấy về, vị trí truyền vào ban đầu
                        // $scope.cpnSelectSettings.load(api.GetAllCompanyByType(),null,true);
                        $scope.cpnSelectSettings.loadPromise(api.GetAllCompanyByType(), null,$q).then(()=>{
                            if(localStorage.getItem('companyIdtaxi') !== '' && localStorage.getItem('companyIdtaxi') !== null){

                                let companyidtaxi:any = localStorage.getItem('companyIdtaxi');
                                $scope.cpnSelectSettings.setItemDefault((m:any)=>m.Id === companyidtaxi*1);
                            }else{
                                $scope.cpnSelectSettings.setting.jqxDropDownList('selectIndex',0);
                            }
                        });



                        //Sự kiện chọn công ty
                        $scope.cpnSelectSettings.customEvent.onSelectOnItem=(index:number,val:App.Shared.Entity.ICompany)=>{
                            var first=Array<App.Shared.Entity.IGroup>();
                            // đẩy 1 group làm thông tin chọn tất cả
                            first.push( {
                                Id: -1,
                                Name: 'Tất cả',
                                CompanyId:val.Id
                            });
                            localStorage.setItem('companyIdtaxi',`${val.Id}`,);
                            // var firstdriver=Array<App.Shared.Entity.IDriver>();
                            // // đẩy 1 group làm thông tin chọn tất cả
                            // firstdriver.push({
                            //     Id: -1,
                            //     Name: 'Tất cả',
                            //
                            // });
                            var first_zone =Array<any>();
                            // đẩy 1 group làm thông tin chọn tất cả
                            first_zone.push( {
                                Id: -1,
                                Name: 'Tất cả',
                            });
                            var first_point =Array<any>();
                            // đẩy 1 group làm thông tin chọn tất cả
                            first_point.push( {
                                Id: -1,
                                Name: 'Tất cả',
                            });
                            console.log(`cong ty select ${val.Id}`);
                            $scope.groupdeviceSelectSettings.load(api.GetAllGroupByCompanyId(val.Id),first,true);

                            $scope.driverSelectSettings.load(api.GetAllDriverByCompany(val.Id),null,false);
                            $scope.zoneSelectSetting.load(api.GetZoneByCompany(val.Id),first_zone);
                            $scope.pointSelectSetting.load(api.GetPOintByCompany(val.Id),first_point);
                        };

                        // cài đặt sự kiện chọn đội xe
                        $scope.groupdeviceSelectSettings.customEvent.onSelectOnItem = (indx:number,val_group:App.Shared.Entity.IGroup)=>{
                            var firstdevice=Array<App.Shared.Entity.IDevice>();
                            firstdevice.push( {
                                Serial: '-1',
                                Bs: 'Tất cả',
                                DisplayId: 'Tất cả'

                            });
                            console.log('group select');
                            // đẩy 1 group làm thông tin chọn tất cả
                            if(val_group.Id == -1){// hiển thị tất cả các xe trong công ty
                                $scope.modelSelectSettings.load(api.GetAllDeviceByCompanyId(val_group.CompanyId),firstdevice,false);
                                // $scope.modelSelectSettings.setting.jqxDropDownList('clearSelection');
                                $scope.modelSelectSettings_multi.load(api.GetAllDeviceByCompanyId(val_group.CompanyId),firstdevice,false);

                            }
                            else{// hiểm thị các xe trong đội xe đã chọn

                                $scope.modelSelectSettings.load(api.GetAllDeviceByGroupId(val_group.CompanyId,val_group.Id),firstdevice,false);
                                // $scope.modelSelectSettings.setting.jqxDropDownList('clearSelection');
                                $scope.modelSelectSettings_multi.load(api.GetAllDeviceByGroupId(val_group.CompanyId,val_group.Id),firstdevice,false);

                            }
                        };

                        /**
                         * Sự kiện chọn checkbox xe
                         */

                        $scope.modelSelectSettings_multi.customEvent.onCheckChange = (check:boolean, val:any)=>{
                            if(val.originalItem.Serial === '-1'){
                                if(check){
                                    $scope.modelSelectSettings_multi.setting.jqxDropDownList('checkAll');
                                }else{
                                    $scope.modelSelectSettings_multi.setting.jqxDropDownList('uncheckAll');
                                }
                            }
                        };

                        /**
                         * Sự kiện chọn tài xế
                         */

                        $scope.driverSelectSettings.customEvent.onCheckChange = (check: boolean, val:any)=>{
                            if(val.originalItem.Id === -1){
                                if(check){
                                    $scope.driverSelectSettings.setting.jqxDropDownList('checkAll');
                                }else{
                                    $scope.driverSelectSettings.setting.jqxDropDownList('uncheckAll');
                                }
                            }
                        };
                        /**
                         * Sự kiện chọn vùng
                         */
                        $scope.zoneSelectSetting.customEvent.onCheckChange = (check:boolean, val:any)=>{
                            if(val.originalItem.Id === -1){
                                if(check){
                                    $scope.zoneSelectSetting.setting.jqxDropDownList('checkAll');
                                }else{
                                    $scope.zoneSelectSetting.setting.jqxDropDownList('uncheckAll');
                                }
                            }
                        };
                        /**
                         * Sự kiện chọn điểm
                         */
                        $scope.pointSelectSetting.customEvent.onCheckChange = (check: boolean, val: any)=>{
                            if(val.originalItem.Id === -1){
                                if(check){
                                    $scope.pointSelectSetting.setting.jqxDropDownList('checkAll');
                                }else{
                                    $scope.pointSelectSetting.setting.jqxDropDownList('uncheckAll');
                                }
                            }
                        };


                    }
                }
                AdModule.controller('DetailreportController', DetailreportController);
            }

        }
    }
}