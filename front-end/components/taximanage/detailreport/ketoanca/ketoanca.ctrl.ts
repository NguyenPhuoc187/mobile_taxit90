/**
 * Created by phamn on 15/11/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Ketoanca{
                    export interface IKetoancaScope extends ng.IScope{
                        gridThongtinchotca:App.Shared.Directives.GridControl;
                        gridThongtincuocca:App.Shared.Directives.GridControl;
                        gridCuochopdong:App.Shared.Directives.GridviewControl;
                        $parent: IDetailreportScope;
                        viewcuoc:()=>any;
                        phidichvu:boolean; // phí dịch vụ
                        dataphidichvu:any;
                        xuatbaocao:()=>any;
                        nuacuoc:boolean;
                        dataxuatbaocao:any;
                        trustAsHtml:(name:string)=>any;
                        print:()=>any;
                        exportword:()=>any;
                        chotca:() => any;
                        datachottam:any;
                        inputMoney:any;
                        itemthongtinca:any;
                        sogiokokinhdoanh:number;
                    }
                    export class KetoancaController{
                        public static $inject = ['$scope','language','global', '$http'];
                        constructor($scope: IKetoancaScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService, $http: ng.IHttpService) {
                            // $(window).resize(function(){
                            //     $scope.gridThongtincuocca.refresh();
                            //     $scope.gridCuochopdong.refresh();
                            // });
                            $scope.$on("$destroy", ()=> {
                                $('div#printbaocao_kettoan').remove();
                            });
                            $scope.$parent.viewData = language.gridLg().viewData;
                            $scope.$parent.checkboxes_combobox = false;
                            $scope.$parent.checkzonelocation = false;
                            $scope.$parent.showlocationandpoint = false;
                            $scope.$parent.hideComboboxCar = false;
                            $scope.$parent.hidecalendar = false;
                            $scope.$parent.hideComboboxGroupCar = false;
                            $scope.$parent.hidesinglecalendar = true;
                            $scope.nuacuoc = false;
                            $scope.sogiokokinhdoanh = 0;

                            $scope.inputMoney = [];
                            $scope.itemthongtinca = {};

                            $scope.gridThongtinchotca = new App.Shared.Directives.GridControl("gridThongtinchotca");
                            $scope.gridThongtinchotca.selectionModeinfo = 'singlerow';
                            $scope.gridThongtinchotca.filename_excel = "BÁO CÁO THÔNG TIN CHỐT CA";
                            $scope.gridThongtinchotca.columns = [];
                            $scope.gridThongtinchotca.height = 500;
                            $scope.gridThongtinchotca.columnsHeight = 30;
                            $scope.gridThongtinchotca.groupable = false;
                            $scope.gridThongtinchotca.groups = [];
                            //bật tính năng lọc dữ liệu
                            // $scope.gridThongtinchotca.isshowAggregates = true;
                            // $scope.gridThongtinchotca.behavior.isCheckSelect = flase;

                            $scope.gridThongtinchotca.isAltRows = true;
                            $scope.gridThongtinchotca.isFilter = true;
                            $scope.gridThongtinchotca.isFilterMode = 'advanced';

                            $scope.gridThongtinchotca.columns.push({
                                text: 'STT',
                                dataField: 'STT',
                                type: 'number',
                                align: 'center',
                                width: '35px',
                                pinned: true
                            });
                            $scope.gridThongtinchotca.columns.push({
                                text: 'Id',
                                dataField: 'Id',
                                type: 'number',
                                align: 'center',
                                width: '50px',
                                hidden: true,

                            });
                            $scope.gridThongtinchotca.columns.push({
                                text: 'Thời gian chốt',
                                dataField: 'UpdateTime_show',
                                type: 'date',
                                width: '100px',
                                align: 'center',
                                filterable: true,
                                pinned: true,
                                cellsformat: 'HH:mm dd/MM/yyyy'
                            });
                            $scope.gridThongtinchotca.columns.push({
                                text: 'Đội xe',
                                dataField: 'GroupName',
                                type: 'string',
                                width: '80px',
                                align: 'center',
                                filterable: true
                            });
                            $scope.gridThongtinchotca.columns.push({
                                text: 'Số tài - Biển số',
                                dataField: 'Display',
                                type: 'string',
                                width: '100px',
                                align: 'center',
                                filterable: true
                            });
                            $scope.gridThongtinchotca.columns.push({
                                text: 'Tài xế',
                                dataField: 'DriverName',
                                type: 'string',
                                width: '130px',
                                align: 'center',
                                filterable: true
                            });
                            $scope.gridThongtinchotca.columns.push({
                                text: 'Tiền trong ca',
                                dataField: 'TongTien',
                                type: 'number',
                                width: 'auto',
                                align: 'center',
                                filterable: true,
                                cellsformat: 'n'
                            });
                            $scope.gridThongtinchotca.columns.push({
                                text: 'Thời gian kết thúc',
                                dataField: 'LastChotCa',
                                type: 'string',
                                width: '200px',
                                align: 'center',
                                filterable: true,
                                hidden: true
                            });
                            $scope.gridThongtinchotca.columns.push({
                                text: 'Thời gian bắt đầu',
                                dataField: 'UpdateTime',
                                type: 'string',
                                width: '200px',
                                align: 'center',
                                filterable: true,
                                hidden: true
                            });
                            $scope.gridThongtinchotca.columns.push({
                                text: 'Id công ty',
                                dataField: 'CompanyId',
                                type: 'number',
                                width: '200px',
                                align: 'center',
                                filterable: true,
                                hidden: true
                            });
                            $scope.gridThongtinchotca.columns.push({
                                text: 'Serial',
                                dataField: 'Serial',
                                type: 'string',
                                width: '200px',
                                align: 'center',
                                filterable: true,
                                hidden: true
                            });
                            $scope.gridThongtinchotca.columns.push({
                                text: '',
                                dataField: 'ButtonChot',
                                type: 'string',
                                width: '70px',
                                align: 'center',
                                // filterable: true,
                                // hidden: true
                            });


                            $scope.gridThongtincuocca = new App.Shared.Directives.GridControl("gridThongtincuocca");
                            $scope.gridThongtincuocca.selectionModeinfo = 'singlerow '; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridThongtincuocca.filename_excel = "BÁO CÁO THÔNG TIN CUỐC TRONG CA";
                            $scope.gridThongtincuocca.columns = [];
                            $scope.gridThongtincuocca.height = 500;
                            $scope.gridThongtincuocca.columnsHeight = 40;
                            $scope.gridThongtincuocca.groupable= false;
                            $scope.gridThongtincuocca.groups= [];

                            //bật tính năng lọc dữ liệu
                            $scope.gridThongtincuocca.isshowAggregates = true;
                            $scope.gridThongtincuocca.isAltRows = true;
                            $scope.gridThongtincuocca.isFilter = true;
                            $scope.gridThongtincuocca.isFilterMode = 'advanced';

                            $scope.gridThongtincuocca.columns.push({
                                text: 'STT',
                                dataField: 'STT',
                                type: 'number',
                                align: 'center',
                                width: '35px',
                                pinned:true
                            });
                            //$scope.gridThongtinchotca.columns.push({ text: 'Serial', dataField: 'Serial', type: 'string', align: 'center',width:'50px' });
                            $scope.gridThongtincuocca.columns.push({
                                text: 'ID Cuốc',
                                dataField: 'SessionTaxiId',
                                type: 'number',
                                align: 'center',
                                width: '60px',
                                filterable: true,
                                pinned:true,
                            });

                            $scope.gridThongtincuocca.columns.push({
                                text: 'Thời gian chờ',
                                dataField: 'ThoiGianCho',
                                type: 'string',
                                align: 'center',
                                filterable: true,
                                width: '80px',
                                columngroup: 'thongtincuoc',
                                className: 'gridtaxidetail_height40_2row'
                            });
                            $scope.gridThongtincuocca.columns.push({
                                text: 'TG đón trả khách',
                                dataField: 'TimeTotal',
                                type: 'string',
                                align: 'center',
                                filterable: true,
                                width: '164px',
                                columngroup: 'thongtincuoc',
                                className: 'gridtaxidetail_height40_2row'
                            });


                            $scope.gridThongtincuocca.columns.push({
                                text: 'Tổng tiền',
                                dataField: 'TongTien',
                                type: 'number',
                                align: 'center',
                                width: '130px',
                                filterable: true,
                                columngroup: 'thongtincuoc',
                                cellsformat: 'n',
                                aggregates: ['sum'],
                            });
                            $scope.gridThongtincuocca.columns.push({
                                text: 'Km rỗng',
                                dataField: 'KmRong',
                                type: 'number',
                                align: 'center',
                                width: '100px',
                                filterable: true,
                                columngroup: 'thongtincuoc',
                                cellsformat: 'n',
                                aggregates: ['sum'],
                            });
                            // $scope.gridThongtincuocca.columns.push({
                            //     text: 'Tổng Km GPS(m)',
                            //     dataField: 'TongKmGps',
                            //     type: 'number',
                            //     align: 'center',
                            //     width: '120px',
                            //     cellsformat: 'n',
                            //     filterable: true,
                            //     columngroup: 'thongtincuoc',
                            //     aggregates: [{
                            //         'Total': function (aggregatedValue: any, currentValue: any, column: any, record: any) {
                            //             let total = currentValue * 1;
                            //             return aggregatedValue + total;
                            //         }
                            //     }],
                            //     aggregatesRenderer: function (aggregates: any, column: any, element: any) {
                            //         var renderString = "<div style='margin: 4px; float: left;  height: 100%;'>";
                            //         renderString += "<span>Tổng = </span>" + ((aggregates.Total != undefined) ? aggregates.Total : 0) + "</div>";
                            //         return renderString;
                            //     }
                            // });
                            $scope.gridThongtincuocca.columns.push({
                                text: 'Km(đồng hồ)',
                                dataField: 'TongKmDongHo',
                                type: 'string',
                                align: 'center',
                                width: '100px',
                                filterable: true,
                                columngroup: 'thongtincuoc',
                                className: 'gridtaxidetail_height40_2row',
                                cellsformat: 'n',
                                aggregates: ['sum'],
                            });
                            // $scope.gridThongtincuocca.columns.push({
                            //     text: 'Tỉ lệ km(dh)/km(gps)',
                            //     dataField: 'TyLeKm',
                            //     type: 'number',
                            //     align: 'center',
                            //     width: '110px',
                            //     filterable: true,
                            //     columngroup: 'thongtincuoc',
                            //     cellsformat: 'p',
                            //     className: 'gridtaxidetail_height40_2row'
                            // });
                            $scope.gridThongtincuocca.columns.push({
                                text: 'Cuốc 0km',
                                dataField: 'CuocKhongKm_show',
                                type: 'string',
                                align: 'center',
                                width: '40px',
                                columngroup: 'thongtincuoc',
                                className: 'gridtaxidetail_height40_2row'
                            });
                            $scope.gridThongtincuocca.columns.push({
                                text: 'Cuốc hợp đồng',
                                dataField: 'IsCuocHopDong_show',
                                type: 'string',
                                align: 'center',
                                width: '70px',
                                columngroup: 'thongtincuoc',
                                className: 'gridtaxidetail_height40_2row'
                            });

                            $scope.gridThongtincuocca.columnGroups.push({text: 'Điểm đón khách', name: 'diemdon'});
                            $scope.gridThongtincuocca.columnGroups.push({text: 'Điểm trả khách', name: 'diemtra'});
                            // $scope.gridThongtincuocca.columnGroups.push({text: 'Thông tin cuốc', name: 'thongtincuoc'});
                            $scope.gridThongtincuocca.columnGroups.push({text: 'Thông tin xe', name: 'thongtinxe'});


                            $scope.gridCuochopdong = new App.Shared.Directives.GridviewControl("gridCuochopdong");
                            $scope.gridCuochopdong.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridCuochopdong.filename_excel = "BÁO CÁO CUỐC HỢP ĐỒNG";
                            $scope.gridCuochopdong.columns = [];
                            $scope.gridCuochopdong.height = 500 ;
                            $scope.gridCuochopdong.columnsHeight = 40;
                            //note: bật tính năng tìm kiếm nâng cao
                            $scope.gridCuochopdong.isAltRows = true;
                            $scope.gridCuochopdong.isshowAggregates = true;
                            $scope.gridCuochopdong.isFilter = true;
                            $scope.gridCuochopdong.isFilterMode = 'advanced';
                            //note: bật tính năng thêm, xóa , sửa
                            // $scope.gridCuochopdong.behavior.isNew = false;
                            // $scope.gridCuochopdong.behavior.isEdit = true;
                            // $scope.gridCuochopdong.behavior.isDelete = true;
                            $scope.gridCuochopdong.columns.push({
                                text: 'STT',
                                dataField: 'STT',
                                type: 'string',
                                align: 'center',
                                width: '35px',
                                pinned:true
                            });
                            // $scope.gridCuochopdong.columns.push({
                            //     text: 'ID cuốc',
                            //     dataField: 'Id',
                            //     type: 'number',
                            //     align: 'center',
                            //     width: '70px',
                            //     filterable: true,
                            //     pinned:true
                            // });
                            $scope.gridCuochopdong.columns.push({
                                text: 'Số tài - Biển số ',
                                dataField: 'Display',
                                type: 'string',
                                align: 'center',
                                width: '100px',
                                filterable: true,
                                pinned:true
                            });
                            $scope.gridCuochopdong.columns.push({
                                text: 'Điểm đón trả khách',
                                dataField: 'Address',
                                type: 'string',
                                align: 'center',
                                width: '150px',
                                filterable: true
                            });

                            $scope.gridCuochopdong.columns.push({
                                text: 'TG xuất phát',
                                dataField: 'BeginTime',
                                type: 'string',
                                align: 'center',
                                width: '100px'
                            });
                            $scope.gridCuochopdong.columns.push({
                                text: 'TG chênh lệch',
                                dataField: 'ChenhLechTime',
                                type: 'string',
                                align: 'center',
                                width: '100px'
                            });
                            $scope.gridCuochopdong.columns.push({
                                text: 'Cuốc khớp',
                                dataField: 'CuocKhopId',
                                type: 'string',
                                align: 'center',
                                width: '80px',
                                filterable: true
                            });
                            $scope.gridCuochopdong.columns.push({
                                text: 'Người tạo',
                                dataField: 'NguoiTao',
                                type: 'string',
                                align: 'center',
                                width: '100px',
                                filterable: true
                            });
                            $scope.gridCuochopdong.columns.push({
                                text: 'Kiểu tính tiền quá',
                                dataField: 'KieuTinhTien',
                                type: 'string',
                                align: 'center',
                                width: '100px',
                                filterable: true
                            });
                            $scope.gridCuochopdong.columns.push({
                                text: 'Cuốc 2 chiều',
                                dataField: 'Cuoc2Chieu_show',
                                type: 'string',
                                align: 'center',
                                width: '80px',
                                filterable: true
                            });
                            $scope.gridCuochopdong.columns.push({
                                text: 'Cuốc vãng lai',
                                dataField: 'CuocVangLai_show',
                                type: 'string',
                                align: 'center',
                                width: '80px',
                                filterable: true
                            });
                            // $scope.gridCuochopdong.columns.push({
                            //     text: 'Điểm bắt đầu',
                            //     dataField: 'AdrressStart',
                            //     type: 'string',
                            //     align: 'center',
                            //     width: '100px',
                            //     filterable: true
                            // });
                            // $scope.gridCuochopdong.columns.push({
                            //     text: 'Điểm kết thúc',
                            //     dataField: 'AdrressEnd',
                            //     type: 'string',
                            //     align: 'center',
                            //     width: '100px',
                            //     filterable: true
                            // });
                            $scope.gridCuochopdong.columns.push({
                                text: 'TG dự kiến',
                                dataField: 'TimeRun',
                                type: 'string',
                                align: 'center',
                                width: '100px',
                                filterable: true
                            });
                            $scope.gridCuochopdong.columns.push({
                                text: 'Km dự kiến(m)',
                                dataField: 'KmRun',
                                type: 'string',
                                align: 'center',
                                width: '100px',
                                filterable: true,
                                cellsFormat: 'n'
                            });
                            $scope.gridCuochopdong.columns.push({
                                text: 'Tiền dự kiến(VNĐ)',
                                dataField: 'MoneyRun',
                                type: 'string',
                                align: 'center',
                                width: '100px',
                                filterable: true,
                                cellsFormat: 'n'
                            });
                            $scope.gridCuochopdong.columns.push({
                                text: 'Tiền quá mỗi km(VNĐ)',
                                dataField: 'MoneyEveryKm',
                                type: 'string',
                                align: 'center',
                                width: '100px',
                                filterable: true,
                                cellsFormat: 'n'
                            });
                            $scope.gridCuochopdong.columns.push({
                                text: 'Tiền quá mỗi giờ(VNĐ)',
                                dataField: 'MoneyEveryTime',
                                type: 'string',
                                align: 'center',
                                width: '100px',
                                filterable: true,
                                cellsFormat: 'n'
                            });
                            $scope.gridCuochopdong.columns.push({
                                text: 'Tiền quá km(VNĐ)',
                                dataField: 'TienQuaKm',
                                type: 'string',
                                align: 'center',
                                width: '100px',
                                filterable: true,
                                columngroup: 'thongtinketoan',
                                cellsFormat: 'n',
                                className: 'gridtaxidetail_height40_2row'
                            });
                            $scope.gridCuochopdong.columns.push({
                                text: 'Tiền quá giờ(VNĐ)',
                                dataField: 'TienQuaGio',
                                type: 'string',
                                align: 'center',
                                width: '100px',
                                filterable: true,
                                columngroup: 'thongtinketoan',
                                cellsFormat: 'n',
                                className: 'gridtaxidetail_height40_2row'
                            });
                            $scope.gridCuochopdong.columns.push({
                                text: 'Tiền tài xế nộp(VNĐ)',
                                dataField: 'TienTaiXeNop',
                                type: 'string',
                                align: 'center',
                                width: 'auto',
                                filterable: true,
                                columngroup: 'thongtinketoan',
                                cellsFormat: 'n',
                                aggregates: ['sum'],
                            });
                            $scope.gridCuochopdong.columnGroups.push({
                                text: 'Cuốc thực tế',
                                name: 'thongtinketoan'
                            });


                            //note: nhấn nút xem để lấy thông tin

                            $scope.$parent.btViewClick = () => {
                                $scope.inputMoney = [];
                                if ($scope.$parent.cpnSelectSettings.itemSelect() !== undefined) {
                                    if ($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1) {
                                        //note: trường hợp chọn đội xe là tất cả
                                        if ($scope.$parent.modelSelectSettings.itemSelect() == undefined || $scope.$parent.modelSelectSettings.itemSelect().Serial == '-1') {
                                            GetDataThongTinChotCaWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.dtTimeSelect.begin(), $scope.$parent.dtTimeSelect.end());
                                            GetDataPhiDichVuCompany($scope.$parent.cpnSelectSettings.itemSelect().Id);
                                        } else {
                                            GetDataThongTinChotCaWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.modelSelectSettings.itemSelect().Serial, $scope.$parent.dtTimeSelect.begin(), $scope.$parent.dtTimeSelect.end())
                                            GetDataPhiDichVuCompany($scope.$parent.cpnSelectSettings.itemSelect().Id);
                                        }
                                    } else {
                                        //note: trường hợp chọn đội xe không phải là tất cả
                                        if ($scope.$parent.modelSelectSettings.itemSelect() == undefined || $scope.$parent.modelSelectSettings.itemSelect().Serial == '-1') {
                                            GetDataThongTinChotCaWithGroup($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.groupdeviceSelectSettings.itemSelect().Id, $scope.$parent.dtTimeSelect.begin(), $scope.$parent.dtTimeSelect.end());
                                            GetDataPhiDichVu($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.groupdeviceSelectSettings.itemSelect().Id);
                                        } else {
                                            GetDataThongTinChotCaWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.modelSelectSettings.itemSelect().Serial, $scope.$parent.dtTimeSelect.begin(), $scope.$parent.dtTimeSelect.end());
                                            GetDataPhiDichVu($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.groupdeviceSelectSettings.itemSelect().Id);
                                        }
                                    }
                                }
                            };

                            // $scope.gridThongtinchotca.event.onDoubleRowClick = (item:any)=>{
                            //     $scope.itemthongtinca = item;
                            //     GetDataCuocXe(item.CompanyId, item.Serial,item.LastChotCa, item.UpdateTime);
                            //     GetDataWithSerialCuocHopDong(item.CompanyId, item.LastChotCa, item.UpdateTime, [item.Serial]);
                            // };
                            //note: Khi nhấn nút xem thông tin cuốc xe
                            // $scope.viewcuoc = () => {
                            //     console.log('click xem cuốc xe');
                            //     if($scope.gridThongtinchotca.event.onClick !== undefined){
                            //         $scope.gridThongtinchotca.event.onClick = (item:any) => {
                            //             console.log(item);
                            //         }
                            //     }
                            //     // let row: any = $scope.gridThongtinchotca.selectRowInfo();
                            //     // console.log(row);
                            //     // GetDataCuocXe(row[0].CompanyId, row[0].Serial, row[0].LastChotCa, row[0].UpdateTime);
                            //     // GetDataWithSerialCuocHopDong(row[0].CompanyId, row[0].LastChotCa, row[0].UpdateTime, [row[0].Serial]);
                            // };


                            //note: khi nhấn nút xuất báo cáo
                            $scope.xuatbaocao = () => {

                                if($scope.itemthongtinca.Id !== undefined){
                                    //nếu cuốc cũ đã được chốt và clear thành
                                    if($scope.itemthongtinca.Id !== 0){
                                        var tmp: any = [];
                                        $.each($scope.dataphidichvu, (ind: number, val: any) => {
                                            tmp.push(
                                                {
                                                    Id: val.Id,
                                                    Money: ($scope.inputMoney[ind]!=undefined)?$scope.inputMoney[ind]:val.Money
                                                }
                                            );
                                        });
                                        $scope.datachottam = {
                                            NuaCuoc: $scope.nuacuoc,
                                            PhiDichVuTranfers: tmp,
                                            ChotCaLogId: $scope.itemthongtinca.Id,
                                            CompanyId: $scope.itemthongtinca.CompanyId,
                                            SoGioKhongKinhDoanh: $scope.sogiokokinhdoanh,
                                        };
                                        $http.post(global.ReportHost + 'api/KetToanCa/HtmlKetToanCa', $scope.datachottam).then((rep: any) => {
                                            if (rep.data.Status == 1) {
                                                $scope.dataxuatbaocao = rep.data.Html;
                                                $('#baocao').modal('show');
                                            }
                                        });
                                        $('#baocao').on('shown.bs.modal',()=> {
                                            $('label.chitietcuocxe').append("<input type='checkbox' id='check_chitietcuocxe' style='margin-top: -17px'/>"); // mặc định là bỏ check (a tí yêu cầu)
                                            $('label.cuochopdong').append("<input type='checkbox' id='check_cuochopdong' style='margin-top: -17px;margin-left: 11px;'/>");// mặc định là bỏ check(a tí yêu cầu)
                                            $('table.chitietcuocxe').hide();
                                            $('table.cuochopdong').hide();
                                            $('#check_chitietcuocxe').click((event:any) => {
                                                console.log(event);
                                                if(!event.currentTarget.checked){
                                                    $('table.chitietcuocxe').hide();
                                                }else{
                                                    $('table.chitietcuocxe').show();
                                                }
                                            });
                                            $('#check_cuochopdong').click((event:any) => {
                                                console.log(event);
                                                if(!event.currentTarget.checked){
                                                    $('table.cuochopdong').hide();
                                                }else{
                                                    $('table.cuochopdong').show();
                                                }
                                            });
                                        });
                                    }else{
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Vui lòng chọn cuốc cần kết toán!!!",
                                        });
                                    }

                                }else{
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Vui lòng chọn cuốc cần kết toán!!!",
                                    });
                                }

                                // if($('#check_chitietcuocxe') !== undefined && $('#check_cuochopdong') !== undefined){
                                //     $('#check_chitietcuocxe').remove();
                                //     $('#check_cuochopdong').remove();
                                //
                                // }

                                // console.log('chay check box');

                            };

                            //    note: xử lý sự kiện in trên modal
                            $scope.print = () => {
                                // printElement(document.getElementById("printbaocao"),null,null);
                                // printElement(document.getElementById("printThisToo"), true, "<hr />");
                                // window.print();
                                // window.print();
                                $("#printbaocao").print(
                                    {
                                        globalStyles: true,
                                        mediaPrint: false,
                                        stylesheet: null,
                                        noPrintSelector: ".no-print",
                                        iframe: true,
                                        append: null,
                                        prepend: null,
                                        manuallyCopyFormValues: true,
                                        deferred: $.Deferred(),
                                        timeout: 750,
                                        title: null,
                                        doctype: '<!doctype html>'
                                    }
                                );

                                // $('label.chitietcuocxe').hide();
                                // $('label.cuochopdong').hide();

                                // $("#printbaocao").printMe();

                            };
                            $scope.exportword = () => {
                                $("#xuatword").wordExport('BAO CAO CHI TIET CHOT CA CUA LAI XE');
                            };

                            //hàm in
                            // function printElement(elem:any, append?:any, delimiter?:any) {
                            //     var domClone = elem.cloneNode(true);
                            //
                            //     var $printSection = document.getElementById("printSection");
                            //
                            //     if (!$printSection) {
                            //         var $printSection = document.createElement("div");
                            //         $printSection.id = "printSection";
                            //         document.body.appendChild($printSection);
                            //     }
                            //
                            //     if (append !== true) {
                            //         $printSection.innerHTML = "";
                            //     }
                            //
                            //     else if (append === true) {
                            //         if (typeof(delimiter) === "string") {
                            //             $printSection.innerHTML += delimiter;
                            //         }
                            //         else if (typeof(delimiter) === "object") {
                            //             $printSection.appendChlid(delimiter);
                            //         }
                            //     }
                            //
                            //     $printSection.appendChild(domClone);
                            // }

                            // note: xử lý chốt ca cuối cùng
                            $scope.chotca = () =>{
                                language.showLoading(false, "");
                                $http.post(global.ReportHost + 'api/KetToanCa/TaoKetToanCa',$scope.datachottam).then((rep:any)=>{
                                    language.showLoading(true, "");
                                   if(rep.data.Status == 1){
                                       bootbox.dialog({
                                           title: "Thông Báo",
                                           message: "Kết toán ca thành công!!!",
                                           buttons: {
                                               confirm: {
                                                   label: "Đồng ý",
                                                   className: "btn-success",
                                                   // callback:()=>{
                                                   //     $scope.$parent.btViewClick();
                                                   // }
                                               }
                                           }
                                       });
                                       $scope.$parent.btViewClick();
                                       $scope.itemthongtinca.Id =0;
                                       $scope.gridThongtincuocca.clear();
                                       $scope.gridCuochopdong.clear();
                                       $('#baocao').modal('hide');
                                   }else{
                                       bootbox.dialog({
                                           title: "Thông Báo",
                                           message: "Kết toán ca không thành công!!!",
                                           buttons: {
                                               confirm: {
                                                   label: "Quay lại",
                                                   className: "btn-danger",
                                               }
                                           }
                                       });
                                   }
                                });
                            };


                            /**
                             * Lấy thông tin chốt ca theo công ty
                             * @param companyId
                             * @param beginTime
                             * @param endTime
                             * @constructor
                             */

                            function GetDataThongTinChotCaWithCompany(companyId: number, beginTime: string, endTime: string) {
                                language.showLoading(false, "");
                                $http.get(global.ReportHost + `api/ChotCaTaxi/ChotCaKtByCompany?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}&daKetToanCa=false`).then((rep: any) => {
                                    console.log(rep);
                                    if (rep.data.Status == 1) {
                                        $scope.gridThongtinchotca.data = rep.data.ChotCaLogTranfers;
                                        ConvertData($scope.gridThongtinchotca.data);
                                        $scope.gridThongtinchotca.refresh();
                                        language.showLoading(true, "");
                                    } else {
                                        console.debug(rep.data.Description);
                                        language.showLoading(true, "");
                                    }
                                });

                            }

                            function GetDataThongTinChotCaWithSerial(companyId: number, serial: string, beginTime: string, endTime: string) {
                                language.showLoading(false, "");
                                $http.get(global.ReportHost + `api/ChotCaTaxi/ChotCaKtLamda?companyId=${companyId}&serial=${serial}&driverId=0&beginTime=${beginTime}&endTime=${endTime}&daKetToanCa=false`).then((rep: any) => {
                                    if (rep.data.Status == 1) {
                                        $scope.gridThongtinchotca.data = rep.data.ChotCaLogTranfers;
                                        ConvertData($scope.gridThongtinchotca.data);
                                        $scope.gridThongtinchotca.refresh();
                                        language.showLoading(true, "");
                                    } else {
                                        console.debug(rep.data.Description);
                                        language.showLoading(true, "");
                                    }
                                });
                            }

                            function GetDataThongTinChotCaWithGroup(companyId: number, groupId: number, beginTime: string, endTime: string) {
                                language.showLoading(false, "");
                                $http.get(global.ReportHost + `api/ChotCaTaxi/ChotCaKtByGroup?companyId=${companyId}&groupId=${groupId}&beginTime=${beginTime}&endTime=${endTime}&daKetToanCa=false`).then((rep: any) => {
                                    if (rep.data.Status == 1) {
                                        $scope.gridThongtinchotca.data = rep.data.ChotCaLogTranfers;
                                        ConvertData($scope.gridThongtinchotca.data);
                                        $scope.gridThongtinchotca.refresh();
                                        language.showLoading(true, "");
                                    } else {
                                        console.debug(rep.data.Description);
                                        language.showLoading(true, "");
                                    }
                                });
                            }

                            /**
                             * Hàm lấy dữ liệu cuốc xe theo serial
                             * @param: companyId id của công ty
                             * @param serial
                             * @param beginTime
                             * @param endTime
                             * @constructor
                             */

                            function GetDataCuocXe(companyId: number, serial: string, beginTime: string, endTime: string) {
                                $http.get(global.ReportHost + `api/SessionTaxiLog/SessionTaxiLogBySerial?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}&serial=${serial}`).then((rep: any) => {
                                    if (rep.data.Status == 1) {
                                        $scope.gridThongtincuocca.data = rep.data.SessionTaxiLogList;
                                        ConvertDataCuocXe($scope.gridThongtincuocca.data);
                                        $scope.gridThongtincuocca.refresh();
                                        language.showLoading(true, "");
                                    } else {
                                        console.debug(rep.data.Description);
                                        language.showLoading(true, "");
                                    }
                                });
                            }

                            /**
                             * Hàm lấy dữ liệu Cuốc hợp đồng
                             * @param companyId công ty
                             * @param beginTime thời gian bắt đầu
                             * @param endTime thời gian kết thúc
                             * @param serial list serial
                             * @constructor
                             */
                            function GetDataWithSerialCuocHopDong(companyId: number, beginTime: string, endTime: string, serial: any) {
                                language.showLoading(false, "");
                                $http.post(global.ReportHost + `api/SessionContract/SessionContractBySerial?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`, serial).then((rep: any) => {
                                    if (rep.data.Status == 1) {
                                        $scope.gridCuochopdong.data = rep.data.SessionContractList;
                                        ConvertDataCuocHopDong($scope.gridCuochopdong.data);
                                        $scope.gridCuochopdong.refresh();
                                        language.showLoading(true, "");
                                    } else {
                                        console.debug(rep.data.Description);
                                        language.showLoading(true, "");
                                    }
                                });
                            }

                            function GetDataPhiDichVu(companyId: number, groupId: number) {
                                language.showLoading(false, "");
                                $http.get(global.ReportHost + `api/InputTaxi/FeeServiceByGroup?companyId=${companyId}&deviceGroupId=${groupId}`).then((rep: any) => {
                                    if (rep.data.Status == 1) {
                                        if (rep.data.FeeServiceList.length > 0) {
                                            $scope.phidichvu = true;
                                            $scope.dataphidichvu = rep.data.FeeServiceList;

                                        }
                                        // $scope.gridFeeservice.data = rep.data.FeeServiceList;
                                        // ConvertData($scope.gridFeeservice.data);
                                        // $scope.gridFeeservice.refresh();
                                        language.showLoading(true, "");
                                        // $scope.$apply();
                                    } else {
                                        $scope.phidichvu = false;
                                        console.debug(rep.data.Description);
                                        language.showLoading(true, "");
                                    }
                                });
                            }

                            function GetDataPhiDichVuCompany(companyId:number){
                                language.showLoading(false, "");
                                $http.get(global.ReportHost + `api/InputTaxi/FeeServiceByCompany?companyId=${companyId}`).then((rep: any) => {
                                    if (rep.data.Status == 1) {
                                        if (rep.data.FeeServiceList.length > 0) {
                                            $scope.phidichvu = true;
                                            $scope.dataphidichvu = rep.data.FeeServiceList;

                                        }
                                        // $scope.gridFeeservice.data = rep.data.FeeServiceList;
                                        // ConvertData($scope.gridFeeservice.data);
                                        // $scope.gridFeeservice.refresh();
                                        language.showLoading(true, "");
                                        // $scope.$apply();
                                    } else {
                                        $scope.phidichvu = false;
                                        console.debug(rep.data.Description);
                                        language.showLoading(true, "");
                                    }
                                });
                            }
                           window.xemthongtinchotca = (id:any) => {

                                console.log('chay ham',$(`#${id}`).attr('data-item'));

                                var serial:any = $(`#${id}`).attr('data-serial');
                                var company:any = $(`#${id}`).attr('data-company');
                                var lastchotca:any = $(`#${id}`).attr('data-lastchotca');
                                var updatetime:any = $(`#${id}`).attr('data-updatetime');
                                var id:any = $(`#${id}`).attr('data-item');
                                $scope.itemthongtinca = {
                                    Id: id,
                                    CompanyId: company,
                                };
                               GetDataCuocXe(company, serial,lastchotca, updatetime);
                               GetDataWithSerialCuocHopDong(company, lastchotca, updatetime, [serial]);
                                // console.log(Serial);
                                // console.log(LastChotCa);
                                // console.log(UpdateTime);
                           };
                            //note: hàm chuyển dữ liệu
                            function ConvertData(value: any) {
                                $.each(value, (ind: number, val: any) => {
                                    val.STT = ind + 1;
                                    val.Display = `<span class="text-danger">${val.SoHieu} - ${val.Bs}</span>`;
                                    val.UpdateTime_show = val.UpdateTime + '+07:00';
                                    // val.UpdateTime = val.UpdateTime + '+07:00';
                                    val.ButtonChot = "<button class='btn btn-success' title='Xem cuốc trong ca' data-item='"+ val.Id + "' data-company='"+ val.CompanyId + "' data-serial='"+ val.Serial + "' data-lastchotca='"+ val.LastChotCa + "' data-updatetime='"+ val.UpdateTime + "' onclick='xemthongtinchotca("+val.Id+")' id='"+val.Id+"'><i class='icon icon-cloud-download2'></i></button>";
                                    // item.CompanyId, item.Serial,item.LastChotCa, item.UpdateTime
                                });
                            }

                            //note: hàm chuyển dữ liệu của thông tin cuốc xe
                            function ConvertDataCuocXe(value: any) {
                                $.each(value, (ind: number, val: any) => {
                                    val.STT = ind + 1;
                                    val.Display = `<span class="text-danger">${val.SoHieu} - ${val.Bs}</span>`;
                                    val.TimeTotal = `<strong>Đón:</strong> ${global.AdapterDate(val.BeginTime)} </br> <strong>Trả :</strong> ${global.AdapterDate(val.EndTime)}`;
                                    if(val.BeginLocation !== null && val.EndLocation !== null){
                                        if(val.BeginLocation.Address !== null && val.EndLocation.Address !== null){
                                            val.Address = `<strong>Đón:</strong> ${val.BeginLocation.Address} </br> <strong>Trả : </strong> ${val.EndLocation.Address}`;
                                        }else{
                                            val.Address = '';
                                        }
                                    }

                                    val.BeginTime = global.AdapterDate(val.BeginTime);
                                    val.EndTime = global.AdapterDate(val.EndTime);
                                    if (val.CuocKhongKm) {
                                        val.CuocKhongKm_show = '<span class="icon-checkbox-checked"></span>';

                                    } else {
                                        val.CuocKhongKm_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if (val.IsCuocHopDong) {
                                        val.IsCuocHopDong_show = '<span class="icon-checkbox-checked"></span>';

                                    } else {
                                        val.IsCuocHopDong_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                });
                            }

                            //note: hàm chuyển dữ liệu của thông tin cuốc hợp đồng
                            function ConvertDataCuocHopDong(value: any) {
                                $.each(value, (ind: number, val: any) => {
                                    val.STT = ind + 1;

                                    val.Display = `<span class="text-danger">${val.SoHieu} - ${val.Bs}</span>`;
                                    if(val.BeginLocation !== null && val.EndLocation !== null){
                                        if(val.BeginLocation.Address !== null && val.EndLocation.Address !== null){
                                            val.Address = `<strong>Đón:</strong> ${val.BeginLocation.Address} </br> <strong>Trả : </strong> ${val.EndLocation.Address}`;
                                        }else{
                                            val.Address = '';
                                        }
                                    }

                                    val.BeginTime = global.AdapterDate(val.BeginTime);
                                    // val.ChenhLechTime = global.AdapterDate(val.ChenhLechTime);
                                    if (val.Cuoc2Chieu) {
                                        val.Cuoc2Chieu_show = '<span class="icon-checkbox-checked"></span>';

                                    } else {
                                        val.Cuoc2Chieu_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if (val.CuocVangLai) {
                                        val.CuocVangLai_show = '<span class="icon-checkbox-checked"></span>';

                                    } else {
                                        val.CuocVangLai_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                });
                            }


                        }
                    }
                    AdModule.controller('KetoancaController', KetoancaController);
                }
            }
        }
    }
}