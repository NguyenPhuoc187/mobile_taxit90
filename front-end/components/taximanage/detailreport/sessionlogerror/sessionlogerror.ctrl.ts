/**
 * Created by phamn on 31/10/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Sessionlogerror{
                    export interface ISessionlogerrorScope extends ng.IScope{
                        gridSessionlogerror: App.Shared.Directives.GridviewControl;
                        $parent: IDetailreportScope;
                    }
                    export class SessionlogerrorController{
                        public static $inject = ['$scope','language','global', '$http'];
                        constructor($scope: ISessionlogerrorScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService, $http: ng.IHttpService){
                            //note: cấu hình trang master
                            $scope.$parent.viewData = language.gridLg().viewData;
                            $scope.$parent.hideComboboxCar = false;
                            $scope.$parent.checkboxes_combobox = false;
                            $scope.$parent.showlocationandpoint = false;
                            $scope.$parent.hidecalendar = false;
                            $scope.$parent.hidesinglecalendar = true;
                            $scope.$parent.hideComboboxGroupCar = false;

                            $scope.gridSessionlogerror = new App.Shared.Directives.GridviewControl("gridSessionlogerror");
                            $scope.gridSessionlogerror.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridSessionlogerror.filename_excel="BÁO CÁO SỐ LƯỢNG CUỐC BẤT THƯỜNG";
                            $scope.gridSessionlogerror.columns = [];
                            $scope.gridSessionlogerror.height = window.innerHeight - 150;
                            $scope.gridSessionlogerror.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            $scope.gridSessionlogerror.isshowAggregates = true;
                            $scope.gridSessionlogerror.isAltRows=true;
                            $scope.gridSessionlogerror.isFilter=true;
                            $scope.gridSessionlogerror.isFilterMode= 'advanced';

                            $scope.gridSessionlogerror.columns.push({ text: 'STT', dataField: 'STT', type: 'number', align: 'center',width:'35px',pinned: true });
                            $scope.gridSessionlogerror.columns.push({ text: 'ID cuốc', dataField: 'SessionTaxiId', type: 'number', align: 'center',width:'70px',filterable:true,pinned: true});
                            $scope.gridSessionlogerror.columns.push({ text: 'Serial', dataField: 'Serial', type: 'string', align: 'center',width:'50px',hidden:true });
                            $scope.gridSessionlogerror.columns.push({ text: 'Số tài - Biển số', dataField: 'Display', type: 'string', align: 'center',width:'120px',filterable:true,pinned: true});
                            // $scope.gridSessionlogerror.columns.push({ text: 'Tài xế', dataField: 'TenTaiXe', type: 'string', align: 'center',width:'140px',filterable:true,columngroup:'thongtintaixe' });
                            // $scope.gridSessionlogerror.columns.push({ text: 'Đội xe', dataField: 'TenDoi', type: 'string', align: 'center',width:'80px',columngroup:'thongtintaixe'  });
                            // $scope.gridSessionlogerror.columns.push({ text: 'SĐT', dataField: 'DtTaiXe', type: 'string', align: 'center',width:'auto',filterable:true,columngroup:'thongtintaixe' });
                            // $scope.gridSessionlogerror.columns.push({ text: 'Số tài - Biển số', dataField: 'Display', type: 'number', align: 'center',width:'120px',filterable:true,columngroup:'thongtinxe' });
                            $scope.gridSessionlogerror.columns.push({ text: 'Địa điểm đón trả khách', dataField: 'Address', type: 'string', align: 'center',width:'250px'});
                            $scope.gridSessionlogerror.columns.push({ text: 'TG đón trả khách', dataField: 'TimeTotal', type: 'string', align: 'center',width:'170px' });
                            // $scope.gridSessionlogerror.columns.push({ text: 'Địa điểm đón trả khách', dataField: 'TimeTotal', type: 'string', align: 'center',width:'250px'});
                            $scope.gridSessionlogerror.columns.push({ text: 'Thời gian', dataField: 'BeginTime', type: 'string', align: 'center',width:'80px',hidden:true  });
                            $scope.gridSessionlogerror.columns.push({ text: 'Thời gian', dataField: 'EndTime', type: 'string', align: 'center',width:'80px',hidden:true  });
                            // $scope.gridSessionlogerror.columns.push({ text: 'Địa điểm', dataField: 'AdrressEnd', type: 'string', align: 'center',width:'220px',columngroup:'diemtra'  });

                            $scope.gridSessionlogerror.columns.push({ text: 'Km GPS', dataField: 'TongKmGps', type: 'float',cellsFormat: 'F1', align: 'center',width:'110px',filterable:true,
                                aggregates: ['sum'],
                            });
                            $scope.gridSessionlogerror.columns.push({ text: 'Km đồng hồ', dataField: 'TongKmDongHo',cellsFormat: 'F1', type: 'float', align: 'center',width:'110px',filterable:true,
                                aggregates: ['sum'],
                            });
                            $scope.gridSessionlogerror.columns.push({ text: 'Tổng tiền', dataField: 'TongTien', type: 'number',cellsFormat: 'n', align: 'center',width:'130px',filterable:true,
                                aggregates: ['sum'],

                            });
                            $scope.gridSessionlogerror.columns.push({ text: 'Tỉ lệ km(dh)/ km(gps)', dataField: 'TyLeKm', type: 'number', align: 'center',width:'auto',filterable:true,cellsFormat: 'p' });




                            $scope.$parent.btViewClick = () => {
                                if($scope.$parent.cpnSelectSettings !== undefined){
                                    if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                        //note: trường hợp chọn đội xe là tất cả
                                        if($scope.$parent.modelSelectSettings.itemSelect() == undefined  || $scope.$parent.modelSelectSettings.itemSelect().Serial == '-1'){
                                            GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                        }else{
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end(),$scope.$parent.modelSelectSettings.itemSelect().Serial);
                                        }
                                    }else{
                                        //note: trường hợp chọn đội xe không phải là tất cả
                                        if($scope.$parent.modelSelectSettings.itemSelect() == undefined  || $scope.$parent.modelSelectSettings.itemSelect().Serial == '-1'){
                                            GetDataWithGroup($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.groupdeviceSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                        }else{
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end(),$scope.$parent.modelSelectSettings.itemSelect().Serial)
                                        }
                                    }

                                }
                            };
                            //Xử lý sự kiện click đúp để hiển thị hành trình taxi
                            $scope.gridSessionlogerror.event.onDoubleClick = (row:any) =>{
                                window.open(`/hanh-trinh-taxi/${$scope.$parent.cpnSelectSettings.itemSelect().Id}/${row.Serial}/${ChangeTime(row.BeginTime)}/${ChangeTime(row.EndTime)}`, '_blank');
                            };


                            function GetDataWithCompany(companyId:number, begindate:string, enddate:string){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.get(global.ReportHost + `api/SessionTaxiLog/CuocBt?companyId=${companyId}&beginTime=${begindate}&endTime=${enddate}`).then((rep:any)=>{
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            if(rep.data.SessionTaxiLogList.length > 0){
                                                $scope.gridSessionlogerror.data = rep.data.SessionTaxiLogList;
                                                ConvertData($scope.gridSessionlogerror.data);
                                                $scope.gridSessionlogerror.refresh();
                                            }else{
                                                $scope.gridSessionlogerror.clear();
                                                $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                    header: 'Thông báo!',
                                                    theme: 'bg-info',
                                                    position: 'center'
                                                });
                                            }
                                        }else{
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            $scope.gridSessionlogerror.clear();
                                            $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                                header: 'Thông báo!',
                                                theme: 'bg-danger',
                                                position: 'center'
                                            });

                                        }
                                    }else{
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridSessionlogerror.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                    }

                                });
                            }
                            function GetDataWithGroup(companyId:number,groupId:number,beginTime:string,endTime:string){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.get(global.ReportHost + `api/SessionTaxiLog/CuocBtByGroup?companyId=${companyId}&group=${groupId}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            if(rep.data.SessionTaxiLogList.length > 0){
                                                $scope.gridSessionlogerror.data = rep.data.SessionTaxiLogList;
                                                ConvertData($scope.gridSessionlogerror.data);
                                                $scope.gridSessionlogerror.refresh();
                                            }else{
                                                $scope.gridSessionlogerror.clear();
                                                $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                    header: 'Thông báo!',
                                                    theme: 'bg-info',
                                                    position: 'center'
                                                });
                                            }
                                        }else{
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            $scope.gridSessionlogerror.clear();
                                            $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                                header: 'Thông báo!',
                                                theme: 'bg-danger',
                                                position: 'center'
                                            });

                                        }
                                    }else{
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridSessionlogerror.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                    }
                                });
                            }
                            function GetDataWithSerial(companyId:number,beginTime:string,endTime:string,serial:string){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.get(global.ReportHost + `api/SessionTaxiLog/CuocBtBySerial?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}&serial=${serial}`).then((rep:any)=>{
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            if(rep.data.SessionTaxiLogList.length > 0){
                                                $scope.gridSessionlogerror.data = rep.data.SessionTaxiLogList;
                                                ConvertData($scope.gridSessionlogerror.data);
                                                $scope.gridSessionlogerror.refresh();
                                            }else{
                                                $scope.gridSessionlogerror.clear();
                                                $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                    header: 'Thông báo!',
                                                    theme: 'bg-info',
                                                    position: 'center'
                                                });
                                            }
                                        }else{
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            $scope.gridSessionlogerror.clear();
                                            $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                                header: 'Thông báo!',
                                                theme: 'bg-danger',
                                                position: 'center'
                                            });

                                        }
                                    }else{
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridSessionlogerror.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                    }
                                });
                            }
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    if(val.BeginLocation !== null && val.EndLocation !== null){
                                        if(val.BeginLocation.Address !== null && val.EndLocation.Address !== null){
                                            val.Address = `<strong>Đón:</strong> ${val.BeginLocation.Address} </br> <strong>Trả : </strong> ${val.EndLocation.Address}`;
                                        }else{
                                            val.Address = '';
                                        }
                                    }
                                    val.TimeTotal = `<strong>Đón:</strong> ${global.AdapterDate(val.BeginTime)} </br> <strong>Trả :</strong> ${global.AdapterDate(val.EndTime)}`;
                                    // if(val.BeginLocation !== null){
                                    //     if(val.BeginLocation.Address !== null){
                                    //         val.AdrressStart = val.BeginLocation.Address;
                                    //     }else{
                                    //         val.AdrressStart = '';
                                    //     }
                                    // }
                                    // if(val.EndLocation !== null){
                                    //     if(val.EndLocation.Address !== null){
                                    //         val.AdrressEnd = val.EndLocation.Address;
                                    //     }else{
                                    //         val.AdrressEnd = '';
                                    //     }
                                    // }else{
                                    //     val.AdrressEnd = val.AdrressStart;
                                    // }
                                    val.TongKmGps = (val.TongKmGps/1000).toFixed(1);
                                    // val.KmRong = (val.KmRong/1000).toFixed(1);
                                    val.TongKmDongHo = (val.TongKmDongHo/1000).toFixed(1);
                                    val.BeginTime = global.AdapterDate(val.BeginTime);
                                    val.EndTime = global.AdapterDate(val.EndTime);
                                    val.Display = `<span class="text-danger">${val.SoHieu} - ${val.Bs}</span>`;
                                });

                            }

                            //chuyển thời gian để đưa qua hành trình
                            function ChangeTime(time:any){
                                var year:string = time.slice(15,19);
                                var month:string = time.slice(12,14);
                                var date:string = time.slice(9,11);

                                var hour:string = time.slice(0,8);

                                var tt:any = hour +' ' +month + '-' + date + '-' + year ;
                                return tt;
                            };
                        }
                    }
                    AdModule.controller('SessionlogerrorController', SessionlogerrorController);
                }
            }
        }
    }
}