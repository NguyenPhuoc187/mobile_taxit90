/**
 * Created by phamn on 26/10/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Zonetaxidetail{
                    export interface IZonetaxidetailScope extends ng.IScope{
                        gridDetailzonetaxi: App.Shared.Directives.GridviewControl;

                        companyId:any;
                        params:any;
                        beginday:string;
                        endday:string;
                        ZoneName:string;
                    }
                    export class ZonetaxidetailController{
                        public static $inject = ['$scope','$http', 'global','language','api','$location','$stateParams'];
                        constructor ($scope: IZonetaxidetailScope, public $http: ng.IHttpService, public global: App.Shared.GlobalService,public language:App.Shared.LanguageService,
                                     public api: App.Shared.Api.IApi,public $location: ng.ILocationService, public stateParams:ng.ui.IStateParamsService){
                            $scope.gridDetailzonetaxi = new App.Shared.Directives.GridviewControl("gridDetailzonetaxi");
                            $scope.gridDetailzonetaxi.filename_excel="BÁO CÁO CHI TIẾT SỐ LƯỢNG CUỐC XE TRONG VÙNG";
                            $scope.gridDetailzonetaxi.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridDetailzonetaxi.columns = [];
                            $scope.gridDetailzonetaxi.height = window.innerHeight - 150;
                            $scope.gridDetailzonetaxi.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            $scope.gridDetailzonetaxi.isAltRows=true;
                            $scope.gridDetailzonetaxi.isFilter=true;
                            $scope.gridDetailzonetaxi.isFilterMode= 'advanced';



                            $scope.gridDetailzonetaxi.columns.push({ text: 'STT', dataField: 'STT', type: 'number', align: 'center',width:'35px',pinned: true  });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'Serial', dataField: 'Serial', type: 'string', align: 'center',width:'50px',hidden:true });
                            // $scope.gridDetailzonetaxi.columns.push({ text: 'ID Cuốc', dataField: 'SessionTaxiId', type: 'number', align: 'center',width:'60px',filterable:true,pinned: true });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'ID Cuốc', dataField: 'SessionTaxiId', type: 'number', align: 'center',width:'42px',filterable:true,pinned: true });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'Số tài - Biển số', dataField: 'Display', type: 'string', align: 'center',width:'114px',filterable:true,pinned: true});
                            $scope.gridDetailzonetaxi.columns.push({ text: 'BeginTime', dataField: 'BeginTime', type: 'string', align: 'center',width:'150px',filterable:true,hidden:true });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'EndTime', dataField: 'EndTime', type: 'string', align: 'center',width:'150px',filterable:true,hidden:true });
                            // $scope.gridDetailzonetaxi.columns.push({ text: 'Đội xe', dataField: 'TenDoi', type: 'string', align: 'center',width:'auto',filterable:true,columngroup:'thongtinxe' });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'TG đón trả khách', dataField: 'TimeTotal', type: 'string', align: 'center',width:'164px'  });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'Địa điểm đón trả khách', dataField: 'Address', type: 'string', align: 'center',width:'237px'  });
                            // $scope.gridDetailzonetaxi.columns.push({ text: 'Thời gian', dataField: 'EndTime', type: 'string', align: 'center',width:'80px',columngroup:'diemtra'  });
                            // $scope.gridDetailzonetaxi.columns.push({ text: 'Địa điểm', dataField: 'AdrressEnd', type: 'string', align: 'center',width:'220px',columngroup:'diemtra'  });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'Thời gian chờ', dataField: 'ThoiGianCho', type: 'string', align: 'center',width:'63px',columngroup:'thongtincuoc',className:'gridtaxidetail_height40_2row' });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'Tổng tiền', dataField: 'TongTien', type: 'number', align: 'center',width:'107px',columngroup:'thongtincuoc',cellsFormat: 'n',
                                aggregates: ['sum'],
                            });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'Km rỗng', dataField: 'KmRong', type: 'float', align: 'center',width:'83px',columngroup:'thongtincuoc' ,cellsFormat: 'F1',
                                aggregates: ['sum'],
                            });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'Km GPS', dataField: 'TongKmGps', type: 'float', align: 'center',width:'79px',cellsFormat: 'F1',filterable:true,columngroup:'thongtincuoc',
                                aggregates: ['sum'],
                            });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'Km(Đồng hồ)', dataField: 'TongKmDongHo', type: 'float', align: 'center',width:'71px',filterable:true,columngroup:'thongtincuoc',className:'gridtaxidetail_height40_2row',cellsFormat: 'F1',
                                aggregates: ['sum'],
                            });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'Tỉ lệ km(dh)/km(gps)', dataField: 'TyLeKm_show', type: 'number', align: 'center',width:'104px',filterable:true,columngroup:'thongtincuoc', cellsFormat: 'p',className:'gridtaxidetail_height40_2row' });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'Cuốc 0km', dataField: 'CuocKhongKm_show', type: 'string', align: 'center',width:'44px',filterable:true,columngroup:'thongtincuoc',className:'gridtaxidetail_height40_2row' });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'Cuốc hợp đồng', dataField: 'IsCuocHopDong_show', type: 'string', align: 'center',width:'63px',filterable:true,columngroup:'thongtincuoc',className:'gridtaxidetail_height40_2row' });
                            $scope.gridDetailzonetaxi.columns.push({ text: 'Cuốc vãng lai', dataField: 'CuocVangLai_show', type: 'string', align: 'center',width:'auto',filterable:true,columngroup:'thongtincuoc',className:'gridtaxidetail_height40_2row' });
                            $scope.gridDetailzonetaxi.columnGroups.push({ text: 'Điểm đón khách', name: 'diemdon'});
                            $scope.gridDetailzonetaxi.columnGroups.push({ text: 'Điểm trả khách', name: 'diemtra'});
                            $scope.gridDetailzonetaxi.columnGroups.push({ text: 'Thông tin cuốc', name: 'thongtincuoc'});
                            $scope.gridDetailzonetaxi.columnGroups.push({ text: 'Thông tin xe', name: 'thongtinxe'});


                            $scope.params = {};
                            $scope.params  = stateParams;

                            $scope.beginday = ChangeTime($scope.params.param3);
                            $scope.endday = ChangeTime($scope.params.param4);
                            $scope.ZoneName = $scope.params.param6;
                            console.log($scope.params.param1);

                            if($scope.params !== undefined){
                                $http.get(global.ReportHost + `api/ZonePointReport/ZoneTaxiDetailReport?companyId=${$scope.params.param1}&beginTime=${$scope.params.param3}&endTime=${$scope.params.param4}&checkZoneId=${$scope.params.param2}&groupId=${$scope.params.param5}`).then((rep:any)=>{
                                    $scope.gridDetailzonetaxi.data = rep.data.SessionTaxiLogList;
                                    ConvertData($scope.gridDetailzonetaxi.data);
                                    $scope.gridDetailzonetaxi.refresh();
                                });
                            }else{
                                return;
                            }
                            // xem hành trình
                            $scope.gridDetailzonetaxi.event.onDoubleClick = (row:any) =>{
                                console.log(row);

                                window.open(`/hanh-trinh-taxi/${$scope.params.param1}/${row.Serial}/${ChangeTimehanhtrinh(row.BeginTime)}/${ChangeTimehanhtrinh(row.EndTime)}`, '_blank');
                            };

                            function ChangeTime(time:any){
                                var year:string = time.slice(12);
                                var month:string = time.slice(6,8);
                                var date:string = time.slice(9,11);

                                var hour:string = time.slice(0,5);

                                var tt:any = date + '-' + month + '-' + year ;
                                return tt;
                            }

                            //chuyển thời gian để đưa qua hành trình
                            function ChangeTimehanhtrinh(time:any){
                                var year:string = time.slice(0,4);
                                var month:string = time.slice(5,7);
                                var date:string = time.slice(8,10);
                                var min:string = time.slice(11,19);
                                var tt:any = min +' ' +month + '-' + date + '-' + year ;
                                return tt;
                            };

                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    // if(val.BeginLocation !== null){
                                    //     if(val.BeginLocation.Address !== null){
                                    //         val.AdrressStart = val.BeginLocation.Address;
                                    //     }else{
                                    //         val.AdrressStart = '';
                                    //     }
                                    // }
                                    // if(val.EndLocation !== null){
                                    //     if(val.EndLocation.Address !== null){
                                    //         val.AdrressEnd = val.EndLocation.Address;
                                    //     }else{
                                    //         val.AdrressEnd = '';
                                    //     }
                                    // }else{
                                    //     val.AdrressEnd = val.AdrressStart;
                                    // }
                                    if(val.BeginLocation !== null && val.EndLocation !== null){
                                        if(val.BeginLocation.Address !== null && val.EndLocation.Address !== null){
                                            val.Address = `<strong>Đón:</strong> ${val.BeginLocation.Address} </br> <strong>Trả : </strong> ${val.EndLocation.Address}`;
                                        }else{
                                            val.Address = '';
                                        }
                                    }
                                    val.TimeTotal = `<strong>Đón:</strong> ${global.AdapterDate(val.BeginTime)} </br> <strong>Trả :</strong> ${global.AdapterDate(val.EndTime)}`;
                                    // val.BeginTime = global.AdapterDate(val.BeginTime);
                                    // val.EndTime = global.AdapterDate(val.EndTime);
                                    if(val.CuocKhongKm){
                                        val.CuocKhongKm_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.CuocKhongKm_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(val.IsCuocHopDong){
                                        val.IsCuocHopDong_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.IsCuocHopDong_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(val.CuocVangLai){
                                        val.CuocVangLai_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.CuocVangLai_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    val.ThoiGianCho = val.ThoiGianCho.slice(0,5);
                                    val.Display = `<span class="text-danger">${val.SoHieu} - ${val.Bs}</span>`;
                                    val.TongKmGps = (val.TongKmGps/1000).toFixed(1);
                                    val.KmRong = (val.KmRong/1000).toFixed(1);
                                    val.TongKmDongHo = (val.TongKmDongHo/1000).toFixed(1);
                                });

                            }
                        }
                    }
                    AdModule.controller('ZonetaxidetailController',ZonetaxidetailController);
                }
            }
        }
    }
}