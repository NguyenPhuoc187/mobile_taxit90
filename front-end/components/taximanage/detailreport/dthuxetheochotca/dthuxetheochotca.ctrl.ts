/**
 * Created by NPPRO on 13/02/2017.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Dthuxetheochotca{
                    export interface IDthuxetheochotcaScope extends ng.IScope{
                        gridDthuxetheochotcadongho: App.Shared.Directives.GridviewControl;

                        companyId:any;
                        params:any;
                        beginday:string;

                        PointName:string;
                    }
                    export class DthuxetheochotcaController{
                        public static $inject = ['$scope','$http', 'global','language','api','$location','$stateParams'];
                        constructor ($scope: IDthuxetheochotcaScope, public $http: ng.IHttpService, public global: App.Shared.GlobalService,public language:App.Shared.LanguageService,
                                     public api: App.Shared.Api.IApi,public $location: ng.ILocationService, public stateParams:ng.ui.IStateParamsService){
                            $scope.gridDthuxetheochotcadongho = new App.Shared.Directives.GridviewControl("gridDthuxetheochotcadongho");
                            $scope.gridDthuxetheochotcadongho.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridDthuxetheochotcadongho.filename_excel="BÁO CÁO SỐ LƯỢNG CHỐT CA ĐỒNG HỒ";
                            $scope.gridDthuxetheochotcadongho.columns = [];
                            $scope.gridDthuxetheochotcadongho.height = window.innerHeight - 150;
                            $scope.gridDthuxetheochotcadongho.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            $scope.gridDthuxetheochotcadongho.isshowAggregates = true;
                            $scope.gridDthuxetheochotcadongho.isAltRows=true;
                            $scope.gridDthuxetheochotcadongho.isFilter=true;
                            $scope.gridDthuxetheochotcadongho.isFilterMode= 'advanced';

                            $scope.gridDthuxetheochotcadongho.columns.push({ text: 'STT', dataField: 'STT', type: 'string', align: 'center', width:'50px',pinned: true });
                            $scope.gridDthuxetheochotcadongho.columns.push({ text: 'Thời gian tính doanh thu', dataField: 'LastChotCa', type: 'string', width:'150px', align: 'center',filterable:true,pinned: true });
                            $scope.gridDthuxetheochotcadongho.columns.push({ text: 'Số tài - Biển số', dataField: 'Display', type: 'string', align: 'center',width:'106px',filterable:true, pinned: true });
                            $scope.gridDthuxetheochotcadongho.columns.push({ text: 'Đội xe', dataField: 'GroupName', type: 'string', align: 'center',width:'75px',filterable:true });
                            $scope.gridDthuxetheochotcadongho.columns.push({ text: 'Tên tài xế', dataField: 'DriverName', type: 'string', align: 'center',width:'100px',filterable:true,  });
                            $scope.gridDthuxetheochotcadongho.columns.push({ text: 'Tổng tiền', dataField: 'TongTien', type: 'number', width:'100px', align: 'center',cellsFormat: 'n',
                                aggregates: ['sum'],
                            });
                            $scope.gridDthuxetheochotcadongho.columns.push({ text: 'Tổng km trong ca', dataField: 'TongKm', type: 'float', align: 'center',width:'73px',filterable:true,cellsFormat: 'F1',className:'gridtaxidetail_height40_2row',
                                aggregates: ['sum'],
                            });
                            $scope.gridDthuxetheochotcadongho.columns.push({ text: 'Tổng km có khách', dataField: 'TongKmKhach', type: 'float', align: 'center',width:'73px',filterable:true,cellsFormat: 'F1',className:'gridtaxidetail_height40_2row',
                                aggregates: ['sum'],
                            });
                            $scope.gridDthuxetheochotcadongho.columns.push({ text: 'Mất nguồn', dataField: 'TongMatNguon', type: 'number', align: 'center',width:'62px',filterable:true,cellsFormat: 'n',className:'gridtaxidetail_height40_2row',
                                aggregates: ['sum'],
                            });
                            $scope.gridDthuxetheochotcadongho.columns.push({ text: 'Tổng cuốc', dataField: 'TongCuoc', type: 'number', align: 'center',width:'75px',filterable:true,cellsFormat: 'n',
                                aggregates: ['sum'],
                            });
                            $scope.gridDthuxetheochotcadongho.columns.push({ text: 'Số cuốc 0km', dataField: 'TongCuoc0Km', type: 'number', align: 'center',width:'65px',filterable:true,cellsFormat: 'n',className:'gridtaxidetail_height40_2row',
                                aggregates: ['sum'],
                            });
                            $scope.gridDthuxetheochotcadongho.columns.push({ text: 'Chích xung', dataField: 'ChichXung', type: 'number', align: 'center',width:'52px',filterable:true,cellsFormat: 'n',className:'gridtaxidetail_height40_2row',
                                aggregates: ['sum'],
                            });

                            $scope.gridDthuxetheochotcadongho.columns.push({ text: 'Tiền tích lũy cũ', dataField: 'TichLuyCu', type: 'number', align: 'center',width:'120px',filterable:true,cellsFormat: 'n'});
                            $scope.gridDthuxetheochotcadongho.columns.push({ text: 'Tiền tích lũy mới', dataField: 'TichLuyMoi', type: 'number', align: 'center',width:'auto',filterable:true, cellsFormat: 'n'});


                            $scope.params = {};
                            $scope.params  = stateParams;

                            $scope.beginday = ChangeTime($scope.params.param3);
                            // $scope.endday = ChangeTime($scope.params.param4);
                            $scope.PointName = '';
                            console.log($scope.params.param1);
                            function ChangeTime(time:any){
                                var year:string = time.slice(6);
                                var date:string = time.slice(3,5);
                                var month:string = time.slice(0,2);
                                var tt:any = date + '-' + month + '-' + year ;
                                return tt;
                            }
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    val.LastChotCa = `${global.AdapterDate(val.LastChotCa)} - ${global.AdapterDate(val.UpdateTime)}`;
                                    val.Display = `<span class="text-danger">${val.SoHieu} - ${val.Bs}</span>`;
                                    val.TongKm = (val.TongKm/1000).toFixed(1);
                                    val.TongKmKhach = (val.TongKmKhach/1000).toFixed(1);
                                    // val.TongCuoc0Km = (val.TongCuoc0Km/1000).toFixed(1);
                                });
                            }
                            if($scope.params !== undefined){
                                $http.get(global.ReportHost + `api/ChotCaTaxi/ChotCaLogLamda?companyId=${$scope.params.param1}&serial=${$scope.params.param2}&driverId=0&beginTime=${`00:00 ${$scope.params.param3}`}&endTime=${`23:59 ${$scope.params.param3}`}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridDthuxetheochotcadongho.data = rep.data.ChotCaLogTranfers;
                                        ConvertData($scope.gridDthuxetheochotcadongho.data);
                                        $scope.gridDthuxetheochotcadongho.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }else{
                                return;
                            }
                        }
                    }
                    AdModule.controller('DthuxetheochotcaController',DthuxetheochotcaController);
                }
            }
        }
    }
}