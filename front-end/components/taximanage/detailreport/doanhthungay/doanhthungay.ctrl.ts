/**
 * Created by NPPRO on 18/11/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Doanhthungay{
                    export interface IDoanhthungayScope extends ng.IScope{
                        gridDoanhthungay: App.Shared.Directives.GridNestTableControl;
                        $parent: IDetailreportScope;
                        nestabledata: any;
                    }
                    export class DoanhthungayController {
                        public static $inject = ['$scope', 'language', 'global', '$http'];

                        constructor($scope: IDoanhthungayScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService, $http: ng.IHttpService) {
                            //note: cấu hình trang master
                            $scope.$parent.viewData = language.gridLg().viewData;
                            $scope.$parent.checkboxes_combobox = true;
                            $scope.$parent.checkzonelocation = false;
                            $scope.$parent.showlocationandpoint = false;
                            $scope.$parent.hideComboboxCar = true;
                            $scope.$parent.hidecalendar = true;
                            $scope.$parent.hidesinglecalendar = false;
                            $scope.$parent.hideComboboxGroupCar = false;

                            // note: cấu hình trang child
                            $scope.gridDoanhthungay = new App.Shared.Directives.GridNestTableControl("gridDoanhthungay");
                            $scope.gridDoanhthungay.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridDoanhthungay.filename_excel = "BÁO CÁO DOANH THU NGÀY";
                            $scope.gridDoanhthungay.columns = [];
                            $scope.gridDoanhthungay.height = window.innerHeight - 200;
                            $scope.gridDoanhthungay.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            // $scope.gridDoanhthungay.isshowAggregates = true;
                            $scope.gridDoanhthungay.isAltRows = true;
                            // $scope.gridDoanhthungay.isFilter = true;
                            // $scope.gridDoanhthungay.isFilterMode = 'advanced';

                            $scope.nestabledata = [];

                            var nestedTables = new Array();
                            $scope.gridDoanhthungay.rowdetail = (id:number, row:any, element:any, rowinfo:any) => {
                                // element.append($("<div style='margin: 10px;'></div>"));
                                element.append($("<div style='margin: 10px;'></div>"));
                                var nestedGrid = $(element.children()[0]);
                                var filterGroup = new $.jqx.filter();
                                var filter_or_operator = 1;
                                var filterValue = id;
                                var filterCondition = 'equal';
                                var filter = filterGroup.createfilter('stringfilter', filterValue, filterCondition);
                                // fill the orders depending on the id.
                                var data_source:any = $scope.gridDoanhthungay.data[row.uid].TongHopNgays;
                                ConvertData(data_source);
                                var ordersSource:any = {
                                    dataFields: Array(),
                                    localdata: data_source
                                };
                                if (nestedGrid != null) {
                                    var nestedGridAdapter = new $.jqx.dataAdapter(ordersSource);
                                    nestedGrid.jqxDataTable({
                                        pageable: true,
                                        columnsResize: true,
                                        pagerMode: 'advanced',
                                        source: nestedGridAdapter,
                                        width: '95%',
                                        height: '180px',
                                        sortable: true,
                                        columnsHeight:40,
                                        columns: [
                                            { text: 'STT',
                                                dataField: 'STT',
                                                type: 'number',
                                                align: 'center',
                                                width: '50px' },

                                            {text: 'Loại xe',
                                                dataField: 'LoaiXe',
                                                type: 'string',
                                                width: '130px',
                                                align: 'center',
                                                filterable: true,
                                                cellsFormat: 'n',
                                                aggregates: ['sum'],
                                            },
                                            {
                                                text: 'Tổng số xe',
                                                dataField: 'TongSoXe',
                                                type: 'string',
                                                width: '150px',
                                                align: 'center',
                                                hidden: true,
                                                cellsFormat: 'n',
                                                aggregates: ['sum'],
                                            },
                                            {
                                                text: 'Số xe được kết toán ca',
                                                dataField: 'SoXeKetToan',
                                                type: 'number',
                                                align: 'center',
                                                width: '100px',
                                                filterable: true,
                                                cellsFormat: 'n',
                                                className:'gridtt09_2row',
                                                aggregates: ['sum'],
                                            },
                                            {
                                                text: 'Số cuốc',
                                                dataField: 'SoCuoc',
                                                type: 'number',
                                                align: 'center',
                                                width: '80px',
                                                filterable: true,
                                                cellsFormat: 'n',
                                                aggregates: ['sum'],
                                            },
                                            {
                                                text: 'Km vận doanh(m)',
                                                dataField: 'KmVanDoanh',
                                                type: 'number',
                                                align: 'center',
                                                width: '120px',
                                                filterable: true,
                                                cellsFormat: 'n',
                                                aggregates: ['sum'],
                                            },
                                            {
                                                text: 'Km có khách(m)',
                                                dataField: 'KmCoKhach',
                                                type: 'number',
                                                align: 'center',
                                                width: '120px',
                                                filterable: true,
                                                cellsFormat: 'n',
                                                aggregates: ['sum'],
                                            },
                                            {
                                                text: 'Tổng doanh thu',
                                                dataField: 'TongDoanhThu',
                                                type: 'number',
                                                align: 'center',
                                                width: '120px',
                                                filterable: true,
                                                cellsFormat: 'n',
                                                aggregates: ['sum'],
                                            },
                                            {
                                                text: 'Doanh thu sau thuế',
                                                dataField: 'DoanhThuSauThe',
                                                type: 'number',
                                                align: 'center',
                                                width: '140px',
                                                filterable: true,
                                                cellsFormat: 'n',
                                                aggregates: ['sum'],
                                            },
                                            {
                                                text: 'Doanh thu TX được chia',
                                                dataField: 'TxDuocChia',
                                                type: 'number',
                                                align: 'center',
                                                width: '170px',
                                                filterable: true,
                                                cellsFormat: 'n',
                                                aggregates: ['sum'],
                                            },
                                            {
                                                text: 'Thực thu (CTY)',
                                                dataField: 'ThucThu',
                                                type: 'number',
                                                align: 'center',
                                                width: '120px',
                                                filterable: true,
                                                cellsFormat: 'n',
                                                aggregates: ['sum'],
                                            },
                                            {
                                                text: 'BQ/XEKD',
                                                dataField: 'XeKd',
                                                type: 'number',
                                                align: 'center',
                                                width: '150px',
                                                filterable: true,
                                                cellsFormat: 'n',
                                                aggregates: ['sum'],
                                            }

                                        ]
                                    });
                                    // store the nested Data Tables and use the Employee ID as a key.
                                    nestedTables[id] = nestedGrid;

                                }

                            };

                            $scope.gridDoanhthungay.columns.push({
                                text: 'STT',
                                dataField: 'STT',
                                type: 'string',
                                align: 'center',
                                width: '100px'
                            });

                            $scope.gridDoanhthungay.columns.push({
                                text: 'Đội xe',
                                dataField: 'DoiXe',
                                type: 'string',
                                align: 'center',
                                width: 'auto'
                            });


                            $scope.$parent.btViewClick = () => {
                                $http.get(global.ReportHost +`api/KetToanCa/DoanhThuNgayByKetToan?companyId=${$scope.$parent.cpnSelectSettings.itemSelect().Id}&beginTime=00:00 ${global.ChangeSignDate_Server($scope.$parent.dtsignleDay.getdulieu)}&endTime=23:59 ${global.ChangeSignDate_Server($scope.$parent.dtsignleDay.getdulieu)}`).then((rep:any)=>{
                                   console.log(rep);
                                   $scope.gridDoanhthungay.data = rep.data.TongHopDoiList;
                                    ConvertData($scope.gridDoanhthungay.data);
                                    $scope.nestabledata = [];

                                   $scope.gridDoanhthungay.refresh();
                                    // let result: any = [];
                                    // $.each(rep.data.TongHopDoiList,(ind:number,val:any)=>{
                                    //     for(var i=0;i<val.TongHopNgays.length;i++){
                                    //         $scope.nestabledata.push(val.TongHopNgays[i]);
                                    //     }
                                    // });
                                    // ConvertData($scope.nestabledata);

                                });
                            };
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                });
                            }

                        }
                    }
                    AdModule.controller('DoanhthungayController', DoanhthungayController);
                }
            }
        }
    }
}