/**
 * Created by phamn on 01/11/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Customzone{
                    export interface ICustomzoneScope extends ng.IScope{
                        map: any;
                        $parent: IDetailreportScope;
                        areaManager:any;
                        gridCuocXeVungTuChon: App.Shared.Directives.GridviewControl;
                    }
                    export class CustomzoneController{
                        public static $inject = ['$scope','language','global', '$http','$q'];
                        constructor($scope: ICustomzoneScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService,
                                    $http: ng.IHttpService,public $q:ng.IQService){

                            $scope.gridCuocXeVungTuChon = new App.Shared.Directives.GridviewControl("gridCuocXeVungTuChon");

                            $scope.gridCuocXeVungTuChon.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột


                            /*Tắt tính năng xuất Excel*/
                            $scope.gridCuocXeVungTuChon.isExport = true;
                            $scope.gridCuocXeVungTuChon.height = window.innerHeight - 150;
                            $scope.gridCuocXeVungTuChon.columnsHeight = 40;
                            $scope.gridCuocXeVungTuChon.isAltRows=true;
                            $scope.gridCuocXeVungTuChon.isFilter=true;
                            $scope.gridCuocXeVungTuChon.isFilterMode= 'advanced';
                            $scope.gridCuocXeVungTuChon.columns.push({ text: 'Id vùng', dataField: 'ZoneId', type: 'string', width:'200px', align: 'center',hidden:true });
                            $scope.gridCuocXeVungTuChon.columns.push({ text: 'Số lượng', dataField: 'BeginCount', type: 'number', align: 'center',width:'100px',filterable:true,columngroup:'cuocbatdau',cellsFormat: 'n'  });
                            $scope.gridCuocXeVungTuChon.columns.push({ text: 'Tổng tiền', dataField: 'BeginTotalMoney', type: 'number', align: 'center',width:'150px',filterable:true,columngroup:'cuocbatdau',cellsFormat: 'n',
                                aggregates: [{
                                    'Total':
                                        function (aggregatedValue:any, currentValue:any, column:any, record:any) {
                                            var total = currentValue *1;
                                            return aggregatedValue + total;
                                        }
                                }],
                                aggregatesRenderer: function (aggregates:any, column:any, element:any) {
                                    var renderString = "<div style='margin: 4px; float: left;  height: 100%;'>";
                                    renderString += "<span>Tổng =  </span>" + ((aggregates.Total != undefined)? aggregates.Total:0) + "</div>";
                                    return renderString;
                                }
                            });
                            $scope.gridCuocXeVungTuChon.columns.push({ text: 'Tổng km', dataField: 'BeginTotalKm', type: 'number', align: 'center',width:'150px',filterable:true,columngroup:'cuocbatdau',cellsFormat: 'n',
                                aggregates: [{
                                    'Total':
                                        function (aggregatedValue:any, currentValue:any, column:any, record:any) {
                                            var total = currentValue *1;
                                            return aggregatedValue + total;
                                        }
                                }],
                                aggregatesRenderer: function (aggregates:any, column:any, element:any) {
                                    var renderString = "<div style='margin: 4px; float: left;  height: 100%;'>";
                                    renderString += "<span>Tổng = </span>" + ((aggregates.Total != undefined)? aggregates.Total:0)+ "</div>";
                                    return renderString;
                                }
                            });
                            $scope.gridCuocXeVungTuChon.columns.push({ text: 'Số lượng', dataField: 'EndCount', type: 'number', align: 'center',width:'100px',filterable:true,columngroup:'cuocketthuc',cellsFormat: 'n'  });
                            $scope.gridCuocXeVungTuChon.columns.push({ text: 'Tổng tiền', dataField: 'EndTotalMoney', type: 'number', align: 'center',width:'150px',filterable:true,columngroup:'cuocketthuc',cellsFormat: 'n',
                                aggregates: [{
                                    'Total':
                                        function (aggregatedValue:any, currentValue:any, column:any, record:any) {
                                            var total = currentValue *1;
                                            return aggregatedValue + total;
                                        }
                                }],
                                aggregatesRenderer: function (aggregates:any, column:any, element:any) {
                                    var renderString = "<div style='margin: 4px; float: left;  height: 100%;'>";
                                    renderString += "<span>Tổng = </span>" + ((aggregates.Total != undefined)? aggregates.Total:0) + "</div>";
                                    return renderString;
                                }

                            });
                            $scope.gridCuocXeVungTuChon.columns.push({ text: 'Tổng km', dataField: 'EndTotalKm', type: 'number', align: 'center',width:'auto',filterable:true,columngroup:'cuocketthuc',cellsFormat: 'n',
                                aggregates: [{
                                    'Total':
                                        function (aggregatedValue:any, currentValue:any, column:any, record:any) {
                                            var total = currentValue *1;
                                            return aggregatedValue + total;
                                        }
                                }],
                                aggregatesRenderer: function (aggregates:any, column:any, element:any) {
                                    var renderString = "<div style='margin: 4px; float: left;  height: 100%;'>";
                                    renderString += "<span>Tổng = </span>" + ((aggregates.Total != undefined)? aggregates.Total: 0) + "</div>";
                                    return renderString;
                                }
                            });

                            $scope.gridCuocXeVungTuChon.columnGroups.push({ text: 'Cuốc bắt đầu', name: 'cuocbatdau'});
                            $scope.gridCuocXeVungTuChon.columnGroups.push({ text: 'Cuốc kết thúc', name: 'cuocketthuc'});

                            $scope.$parent.viewData = 'Tạo vùng';
                            $scope.$parent.hideComboboxCar = true;
                            $scope.$parent.checkboxes_combobox = false;
                            $scope.$parent.showlocationandpoint = false;
                            $scope.$parent.hidecalendar = false;
                            $scope.$parent.hidesinglecalendar = true;
                            $scope.map = new google.maps.Map(document.getElementById("mapcustomzone"), {
                                center: new google.maps.LatLng(10.777256, 106.6053314),
                                scrollwheel: true,
                                zoom: 12,
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                disableDefaultUI: true,
                                mapTypeControl: true,
                                mapTypeControlOptions: {
                                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                                    position: google.maps.ControlPosition.RIGHT_TOP
                                },
                                backgroundColor: 'hsla(0, 0%, 0%, 0)',
                                panControl: true,
                                panControlOptions: {
                                    position: google.maps.ControlPosition.RIGHT_TOP
                                },
                                zoomControl: true,
                                streetViewControl: false,
                                zoomControlOptions: {
                                    style: google.maps.ZoomControlStyle.DEFAULT,
                                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                                }

                            });
                            var getdirection = /** @type {HTMLInputElement} */(
                                document.getElementById('get_direction'));
                            $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(getdirection);
                            // bảng hiển thị chi tiết đường đi
                            var right_panel = /** @type {HTMLInputElement} */(
                                document.getElementById('right-panel-hightwayzone'));
                            $scope.map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(right_panel);

                            var contextMenu:any = null;
                            var map_ready_idle = false;

                            /*sau khi map load xong thì chạy event này*/
                            google.maps.event.addListener($scope.map, 'idle', (mouseEvent:any)=> {
                                map_ready_idle = true;
                                // gọi hàm này dể hiển thị map đề phòng trường hơp map trắng phau ko có hình ảnh
                                google.maps.event.trigger($scope.map, 'resize');

                            });

                            google.maps.event.addListener($scope.map, 'bounds_changed', ()=> {
                                google.maps.event.trigger($scope.map, 'resize');
                                map_ready_idle = false;
                            });


                            //var oldTimeSelectCompany=0;/*quản lý thời gian chọn công ty ( ko được phép chọn quá nhanh)*/
                            var eventRightClickOnMapId:google.maps.MapsEventListener;
                            // /*Khởi tạo biến chứa tìm đường*/
                            // $scope.getdirectionManager = new App.Shared.Library.Getdirection($scope.map, $q);
                            //
                            //
                            $scope.areaManager = new App.Shared.Library.AreaCustomZoneManage($scope.map,$q,0,$http,global);
                            $scope.$parent.btViewClick = ()=>{
                                $scope.areaManager.updateParametter($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                $scope.areaManager.areaDraw();
                            };
                            $scope.areaManager.senddatazone = (item:any) => {
                                language.showLoading(false,"");
                                console.log(item);
                                //note: lấy theo công ty nếu đội xe chọn tất cả
                                if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                    $http.post(global.ReportHost + `api/ZonePointReport/CustomZoneTaxiReport?companyId=${$scope.$parent.cpnSelectSettings.itemSelect().Id}&beginTime=${$scope.$parent.dtTimeSelect.begin()}&endTime=${$scope.$parent.dtTimeSelect.end()}`,item).then((rep:any)=>{
                                        language.showLoading(true,"");
                                        $scope.gridCuocXeVungTuChon.data = rep.data.ZoneTaxiReports;
                                        // $scope.gridCuocXeVungTuChon.refresh();

                                        $('#myModal12').modal('show');
                                    });
                                }else {
                                    //note: lấy theo đội xe
                                    $http.post(global.ReportHost + `/api/ZonePointReport/CustomZoneTaxiByGroup?companyId=${$scope.$parent.cpnSelectSettings.itemSelect().Id}&group=${$scope.$parent.groupdeviceSelectSettings.itemSelect().Id}&beginTime=${$scope.$parent.dtTimeSelect.begin()}&endTime=${$scope.$parent.dtTimeSelect.end()}`,item).then((rep:any)=>{
                                        language.showLoading(true,"");
                                        $scope.gridCuocXeVungTuChon.data = rep.data.ZoneTaxiReports;
                                        // $scope.gridCuocXeVungTuChon.refresh();
                                        $('#myModal12').modal('show');
                                    });
                                }

                            };
                            //note: trigger sự kiện hiển thị modal refesh lại để ko bị lỗi mất grid-view
                            $('#myModal12').on('shown.bs.modal', function (event:any) {
                                $scope.gridCuocXeVungTuChon.refresh();
                            })


                        }
                    }
                    AdModule.controller('CustomzoneController',CustomzoneController);
                }
            }
        }
    }
}