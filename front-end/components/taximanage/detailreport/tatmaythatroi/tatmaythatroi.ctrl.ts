/**
 * Created by phamn on 11/11/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Tatmaythatroi{
                    export interface ITatmaythatroiScope extends ng.IScope{
                        gridTatmaythatroi: App.Shared.Directives.GridviewControl;
                        $parent: IDetailreportScope;
                    }
                    export class TatmaythatroiController{
                        public static $inject = ['$scope','language','global', '$http'];
                        constructor($scope: ITatmaythatroiScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService, $http: ng.IHttpService){
                            $scope.$parent.viewData = language.gridLg().viewData;
                            $scope.$parent.hideComboboxCar = false;
                            $scope.$parent.checkboxes_combobox = false;
                            $scope.$parent.showlocationandpoint = false;
                            $scope.$parent.hidecalendar = false;
                            $scope.$parent.hidesinglecalendar = true;
                            $scope.$parent.hideComboboxGroupCar = false;


                            $scope.gridTatmaythatroi = new App.Shared.Directives.GridviewControl("gridTatmaythatroi");
                            $scope.gridTatmaythatroi.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridTatmaythatroi.filename_excel="BÁO CÁO SỐ LƯỢNG TẮT MÁY THẢ TRÔI";
                            $scope.gridTatmaythatroi.columns = [];
                            $scope.gridTatmaythatroi.height = window.innerHeight - 150;
                            $scope.gridTatmaythatroi.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            // $scope.gridTatmaythatroi.isshowAggregates = true;
                            $scope.gridTatmaythatroi.isAltRows=true;
                            $scope.gridTatmaythatroi.isFilter=true;
                            $scope.gridTatmaythatroi.isFilterMode= 'advanced';

                            $scope.gridTatmaythatroi.columns.push({ text: 'STT', dataField: 'STT', type: 'number', align: 'center',width:'35px',pinned: true });
                            $scope.gridTatmaythatroi.columns.push({ text: 'Số tài - Biển số', dataField: 'Display', type: 'string', align: 'center',width:'130px',filterable:true,pinned: true  });
                            // $scope.gridTatmaythatroi.columns.push({ text: 'Số tài', dataField: 'SoTai', type: 'string', align: 'center',width:'60px',filterable:true });
                            $scope.gridTatmaythatroi.columns.push({ text: 'Đội xe', dataField: 'GroupName', type: 'string', align: 'center',width:'80px', });
                            $scope.gridTatmaythatroi.columns.push({ text: 'Tên tài xế', dataField: 'DriverName', type: 'string', align: 'center',width:'220px' });
                            $scope.gridTatmaythatroi.columns.push({ text: 'Cuốc xe', dataField: 'CuocXeId', type: 'number', align: 'center',width:'80px',});
                            $scope.gridTatmaythatroi.columns.push({ text: 'Thời gian', dataField: 'BeginTime', type: 'date', align: 'center',width:'120px',columngroup:'diemdon',cellsFormat: 'dd/MM/yyyy' });
                            $scope.gridTatmaythatroi.columns.push({ text: 'Địa điểm', dataField: 'AdrressStart', type: 'string', align: 'center',width:'130px',columngroup:'diemdon',});
                            $scope.gridTatmaythatroi.columns.push({ text: 'Thời gian', dataField: 'EndTime', type: 'date', align: 'center',width:'100px',columngroup:'diemtra',cellsFormat: 'dd/MM/yyyy'});
                            $scope.gridTatmaythatroi.columns.push({ text: 'Địa điểm', dataField: 'AdrressEnd', type: 'string', align: 'center',width:'auto',columngroup:'diemtra',});
                            $scope.gridTatmaythatroi.columnGroups.push({ text: 'Điểm bắt đâu', name: 'diemdon'});
                            $scope.gridTatmaythatroi.columnGroups.push({ text: 'Điểm kết thúc', name: 'diemtra'});

                            $scope.$parent.btViewClick = () => {
                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined){
                                    if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                        //note: trường hợp chọn đội xe là tất cả
                                        if($scope.$parent.modelSelectSettings.itemSelect() == undefined  || $scope.$parent.modelSelectSettings.itemSelect().Serial == '-1'){
                                            GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                        }else{
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end(),$scope.$parent.modelSelectSettings.valueSelects());
                                        }
                                    }else{
                                        //note: trường hợp chọn đội xe không phải là tất cả
                                        if($scope.$parent.modelSelectSettings.itemSelect() == undefined  || $scope.$parent.modelSelectSettings.itemSelect().Serial == '-1'){
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end(),$scope.$parent.modelSelectSettings.valueSelects());
                                        }else{
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end(),[$scope.$parent.modelSelectSettings.itemSelect().Serial])
                                        }
                                    }
                                }
                            };

                            function GetDataWithCompany(companyId:number, begindate:string, enddate:string){
                                $http.get(global.ReportHost + `api/Utils/ThaTroiXe?companyId=${companyId}&beginTime=${begindate}&endTime=${enddate}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridTatmaythatroi.data = rep.data.ThaTroiXeTranfers;
                                        ConvertData($scope.gridTatmaythatroi.data);
                                        $scope.gridTatmaythatroi.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }
                            function GetDataWithSerial(companyId:number,beginTime:string,endTime:string,serial:Array<string>){
                                $http.post(global.ReportHost + `api/Utils/ThaTroiXeBySerial?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`,serial).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridTatmaythatroi.data = rep.data.ThaTroiXeTranfers;
                                        ConvertData($scope.gridTatmaythatroi.data);
                                        $scope.gridTatmaythatroi.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    if(val.BeginLocation !== null){
                                        if(val.BeginLocation.Address !== null){
                                            val.AdrressStart = val.BeginLocation.Address;
                                        }else{
                                            val.AdrressStart = '';
                                        }
                                    }
                                    if(val.EndLocation !== null){
                                        if(val.EndLocation.Address !== null){
                                            val.AdrressEnd = val.EndLocation.Address;
                                        }else{
                                            val.AdrressEnd = '';
                                        }
                                    }else{
                                        val.AdrressEnd = val.AdrressStart;
                                    }
                                    // val.BeginTime = global.AdapterDate(val.BeginTime);
                                    // val.EndTime = global.AdapterDate(val.EndTime);
                                    val.Display = `<span class="text-danger">${val.SoHieu} - ${val.Bs}</span>`;
                                });

                            }

                        }
                    }
                    AdModule.controller('TatmaythatroiController', TatmaythatroiController);
                }
            }
        }
    }
}