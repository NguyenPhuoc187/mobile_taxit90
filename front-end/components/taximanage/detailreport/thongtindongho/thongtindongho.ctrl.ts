/**
 * Created by phamn on 12/11/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Thongtindongho{
                    export interface IThongtindonghoScope extends ng.IScope{
                        gridThongtindongho: App.Shared.Directives.GridviewControl;
                        $parent: IDetailreportScope;
                    }
                    export class ThongtindonghoController{
                        public static $inject = ['$scope','language','global', '$http'];
                        constructor($scope: IThongtindonghoScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService, $http: ng.IHttpService){
                            $scope.$parent.viewData = language.gridLg().viewData;
                            $scope.$parent.hideComboboxCar = false;
                            $scope.$parent.checkboxes_combobox = false;
                            $scope.$parent.showlocationandpoint = false;
                            $scope.$parent.hidecalendar = true;
                            $scope.$parent.hidesinglecalendar = true;
                            $scope.$parent.hideComboboxGroupCar = false;


                            $scope.gridThongtindongho = new App.Shared.Directives.GridviewControl("gridThongtindongho");
                            $scope.gridThongtindongho.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridThongtindongho.filename_excel="BÁO CÁO THÔNG TIN ĐỒNG HỒ";
                            $scope.gridThongtindongho.columns = [];
                            $scope.gridThongtindongho.height = window.innerHeight - 150;
                            $scope.gridThongtindongho.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            $scope.gridThongtindongho.isshowAggregates = true;
                            $scope.gridThongtindongho.isAltRows=true;
                            $scope.gridThongtindongho.isFilter=true;
                            $scope.gridThongtindongho.isFilterMode= 'simple';
                            $scope.gridThongtindongho.isExport= true;

                            $scope.gridThongtindongho.columns.push({ text: 'STT', dataField: 'STT', type: 'number', align: 'center',width:'35px',pinned: true });
                            $scope.gridThongtindongho.columns.push({ text: 'Số tài - Biển số', dataField: 'Display', type: 'string', align: 'center',width:'110px', filterable:true });
                            // $scope.gridThongtindongho.columns.push({ text: 'Id', dataField: 'SoHieu', type: 'string', align: 'center',width:'100px',filterable:true });
                            $scope.gridThongtindongho.columns.push({ text: 'Cuốc 0km', dataField: 'TotalSession0Km', type: 'number', align: 'center',width:'60px',cellsFormat: 'n',filterable:true,
                                aggregates: ['sum'],
                            });
                            $scope.gridThongtindongho.columns.push({ text: 'Tắt nguồn', dataField: 'TotalMissPower', type: 'number', align: 'center',width:'50px',cellsFormat: 'n',filterable:true,
                                aggregates: ['sum'],
                            });
                            // $scope.gridThongtindongho.columns.push({ text: 'Chích xung', dataField: 'TotalInvalidClockSignal', type: 'number', align: 'center',width:'50px',filterable:true,cellsFormat: 'n',
                            //     aggregates: ['sum'],
                            // });
                            $scope.gridThongtindongho.columns.push({ text: 'Tổng cuốc', dataField: 'TotalSession', type: 'number', align: 'center',width:'90px',cellsFormat: 'n',filterable:true,columngroup:'soliecakinhdoanh',
                                aggregates: ['sum'],
                            });
                            $scope.gridThongtindongho.columns.push({ text: 'Tổng km', dataField: 'TotalKm', type: 'float', align: 'center',width:'90px',cellsFormat: 'F1',columngroup:'soliecakinhdoanh',
                                aggregates: ['sum'],
                            });
                            $scope.gridThongtindongho.columns.push({ text: 'Tổng km có khách', dataField: 'TotalKmUse', type: 'float', align: 'center',width:'90px',cellsFormat: 'F1',columngroup:'soliecakinhdoanh',
                                aggregates: ['sum'],
                            });
                            $scope.gridThongtindongho.columns.push({ text: 'Tổng tiền', dataField: 'TotalMoney', type: 'number', align: 'center',width:'90px',cellsFormat: 'n',columngroup:'soliecakinhdoanh',
                                aggregates: ['sum'],
                            });

                            $scope.gridThongtindongho.columns.push({ text: 'Tổng cuốc', dataField: 'TichLuyCuoc', type: 'number', align: 'center',width:'60px',columngroup:'solieutichluy',cellsFormat: 'n',
                                aggregates: ['sum'],
                            });
                            $scope.gridThongtindongho.columns.push({ text: 'Tổng km', dataField: 'TichLuyKm', type: 'float', align: 'center',width:'100px',columngroup:'solieutichluy',cellsFormat: 'F1',
                                aggregates: ['sum'],
                            });
                            $scope.gridThongtindongho.columns.push({ text: 'Tổng km có khách', dataField: 'TichLuyKmCoKhach', type: 'float', align: 'center',width:'120px',columngroup:'solieutichluy',cellsFormat: 'F1',
                                aggregates: ['sum'],
                            });
                            $scope.gridThongtindongho.columns.push({ text: 'Tổng tiền', dataField: 'TichLuyTien', type: 'number', align: 'center',width:'200px',columngroup:'solieutichluy',cellsFormat: 'n',
                                aggregates: ['sum'],
                            });
                            $scope.gridThongtindongho.columnGroups.push({ text: 'Số liệu tích lũy', name: 'solieutichluy'});
                            $scope.gridThongtindongho.columnGroups.push({ text: 'Số liệu ca đang kinh doanh', name: 'soliecakinhdoanh'});


                            $scope.$parent.btViewClick = () => {
                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined){
                                    if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                        //note: trường hợp chọn đội xe là tất cả
                                        if($scope.$parent.modelSelectSettings.itemSelect() == undefined  || $scope.$parent.modelSelectSettings.itemSelect().Serial == '-1'){
                                            GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id);
                                        }else{
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,[$scope.$parent.modelSelectSettings.itemSelect().Serial]);
                                        }
                                    }else{
                                        //note: trường hợp chọn đội xe không phải là tất cả
                                        if($scope.$parent.modelSelectSettings.itemSelect() == undefined  || $scope.$parent.modelSelectSettings.itemSelect().Serial == '-1'){
                                            GetDataWithGroup($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.groupdeviceSelectSettings.itemSelect().Id);
                                        }else{
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,[$scope.$parent.modelSelectSettings.itemSelect().Serial])
                                        }
                                    }
                                }
                            };

                            function GetDataWithCompany(companyId:number){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.get(global.ReportHost + `api/TrangThaiTaxi/StatusDongHo?companyId=${companyId}`).then((rep:any)=>{
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            if(rep.data.StatusDonghoTranfers.length > 0){
                                                $scope.gridThongtindongho.data = rep.data.StatusDonghoTranfers;
                                                ConvertData($scope.gridThongtindongho.data);
                                                $scope.gridThongtindongho.refresh();
                                            }else{
                                                $scope.gridThongtindongho.clear();
                                                $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                    header: 'Thông báo!',
                                                    theme: 'bg-info',
                                                    position: 'center'
                                                });
                                            }
                                        }else{
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            $scope.gridCuochongngoai.clear();
                                            $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                                header: 'Thông báo!',
                                                theme: 'bg-danger',
                                                position: 'center'
                                            });

                                        }
                                    }else{
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridCuochongngoai.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                    }

                                });
                            }
                            //todo: lỗi 500 server
                            function GetDataWithGroup(companyId:number, groupId:number){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.get(global.ReportHost + `api/TrangThaiTaxi/StatusDongHoByGroup?companyId=${companyId}&groupId=${groupId}`).then((rep:any)=>{
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            if(rep.data.StatusDonghoTranfers.length > 0){
                                                $scope.gridThongtindongho.data = rep.data.StatusDonghoTranfers;
                                                ConvertData($scope.gridThongtindongho.data);
                                                $scope.gridThongtindongho.refresh();
                                            }else{
                                                $scope.gridThongtindongho.clear();
                                                $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                    header: 'Thông báo!',
                                                    theme: 'bg-info',
                                                    position: 'center'
                                                });
                                            }
                                        }else{
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            $scope.gridCuochongngoai.clear();
                                            $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                                header: 'Thông báo!',
                                                theme: 'bg-danger',
                                                position: 'center'
                                            });

                                        }
                                    }else{
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridCuochongngoai.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                    }
                                });
                            }
                            function GetDataWithSerial(companyId:number,serial:Array<string>){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.post(global.ReportHost + `api/TrangThaiTaxi/StatusDongHoBySerial?companyId=${companyId}`,serial).then((rep:any)=>{
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            if(rep.data.StatusDonghoTranfers.length > 0){
                                                $scope.gridThongtindongho.data = rep.data.StatusDonghoTranfers;
                                                ConvertData($scope.gridThongtindongho.data);
                                                $scope.gridThongtindongho.refresh();
                                            }else{
                                                $scope.gridThongtindongho.clear();
                                                $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                    header: 'Thông báo!',
                                                    theme: 'bg-info',
                                                    position: 'center'
                                                });
                                            }
                                        }else{
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            $scope.gridCuochongngoai.clear();
                                            $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                                header: 'Thông báo!',
                                                theme: 'bg-danger',
                                                position: 'center'
                                            });

                                        }
                                    }else{
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridCuochongngoai.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                    }
                                });
                            }
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    val.Display = `<span class="text-danger">${val.SoHieu} - ${val.Bs}</span>`;

                                    val.TotalKm = (val.TotalKm/1000).toFixed(1);
                                    val.TichLuyKm = (val.TichLuyKm/1000).toFixed(1);
                                    val.TotalKmUse = (val.TotalKmUse/1000).toFixed(1);
                                    val.TichLuyKmCoKhach = (val.TichLuyKmCoKhach/1000).toFixed(1);
                                    // val.TichLuyKm = (val.TichLuyKm/1000).toFixed(1);
                                });

                            }

                        }
                    }
                    AdModule.controller('ThongtindonghoController', ThongtindonghoController);
                }
            }
        }
    }
}