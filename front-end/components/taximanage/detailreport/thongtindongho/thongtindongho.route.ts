module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export class ThongtindonghoModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper,global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'detailreport.thongtindongho',
                            config: {
                                controller: Taximanage.Detailreport.Thongtindongho.ThongtindonghoController,
                                templateUrl: '/components/taximanage/detailreport/thongtindongho/thongtindongho.html'+`?v=${global.Version}`,
                                url: '/thong-tin-dong-ho',
                                tag:{
                                    title:'Thông tin đồng hồ',
                                    icon:'icon-file-presentation',
                                    groupId:'taxi',
                                    index:3
                                }
                            }
                        });
                    }
                }
                AdModule.run(ThongtindonghoModel);
            }
        }
    }
}