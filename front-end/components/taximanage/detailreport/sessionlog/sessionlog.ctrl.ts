/**
 * Created by phamn on 31/10/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Sessionlog{
                    export interface ISessionlogScope extends ng.IScope{
                        gridSessionlog: App.Shared.Directives.GridviewControl;

                        $parent: IDetailreportScope;
                    }
                    export class SessionlogController{
                        public static $inject = ['$scope','language','global', '$http'];
                        constructor($scope: ISessionlogScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService, $http: ng.IHttpService){
                            window.onresize = (event)=> {
                                $scope.gridSessionlog.height = window.innerHeight - 100;
                            };
                            $scope.$parent.viewData = language.gridLg().viewData;
                            $scope.$parent.hideComboboxCar = false;
                            $scope.$parent.checkboxes_combobox = false;
                            $scope.$parent.showlocationandpoint = false;
                            $scope.$parent.hidecalendar = false;
                            $scope.$parent.hidesinglecalendar = true;
                            $scope.$parent.hideComboboxGroupCar = false;

                            $scope.gridSessionlog = new App.Shared.Directives.GridviewControl("gridSessionlog");
                            $scope.gridSessionlog.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridSessionlog.filename_excel="BÁO CÁO SỐ LƯỢNG CUỐC XE";
                            $scope.gridSessionlog.columns = [];
                            $scope.gridSessionlog.height = window.innerHeight - 100;
                            $scope.gridSessionlog.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            $scope.gridSessionlog.isshowAggregates = true;
                            $scope.gridSessionlog.isAltRows=true;
                            $scope.gridSessionlog.isFilter=true;
                            $scope.gridSessionlog.isFilterMode= 'simple';
                            $scope.gridSessionlog.isExport= true;


                            // $scope.gridSessionlog.isFilterMode= 'advanced';

                            var cellClass:any = (row:any, dataField:any, cellText:any, rowData:any)=> {
                                var cellValue:any = rowData[dataField];

                                // var dateString:string = rowData['TyLeKm_show']; // Oct 23
                                //
                                // var dateParts:any = dateString.split("/");
                                // //định dạng format chuỗi 'dd/mm/yyyy' thành object ngày
                                // var dateObject:any = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                                var cellValueEndtime:any = rowData['TyLeKm_show'];

                                switch (dataField) {
                                    case "TyLeKm_show":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;

                                    case "STT":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;

                                    case "SessionTaxiId":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "Display":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "TenTaiXe":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "TenDoi":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "BeginTime":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "AdrressStart":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "EndTime":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "AdrressEnd":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "ThoiGianCho":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "TongTien":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "KmRong":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "TongKmGps":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "TongKmDongHo":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "CuocKhongKm_show":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "IsCuocHopDong_show":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                    case "CuocVangLai_show":
                                        if (cellValueEndtime <  50) {
                                            return "organce";
                                        }
                                        break;
                                }

                            };

                            $scope.gridSessionlog.columns.push({ text: 'STT', dataField: 'STT', type: 'number', align: 'center',width:'35px',cellClassName: cellClass,pinned: true  });
                            $scope.gridSessionlog.columns.push({ text: 'Serial', dataField: 'Serial', type: 'string', align: 'center',width:'50px',hidden:true,cellClassName: cellClass });
                            // $scope.gridSessionlog.columns.push({ text: 'ID Cuốc', dataField: 'SessionTaxiId', type: 'number', align: 'center',width:'60px',filterable:true,pinned: true });
                            $scope.gridSessionlog.columns.push({ text: 'ID Cuốc', dataField: 'SessionTaxiId', type: 'number', align: 'center',width:'42px',cellClassName: cellClass});
                            $scope.gridSessionlog.columns.push({ text: 'Số tài - Biển số', dataField: 'Display', type: 'string', align: 'center',width:'100px',filterable:true,cellClassName: cellClass});
                            // $scope.gridSessionlog.columns.push({ text: 'Tài xế', dataField: 'TenTaiXe', type: 'string', align: 'center',width:'150px',filterable:true,columngroup:'thongtinxe',cellClassName: cellClass });
                            // $scope.gridSessionlog.columns.push({ text: 'Đội xe', dataField: 'TenDoi', type: 'string', align: 'center',width:'auto',filterable:true,columngroup:'thongtinxe',cellClassName: cellClass });
                            $scope.gridSessionlog.columns.push({ text: 'TG đón trả khách', dataField: 'TimeTotal', type: 'string', align: 'center',width:'164px',cellClassName: cellClass,filterable:true  });
                            // $scope.gridSessionlog.columns.push({ text: 'Địa điểm đón trả khách', dataField: 'Address', type: 'string', align: 'center',width:'237px',cellClassName: cellClass,filterable:true  });
                            // $scope.gridSessionlog.columns.push({ text: 'Vùng đón trả khách', dataField: 'VungAddress', type: 'string', align: 'center',width:'120px',cellClassName: cellClass,filterable:true  });
                            // $scope.gridSessionlog.columns.push({ text: 'Điểm đón trả khách', dataField: 'DiemAddress', type: 'string', align: 'center',width:'120px',cellClassName: cellClass,filterable:true  });
                            $scope.gridSessionlog.columns.push({ text: 'Thời gian', dataField: 'EndTime', type: 'string', align: 'center',width:'80px',cellClassName: cellClass , hidden:true });
                            $scope.gridSessionlog.columns.push({ text: 'Thời gian', dataField: 'BeginTime', type: 'string', align: 'center',width:'80px',cellClassName: cellClass,hidden:true  });
                            // $scope.gridSessionlog.columns.push({ text: 'Địa điểm', dataField: 'AdrressEnd', type: 'string', align: 'center',width:'220px',columngroup:'diemtra',cellClassName: cellClass  });
                            $scope.gridSessionlog.columns.push({ text: 'Thời gian chờ', dataField: 'ThoiGianCho', type: 'string', align: 'center',width:'63px',columngroup:'thongtincuoc',className:'gridtaxidetail_height40_2row',cellClassName: cellClass,filterable:true });
                            $scope.gridSessionlog.columns.push({ text: 'Tổng tiền', dataField: 'TongTien', type: 'number', align: 'center',width:'107px',columngroup:'thongtincuoc',cellsFormat: 'n',cellClassName: cellClass,filterable:true,
                                aggregates: ['sum'],
                            });
                            $scope.gridSessionlog.columns.push({ text: 'Km rỗng', dataField: 'KmRong', type: 'float', align: 'center',width:'83px',columngroup:'thongtincuoc' ,cellsFormat: 'F1',cellClassName: cellClass,filterable:true,
                                aggregates: ['sum'],
                            });
                            $scope.gridSessionlog.columns.push({ text: 'Km GPS', dataField: 'TongKmGps', type: 'float', align: 'center',width:'79px',cellsFormat: 'F1',filterable:true,columngroup:'thongtincuoc',cellClassName: cellClass,
                                aggregates: ['sum'],
                            });
                            $scope.gridSessionlog.columns.push({ text: 'Km(Đồng hồ)', dataField: 'TongKmDongHo', type: 'float', align: 'center',width:'71px',filterable:true,columngroup:'thongtincuoc',className:'gridtaxidetail_height40_2row',cellsFormat: 'F1',cellClassName: cellClass,
                                aggregates: ['sum'],
                            });
                            // $scope.gridSessionlog.columns.push({ text: 'Tỉ lệ km(dh)/km(gps)', dataField: 'TyLeKm_show', type: 'number', align: 'center',width:'104px',filterable:true,columngroup:'thongtincuoc', cellsFormat: 'p',className:'gridtaxidetail_height40_2row',cellClassName: cellClass });
                            $scope.gridSessionlog.columns.push({ text: 'Cuốc 0km', dataField: 'CuocKhongKm_show', type: 'string', align: 'center',width:'44px',filterable:true,columngroup:'thongtincuoc',cellClassName: cellClass,className:'gridtaxidetail_height40_2row' });
                            // $scope.gridSessionlog.columns.push({ text: 'Cuốc hợp đồng', dataField: 'IsCuocHopDong_show', type: 'string', align: 'center',width:'63px',filterable:true,columngroup:'thongtincuoc',className:'gridtaxidetail_height40_2row',cellClassName: cellClass });
                            // $scope.gridSessionlog.columns.push({ text: 'Cuốc vãng lai', dataField: 'CuocVangLai_show', type: 'string', align: 'center',width:'auto',filterable:true,columngroup:'thongtincuoc',className:'gridtaxidetail_height40_2row',cellClassName: cellClass });
                            $scope.gridSessionlog.columnGroups.push({ text: 'Điểm đón khách', name: 'diemdon'});
                            $scope.gridSessionlog.columnGroups.push({ text: 'Điểm trả khách', name: 'diemtra'});
                            $scope.gridSessionlog.columnGroups.push({ text: 'Thông tin cuốc', name: 'thongtincuoc'});
                            $scope.gridSessionlog.columnGroups.push({ text: 'Thông tin xe', name: 'thongtinxe'});

                            $scope.$parent.btViewClick = () => {
                                var starttime_xphut  = `${$scope.$parent.dtTimeStartSelect.GetTimeClockDefault()} ${$scope.$parent.dtTimeStartSelect.GetPickerDate_upserver()}`;
                                var endtime = `${$scope.$parent.dtTimeEndSelect.GetTimeClockDefault()} ${$scope.$parent.dtTimeEndSelect.GetPickerDate_upserver()}`;
                                if($scope.$parent.cpnSelectSettings !== undefined){
                                    if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                        //note: trường hợp chọn đội xe là tất cả
                                        if($scope.$parent.modelSelectSettings.itemSelect() == undefined  || $scope.$parent.modelSelectSettings.itemSelect().Serial == '-1'){
                                            GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id,starttime_xphut,endtime);
                                        }else{
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,starttime_xphut,endtime,$scope.$parent.modelSelectSettings.itemSelect().Serial)
                                        }
                                    }else{
                                        //note: trường hợp chọn đội xe không phải là tất cả
                                        if($scope.$parent.modelSelectSettings.itemSelect() == undefined  || $scope.$parent.modelSelectSettings.itemSelect().Serial == '-1'){
                                            GetDataWithGroup($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.groupdeviceSelectSettings.itemSelect().Id,starttime_xphut,endtime);
                                        }else{
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,starttime_xphut,endtime,$scope.$parent.modelSelectSettings.itemSelect().Serial)
                                        }
                                    }

                                }
                            };

                            //Xử lý sự kiện click đúp để hiển thị hành trình taxi
                            $scope.gridSessionlog.event.onDoubleClick = (row:any) =>{
                                // console.log(row);
                                window.open(`/hanh-trinh-taxi/${$scope.$parent.cpnSelectSettings.itemSelect().Id}/${row.Serial}/${ChangeTime(row.BeginTime)}/${ChangeTime(row.EndTime)}`,'_self' );
                            };


                            function GetDataWithCompany(companyId:number, begindate:string, enddate:string){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.get(global.ReportHost + `api/SessionTaxiLog/SessionTaxiLogByCompany?companyId=${companyId}&beginTime=${begindate}&endTime=${enddate}`).then((rep:any)=>{
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            if(rep.data.SessionTaxiLogList.length > 0){
                                                $scope.gridSessionlog.data = rep.data.SessionTaxiLogList;
                                                ConvertData($scope.gridSessionlog.data);
                                                $scope.gridSessionlog.refresh();
                                            }else{
                                                $scope.gridSessionlog.clear();
                                                $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                    header: 'Thông báo!',
                                                    theme: 'bg-info',
                                                    position: 'center'
                                                });
                                            }

                                        }else{
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            $scope.gridSessionlog.clear();
                                            $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                                header: 'Thông báo!',
                                                theme: 'bg-danger',
                                                position: 'center'
                                            });


                                        }
                                    }else{
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridSessionlog.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                    }

                                });
                            }
                            function GetDataWithGroup(companyId:number,groupId:number,beginTime:string,endTime:string){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.get(global.ReportHost + `api/SessionTaxiLog/SessionTaxiLogByGroup?companyId=${companyId}&group=${groupId}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            if(rep.data.SessionTaxiLogList.length > 0){
                                                $scope.gridSessionlog.data = rep.data.SessionTaxiLogList;
                                                ConvertData($scope.gridSessionlog.data);
                                                $scope.gridSessionlog.refresh();
                                            }else{
                                                $scope.gridSessionlog.clear();
                                                $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                    header: 'Thông báo!',
                                                    theme: 'bg-info',
                                                    position: 'center'
                                                });
                                            }

                                        }else{
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            $scope.gridSessionlog.clear();
                                            $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                                header: 'Thông báo!',
                                                theme: 'bg-danger',
                                                position: 'center'
                                            });
                                        }
                                    }else{
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridSessionlog.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                    }

                                });
                            }
                            function GetDataWithSerial(companyId:number,beginTime:string,endTime:string,serial:string){
                                $scope.$parent.viewData='Đang tải';
                                $scope.$parent.loading = true;
                                $http.get(global.ReportHost + `api/SessionTaxiLog/SessionTaxiLogBySerial?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}&serial=${serial}`).then((rep:any)=>{
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            if(rep.data.SessionTaxiLogList.length > 0){
                                                $scope.gridSessionlog.data = rep.data.SessionTaxiLogList;
                                                ConvertData($scope.gridSessionlog.data);
                                                $scope.gridSessionlog.refresh();
                                            }else{
                                                $scope.gridSessionlog.clear();
                                                $.jGrowl('Không có dữ liệu trả về. Vui lòng chọn ngày tháng khác để xem', {
                                                    header: 'Thông báo!',
                                                    theme: 'bg-info',
                                                    position: 'center'
                                                });
                                            }

                                        }else{
                                            $scope.$parent.viewData='Xem';
                                            $scope.$parent.loading = false;
                                            $scope.gridSessionlog.clear();
                                            $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                                header: 'Thông báo!',
                                                theme: 'bg-danger',
                                                position: 'center'
                                            });
                                        }
                                    }else {
                                        $scope.$parent.viewData='Xem';
                                        $scope.$parent.loading = false;
                                        $scope.gridSessionlog.clear();
                                        $.jGrowl('Lỗi trong quá trình lấy dữ liệu. Xin liên hệ quản trị viên.', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });
                                    }

                                });
                            }
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    if(val.BeginLocation !== null && val.EndLocation !== null){
                                        if(val.BeginLocation.Address !== null && val.EndLocation.Address !== null){
                                            val.Address = `<strong>Đón:</strong> ${val.BeginLocation.Address} </br> <strong>Trả : </strong> ${val.EndLocation.Address}`;
                                        }else{
                                            val.Address = '';
                                        }
                                    }
                                    val.VungAddress = `<strong>Đón:</strong> ${val.VungBatDau} </br> <strong>Trả : </strong> ${val.VungKetThuc}`;
                                    val.DiemAddress = `<strong>Đón:</strong> ${val.DiemBatDau} </br> <strong>Trả : </strong> ${val.DiemKetThuc}`;
                                    // if(val.BeginLocation !== null){
                                    //     if(val.BeginLocation.Address !== null){
                                    //         val.AdrressStart = val.BeginLocation.Address;
                                    //     }else{
                                    //         val.AdrressStart = '';
                                    //     }
                                    // }
                                    // if(val.EndLocation !== null){
                                    //     if(val.EndLocation.Address !== null){
                                    //         val.AdrressEnd = val.EndLocation.Address;
                                    //     }else{
                                    //         val.AdrressEnd = '';
                                    //     }
                                    // }else{
                                    //     val.AdrressEnd = val.AdrressStart;
                                    // }
                                    val.TimeTotal = `<strong>Đón:</strong> ${global.AdapterDate(val.BeginTime)} </br> <strong>Trả :</strong> ${global.AdapterDate(val.EndTime)}`;
                                    val.BeginTime = global.AdapterDate(val.BeginTime);
                                    val.EndTime = global.AdapterDate(val.EndTime);
                                    if(val.CuocKhongKm){
                                        val.CuocKhongKm_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.CuocKhongKm_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(val.CuocVangLai){
                                        val.CuocVangLai_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.CuocVangLai_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(val.IsCuocHopDong){
                                        val.IsCuocHopDong_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.IsCuocHopDong_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    val.ThoiGianCho = val.ThoiGianCho.slice(0,5);
                                    val.TongKmGps = (val.TongKmGps/1000).toFixed(1);
                                    val.KmRong = (val.KmRong/1000).toFixed(1);
                                    val.TongKmDongHo = (val.TongKmDongHo/1000).toFixed(1);
                                    val.Display = `${val.SoHieu} - ${val.Bs}`;
                                    val.TyLeKm_show = ((val.TongKmDongHo / val.TongKmGps) * 100).toFixed(0);
                                });

                            }
                            //chuyển thời gian để đưa qua hành trình
                            function ChangeTime(time:any){
                                var year:string = time.slice(15,19);
                                var month:string = time.slice(12,14);
                                var date:string = time.slice(9,11);

                                var hour:string = time.slice(0,8);

                                var tt:any = hour +' ' +month + '-' + date + '-' + year ;
                                return tt;
                            };
                        }
                    }
                    AdModule.controller('SessionlogController', SessionlogController);
                }
            }
        }
    }
}