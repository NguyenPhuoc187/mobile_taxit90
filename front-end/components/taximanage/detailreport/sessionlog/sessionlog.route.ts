module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export class SessionlogRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper,global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'detailreport.sessionlog',
                            config: {
                                controller: Taximanage.Detailreport.Sessionlog.SessionlogController,
                                templateUrl: '/components/taximanage/detailreport/sessionlog/sessionlog.html'+`?v=${global.Version}`,
                                url: '/chi-tiet-cuoc-xe',
                                tag:{
                                    title:'Chi tiết cuốc xe',
                                    icon:'icon-clipboard3',
                                    groupId:'taxi',
                                    index:2
                                }
                            }
                        });
                    }
                }
                AdModule.run(SessionlogRouteModel);
            }
        }
    }
}