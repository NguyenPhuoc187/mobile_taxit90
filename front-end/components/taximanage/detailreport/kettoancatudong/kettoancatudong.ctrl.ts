/**
 * Created by NPPRO on 16/11/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Kettoancatudong{
                    export interface IKettoancatudongScope extends ng.IScope{
                        $parent: IDetailreportScope;
                        dataphidichvu:any;
                        dataxuatbaocao:any;
                        print:()=>any;
                        exportword:()=>any;
                        gridSettings:any;
                        createWidget:boolean;
                        heightgrid:number;
                        exportexcell:()=>any;
                        datafilterview:any; // data lọc của grid
                        rowDoubleClick:(event:any)=>any;
                    }
                    export class KettoancatudongController{
                        public static $inject = ['$scope','language','global', '$http','$q'];
                        constructor($scope: IKettoancatudongScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService, $http: ng.IHttpService,$q:ng.IQService) {
                            $scope.$parent.viewData = language.gridLg().viewData;
                            $scope.$parent.checkboxes_combobox = false;
                            $scope.$parent.checkzonelocation = false;
                            $scope.$parent.showlocationandpoint = false;
                            $scope.$parent.hideComboboxCar = false;
                            $scope.$parent.hidecalendar = false;
                            $scope.$parent.hidesinglecalendar = true;

                            $scope.$parent.hideComboboxGroupCar = false;

                            $scope.$on('$destroy',() =>{
                                var printelment = document.getElementById("printSection");
                                if(printelment !== null && printelment !== undefined){
                                    printelment.remove();
                                }
                            });
                            // sự kiện nhấn xem báo cáo
                            $scope.$parent.btViewClick = () => {
                                $scope.gridSettings = {};
                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined){
                                    if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id !== -1){
                                        if($scope.$parent.modelSelectSettings.itemSelect() !== undefined){
                                            if($scope.$parent.modelSelectSettings.itemSelect().Serial  !== '-1'){
                                                //lấy báo cáo theo serial
                                                GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.modelSelectSettings.itemSelect().Serial,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end(),$scope.$parent.groupdeviceSelectSettings.itemSelect().Id);
                                            }else{
                                                //lấy báo cáo theo đội xe
                                                GetDataWithGroup($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.groupdeviceSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(), $scope.$parent.dtTimeSelect.end());
                                            }
                                        }else{
                                            GetDataWithGroup($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.groupdeviceSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(), $scope.$parent.dtTimeSelect.end());
                                        }
                                    }else{

                                        if($scope.$parent.modelSelectSettings.itemSelect() !== undefined){
                                            if($scope.$parent.modelSelectSettings.itemSelect().Serial  !== '-1'){
                                                //lấy báo cáo theo serial
                                                GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.modelSelectSettings.itemSelect().Serial,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end(),$scope.$parent.groupdeviceSelectSettings.itemSelect().Id);
                                            }else{
                                                //lấy báo cáo theo đội xe
                                                GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(), $scope.$parent.dtTimeSelect.end());
                                            }
                                        }else{
                                            GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(), $scope.$parent.dtTimeSelect.end());
                                        }
                                    }

                                }
                            };
                            // xử lý sự kiện xuât hiện modal của lấy kết toán
                            $('#baocao').on('shown.bs.modal',()=> {
                                // console.log($('label.chitietcuocxe'));
                                // console.log($('label.cuochopdong'));
                                $('label.chitietcuocxe').append("<input type='checkbox' id='check_chitietcuocxe' style='margin-top: -17px'/>"); // mặc định là bỏ check (a tí yêu cầu)
                                $('label.cuochopdong').append("<input type='checkbox' id='check_cuochopdong' style='margin-top: -17px;margin-left: 11px;'/>");// mặc định là bỏ check(a tí yêu cầu)
                                $('table.chitietcuocxe').hide();
                                $('table.cuochopdong').hide();
                                $('input#check_chitietcuocxe').click((event:any) => {
                                    // console.log(event);
                                    if(!event.currentTarget.checked){
                                        $('table.chitietcuocxe').hide();
                                    }else{
                                        $('table.chitietcuocxe').show();
                                    }
                                });
                                $('input#check_cuochopdong').click((event:any) => {
                                    // console.log(event);
                                    if(!event.currentTarget.checked){
                                        $('table.cuochopdong').hide();
                                    }else{
                                        $('table.cuochopdong').show();
                                    }
                                });
                            });
                            $scope.createWidget = false;
                            /**
                             * In trực tiếp ra ngoài máy in trên trình duyệt
                             * @param elem
                             * @param append
                             * @param delimiter
                             */
                            // function printElement(elem:any, append?:any, delimiter?:any) {
                            //     var domClone = elem.cloneNode(true);
                            //
                            //     var $printSection = document.getElementById("printSection");
                            //
                            //     if (!$printSection) {
                            //         var $printSection = document.createElement("div");
                            //         $printSection.id = "printSection";
                            //         document.body.appendChild($printSection);
                            //     }
                            //
                            //     if (append !== true) {
                            //         $printSection.innerHTML = "";
                            //     }
                            //
                            //     else if (append === true) {
                            //         if (typeof(delimiter) === "string") {
                            //             $printSection.innerHTML += delimiter;
                            //         }
                            //         else if (typeof(delimiter) === "object") {
                            //             $printSection.appendChlid(delimiter);
                            //         }
                            //     }
                            //
                            //     $printSection.appendChild(domClone);
                            // }
                            /**
                             * xử lý sự kiện in trên modal
                             */
                            $scope.print = () => {
                                $("#printbaocao").print(
                                    {
                                        globalStyles: true,
                                        mediaPrint: false,
                                        stylesheet: null,
                                        noPrintSelector: ".no-print",
                                        iframe: true,
                                        append: null,
                                        prepend: null,
                                        manuallyCopyFormValues: true,
                                        deferred: $.Deferred(),
                                        timeout: 750,
                                        title: null,
                                        doctype: '<!doctype html>'
                                    }
                                );
                            };
                            /**
                             * Xử lý sự kiện xuất file word kết toán ca
                             */
                            $scope.exportword = () => {
                                $("#xuatword").wordExport('BAO CAO CHI TIET CHOT CA CUA LAI XE');
                            };


                            function GetDataWithCompany(companyId:number,beginTime:string,endTime:string){
                                language.showLoading(false, "");
                                $http.get(global.ReportHost + `api/InputTaxi/FeeServiceByCompany?companyId=${companyId}`).then((rep: any) => {
                                    if (rep.data.Status == 1) {
                                        if (rep.data.FeeServiceList.length > 0) {
                                            // var datadichvu:any = rep.data.FeeServiceList;
                                            $scope.dataphidichvu = rep.data.FeeServiceList;
                                            $http.get(global.ReportHost + `api/KetToanCaAuto/KetToanCaAutoByCompany?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{
                                                if(rep.data.Status == 1){
                                                    ConvertData(rep.data.KetToanCaTranfers);
                                                    BuildDataGrid(rep.data.KetToanCaTranfers,$scope.dataphidichvu );
                                                }
                                            });
                                        }else{
                                            $scope.dataphidichvu = rep.data.FeeServiceList;
                                            $http.get(global.ReportHost + `api/KetToanCaAuto/KetToanCaAutoByCompany?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{
                                                if(rep.data.Status == 1){
                                                    ConvertData(rep.data.KetToanCaTranfers);
                                                    BuildDataGrid(rep.data.KetToanCaTranfers,[] );
                                                }
                                            });

                                        }
                                        language.showLoading(true, "");
                                    } else {
                                        console.debug(rep.data.Description);
                                        language.showLoading(true, "");
                                    }
                                });
                            }



                            /**
                             * Lấy dữ liệu báo cáo lịch sử kết toán ca theo đội xe
                             * @param {number} companyId: giá trị id công ty
                             * @param {number} groupId: giá trị id đội xe
                             * @param {string} beginTime: thời gian bắt đầu
                             * @param {string} endTime: thời gian kết thúc
                             * @constructor
                             */
                            function GetDataWithGroup(companyId:number,groupId:number,beginTime:string,endTime:string){
                                language.showLoading(false, "");
                                $http.get(global.ReportHost + `api/InputTaxi/FeeServiceByGroup?companyId=${companyId}&deviceGroupId=${groupId}`).then((rep: any) => {
                                    if (rep.data.Status == 1) {
                                        if (rep.data.FeeServiceList.length > 0) {
                                            // var datadichvu:any = rep.data.FeeServiceList;
                                            $scope.dataphidichvu = rep.data.FeeServiceList;
                                            $http.get(global.ReportHost + `api/KetToanCaAuto/KetToanCaAutoByGroup?companyId=${companyId}&groupId=${groupId}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{
                                                if(rep.data.Status == 1){
                                                    ConvertData(rep.data.KetToanCaTranfers);
                                                    BuildDataGrid(rep.data.KetToanCaTranfers,$scope.dataphidichvu );
                                                }
                                            });
                                        }else{
                                            $scope.dataphidichvu = rep.data.FeeServiceList;
                                            $http.get(global.ReportHost + `api/KetToanCaAuto/KetToanCaAutoByGroup?companyId=${companyId}&groupId=${groupId}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{
                                                if(rep.data.Status == 1){
                                                    ConvertData(rep.data.KetToanCaTranfers);
                                                    BuildDataGrid(rep.data.KetToanCaTranfers,[] );
                                                }
                                            });

                                        }
                                        language.showLoading(true, "");
                                    } else {
                                        console.debug(rep.data.Description);
                                        language.showLoading(true, "");
                                    }
                                });

                            }
                            /**
                             * Lấy dữ liệu báo cáo lịch sử kết toán ca theo serial
                             * @param {number} companyId
                             * @param {string} serial
                             * @param {string} beginTime
                             * @param {string} endTime
                             * @param {number} groupId
                             * @constructor:
                             */
                            function GetDataWithSerial(companyId:number,serial:string,beginTime:string,endTime:string, groupId:number){
                                language.showLoading(false, "");
                                $http.get(global.ReportHost + `api/InputTaxi/FeeServiceByGroup?companyId=${companyId}&deviceGroupId=${groupId}`).then((rep: any) => {
                                    if (rep.data.Status == 1) {
                                        if (rep.data.FeeServiceList.length > 0) {
                                            // var datadichvu:any = rep.data.FeeServiceList;
                                            $scope.dataphidichvu = rep.data.FeeServiceList;
                                            $http.get(global.ReportHost + `api/KetToanCaAuto/KetToanCaAutoBySerial?companyId=${companyId}&serial=${serial}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{
                                                if(rep.data.Status == 1){
                                                    ConvertData(rep.data.KetToanCaTranfers);
                                                    BuildDataGrid(rep.data.KetToanCaTranfers,$scope.dataphidichvu );
                                                }
                                            });
                                        }else{
                                            $scope.dataphidichvu = rep.data.FeeServiceList;
                                            $http.get(global.ReportHost + `api/KetToanCaAuto/KetToanCaAutoBySerial?companyId=${companyId}&serial=${serial}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{
                                                if(rep.data.Status == 1){
                                                    ConvertData(rep.data.KetToanCaTranfers);
                                                    BuildDataGrid(rep.data.KetToanCaTranfers,[] );
                                                }
                                            });
                                        }
                                        language.showLoading(true, "");
                                    } else {
                                        // console.debug(rep.data.Description);
                                        language.showLoading(true, "");
                                    }
                                });


                            }
                            /**
                             * Chuyển dữ liệu kết toán ca để build data bỏ vào grid-view gồm phí dịch vụ công ty get về
                             * @param value: dữ liệu lịch sử kết toán
                             * @constructor
                             */
                            function ConvertData(value:any){

                                var result:any = [];
                                var result_phitaixe:any = [];
                                $.each(value,(ind:number,val:any)=>{
                                    console.log(val.PhiCongTyTra);

                                    var phicongtytra = val.PhiCongTyTra;
                                    eval('result ='+phicongtytra);

                                    var phitaixetra = val.PhiTaiXeTra;
                                    eval('result_phitaixe ='+phitaixetra);
                                    console.log(result);



                                    val.totalnodatachiphi = result.Tong;
                                    val.totalnodatataixe = result_phitaixe.Tong;
                                    val.TongKmSuDung = (val.TongKmSuDung/1000).toFixed(1);
                                    val.TongKm = (val.TongKm/1000).toFixed(1);
                                    val.TaiXeChotTime = val.TaiXeChotTime + '+07:00';

                                    // var datetime_gmt = new Date(val.TaiXeChotTime);
                                    //
                                    // val.TaiXeChotTime = datetime_gmt.setHours(datetime_gmt.getHours() - 7);
                                    val.Display = `<span class="text-danger">${val.SoHieu} - ${val.Bs}</span>`;
                                    val.STT = ind +1;

                                    try {
                                        for(var i=0;i<$scope.dataphidichvu.length;i++){
                                            if($scope.dataphidichvu[i].CongTyChiu){
                                                val[`F${$scope.dataphidichvu[i].Id}`] = result[`F${$scope.dataphidichvu[i].Id}`];
                                            }else{
                                                val[`F${$scope.dataphidichvu[i].Id}`] = result_phitaixe[`F${$scope.dataphidichvu[i].Id}`];
                                            }
                                        }
                                    }catch (e){
                                        console.log('không có phí dịch vụ');
                                    }
                                });

                            }
                            /**
                             * hàm build table lịch sử kết toán ca động theo dữ liệu dịch vụ
                             * @param data: data của api get về sau khi được covert
                             * @param datafieldichvu: data của dịch vụ được get về
                             * @constructor
                             */
                            function BuildDataGrid(data:any, datafieldichvu:any){
                                console.log(data);
                                console.log(datafieldichvu);
                                var defer_source = $q.defer();
                                var defer_colums = $q.defer();
                                var source:any = {
                                    localdata: data,
                                    datafields:[
                                        {name:'STT',type:'number'},
                                        {name:'DataReport',type:'string'},
                                        {name:'TaiXeChotTime',type:'date'},
                                        {name:'CheckerChotTime',type:'date'},
                                        {name:'TaiKhoanChot',type:'string'},
                                        {name:'Display',type:'string'},
                                        {name:'Mnv',type:'string'},
                                        {name:'TenTaiXe',type:'string'},
                                        {name:'SoCaKd',type:'number'},
                                        {name:'TienTaiXeNopTruocThue',type:'number'},
                                        {name:'TienTaiXeNop',type:'number'},
                                        {name:'TienCongTyThu',type:'number'},
                                        {name:'TienTaiXeThu',type:'number'},
                                        {name:'ChichXung',type:'number'},
                                        {name:'SoLanTatNguon',type:'number'},
                                        {name:'SoCuoc',type:'number'},
                                        {name:'SoCuoc0Km',type:'number'},
                                        {name:'SoCuocHopDong',type:'number'},
                                        {name:'ThoiGianLamViec',type:'string'},
                                        {name:'TongKm',type:'string'},
                                        {name:'TongKmSuDung',type:'string'},
                                        {name:'totalnodatachiphi',type:'number'},
                                        {name:'totalnodatataixe',type:'number'},
                                    ],
                                    datatype:"array"
                                    // id: 'id',
                                };
                                var column:any = [
                                    {text:'STT', datafield:'STT',width: '35px',pinned: true, filterable:false,},
                                    {text:'report', datafield:'DataReport',width: '35px',hidden: true,filterable:false},
                                    {text:'TG chốt đồng hồ', datafield:'TaiXeChotTime',pinned: true,cellsFormat: 'HH:mm dd/MM/yyyy',width: '82px',className: 'gridtaxidetail_height40_2row', filterable:false,},
                                    {text: 'TG kết toán', datafield: 'CheckerChotTime',pinned: true,cellsFormat: 'HH:mm dd/MM/yyyy',width: '82px',className: 'gridtaxidetail_height40_2row', filterable:false,},
                                    {text: 'Tài khoản kết toán', datafield: 'TaiKhoanChot',pinned: true,width: '87px',className: 'gridtaxidetail_height40_2row', filterable:false,},
                                    {text: 'Số tài - Biển số', datafield: 'Display',pinned: true,width: '90px',className: 'gridtaxidetail_height40_2row'},
                                    {text: 'Mã nhân viên', datafield: 'Mnv',width: '83px'},
                                    {text: 'Tên tài xế', datafield: 'TenTaiXe',width: '134px'},
                                    {text: 'Số ca <br/>kinh doanh', datafield: 'SoCaKd',width: '73px',className: 'gridtaxidetail_height40_2row'},



                                ];
                                if(datafieldichvu.length >0){
                                    // var count = 0;
                                    // var count_phi = 0;
                                    // for(var i=0;i<datafieldichvu.length;i++){
                                    //     source.datafields.push({name:`F${datafieldichvu[i].Id}`,type:'number'});
                                    //     if(datafieldichvu[i].CongTyChiu){
                                    //         if(count == 0){
                                    //             count = 1;
                                    //             column.push({text: 'Tổng Cty trả', datafield: 'totalnodatachiphi',className: 'gridtaxidetail_height40_2row',width: '90px',cellsFormat: 'n'},{text: `Cty trả <br/>${datafieldichvu[i].Name}`, datafield: `F${datafieldichvu[i].Id}`,className: 'gridtaxidetail_height40_2row',width: '90px',cellsFormat: 'n'})
                                    //         }else{
                                    //             column.push({text: `Cty trả <br/> ${datafieldichvu[i].Name}`,className: 'gridtaxidetail_height40_2row', datafield: `F${datafieldichvu[i].Id}`,width: '90px'})
                                    //         }
                                    //     }else{
                                    //         if(count_phi == 0){
                                    //             count_phi = 1;
                                    //             column.push({text: 'Tổng TX trả', datafield: 'totalnodatataixe',className: 'gridtaxidetail_height40_2row',width: '90px',cellsFormat: 'n'},{text: `TX trả <br/>${datafieldichvu[i].Name}`, datafield: `F${datafieldichvu[i].Id}`,className: 'gridtaxidetail_height40_2row',width: '90px',cellsFormat: 'n'})
                                    //         }else{
                                    //             column.push({text: `TX trả <br/>${datafieldichvu[i].Name}`, datafield: `F${datafieldichvu[i].Id}`,className: 'gridtaxidetail_height40_2row',width: '90px',cellsFormat: 'n'})
                                    //         }
                                    //     }
                                    // }


                                    BuilDataPhiCty(source,datafieldichvu).then((data:any) => {

                                        console.log(data);
                                        $.each(data,(ind:number,val:any)=>{
                                            column.push(val);
                                        });
                                        BuilDataPhitaixe(datafieldichvu).then((phitaixe:any)=>{
                                            $.each(phitaixe,(ind:number,val:any)=>{
                                                column.push(val);
                                            });
                                            BuildThongtindongho().then((dongho:any)=>{
                                                $.each(dongho,(ind:number,val:any)=>{
                                                    column.push(val);
                                                });
                                            })
                                        });

                                    });

                                    var getLocalization:any = ()=>{
                                        var localizationobj:any = {
                                            pagergotopagestring: "Trang:",
                                            decimalseparator: ',',
                                            thousandsseparator: '.',
                                            pagershowrowsstring: " Hiển thị:",
                                            pagerrangestring: " / ",
                                            pagerpreviousbuttonstring: "Quay lại",
                                            pagernextbuttonstring: "Tiếp theo",
                                            pagerfirstbuttonstring: "Đầu tiên",
                                            pagerlastbuttonstring: "Cuối cùng",
                                            sortascendingstring :"Sắp xếp tăng dần",
                                            sortdescendingstring :"Sắp xếp giảm dần",
                                            sortremovestring :"Xóa bỏ sắp xếp",
                                            filterapplystring: "Đồng ý",
                                            filtercancelstring: "Hủy",
                                            filterclearstring: "Xóa tìm kiếm",
                                            filterstring: "<p class='text-success' style='margin-left: 19px'>Tìm kiếm nâng cao</p>",
                                            filtersearchstring: "Tìm kiếm:",
                                            filterstringcomparisonoperators: ['Không lọc', 'Tất cả', 'Dữ liệu', 'Dữ liệu(theo ký tự)',
                                                'Không trùng với dữ liệu', 'Không trùng với dữ liệu(theo ký tự)', 'Ký tự bắt đầu',
                                                'Ký tự cuối', 'Bằng giá trị'],
                                            filternumericcomparisonoperators: ['Bằng', 'Không Bằng', 'Nhỏ hơn', 'Nhỏ hơn hoặc bằng', 'Lớn hơn', 'Lớn hơn hoặc bằng', 'null', 'not null'],
                                            filterdatecomparisonoperators: ['Bằng', 'Không Bằng', 'Nhỏ hơn', 'Nhỏ hơn hoặc bằng', 'Lớn hơn', 'Lớn hơn hoặc bằng', 'null', 'not null'],
                                            filterbooleancomparisonoperators: ['equal', 'not equal'],
                                            validationstring: "Entered value is not valid",
                                            emptydatastring: "Không có dữ liệu để hiển thị",
                                            filterselectstring: "Select Filter",
                                            loadtext: "Đợi trong giây lát....",
                                            clearstring: "Xóa",
                                            todaystring: "Hôm nay",
                                            groupsheaderstring:'Kéo và thả tiêu đề của cột vào đây để thực hiện nhóm dữ liệu theo cột này',
                                        };
                                        return localizationobj;

                                    };

                                    $scope.gridSettings =
                                        {
                                            width: '100%',
                                            height:'500px',
                                            source: new $.jqx.dataAdapter(source),
                                            columnsResize: true,
                                            columns: column,
                                            showAggregates: true,
                                            columnGroups:
                                                [
                                                    { text: 'Thông tin đồng hồ', align: 'center', name: 'thongtindongho' },
                                                    { text: 'Thông tin doanh thu và chi phí', align: 'center', name: 'thongtinchung' },
                                                    { text: 'Chi phí công ty trả',parentGroup: 'thongtinchung', align: 'center', name: 'chiphictytra' },
                                                    { text: 'Chi phí tài xế trả', parentGroup: 'thongtinchung', align: 'center', name: 'chiphitaixe' },
                                                    // { text: 'Location', align: 'center', name: 'Location' }
                                                ],
                                            sortable: true,
                                            pageable: true,
                                            // autoheight: true,

                                            pagerMode: 'advanced',
                                            pageSize: 20,
                                            filterable: true,
                                            enableHover: true,
                                            localization: getLocalization(),

                                            filter:(event:any)=>{
                                                // console.log(event);
                                                // console.log(defaultFilterResult);
                                                $scope.datafilterview = event.args.owner.dataview.bounditems;

                                            },
                                            // rowDoubleClick: (event:any) => {
                                            //     try {
                                            //         var args = event.args;
                                            //         // row data.
                                            //         var row = args.row.bounddata;
                                            //         // console.log(row);
                                            //         if(row !== undefined && row !== null){
                                            //             $scope.dataxuatbaocao = row.DataReport;
                                            //             setTimeout(()=>{
                                            //                 $('#baocao').modal('show');
                                            //             },500)
                                            //         }
                                            //
                                            //     } catch(e) {
                                            //
                                            //     }
                                            // },
                                        };

                                    // $scope.gridSettings.on('rowDoubleClick',(event:any)=>{
                                    //     try {
                                    //         var args = event.args;
                                    //         // row data.
                                    //         var row = args.row.bounddata;
                                    //         // console.log(row);
                                    //         if(row !== undefined && row !== null){
                                    //             $scope.dataxuatbaocao = row.DataReport;
                                    //             setTimeout(()=>{
                                    //                 $('#baocao').modal('show');
                                    //             },500)
                                    //         }
                                    //
                                    //     } catch(e) {
                                    //
                                    //     }
                                    // });
                                    $scope.heightgrid = 500;
                                    $scope.createWidget = true;

                                    // $scope.gridLichsukettoanca.columns = column;
                                    // $scope.gridLichsukettoanca.data = data;
                                    // $scope.gridLichsukettoanca.refresh();

                                }else{
                                    var getLocalization:any = ()=>{
                                        var localizationobj:any = {
                                            pagergotopagestring: "Trang:",
                                            decimalseparator: ',',
                                            thousandsseparator: '.',
                                            pagershowrowsstring: " Hiển thị:",
                                            pagerrangestring: " / ",
                                            pagerpreviousbuttonstring: "Quay lại",
                                            pagernextbuttonstring: "Tiếp theo",
                                            pagerfirstbuttonstring: "Đầu tiên",
                                            pagerlastbuttonstring: "Cuối cùng",
                                            sortascendingstring :"Sắp xếp tăng dần",
                                            sortdescendingstring :"Sắp xếp giảm dần",
                                            sortremovestring :"Xóa bỏ sắp xếp",
                                            filterapplystring: "Đồng ý",
                                            filtercancelstring: "Hủy",
                                            filterclearstring: "Xóa tìm kiếm",
                                            filterstring: "<p class='text-success' style='margin-left: 19px'>Tìm kiếm nâng cao</p>",
                                            filtersearchstring: "Tìm kiếm:",
                                            filterstringcomparisonoperators: ['Không lọc', 'Tất cả', 'Dữ liệu', 'Dữ liệu(theo ký tự)',
                                                'Không trùng với dữ liệu', 'Không trùng với dữ liệu(theo ký tự)', 'Ký tự bắt đầu',
                                                'Ký tự cuối', 'Bằng giá trị'],
                                            filternumericcomparisonoperators: ['Bằng', 'Không Bằng', 'Nhỏ hơn', 'Nhỏ hơn hoặc bằng', 'Lớn hơn', 'Lớn hơn hoặc bằng', 'null', 'not null'],
                                            filterdatecomparisonoperators: ['Bằng', 'Không Bằng', 'Nhỏ hơn', 'Nhỏ hơn hoặc bằng', 'Lớn hơn', 'Lớn hơn hoặc bằng', 'null', 'not null'],
                                            filterbooleancomparisonoperators: ['equal', 'not equal'],
                                            validationstring: "Entered value is not valid",
                                            emptydatastring: "Không có dữ liệu để hiển thị",
                                            filterselectstring: "Select Filter",
                                            loadtext: "Đợi trong giây lát....",
                                            clearstring: "Xóa",
                                            todaystring: "Hôm nay",
                                            groupsheaderstring:'Kéo và thả tiêu đề của cột vào đây để thực hiện nhóm dữ liệu theo cột này',
                                        };
                                        return localizationobj;

                                    };
                                    BuildThongtindongho().then((data:any)=>{
                                        column.push(
                                            {text: 'Tổng tiền tài xế nộp trước thuế', datafield: 'TienTaiXeNopTruocThue',width: '100px',cellsFormat: 'n',columngroup:'thongtinchung',aggregates: ['sum']},
                                            {text: 'Tiền tài xế nộp', datafield: 'TienTaiXeNop',width: '100px',cellsFormat: 'n',columngroup:'thongtinchung',aggregates: ['sum']},
                                            {text: 'Tổng', datafield: 'totalnodatachiphi',width: '90px',cellsFormat: 'n',columngroup: 'chiphictytra'},
                                            {text: 'Tổng', datafield: 'totalnodatataixe',width: '90px',cellsFormat: 'n',columngroup: 'chiphitaixe'},
                                            {text: 'Tiền công ty thu', datafield: 'TienCongTyThu',width: '120px',cellsFormat: 'n',columngroup:'thongtinchung',aggregates: ['sum']},
                                            {text: 'Tiền tài xế thu', datafield: 'TienTaiXeThu',width: '120px',cellsFormat: 'n',columngroup:'thongtinchung',aggregates: ['sum']},
                                        );
                                        $.each(data,(ind:number,val:any)=>{
                                            column.push(val);
                                        })
                                    });
                                    $scope.gridSettings =
                                        {
                                            width: '100%',
                                            height:'500px',
                                            source: new $.jqx.dataAdapter(source),
                                            columnsResize: true,
                                            showAggregates: true,
                                            columns: column,
                                            columnGroups:
                                                [
                                                    { text: 'Thông tin đồng hồ', align: 'center', name: 'thongtindongho' },
                                                    { text: 'Thông tin doanh thu và chi phí', align: 'center', name: 'thongtinchung' },
                                                    { text: 'Chi phí công ty trả',parentGroup: 'thongtinchung', align: 'center', name: 'chiphictytra' },
                                                    { text: 'Chi phí tài xế trả', parentGroup: 'thongtinchung', align: 'center', name: 'chiphitaixe' },
                                                    // { text: 'Location', align: 'center', name: 'Location' }
                                                ],
                                            sortable: true,
                                            pageable: true,
                                            pageSize: 10,
                                            filterable: true,
                                            pagerMode: 'advanced',
                                            enableHover: true,
                                            localization: getLocalization(),
                                            // showfilterrow: true,
                                            // rowsheight: 30,
                                            // columnsheight: 40,
                                            // autorowheight: true,

                                            filter:(event:any)=>{
                                                // console.log(event);
                                                // console.log(defaultFilterResult);
                                                $scope.datafilterview = event.args.owner.dataview.bounditems;

                                            },
                                            rowDoubleClick: (event:any) => {
                                                try {
                                                    var args = event.args;
                                                    // row data.
                                                    var row = args.row.bounddata;
                                                    console.log(row);
                                                    if(row !== undefined && row !== null){
                                                        $scope.dataxuatbaocao = row.DataReport;
                                                        setTimeout(()=>{
                                                            $('#baocao').modal('show');
                                                        },500)
                                                    }

                                                } catch(e) {

                                                }
                                            },
                                        };
                                    $scope.heightgrid = 500;
                                    $scope.createWidget = true;
                                }
                                // console.log(source);
                                // var dataAdapter = new $.jqx.dataAdapter(source);


                                // // now create the widget.
                                // console.log(defer.promise);
                                // if(defer.promise.$$state.status == 1){
                                //     // $scope.gridSettings =
                                //     //     {
                                //     //         width: '100%',
                                //     //         height:700,
                                //     //         source: dataAdapter,
                                //     //         columnsresize: true,
                                //     //         columns: defer.promise
                                //     //     };
                                //     console.log($scope.gridSettings);
                                //     $scope.createWidget = true;
                                // }
                            }
                            /**
                             * Hàm xuất excel
                             */
                            $scope.exportexcell = () => {
                                var today = new Date(),
                                    time = today.toTimeString().split(':').join('').substr(0, 4),
                                    timestamp = getDate('dd-mm-yyyy', today);
                                var title= 'BÁO CÁO LỊCH SỬ KẾT TOÁN CA';
                                var info:any= {'Bs':'Tất cả', 'Starttime':$scope.$parent.dtTimeSelect.beginviewexcel(),Endtime:$scope.$parent.dtTimeSelect.endviewexcel()};
                                var data:any=[];
                                if($scope.datafilterview !== null && $scope.datafilterview !== undefined){
                                    data = $scope.datafilterview;
                                }else{
                                    console.log($scope.gridSettings.source);
                                    data = $scope.gridSettings.source.records;

                                }
                                var nameheader:Array<any> = [];//$scope.columns;
                                // if(data.length===0) {
                                //     ul.message.warning("Chưa có dữ liệu để xuất excel");
                                //     return;
                                // }

                                angular.forEach($scope.gridSettings.columns,function(c){
                                    // xét cột nào có thuộc tính ẩn
                                    if(c.hidden !== true){
                                        nameheader.push(c);//headerlenght++;
                                    }
                                });

                                /*
                                 function xuất excel mới
                                 * */
                                var hex = new Array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');


                                function escapeHTML(val:any) {
                                    var x = document.getElementById("from_text");


                                    var preescape = "" + val;
                                    var escaped = "";

                                    var i = 0;
                                    var brEscape = false;//document.getElementById('br').checked;
                                    var tagEscape = false;//document.getElementById('tag').checked;
                                    var others = true;//document.getElementById('other').checked;
                                    var nbsp = false;//document.getElementById('nbsp').checked;
                                    for (i = 0; i < preescape.length; i++) {
                                        var p = preescape.charAt(i);

                                        if (others) p = "" + escapeCharx(p);
                                        if (tagEscape) p = "" + escapeTags(p);
                                        if (brEscape) p = "" + escapeBR(p);
                                        if (nbsp) p = "" + escapeNBSP(p);

                                        escaped = escaped + p;
                                    }
                                    return escaped;
                                    //x = document.getElementById("to_text");
                                    //x.value = escaped;
                                }


                                function escapeBR(original:any) {
                                    var thechar = original.charCodeAt(0);

                                    switch (thechar) {
                                        case 10: return "<br/>";  //newline
                                        case '\r': break;
                                    }
                                    return original;
                                }

                                function escapeNBSP(original:any) {
                                    var thechar = original.charCodeAt(0);
                                    switch (thechar) {
                                        case 32: return "&nbsp;";  //space
                                    }
                                    return original;
                                }


                                function escapeTags(original:any) {
                                    var thechar = original.charCodeAt(0);
                                    switch (thechar) {
                                        case 60: return "&lt;";  //<
                                        case 62: return "&gt;";  //>
                                        case 34: return "&quot;";  //"
                                    }
                                    return original;

                                }

                                function escapeCharx(original:any) {
                                    var found = true;
                                    var thechar = original.charCodeAt(0);
                                    switch (thechar) {
                                        case 38: return "&amp;";
                                        case 198: return "&AElig;";
                                        case 193: return "&Aacute;";
                                        case 194: return "&Acirc;";
                                        case 192: return "&Agrave;";
                                        case 197: return "&Aring;";
                                        case 195: return "&Atilde;";
                                        case 196: return "&Auml;";
                                        case 199: return "&Ccedil;";
                                        case 208: return "&ETH;";
                                        case 201: return "&Eacute;";
                                        case 202: return "&Ecirc;";
                                        case 200: return "&Egrave;";
                                        case 203: return "&Euml;";
                                        case 205: return "&Iacute;";
                                        case 206: return "&Icirc;";
                                        case 204: return "&Igrave;";
                                        case 207: return "&Iuml;";
                                        case 209: return "&Ntilde;";
                                        case 211: return "&Oacute;";
                                        case 212: return "&Ocirc;";
                                        case 210: return "&Ograve;";
                                        case 216: return "&Oslash;";
                                        case 213: return "&Otilde;";
                                        case 214: return "&Ouml;";
                                        case 222: return "&THORN;";
                                        case 218: return "&Uacute;";
                                        case 219: return "&Ucirc;";
                                        case 217: return "&Ugrave;";
                                        case 220: return "&Uuml;";
                                        case 221: return "&Yacute;";
                                        case 225: return "&aacute;";
                                        case 226: return "&acirc;";
                                        case 230: return "&aelig;";
                                        case 224: return "&agrave;";
                                        case 229: return "&aring;";
                                        case 227: return "&atilde;";
                                        case 228: return "&auml;";
                                        case 231: return "&ccedil;";
                                        case 233: return "&eacute;";
                                        case 234: return "&ecirc;";
                                        case 232: return "&egrave;";
                                        case 240: return "&eth;";
                                        case 235: return "&euml;";
                                        case 237: return "&iacute;";
                                        case 238: return "&icirc;";
                                        case 236: return "&igrave;";
                                        case 239: return "&iuml;";
                                        case 241: return "&ntilde;";
                                        case 243: return "&oacute;";
                                        case 244: return "&ocirc;";
                                        case 242: return "&ograve;";
                                        case 248: return "&oslash;";
                                        case 245: return "&otilde;";
                                        case 246: return "&ouml;";
                                        case 223: return "&szlig;";
                                        case 254: return "&thorn;";
                                        case 250: return "&uacute;";
                                        case 251: return "&ucirc;";
                                        case 249: return "&ugrave;";
                                        case 252: return "&uuml;";
                                        case 253: return "&yacute;";
                                        case 255: return "&yuml;";
                                        case 162: return "&cent;";
                                        default:
                                            found = false;
                                            break;
                                    }
                                    if (!found) {
                                        if (thechar > 127) {
                                            var c = thechar;
                                            var a4 = c % 16;
                                            c = Math.floor(c / 16);
                                            var a3 = c % 16;
                                            c = Math.floor(c / 16);
                                            var a2 = c % 16;
                                            c = Math.floor(c / 16);
                                            var a1 = c % 16;
                                            //	alert(a1);
                                            return "&#x" + hex[a1] + hex[a2] + hex[a3] + hex[a4] + ";";
                                        }
                                        else {
                                            return original;
                                        }
                                    }
                                }


                                var exportExcel = new App.Shared.Library.ExportExcell();

                                // exportExcel.AddInfoHeader('QCVN-31 BÁO CÁO HÀNH TRÌNH XE',nameheader.length,null);
                                exportExcel.AddInfoHeader(`${title}`,nameheader.length,{
                                    color:'black',
                                    text_align: 'center',
                                    width:'auto',
                                    font_size:25,

                                });
                                if(info !== undefined && info !== null){
                                    exportExcel.AddInfoHeaderChildren(`Từ ${info.Starttime} đến ${info.Endtime} <br/> Đơn vị kinh doanh vận tải: ${localStorage.getItem('accinfo')}<br/> Biển số: ${info.Bs}`,nameheader.length,null);
                                }

                                for(var i=0;i<nameheader.length;i++){
                                    //if(nameheader[i].visible)
                                    exportExcel.AddInfoColumn(nameheader[i].text,null);
                                }

                                for(var i=0;i<data.length;i++) {
                                    exportExcel.AddInfoRow(mergeExcelData(data[i]),{
                                        color:'black',
                                        border: 'thin solid black',
                                        text_align: 'left',
                                        width:'auto',
                                        font_size:'11px',

                                    });
                                }
                                exportExcel.buildTable('exportable');
                                function mergeExcelData(data:any){
                                    var result:any={};
                                    angular.forEach($scope.gridSettings.columns,function(c){
                                        if(c.hidden!==true){
                                            if(data[c.datafield]==null) data[c.datafield]="";
                                            if (result[c.datafield] == undefined && data != null && data != undefined) {
                                                if(c.cellsFormat !==undefined && c.cellsFormat === 'dd-MM-yyyy'){
                                                    //todo: chuyển cột nào được định dạng ngày tháng thì chuyển sang dạng string hết để hiện thị ra excel cho đúng
                                                    // result[c.dataField] = data[c.dataField].toLocaleDateString();
                                                    result[c.datafield] = AdapterDateTime(data[c.datafield]);
                                                    // console.log(data[c.dataField]);
                                                }else if(c.cellsFormat !==undefined && c.cellsFormat === 'HH:mm:ss dd/MM/yyyy'){
                                                    result[c.datafield] = AdapterDateTime(data[c.datafield]);
                                                }else if(c.cellsFormat !==undefined && c.cellsFormat === 'HH:mm dd/MM/yyyy'){
                                                    result[c.datafield] = AdapterDateTime(data[c.datafield]);
                                                }
                                                else{
                                                    if(data[c.datafield] === '<span class="icon-checkbox-checked"></span>'){
                                                        result[c.datafield] = 'Có';
                                                    }else if(data[c.datafield] === '<span class="icon-checkbox-unchecked"></span>'){
                                                        result[c.datafield] = 'Không';
                                                    }else{
                                                        result[c.datafield] = data[c.datafield];
                                                    }
                                                    // result[c.dataField] = data[c.dataField];
                                                }
                                            }
                                        }
                                    });
                                    // console.debug(result);
                                    return result;
                                }
                                function getDate (mode:any, userdate:any) {
                                    var dte = userdate || new Date(),
                                        d = dte.getDate().toString(),
                                        m = (dte.getMonth() + 1).toString(),
                                        yyyy = dte.getFullYear().toString(),
                                        dd = (d.length < 2) ? '0' + d : d,
                                        mm = (m.length < 2) ? '0' + m : m,
                                        yy = yyyy.substring(2, 4);
                                    switch (mode) {
                                        case 'dd-mm-yyyy': return dd + '-' + mm + '-' + yyyy;
                                        case 'yyyymmdd': return yyyy + mm + dd;
                                        default: return dte;
                                    }
                                }

                                function AdapterDateTime(time:any){
                                    var year:string = time.getFullYear();
                                    var month:string = (time.getMonth()+1 >= 10)?time.getMonth() + 1: `0${time.getMonth() + 1}`;
                                    var date:string = (time.getDate() >= 10)?time.getDate():`0${time.getDate()}`;
                                    var min:string = `${(time.getHours() >= 10)?time.getHours():`0${time.getHours()}`} : ${(time.getMinutes()>= 10)?time.getMinutes():`0${time.getMinutes()}`}`;
                                    var tt:any = min +' ' +date + '/' + month + '/' + year ;
                                    return tt;
                                }

                                function AdapterDate(time:any){
                                    var year:string = time.getFullYear();
                                    var month:string = (time.getMonth() + 1 >= 10)?time.getMonth() + 1: `0${time.getMonth() + 1}`;
                                    var date:string = (time.getDate() >= 10)?time.getDate():`0${time.getDate()}`;
                                    // var min:string = `${(time.getHours() >= 10)?time.getHours():`0${time.getHours()}`} : ${time.getMinutes()}`;
                                    var tt:any = date + '/' + month + '/' + year ;
                                    return tt;
                                }


                                //     function Workbook() {
                                //         if(!(this instanceof Workbook))
                                //             return new Workbook();
                                //         this.SheetNames = [];
                                //         this.Sheets = {};
                                //     }
                                //     //
                                //     var ws_name = 'Test';
                                //     var ws:any = {};
                                // var tbl = document.getElementById('exportable');
                                //
                                //     $("#exportable").innerHTML.table2excel({
                                //         exclude: ".table",
                                //         name: "Worksheet Name",
                                //         filename: "SomeFile" //do not include extension
                                //     });
                                //
                                //
                                //
                                //     console.log(escapeHTML(document.getElementById('exportable').innerHTML));
                                // // var readtbl = XLSX.read(tbl);
                                // // var ws = XLSX.utils.table_to_sheet(tbl);
                                //     // console.log(tbl);
                                // // var readtbl:any = XLSX.read(tbl);
                                // // var wb = XLSX.utils.table_to_book(readtbl);
                                //
                                //     var wb = new Workbook(),ws = XLSX.utils.table_to_sheet(tbl);
                                //     //
                                //     // if(!ws.A1.c) ws.A1.c = [];
                                //     // ws.A1.c.push({a:"SheetJS", t:"I'm a little comment, short and stout!"});
                                //
                                //     if(!ws['A1'].s) ws['A1'].s = {};
                                //     ws['A1'].s = {fill:{patternType:"solid",bgColor:{ rgb: "ff0000" }}};
                                //     if(!ws.A2.s) ws.A2.s = {};
                                //         ws.A2.s = {border:{
                                //             top:{style:"medium",color:{rgb: "FFFFAA00"}},
                                //             bottom:{style:"medium",color:{rgb: "FFFFAA00"}},
                                //             left:{style:"medium",color:{rgb: "FFFFAA00"}},
                                //             right:{style:"medium",color:{rgb: "FFFFAA00"}}
                                //         }};
                                //         /* add worksheet to workbook */
                                //         wb.SheetNames.push(ws_name);
                                //         wb.Sheets[ws_name] = ws;
                                //     if(!ws['!cols']) ws['!cols'] = [];
                                //         ws['!cols'].push({wpx:100});
                                //
                                //     if(!ws['!rows']) ws['!rows'] = [];
                                //     ws['!rows'].push({hpx:50,level:7});
                                //     ws['!rowBreaks'] = [16,32];
                                //     console.log(wb);
                                // //
                                //
                                //
                                //
                                //     var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});

                                // var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary',themeXLSX:'XLSX'});
                                function s2ab(s:any) {
                                    var buf = new ArrayBuffer(s.length);
                                    var view = new Uint8Array(buf);
                                    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                                    return buf;
                                }

                                // var s = saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), `${title}_${timestamp}.xlsx`);
                                // var wb1 = XLSX.readFile(s,{cellStyles:true});




                                // var blob = new Blob([s2ab("<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns='http://www.w3.org/TR/REC-html40'><meta http-equiv='content-type' content='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8'><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>Sheet0</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body>"+document.getElementById('exportable').innerHTML + "</body></html>")], {
                                //     type: "application/octet-stream"
                                // });
                                // saveAs(blob, `${title}_${timestamp}.xlsx`);

                                console.log(document.getElementById('exportable').innerHTML);
                                var blob = new Blob([escapeHTML("<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns='http://www.w3.org/TR/REC-html40'><meta http-equiv='content-type' content='application/vnd.ms-excel; charset=UTF-8'><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>Sheet0</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body>"+document.getElementById('exportable').innerHTML + "</body></html>")], {
                                    type: "application/octet-stream"
                                });
                                saveAs(blob, `${title}_${timestamp}.xls`);
                            };

                            function BuilDataPhiCty(source:any,value:any){
                                var defer:any = $q.defer();
                                var result:any = [];
                                result.push(
                                    {text: 'Tổng tiền tài xế nộp trước thuế', datafield: 'TienTaiXeNopTruocThue',width: '120px',cellsFormat: 'n',columngroup:'thongtinchung',aggregates: ['sum']},
                                    {text: 'Tiền tính doanh thu (sau khi trừ thuế)', datafield: 'TienTaiXeNop',width: '120px',cellsFormat: 'n',columngroup:'thongtinchung',aggregates: ['sum']});

                                $.each(value,(ind:number,val:any)=> {
                                    source.datafields.push({name:`F${val.Id}`,type:'number'});
                                    if(val.CongTyChiu){
                                        result.push({text: `${val.Name}`,datafield: `F${val.Id}`,width: '90px',columngroup: 'chiphictytra'});
                                    }
                                });
                                result.push({text: 'Tổng', datafield: 'totalnodatachiphi',width: '90px',cellsFormat: 'n',columngroup: 'chiphictytra',aggregates: ['sum']});
                                defer.resolve(result);
                                return defer.promise;
                            }
                            function BuilDataPhitaixe(value:any){
                                var defer:any = $q.defer();
                                var result:any = [];
                                $.each(value,(ind:number,val:any)=> {
                                    if(!val.CongTyChiu){
                                        result.push({text: `${val.Name}`, datafield: `F${val.Id}`,width: '90px',columngroup: 'chiphitaixe',cellsFormat: 'n'});
                                    }
                                });
                                result.push(
                                    {text: 'Tổng', datafield: 'totalnodatataixe',width: '90px',cellsFormat: 'n',columngroup: 'chiphitaixe',aggregates: ['sum']},
                                    {text: 'Tiền công ty thu', datafield: 'TienCongTyThu',width: '120px',cellsFormat: 'n',columngroup:'thongtinchung',aggregates: ['sum']},
                                    {text: 'Tiền tài xế thu', datafield: 'TienTaiXeThu',width: '120px',cellsFormat: 'n',columngroup:'thongtinchung',aggregates: ['sum']},
                                );
                                defer.resolve(result);
                                return defer.promise;
                            }

                            function BuildThongtindongho(){
                                var defer:any = $q.defer();
                                var result:any = [];
                                result.push(
                                    {text: 'Chích xung', datafield: 'ChichXung',width: '55px',columngroup: 'thongtindongho'},
                                    {text: 'Số lần tắt nguồn', datafield: 'SoLanTatNguon',width: '70px',columngroup: 'thongtindongho'},
                                    {text: 'Số cuốc', datafield: 'SoCuoc',width: '55px',columngroup: 'thongtindongho'},
                                    {text: 'Số cuốc 0km', datafield: 'SoCuoc0Km',width: '55px',columngroup: 'thongtindongho'},
                                    {text: 'Số cuốc hợp đồng', datafield: 'SoCuocHopDong',width: '70px',columngroup: 'thongtindongho'},
                                    {text: 'Thời gian làm việc', datafield: 'ThoiGianLamViec',width: '140px',columngroup: 'thongtindongho'},
                                    {text: 'Tổng Km', datafield: 'TongKm',width: '90px',columngroup: 'thongtindongho',aggregates: ['sum']},
                                    {text: 'Tổng Km sử dụng', datafield: 'TongKmSuDung',width: '100px',columngroup: 'thongtindongho',aggregates: ['sum']},
                                );
                                defer.resolve(result);
                                return defer.promise;
                            }

                            $scope.rowDoubleClick = (event) => {
                                var args = event.args;
                                //         // row data.
                                var row = args.row;
                                // console.log(row);
                                if(row !== undefined && row !== null){
                                    $scope.dataxuatbaocao = row.DataReport;
                                    setTimeout(()=>{
                                        $('#baocao').modal('show');
                                    },500)
                                }
                            }
                        }
                    }
                    AdModule.controller('KettoancatudongController', KettoancatudongController);
                }
            }
        }
    }
}