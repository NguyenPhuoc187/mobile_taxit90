/**
 * Created by phamn on 26/10/2016.
 */

module App{
    export module Components{
        export module Taximanage{
            export module Detailreport{
                export module Zonetaxi{
                    export interface IZonetaxiScope extends ng.IScope{
                        gridZonetaxi: App.Shared.Directives.GridviewControl;
                        $parent: IDetailreportScope;
                    }
                    export class ZonetaxiController{
                        public static $inject = ['$scope','language','global', '$http'];
                        constructor($scope: IZonetaxiScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService, $http: ng.IHttpService){
                        //note: cấu hình trang master
                            $scope.$parent.viewData = language.gridLg().viewData;
                            $scope.$parent.checkboxes_combobox = false; //  tính năng chọn nhiều xe true: bật, false: tắt
                            $scope.$parent.checkzonelocation = true; // tính năng chọn vùng
                            $scope.$parent.showlocationandpoint = true; //
                            $scope.$parent.hideComboboxCar = true; // true: ẩn combobox chọn xe
                            $scope.$parent.hidecalendar = false;
                            $scope.$parent.hidesinglecalendar = true;
                            $scope.$parent.hideComboboxGroupCar = false;


                            $scope.gridZonetaxi = new App.Shared.Directives.GridviewControl("gridZonetaxi");
                            $scope.gridZonetaxi.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridZonetaxi.filename_excel="BÁO CÁO SỐ LƯỢNG CUỐC XE TRONG VÙNG";
                            $scope.gridZonetaxi.columns = [];
                            $scope.gridZonetaxi.height = window.innerHeight - 200;
                            $scope.gridZonetaxi.columnsHeight = 30;
                            //bật tính năng lọc dữ liệu
                            $scope.gridZonetaxi.isshowAggregates = true;
                            $scope.gridZonetaxi.isAltRows=true;
                            $scope.gridZonetaxi.isFilter=true;
                            $scope.gridZonetaxi.isFilterMode= 'advanced';

                            $scope.gridZonetaxi.columns.push({ text: 'STT', dataField: 'STT', type: 'string', align: 'center', width:'50px',pinned: true });
                            $scope.gridZonetaxi.columns.push({ text: 'Tên vùng', dataField: 'ZoneName', type: 'string', width:'200px', align: 'center',filterable:true,pinned: true });
                            $scope.gridZonetaxi.columns.push({ text: 'Id vùng', dataField: 'ZoneId', type: 'string', width:'200px', align: 'center',hidden:true });
                            $scope.gridZonetaxi.columns.push({ text: 'Số lượng', dataField: 'BeginCount', type: 'number', align: 'center',width:'100px',filterable:true,columngroup:'cuocbatdau'  });
                            $scope.gridZonetaxi.columns.push({ text: 'Tổng tiền', dataField: 'BeginTotalMoney', type: 'number', align: 'center',width:'150px',filterable:true,columngroup:'cuocbatdau',cellsFormat: 'n',
                                aggregates: ['sum'],
                            });
                            $scope.gridZonetaxi.columns.push({ text: 'Tổng km', dataField: 'BeginTotalKm', type: 'float', align: 'center',width:'150px',filterable:true,columngroup:'cuocbatdau',cellsFormat: 'F1',
                                aggregates: ['sum'],
                            });
                            $scope.gridZonetaxi.columns.push({ text: 'Số lượng', dataField: 'EndCount', type: 'number', align: 'center',width:'100px',filterable:true,columngroup:'cuocketthuc'  });
                            $scope.gridZonetaxi.columns.push({ text: 'Tổng tiền', dataField: 'EndTotalMoney', type: 'number', align: 'center',width:'150px',filterable:true,columngroup:'cuocketthuc',cellsFormat: 'n',
                                aggregates: ['sum'],

                            });
                            $scope.gridZonetaxi.columns.push({ text: 'Tổng km', dataField: 'EndTotalKm', type: 'float', align: 'center',width:'auto',filterable:true,columngroup:'cuocketthuc',cellsFormat: 'F1',
                                aggregates: ['sum'],
                            });

                            $scope.gridZonetaxi.columnGroups.push({ text: 'Cuốc bắt đầu', name: 'cuocbatdau'});
                            $scope.gridZonetaxi.columnGroups.push({ text: 'Cuốc kết thúc', name: 'cuocketthuc'});

                            /**
                             * Xử lý sự kiện khi nhấn nút xem
                             */
                            $scope.$parent.btViewClick = ()=>{
                                //Nhấn nút xem

                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined){
                                    if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                        if($scope.$parent.zoneSelectSetting.valueSelects().length !== 0 && $scope.$parent.zoneSelectSetting.valueSelects() !== undefined){
                                            //todo: Xử lý chọn vùng mà ko chọn đội xe hoặc chọn đội xe là tất cả
                                            if($scope.$parent.zoneSelectSetting.valueSelects()[0].Id == '-1'){
                                                //note: xử lý khi chọn vùng là tất cả ta get theo công ty

                                                GetDataWithCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());

                                            }else{
                                                //note: nếu chọn vùng là khác tất cả
                                                GetDataGroupAndZoneApi($scope.$parent.zoneSelectSetting.valueSelects(),null,$scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                            }

                                        }else{
                                            //todo: xử lý không chọn vùng
                                            GetDataWithCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                        }

                                    }else{
                                        //note: có chọn đội xe
                                        if($scope.$parent.zoneSelectSetting.valueSelects().length !== 0 && $scope.$parent.zoneSelectSetting.valueSelects() !== undefined){
                                            //todo: chọn đội xe và vừa chọn vùng
                                            if($scope.$parent.zoneSelectSetting.valueSelects()[0].Id == '-1'){
                                                //note: lấy theo đội xe
                                                GetDataGroupAndZoneApi(null,[$scope.$parent.groupdeviceSelectSettings.itemSelect().Id],$scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                            }else{
                                                //note: lấy theo đội xe và vùng
                                                GetDataGroupAndZoneApi($scope.$parent.zoneSelectSetting.valueSelects(),[$scope.$parent.groupdeviceSelectSettings.itemSelect().Id],$scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                            }
                                        }else{
                                            //todo: chỉ chọn đội xe ko chọn vùng hoặc chọn vùng là tất cả
                                            GetDataGroupAndZoneApi(null,[$scope.$parent.groupdeviceSelectSettings.itemSelect().Id],$scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                        }
                                    }


                                    //todo:trường hợp chọn xe và chọn vùng
                                    // if($scope.$parent.modelSelectSettings.itemSelect() !== undefined){
                                    //     if($scope.$parent.modelSelectSettings.itemSelect() !== undefined){
                                    //         //nếu chọn xe và chọn vùng không thuộc trường hợp tất cả
                                    //         if($scope.$parent.modelSelectSettings.itemSelect().Serial !== '-1'){
                                    //             if($scope.$parent.zoneSelectSetting.valueSelects().length !== 0 && $scope.$parent.zoneSelectSetting.valueSelects() !== undefined){
                                    //                 GetDataSerialAndZone($scope.$parent.zoneSelectSetting.valueSelects(),$scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end(),$scope.$parent.modelSelectSettings.itemSelect().Serial);
                                    //             }else{
                                    //                 bootbox.dialog({
                                    //                     title: "Thông Báo",
                                    //                     message: "Vui lòng chọn vùng!!!",
                                    //                     buttons: {
                                    //                         confirm: {
                                    //                             label: "Quay lại",
                                    //                             className: "btn-default",
                                    //                         }
                                    //                     }
                                    //                 });
                                    //             }
                                    //         }else{
                                    //             if($scope.$parent.zoneSelectSetting.valueSelects()[0].Id == '-1'){
                                    //                 //note: lấy theo đội xe
                                    //                 GetDataGroupAndZoneApi(null,[$scope.$parent.groupdeviceSelectSettings.itemSelect().Id],$scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                    //             }else{
                                    //                 //note: lấy theo đội xe và vùng
                                    //                 GetDataGroupAndZoneApi($scope.$parent.zoneSelectSetting.valueSelects(),[$scope.$parent.groupdeviceSelectSettings.itemSelect().Id],$scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                    //             }
                                    //         }
                                    //     }
                                    // }else{
                                    //     bootbox.dialog({
                                    //         title: "Thông Báo",
                                    //         message: "Vui lòng chọn xe!!!",
                                    //         buttons: {
                                    //             confirm: {
                                    //                 label: "Quay lại",
                                    //                 className: "btn-default",
                                    //             }
                                    //         }
                                    //     });
                                    // }

                                }
                            };

                            /**
                             *  Chi tiết cuốc xe
                             */
                            $scope.gridZonetaxi.event.onDoubleClick = (row:any)=>{
                                if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                    var groupId = 0;
                                }else{
                                    groupId = $scope.$parent.groupdeviceSelectSettings.itemSelect().Id;
                                }
                                console.log(row);
                                window.open(`/chi-tiet-cuoc-xe/${$scope.$parent.cpnSelectSettings.itemSelect().Id}/${row.ZoneId}/${global.ChangeTimeGetLink($scope.$parent.dtTimeSelect.begin())}/${global.ChangeTimeGetLink($scope.$parent.dtTimeSelect.end())}/${groupId}/${row.ZoneName}`, '_blank');

                            };


                            /**
                             * Get hàm riêng xử lý api
                             */
                            function GetDataGroupAndZoneApi(checkzoneid:any, groupId:any,companyId:number, begindate:string, enddate:string){
                                language.showLoading(false,"");
                                //note: nếu chọn vùng là khác tất cả
                                var req:any = {
                                    checkZoneId: checkzoneid,
                                    groupId: groupId
                                };
                                $http.post(global.ReportHost + `api/ZonePointReport/ZoneTaxiReportLamda?companyId=${companyId}&beginTime=${begindate}&endTime=${enddate}`,req).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridZonetaxi.data = rep.data.ZoneTaxiReports;
                                        ConvertData($scope.gridZonetaxi.data);
                                        $scope.gridZonetaxi.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.log(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }

                            function GetDataWithCompanyId(companyId:number, begindate:string,enddate:string){
                                language.showLoading(false,"");
                                $http.get(global.ReportHost + `api/ZonePointReport/ZoneTaxiReport?companyId=${companyId}&beginTime=${begindate}&endTime=${enddate}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridZonetaxi.data = rep.data.ZoneTaxiReports;
                                        ConvertData($scope.gridZonetaxi.data);
                                        $scope.gridZonetaxi.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }

                            function GetDataSerialAndZone(checkzoneid:any, companyId:number, begindate:string, enddate:string, serial:string){
                                if(checkzoneid[0] == -1){
                                    checkzoneid.shift(); // cắt phần tử đầu tiên trong mảng (giá trị -1 của phần tử tất cả)

                                }
                                language.showLoading(false,"");
                                $http.post(global.ReportHost + `api/ZonePointReport/ZoneTaxiReportBySerial?companyId=${companyId}&beginTime=${begindate}&endTime=${enddate}&serial=${serial}`,checkzoneid).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridZonetaxi.data = rep.data.ZoneTaxiReports;
                                        ConvertData($scope.gridZonetaxi.data);
                                        $scope.gridZonetaxi.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }
                            /**
                             * Conver data
                             */
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    val.BeginTotalKm = (val.BeginTotalKm/1000).toFixed(1);
                                    val.EndTotalKm = (val.EndTotalKm/1000).toFixed(1);
                                });
                            }

                        }
                    }
                    AdModule.controller('ZonetaxiController', ZonetaxiController);
                }
            }
        }
    }
}