/**
 * Created by phamn on 28/10/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Inputtaxi{
                export module Percentbenefit{
                    export interface INewPercentbenefit{
                        Id?: number;
                        CompanyId?: number;
                        CompanyName?:string;
                        GroupId?: number;
                        GroupName?: string;
                        ModelName?: string;
                        FromMoney: number;
                        ToMoney: number;
                        Percent: number;
                        PercentOver: number;
                    }
                    export interface IPercentbenefitScope extends ng.IScope{
                        gridPercentbenefit: App.Shared.Directives.GridviewControl;
                        $parent:IInputtaxiScope;
                        newFormAccept:()=>any;
                        modelNameSelectting:App.Shared.Gui.IDropDown<any>;
                        grouppopupSelectting:App.Shared.Gui.IDropDown<App.Shared.Entity.IGroup>;
                        funNew:INewPercentbenefit;
                        isNew:boolean;
                    }
                    export class PercentbenefitController{
                        public static $inject = ['$scope','language','global', '$http','api','$q'];
                        constructor ($scope: IPercentbenefitScope,public language:App.Shared.LanguageService, public global: App.Shared.GlobalService, public $http: ng.IHttpService,
                                     public api:App.Shared.Api.IApi, public $q: ng.IQService){
                            $scope.$parent.hidemodel = false;

                            $scope.$parent.hidecar = true;
                            $scope.$parent.hidedatetime = true;
                            $scope.$parent.hideComboboxGroupCar = false;
                            $scope.isNew = true;

                            $scope.gridPercentbenefit = new App.Shared.Directives.GridviewControl("gridPercentbenefit");
                            $scope.gridPercentbenefit.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridPercentbenefit.filename_excel="Báo cáo tỉ lệ ăn chia";
                            $scope.gridPercentbenefit.columns = [];
                            $scope.gridPercentbenefit.height = window.innerHeight - 150;
                            $scope.gridPercentbenefit.columnsHeight = 40;
                            //note: bật tính năng tìm kiếm nâng cao
                            $scope.gridPercentbenefit.isAltRows=true;
                            $scope.gridPercentbenefit.isFilter=true;
                            $scope.gridPercentbenefit.isFilterMode= 'advanced';
                            //note: bật tính năng thêm, xóa , sửa
                            $scope.gridPercentbenefit.behavior.isNew = true;
                            $scope.gridPercentbenefit.behavior.isEdit = true;
                            $scope.gridPercentbenefit.behavior.isDelete = true;
                            $scope.gridPercentbenefit.columns.push({ text: 'STT', dataField: 'STT', type: 'string', align: 'center', width:'50px' });
                            $scope.gridPercentbenefit.columns.push({ text: 'Id', dataField: 'Id', type: 'string', align: 'center', width:'50px',hidden:true });
                            $scope.gridPercentbenefit.columns.push({ text: 'Đội xe', dataField: 'GroupName', type: 'string', align: 'center', width:'200px',filterable:true  });
                            $scope.gridPercentbenefit.columns.push({ text: 'Loại xe', dataField: 'ModelName', type: 'string', align: 'center', width:'200px',filterable:true,});
                            $scope.gridPercentbenefit.columns.push({ text: 'Từ mức', dataField: 'FromMoney', type: 'number', align: 'center', width:'150px',filterable:true, cellsFormat:'n'  });
                            $scope.gridPercentbenefit.columns.push({ text: 'Đến mức', dataField: 'ToMoney', type: 'number', align: 'center', width:'150px',filterable:true,cellsFormat:'n'  });
                            $scope.gridPercentbenefit.columns.push({ text: 'Tỉ lệ chia', dataField: 'Percent', type: 'number', align: 'center', width:'150px',filterable:true ,cellsFormat:'p' });
                            $scope.gridPercentbenefit.columns.push({ text: 'Tỉ lệ vượt mức', dataField: 'PercentOver', type: 'number', align: 'center', width:'150px',filterable:true, cellsFormat:'p'});


                            $scope.modelNameSelectting = new App.Shared.Gui.DropDown<any>('dropModelNamePercentbenefit','Name','Name');
                            $scope.modelNameSelectting.setting.width = '100%';
                            $scope.modelNameSelectting.setting.placeHolder = 'Chọn loại xe';
                            $scope.modelNameSelectting.setting.filterPlaceHolder = 'Tìm loại xe...';
                            $scope.modelNameSelectting.setting.checkboxes = false;
                            $scope.modelNameSelectting.title = "";

                            $scope.modelNameSelectting.load(api.GetAllModelCar(),null,true);

                            $scope.grouppopupSelectting = new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('dropGrouppopupPercentbenefit','Name','Id');
                            $scope.grouppopupSelectting.setting.width = '100%';
                            $scope.grouppopupSelectting.setting.placeHolder = 'Chọn đội xe';
                            $scope.grouppopupSelectting.setting.filterPlaceHolder = 'Tìm đội xe...';
                            $scope.grouppopupSelectting.setting.checkboxes = false;
                            $scope.grouppopupSelectting.title = "";

                            $scope.$parent.btViewClick = () => {
                                var first=Array<App.Shared.Entity.IGroup>();
                                // đẩy 1 group làm thông tin chọn tất cả
                                first.push( {
                                    Id: -1,
                                    Name: 'Tất cả',
                                    CompanyId:$scope.$parent.cpnSelectSettings.itemSelect().Id
                                });
                                $scope.grouppopupSelectting.load(api.GetAllGroupByCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id),first,true);
                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined){
                                    if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                        //note: nếu chọn đội là tất cả
                                        if($scope.$parent.modelNameSelectting.itemSelect().Name == 'Tất cả'){
                                            GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id);
                                        }else{
                                            GetDataWithLamda($scope.$parent.cpnSelectSettings.itemSelect().Id,0,$scope.$parent.modelNameSelectting.itemSelect().Name);
                                        }

                                    }else{
                                        //note: chọn đội khác
                                        if($scope.$parent.modelNameSelectting.itemSelect().Name == 'Tất cả'){
                                            GetDataWithGroup($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.groupdeviceSelectSettings.itemSelect().Id);
                                        }else{
                                            GetDataWithLamda($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.groupdeviceSelectSettings.itemSelect().Id,$scope.$parent.modelNameSelectting.itemSelect().Name);
                                        }

                                    }
                                }
                            };

                            //note: xử lý sự kiện tạo mới
                            $scope.gridPercentbenefit.event.onNew=() => {
                                $scope.isNew = true;
                                $scope.funNew = {
                                    FromMoney: 0,
                                    ToMoney: 0,
                                    Percent: 0,
                                    PercentOver: 0,
                                };
                                $scope.grouppopupSelectting.load(api.GetAllGroupByCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id),null,true);
                                $('#myModal_percentbenefit').modal('show');
                            };

                            //xử lý sự kiện cập nhật
                            $scope.gridPercentbenefit.event.onEdit = (ne:any, old:any) => {
                                console.log('dữ liệu',ne);
                                $scope.isNew = false;
                                $scope.funNew = {
                                    Id: ne.Id,
                                    FromMoney: ne.FromMoney,
                                    ToMoney: ne.ToMoney,
                                    Percent: ne.Percent,
                                    PercentOver: ne.PercentOver,
                                };
                                $scope.grouppopupSelectting.setItemDefault((m:any)=>m.Name == ne.GroupName);
                                $scope.modelNameSelectting.setItemDefault((m:any) => m.Name == ne.ModelName);
                                $scope.$apply();
                                $('#myModal_percentbenefit').modal('show');
                            };


                            //note: thực hiện xóa phí dịch vụ
                            $scope.gridPercentbenefit.event.onDelete = (ne:any)=>{
                                bootbox.dialog({
                                    title: "Thông Báo",
                                    message: "Bạn muốn xóa tỉ lệ ăn chia này !!!",
                                    buttons: {
                                        confirm: {
                                            label: "Đồng ý",
                                            className: "btn-danger",
                                            callback:()=>{
                                                DeleteTiLeAnChia(ne.Id,ne.CompanyId);
                                            }
                                        },
                                        cancle:{
                                            label:'Hủy',
                                            className: "btn-default",
                                        }
                                    }
                                });
                            };

                            //note: xử lý sự kiện nhấn nút submit khi tạo mới
                            $scope.newFormAccept = ()=>{
                                if($scope.isNew){
                                    var req:any = {
                                        CompanyId: $scope.$parent.cpnSelectSettings.itemSelect().Id,
                                        CompanyName: $scope.$parent.cpnSelectSettings.itemSelect().Name,
                                        GroupId: $scope.grouppopupSelectting.itemSelect().Id,
                                        GroupName: $scope.grouppopupSelectting.itemSelect().Name,
                                        ModelName: $scope.modelNameSelectting.itemSelect().Name,
                                        FromMoney: $scope.funNew.FromMoney,
                                        ToMoney: $scope.funNew.ToMoney,
                                        Percent: $scope.funNew.Percent,
                                        PercentOver: $scope.funNew.PercentOver
                                    };
                                    AddTiLeAnChia(req);
                                }else{
                                    var req_capnhat:any = {
                                        CompanyId: $scope.$parent.cpnSelectSettings.itemSelect().Id,
                                        Id: $scope.funNew.Id,
                                        FromMoney :$scope.funNew.FromMoney,
                                        ToMoney : $scope.funNew.ToMoney,
                                        Percent : $scope.funNew.Percent,
                                        PercentOver : $scope.funNew.PercentOver,
                                        ModelName : $scope.modelNameSelectting.itemSelect().Name,
                                        GroupId : $scope.grouppopupSelectting.itemSelect().Id,
                                        GroupName: $scope.grouppopupSelectting.itemSelect().Name,
                                    };
                                    UpdateTiLeAnChia(req_capnhat);

                                }

                            };

                            function GetDataWithCompany(companyId:number){
                                language.showLoading(false,"");
                                $http.get(global.ReportHost + `api/InputTaxi/PercentBenefitByCompany?companyId=${companyId}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridPercentbenefit.data = rep.data.PercentBenefitList;
                                        ConvertData($scope.gridPercentbenefit.data);
                                        $scope.gridPercentbenefit.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }
                            function GetDataWithGroup(companyId:number,groupId:number ){
                                language.showLoading(true,"");
                                $http.get(global.ReportHost + `api/InputTaxi/PercentBenefitByGroup?companyId=${companyId}&groupId=${groupId}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridPercentbenefit.data = rep.data.PercentBenefitList;
                                        ConvertData($scope.gridPercentbenefit.data);
                                        $scope.gridPercentbenefit.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }
                            function GetDataWithLamda(companyId:number,groupId:number,modelName:string){
                                language.showLoading(true,"");
                                $http.get(global.ReportHost + `api/InputTaxi/PercentBenefitByLamda?companyId=${companyId}&groupId=${groupId}&modelName=${modelName}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridPercentbenefit.data = rep.data.PercentBenefitList;
                                        ConvertData($scope.gridPercentbenefit.data);
                                        $scope.gridPercentbenefit.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }
                            function AddTiLeAnChia(req:any){
                                language.showLoading(false,"");
                                $http.post(global.ReportHost + 'api/InputTaxi/AddPercentBenefit',req).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "<p class='text-success'>Tạo tỉ lệ ăn chia thành công!!!</p>",

                                        });
                                        $scope.$parent.btViewClick();
                                    }else{
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "<p class='text-danger'>Tạo tỉ lệ ăn chia không thành công!!!</p>",
                                        });
                                    }
                                });
                                $('#myModal_percentbenefit').modal('hide');
                            }
                            function UpdateTiLeAnChia(req:any){
                                language.showLoading(false,"");
                                $http.put(global.ReportHost + `api/InputTaxi/UpdatePercentBenefit`,req).then((rep:any)=>{
                                    language.showLoading(true,"");
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            language.showLoading(true,"");
                                            bootbox.dialog({
                                                title: "Thông Báo",
                                                message: "<p class='text-success'>Cập nhật tỉ lệ ăn chia thành công!!!</p>",

                                            });
                                            for(var i=0;i<$scope.gridPercentbenefit.data.length; i++){
                                                if($scope.gridPercentbenefit.data[i].Id == $scope.funNew.Id){
                                                    $scope.gridPercentbenefit.data[i] = req;
                                                    ConvertData($scope.gridPercentbenefit.data);
                                                    $scope.gridPercentbenefit.refreshupdate();
                                                    // $scope.gridListCars.refresh();
                                                    break;
                                                }
                                            }
                                            // $scope.$parent.btViewClick();

                                        }else{
                                            bootbox.dialog({
                                                title: "Thông Báo",
                                                message: "<p class='text-danger'>Cập nhật tỉ lệ ăn chia không thành công</p>",
                                            });
                                        }
                                    }else{
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "<p class='text-danger'>Lỗi trong quá trình gửi dữ liệu lên server. Vui lòng liên hệ quản trị viên!!!</p>",
                                        });
                                    }
                                });
                                $('#myModal_percentbenefit').modal('hide');

                            }
                            function DeleteTiLeAnChia(percentBenefitId:number,companyId:number){
                                language.showLoading(true,"");
                                $http.delete(global.ReportHost + `api/InputTaxi/DeletePercentBenefit?percentBenefitId=${percentBenefitId}&companyId=${companyId}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Xóa tỉ lệ ăn chia thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Đồng ý",
                                                    className: "btn-success",
                                                    callback:()=>{
                                                        $scope.$parent.btViewClick();
                                                    }
                                                }
                                            }
                                        });
                                    }else{
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Xóa tỉ lệ ăn chia không thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Quay lại",
                                                    className: "btn-default",
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                   val.STT = ind + 1;
                                });
                            }

                        }
                    }
                    AdModule.controller('PercentbenefitController', PercentbenefitController);
                }
            }
        }
    }
}