/**
 * Created by phamn on 02/11/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Inputtaxi{
                export module Sessioncontract{
                    export interface INewCuocHopDong{
                        Id?: number;
                        CuocKhopId?: number;
                        Bs?: string;
                        SoTai?: string;
                        CompanyId?: number;
                        CompanyName?: string;
                        Serial?: string;
                        BeginLocation?: {
                            Lat?: number;
                            Lng?: number;
                            Address?: string;
                        },
                        EndLocation?: {
                            Lat?: number;
                            Lng?: number;
                            Address?: string;
                        },
                        BeginTime?: string;
                        ChenhLechTime?: number;
                        TimeRun?: string;
                        KmRun?: number;
                        MoneyRun?: number;
                        MoneyEveryKm?: number;
                        MoneyEveryTime?: number;
                        Cuoc2Chieu?: boolean;
                        CuocVangLai?: boolean;
                        NguoiTao?: string;
                        KieuTinhTien?: string;
                        TienQuaKm?:  number;
                        TienQuaGio?:  number;
                        TienTaiXeNop?: number;
                    }
                    export interface IEditCuocHopDong{
                        diembatdau?:string;
                        diemketthuc?:string;
                        TimeRun?:any;
                        ChenhLechTime?:number;
                        KmRun?:number;
                        MoneyEveryKm?:number;
                        CuocVangLai?:boolean;
                        Cuoc2Chieu?:boolean;
                        BeginTime?:string;
                        Id?:number;
                        MoneyEveryTime?:number;
                        MoneyRun?:number;
                        CompanyId?: number;
                    }
                    export interface IEditCuocVangLai{
                        // companyId?:number;
                        idCuocXe?:number;
                        beginTime?:string;
                        chenhLech?:number;
                    }
                    export interface ISessioncontractScope extends ng.IScope{
                        $parent: IInputtaxiScope;
                        startpointSelectting:  App.Shared.Gui.ICombobox<any>;
                        endpointSelectting:  App.Shared.Gui.ICombobox<any>;
                        companyId:number;
                        modelName:string;
                        groupId:number;
                        databangdinhtuyen:any;
                        init_date:() =>void; // hàm load sự kiện dom-on-creat khi đc load xong element
                        newFormAccept:()=>any;
                        newCuocVangLaiFormAccept:()=>any;
                        editFormAccept:()=>any;
                        funNew: INewCuocHopDong;
                        gridCuocdanhap: App.Shared.Directives.GridviewControl;
                        funEdit: IEditCuocHopDong;
                        showstatus:string;
                        funNewCuocVangLai:IEditCuocVangLai;
                        cuoc2chieu:(value:any)=>any;
                        thoigiannhankhach:string;
                    }
                    export class SessioncontractController{
                        public static $inject = ['$scope','language','global', '$http','api','$q'];
                        constructor ($scope: ISessioncontractScope,public language:App.Shared.LanguageService, public global: App.Shared.GlobalService, public $http: ng.IHttpService,
                                     public api:App.Shared.Api.IApi, public $q: ng.IQService){
                            $scope.$parent.hidecar = false;
                            $scope.$parent.hidemodel = true;
                            $scope.$parent.hidedatetime = false;
                            $scope.$parent.hideComboboxGroupCar = false;
                            //note: khai báo mới combobox nhập giá trị điểm bắt đầu
                            $scope.startpointSelectting = new App.Shared.Gui.Combobox('cbstartpoint','Name','BeginAddressLocation');
                            $scope.startpointSelectting.setting.placeHolder = 'Chọn điểm bắt đầu';
                            $scope.startpointSelectting.title = '';
                            $scope.startpointSelectting.setting.width = '100%';
                            //note: khai báo mới combobox nhập giá trị điểm bắt kết thúc
                            $scope.endpointSelectting = new App.Shared.Gui.Combobox('cbendpoint','EndAddressLocation','EndAddressLocation');
                            $scope.endpointSelectting.setting.placeHolder = 'Chọn điểm kết thúc';
                            $scope.endpointSelectting.title = '';
                            $scope.endpointSelectting.setting.width = '100%';


                            $scope.gridCuocdanhap = new App.Shared.Directives.GridviewControl("gridCuocdanhap");
                            $scope.gridCuocdanhap.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridCuocdanhap.filename_excel="Báo cáo cuốc đã nhập";
                            $scope.gridCuocdanhap.columns = [];
                            $scope.gridCuocdanhap.height = window.innerHeight - 200;
                            $scope.gridCuocdanhap.columnsHeight = 40;
                            //note: bật tính năng tìm kiếm nâng cao
                            $scope.gridCuocdanhap.isAltRows=true;
                            $scope.gridCuocdanhap.isshowAggregates = true;
                            $scope.gridCuocdanhap.isFilter=true;
                            $scope.gridCuocdanhap.isFilterMode= 'advanced';
                            //note: bật tính năng thêm, xóa , sửa
                            $scope.gridCuocdanhap.behavior.isNew = false;
                            $scope.gridCuocdanhap.behavior.isEdit = true;
                            $scope.gridCuocdanhap.behavior.isDelete = true;
                            $scope.gridCuocdanhap.columns.push({ text: 'STT', dataField: 'STT', type: 'string', align: 'center', width:'35px' });
                            $scope.gridCuocdanhap.columns.push({ text: 'Cuốc khớp', dataField: 'CuocKhopId', type: 'number', align: 'center', width:'47px',filterable:true});
                            // $scope.gridCuocdanhap.columns.push({ text: 'Id cuốc', dataField: 'Id', type: 'number', align: 'center', width:'50px', hidden:true });
                            $scope.gridCuocdanhap.columns.push({ text: 'Số tài - Biển số', dataField: 'Display', type: 'string', align: 'center', width:'96px',filterable:true });
                            $scope.gridCuocdanhap.columns.push({ text: 'Địa điểm đón trả khách', dataField: 'Address', type: 'string', align: 'center', width:'142px',filterable:true});

                            $scope.gridCuocdanhap.columns.push({ text: 'TG xuất phát', dataField: 'BeginTime', type: 'date', align: 'center', width:'82px', cellsFormat: 'HH:mm dd/MM/yyyy'});
                            $scope.gridCuocdanhap.columns.push({ text: 'TG chênh lệch', dataField: 'ChenhLechTime', type: 'string', align: 'center', width:'64px'});
                            $scope.gridCuocdanhap.columns.push({ text: 'Người tạo', dataField: 'NguoiTao', type: 'string', align: 'center', width:'75px',filterable:true  });
                            $scope.gridCuocdanhap.columns.push({ text: 'Kiểu tính tiền quá Km hoặc giờ', dataField: 'KieuTinhTien', type: 'string', align: 'center', width:'79px',filterable:true,className:'gridtaxidetail_height40_2row'});
                            $scope.gridCuocdanhap.columns.push({ text: 'Cuốc 2 chiều', dataField: 'Cuoc2Chieu_show', type: 'string', align: 'center', width:'43px',filterable:true,className:'gridtaxidetail_height40_2row'});
                            $scope.gridCuocdanhap.columns.push({ text: 'Cuốc vãng lai', dataField: 'CuocVangLai_show', type: 'string', align: 'center', width:'43px',filterable:true,className:'gridtaxidetail_height40_2row'});

                            // $scope.gridCuocdanhap.columns.push({ text: 'Điểm kết thúc', dataField: 'AdrressEnd', type: 'string', align: 'center', width:'100px',filterable:true});
                            $scope.gridCuocdanhap.columns.push({ text: 'Thời gian dự kiến', dataField: 'TimeRun', type: 'string', align: 'center', width:'64px',filterable:true});
                            $scope.gridCuocdanhap.columns.push({ text: 'Km dự kiến(m)', dataField: 'KmRun', type: 'number', align: 'center', width:'84px',filterable:true, cellsFormat: 'n'});
                            $scope.gridCuocdanhap.columns.push({ text: 'Tiền dự kiến', dataField: 'MoneyRun', type: 'number', align: 'center', width:'84px',filterable:true,cellsFormat: 'n'});
                            // $scope.gridCuocdanhap.columns.push({ text: 'Tiền quá mỗi km(VNĐ)', dataField: 'MoneyEveryKm', type: 'string', align: 'center', width:'100px',filterable:true,cellsFormat: 'n'});
                            // $scope.gridCuocdanhap.columns.push({ text: 'Tiền quá mỗi giờ(VNĐ)', dataField: 'MoneyEveryTime', type: 'string', align: 'center', width:'100px',filterable:true,cellsFormat: 'n'});
                            $scope.gridCuocdanhap.columns.push({ text: 'Tiền quá km', dataField: 'TienQuaKm', type: 'number', align: 'center', width:'77px',filterable:true,columngroup:'thongtinketoan',cellsFormat: 'n',className:'gridtaxidetail_height40_2row'});
                            $scope.gridCuocdanhap.columns.push({ text: 'Tiền quá giờ', dataField: 'TienQuaGio', type: 'number', align: 'center', width:'77px',filterable:true,columngroup:'thongtinketoan',cellsFormat: 'n',className:'gridtaxidetail_height40_2row'});
                            $scope.gridCuocdanhap.columns.push({ text: 'Tiền tài xế nộp', dataField: 'TienTaiXeNop', type: 'number', align: 'center', width:'77px',filterable:true,columngroup:'thongtinketoan',cellsFormat: 'n',className:'gridtaxidetail_height40_2row',
                                aggregates: [{
                                    'Total':
                                        function (aggregatedValue:any, currentValue:any, column:any, record:any) {
                                            var total = currentValue *1;
                                            return aggregatedValue + total;
                                        }
                                }],
                                aggregatesRenderer: function (aggregates:any, column:any, element:any) {
                                    var renderString = "<div style='margin: 4px; float: left;  height: 100%;'>";
                                    renderString += "<span>Tổng = </span>" + ((aggregates.Total != undefined)? aggregates.Total:0) + "</div>";
                                    return renderString;
                                }
                            });

                            $scope.gridCuocdanhap.columns.push({ text: 'TG xuất phát thực tế', dataField: 'ThoiGianDonKhach', type: 'date', align: 'center', width:'80px',filterable:true,columngroup:'cuocthucte',className:'gridtaxidetail_height40_2row',cellsFormat: 'HH:mm dd/MM/yyyy'});
                            $scope.gridCuocdanhap.columns.push({ text: 'Tiền cuốc', dataField: 'TienCuocXe', type: 'number', align: 'center', width:'77px',filterable:true,columngroup:'cuocthucte',cellsFormat: 'n'});
                            $scope.gridCuocdanhap.columns.push({ text: 'Km cuốc', dataField: 'KmCuocXe', type: 'number', align: 'center', width:'auto',filterable:true,columngroup:'cuocthucte',cellsFormat: 'n'});
                            $scope.gridCuocdanhap.columnGroups.push({ text: 'Kết toán cuốc', name: 'thongtinketoan'});
                            $scope.gridCuocdanhap.columnGroups.push({ text: 'Cuốc thực tế', name: 'cuocthucte'});

                            $scope.init_date = () =>{
                                var date = {
                                    singleDatePicker: true,
                                    timePicker: true,
                                    format: 'HH:mm DD/MM/YYYY',
                                    showDropdowns: true,
                                    opens:'left',
                                    timePicker12Hour: false,
                                    timePickerIncrement:1,
                                    // autoApply:true,
                                    locale: { cancelLabel: 'Hủy',
                                        applyLabel: 'Đồng ý',
                                        fromLabel:'Từ ngày',
                                        toLabel:'Đến ngày',
                                        daysOfWeek: ['<label class="text-danger">CN</label>', 'T2', 'T3', 'T4', 'T5', 'T6','T7'],
                                        monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
                                        firstDay: 1

                                    }
                                };
                                $('#TimeBegin').daterangepicker(date);
                                $('#TimeBegin_edit').daterangepicker(date);
                                $('#TimeBeginCuocVangLai').daterangepicker(date);

                            };

                            $scope.funNew = {
                                KmRun: 0,
                                MoneyEveryKm:0,
                                MoneyEveryTime:0,
                                MoneyRun:0,
                                ChenhLechTime: 10,
                                Cuoc2Chieu:false,
                                CuocVangLai:false,

                            };

                            $scope.funEdit = {
                                diembatdau:'',
                                diemketthuc:'',
                                // TimeRun:0,
                                ChenhLechTime: 10,
                                KmRun:0,
                                MoneyEveryKm:0,
                                CuocVangLai:false,
                                Cuoc2Chieu:false,
                                BeginTime:'',
                            };

                            $scope.funNewCuocVangLai = {
                                idCuocXe:0,
                                beginTime:'',
                                chenhLech:10,
                            };
                            //note: sự kiện chọn xe
                            $scope.$parent.selectcar = (item) =>{
                                api.GetBangGiaTuyen(item.CompanyId,item.GroupId,item.ModelName).then((data:any)=>{
                                    if(data !== undefined){
                                        var result_startpoint=$q.defer();
                                        var result_sosanh=$q.defer();
                                        // var beginname:string = '';
                                        // var result_data:any = [];
                                        // for(var i = 0; i< data.length; i++){
                                        //     if(beginname !== data[i].BeginAddressLocation){
                                        //         result_data.push(data[i]);
                                        //     }
                                        // }
                                        $scope.startpointSelectting.loaddataarraywithapi(result_startpoint.promise,$q,false,null);
                                        //$scope.endpointSelectting.loaddataarraywithapi(result.promise,$q,false,null);
                                        $scope.databangdinhtuyen = result_sosanh.promise;
                                        result_startpoint.resolve(data.datanamebegin);
                                        result_sosanh.resolve(data.datasosanh);
                                    }
                                });
                                $http.post(global.ReportHost + `api/TrangThaiTaxi/StatusDongHoBySerial?companyId=${item.CompanyId}`,[item.Serial]).then((rep:any)=>{
                                    // console.log(rep);
                                    if(rep.data.StatusDonghoTranfers.length == 1){
                                        var thoigian:string = rep.data.StatusDonghoTranfers[0].ThoiGianDonKhach;
                                        console.log(rep.data.StatusDonghoTranfers);
                                        switch (rep.data.StatusDonghoTranfers[0].TrangThaiTaxi){
                                            case 'Free':
                                                $scope.showstatus = 'Xe đang rỗi';
                                                $scope.thoigiannhankhach = '';
                                                break;
                                            case 'Work':
                                                $scope.showstatus = 'Xe đang chở khách';
                                                $scope.thoigiannhankhach = '- Thời gian nhận khách : ' + global.AdapterDate(thoigian);
                                                break;

                                        }
                                    }else{
                                        $scope.showstatus = '';
                                    }
                                });
                            };

                            //note: xử lý sự kiện chọn điểm bắt đầu
                            $scope.startpointSelectting.customEvent.onChange = (ind:number,val:any) =>{
                                // console.log(val);

                                // var result:any = [];
                                // for(var i = 0; i< $scope.databangdinhtuyen.$$state.value.length;i++){
                                //     if(val.BeginAddressLocation == $scope.databangdinhtuyen.$$state.value[i].BeginAddressLocation ){
                                //         result.push($scope.databangdinhtuyen.$$state.value[i]);
                                //     }
                                // }

                                GetDataDiemKetThuc(val).then(data => {
                                    // console.log(data);
                                    $scope.endpointSelectting.setting.jqxComboBox('clear');
                                    $scope.endpointSelectting.setting.source = data;
                                    $scope.startpointSelectting.instance.searchString = '';
                                });




                            };
                            //note: xử lý sự kiện chọn điểm kết thúc
                            $scope.endpointSelectting.customEvent.onChange = (index:number, val_end:any)=>{
                                $scope.funNew.Cuoc2Chieu = false;
                                // console.log($scope.databangdinhtuyen.$$state);
                                for(var i = 0; i< $scope.databangdinhtuyen.$$state.value.length;i++){
                                    if(val_end.Id == $scope.databangdinhtuyen.$$state.value[i].Id && val_end.BeginAddressLocation == $scope.databangdinhtuyen.$$state.value[i].BeginAddressLocation){
                                        $scope.funNew.KmRun = $scope.databangdinhtuyen.$$state.value[i].KmRun;
                                        $scope.funNew.MoneyEveryKm = $scope.databangdinhtuyen.$$state.value[i].MoneyEveryKm;
                                        $scope.funNew.MoneyEveryTime = $scope.databangdinhtuyen.$$state.value[i].MoneyEveryTime;
                                        $scope.funNew.MoneyRun = $scope.databangdinhtuyen.$$state.value[i].MoneyRun;
                                        $scope.$apply();
                                    }
                                }
                                $scope.endpointSelectting.instance.searchString = '';
                            };


                            /**
                             * Hàm lấy data địa chỉ kết thúc - tránh trường hợp mất data khi chọn điểm bắt đầu
                             * @param value: giá trị truyền vào từ điểm bắt đầu
                             * @returns {angular.IPromise<any>}
                             * @constructor
                             */
                            function GetDataDiemKetThuc(value:any){
                                var result: any = [];
                                var result_sosanh=$q.defer();
                                for(var i = 0; i< $scope.databangdinhtuyen.$$state.value.length;i++){
                                    if(value.BeginAddressLocation == $scope.databangdinhtuyen.$$state.value[i].BeginAddressLocation ){
                                        result.push($scope.databangdinhtuyen.$$state.value[i]);
                                    }
                                }
                                result_sosanh.resolve(result);
                                return result_sosanh.promise;
                            }

                            //note: sự kiện submit form tạo mới cuốc hợp đồng
                            $scope.newFormAccept = () => {
                                if($scope.$parent.carNameSelectting.itemSelect() === undefined){
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Vui lòng chọn xe trước khi cập nhật cuốc hợp đồng!!!",
                                        buttons: {
                                            confirm: {
                                                label: "Đồng ý",
                                                className: "btn-success",
                                            }
                                        }
                                    });
                                }else{
                                    console.log($scope.startpointSelectting.instance.searchString);
                                    var diembatdau:string = '';
                                    var diemketthuc: string = '';
                                    //note: kiểm tra khi chọn hay nhập giá trị vào ở ô địa chỉ bắt đầu
                                    if($scope.startpointSelectting.instance.searchString === ''){
                                        if($scope.startpointSelectting.itemSelect() !== undefined){
                                            diembatdau = $scope.startpointSelectting.itemSelect().BeginAddressLocation;
                                        }else{
                                            bootbox.dialog({
                                                title: "Thông Báo",
                                                message: "Vui lòng nhập địa điểm bắt đầu!!!",
                                                buttons: {
                                                    confirm: {
                                                        label: "Quay lại",
                                                        className: "btn-primary",
                                                    }
                                                }
                                            });
                                        }

                                    }else{
                                        diembatdau = $scope.startpointSelectting.instance.searchString;
                                    }
                                    //note: kiểm tra khi chọn hay nhập giá trị vào ở ô địa chỉ kết thúc
                                    if($scope.endpointSelectting.instance.searchString === ''){
                                        if($scope.endpointSelectting.itemSelect() !== undefined){
                                            diemketthuc = $scope.endpointSelectting.itemSelect().EndAddressLocation;
                                        }else{
                                            bootbox.dialog({
                                                title: "Thông Báo",
                                                message: "Vui lòng nhập địa điểm kết thúc!!!",
                                                buttons: {
                                                    confirm: {
                                                        label: "Quay lại",
                                                        className: "btn-primary",
                                                    }
                                                }
                                            });
                                        }
                                    }else{
                                        diemketthuc = $scope.endpointSelectting.instance.searchString;
                                    }
                                    console.log(diembatdau);
                                    var req:any = {
                                        Bs:$scope.$parent.carNameSelectting.itemSelect().Bs,
                                        CompanyId: $scope.$parent.cpnSelectSettings.itemSelect().Id,
                                        Serial: $scope.$parent.carNameSelectting.itemSelect().Serial,
                                        GroupId: $scope.$parent.carNameSelectting.itemSelect().GroupId,
                                        BeginLocation: {
                                            Lat: 0,
                                            Lng: 0,
                                            Address: diembatdau
                                        },
                                        EndLocation:{
                                            Lat: 0,
                                            Lng: 0,
                                            Address: diemketthuc
                                        },
                                        BeginTime: global.ChangeTime($scope.funNew.BeginTime),
                                        ChenhLechTime: $scope.funNew.ChenhLechTime,
                                        TimeRun: $scope.funNew.TimeRun,
                                        KmRun:$scope.funNew.KmRun,
                                        MoneyRun:$scope.funNew.MoneyRun,
                                        MoneyEveryKm:$scope.funNew.MoneyEveryKm,
                                        MoneyEveryTime: $scope.funNew.MoneyEveryTime,
                                        Cuoc2Chieu: $scope.funNew.Cuoc2Chieu,
                                        CuocVangLai:$scope.funNew.CuocVangLai,
                                    };
                                    AddCuocHopDong(req);
                                }


                            };

                            //note: sự kiện submit form tạo mới cuốc vãng lai
                            $scope.newCuocVangLaiFormAccept = () => {

                                if($scope.funNewCuocVangLai.idCuocXe == undefined){
                                    $scope.funNewCuocVangLai.idCuocXe = 0;
                                }
                                if($scope.funNewCuocVangLai.beginTime == undefined || $scope.funNewCuocVangLai.beginTime == ''){
                                    $scope.funNewCuocVangLai.beginTime = '00:00 01/01/2010';
                                }
                                if($scope.funNewCuocVangLai.chenhLech == undefined){
                                    $scope.funNewCuocVangLai.chenhLech = 10;
                                }
                                if($scope.$parent.carNameSelectting.itemSelect() !== undefined){
                                    language.showLoading(false,"");
                                    $http.put(global.ReportHost + `api/SessionContract/UpdateCuocVangLai?companyId=${$scope.$parent.cpnSelectSettings.itemSelect().Id}&idCuocXe=${$scope.funNewCuocVangLai.idCuocXe}&beginTime=${global.ChangeTime($scope.funNewCuocVangLai.beginTime)}&chenhLech=${$scope.funNewCuocVangLai.chenhLech}&serial=${$scope.$parent.carNameSelectting.itemSelect().Serial}`,null).then((rep:any)=>{
                                        if(rep.data.Status == 1){
                                            language.showLoading(true,"");
                                            bootbox.dialog({
                                                title: "Thông Báo",
                                                message: "Cập nhật cuốc vãng lai thành công!!!",
                                                buttons: {
                                                    confirm: {
                                                        label: "Đồng ý",
                                                        className: "btn-success",
                                                        callback:()=>{
                                                            $scope.$parent.btViewClick();
                                                        }
                                                    }
                                                }
                                            });
                                        }else{
                                            language.showLoading(true,"");
                                            bootbox.dialog({
                                                title: "Thông Báo",
                                                message: "Cập nhật cuốc vãng lai không thành công!!!",
                                                buttons: {
                                                    confirm: {
                                                        label: "Quay lại",
                                                        className: "btn-default",
                                                    }
                                                }
                                            });
                                        }
                                    });
                                }else{
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Vui lòng chọn xe!!!",
                                        buttons: {
                                            confirm: {
                                                label: "Quay lại",
                                                className: "btn-default",
                                            }
                                        }
                                    });
                                }

                            };

                            $('.nav-tabs a#cuocdanhap').on('shown.bs.tab', function (event:any){
                                $scope.gridCuocdanhap.refresh();
                            });

                            //note: sự kiện chọn nút xem
                            $scope.$parent.btViewClick = () => {
                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined){
                                    if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                        //nếu đội xe chọn tất cả
                                        if($scope.$parent.carNameSelectting.itemSelect() == undefined  || $scope.$parent.carNameSelectting.itemSelect().Serial == '-1'){
                                            //note: get dữ liệu theo công ty
                                            GetDataWithCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                        }else{
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end(),[$scope.$parent.carNameSelectting.itemSelect().Serial]);
                                        }
                                    }else{
                                        if($scope.$parent.carNameSelectting.itemSelect() == undefined  || $scope.$parent.carNameSelectting.itemSelect().Serial == '-1'){
                                            //note: get dữ liệu theo đội xe
                                            GetDataWithGroup($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.groupdeviceSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end());
                                        }else{
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end(),[$scope.$parent.carNameSelectting.itemSelect().Serial]);
                                        }
                                    }
                                }
                            };

                            //note: sự kiện chỉnh sửa cuốc hợp đồng
                            $scope.gridCuocdanhap.event.onEdit = (ne:any,ol:any)=>{
                                console.log(ne);
                                $scope.funEdit.diembatdau = ne.BeginLocation.Address;
                                $scope.funEdit.diemketthuc = ne.EndLocation.Address;
                                $scope.funEdit.TimeRun = ne.TimeRun;
                                $scope.funEdit.ChenhLechTime = ne.ChenhLechTime;
                                $scope.funEdit.KmRun = ne.KmRun;
                                $scope.funEdit.MoneyEveryKm = ne.MoneyEveryKm;
                                $scope.funEdit.MoneyRun = ne.MoneyRun;
                                $scope.funEdit.MoneyEveryTime = ne.MoneyEveryTime;
                                $scope.funEdit.CuocVangLai = ne.CuocVangLai;
                                $scope.funEdit.Cuoc2Chieu = ne.Cuoc2Chieu;
                                $scope.funEdit.BeginTime = AdapterDate(ne.BeginTime);
                                $scope.funEdit.Id = ne.Id;
                                $scope.funEdit.CompanyId = ne.CompanyId;

                                $scope.$apply();
                                $('#myModal_sessioncontract').modal('show');
                            };

                            $scope.gridCuocdanhap.event.onDelete = (old:any)=>{
                                bootbox.dialog({
                                    title: 'Thông báo',
                                    message: 'Bạn muốn xóa cuốc hợp đồng này!!',
                                    buttons: {
                                        cancel: {
                                            label: 'Quay lại'
                                        },
                                        confirm: {
                                            label: 'Xác nhận',
                                            className: "btn-danger",
                                            callback: () => {
                                                DeleteCuocHopDong(old.Id,old.CompanyId);
                                            }
                                        }
                                    },
                                    animate: true
                                });

                            };

                            $scope.editFormAccept = ( ) =>{
                                var req: any = {
                                    Id: $scope.funEdit.Id,
                                    BeginLocation: {
                                        Lat: 0,
                                        Lng: 0,
                                        Address: $scope.funEdit.diembatdau
                                    },
                                    EndLocation: {
                                        Lat: 0,
                                        Lng: 0,
                                        Address: $scope.funEdit.diemketthuc
                                    },
                                    BeginTime:global.ChangeTime($scope.funEdit.BeginTime),
                                    ChenhLechTime: $scope.funEdit.ChenhLechTime,
                                    TimeRun: $scope.funEdit.TimeRun,
                                    KmRun: $scope.funEdit.KmRun,
                                    MoneyRun: $scope.funEdit.MoneyRun,
                                    MoneyEveryKm: $scope.funEdit.MoneyEveryKm,
                                    MoneyEveryTime: $scope.funEdit.MoneyEveryTime,
                                    Cuoc2Chieu: $scope.funEdit.Cuoc2Chieu,
                                    CuocVangLai: $scope.funEdit.CuocVangLai,
                                    CompanyId:$scope.funEdit.CompanyId,
                                };
                                PutUpdateCuocHopDong(req);
                                $('#myModal_sessioncontract').modal('hide');
                            };

                            //khi chọn cuốc 2 chiều
                            $scope.cuoc2chieu = (value:any)=>{
                               // console.log(value);
                                // var tmp: any = {
                                //     money: $scope.funNew.MoneyRun,
                                //     km: $scope.funNew.KmRun
                                // };
                               if(value){
                                   // $scope.funNew.MoneyRun = $scope.funNew.MoneyRun * 2;
                                   $scope.funNew.KmRun = $scope.funNew.KmRun * 2;
                               }else{
                                   // $scope.funNew.MoneyRun = $scope.funNew.MoneyRun/ 2;
                                   $scope.funNew.KmRun = $scope.funNew.KmRun / 2;
                               }
                            };


                            //note: các hàm xử lý
                            /**
                             * Hàm tạo cuốc hợp đồng
                             * @param value
                             * @constructor
                             */
                            function AddCuocHopDong(value:any){
                                $http.post(global.ReportHost + `api/SessionContract/AddSessionContract`,value).then((rep:any)=>{
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            language.showLoading(true,"");
                                            $scope.$parent.btViewClick();
                                            ResetData();
                                            $.jGrowl('Tạo cuốc hợp đồng thành công!!!', {
                                                header: 'Thông báo!',
                                                theme: 'bg-success',
                                                position: 'center'
                                            });
                                            // bootbox.dialog({
                                            //     title: "Thông Báo",
                                            //     message: "Tạo cuốc hợp đồng thành công!!!",
                                            //     buttons: {
                                            //         confirm: {
                                            //             label: "Đồng ý",
                                            //             className: "btn-success",
                                            //             callback:()=>{
                                            //                 $scope.$parent.btViewClick();
                                            //                 ResetData();
                                            //             }
                                            //         }
                                            //     }
                                            // });
                                        }else{
                                            language.showLoading(true,"");
                                            $.jGrowl('Tạo cuốc hợp đồng không thành công', {
                                                header: 'Thông báo!',
                                                theme: 'bg-danger',
                                                position: 'center'
                                            });

                                        }
                                    }else{
                                        language.showLoading(true,"");
                                        $.jGrowl('Tạo cuốc hợp đồng không thành công', {
                                            header: 'Thông báo!',
                                            theme: 'bg-danger',
                                            position: 'center'
                                        });

                                    }

                                });
                            }

                            function GetDataWithCompanyId(companyId:number,beginTime:string,endTime:string ){
                                language.showLoading(false,"");
                                $http.get(global.ReportHost + `api/SessionContract/SessionContractByCompany?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridCuocdanhap.data = rep.data.SessionContractList;
                                        ConvertData($scope.gridCuocdanhap.data);
                                        $scope.gridCuocdanhap.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }

                                });
                            }
                            function GetDataWithSerial(companyId:number,beginTime:string,endTime:string,serial:any){
                                language.showLoading(false,"");
                                $http.post(global.ReportHost + `api/SessionContract/SessionContractBySerial?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`,serial).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridCuocdanhap.data = rep.data.SessionContractList;
                                        ConvertData($scope.gridCuocdanhap.data);
                                        $scope.gridCuocdanhap.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }
                            function GetDataWithGroup(companyId:number,groupId:number,beginTime:string,endTime:string){
                                language.showLoading(false,"");
                                $http.get(global.ReportHost + `api/SessionContract/SessionContractByGroup?companyId=${companyId}&groupId=${groupId}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any)=>{

                                    if(rep.data.Status == 1){
                                        $scope.gridCuocdanhap.data = rep.data.SessionContractList;
                                        ConvertData($scope.gridCuocdanhap.data);
                                        $scope.gridCuocdanhap.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }

                                });
                            }
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    val.Display = `<span class="text-danger">${val.SoHieu} - ${val.Bs}</span>`;
                                    if(val.BeginLocation !== null && val.EndLocation !== null){
                                        if(val.BeginLocation.Address !== null && val.EndLocation.Address !== null){
                                            val.Address = `<strong>Đón:</strong> ${val.BeginLocation.Address} </br> <strong>Trả : </strong> ${val.EndLocation.Address}`;
                                        }else{
                                            val.Address = '';
                                        }
                                    }
                                    // if(val.BeginLocation !== null){
                                    //     if(val.BeginLocation.Address !== null){
                                    //         val.AdrressStart = val.BeginLocation.Address;
                                    //     }else{
                                    //         val.AdrressStart = '';
                                    //     }
                                    // }
                                    // if(val.EndLocation !== null){
                                    //     if(val.EndLocation.Address !== null){
                                    //         val.AdrressEnd = val.EndLocation.Address;
                                    //     }else{
                                    //         val.AdrressEnd = '';
                                    //     }
                                    // }else{
                                    //     val.AdrressEnd = val.AdrressStart;
                                    // }
                                    val.BeginTime = val.BeginTime + '+07:00';
                                    // val.ChenhLechTime = global.AdapterDate(val.ChenhLechTime);
                                    if(val.Cuoc2Chieu){
                                        val.Cuoc2Chieu_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.Cuoc2Chieu_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(val.CuocVangLai){
                                        val.CuocVangLai_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.CuocVangLai_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(val.CuocKhopId == 0){
                                        val.CuocKhopId = 'Không khớp';
                                    }

                                });
                            }
                            //hàm chuyển thời gian
                            function AdapterDate(time:any){
                                var year:string = time.slice(0,4);
                                var month:string = time.slice(5,7);
                                var date:string = time.slice(8,10);
                                var min:string = time.slice(11,16);
                                var tt:any = min +' ' +date + '/' + month + '/' + year ;
                                return tt;
                            }
                            function ResetData(){
                                $scope.funNew = {
                                    KmRun: 0,
                                    MoneyEveryKm:0,
                                    MoneyEveryTime:0,
                                    MoneyRun:0,
                                    ChenhLechTime: 10,
                                    Cuoc2Chieu:false,
                                    CuocVangLai:false,

                                };
                                if($scope.startpointSelectting.itemSelect() !== undefined){
                                    $scope.startpointSelectting.setting.jqxComboBox('clearSelection');
                                }else{
                                    $('#' + `${$scope.startpointSelectting.id}`).val('');
                                    $scope.startpointSelectting.instance.searchString = '';
                                }
                                if($scope.endpointSelectting.itemSelect() !== undefined){
                                    $scope.endpointSelectting.setting.jqxComboBox('clearSelection');
                                }else{
                                    $('#' + `${$scope.endpointSelectting.id}`).val('');
                                    $scope.endpointSelectting.instance.searchString = '';
                                }
                                $scope.$apply();

                            }
                            function PutUpdateCuocHopDong(value:any){
                                language.showLoading(false,"");
                                $http.put(global.ReportHost + 'api/SessionContract/UpdateSessionContract',value).then((rep:any)=>{
                                    console.log(rep);
                                    if(rep.data.Status == 1){
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật cuốc hợp đồng thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Đồng ý",
                                                    className: "btn-success",
                                                    callback:()=>{
                                                        $scope.$parent.btViewClick();
                                                    }
                                                }
                                            }
                                        });
                                    }else{
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật cuốc hợp đồng không thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Quay lại",
                                                    className: "btn-default",
                                                }
                                            }
                                        });
                                    }
                                });
                            }


                            function DeleteCuocHopDong(sessionContractId:number,companyId:number ){
                                language.showLoading(false,"");
                                $http.delete(global.ReportHost + `api/SessionContract/DeleteSessionContract?sessionContractId=${sessionContractId}&companyId=${companyId}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        language.showLoading(true,"");

                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Xóa cuốc hợp đồng thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Đồng ý",
                                                    className: "btn-success",
                                                    callback:()=>{
                                                        $scope.$parent.btViewClick();
                                                    }
                                                }
                                            }
                                        });
                                    }else{
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật cuốc hợp đồng không thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Quay lại",
                                                    className: "btn-default",
                                                }
                                            }
                                        });
                                    }
                                });
                            }

                        }
                    }
                    AdModule.controller('SessioncontractController', SessioncontractController);
                }
            }
        }
    }
}