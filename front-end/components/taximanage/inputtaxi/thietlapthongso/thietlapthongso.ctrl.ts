/**
 * Created by NPPRO on 08/02/2017.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Inputtaxi{
                export module Thietlapthongso{
                    export interface Ithongsocapnhat{
                        Id?: number;
                        CompanyId?: number;
                        CompanyName?: string;
                        TimeoutHidenDevice: number;
                        TimeoutLostDevice: number;
                        SudungKmCo: boolean;
                        TinhDungXeLau: number;
                        KetToanCaAuto: boolean;
                        ChotCaAuto: boolean;
                        ChotCaAutoTime: string;
                        CachTinhTien: string;
                        CachKetToanCa: string;
                        Cuoc0Km: number;
                        // TruyenDieuHanh:boolean;
                        VatTinhSau:boolean;
                        Vat:number;
                        TinhTienAnChiaTheoBac:boolean;
                    }
                    export interface IThietlapthongsoScope{
                        $parent:IInputtaxiScope;
                        init:()=> void;
                        drCachTinhTienSelectSettings:App.Shared.Gui.IDropDown<any>;
                        drCachKettoancaSelectSettings:App.Shared.Gui.IDropDown<any>;
                        thongso:Ithongsocapnhat;
                        capnhatthongso:()=>any;
                    }
                    export class ThietlapthongsoController{
                        public static $inject = ['$scope','language','global', '$http','api','$q'];
                        constructor ($scope: IThietlapthongsoScope,public language:App.Shared.LanguageService, public global: App.Shared.GlobalService, public $http: ng.IHttpService,
                                     public api:App.Shared.Api.IApi, public $q: ng.IQService){
                            $scope.$parent.hidemodel = true;
                            $scope.$parent.hidecar = true;
                            $scope.$parent.hidedatetime = true;
                            $scope.$parent.hideComboboxGroupCar = true;
                            // $scope.$parent.$apply();


                            $scope.init = () => {
                                $("#ChotCaAutoTime").timepicker({
                                        // pickTime: true
                                    showMeridian:false,
                                    minuteStep:60,
                                    defaultTime: '00:00'
                                });
                            };
                            $scope.thongso = {
                                TimeoutHidenDevice:0,
                                TimeoutLostDevice:0,
                                SudungKmCo:false,
                                TinhDungXeLau:0,
                                KetToanCaAuto:true,
                                ChotCaAuto:true,
                                ChotCaAutoTime:"00:00",
                                CachTinhTien:'',
                                CachKetToanCa:'',
                                Cuoc0Km:0,
                                // TruyenDieuHanh: false,
                                VatTinhSau:false,
                                Vat:0,
                                TinhTienAnChiaTheoBac:false,
                            };

                            $scope.drCachTinhTienSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('dropCachtinhtien','Name','Id');
                            $scope.drCachTinhTienSelectSettings.setting.width = '100%';
                            $scope.drCachTinhTienSelectSettings.setting.placeHolder = 'Chọn cách tính tiền';
                            $scope.drCachTinhTienSelectSettings.setting.filterPlaceHolder = 'Tìm cách tính tiền...';
                            $scope.drCachTinhTienSelectSettings.setting.checkboxes = false;
                            $scope.drCachTinhTienSelectSettings.title = "";


                            $scope.drCachKettoancaSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('dropCachketoan','Name','Id');
                            $scope.drCachKettoancaSelectSettings.setting.width = '100%';
                            $scope.drCachKettoancaSelectSettings.setting.placeHolder = 'Chọn cách kết toán ca';
                            $scope.drCachKettoancaSelectSettings.setting.filterPlaceHolder = 'Tìm cách kết toán...';
                            $scope.drCachKettoancaSelectSettings.setting.checkboxes = false;
                            $scope.drCachKettoancaSelectSettings.title = "";


                            $scope.$parent.btViewClick = () =>{
                                language.showLoading(false,"");
                                $http.get(global.ReportHost + `api/Company/GetAllSettingByCompanyId/${$scope.$parent.cpnSelectSettings.itemSelect().Id}`).then((rep:any)=>{
                                    language.showLoading(true,"");
                                    let result:any = rep.data.Setting;
                                    $scope.thongso = {
                                        TimeoutHidenDevice:result.TimeoutHidenDevice,
                                        TimeoutLostDevice:result.TimeoutLostDevice,
                                        SudungKmCo:result.SudungKmCo,
                                        TinhDungXeLau:result.TinhDungXeLau,
                                        KetToanCaAuto:result.KetToanCaAuto,
                                        ChotCaAuto:result.ChotCaAuto,
                                        ChotCaAutoTime:result.ChotCaAutoTime,
                                        CachTinhTien:result.CachTinhTien,
                                        CachKetToanCa:result.CachKetToanCa,
                                        Cuoc0Km:result.Cuoc0Km,
                                        // TruyenDieuHanh:result.TruyenDieuHanh,
                                        VatTinhSau:result.VatTinhSau,
                                        Vat:result.Vat,
                                        TinhTienAnChiaTheoBac:result.TinhTienAnChiaTheoBac
                                    };
                                    // console.log(rep);
                                    $scope.drCachTinhTienSelectSettings.loadPromise(api.GetCachTinhTien($scope.$parent.cpnSelectSettings.itemSelect().Id),null,$q).then(()=>{
                                        $scope.drCachTinhTienSelectSettings.setItemDefault((m:any)=>m.Id === $scope.thongso.CachTinhTien);
                                    });
                                    $scope.drCachKettoancaSelectSettings.loadPromise(api.GetCachKettoanca($scope.$parent.cpnSelectSettings.itemSelect().Id),null,$q).then(()=>{
                                        $scope.drCachKettoancaSelectSettings.setItemDefault((m:any)=>m.Id === $scope.thongso.CachKetToanCa);
                                    });
                                });


                            };

                            // cập nhật thông số công ty
                            $scope.capnhatthongso = ()=>{
                                language.showLoading(false,"");
                                let req:Ithongsocapnhat = {
                                    Id:$scope.$parent.cpnSelectSettings.itemSelect().Id,
                                    CompanyId: $scope.$parent.cpnSelectSettings.itemSelect().Id,
                                    CompanyName: $scope.$parent.cpnSelectSettings.itemSelect().Name,
                                    TimeoutHidenDevice:$scope.thongso.TimeoutHidenDevice,
                                    TimeoutLostDevice:$scope.thongso.TimeoutLostDevice,
                                    SudungKmCo:$scope.thongso.SudungKmCo,
                                    TinhDungXeLau:$scope.thongso.TinhDungXeLau,
                                    KetToanCaAuto:$scope.thongso.KetToanCaAuto,
                                    ChotCaAuto:$scope.thongso.ChotCaAuto,
                                    ChotCaAutoTime:$scope.thongso.ChotCaAutoTime,
                                    CachTinhTien:$scope.drCachTinhTienSelectSettings.itemSelect().Id,
                                    CachKetToanCa:$scope.drCachKettoancaSelectSettings.itemSelect().Id,
                                    Cuoc0Km:$scope.thongso.Cuoc0Km,
                                    // TruyenDieuHanh:$scope.thongso.TruyenDieuHanh,
                                    VatTinhSau:$scope.thongso.VatTinhSau,
                                    Vat:$scope.thongso.Vat,
                                    TinhTienAnChiaTheoBac:$scope.thongso.TinhTienAnChiaTheoBac
                                };
                                CapNhatThongSoCongty(req);
                            };
                            function CapNhatThongSoCongty(data:any){
                                $http.put(global.ReportHost + 'api/Company/UpdateAllCompanySetting',data).then((rep:any)=>{
                                    language.showLoading(true,"");
                                    console.log(rep);
                                    if(rep.data.Status == 1){
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật thông số công ty thành công",
                                            buttons: {
                                                confirm: {
                                                    label: "Đồng ý",
                                                    className: "btn-success",
                                                }
                                            }
                                        });
                                    }else{
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật thông số công ty không thành công",
                                            buttons: {
                                                confirm: {
                                                    label: "Quay lại",
                                                    className: "btn-default",
                                                }
                                            }
                                        });
                                    }

                                });
                            }
                        }
                    }
                    AdModule.controller('ThietlapthongsoController', ThietlapthongsoController);
                }
            }
        }
    }
}