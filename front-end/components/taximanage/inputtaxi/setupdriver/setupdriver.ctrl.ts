/**
 * Created by phamn on 28/10/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Inputtaxi{
                export module Setupdriver{
                    export interface IEditSetupDriver{
                        Serial?:any;
                        Id?:any;
                        GroupId?:number;
                        GroupName?:string;
                        StateTaxi?:string;
                        DriverId?:number;
                        DriverName?:string;
                        TimeUpdate?:string;
                        Mnv?:string;
                    }
                    export interface ISetupdriverScope extends ng.IScope{
                        gridSetupdriver: App.Shared.Directives.GridviewControl;
                        gridJsonSetupdriver: App.Shared.Directives.GridviewControl;
                        $parent:IInputtaxiScope;
                        grouppopupSelectting: App.Shared.Gui.IDropDown<App.Shared.Entity.IGroup>;
                        drsettupdriverSelectting: App.Shared.Gui.IDropDown<App.Shared.Entity.IDriver>;
                        funNew:IEditSetupDriver;
                        newFormAccept:()=>any;
                        dataexceljson:any;
                        taobang:()=>any;
                        fileexcelmau:string;
                        lammoi:()=>any;
                    }
                    export class SetupdriverController{
                        public static $inject = ['$scope','language','global', '$http','api','$q'];
                        constructor ($scope: ISetupdriverScope,public language:App.Shared.LanguageService, public global: App.Shared.GlobalService, public $http: ng.IHttpService,
                                     public api:App.Shared.Api.IApi, public $q: ng.IQService){

                            $scope.$parent.hidemodel = true;
                            $scope.$parent.hidecar = true;
                            $scope.$parent.hidedatetime = true;
                            $scope.$parent.hideComboboxGroupCar = false;

                            $scope.fileexcelmau = 'http://'+window.location.host +'//ExelTemplate/THIET_LAP_TAI_XE_FILE_MAU.xls';
                            $scope.gridSetupdriver = new App.Shared.Directives.GridviewControl("gridSetupdriver");
                            $scope.gridSetupdriver.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridSetupdriver.filename_excel="Báo cáo thiết lập tài xế";
                            $scope.gridSetupdriver.columns = [];
                            $scope.gridSetupdriver.height = window.innerHeight - 150;
                            $scope.gridSetupdriver.columnsHeight = 40;
                            //note: bật tính năng tìm kiếm nâng cao
                            $scope.gridSetupdriver.isAltRows=true;
                            $scope.gridSetupdriver.isFilter=true;
                            $scope.gridSetupdriver.isFilterMode= 'simple';
                            $scope.gridSetupdriver.isExport= true;
                            //note: bật tính năng thêm, xóa , sửa
                            $scope.gridSetupdriver.behavior.isNew = false;
                            $scope.gridSetupdriver.behavior.isEdit = true;
                            $scope.gridSetupdriver.behavior.isDelete = false;
                            $scope.gridSetupdriver.columns.push({ text: 'STT', dataField: 'STT', type: 'string', align: 'center', width:'35px', pinned:true });
                            $scope.gridSetupdriver.columns.push({ text: 'Số tài - Biển số', dataField: 'Display', type: 'string', align: 'center', width:'100px',filterable:true,pinned:true   });
                            // $scope.gridSetupdriver.columns.push({ text: 'Số tài', dataField: 'Id', type: 'string', align: 'center', width:'100px',filterable:true });
                            $scope.gridSetupdriver.columns.push({ text: 'MNV', dataField: 'Mnv', type: 'string', align: 'center', width:'70px',filterable:true });
                            $scope.gridSetupdriver.columns.push({ text: 'Tài xế', dataField: 'DriverName', type: 'string', align: 'center', width:'120px',filterable:true  });
                            $scope.gridSetupdriver.columns.push({ text: 'Đội xe', dataField: 'GroupName', type: 'string', align: 'center', width:'90px',filterable:true  });
                            $scope.gridSetupdriver.columns.push({ text: 'Thời gian thiết lập', dataField: 'TimeUpdate', type: 'string', align: 'center', width:'auto',filterable:true});


                            $scope.gridJsonSetupdriver = new App.Shared.Directives.GridviewControl("gridJsonSetupdriver");
                            $scope.gridJsonSetupdriver.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridJsonSetupdriver.filename_excel="Báo cáo thiết lập tài xế";
                            $scope.gridJsonSetupdriver.columns = [];
                            $scope.gridJsonSetupdriver.height = window.innerHeight - 150;
                            $scope.gridJsonSetupdriver.columnsHeight = 40;
                            //note: bật tính năng tìm kiếm nâng cao
                            $scope.gridJsonSetupdriver.isAltRows=true;
                            $scope.gridJsonSetupdriver.isFilter=true;
                            $scope.gridJsonSetupdriver.isFilterMode= 'advanced';

                            $scope.gridJsonSetupdriver.columns.push({ text: 'STT', dataField: 'STT', type: 'string', align: 'center', width:'50px' });
                            $scope.gridJsonSetupdriver.columns.push({ text: 'Số tài', dataField: 'Id', type: 'string', align: 'center', width:'100px',filterable:true});
                            $scope.gridJsonSetupdriver.columns.push({ text: 'Mã nhân viên', dataField: 'Mnv', type: 'string', align: 'center', width:'200px',filterable:true});
                            $scope.gridJsonSetupdriver.columns.push({ text: 'Tài xế', dataField: 'DriverName', type: 'string', align: 'center', width:'200px',filterable:true  });
                            $scope.gridJsonSetupdriver.columns.push({ text: 'ID Công ty', dataField: 'CompanyId', type: 'string', align: 'center', width:'200px',filterable:true });
                            $scope.gridJsonSetupdriver.columns.push({ text: 'Tên Công ty', dataField: 'CompanyName', type: 'string', align: 'center', width:'auto',filterable:true });
                            //note: tạo mới drop down đội xe
                            // $scope.grouppopupSelectting = new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('dropGrouppopupSetupDriver','Name','Id');
                            // $scope.grouppopupSelectting.setting.width = '100%';
                            // $scope.grouppopupSelectting.setting.placeHolder = 'Chọn đội xe';
                            // $scope.grouppopupSelectting.setting.filterPlaceHolder = 'Tìm đội xe...';
                            // $scope.grouppopupSelectting.setting.checkboxes = false;
                            // $scope.grouppopupSelectting.title = "";


                            //note: tạo mới drop down đội xe
                            $scope.drsettupdriverSelectting = new App.Shared.Gui.DropDown<App.Shared.Entity.IDriver>('dropDriverSetupDriver','DisplayName','Id');
                            $scope.drsettupdriverSelectting.setting.width = '100%';
                            $scope.drsettupdriverSelectting.setting.placeHolder = 'Chọn tài xế';
                            $scope.drsettupdriverSelectting.setting.filterPlaceHolder = 'Tìm tài xế...';
                            $scope.drsettupdriverSelectting.setting.checkboxes = false;
                            $scope.drsettupdriverSelectting.title = "";

                            $scope.funNew = {
                                Serial:'',
                                Id:'',
                                StateTaxi:''
                            };


                            $scope.$parent.btViewClick = () => {
                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined){
                                    // $scope.grouppopupSelectting.load(api.GetAllGroupByCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id),null,true);
                                    $scope.drsettupdriverSelectting.load(api.GetAllDriverByCompany($scope.$parent.cpnSelectSettings.itemSelect().Id),null,false);
                                    if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                        GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id);
                                    }else{
                                        GetDataWithGroup($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.groupdeviceSelectSettings.itemSelect().Id);
                                    }
                                }
                            };

                            $scope.gridSetupdriver.event.onEdit =(ne:any,od:any)=>{
                                console.log(ne);
                                $scope.funNew.Serial = ne.Serial;
                                $scope.funNew.Id = ne.Id;
                                $scope.funNew.StateTaxi = ne.StateTaxi;
                                $scope.funNew.GroupName = ne.GroupName;
                                $scope.funNew.GroupId = ne.GroupId;
                                $scope.funNew.TimeUpdate = ne.TimeUpdate;
                                // $scope.grouppopupSelectting.setItemDefault(m=>m.Id == ne.GroupId);
                                $scope.drsettupdriverSelectting.setItemDefault(m=>m.Id == ne.DriverId);
                                $scope.$apply();
                                $('#myModal_setupdriver').modal('show');
                            };


                            $scope.newFormAccept = () => {
                                var req:any = [{
                                    Serial: $scope.funNew.Serial,
                                    Id: $scope.funNew.Id,
                                    CompanyId: $scope.$parent.cpnSelectSettings.itemSelect().Id,
                                    DriverId: $scope.drsettupdriverSelectting.itemSelect().Id,
                                    Mvn:$scope.funNew.Mnv,
                                    TimeUpdate: '2016-10-31T07:13:00.146Z'
                                }];
                                PutUpdateDriver(req,$scope.$parent.cpnSelectSettings.itemSelect().Id);
                                $('#myModal_setupdriver').modal('hide');
                            };

                            //note: xử lý file Excel
                            $("#xlf_setupdriver").on('change',function (event:any) {

                                // $scope.getPercentage = 0;

                                var oFile:any = event.target.files[0];
                                console.log(oFile);


                                // Create A File Reader HTML5
                                var reader = new FileReader();

                                // Ready The Event For When A File Gets Selected
                                reader.onload  = (e:any)=> {
                                    // language.showLoading(false,"");
                                    console.log(e);
                                    var data = e.target.result;
                                    var cfb = XLSX.read(data, {type: 'binary'});
                                    if(cfb == undefined){
                                        alert('file này không hợp lê');
                                        $scope.gridJsonSetupdriver.data = $scope.dataexceljson;
                                    }else{
                                        var wb = cfb;
                                        console.log(wb);
                                        // Loop Over Each Sheet

                                        wb.SheetNames.forEach(function(sheetName:any) {
                                            // Obtain The Current Row As CSV
                                            // var sCSV = XLS.utils.sheet_to_json(wb.Sheets['Sheet1']);
                                            $scope.dataexceljson = XLSX.utils.sheet_to_json(wb.Sheets['Sheet1']);
                                            // var oJS = XLS.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);

                                            // $("#my_file_output").html(sCSV);
                                            // console.log(sCSV);
                                            console.log($scope.dataexceljson);
                                            $scope.dataexceljson.shift();
                                            // console.log($scope.dataexceljson.shift());
                                            $scope.gridJsonSetupdriver.data = $scope.dataexceljson;


                                            // var x:string = '';
                                            // $scope.datacreategroup = [];
                                            // for(var i = 0;i<$scope.dataexceljson.length;i++){
                                            //     if(x !== $scope.dataexceljson[i].GroupName && $scope.dataexceljson[i].GroupName !== undefined){
                                            //         $scope.datacreategroup.push({Name:$scope.dataexceljson[i].GroupName, CompanyId: $scope.dataexceljson[i].companyId});
                                            //     }
                                            //     x = $scope.dataexceljson[i].GroupName;
                                            //     companyID = $scope.dataexceljson[i].companyId;
                                            // }
                                            // console.log($scope.datacreategroup);

                                            // AdapterData($scope.gridJsonDataPriceLine.data);
                                            // $scope.gridJsonDataPriceLine.refresh();

                                            $scope.$apply();
                                        });
                                    }


                                    // if($scope.dataexceljson.length > 0){
                                    //     language.showLoading(true,"");
                                    // }
                                };


                                // Tell JS To Start Reading The File.. You could delay this if desired

                                reader.readAsBinaryString(oFile);

                            });

                            $scope.taobang = () => {
                                language.showLoading(false,"");
                                var req:any= [];
                                if($scope.dataexceljson !== undefined){
                                    for(var i = 0; i< $scope.dataexceljson.length; i++){
                                        req.push({
                                            Id: $scope.dataexceljson[i].Id,
                                            CompanyId: $scope.dataexceljson[i].CompanyId,
                                            Mnv: $scope.dataexceljson[i].Mnv,
                                        });
                                    }
                                    ImportExcelThietLapTaixe(req,$scope.$parent.cpnSelectSettings.itemSelect().Id);
                                }else{
                                    language.showLoading(true,"");
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Vui lòng thêm file excel!!!",
                                        buttons: {
                                            cancle: {
                                                label: "Quay lại",
                                                className: "btn-default",

                                            }
                                        }
                                    });
                                }

                            };
                            $scope.lammoi = () =>{
                                //note: xử lý hàm làm mới khi nhập excel
                                console.log($("#xlf_setupdriver"));
                                $("#xlf_setupdriver").val('');
                                $scope.dataexceljson = [];
                            };

                            function GetDataWithCompany(companyId:number){
                                $http.get(global.ReportHost + `api/InputTaxi/SetupDriverByCompany?companyId=${companyId}`).then((rep:any)=>{
                                     if(rep.data.Status == 1){
                                         $scope.gridSetupdriver.data = rep.data.SetupDriverTranfers;
                                         ConvertData($scope.gridSetupdriver.data);
                                         $scope.gridSetupdriver.refresh();
                                     }else{
                                         console.log(rep.data.Description);
                                     }
                                });
                            }
                            function GetDataWithGroup(companyId:number,groupId:number ){
                                $http.get(global.ReportHost + `api/InputTaxi/SetupDriverByGroupId?companyId=${companyId}&groupId=${groupId}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridSetupdriver.data = rep.data.SetupDriverTranfers;
                                        ConvertData($scope.gridSetupdriver.data);
                                        $scope.gridSetupdriver.refresh();
                                    }else{
                                        console.log(rep.data.Description);
                                    }
                                });
                            }

                            function PutUpdateDriver(req:any,companyId:number ){
                                $http.put(global.ReportHost + `api/InputTaxi/UpdateSetupDriver?companyId=${companyId}`,req).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật thiết lập tài xế thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Đồng ý",
                                                    className: "btn-success",
                                                    callback:()=>{
                                                        $scope.$parent.btViewClick();
                                                    }
                                                }
                                            }
                                        });
                                    }else{
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật thiết lập tài xế không thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Quay lại",
                                                    className: "btn-default",
                                                }
                                            }
                                        });
                                    }
                                });
                            }

                            function ImportExcelThietLapTaixe(value:any,companyId:number){
                                $http.put(global.ReportHost + `api/InputTaxi/ImportExcelSetupDriver?companyId=${companyId}`,value).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        language.showLoading(true,"");
                                        if(rep.data.Description !== ""){
                                            bootbox.dialog({
                                                title: "Thông Báo",
                                                message: `Thiết lập tài hoàn tất. Đã xảy ra lỗi thiết lập ở các tài có MNV sau: <span class="text-danger">${rep.data.Description}</span>`,

                                            });
                                        }else{
                                            bootbox.dialog({
                                                title: "Thông Báo",
                                                message: "Thiết lập tất cả các tài xế trong file thành công!!!",
                                                buttons: {
                                                    confirm: {
                                                        label: "Đồng ý",
                                                        className: "btn-success",
                                                        callback:()=>{
                                                            $scope.$parent.btViewClick();
                                                        }
                                                    }
                                                }
                                            });
                                        }

                                    }else{
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Thiết lập tài không thành công. Xin vui lòng liên hệ quản trị viên",
                                            buttons: {
                                                confirm: {
                                                    label: "Đồng ý",
                                                    className: "btn-success",
                                                    callback:()=>{
                                                        $scope.$parent.btViewClick();
                                                    }
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    val.TimeUpdate = global.AdapterDate(val.TimeUpdate);
                                    val.Display = `${val.Id} - ${val.Bs}`;
                                });
                            }

                        }
                    }
                    AdModule.controller('SetupdriverController', SetupdriverController);
                }
            }
        }
    }
}