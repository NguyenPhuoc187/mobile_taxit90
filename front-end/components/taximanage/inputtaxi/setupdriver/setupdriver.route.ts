module App{
    export module Components{
        export module Taximanage{
            export module Inputtaxi{
                export class SetupdriverRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper,global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'inputtaxi.setupdriver',
                            config: {
                                controller: Taximanage.Inputtaxi.Setupdriver.SetupdriverController,
                                templateUrl: '/components/taximanage/inputtaxi/setupdriver/setupdriver.html'+`?v=${global.Version}`,
                                url: '/thiet-lap-tai-xe',
                                tag:{
                                    title:'Thiết lập tài xế',
                                    icon:'icon-users2',
                                    groupId:'taxi',
                                    index:4
                                }
                            }
                        });
                    }
                }
                AdModule.run(SetupdriverRouteModel);
            }
        }
    }
}