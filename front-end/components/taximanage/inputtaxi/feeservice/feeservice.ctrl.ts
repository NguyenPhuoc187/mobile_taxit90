/**
 * Created by phamn on 28/10/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Inputtaxi{
                export module Feeservice{
                    export interface INewFeeService{
                        Id?:number;
                        Name: string;
                        Money:number;
                        TheoNgay: boolean;
                        CongTyChiu: boolean;
                        CompanyId?: number;
                        GroupId?: number;
                        CompanyName?: string;
                        GroupName?: string;
                    }
                    export interface IFeeserviceScope extends ng.IScope{
                        $parent: IInputtaxiScope;
                        gridFeeservice: App.Shared.Directives.GridviewControl;
                        grouppopupSelectting:App.Shared.Gui.IDropDown<App.Shared.Entity.IGroup>;
                        newFormAccept:()=>any;
                        funNew: INewFeeService;
                    }
                    export class FeeserviceController{
                        public static $inject = ['$scope','language','global', '$http','api','$q'];
                        constructor ($scope: IFeeserviceScope,public language:App.Shared.LanguageService, public global: App.Shared.GlobalService, public $http: ng.IHttpService,
                                     public api:App.Shared.Api.IApi, public $q: ng.IQService){
                            $scope.$parent.hidemodel = true;
                            $scope.$parent.hidecar = true;
                            $scope.$parent.hidedatetime = true;
                            $scope.$parent.hideComboboxGroupCar = false;

                            $scope.gridFeeservice = new App.Shared.Directives.GridviewControl("gridFeeservice");
                            $scope.gridFeeservice.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridFeeservice.filename_excel="Báo cáo bảng phí dịch vụ";
                            $scope.gridFeeservice.columns = [];
                            $scope.gridFeeservice.height = window.innerHeight - 150;
                            $scope.gridFeeservice.columnsHeight = 40;
                            //note: bật tính năng tìm kiếm nâng cao
                            $scope.gridFeeservice.isAltRows=true;
                            $scope.gridFeeservice.isFilter=true;
                            $scope.gridFeeservice.isFilterMode= 'advanced';
                            //note: bật tính năng thêm, xóa , sửa
                            $scope.gridFeeservice.behavior.isNew = true;
                            $scope.gridFeeservice.behavior.isEdit = false;
                            $scope.gridFeeservice.behavior.isDelete = true;
                            $scope.gridFeeservice.columns.push({ text: 'STT', dataField: 'STT', type: 'string', align: 'center', width:'50px' });
                            $scope.gridFeeservice.columns.push({ text: 'Id', dataField: 'Id', type: 'string', align: 'center', width:'50px',hidden:true });
                            $scope.gridFeeservice.columns.push({ text: 'Tên dịch vụ', dataField: 'Name', type: 'string', align: 'center', width:'200px',filterable:true,});
                            $scope.gridFeeservice.columns.push({ text: 'Tiền', dataField: 'Money', type: 'number', align: 'center', width:'150px',filterable:true, cellsFormat:'n'  });
                            $scope.gridFeeservice.columns.push({ text: 'Tiền tính theo ngày', dataField: 'TheoNgay_show', type: 'string', align: 'center', width:'150px',filterable:true  });
                            $scope.gridFeeservice.columns.push({ text: 'Tiền công ty chịu', dataField: 'CongTyChiu_show', type: 'string', align: 'center', width:'150px',filterable:true, });
                            $scope.gridFeeservice.columns.push({ text: 'Đội xe', dataField: 'GroupName', type: 'string', align: 'center', width:'auto',filterable:true  });


                            //note: tạo mới drop down đội xe
                            $scope.grouppopupSelectting = new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('dropGrouppopupFeeService','Name','Id');
                            $scope.grouppopupSelectting.setting.width = '100%';
                            $scope.grouppopupSelectting.setting.placeHolder = 'Chọn đội xe';
                            $scope.grouppopupSelectting.setting.filterPlaceHolder = 'Tìm đội xe...';
                            $scope.grouppopupSelectting.setting.checkboxes = false;
                            $scope.grouppopupSelectting.title = "";


                            //note: thưc hiện tạo mới phí dịch vụ
                            $scope.gridFeeservice.event.onNew = ()=>{
                                $scope.funNew = {
                                    Name: '',
                                    Money:0,
                                    TheoNgay: false,
                                    CongTyChiu: false,
                                };
                                $scope.grouppopupSelectting.load(api.GetAllGroupByCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id),null,true);
                                $('#myModal_feeservice').modal('show');
                            };

                            //note: thực hiện xóa phí dịch vụ
                            $scope.gridFeeservice.event.onDelete = (ne:any)=>{
                                bootbox.dialog({
                                    title: "Thông Báo",
                                    message: "Bạn muốn xóa bảng giá tuyến này !!!",
                                    buttons: {
                                        confirm: {
                                            label: "Đồng ý",
                                            className: "btn-danger",
                                            callback:()=>{
                                                DeleteDataPhiDichVu(ne.Id,ne.CompanyId);
                                            }
                                        },
                                        cancle:{
                                            label:'Hủy',
                                            className: "btn-default",
                                        }
                                    }
                                });
                            };

                            //note: thực hiện xử lý sự kiện submit form tạo mới
                            $scope.newFormAccept = () =>{
                                language.showLoading(false,"");
                                var req:any = {
                                    Name: $scope.funNew.Name,
                                    Money: $scope.funNew.Money,
                                    TheoNgay: $scope.funNew.TheoNgay,
                                    CongTyChiu: $scope.funNew.CongTyChiu,
                                    CompanyId: $scope.$parent.cpnSelectSettings.itemSelect().Id,
                                    GroupId: $scope.grouppopupSelectting.itemSelect().Id,
                                    CompanyName: $scope.$parent.cpnSelectSettings.itemSelect().Name,
                                    GroupName: $scope.grouppopupSelectting.itemSelect().Name
                                };
                                $http.post(global.ReportHost + 'api/InputTaxi/AddFeeService',req).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Tạo bảng phí dịch vụ thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Đồng ý",
                                                    className: "btn-success",
                                                    callback:()=>{
                                                        $scope.$parent.btViewClick();
                                                    }
                                                }
                                            }
                                        });
                                    }else{
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Tạo bảng phí dịch vụ không thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Quay lại",
                                                    className: "btn-default",
                                                }
                                            }
                                        });
                                    }
                                });
                                $('#myModal_feeservice').modal('hide');
                            };

                            //note: xử lý sự kiện view bảng phí dịch vụ
                            $scope.$parent.btViewClick = ( ) => {
                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined){
                                    if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                        GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id);
                                    }else{
                                        GetDataWithGroup($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.groupdeviceSelectSettings.itemSelect().Id);
                                    }
                                }
                            };

                            /**
                             * Get data phí dịch vụ với công ty
                             * @param companyId: id công ty muốn lấy dữ liệu
                             * @constructor
                             */

                            function GetDataWithCompany(companyId:number){
                                language.showLoading(false,"");
                                $http.get(global.ReportHost + `api/InputTaxi/FeeServiceByCompany?companyId=${companyId}`).then((rep:any)=>{
                                   if(rep.data.Status == 1){
                                       $scope.gridFeeservice.data = rep.data.FeeServiceList;
                                       ConvertData($scope.gridFeeservice.data);
                                       $scope.gridFeeservice.refresh();
                                       language.showLoading(true,"");
                                   }else{
                                       console.debug(rep.data.Description);
                                       language.showLoading(true,"");
                                   }
                                });
                            }

                            /**
                             * Get data phí dịch vụ với
                             * @param companyId: giá trị id công ty
                             * @param groupId: giá trị id đội xe
                             * @constructor
                             */

                            function GetDataWithGroup(companyId:number, groupId: number){
                                language.showLoading(false,"");
                                $http.get(global.ReportHost + `api/InputTaxi/FeeServiceByGroup?companyId=${companyId}&deviceGroupId=${groupId}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridFeeservice.data = rep.data.FeeServiceList;
                                        ConvertData($scope.gridFeeservice.data);
                                        $scope.gridFeeservice.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }
                            function DeleteDataPhiDichVu(feeServiceId:number, companyId:number){
                                language.showLoading(true,"");
                                $http.delete(global.ReportHost + `api/InputTaxi/DeleteFeeService?feeServiceId=${feeServiceId}&companyId=${companyId}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Xóa bảng phí dịch vụ thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Đồng ý",
                                                    className: "btn-success",
                                                    callback:()=>{
                                                        $scope.$parent.btViewClick();
                                                    }
                                                }
                                            }
                                        });
                                    }else{
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Xóa bảng phí dịch vụ không thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Quay lại",
                                                    className: "btn-default",
                                                }
                                            }
                                        });
                                    }
                                });
                            }

                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    if(val.TheoNgay){
                                        val.TheoNgay_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.TheoNgay_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(val.CongTyChiu){
                                        val.CongTyChiu_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.CongTyChiu_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                });
                            }
                        }
                    }
                    AdModule.controller('FeeserviceController', FeeserviceController);
                }
            }
        }
    }
}