/**
 * Created by phamn on 26/10/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Inputtaxi{
                export class InputtaxiRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper,global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'inputtaxi',
                            config: {
                                controller: Taximanage.Inputtaxi.InputtaxiController,
                                templateUrl: '/components/taximanage/inputtaxi/inputtaxi.html'+`?v=${global.Version}`,
                                url: '/nhap-lieu',
                                tag:{
                                    title:'Nhập liệu',
                                    icon:'icon-pencil3',
                                    groupId:'taxi',
                                    index:6
                                },
                                show:false
                            }
                        });
                        sysRouter.configureStates({
                            name: 'inputtaxi.priceline',
                            config: {
                                controller: Taximanage.Inputtaxi.Priceline.PricelineController,
                                templateUrl: '/components/taximanage/inputtaxi/priceline/priceline.html'+`?v=${global.Version}`,
                                url: '/bang-gia-tuyen',
                                parent:'inputtaxi',
                                tag:{
                                    title:'Bảng giá tuyến',
                                    icon:'icon-calculator2',
                                    groupId:'taxi',
                                    index:1
                                }
                            }
                        });
                        sysRouter.configureStates({
                            name: 'inputtaxi.feeservice',
                            config: {
                                controller: Taximanage.Inputtaxi.Feeservice.FeeserviceController,
                                templateUrl: '/components/taximanage/inputtaxi/feeservice/feeservice.html'+`?v=${global.Version}`,
                                url: '/phi-dich-vu',
                                parent:'inputtaxi',
                                tag:{
                                    title:'Phí dịch vụ',
                                    icon:'icon-cash2',
                                    groupId:'taxi',
                                    index:2
                                }
                            }
                        });
                        sysRouter.configureStates({
                            name: 'inputtaxi.percentbenefit',
                            config: {
                                controller: Taximanage.Inputtaxi.Percentbenefit.PercentbenefitController,
                                templateUrl: '/components/taximanage/inputtaxi/percentbenefit/percentbenefit.html'+`?v=${global.Version}`,
                                url: '/ti-le-an-chia',
                                parent:'inputtaxi',
                                tag:{
                                    title:'Tỉ lệ ăn chia',
                                    icon:'icon-percent',
                                    groupId:'taxi',
                                    index:3
                                }
                            }
                        });
                        // sysRouter.configureStates({
                        //     name: 'inputtaxi.setupdriver',
                        //     config: {
                        //         controller: Taximanage.Inputtaxi.Setupdriver.SetupdriverController,
                        //         templateUrl: '/components/taximanage/inputtaxi/setupdriver/setupdriver.html'+`?v=${global.Version}`,
                        //         url: '/thiet-lap-tai-xe',
                        //         parent:'inputtaxi',
                        //         tag:{
                        //             title:'Thiết lập tài xế',
                        //             icon:'icon-users2',
                        //             groupId:'taxi',
                        //             index:4
                        //         }
                        //     }
                        // });
                        sysRouter.configureStates({
                            name: 'inputtaxi.sessioncontract',
                            config: {
                                controller: Taximanage.Inputtaxi.Sessioncontract.SessioncontractController,
                                templateUrl: '/components/taximanage/inputtaxi/sessioncontract/sessioncontract.html'+`?v=${global.Version}`,
                                url: '/cuoc-hop-dong',
                                parent:'inputtaxi',
                                tag:{
                                    title:'Cuốc hợp đồng',
                                    icon:'icon-copy',
                                    groupId:'taxi',
                                    index:5
                                }
                            }
                        });
                        sysRouter.configureStates({
                            name: 'inputtaxi.thietlapthongso',
                            config: {
                                controller: Taximanage.Inputtaxi.Thietlapthongso.ThietlapthongsoController,
                                templateUrl: '/components/taximanage/inputtaxi/thietlapthongso/thietlapthongso.html'+`?v=${global.Version}`,
                                url: '/thiet-lap-thong-so',
                                parent:'inputtaxi',
                                tag:{
                                    title:'Thiết lập thông số',
                                    icon:'icon-cog2',
                                    groupId:'taxi',
                                    index:6
                                }
                            }
                        });
                    }
                }
                AdModule.run(InputtaxiRouteModel);
            }
        }
    }
}