/**
 * Created by phamn on 26/10/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export interface IInputtaxiScope extends ng.IScope{
                cpnSelectSettings:App.Shared.Gui.IDropDown<App.Shared.Entity.ICompany>;
                groupdeviceSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IGroup>;
                modelNameSelectting:App.Shared.Gui.IDropDown<any>;
                viewData:string;
                btViewClick:() => any;
                hidemodel:boolean;
                hidecar: boolean;
                hidedatetime:boolean;
                hideComboboxGroupCar:boolean;
                carNameSelectting: App.Shared.Gui.IDropDown<App.Shared.Entity.IDevice>;
                selectcar:(item:any)=>any;
                dtTimeSelect:App.Shared.Gui.IDateTimeControl;
            }
            export module Inputtaxi{
                export class InputtaxiController{
                    public static $inject = ['$scope','language','global', '$http','api','$q'];
                    constructor ($scope: IInputtaxiScope,public language:App.Shared.LanguageService, public global: App.Shared.GlobalService, public $http: ng.IHttpService,
                                 public api:App.Shared.Api.IApi, public $q: ng.IQService){
                        $scope.hidemodel = true;
                        $scope.hidecar = true;
                        $scope.hidedatetime = true;
                        $scope.hideComboboxGroupCar = false;
                        $scope.viewData=language.gridLg().viewData;

                        //Khai báo lịch
                        $scope.dtTimeSelect=new App.Shared.Gui.DateTimeControl('dtTimeSelectinputtaxiMaster');
                        $scope.dtTimeSelect.titleTime=language.gridLg().titleTime;

                        // Khai báo mới combobox truyền vào 3 thông số : tên định dạng của combobox, hiển thị tên trường, giá trị chọn
                        $scope.cpnSelectSettings =new App.Shared.Gui.DropDown<App.Shared.Entity.ICompany>('cbCompanyInputTaxiMaster','Display','Id');
                        // Tiêu đề trên combobox
                        $scope.cpnSelectSettings.title= language.gridLg().company;
                        // Set chiều dài cho combobox
                        $scope.cpnSelectSettings.setting.width = '100%';
                        $scope.cpnSelectSettings.setting.checkboxes = false;
                        $scope.cpnSelectSettings.setting.filterPlaceHolder = 'Tìm công ty...';
                        $scope.cpnSelectSettings.setting.placeHolder = 'Chọn công ty';

                        // Khai báo combobox đội xe
                        $scope.groupdeviceSelectSettings=new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('dropdownGroupInputTaxiMaster','Name','Id');
                        $scope.groupdeviceSelectSettings.title=language.gridLg().group;
                        $scope.groupdeviceSelectSettings.setting.width = '100%';
                        $scope.groupdeviceSelectSettings.setting.checkboxes = false;
                        $scope.groupdeviceSelectSettings.setting.filterPlaceHolder = 'Tìm đội xe...';
                        $scope.groupdeviceSelectSettings.setting.placeHolder = 'Chọn đội xe';

                        $scope.modelNameSelectting=new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('dropdownModelNameInputTaxiMaster','Name','Name');
                        $scope.modelNameSelectting.title='Loại xe';
                        $scope.modelNameSelectting.setting.width = '100%';
                        $scope.modelNameSelectting.setting.checkboxes = false;
                        $scope.modelNameSelectting.setting.filterPlaceHolder = 'Tìm loại xe...';
                        $scope.modelNameSelectting.setting.placeHolder = 'Chọn loại xe';

                        $scope.carNameSelectting = new App.Shared.Gui.DropDown<App.Shared.Entity.IDevice>('dropCarNameInputTaxiMaster','DisplayId','Serial');
                        $scope.carNameSelectting.title=language.gridLg().car;
                        $scope.carNameSelectting.setting.placeHolder = 'Chọn xe';
                        $scope.carNameSelectting.setting.width = '100%';
                        $scope.carNameSelectting.setting.checkboxes = false;

                        // $scope.cpnSelectSettings.load(api.GetAllCompanyByType(),null,true);

                        $scope.cpnSelectSettings.loadPromise(api.GetAllCompanyByType(), null,$q).then(()=>{
                            if(localStorage.getItem('companyIdtaxi') !== '' && localStorage.getItem('companyIdtaxi') !== null){

                                let companyidtaxi:any = localStorage.getItem('companyIdtaxi');
                                $scope.cpnSelectSettings.setItemDefault((m:any)=>m.Id === companyidtaxi*1);
                            }else{
                                $scope.cpnSelectSettings.setting.jqxDropDownList('selectIndex',0);
                            }
                        });

                        $scope.cpnSelectSettings.customEvent.onSelectOnItem=(index:number,val:App.Shared.Entity.ICompany)=>{
                            localStorage.setItem('companyIdtaxi',`${val.Id}`,);
                            var first=Array<App.Shared.Entity.IGroup>();
                            // đẩy 1 group làm thông tin chọn tất cả
                            first.push( {
                                Id: -1,
                                Name: 'Tất cả',
                                CompanyId:val.Id
                            });

                            var first_model=Array<any>();
                            // đẩy 1 group làm thông tin chọn tất cả
                            first_model.push( {
                                Name: 'Tất cả',
                            });

                            $http.get(global.ReportHost + `api/DeviceModel/GetAllByCompany?companyId=${val.Id}`).then((rep:any)=>{
                               if(rep !== undefined){
                                   if(rep.data.Status == 1){
                                          // var result:any = [];
                                          // $.each(rep.data.DeviceModelList,(ind:number,val:any)=>{
                                          //    if(val.IsMayChuTaxi){
                                          //        result.push(val);
                                          //    }
                                          // });
                                       $scope.modelNameSelectting.loadlocaldata_dropuser(rep.data.DeviceModelList,null,true,first_model);
                                   }
                               }
                            });

                            // api.GetAllModelCar().then((data:any)=>{
                            //    // console.log(data);
                            //    var result:any = [];
                            //    $.each(data,(ind:number,val:any)=>{
                            //       if(val.IsMayChuTaxi){
                            //           result.push(val);
                            //       }
                            //    });
                            //    // $scope.modelNameSelectting.loadlocaldata_dropuser(result,null,true,first_model);
                            // });

                            // $scope.modelNameSelectting.load(api.GetAllModelCar(),first_model,true);
                            $scope.groupdeviceSelectSettings.load(api.GetAllGroupByCompanyId(val.Id),first,true);
                        };
                        $scope.groupdeviceSelectSettings.customEvent.onSelectOnItem = (ind:number,val_group: App.Shared.Entity.IGroup)=>{
                            console.log(val_group);
                            var firstdevice=Array<App.Shared.Entity.IDevice>();
                            firstdevice.push( {
                                Serial: '-1',
                                DisplayId: 'Tất cả',
                                GroupId:0,
                                CompanyId:0,
                                ModelName:'',
                            });
                            if(val_group.Id == -1){// hiển thị tất cả các xe trong công ty
                                $scope.carNameSelectting.load(api.GetAllDeviceByCompanyId(val_group.CompanyId),firstdevice,false);

                            }
                            else{// hiểm thị các xe trong đội xe đã chọn

                                $scope.carNameSelectting.load(api.GetAllDeviceByGroupId(val_group.CompanyId,val_group.Id),firstdevice,false);

                            }
                        };
                        $scope.carNameSelectting.customEvent.onSelectOnItem=(ind:number, val:App.Shared.Entity.IDevice) => {
                            console.log(val);
                            if($scope.selectcar !== undefined || $scope.selectcar !== null){
                                $scope.selectcar(val);
                            }
                        };
                    }

                }
                AdModule.controller('InputtaxiController', InputtaxiController);
            }
        }
    }
}