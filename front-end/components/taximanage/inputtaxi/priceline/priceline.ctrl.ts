/**
 * Created by phamn on 27/10/2016.
 */
/// <reference path="../../../../shared/library.ts" />
module App{
    export module Components{
        export module Taximanage{
            export module Inputtaxi{
                export module Priceline{
                    export interface ILocation{
                        Lat?: number;
                        Lng?: number;
                        Address: string;
                    }
                    export interface INewBangGiaTuyen{
                        Id?:number;
                        ModelName: string;
                        BeginLocation?: ILocation;
                        EndLocation?:ILocation;
                        TimeRun: string;
                        KmRun: number;
                        MoneyRun: number;
                        MoneyEveryKm: number;
                        MoneyEveryTime: number;
                        CompanyId: number;
                        CompanyName: string;
                        DeviceGroup: number;
                        GroupName: string;
                    }
                    export interface IPricelineScope extends ng.IScope{
                        gridJsonDataPriceLine: App.Shared.Directives.GridviewControl;
                        dataexceljson: any;
                        lammoi:()=>any;
                        taobang:()=>any;
                        gridPriceline: App.Shared.Directives.GridviewControl;
                        $parent: IInputtaxiScope;
                        newFormAccept:(value:boolean)=>any;
                        funNew: INewBangGiaTuyen;
                        isNew:boolean;
                        modelNameSelectting:App.Shared.Gui.IDropDown<any>;
                        grouppopupSelectting:App.Shared.Gui.IDropDown<App.Shared.Entity.IGroup>;
                        fileexcelmau:string;
                        loithongbao:string;
                    }
                    export class PricelineController{
                        public static $inject = ['$scope','language','global', '$http','api','$q'];
                        constructor ($scope: IPricelineScope,public language:App.Shared.LanguageService, public global: App.Shared.GlobalService, public $http: ng.IHttpService,
                                     public api:App.Shared.Api.IApi, public $q: ng.IQService){
                            $scope.$parent.hidemodel = false;

                            $scope.$parent.hidecar = true;
                            $scope.$parent.hidedatetime = true;
                            $scope.$parent.hideComboboxGroupCar = false;
                            $scope.fileexcelmau = 'http://'+window.location.host +'//ExelTemplate/BANG_GIA_TUYEN_FILE_MAU.xls';
                            $scope.gridJsonDataPriceLine = new App.Shared.Directives.GridviewControl("gridJsonDataPriceLine");
                            $scope.gridJsonDataPriceLine.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridJsonDataPriceLine.filename_excel="Báo cáo số lượng cuốc xe trong điểm";
                            $scope.gridJsonDataPriceLine.columns = [];
                            $scope.gridJsonDataPriceLine.height = window.innerHeight - 200;
                            $scope.gridJsonDataPriceLine.columnsHeight = 30;
                            //bật tính năng lọc dữ liệu
                            $scope.gridJsonDataPriceLine.isAltRows=true;
                            $scope.gridJsonDataPriceLine.isFilter=true;
                            $scope.gridJsonDataPriceLine.isFilterMode= 'advanced';

                            $scope.gridJsonDataPriceLine.columns.push({ text: 'STT', dataField: 'STT', type: 'string', align: 'center', width:'50px' });
                            $scope.gridJsonDataPriceLine.columns.push({ text: 'Điểm xuất phát', dataField: 'AddressStart', type: 'string', align: 'center', width:'auto',filterable:true  });
                            $scope.gridJsonDataPriceLine.columns.push({ text: 'Điểm kết thúc', dataField: 'AddressEnd', type: 'string', align: 'center', width:'auto',filterable:true  });
                            $scope.gridJsonDataPriceLine.columns.push({ text: 'Thời gian dự kiến', dataField: 'TimeRun', type: 'string', align: 'center', width:'100px' });
                            $scope.gridJsonDataPriceLine.columns.push({ text: 'Km dự kiến', dataField: 'KmRun', type: 'number', align: 'center', width:'auto',filterable:true,cellsFormat: 'n' });
                            $scope.gridJsonDataPriceLine.columns.push({ text: 'Tiền dự kiến', dataField: 'MoneyRun', type: 'number', align: 'center', width:'auto',filterable:true, cellsFormat: 'n' });
                            $scope.gridJsonDataPriceLine.columns.push({ text: 'Tiền quá mỗi km', dataField: 'MoneyEveryKm', type: 'number', align: 'center', width:'auto',filterable:true,cellsFormat: 'n'});
                            $scope.gridJsonDataPriceLine.columns.push({ text: 'Tiền quá mỗi giờ', dataField: 'MoneyEveryTime', type: 'number', align: 'center', width:'auto',filterable:true,cellsFormat: 'n'});
                            $scope.gridJsonDataPriceLine.columns.push({ text: 'ID Công ty', dataField: 'CompanyId', type: 'string', align: 'center', width:'auto',filterable:true  });
                            // $scope.gridJsonDataPriceLine.columns.push({ text: 'Tên công ty', dataField: 'CompanyName', type: 'string', align: 'center', width:'150px',filterable:true  });
                            $scope.gridJsonDataPriceLine.columns.push({ text: 'ID đội xe', dataField: 'DeviceGroup', type: 'string', align: 'center', width:'auto',filterable:true  });
                            $scope.gridJsonDataPriceLine.columns.push({ text: 'Tên đội xe', dataField: 'GroupName', type: 'string', align: 'center', width:'auto',filterable:true  });
                            $scope.gridJsonDataPriceLine.columns.push({ text: 'Loại xe', dataField: 'ModelName', type: 'string', align: 'center', width:'auto',filterable:true  });



                            $scope.gridPriceline = new App.Shared.Directives.GridviewControl("gridPriceline");
                            $scope.gridPriceline.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridPriceline.filename_excel="Báo cáo bảng giá tuyến";
                            $scope.gridPriceline.columns = [];
                            $scope.gridPriceline.height = window.innerHeight - 150;
                            $scope.gridPriceline.columnsHeight = 40;
                            //note: bật tính năng tìm kiếm nâng cao
                            $scope.gridPriceline.isAltRows=true;
                            $scope.gridPriceline.isFilter=true;
                            $scope.gridPriceline.isFilterMode= 'advanced';
                            //note: bật tính năng thêm, xóa , sửa
                            $scope.gridPriceline.behavior.isNew = true;
                            $scope.gridPriceline.behavior.isEdit = true;
                            $scope.gridPriceline.behavior.isDelete = true;
                            $scope.gridPriceline.columns.push({ text: 'STT', dataField: 'STT', type: 'string', align: 'center', width:'50px' });
                            $scope.gridPriceline.columns.push({ text: 'Id', dataField: 'Id', type: 'string', align: 'center', width:'50px',hidden:true });
                            $scope.gridPriceline.columns.push({ text: 'Điểm xuất phát', dataField: 'AddressStart', type: 'string', align: 'center', width:'auto',filterable:true  });
                            $scope.gridPriceline.columns.push({ text: 'Điểm kết thúc', dataField: 'AddressEnd', type: 'string', align: 'center', width:'auto',filterable:true  });
                            $scope.gridPriceline.columns.push({ text: 'Thời gian dự kiến', dataField: 'TimeRun', type: 'string', align: 'center', width:'110px' });
                            $scope.gridPriceline.columns.push({ text: 'Km dự kiến', dataField: 'KmRun', type: 'number', align: 'center', width:'auto',filterable:true, cellsFormat:'n'  });
                            $scope.gridPriceline.columns.push({ text: 'Tiền dự kiến', dataField: 'MoneyRun', type: 'number', align: 'center', width:'auto',filterable:true, cellsFormat:'n'  });
                            $scope.gridPriceline.columns.push({ text: 'Tiền quá mỗi km', dataField: 'MoneyEveryKm', type: 'number', align: 'center', width:'auto',filterable:true,cellsFormat:'n'  });
                            $scope.gridPriceline.columns.push({ text: 'Tiền quá mỗi giờ', dataField: 'MoneyEveryTime', type: 'number', align: 'center', width:'auto',filterable:true,cellsFormat:'n'  });
                            $scope.gridPriceline.columns.push({ text: 'Loại xe', dataField: 'ModelName', type: 'string', align: 'center', width:'auto',filterable:true  });
                            $scope.gridPriceline.columns.push({ text: 'Đội xe', dataField: 'GroupName', type: 'string', align: 'center', width:'auto',filterable:true  });


                            $scope.modelNameSelectting = new App.Shared.Gui.DropDown<any>('dropModelNamePriceline','Name','Name');
                            $scope.modelNameSelectting.setting.width = '100%';
                            $scope.modelNameSelectting.setting.placeHolder = 'Chọn loại xe';
                            $scope.modelNameSelectting.setting.filterPlaceHolder = 'Tìm loại xe...';
                            $scope.modelNameSelectting.setting.checkboxes = false;
                            $scope.modelNameSelectting.title = "";
                            $scope.modelNameSelectting.load(api.GetAllModelCar(),null,false);

                            $scope.grouppopupSelectting = new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('dropGrouppopupPriceline','Name','Id');
                            $scope.grouppopupSelectting.setting.width = '100%';
                            $scope.grouppopupSelectting.setting.placeHolder = 'Chọn đội xe';
                            $scope.grouppopupSelectting.setting.filterPlaceHolder = 'Tìm đội xe...';
                            $scope.grouppopupSelectting.setting.checkboxes = false;
                            $scope.grouppopupSelectting.title = "";


                            /**
                             * Xử lý khi import file excel
                             */

                            $("#xlf_priceline").on('change',function (event:any) {

                                // $scope.getPercentage = 0;

                                    var oFile:any = event.target.files[0];
                                    console.log(oFile);


                                    // Create A File Reader HTML5
                                    var reader = new FileReader();

                                    // Ready The Event For When A File Gets Selected
                                    reader.onload  = (e:any)=> {
                                        // language.showLoading(false,"");
                                        console.log(e);
                                        var data = e.target.result;
                                        console.log(data);
                                        var cfb = XLSX.read(data, {type: 'binary'});
                                        if(cfb == undefined){
                                            alert('file này không hợp lê');
                                            $scope.gridJsonDataPriceLine.data = $scope.dataexceljson;
                                        }else{

                                            // var wb = XLSX.parse_xlscfb(cfb);
                                            var wb = cfb;
                                            // console.log(cfb);
                                            // Loop Over Each Sheet



                                            wb.SheetNames.forEach(function(sheetName:any) {
                                                // Obtain The Current Row As CSV
                                                // var sCSV = XLS.utils.sheet_to_json(wb.Sheets['Sheet1']);
                                                $scope.dataexceljson = XLSX.utils.sheet_to_json(wb.Sheets['Sheet1']);
                                                // var oJS = XLS.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);

                                                // $("#my_file_output").html(sCSV);
                                                // console.log(sCSV);
                                                console.log($scope.dataexceljson);
                                                $scope.dataexceljson.shift();
                                                // console.log($scope.dataexceljson.shift());
                                                $scope.gridJsonDataPriceLine.data = $scope.dataexceljson;


                                                // var x:string = '';
                                                // $scope.datacreategroup = [];
                                                // for(var i = 0;i<$scope.dataexceljson.length;i++){
                                                //     if(x !== $scope.dataexceljson[i].GroupName && $scope.dataexceljson[i].GroupName !== undefined){
                                                //         $scope.datacreategroup.push({Name:$scope.dataexceljson[i].GroupName, CompanyId: $scope.dataexceljson[i].companyId});
                                                //     }
                                                //     x = $scope.dataexceljson[i].GroupName;
                                                //     companyID = $scope.dataexceljson[i].companyId;
                                                // }
                                                // console.log($scope.datacreategroup);

                                                // AdapterData($scope.gridJsonDataPriceLine.data);
                                                // $scope.gridJsonDataPriceLine.refresh();

                                                $scope.$apply();
                                            });
                                        }


                                        // if($scope.dataexceljson.length > 0){
                                        //     language.showLoading(true,"");
                                        // }
                                    };


                                    // Tell JS To Start Reading The File.. You could delay this if desired

                                    reader.readAsBinaryString(oFile);

                            });


                            $scope.lammoi = () =>{
                                //note: xử lý hàm làm mới khi nhập excel
                                console.log($("#xlf_priceline"));
                                $("#xlf_priceline").val('');
                                $scope.dataexceljson = [];
                            };

                            $scope.taobang = () => {
                                var tmp:any = [];
                                bootbox.dialog({
                                    title: "Thông Báo",
                                    message: "Bạn có đồng ý tạo bảng giá tuyến này !!!",
                                    buttons: {
                                        confirm: {
                                            label: "Đồng ý",
                                            className: "btn-danger",
                                            callback:()=>{
                                                var count_success:number = 0;

                                                console.log($scope.dataexceljson);
                                                for(var i =0; i< ($scope.dataexceljson.length);i++){
                                                    var req:any = {
                                                        ModelName: $scope.dataexceljson[i].ModelName,
                                                        BeginLocation: {
                                                            Lat:0,
                                                            Lng:0,
                                                            Address: $scope.dataexceljson[i].AddressStart,
                                                        },
                                                        EndLocation:{
                                                            Lat:0,
                                                            Lng:0,
                                                            Address: $scope.dataexceljson[i].AddressEnd,
                                                        },
                                                        TimeRun: $scope.dataexceljson[i].TimeRun,
                                                        KmRun: $scope.dataexceljson[i].KmRun,
                                                        MoneyRun: $scope.dataexceljson[i].MoneyRun,
                                                        MoneyEveryKm: $scope.dataexceljson[i].MoneyEveryKm,
                                                        MoneyEveryTime: $scope.dataexceljson[i].MoneyEveryTime,
                                                        CompanyId:$scope.dataexceljson[i].CompanyId ,
                                                        DeviceGroup: $scope.dataexceljson[i].DeviceGroup,
                                                        GroupName:  $scope.dataexceljson[i].GroupName,
                                                    };

                                                    $http.post(global.ReportHost + 'api/InputTaxi/AddPriceLine',req).then((rep:any)=>{
                                                        if(rep.data.Status == 1){
                                                            count_success++
                                                        }else{
                                                            tmp.push(`${$scope.dataexceljson[i].AddressStart} - ${$scope.dataexceljson[i].AddressEnd}`);
                                                            // this.logthongbao = tmp.toString();
                                                            // this.thongbaoexcel= true;
                                                        }
                                                        if(count_success == $scope.dataexceljson.length){
                                                            $scope.$parent.btViewClick();
                                                            $.jGrowl('Thêm dữ liệu từ file excel thành công.', {
                                                                header: 'Thông báo!',
                                                                theme: 'bg-success',
                                                                position: 'center'
                                                            });
                                                        }
                                                    });
                                                    // AddNewBangGiaTuyen(req);
                                                }
                                                if(tmp.length > 0){
                                                    $scope.loithongbao = tmp.toString();
                                                    $("#myModal_thongbao").modal('show');
                                                }
                                            }
                                        },
                                        cancle:{
                                            label:'Hủy',
                                            className: "btn-default",
                                        }
                                    }
                                });


                            };

                            $scope.$parent.btViewClick = () => {
                                var first=Array<App.Shared.Entity.IGroup>();
                                // đẩy 1 group làm thông tin chọn tất cả
                                first.push( {
                                    Id: -1,
                                    Name: 'Tất cả',
                                    CompanyId:$scope.$parent.cpnSelectSettings.itemSelect().Id
                                });
                                $scope.grouppopupSelectting.load(api.GetAllGroupByCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id),first,true);
                                    if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined){
                                    if($scope.$parent.groupdeviceSelectSettings.itemSelect().Id == -1){
                                        //note: nếu chọn đội là tất cả
                                        if($scope.$parent.modelNameSelectting.itemSelect().Name == 'Tất cả'){
                                            GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id);
                                        }else{
                                            GetDataLamDa($scope.$parent.cpnSelectSettings.itemSelect().Id,0,$scope.$parent.modelNameSelectting.itemSelect().Name);
                                        }

                                    }else{
                                        //note: chọn đội khác
                                        if($scope.$parent.modelNameSelectting.itemSelect().Name == 'Tất cả'){
                                            GetDataWithGroup($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.groupdeviceSelectSettings.itemSelect().Id);
                                        }else{
                                            GetDataLamDa($scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.groupdeviceSelectSettings.itemSelect().Id,$scope.$parent.modelNameSelectting.itemSelect().Name);
                                        }

                                    }
                                }
                            };


                            /**
                             *  Khai báo mới biến tạo bảng giá tuyến
                             */
                            $scope.funNew = {
                                ModelName: "",
                                BeginLocation: {
                                    Address: "",
                                },
                                EndLocation:{
                                    Address: "",
                                },
                                TimeRun: "",
                                KmRun: 0,
                                MoneyRun: 0,
                                MoneyEveryKm: 0,
                                MoneyEveryTime: 0,
                                CompanyId: 0,
                                CompanyName: "",
                                DeviceGroup: 0,
                                GroupName: "",
                            };

                            /**
                             * @param ne: giá trị tạo mới
                             */
                            $scope.gridPriceline.event.onNew = (ne:any) => {
                                $scope.isNew = true;
                                $scope.funNew = {
                                    ModelName: "",
                                    BeginLocation: {
                                        Address: "",
                                    },
                                    EndLocation:{
                                        Address: "",
                                    },
                                    TimeRun: "",
                                    KmRun: 0,
                                    MoneyRun: 0,
                                    MoneyEveryKm: 0,
                                    MoneyEveryTime: 0,
                                    CompanyId: 0,
                                    CompanyName: "",
                                    DeviceGroup: 0,
                                    GroupName: "",
                                };
                                $scope.grouppopupSelectting.setting.jqxDropDownList('clearSelection');
                                $scope.modelNameSelectting.setting.jqxDropDownList('clearSelection');
                                if($scope.grouppopupSelectting.setting.source == undefined){
                                    var first=Array<App.Shared.Entity.IGroup>();
                                    // đẩy 1 group làm thông tin chọn tất cả
                                    first.push( {
                                        Id: -1,
                                        Name: 'Tất cả',
                                        CompanyId:$scope.$parent.cpnSelectSettings.itemSelect().Id
                                    });
                                    $scope.grouppopupSelectting.load(api.GetAllGroupByCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id),first,true);
                                }
                                $scope.$apply();
                                $('#myModal_priceline').modal('show');
                            };

                            /**
                             * Chỉnh sửa
                             */
                            $scope.gridPriceline.event.onEdit = (ne:any,odl:any)=>{
                                console.log(ne);
                                $scope.isNew = false;
                                $scope.funNew.Id = ne.Id;
                                $scope.funNew.BeginLocation.Address = ne.BeginLocation.Address;
                                $scope.funNew.EndLocation.Address = ne.EndLocation.Address;
                                $scope.funNew.TimeRun = ne.TimeRun;
                                $scope.funNew.MoneyEveryKm = ne.MoneyEveryKm;
                                $scope.funNew.MoneyRun = ne.MoneyRun;
                                $scope.funNew.KmRun = ne.KmRun;
                                $scope.funNew.MoneyEveryTime = ne.MoneyEveryTime;

                                $scope.grouppopupSelectting.setItemDefault((m:any)=>m.Name == ne.GroupName);
                                $scope.modelNameSelectting.setItemDefault((m:any) => m.Name == ne.ModelName);
                                $scope.$apply();
                                $('#myModal_priceline').modal('show');
                            };

                            /**
                             * Xóa bảng giá tuyến
                             */
                            $scope.gridPriceline.event.onDelete = (ne:any)=>{
                                bootbox.dialog({
                                    title: "Thông Báo",
                                    message: "Bạn muốn xóa bảng giá tuyến này !!!",
                                    buttons: {
                                        confirm: {
                                            label: "Đồng ý",
                                            className: "btn-danger",
                                            callback:()=>{
                                                DeleteBangGiaTuyenRow(ne.Id,ne.CompanyId);
                                            }
                                        },
                                        cancle:{
                                            label:'Hủy',
                                            className: "btn-default",
                                        }
                                    }
                                });

                            };

                            $scope.newFormAccept = (isNew) =>{
                                switch (isNew){
                                    case true:
                                        var req:any = {
                                            ModelName: ($scope.modelNameSelectting.itemSelect() !== undefined)?$scope.modelNameSelectting.itemSelect().Name:'Khác',
                                            BeginLocation: {
                                                Lat:0,
                                                Lng:0,
                                                Address: $scope.funNew.BeginLocation.Address,
                                            },
                                            EndLocation:{
                                                Lat:0,
                                                Lng:0,
                                                Address: $scope.funNew.EndLocation.Address,
                                            },
                                            TimeRun: $scope.funNew.TimeRun,
                                            KmRun: $scope.funNew.KmRun,
                                            MoneyRun: $scope.funNew.MoneyRun,
                                            MoneyEveryKm: $scope.funNew.MoneyEveryKm,
                                            MoneyEveryTime: $scope.funNew.MoneyEveryTime,
                                            CompanyId: $scope.$parent.cpnSelectSettings.itemSelect().Id,
                                            CompanyName: $scope.$parent.cpnSelectSettings.itemSelect().Name,
                                            DeviceGroup: ($scope.grouppopupSelectting.itemSelect() !== undefined)?$scope.grouppopupSelectting.itemSelect().Id:0,
                                            GroupName: ($scope.grouppopupSelectting.itemSelect() !== undefined)?$scope.grouppopupSelectting.itemSelect().Name:'',
                                        };
                                        AddNewBangGiaTuyen(req);
                                        $('#myModal_priceline').modal('hide');
                                        break;
                                    case false:
                                        var req_capnhat:any = {
                                            Id: $scope.funNew.Id,
                                            ModelName: ($scope.modelNameSelectting.itemSelect() !== undefined)?$scope.modelNameSelectting.itemSelect().Name:'Khác',
                                            BeginLocation: {
                                                Lat:0,
                                                Lng:0,
                                                Address: $scope.funNew.BeginLocation.Address,
                                            },
                                            EndLocation:{
                                                Lat:0,
                                                Lng:0,
                                                Address: $scope.funNew.EndLocation.Address,
                                            },
                                            TimeRun: $scope.funNew.TimeRun,
                                            KmRun: $scope.funNew.KmRun,
                                            MoneyRun: $scope.funNew.MoneyRun,
                                            MoneyEveryKm: $scope.funNew.MoneyEveryKm,
                                            MoneyEveryTime: $scope.funNew.MoneyEveryTime,
                                            CompanyId: $scope.$parent.cpnSelectSettings.itemSelect().Id,
                                            CompanyName: $scope.$parent.cpnSelectSettings.itemSelect().Name,
                                            DeviceGroup: ($scope.grouppopupSelectting.itemSelect() !== undefined)?$scope.grouppopupSelectting.itemSelect().Id:0,
                                            GroupName:  ($scope.grouppopupSelectting.itemSelect() !== undefined)?$scope.grouppopupSelectting.itemSelect().Name:'',
                                        };
                                        PutUpdateBangGiaTuyen(req_capnhat);
                                        $('#myModal_priceline').modal('hide');
                                        break;
                                }

                            };


                            /**
                             * @GetDataWithCompany
                             * @param companyId gán giá trị id công ty
                             * @constructor hàm lấy danh sách bảng giá tuyến theo công ty
                             */
                            function GetDataWithCompany(companyId:number){
                                language.showLoading(false,"");
                                $http.get(global.ReportHost + `api/InputTaxi/PriceLineByCompany?companyId=${companyId}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridPriceline.data = rep.data.PriceLineList;
                                        ConvertData($scope.gridPriceline.data);
                                        $scope.gridPriceline.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }

                            /**
                             * @GetDataWithGroup
                             * @param companyId: giá trị id công ty
                             * @param groupId: giá trị id đội xe
                             * @constructor: hàm lấy danh sách bảng giá tuyến theo đội xe
                             */
                            function GetDataWithGroup(companyId: number, groupId:number){
                                language.showLoading(false,"");
                                $http.get(global.ReportHost + `api/InputTaxi/PriceLineByGroup?companyId=${companyId}&deviceGroupId=${groupId}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridPriceline.data = rep.data.PriceLineList;
                                        ConvertData($scope.gridPriceline.data);
                                        $scope.gridPriceline.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }

                            function AddNewBangGiaTuyen(req:any){
                                language.showLoading(false,"");
                                $http.post(global.ReportHost + 'api/InputTaxi/AddPriceLine',req).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Tạo bảng giá tuyến thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Đồng ý",
                                                    className: "btn-success",
                                                    callback:()=>{
                                                        $scope.$parent.btViewClick();
                                                    }
                                                }
                                            }
                                        });
                                    }else{
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Tạo bảng giá tuyến không thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Quay lại",
                                                    className: "btn-default",
                                                }
                                            }
                                        });
                                    }
                                });
                            }

                            function PutUpdateBangGiaTuyen(req:any){
                                language.showLoading(false,"");
                                $http.put(global.ReportHost + 'api/InputTaxi/UpdatePriceLine',req).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật bảng giá tuyến thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Đồng ý",
                                                    className: "btn-success",
                                                    callback:()=>{
                                                        $scope.$parent.btViewClick();
                                                    }
                                                }
                                            }
                                        });
                                    }else{
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật bảng giá tuyến không thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Quay lại",
                                                    className: "btn-default",
                                                }
                                            }
                                        });
                                    }
                                });
                            }

                            function GetDataLamDa(companyId:number,deviceGroupId:number,modelName:string){
                                language.showLoading(false,"");
                                $http.get(global.ReportHost + `api/InputTaxi/PriceLineLamda?companyId=${companyId}&deviceGroupId=${deviceGroupId}&modelName=${modelName}`).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        $scope.gridPriceline.data = rep.data.PriceLineList;
                                        ConvertData($scope.gridPriceline.data);
                                        $scope.gridPriceline.refresh();
                                        language.showLoading(true,"");
                                    }else{
                                        console.debug(rep.data.Description);
                                        language.showLoading(true,"");
                                    }
                                });
                            }

                            function DeleteBangGiaTuyenRow(pricelineId:number,companyId:number){
                                language.showLoading(false,"");
                                $http.delete(global.ReportHost + `api/InputTaxi/DeletePriceLine?priceLineId=${pricelineId}&companyId=${companyId}`).then((rep:any)=>{
                                    console.log(rep);
                                    if(rep.data.Status == 1){
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Xóa bảng giá tuyến thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Đồng ý",
                                                    className: "btn-success",
                                                    callback:()=>{
                                                        $scope.$parent.btViewClick();
                                                    }
                                                }
                                            }
                                        });
                                    }else{
                                        language.showLoading(true,"");
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Xóa bảng giá tuyến không thành công!!!",
                                            buttons: {
                                                confirm: {
                                                    label: "Quay lại",
                                                    className: "btn-default",
                                                }
                                            }
                                        });
                                    }
                                });
                            }

                            function DeleteBangGiaTuyenWithGroupOrCompany(companyId:number,groupId:number){
                                $http.delete(global.ReportHost + `api/InputTaxi/DeletePriceLineByGroup?companyId=${companyId}&groupId=${groupId}`).then((rep:any)=>{
                                    console.log(rep);
                                });
                            }

                            function ConvertData(value:any){
                                $.each(value,(ind,val)=>{
                                    val.STT = ind + 1;
                                    val.AddressStart = val.BeginLocation.Address;
                                    val.AddressEnd = val.EndLocation.Address;
                                });
                            }
                        }
                    }
                    AdModule.controller('PricelineController', PricelineController);
                }
            }
        }
    }
}