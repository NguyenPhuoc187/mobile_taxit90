/**
 * Created by NPPRO on 25/11/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Hanhtrinhtaxi{
                export class HanhtrinhtaxiRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper,global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'hanhtrinhtaxi',
                            config: {
                                controller: Taximanage.Hanhtrinhtaxi.HanhtrinhtaxiController,
                                templateUrl: '/components/taximanage/hanhtrinhtaxi/hanhtrinhtaxi.html'+`?v=${global.Version}`,
                                url: '/hanh-trinh-taxi/:param1/:param2/:param3/:param4',
                                params: {
                                    // id công ty
                                    param1: {
                                        value: null,
                                        squash: true
                                    },
                                    //serial thiết bị
                                    param2: {
                                        value: null,
                                        squash: true
                                    },
                                    // ngày bắt đầu
                                    param3: {
                                        value: null,
                                        squash: true
                                    },
                                    // ngày kết thúc
                                    param4: {
                                        value: null,
                                        squash: true
                                    }
                                },
                                tag:{
                                    title:'Hành trình taxi',
                                    icon:'icon-transmission',
                                    groupId:'taxi',
                                    index:1
                                }
                            }
                        });
                    }
                }
                AdModule.run(HanhtrinhtaxiRouteModel);
            }
        }
    }
}