/**
 * Created by NPPRO on 25/11/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Hanhtrinhtaxi{
                export interface ISysHanhTrinhTaxiMarker {

                    lngLatTrip: string;
                    // speedTrip: number;
                    // gpsTrip:any;
                    // doorStatus: number;
                    //
                    Address:any;
                    // freezerStatus:any;

                    TimeUpdate: string;
                    Speed: number;
                    MachineStatus: string;
                    AirMachineStatus:  string;
                    DoorStatus: string;
                    Fuel:number;
                    Power:any;
                    Temperature:number;
                    GpsStatus: string;
                    Distance:any;
                    TotalDistance:any;
                    Angle: number;
                    DistanceCo: number;
                    TotalDistanceCo:number;
                    StatusMeter: string;
                    StatusDevice: string;
                    CurentMoneySession: any;
                    CurrentMeterKm: any;
                    TotalMoney: any;
                    TotalKm: any;
                    ThoiGianCho: string;
                    TongCuoc: number;
                    GsmSignal:number;
                    SumBanTin:number;
                    HongNgoai:string;
                    map:any;


                }
                export interface IHanhtrinhtaxiScope extends ng.IScope{

                    cpnSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.ICompany>;
                    //cpnTagSelect:(event: any) =>any;
                    modelmarker:ISysHanhTrinhTaxiMarker;
                    carSelectSettings:App.Shared.Gui.IDropDown<App.Shared.Entity.IDevice>;
                    getdataClick:(sophut: number) =>any;
                    controlPlayClick:(event: any) =>any;
                    controlPauseClick:(event: any) =>any;
                    controlReplayClick:(event:any) =>any;
                    controlPrevClick:(event:any) =>any;
                    controlNextClick:(event:any) =>any;
                    onProcessbarClick:(event:any) =>any;
                    onProcessbarMouse:(event:any) =>any;
                    clickInputTime:(event:any) =>any;
                    timeStart: string;
                    timeEnd: string;
                    thoi_gian:string;
                    // listarray: Array<ISysMarker>;
                    BS:string;
                    transportEventLoad:(event:any) =>any;
                    totalRecordTrip:any;
                    totalCarKmTrip:number;
                    isPlaying:boolean;
                    companyId:number;
                    carId:any;
                    params:any;
                    cpnInstance:any;
                    carInstance:any;
                    ListStopStart:Array<any>;
                    timerselectcar:any;

                    timerselectcpn:any;
                    gaugeSettings:any;
                    valueSpeed:any;
                    dtTimeStartSelect:any;
                    dtTimeEndSelect:any;
                    // pointManager:any;
                    showNgaythang:()=>any;
                    isShowkhac:boolean;
                }
                export class HanhtrinhtaxiController{
                    public static $inject = ['$scope','language','global', '$http','$q','api','$location','$stateParams'];
                    constructor($scope: IHanhtrinhtaxiScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService,
                                $http: ng.IHttpService,public $q:ng.IQService,api:App.Shared.Api.IApi,public $location: ng.ILocationService,public stateParams:ng.ui.IStateParamsService){
                        var timeUpdateManageCarTrip:any;
                        var managertrip:any;
                        $('#chonxe').boxWidget({
                            animationSpeed: 500,
                            collapseTrigger: '#my-collapse-button-trigger',
                            removeTrigger: '#my-remove-button-trigger',
                            collapseIcon: 'fa-minus',
                            expandIcon: 'fa-plus',
                            removeIcon: 'fa-times'
                        });
                        $('[data-popup="tooltip"]').tooltip();
                        $scope.cpnSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.ICompany>('drHanhtrinhCtytaxiMaster','Display','Id');
                        // Tiêu đề trên combobox
                        $scope.cpnSelectSettings.title="";
                        // Set chiều dài cho combobox
                        $scope.cpnSelectSettings.setting.width = '99%';
                        $scope.cpnSelectSettings.setting.placeHolder = 'Chọn công ty';
                        $scope.cpnSelectSettings.setting.checkboxes = false;

                        $scope.carSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IDevice>('drHanhtrinhXeMaster','DisplayId','Serial');
                        $scope.carSelectSettings.title= "";
                        $scope.carSelectSettings.setting.width = '99%';
                        $scope.carSelectSettings.setting.placeHolder = 'Chọn xe';
                        $scope.carSelectSettings.setting.checkboxes = false;
                        $scope.params = {};
                        $scope.params  = stateParams;
                        $scope.isPlaying = true;

                        $scope.dtTimeStartSelect= new App.Shared.Gui.DateTimeControl('dtTimeSelectStartMobileTaxi');
                        $scope.dtTimeStartSelect.titleTime='';
                        $scope.dtTimeStartSelect.minday = -3;
                        // var min = $scope.dtTimeEndSelect.GetPickerDate();

                        $scope.dtTimeEndSelect=new App.Shared.Gui.DateTimeControl('dtTimeSelectEndMobileTaxi');
                        $scope.dtTimeEndSelect.titleTime='';
                        $scope.dtTimeEndSelect.minday = -3;
                        //hiển thị button quay về
                        $scope.dtTimeStartSelect.valuetime_default = '00:00';
                        $scope.dtTimeEndSelect.valuetime_default = '23:59';
                        $scope.dtTimeStartSelect.onSelect = () =>{
                            console.log('alo');
                            $scope.dtTimeEndSelect.SetMinDatePicker($scope.dtTimeStartSelect.GetPickerDate_setmin());
                        };
                        $scope.isShowkhac = false;
                        $scope.showNgaythang = () => {
                            $scope.isShowkhac = true;
                        }

                        // $scope.thoi_gian = '';
                        // $scope.thoi_gian=global.getnowday();
                        if(global.IndexReadly.GetValue() == true){
                            var t1 = setInterval(()=>{
                                clearInterval(t1);
                                buildLayout();
                            },50);
                        }
                        global.IndexReadly.Watch = (n:any)=>{
                            var t = setInterval(()=>{
                                clearInterval(t);
                                buildLayout();
                            },50);
                        };
                        function buildLayout(){
                            $scope.$on("$destroy", function() {
                                dispose();
                            });
                            $('#vantoc').hide();

                            var map:any;
                            var markerStart:any;
                            var markerEnd:any;
                            var marker: any;
                            var geocoder:any;


                            $scope.modelmarker= {
                                TimeUpdate: '',
                                lngLatTrip: '',
                                // speedTrip: 0,
                                // gpsTrip:'',
                                // doorStatus: 0,
                                Address:'',
                                // freezerStatus:'',
                                // MachineStatus:'',
                                // freezerStatus:any;
                                Speed: 0,
                                MachineStatus: '',
                                AirMachineStatus:  '',
                                DoorStatus: '',
                                HongNgoai:'',
                                Fuel:0,
                                Power:0,
                                Temperature:0,
                                GpsStatus: '',
                                Distance:0,
                                TotalDistance:0,
                                Angle: 0,
                                DistanceCo: 0,
                                TotalDistanceCo:0,
                                StatusMeter: '',
                                StatusDevice: '',
                                CurentMoneySession: 0,
                                CurrentMeterKm: 0,
                                TotalMoney: 0,
                                TotalKm: 0,
                                ThoiGianCho: '',
                                TongCuoc: 0,
                                GsmSignal:0,
                                SumBanTin:0,

                            };
                            loadMap();
                            function loadMap (){
                                geocoder = new google.maps.Geocoder();
                                var latlng = new google.maps.LatLng(10.777256,106.6053314);
                                var vbdID = "Vietbando";
                                var vbdType = new google.maps.ImageMapType({
                                    getTileUrl: function (xy, z) {
                                        return "http://mt3.google.com/vt/lyrs=m&z=" + z + "&x=" + xy.x + "&y=" + xy.y; // vietbando
                                    },
                                    tileSize: new google.maps.Size(256, 256),
                                    opacity: 1,
                                    maxZoom: 21, minZoom: 0,
                                    name: "Adsun",
                                    alt: "Bản đồ Việt Nam từ adsun.vn",
                                    isPng: true
                                });
                                var mapOptions = {
                                    center: latlng,
                                    //scrollwheel: false,
                                    zoom: 17,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                                    disableDefaultUI: true,
                                    mapTypeControl: false,
                                    backgroundColor: 'White',

                                    zoomControl: true,
                                    streetViewControl: false,
                                    zoomControlOptions: {
                                        style: google.maps.ZoomControlStyle.LARGE,
                                        position: google.maps.ControlPosition.RIGHT_BOTTOM
                                    }
                                };
                                map = new google.maps.Map(document.getElementById("maphanhtrinhxetaxi"), mapOptions);
                                var tienich = /** @type {HTMLInputElement} */(
                                    document.getElementById('controltripTaxiMap'));
                                map.controls[google.maps.ControlPosition.TOP_LEFT].push(tienich);
                                var playtienich = /** @type {HTMLInputElement} */(
                                    document.getElementById('playHanhtrinh'));
                                map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(playtienich);

                                $scope.map = map;
                                $scope.map.mapTypes.set(vbdID, vbdType);
                                $scope.map.setMapTypeId(vbdID);
                                // $scope.$on('$viewContentLoaded', function(){
                                //     //Here your view content is fully loaded !!
                                //     //$('.tooltitle_journey').tooltipster({
                                //     //    position: 'bottom'
                                //     //});
                                // });


                                managertrip = new Shared.Library.CarTaxiManagerTrip({map:map,q:$q,$http:$http});
                                google.maps.event.addListener(map, "bounds_changed", function(){
                                    google.maps.event.trigger(map, 'resize');
                                });
                                //event load map finish
                                google.maps.event.addListener(map, 'idle', function() {
                                    //khai báo thời gian mặc định
                                    var rep:any = {
                                        Domain: 'dinhvi.adsun.vn/hanh-trinh-taxi',
                                        Note: 'Map Hành trình'
                                    };
                                    $http.post(global.AuthHost + `StatictisMap/insert`,rep ).then((data:any)=>{
                                        console.log('ok');
                                    });
                                    $('.demo i').click(function() {
                                        $(this).parent().find('input').click();
                                    });
                                    var element=$('#timehanhtrinh');
                                    element.daterangepicker(global.CustomDaterange);
                                    element.on('apply.daterangepicker',(ev:any, picker:any)=> {
                                        // console.log(picker);
                                        // console.log(ev);
                                        // // picker.setEndDate('23:59')
                                        // console.log(ev.currentTarget.value);
                                        var e = ev.currentTarget.value;
                                        var ar = e.split(" - ");
                                        var p1 = ar[1].split(" ");
                                        // console.log(p1);
                                        if(p1[0] == '00:00'){
                                            element.data('daterangepicker').setEndDate(`23:59 ${p1[1]}`);
                                        }else{
                                            element.data('daterangepicker').setEndDate(`${p1[0]} ${p1[1]}`);
                                        }
                                        // console.log(e.sp)

                                    });
                                });

                            }

                            $scope.cpnSelectSettings.loadPromise(api.GetAllCompanyByType(),null,$q).then(()=>{
                                if($scope.params.param1 != null){
                                    $scope.cpnSelectSettings.setItemDefault(m=>m.Id == $scope.params.param1);
                                }
                            });

                            // $scope.cpnSelectSettings.load(api.GetAllCompanyByType(),null,true);

                            $scope.cpnSelectSettings.customEvent.onSelectOnItem = (index:number, val:App.Shared.Entity.ICompany)=> {
                                // $scope.carSelectSettings.load(api.GetAllDeviceByCompanyId(val.Id),null,true);

                                if($scope.params.param1 == null){
                                    $scope.carSelectSettings.load(api.GetAllDeviceByCompanyId(val.Id),null);
                                    // $scope.carSelectSettings.loadPromise(api.GetAllDeviceByCompanyId(val.Id),null,$q).then(()=>{
                                    //     if($scope.params.param2!=null) {
                                    //         // for (var i = 0; i < $scope.carSelectSettings.source.length; i++) {
                                    //         //     if ($scope.carSelectSettings.source[i].Serial == $scope.params.param2) {
                                    //         //         $scope.carSelectSettings.jqxComboBox('selectIndex', i);
                                    //         //         break;
                                    //         //     }
                                    //         // }
                                    //         $scope.carSelectSettings.setItemDefault(m=> m.Serial == $scope.params.param2);
                                    //         if(chectPaintStart==false) {
                                    //             $scope.timerselectcpn = setInterval(()=>{
                                    //                 /*Lấy danh sách công ty*/
                                    //                 clearInterval($scope.timerselectcpn);
                                    //                 vehanhtrinh($scope.params.param1, $scope.params.param2, $scope.params.param3, $scope.params.param4);
                                    //
                                    //             },1000);
                                    //
                                    //         }
                                    //     }
                                    // });
                                }else{
                                    if(val.Id !== $scope.params.param1*1) return;
                                    else{
                                        $scope.carSelectSettings.loadPromise(api.GetAllDeviceByCompanyId(val.Id),null,$q).then(()=>{
                                            if($scope.params.param2!=null) {
                                                // for (var i = 0; i < $scope.carSelectSettings.source.length; i++) {
                                                //     if ($scope.carSelectSettings.source[i].Serial == $scope.params.param2) {
                                                //         $scope.carSelectSettings.jqxComboBox('selectIndex', i);
                                                //         break;
                                                //     }
                                                // }
                                                $scope.carSelectSettings.setItemDefault(m=> m.Serial == $scope.params.param2);
                                                if(chectPaintStart==false) {
                                                    $scope.timerselectcpn = setInterval(()=>{
                                                        /*Lấy danh sách công ty*/
                                                        clearInterval($scope.timerselectcpn);
                                                        vehanhtrinh($scope.params.param1, $scope.params.param2, $scope.params.param3, $scope.params.param4);

                                                    },1000);

                                                }
                                            }
                                        });
                                    }
                                }
                                if(val.Lng !== null && val.Lat !== null){
                                    $scope.map.setCenter(new google.maps.LatLng(val.Lat, val.Lng));
                                }

                                // if ($scope.pointManager != null || $scope.pointManager != undefined) {
                                //     $scope.pointManager.clearhanhtrinh();
                                //
                                //     $scope.pointManager.load(val.Id).then((arraypoint:any)=>{
                                //         $.each(arraypoint,(ind:number,val:any)=>{
                                //             val.value.show();
                                //             val.value.showinfowindow();
                                //
                                //         })
                                //
                                //     });
                                // }
                                // else{
                                //     $scope.pointManager = new App.Shared.Library.PointManage($scope.map, $q, val.Id, $http, global);
                                //     $scope.pointManager.load(val.Id).then((arraypoint:any)=>{
                                //         $.each(arraypoint,(ind:number,val:any)=>{
                                //
                                //             val.value.show();
                                //             val.value.showinfowindow();
                                //
                                //         })
                                //
                                //     });
                                // }

                                // $scope.pointManager.show(true);
                                // console.log($scope.pointManager);



                            };
                            var chectPaintStart=false;
                            var contextMenu:any = null;
                            if (contextMenu == null)
                                contextMenu = new App.Shared.Library.MenuMap({
                                    map: $scope.map,
                                    managePoint: $scope.pointManager,
                                });

                            contextMenu.contextMenu.setMapInstance($scope.map);

                            //vẽ hành trình cho báo cáo cuốc xe
                            function vehanhtrinh(Idcongty:any,serialCar:any,startTime:any,endTime:any){
                                language.showLoading(false,"");
                                $scope.thoi_gian=ChangeTime(startTime)+" - "+ChangeTime(endTime);
                                chectPaintStart=true;
                                if(markerStart!=undefined)
                                {
                                    markerStart.setMap(null);
                                    markerEnd.setMap(null);
                                    $.each($scope.ListStopStart, (index:number, value:any)=> {
                                        value.setMap(null);
                                    })
                                }
                                $http.get(global.ReportHost + `api/Device/GetTaxiDeviceTripBySerial?companyId=${Idcongty}&serial=${serialCar}&beginTime=${startTime}&endTime=${endTime}`).then((rep:any) => {
                                    //$scope.listarray=rep.data.DeviceTripList;
                                    // $scope.listarray=[];
                                    language.showLoading(true,"");
                                    if( rep.data.DeviceTripList.length>0) {
                                        managertrip.load(rep.data.DeviceTripList, $scope.carSelectSettings.itemSelect().Serial, $scope.carSelectSettings.itemSelect().Id).then(function (t:any) {
                                            if ($scope.transportEventLoad != undefined) {
                                                $scope.transportEventLoad(t);
                                                // zoom vị trí bản đồ về record đầu tiên của hành trình
                                                map.setCenter(t.listArray[0].latLng);
                                                map.setZoom(16);
                                                markerStart = new google.maps.Marker({
                                                    position: t.listArray[0].latLng,
                                                    map: map,
                                                    icon: '../../assets/images/carIcon/Start.png'
                                                });
                                                markerEnd = new google.maps.Marker({
                                                    position: t.listArray[t.listArray.length - 1].latLng,
                                                    map: map,
                                                    icon: '../../assets/images/carIcon/End.png'
                                                });
                                                markerStart.setMap(map);
                                                markerEnd.setMap(map);
                                                $scope.totalCarKmTrip = managertrip.totalCarKmTrip;

                                                //$scope.ListStopStart=[];
                                                //$.each(t.listStop, function (index, value) {
                                                //    if(value.status==0) {
                                                //        var markerStop = new google.maps.Marker({
                                                //            position: value.latLng,
                                                //            map: map,
                                                //            icon: '../../assets/images/carIcon/Stop.png',
                                                //            title: "Dừng: "+value.title
                                                //        });
                                                //        markerStop.setMap(map);
                                                //        $scope.ListStopStart.push(markerStop);
                                                //    }
                                                //    if(value.status==1) {
                                                //        var markerStop = new google.maps.Marker({
                                                //            position: value.latLng,
                                                //            map: map,
                                                //            icon: '../../assets/images/carIcon/Off.png',
                                                //            title: "Đỗ: "+value.title
                                                //        });
                                                //        markerStop.setMap(map);
                                                //        $scope.ListStopStart.push(markerStop);
                                                //    }
                                                //
                                                //
                                                //});
                                            }
                                            if(timeUpdateManageCarTrip!=undefined)
                                                clearInterval(timeUpdateManageCarTrip);
                                            timeUpdateManageCarTrip=setInterval(function(){
                                                $scope.$apply(function(){
                                                    managertrip=t;
                                                    //clearInterval(time);
                                                });
                                            },50);
                                            //$('.progress-bar').width(100+"%").text(0);
                                            //var percent=getPercentProcessbar(e);
                                            //var index=convertPercentProcessToRecordIndex(percent);
                                            //$('.progress-bar').width(percent+"%").text(index);
                                            //
                                            //managertrip.seek(index);
                                            // $scope.thoi_gian=ChangeTime(startTime)+" - "+ChangeTime(endTime);
                                            managertrip.seek(rep.data.DeviceTripList.length-1);
                                        });
                                    }
                                    else{
                                        //set icon false
                                        if(markerStart!=undefined)
                                        {
                                            markerStart.setMap(null);
                                            markerEnd.setMap(null);
                                        }
                                        managertrip.clear();
                                        $('.progress-bar').width(0+"%").text(0);

                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Không có dữ liệu trả về trong khoảng thời gian: "+$scope.thoi_gian,
                                            buttons: {
                                                cancle: {
                                                    label: "Hủy",
                                                    className: "btn-danger",
                                                }
                                            }
                                        });

                                    }

                                });

                            }



                            /*Khi nhấn nút lấy dữ liệu*/
                            $scope.getdataClick = (sophut:number) =>{
                                $scope.isShowkhac = false;
                                if(markerStart!=undefined)
                                {
                                    checkplay=false;
                                    markerStart.setMap(null);
                                    markerEnd.setMap(null);
                                    // $.each($scope.ListStopStart,  (index:number, value:any)=> {
                                    //     value.setMap(null);
                                    // });
                                    $scope.isPlaying=true;
                                    document.getElementById("playPauseSpanId").className ="glyphicon glyphicon-play";
                                }

                                if($scope.cpnSelectSettings.itemSelect() !== undefined) {
                                    if($scope.thoi_gian !== ''){
                                        var endtime:any;
                                        var starttime_xphut:any;
                                        // var mang:any = $scope.thoi_gian.split(' - ');
                                        // $scope.timeStart = mang[0];
                                        // $scope.timeEnd = mang[1];
                                        if(sophut !== 0){
                                            endtime = GetDateTimeCurrent();
                                            starttime_xphut = GetDateTimeBeforeXmin(sophut);
                                        }else{
                                            starttime_xphut  = `${$scope.dtTimeStartSelect.GetTimeClockDefault()} ${$scope.dtTimeStartSelect.GetPickerDate_upserver()}`;
                                            endtime = `${$scope.dtTimeEndSelect.GetTimeClockDefault()} ${$scope.dtTimeEndSelect.GetPickerDate_upserver()}`;
                                        }


                                        if ($scope.carSelectSettings.itemSelect() != undefined){
                                            //lấy dữ liệu

                                            language.showLoading(false,"");
                                            $http.get(global.ReportHost + `api/Device/GetTaxiDeviceTripBySerial?companyId=${$scope.cpnSelectSettings.itemSelect().Id}&serial=${$scope.carSelectSettings.itemSelect().Serial}&beginTime=${starttime_xphut}&endTime=${endtime}`).then((rep:any) => {
                                                //$scope.listarray=rep.data.DeviceTripList;
                                                // $scope.listarray=[];
                                                language.showLoading(true,"");
                                                $('#chonxe').boxWidget('collapse');
                                                if( rep.data.DeviceTripList.length>0) {
                                                    managertrip.load(rep.data.DeviceTripList, $scope.carSelectSettings.itemSelect().Serial, $scope.carSelectSettings.itemSelect().Id).then((t:any)=> {
                                                        if ($scope.transportEventLoad != undefined) {
                                                            $scope.transportEventLoad(t);
                                                            // zoom vị trí bản đồ về record đầu tiên của hành trình
                                                            map.setCenter(t.listArray[0].latLng);
                                                            map.setZoom(12);
                                                            markerStart = new google.maps.Marker({
                                                                position: t.listArray[0].latLng,
                                                                map: map,
                                                                icon: '../../assets/images/carIcon/Start.png',
                                                                title:'Điểm bắt đầu'
                                                            });
                                                            markerEnd = new google.maps.Marker({
                                                                position: t.listArray[t.listArray.length - 1].latLng,
                                                                map: map,
                                                                icon: '../../assets/images/carIcon/End.png',
                                                                title:'Điểm kết thúc'
                                                            });
                                                            markerStart.setMap(map);
                                                            markerEnd.setMap(map);
                                                            $scope.totalCarKmTrip = managertrip.totalCarKmTrip;

                                                            //$scope.ListStopStart=[];
                                                            //$.each(t.listStop, function (index, value) {
                                                            //    if(value.status==0) {
                                                            //        var markerStop = new google.maps.Marker({
                                                            //            position: value.latLng,
                                                            //            map: map,
                                                            //            icon: '../../assets/images/carIcon/Stop.png',
                                                            //            title: "Dừng: "+value.title
                                                            //        });
                                                            //        markerStop.setMap(map);
                                                            //        $scope.ListStopStart.push(markerStop);
                                                            //    }
                                                            //    if(value.status==1) {
                                                            //        var markerStop = new google.maps.Marker({
                                                            //            position: value.latLng,
                                                            //            map: map,
                                                            //            icon: '../../assets/images/carIcon/Off.png',
                                                            //            title: "Đỗ: "+value.title
                                                            //        });
                                                            //        markerStop.setMap(map);
                                                            //        $scope.ListStopStart.push(markerStop);
                                                            //    }
                                                            //
                                                            //
                                                            //});
                                                        }
                                                        // console.log($scope.ListStopStart);
                                                        if(timeUpdateManageCarTrip!=undefined) clearInterval(timeUpdateManageCarTrip);
                                                        timeUpdateManageCarTrip=setInterval(function(){
                                                            $scope.$apply(function(){
                                                                managertrip=t;
                                                                //clearInterval(time);
                                                            });
                                                        },50);
                                                        managertrip.seek(rep.data.DeviceTripList.length-1);
                                                    });

                                                }
                                                else{
                                                    //set icon false
                                                    if(markerStart!=undefined)
                                                    {
                                                        markerStart.setMap(null);
                                                        markerEnd.setMap(null);
                                                    }
                                                    managertrip.clear();
                                                    $('.progress-bar').width(0+"%").text(0);

                                                    bootbox.dialog({
                                                        title: "Thông Báo",
                                                        message: "Không có dữ liệu trả về trong khoảng thời gian: "+$scope.thoi_gian,
                                                        buttons: {
                                                            cancle: {
                                                                label: "Hủy",
                                                                className: "btn-danger",
                                                            }
                                                        }
                                                    });

                                                }

                                            });
                                        }
                                        else {
                                            bootbox.dialog({
                                                title: "Thông Báo",
                                                message: "Vui lòng chọn xe !!",
                                                buttons: {
                                                    cancle: {
                                                        label: "Hủy",
                                                        className: "btn-danger",
                                                    }
                                                }
                                            });
                                        }
                                    }else{
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Vui lòng chọn thời gian !!",
                                            buttons: {
                                                cancle: {
                                                    label: "Hủy",
                                                    className: "btn-danger",
                                                }
                                            }
                                        });
                                    }
                                }else{
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Vui lòng chọn công ty !!",
                                        buttons: {
                                            cancle: {
                                                label: "Hủy",
                                                className: "btn-danger",
                                            }
                                        }
                                    });
                                }

                            };
                            /*bắt sự kiện nhấn nút play*/
                            $scope.controlPlayClick=(event)=>{
                                map.fitBounds(map.getBounds(),10);
                                //chọn toc độ cho xe
                                if(checkplay==false)
                                {
                                    checkplay=true;
                                    $scope.isPlaying=true;
                                    document.getElementById("playPauseSpanId").className ="glyphicon glyphicon-play";
                                    managertrip.replay();
                                }
                                var speedchoise = (<HTMLInputElement>document.getElementById("mySelectSpeed")).value;
                                if(speedchoise=='0'){managertrip.setSpeed(1000)}
                                if(speedchoise=='1'){managertrip.setSpeed(800)}
                                if(speedchoise=='2'){managertrip.setSpeed(600)}
                                if(speedchoise=='3'){managertrip.setSpeed(300)}
                                if(speedchoise=='4'){managertrip.setSpeed(200)}
                                if(speedchoise=='5'){managertrip.setSpeed(100)}
                                //play
                                if($scope.isPlaying==true){
                                    $scope.isPlaying=false;
                                    document.getElementById("playPauseSpanId").className ="glyphicon glyphicon-pause";
                                    managertrip.play();

                                }else {
                                    //dừng
                                    $scope.isPlaying=true;
                                    document.getElementById("playPauseSpanId").className ="glyphicon glyphicon-play";
                                    managertrip.pause();
                                }
                            };
                            /*bắt sự kiện nhấn nút dừng*/
                            $scope.controlPauseClick=(event)=>{

                            };
                            //click vào in phut time
                            var checkplay= false;
                            $scope.clickInputTime=(event)=>{

                                if($scope.isPlaying==false){
                                    $scope.isPlaying=true;
                                    document.getElementById("playPauseSpanId").className ="glyphicon glyphicon-play";
                                    managertrip.pause();

                                }
                            };
                            //chạy lại
                            $scope.controlReplayClick=(event)=>{
                                $scope.isPlaying=true;
                                document.getElementById("playPauseSpanId").className ="glyphicon glyphicon-play";
                                managertrip.replay();
                            };
                            //nút next
                            $scope.controlNextClick=(event)=>{
                                managertrip.next();
                            };
                            //nút pre
                            $scope.controlPrevClick=(event)=>{
                                managertrip.prev();
                            };
                            //thêm thông tin vô mảng
                            $scope.transportEventLoad=(obj:any)=>{
                                managertrip.totalCarKmTrip=obj.totalCarKmTrip/1000;
                                $scope.totalRecordTrip=obj.totalRecord;
                                managertrip.onChange = viewInfoCarTrip;
                            };
                            $scope.onProcessbarClick=(e:any)=>{
                                /*thay đổi % đến chỗ click*/
                                var percent=getPercentProcessbar(e);
                                var index=convertPercentProcessToRecordIndex(percent);
                                $('.progress-bar').width(percent+"%").text(index);

                                managertrip.seek(index);
                            };
                            $scope.onProcessbarMouse=(e:any)=>{
                                /*thay  đổi tooltip */
                                var percent=getPercentProcessbar(e);
                                $('#pProcessTrip').attr('title',"record : " + convertPercentProcessToRecordIndex(percent));
                            };

                            function getPercentProcessbar(e:any){
                                var width=$('#pProcessTrip').width();
                                var x = e.pageX- $('#pProcessTrip').offset().left;
                                return x/width*100;
                            }

                            function convertPercentProcessToRecordIndex(percent:number){
                                if(managertrip.listArray.length>0){
                                    var count: number = managertrip.listArray.length;
                                    return parseInt(`${count * percent/100}`);
                                }
                                return 0;
                            }
                            function viewInfoCarTrip(ob:any){
                                $scope.modelmarker.TimeUpdate=ob.TimeUpdate;
                                $scope.modelmarker.lngLatTrip=ob.latLng+"";
                                $scope.modelmarker.Speed=ob.Speed;
                                $scope.modelmarker.Address=ob.Address;
                                $scope.modelmarker.DoorStatus=ob.DoorStatus;
                                $scope.modelmarker.HongNgoai=ob.HongNgoai;
                                $scope.modelmarker.MachineStatus=ob.MachineStatus;
                                $scope.modelmarker.StatusMeter=ob.StatusMeter;
                                $scope.modelmarker.TotalDistance=AdapterMoney(ob.TotalDistance);
                                $scope.modelmarker.Power=AdapterDienAp(ob.Power);
                                //$scope.modelmarker.powerBatteryTrip=ob.powerBattery;
                                //$scope.modelmarker.fuelTrip=ob.fuel;
                                $scope.modelmarker.AirMachineStatus=ob.AirMachineStatus;
                                $scope.modelmarker.GpsStatus=ob.GpsStatus;
                                $scope.modelmarker.GsmSignal=ob.GsmSignal;
                                $scope.modelmarker.SumBanTin=ob.SumBanTin;
                                $scope.modelmarker.StatusDevice=ob.StatusDevice?"Có Khách":"Không Khách";
                                $scope.modelmarker.CurentMoneySession=AdapterMoney(ob.CurentMoneySession);
                                $scope.modelmarker.TongCuoc=ob.TongCuoc;
                                $scope.modelmarker.CurrentMeterKm=AdapterKmHienTai(ob.CurrentMeterKm/1000);
                                $scope.modelmarker.ThoiGianCho=ob.ThoiGianCho;
                                $scope.modelmarker.TotalKm=AdapterMoney(ob.TotalKm);
                                $scope.modelmarker.Distance=AdapterMeterToKm(ob.Distance);
                                var val = Math.floor((((ob.index+1)/$scope.totalRecordTrip) * 100)) + '%';
                                $('.progress-bar').width(val).text(val);
                                transportEventOnchange(ob.latLng);

                                // if(!map.getBounds().contains(ob.latLng))
                                //     transportEventOnchange(ob.latLng);

                                // transportEventOnchange(ob.latLng);
                                // map.fitBounds(ob.latLng, 170);

                            }
                            function transportEventOnchange(latLng:any){
                                map.setCenter(latLng);
                                map.panToBounds(map.getBounds());
                            }
                            function dispose(){
                                if(timeUpdateManageCarTrip!=undefined)
                                    clearInterval(timeUpdateManageCarTrip);
                                managertrip.clear();
                            }
                            //hàm chuyển thời gian
                            //chuyển thời gian để đưa qua hành trình
                            function ChangeTime(time:any){
                                let year:string = time.slice(15,19);
                                let month:string = time.slice(12,14);
                                let date:string = time.slice(9,11);

                                let hour:string = time.slice(0,5);

                                let tt:any = hour +' ' +month + '/' + date + '/' + year ;
                                return tt;
                            }
                            //    hàm timespan
                            function  ChangeTimeSpan(date1:any,date2:any){
                                var diff = date2.getTime() - date1.getTime();

                                var days = Math.floor(diff / (1000 * 60 * 60 * 24));
                                diff -=  days * (1000 * 60 * 60 * 24);

                                var hours = Math.floor(diff / (1000 * 60 * 60));
                                diff -= hours * (1000 * 60 * 60);

                                var mins = Math.floor(diff / (1000 * 60));
                                diff -= mins * (1000 * 60);

                                var seconds = Math.floor(diff / (1000));
                                diff -= seconds * (1000);
                                var resuflt:any=hours+":"+mins+":"+seconds;
                                return resuflt;
                            }
                            function AdapterMeterToKm(value:any){
                                Sugar.Number.setOption('decimal',',');
                                Sugar.Number.setOption('thousands','.');
                                return Sugar.Number(value/1000).round().format(0);
                            }
                        }

                        function AdapterMoney(value:number){
                            // var tmp:number = 0;
                            Sugar.Number.setOption('decimal',',');
                            Sugar.Number.setOption('thousands','.');
                            return Sugar.Number(value).format(0);
                        }
                        function AdapterKmHienTai(value:number){
                            Sugar.Number.setOption('decimal',',');
                            Sugar.Number.setOption('thousands','.');
                            return Sugar.Number(value).format(2);
                        }
                        function AdapterDienAp(value:number){
                            Sugar.Number.setOption('decimal',',');
                            Sugar.Number.setOption('thousands','.');
                            return Sugar.Number(value).format(1);
                        }
                        function GetDateTimeCurrent(){
                            let starttime:any = new Date();
                            let hournow:any = starttime.getHours() > 9? starttime.getHours(): `0${starttime.getHours()}`;
                            let minnow:any = starttime.getMinutes() > 9? starttime.getMinutes(): `0${starttime.getMinutes()}`;

                            let daynow:any = starttime.getDate() > 9? starttime.getDate(): `0${starttime.getDate()}`;
                            let monthnow:any = (starttime.getMonth() + 1) > 9? starttime.getMonth() + 1: `0${starttime.getMonth() + 1}`;
                            let yearnow:any = starttime.getFullYear();

                            let tmp = `${hournow}:${minnow}:00 ${monthnow}-${daynow}-${yearnow}`;
                            return tmp;
                        }
                        function GetDateTimeBeforeXmin(sophut:any){
                            let starttime:any = new Date();
                            starttime.setMinutes(starttime.getMinutes() - sophut);

                            let hournow:any = starttime.getHours() > 9? starttime.getHours(): `0${starttime.getHours()}`;
                            let minnow:any = starttime.getMinutes() > 9? starttime.getMinutes(): `0${starttime.getMinutes()}`;

                            let daynow:any = starttime.getDate() > 9? starttime.getDate(): `0${starttime.getDate()}`;
                            let monthnow:any = (starttime.getMonth() + 1) > 9? starttime.getMonth() + 1: `0${starttime.getMonth() + 1}`;
                            let yearnow:any = starttime.getFullYear();

                            let tmp = `${hournow}:${minnow}:00 ${monthnow}-${daynow}-${yearnow}`;
                            return tmp;
                        }




                    }
                }
                AdModule.controller('HanhtrinhtaxiController', HanhtrinhtaxiController);
            }
        }
    }
}
