module App{
    export module Components{
        export module Taximanage{
            export module Bcdoanhthu{
                export class BcdoanhthuxeRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper,global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'bcdoanhthu.doanhthuxe',
                            config: {
                                controller: Taximanage.Bcdoanhthu.Bcdoanhthuxe.BcdoanhthuxeController,
                                templateUrl: '/components/taximanage/bcdoanhthu/bcdoanhthuxe/bcdoanhthuxe.html'+`?v=${global.Version}`,
                                url: '/bao-cao-doanh-thu-xe',
                                tag:{
                                    title:'Báo cáo doanh thu xe',
                                    icon:'icon-pie-chart3',
                                    groupId:'taxi',
                                    index:2
                                }
                            }
                        });

                    }
                }
                AdModule.run(BcdoanhthuxeRouteModel);
            }
        }
    }
}