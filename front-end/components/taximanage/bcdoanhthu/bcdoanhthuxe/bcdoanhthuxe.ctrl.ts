/**
 * Created by phamn on 05/05/2017.
 */
module App {
    export module Components {
        export module Taximanage {
            export module Bcdoanhthu {
                export module Bcdoanhthuxe {
                    export interface IBcdoanhthuxeScope extends ng.IScope {
                        gridDoanhthuxe: App.Shared.Directives.GridControl;
                        $parent: IBcdoanhthuScope;
                    }
                    export class BcdoanhthuxeController {
                        public static $inject = ['$scope', 'language', 'global', '$http', '$q'];

                        constructor($scope: IBcdoanhthuxeScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService,
                                    $http: ng.IHttpService, $q: ng.IQService) {
                            $scope.$parent.hideComboboxGroupCar = false;
                            $scope.$parent.hideComboboxCar = false;
                            $scope.$parent.checkboxes_group = false;
                            $scope.$parent.checkboxes_combobox = true;
                            $scope.$parent.hideDriver = true;
                            $scope.gridDoanhthuxe = new App.Shared.Directives.GridControl("gridDoanhthuxe");
                            $scope.gridDoanhthuxe.selectionModeinfo = 'singlerow'; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridDoanhthuxe.filename_excel = "Báo cáo thống kê hoạt động và doanh thu xe";
                            $scope.gridDoanhthuxe.columns = [];
                            $scope.gridDoanhthuxe.height = window.innerHeight - 250;
                            $scope.gridDoanhthuxe.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            $scope.gridDoanhthuxe.isshowAggregates = true;
                            $scope.gridDoanhthuxe.isAltRows = true;
                            $scope.gridDoanhthuxe.isExport = true;
                            $scope.gridDoanhthuxe.isFilter = true;
                            $scope.gridDoanhthuxe.groupable= true;
                            $scope.gridDoanhthuxe.groups = [];

                            $scope.gridDoanhthuxe.columns.push({
                                text: 'STT',
                                dataField: 'STT',
                                type: 'string',
                                align: 'center',
                                width: '35px',
                                pinned:true
                            });
                            $scope.gridDoanhthuxe.columns.push({
                                text: 'Ngày',
                                dataField: 'ReportTime_show',
                                type: 'date',
                                width: '85px',
                                align: 'center',
                                filterable: true,
                                cellsformat: 'dd/MM/yyyy',

                            });
                            $scope.gridDoanhthuxe.columns.push({
                                text: 'Ngày',
                                dataField: 'ReportTime_signdate',
                                type: 'string',
                                width: '100px',
                                align: 'center',
                                hidden:true
                            });
                            $scope.gridDoanhthuxe.columns.push({
                                text: 'Số tài - Biển số',
                                dataField: 'DisplayId',
                                type: 'string',
                                width: '90px',
                                align: 'center',
                                filterable: true,

                                className:'gridtaxidetail_height40_2row'
                            });

                            $scope.gridDoanhthuxe.columns.push({
                                text: 'Tiền đồng hồ',
                                dataField: 'TienDongHo',
                                type: 'number',
                                width: '90px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                            });
                            // $scope.gridDoanhthuxe.columns.push({
                            //     text: 'Chích xung',
                            //     dataField: 'SlChichXung',
                            //     type: 'number',
                            //     width: '90px',
                            //     align: 'center',
                            //     filterable: true,
                            //     aggregates: ['sum'],
                            //     cellsformat: 'n',
                            // });
                            // $scope.gridDoanhthuxe.columns.push({
                            //     text: 'Tắt nguồn',
                            //     dataField: 'SlTatNguon',
                            //     type: 'number',
                            //     width: '90px',
                            //     align: 'center',
                            //     filterable: true,
                            //     aggregates: ['sum'],
                            //     cellsformat: 'n',
                            // });
                            // $scope.gridDoanhthuxe.columns.push({
                            //     text: 'Cuốc 0Km',
                            //     dataField: 'SlCuoc0Km',
                            //     type: 'number',
                            //     width: '90px',
                            //     align: 'center',
                            //     filterable: true,
                            //     aggregates: ['sum'],
                            //     cellsformat: 'n',
                            // });
                            $scope.gridDoanhthuxe.columns.push({
                                text: 'Tiền sau kết toán ca',
                                dataField: 'TienSauChotCa',
                                type: 'number',
                                width: '90px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                                className:'gridtaxidetail_height40_2row'
                            });
                            $scope.gridDoanhthuxe.columns.push({
                                text: 'Tổng cuốc',
                                dataField: 'TongCuoc',
                                type: 'number',
                                width: '90px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                                className:'gridtaxidetail_height40_2row'
                            });
                            $scope.gridDoanhthuxe.columns.push({
                                text: 'Tổng Km',
                                dataField: 'TongKmDongHo',
                                type: 'number',
                                width: '70px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                                className:'gridtaxidetail_height40_2row'
                            });
                            $scope.gridDoanhthuxe.columns.push({
                                text: 'Tổng Km sử dụng',
                                dataField: 'TongKmSuDung',
                                type: 'number',
                                width: '70px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                                className:'gridtaxidetail_height40_2row'
                            });

                            $scope.gridDoanhthuxe.columns.push({
                                text: 'Tổng Km rỗng',
                                dataField: 'TongKmRong',
                                type: 'number',
                                width: '70px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                                className:'gridtaxidetail_height40_2row'
                            });
                            $scope.gridDoanhthuxe.columns.push({
                                text: 'Tỉ lệ(Km sử dụng/Tổng Km)',
                                dataField: 'TyLeKm',
                                type: 'number',
                                width: '90px',
                                align: 'center',
                                filterable: true,
                                cellsformat: 'p',
                                className:'gridtaxidetail_height40_2row'
                            });
                            $scope.gridDoanhthuxe.columns.push({
                                text: 'Serial',
                                dataField: 'Serial',
                                type: 'string',
                                width: 'auto',
                                align: 'center',
                                filterable: true,
                                hidden:true
                            });
                            $scope.gridDoanhthuxe.columns.push({
                                text: 'DoiXe',
                                dataField: 'DoiXe',
                                type: 'string',
                                width: 'auto',
                                align: 'center',
                                filterable: true,
                                hidden:true
                            });

                            $scope.$parent.btViewClick = () => {
                                var starttime_xphut  = `${$scope.$parent.dtTimeStartSelect.GetTimeClockDefault()} ${$scope.$parent.dtTimeStartSelect.GetPickerDate_upserver()}`;
                                var endtime = `${$scope.$parent.dtTimeEndSelect.GetTimeClockDefault()} ${$scope.$parent.dtTimeEndSelect.GetPickerDate_upserver()}`;
                                language.showLoading(false,"");
                                if ($scope.$parent.cpnSelectSettings.itemSelect() !== undefined) {
                                    if ($scope.$parent.modelSelectSettings_multi.itemSelect() !== undefined) {
                                        if($scope.$parent.modelSelectSettings_multi.valueSelects()[0] == '-1'){
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.modelSelectSettings_multi.valueSelects().slice(1), starttime_xphut, endtime);
                                        }else{
                                            GetDataWithSerial($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.modelSelectSettings_multi.valueSelects(), starttime_xphut, endtime);
                                        }

                                    }
                                }
                            };

                            // $scope.gridDoanhthuxe.event.onDoubleRowClick = (ne:any) =>{
                            //     // console.log(ne);
                            //     if(ne.TienSauChotCa !== 0){
                            //         // console.log(ne);
                            //         // console.log(ne.ReportTime_signdate.split('/').toString().replace(/,/g,"-"));
                            //         window.open(`/doanh-thu-xe-ket-toan/${$scope.$parent.cpnSelectSettings.itemSelect().Id}/${ne.Serial}/00:00 ${ChangeTimeGetLink(ne.ReportTime_signdate)}/23:59 ${ChangeTimeGetLink(ne.ReportTime_signdate)}/${ne.DoiXe}`, '_blank');
                            //     }else{
                            //         // console.log(ne);
                            //         window.open(`/doanh-thu-xe-chot-ca/${$scope.$parent.cpnSelectSettings.itemSelect().Id}/${ne.Serial}/${ChangeTimeGetLink(ne.ReportTime_signdate)}`, '_blank');
                            //         // window.open(`/hanh-trinh-taxi/${$scope.$parent.cpnSelectSettings.itemSelect().Id}/${row.Serial}/${ChangeTime(row.BeginTime)}/${ChangeTime(row.EndTime)}`, '_blank');
                            //
                            //     }
                            //
                            // };


                            function GetDataWithSerial(companyId: number, serial: Array<string>, beginTime: string, endTime: string) {
                                $http.post(global.ReportHost + `api/BcDoanhThu/BdDoanhThuBySerial?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`,serial).then((rep: any)=> {
                                    language.showLoading(true,"");
                                    if (rep.data.Status == 1) {
                                        $scope.gridDoanhthuxe.data = rep.data.DThuXeTranfers;
                                        ConvertData($scope.gridDoanhthuxe.data);
                                        $scope.gridDoanhthuxe.refresh();
                                        language.showLoading(true, "");
                                    } else {
                                        console.debug(rep.data.Description);
                                    }
                                });
                            }
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    val.ReportTime_show = val.ReportTime;
                                    val.DisplayId = `${val.SoHieu} - ${val.Bs}`;
                                    val.ReportTime_signdate = global.AdapterSignDate(val.ReportTime);
                                    val.TongKmGps = (val.TongKmGps/1000).toFixed(1);
                                    val.TongKmSuDung = (val.TongKmSuDung/1000).toFixed(1);
                                    val.TongKmRong = (val.TongKmRong/1000).toFixed(1);
                                    val.TongKmDongHo = (val.TongKmDongHo/1000).toFixed(1);
                                });
                            }
                            function AdapterMoney(value:number){
                                // var tmp:number = 0;
                                Sugar.Number.setOption('decimal',',');
                                Sugar.Number.setOption('thousands','.');
                                return Sugar.Number(value).format(0);
                            }
                            function ChangeTimeGetLink(time:any){
                                var year:string = time.slice(6);
                                var month:string = time.slice(3,5);
                                var date:string = time.slice(0,2);
                                var tt:any = month + '-' + date + '-' + year ;
                                return tt;
                            }

                        }
                    }
                    AdModule.controller('BcdoanhthuxeController', BcdoanhthuxeController);
                }
            }
        }
    }
}