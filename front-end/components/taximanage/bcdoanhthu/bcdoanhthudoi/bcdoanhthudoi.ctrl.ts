/**
 * Created by phamn on 05/05/2017.
 */
module App {
    export module Components {
        export module Taximanage {
            export module Bcdoanhthu {
                export module Bcdoanhthudoi {
                    export interface IBcdoanhthudoiScope extends ng.IScope {
                        gridDoanhthudoi: App.Shared.Directives.GridviewControl;
                        $parent: IBcdoanhthuScope;

                    }
                    export class BcdoanhthudoiController {
                        public static $inject = ['$scope', 'language', 'global', '$http', '$q'];

                        constructor($scope: IBcdoanhthudoiScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService,
                                    $http: ng.IHttpService, $q: ng.IQService) {
                            $scope.$parent.hideComboboxGroupCar = false;
                            $scope.$parent.hideComboboxCar = true;
                            $scope.$parent.checkboxes_group = true;
                            $scope.$parent.checkboxes_combobox = true;
                            $scope.$parent.hideDriver = true;
                            $scope.gridDoanhthudoi = new App.Shared.Directives.GridviewControl("gridDoanhthudoi");
                            $scope.gridDoanhthudoi.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridDoanhthudoi.filename_excel = "Báo cáo số lượng cuốc xe trong điểm";
                            $scope.gridDoanhthudoi.columns = [];
                            $scope.gridDoanhthudoi.height = window.innerHeight - 250;
                            $scope.gridDoanhthudoi.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            $scope.gridDoanhthudoi.isshowAggregates = true;
                            $scope.gridDoanhthudoi.isAltRows = true;
                            $scope.gridDoanhthudoi.isFilter = true;
                            $scope.gridDoanhthudoi.isFilterMode = 'advanced';

                            $scope.gridDoanhthudoi.columns.push({
                                text: 'STT',
                                dataField: 'STT',
                                type: 'string',
                                align: 'center',
                                width: '35px'
                            });

                            $scope.gridDoanhthudoi.columns.push({
                                text: 'Ngày',
                                dataField: 'ReportTime',
                                type: 'date',
                                width: '100px',
                                align: 'center',
                                cellsFormat: 'dd/MM/yyyy',
                                filterable: true
                            });
                            $scope.gridDoanhthudoi.columns.push({
                                text: 'Tên Đội',
                                dataField: 'TenDoi',
                                type: 'string',
                                align: 'center',
                                width: '100px'
                            });
                            $scope.gridDoanhthudoi.columns.push({
                                text: 'Tiền đồng hồ',
                                dataField: 'TienDongHo',
                                type: 'number',
                                width: '130px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsFormat: 'n',
                            });
                            $scope.gridDoanhthudoi.columns.push({
                                text: 'Tiền sau chốt ca',
                                dataField: 'TienSauChotCa',
                                type: 'number',
                                width: '130px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsFormat: 'n',
                            });
                            $scope.gridDoanhthudoi.columns.push({
                                text: 'Tổng cuốc',
                                dataField: 'TongCuoc',
                                type: 'number',
                                width: '100px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsFormat: 'n',
                            });
                            $scope.gridDoanhthudoi.columns.push({
                                text: 'Tổng Km',
                                dataField: 'TongKmDongHo',
                                type: 'number',
                                width: '130px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsFormat: 'n',
                            });
                            $scope.gridDoanhthudoi.columns.push({
                                text: 'Tổng Km sử dụng',
                                dataField: 'TongKmSuDung',
                                type: 'number',
                                width: '140px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsFormat: 'n',
                            });

                            $scope.gridDoanhthudoi.columns.push({
                                text: 'Tổng Km rỗng',
                                dataField: 'TongKmRong',
                                type: 'number',
                                width: '130px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsFormat: 'n',
                            });
                            $scope.gridDoanhthudoi.columns.push({
                                text: 'Tỉ lệ(Km sử dụng/Tổng Km)',
                                dataField: 'TyLeKm',
                                type: 'number',
                                width: 'auto',
                                align: 'center',
                                filterable: true,
                                cellsFormat: 'p',
                            });

                            $scope.$parent.btViewClick = () => {
                                language.showLoading(false, "");
                                if ($scope.$parent.cpnSelectSettings.itemSelect() !== undefined) {
                                    if ($scope.$parent.groupdeviceSelectSettings_muti.itemSelect() !== undefined) {
                                        GetDataWithGroup($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.groupdeviceSelectSettings_muti.valueSelects(), $scope.$parent.dtTimeSelect.begin(), $scope.$parent.dtTimeSelect.end());
                                    }
                                }
                            };


                            function GetDataWithGroup(companyId: number, groupId: Array<number>, beginTime: string, endTime: string) {
                                if(groupId[0] === -1 ){
                                    $http.post(global.ReportHost + `api/BcDoanhThu/BdDoanhThuByGroup?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`,groupId.slice(1)).then((rep: any)=> {
                                        if (rep.data.Status == 1) {
                                            console.log(rep);
                                            $scope.gridDoanhthudoi.data = rep.data.DThuDoiXeTranfers;
                                            ConvertData($scope.gridDoanhthudoi.data);
                                            $scope.gridDoanhthudoi.refresh();
                                            language.showLoading(true, "");

                                        } else {
                                            console.debug(rep.data.Description);
                                            language.showLoading(true, "");
                                        }
                                    });
                                }else{
                                    $http.post(global.ReportHost + `api/BcDoanhThu/BdDoanhThuByGroup?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`,groupId).then((rep: any)=> {
                                        if (rep.data.Status == 1) {
                                            console.log(rep);
                                            $scope.gridDoanhthudoi.data = rep.data.DThuDoiXeTranfers;
                                            ConvertData($scope.gridDoanhthudoi.data);
                                            $scope.gridDoanhthudoi.refresh();
                                            language.showLoading(true, "");

                                        } else {
                                            console.debug(rep.data.Description);
                                            language.showLoading(true, "");
                                        }
                                    });
                                }

                            }
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    val.ReportTime = global.AdapterSignDate(val.ReportTime);
                                    val.TongKmGps = (val.TongKmGps/1000).toFixed(1);
                                    val.TongKmSuDung = (val.TongKmSuDung/1000).toFixed(1);
                                    val.TongKmRong = (val.TongKmRong/1000).toFixed(1);
                                    val.TongKmDongHo = (val.TongKmDongHo/1000).toFixed(1);
                                });
                            }
                            function AdapterMoney(value:number){
                                // var tmp:number = 0;
                                Sugar.Number.setOption('decimal',',');
                                Sugar.Number.setOption('thousands','.');
                                return Sugar.Number(value).format(0);
                            }

                        }
                    }
                    AdModule.controller('BcdoanhthudoiController', BcdoanhthudoiController);
                }
            }
        }
    }
}