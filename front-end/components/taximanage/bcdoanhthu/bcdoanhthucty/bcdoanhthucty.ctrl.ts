/**
 * Created by phamn on 05/05/2017.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Bcdoanhthu{
                export module Bcdoanhthucty{
                    export interface IBcdoanhthuctyScope extends ng.IScope{
                        gridDoanhthucty: App.Shared.Directives.GridviewControl;
                        $parent: IBcdoanhthuScope;
                    }
                    export class BcdoanhthuctyController{
                        public static $inject = ['$scope','language','global', '$http'];
                        constructor($scope: IBcdoanhthuctyScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService, $http: ng.IHttpService){
                            // $scope.$parent.dtTimeSelect.open = 'center';
                            $scope.$parent.hideComboboxGroupCar = true;
                            $scope.$parent.hideComboboxCar = true;
                            $scope.$parent.hideDriver = true;
                            $scope.gridDoanhthucty = new App.Shared.Directives.GridviewControl("gridDoanhthucty");
                            $scope.gridDoanhthucty.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridDoanhthucty.filename_excel = "Báo cáo thống kê hoạt động và doanh thu công ty";
                            $scope.gridDoanhthucty.columns = [];
                            $scope.gridDoanhthucty.height = window.innerHeight - 250;
                            $scope.gridDoanhthucty.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            $scope.gridDoanhthucty.isshowAggregates = true;
                            $scope.gridDoanhthucty.isAltRows = true;
                            $scope.gridDoanhthucty.isFilter = true;
                            $scope.gridDoanhthucty.isFilterMode = 'advanced';

                            $scope.gridDoanhthucty.columns.push({
                                text: 'STT',
                                dataField: 'STT',
                                type: 'string',
                                align: 'center',
                                width: '35px'
                            });
                            $scope.gridDoanhthucty.columns.push({
                                text: 'Ngày',
                                dataField: 'ReportTime',
                                type: 'string',
                                width: '100px',
                                align: 'center',
                                filterable: true
                            });
                            $scope.gridDoanhthucty.columns.push({
                                text: 'Tiền đồng hồ',
                                dataField: 'TienDongHo',
                                type: 'number',
                                width: '130px',
                                align: 'center',
                                filterable: true,
                                aggregates: [{
                                    'Total':
                                        function (aggregatedValue:any, currentValue:any, column:any, record:any) {
                                            var total = currentValue *1;
                                            return aggregatedValue + total;
                                        }
                                }],
                                aggregatesRenderer: function (aggregates:any, column:any, element:any) {
                                    var renderString = "<div style='margin: 4px; float: left;  height: 100%;'>";
                                    renderString += "<span>Tổng = </span>" + ((aggregates.Total != undefined)? aggregates.Total:0) + "</div>";
                                    return renderString;
                                },cellsFormat: 'n',
                            });
                            $scope.gridDoanhthucty.columns.push({
                                text: 'Tiền sau chốt ca',
                                dataField: 'TienSauChotCa',
                                type: 'number',
                                width: '130px',
                                align: 'center',
                                filterable: true,
                                aggregates: [{
                                    'Total':
                                        function (aggregatedValue:any, currentValue:any, column:any, record:any) {
                                            var total = currentValue *1;
                                            return aggregatedValue + total;
                                        }
                                }],
                                aggregatesRenderer: function (aggregates:any, column:any, element:any) {
                                    var renderString = "<div style='margin: 4px; float: left;  height: 100%;'>";
                                    renderString += "<span>Tổng = </span>" + ((aggregates.Total != undefined)? aggregates.Total:0) + "</div>";
                                    return renderString;
                                },
                                cellsFormat: 'n',
                            });
                            $scope.gridDoanhthucty.columns.push({
                                text: 'Tổng cuốc',
                                dataField: 'TongCuoc',
                                type: 'number',
                                width: '100px',
                                align: 'center',
                                filterable: true,
                                aggregates: [{
                                    'Total':
                                        function (aggregatedValue:any, currentValue:any, column:any, record:any) {
                                            var total = currentValue *1;
                                            return aggregatedValue + total;
                                        }
                                }],
                                aggregatesRenderer: function (aggregates:any, column:any, element:any) {
                                    var renderString = "<div style='margin: 4px; float: left;  height: 100%;'>";
                                    renderString += "<span>Tổng = </span>" + ((aggregates.Total != undefined)? aggregates.Total:0) + "</div>";
                                    return renderString;
                                },
                            });
                            $scope.gridDoanhthucty.columns.push({
                                text: 'Tổng Km',
                                dataField: 'TongKmDongHo',
                                type: 'number',
                                width: '130px',
                                align: 'center',
                                filterable: true,
                                aggregates: [{
                                    'Total':
                                        function (aggregatedValue:any, currentValue:any, column:any, record:any) {
                                            var total = currentValue *1;
                                            return aggregatedValue + total;
                                        }
                                }],
                                aggregatesRenderer: function (aggregates:any, column:any, element:any) {
                                    var renderString = "<div style='margin: 4px; float: left;  height: 100%;'>";
                                    renderString += "<span>Tổng = </span>" + ((aggregates.Total != undefined)? aggregates.Total:0) + "</div>";
                                    return renderString;
                                },
                                cellsFormat: 'n',
                            });
                            $scope.gridDoanhthucty.columns.push({
                                text: 'Tổng Km sử dụng',
                                dataField: 'TongKmSuDung',
                                type: 'number',
                                width: '140px',
                                align: 'center',
                                filterable: true,
                                aggregates: [{
                                    'Total':
                                        function (aggregatedValue:any, currentValue:any, column:any, record:any) {
                                            var total = currentValue *1;
                                            return aggregatedValue + total;
                                        }
                                }],
                                aggregatesRenderer: function (aggregates:any, column:any, element:any) {
                                    var renderString = "<div style='margin: 4px; float: left;  height: 100%;'>";
                                    renderString += "<span>Tổng = </span>" + ((aggregates.Total != undefined)? aggregates.Total:0) + "</div>";
                                    return renderString;
                                },
                                cellsFormat: 'n',
                            });

                            $scope.gridDoanhthucty.columns.push({
                                text: 'Tổng Km rỗng',
                                dataField: 'TongKmRong',
                                type: 'number',
                                width: '130px',
                                align: 'center',
                                filterable: true,
                                aggregates: [{
                                    'Total':
                                        function (aggregatedValue:any, currentValue:any, column:any, record:any) {
                                            var total = currentValue *1;
                                            return aggregatedValue + total;
                                        }
                                }],
                                aggregatesRenderer: function (aggregates:any, column:any, element:any) {
                                    var renderString = "<div style='margin: 4px; float: left;  height: 100%;'>";
                                    renderString += "<span>Tổng = </span>" + ((aggregates.Total != undefined)? aggregates.Total:0) + "</div>";
                                    return renderString;
                                },
                                cellsFormat: 'n',
                            });
                            $scope.gridDoanhthucty.columns.push({
                                text: 'Tỉ lệ(Km sử dụng/Tổng Km)',
                                dataField: 'TyLeKm',
                                type: 'number',
                                width: 'auto',
                                align: 'center',
                                filterable: true,
                                cellsFormat: 'p',
                            });

                            $scope.$parent.btViewClick = () => {
                                language.showLoading(false, "");
                                if ($scope.$parent.cpnSelectSettings.itemSelect() !== undefined) {
                                    GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.dtTimeSelect.begin(), $scope.$parent.dtTimeSelect.end());
                                }
                            };

                            function GetDataWithCompany(companyId: number, beginTime: string, endTime: string) {
                                $http.get(global.ReportHost + `api/BcDoanhThu/BdDoanhThuByCompany?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`).then((rep: any)=> {
                                    language.showLoading(true, "");
                                    if (rep.data.Status == 1) {
                                        $scope.gridDoanhthucty.data = rep.data.DoanhThuTranfers;
                                        ConvertData($scope.gridDoanhthucty.data);
                                        $scope.gridDoanhthucty.refresh();

                                    } else {
                                        console.debug(rep.data.Description);

                                    }
                                });
                            }
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    val.ReportTime = global.AdapterSignDate(val.ReportTime);
                                    // val.ThoiGianCho = val.ThoiGianCho.slice(0,5);
                                    val.TongKmGps = (val.TongKmGps/1000).toFixed(1);
                                    val.TongKmSuDung = (val.TongKmSuDung/1000).toFixed(1);
                                    val.TongKmRong = (val.TongKmRong/1000).toFixed(1);
                                    val.TongKmDongHo = (val.TongKmDongHo/1000).toFixed(1);

                                });
                            }
                            function AdapterMoney(value:number){
                                // var tmp:number = 0;
                                Sugar.Number.setOption('decimal',',');
                                Sugar.Number.setOption('thousands','.');
                                return Sugar.Number(value).format(0);
                            }
                        }
                    }
                    AdModule.controller('BcdoanhthuctyController', BcdoanhthuctyController);
                }
            }
        }
    }
}