/**
 * Created by phamn on 26/05/2017.
 */
module App {
    export module Components {
        export module Taximanage {
            export module Bcdoanhthu {
                export module Bcdoanhthutaixe {
                    export interface IBcdoanhthutaixeScope extends ng.IScope {
                        gridDoanhthutaixe: App.Shared.Directives.GridControl;
                        $parent: IBcdoanhthuScope;
                    }
                    export class BcdoanhthutaixeController {
                        public static $inject = ['$scope', 'language', 'global', '$http', '$q'];

                        constructor($scope: IBcdoanhthutaixeScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService,
                                    $http: ng.IHttpService, $q: ng.IQService) {

                            $scope.$parent.hideComboboxGroupCar = true;
                            $scope.$parent.hideComboboxCar = true;
                            $scope.$parent.checkboxes_group = true;
                            $scope.$parent.checkboxes_combobox = true;
                            $scope.$parent.hideDriver = false;


                            $scope.gridDoanhthutaixe = new App.Shared.Directives.GridControl("gridDoanhthutaixe");
                            $scope.gridDoanhthutaixe.selectionModeinfo = 'singlerow'; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridDoanhthutaixe.filename_excel = "Báo cáo thống kê hoạt động và doanh thu tài xế";
                            $scope.gridDoanhthutaixe.columns = [];
                            $scope.gridDoanhthutaixe.height = window.innerHeight - 250;
                            $scope.gridDoanhthutaixe.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            $scope.gridDoanhthutaixe.isshowAggregates = true;
                            $scope.gridDoanhthutaixe.isAltRows = true;
                            $scope.gridDoanhthutaixe.isFilter = true;
                            $scope.gridDoanhthutaixe.groupable= true;
                            $scope.gridDoanhthutaixe.groups = ['TenTaiXe'];

                            $scope.gridDoanhthutaixe.columns.push({
                                text: 'STT',
                                dataField: 'STT',
                                type: 'string',
                                align: 'center',
                                width: '35px',
                                pinned:true
                            });
                            $scope.gridDoanhthutaixe.columns.push({
                                text: 'Ngày',
                                dataField: 'ReportTime_show',
                                type: 'date',
                                width: '100px',
                                align: 'center',
                                filterable: true,
                                cellsformat: 'dd/MM/yyyy',
                                pinned:true
                            });
                            $scope.gridDoanhthutaixe.columns.push({
                                text: 'Ngày',
                                dataField: 'ReportTime_signdate',
                                type: 'string',
                                width: '100px',
                                align: 'center',
                                hidden:true
                            });
                            $scope.gridDoanhthutaixe.columns.push({
                                text: 'Tài xế',
                                dataField: 'TenTaiXe',
                                type: 'string',
                                width: '130px',
                                align: 'center',
                                filterable: true,
                                pinned:true
                            });

                            $scope.gridDoanhthutaixe.columns.push({
                                text: 'Tiền đồng hồ',
                                dataField: 'TienDongHo',
                                type: 'number',
                                width: '130px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                            });
                            $scope.gridDoanhthutaixe.columns.push({
                                text: 'Chích xung',
                                dataField: 'SlChichXung',
                                type: 'number',
                                width: '90px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                            });
                            $scope.gridDoanhthutaixe.columns.push({
                                text: 'Tắt nguồn',
                                dataField: 'SlTatNguon',
                                type: 'number',
                                width: '90px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                            });
                            $scope.gridDoanhthutaixe.columns.push({
                                text: 'Cuốc 0Km',
                                dataField: 'SlCuoc0Km',
                                type: 'number',
                                width: '90px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                            });
                            $scope.gridDoanhthutaixe.columns.push({
                                text: 'Tiền sau kết toán ca',
                                dataField: 'TienSauChotCa',
                                type: 'number',
                                width: '130px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                            });
                            $scope.gridDoanhthutaixe.columns.push({
                                text: 'Tổng cuốc',
                                dataField: 'TongCuoc',
                                type: 'number',
                                width: '100px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                            });
                            $scope.gridDoanhthutaixe.columns.push({
                                text: 'Tổng Km',
                                dataField: 'TongKmDongHo',
                                type: 'number',
                                width: '130px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                            });
                            $scope.gridDoanhthutaixe.columns.push({
                                text: 'Tổng Km sử dụng',
                                dataField: 'TongKmSuDung',
                                type: 'number',
                                width: '120px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                            });

                            $scope.gridDoanhthutaixe.columns.push({
                                text: 'Tổng Km rỗng',
                                dataField: 'TongKmRong',
                                type: 'number',
                                width: '130px',
                                align: 'center',
                                filterable: true,
                                aggregates: ['sum'],
                                cellsformat: 'n',
                            });
                            $scope.gridDoanhthutaixe.columns.push({
                                text: 'Tỉ lệ(Km sử dụng/Tổng Km)',
                                dataField: 'TyLeKm',
                                type: 'number',
                                width: 'auto',
                                align: 'center',
                                filterable: true,
                                cellsformat: 'p',
                            });

                            $scope.$parent.btViewClick = () => {
                                language.showLoading(false,"");
                                if ($scope.$parent.cpnSelectSettings.itemSelect() !== undefined) {
                                    if ($scope.$parent.modelSelectSettings_multi.itemSelect() !== undefined) {
                                        if($scope.$parent.modelSelectSettings_multi.valueSelects()[0] == '-1'){
                                            GetDataWithDriver($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.driverSelectSettings.valueSelects().slice(1), $scope.$parent.dtTimeSelect.begin(), $scope.$parent.dtTimeSelect.end());
                                        }else{
                                            GetDataWithDriver($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.driverSelectSettings.valueSelects(), $scope.$parent.dtTimeSelect.begin(), $scope.$parent.dtTimeSelect.end());
                                        }

                                    }
                                }
                            };

                            // $scope.gridDoanhthutaixe.event.onDoubleRowClick = (ne:any) =>{
                            //     console.log(ne);
                            //     if(ne.TienSauChotCa !== 0){
                            //         window.open(`/doanh-thu-xe-ket-toan/${$scope.$parent.cpnSelectSettings.itemSelect().Id}/${$scope.$parent.groupdeviceSelectSettings.itemSelect().Id}/${$scope.$parent.modelSelectSettings.itemSelect().Serial}/${global.ChangeTimeGetLink($scope.$parent.dtTimeSelect.begin())}/${global.ChangeTimeGetLink($scope.$parent.dtTimeSelect.end())}`, '_blank');
                            //     }else{
                            //         window.open(`/doanh-thu-xe-chot-ca/${$scope.$parent.cpnSelectSettings.itemSelect().Id}/${$scope.$parent.modelSelectSettings.itemSelect().Serial}/${ChangeTimeGetLink(ne.ReportTime_signdate)}`, '_blank');
                            //         // window.open(`/hanh-trinh-taxi/${$scope.$parent.cpnSelectSettings.itemSelect().Id}/${row.Serial}/${ChangeTime(row.BeginTime)}/${ChangeTime(row.EndTime)}`, '_blank');
                            //
                            //     }
                            //
                            // };


                            function GetDataWithDriver(companyId: number, driverId: Array<number>, beginTime: string, endTime: string) {
                                $http.post(global.ReportHost + `api/BcDoanhThu/BdDoanhThuByDriver?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`,driverId).then((rep: any)=> {
                                    language.showLoading(true,"");
                                    if (rep.data.Status == 1) {
                                        $scope.gridDoanhthutaixe.data = rep.data.DThuTaiXeTranfers;
                                        ConvertData($scope.gridDoanhthutaixe.data);
                                        $scope.gridDoanhthutaixe.refresh();

                                        language.showLoading(true, "");
                                    } else {
                                        console.debug(rep.data.Description);
                                    }
                                });
                            }
                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    val.ReportTime_show = val.ReportTime;
                                    val.DisplayId = `${val.SoHieu} - ${val.Bs}`;
                                    val.ReportTime_signdate = global.AdapterSignDate(val.ReportTime);
                                    val.TongKmGps = (val.TongKmGps/1000).toFixed(1);
                                    val.TongKmSuDung = (val.TongKmSuDung/1000).toFixed(1);
                                    val.TongKmRong = (val.TongKmRong/1000).toFixed(1);
                                    val.TongKmDongHo = (val.TongKmDongHo/1000).toFixed(1);
                                });
                            }
                            function AdapterMoney(value:number){
                                // var tmp:number = 0;
                                Sugar.Number.setOption('decimal',',');
                                Sugar.Number.setOption('thousands','.');
                                return Sugar.Number(value).format(0);
                            }
                            function ChangeTimeGetLink(time:any){
                                var year:string = time.slice(6);
                                var month:string = time.slice(3,5);
                                var date:string = time.slice(0,2);
                                var tt:any = month + '-' + date + '-' + year ;
                                return tt;
                            }

                        }
                    }
                    AdModule.controller('BcdoanhthutaixeController', BcdoanhthutaixeController);
                }
            }
        }
    }
}