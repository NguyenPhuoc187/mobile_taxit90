/**
 * Created by phamn on 05/05/2017.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Bcdoanhthu{
                export interface IBcdoanhthuScope extends ng.IScope{
                    cpnSelectSettings:App.Shared.Gui.IDropDown<App.Shared.Entity.ICompany>;
                    groupdeviceSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IGroup>;
                    groupdeviceSelectSettings_muti: App.Shared.Gui.IDropDown<App.Shared.Entity.IGroup>;
                    modelSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IDevice>;
                    modelSelectSettings_multi: App.Shared.Gui.IDropDown<App.Shared.Entity.IDevice>;
                    driverSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IDriver>;

                    dtTimeSelect:App.Shared.Gui.IDateTimeControl;
                    checkboxes_combobox:boolean;
                    showComboboxGroupCar:any;
                    hideComboboxGroupCar:boolean;
                    viewData:string;
                    btViewClick:() => any;
                    hideComboboxCar:boolean;
                    checkboxes_group:boolean;
                    hideDriver:boolean;

                    dtTimeStartSelect:any;
                    dtTimeEndSelect:any;

                }
                export class BcdoanhthuController{
                    public static $inject = ['$scope','language','global', '$http','api','$q'];
                    constructor ($scope: IBcdoanhthuScope,public language:App.Shared.LanguageService, public global: App.Shared.GlobalService, public $http: ng.IHttpService,
                                 public api:App.Shared.Api.IApi, public $q: ng.IQService){
                        $scope.viewData=language.gridLg().viewData;
                        // Khai báo mới combobox truyền vào 3 thông số : tên định dạng của combobox, hiển thị tên trường, giá trị chọn
                        $scope.cpnSelectSettings =new App.Shared.Gui.DropDown<App.Shared.Entity.ICompany>('cbCompanyChartTaxiMaster','Display','Id');
                        // Tiêu đề trên combobox
                        $scope.cpnSelectSettings.title= language.gridLg().company;
                        // Set chiều dài cho combobox
                        $scope.cpnSelectSettings.setting.width = '100%';
                        $scope.cpnSelectSettings.setting.checkboxes = false;
                        $scope.cpnSelectSettings.setting.filterPlaceHolder = 'Tìm công ty...';

                        // Khai báo combobox đội xe
                        $scope.groupdeviceSelectSettings=new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('dropdownGroupDeivesChartTaxiMaster','Name','Id');
                        $scope.groupdeviceSelectSettings.title=language.gridLg().group;
                        $scope.groupdeviceSelectSettings.setting.width = '100%';
                        $scope.groupdeviceSelectSettings.setting.checkboxes = false;
                        $scope.groupdeviceSelectSettings.setting.filterPlaceHolder = 'Tìm đội xe...';



                        $scope.groupdeviceSelectSettings_muti=new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('dropdownGroupDeivesMutiChartTaxiMaster','Name','Id');
                        $scope.groupdeviceSelectSettings_muti.title=language.gridLg().group;
                        $scope.groupdeviceSelectSettings_muti.setting.width = '100%';
                        $scope.groupdeviceSelectSettings_muti.setting.checkboxes = true;
                        $scope.groupdeviceSelectSettings_muti.setting.filterPlaceHolder = 'Tìm đội xe...';
                        $scope.groupdeviceSelectSettings_muti.setting.placeHolder = 'Chọn đội xe...';

                        // Khai báo combobox xe
                        $scope.modelSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IDevice>('cbDeviceChartTaxiMaster','DisplayId','Serial');
                        $scope.modelSelectSettings.title=language.gridLg().car;
                        $scope.modelSelectSettings.setting.width = '100%';
                        $scope.modelSelectSettings.setting.placeHolder = 'Chọn xe';
                        $scope.modelSelectSettings.setting.checkboxes = false;

                        //Khai báo combobox xe multi
                        $scope.modelSelectSettings_multi = new App.Shared.Gui.DropDown<App.Shared.Entity.IDevice>('cbDeviceMutiChartTaxiMaster','DisplayId','Serial');
                        $scope.modelSelectSettings_multi.title=language.gridLg().car;
                        $scope.modelSelectSettings_multi.setting.width = '100%';
                        $scope.modelSelectSettings_multi.setting.placeHolder = 'Chọn xe';


                        //Khai báo lịch
                        $scope.dtTimeSelect=new App.Shared.Gui.DateTimeControl('dtTimeSelectChartTaxiMaster');
                        $scope.dtTimeSelect.titleTime=language.gridLg().titleTime;

                        //Khai báo tài xế
                        $scope.driverSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IDriver>('cbDriverCharttaixiMaster','DisplayName','Id');
                        $scope.driverSelectSettings.setting.width = '100%';
                        $scope.driverSelectSettings.setting.placeHolder = 'Chọn tài xế';
                        $scope.driverSelectSettings.setting.filterPlaceHolder = 'Tìm tài xế...';
                        $scope.driverSelectSettings.title = language.gridLg().titleDriver;


                        $scope.dtTimeStartSelect= new App.Shared.Gui.DateTimeControl('dtTimeSelectStartMobileReportTaxi');
                        $scope.dtTimeStartSelect.titleTime='';
                        $scope.dtTimeStartSelect.minday = -3;
                        // var min = $scope.dtTimeEndSelect.GetPickerDate();

                        $scope.dtTimeEndSelect=new App.Shared.Gui.DateTimeControl('dtTimeSelectEndMobileReportTaxi');
                        $scope.dtTimeEndSelect.titleTime='';
                        $scope.dtTimeEndSelect.minday = -3;
                        //hiển thị button quay về
                        $scope.dtTimeStartSelect.valuetime_default = '00:00';
                        $scope.dtTimeEndSelect.valuetime_default = '23:59';
                        $scope.dtTimeStartSelect.onSelect = () =>{

                            $scope.dtTimeEndSelect.SetMinDatePicker($scope.dtTimeStartSelect.GetPickerDate_setmin());
                        };

                        $scope.checkboxes_combobox = false;
                        $scope.checkboxes_group = false;

                        $scope.hideComboboxCar = true; // ẩn dropdown xe
                        $scope.hideDriver = true; // ẩn dropdown xe
                        // api.GetAllCompany().then((data:any)=>{
                        //     var result_promise=$q.defer();
                        //     //todo: thêm dạng promise để lấy dữ liệu được
                        //     var result:any = [];
                        //     $.each(data,(ind,val)=>{
                        //         if(val.Id !== 18){
                        //             result.push(val);
                        //         }
                        //     });
                        //     $scope.cpnSelectSettings.loaddataarraywithapi(result_promise.promise,$q,true);
                        //     result_promise.resolve(result);
                        // });
                        // load công ty 2 thông số truyền vào : dữ liệu lấy về, vị trí truyền vào ban đầu
                        // $scope.cpnSelectSettings.load(api.GetAllCompanyByType(),null,true);
                        $scope.cpnSelectSettings.loadPromise(api.GetAllCompanyByType(), null,$q).then(()=>{

                            if(localStorage.getItem('companyIdtaxi') !== '' && localStorage.getItem('companyIdtaxi') !== null){

                                let companyidtaxi:any = localStorage.getItem('companyIdtaxi');
                                $scope.cpnSelectSettings.setItemDefault((m:any)=>m.Id === companyidtaxi*1);
                            }else{
                                $scope.cpnSelectSettings.setting.jqxDropDownList('selectIndex',0);
                            }
                        });
                        //Sự kiện chọn công ty
                        $scope.cpnSelectSettings.customEvent.onSelectOnItem=(index:number,val:App.Shared.Entity.ICompany)=>{
                            localStorage.setItem('companyIdtaxi',`${val.Id}`,);
                            var first=Array<App.Shared.Entity.IGroup>();
                            var firstdriver=Array<App.Shared.Entity.IDriver>();
                            // đẩy 1 group làm thông tin chọn tất cả
                            first.push( {
                                Id: -1,
                                Name: 'Tất cả',
                                CompanyId:val.Id
                            });
                            firstdriver.push( {
                                Id: -1,
                                DisplayName: 'Tất cả',
                                CompanyId:val.Id
                            });

                            console.log(`cong ty select ${val.Id}`);
                            $scope.groupdeviceSelectSettings.load(api.GetAllGroupByCompanyId(val.Id),null,true);
                            $scope.groupdeviceSelectSettings_muti.load(api.GetAllGroupByCompanyId(val.Id),first,true);

                            $scope.driverSelectSettings.load(api.GetAllDriverByCompany(val.Id),firstdriver,false);

                        };

                        // cài đặt sự kiện chọn đội xe
                        $scope.groupdeviceSelectSettings.customEvent.onSelectOnItem = (indx:number,val_group:App.Shared.Entity.IGroup)=>{
                            var firstdevice=Array<App.Shared.Entity.IDevice>();
                            firstdevice.push( {
                                Serial: '-1',
                                DisplayId: 'Tất cả',

                            });
                            console.log('group select');
                            // đẩy 1 group làm thông tin chọn tất cả
                            if(val_group.Id == -1){// hiển thị tất cả các xe trong công ty
                                $scope.modelSelectSettings.load(api.GetAllDeviceByCompanyId(val_group.CompanyId),null,true);
                                // $scope.modelSelectSettings.setting.jqxDropDownList('clearSelection');
                                $scope.modelSelectSettings_multi.load(api.GetAllDeviceByCompanyId(val_group.CompanyId),firstdevice,false);

                            }
                            else{// hiểm thị các xe trong đội xe đã chọn

                                $scope.modelSelectSettings.load(api.GetAllDeviceByGroupId(val_group.CompanyId,val_group.Id),null,true);
                                // $scope.modelSelectSettings.setting.jqxDropDownList('clearSelection');
                                $scope.modelSelectSettings_multi.load(api.GetAllDeviceByGroupId(val_group.CompanyId,val_group.Id),firstdevice,false);

                            }
                        };

                        /**
                         * Sự kiện chọn checkbox xe
                         */

                        $scope.modelSelectSettings_multi.customEvent.onCheckChange = (check:boolean, val:any)=>{
                            if(val.originalItem.Serial === '-1'){
                                if(check){
                                    $scope.modelSelectSettings_multi.setting.jqxDropDownList('checkAll');
                                }else{
                                    $scope.modelSelectSettings_multi.setting.jqxDropDownList('uncheckAll');
                                }
                            }
                        };


                        $scope.groupdeviceSelectSettings_muti.customEvent.onCheckChange = (check:boolean, val:any)=>{
                            if(val.originalItem.Id === -1){
                                if(check){
                                    $scope.groupdeviceSelectSettings_muti.setting.jqxDropDownList('checkAll');
                                }else{
                                    $scope.groupdeviceSelectSettings_muti.setting.jqxDropDownList('uncheckAll');
                                }
                            }
                        };

                        /**
                         * Sự kiện chọn tài xế
                         */

                        // $scope.driverSelectSettings.customEvent.onCheckChange = (check: boolean, val:any)=>{
                        //     if(val.originalItem.Id === -1){
                        //         if(check){
                        //             $scope.driverSelectSettings.setting.jqxDropDownList('checkAll');
                        //         }else{
                        //             $scope.driverSelectSettings.setting.jqxDropDownList('uncheckAll');
                        //         }
                        //     }
                        // };


                    }
                }
                AdModule.controller('BcdoanhthuController', BcdoanhthuController);
            }

        }
    }
}