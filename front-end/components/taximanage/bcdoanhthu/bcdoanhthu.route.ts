/**
 * Created by phamn on 05/05/2017.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Bcdoanhthu{
                export class BcdoanhthuRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper,global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'bcdoanhthu',
                            config: {
                                controller: Taximanage.Bcdoanhthu.BcdoanhthuController,
                                templateUrl: '/components/taximanage/bcdoanhthu/bcdoanhthu.html'+`?v=${global.Version}`,
                                url: '/bao-cao-doanh-thu',
                                tag:{
                                    title:'Báo cáo doanh thu',
                                    icon:'icon-bookmark',
                                    groupId:'taxi',
                                    index:3
                                },
                                show:false
                            }
                        });
                        sysRouter.configureStates({
                            name: 'bcdoanhthu.doanhthucty',
                            config: {
                                controller: Taximanage.Bcdoanhthu.Bcdoanhthucty.BcdoanhthuctyController,
                                templateUrl: '/components/taximanage/bcdoanhthu/bcdoanhthucty/bcdoanhthucty.html'+`?v=${global.Version}`,
                                url: '/bao-cao-doanh-thu-cong-ty',
                                parent:'bcdoanhthu',
                                tag:{
                                    title:'Báo cáo doanh thu công ty',
                                    icon:' icon-pie-chart7',
                                    groupId:'taxi',
                                    index:0
                                }
                            }
                        });
                        // sysRouter.configureStates({
                        //     name: 'bcdoanhthu.doanhthuxe',
                        //     config: {
                        //         controller: Taximanage.Bcdoanhthu.Bcdoanhthuxe.BcdoanhthuxeController,
                        //         templateUrl: '/components/taximanage/bcdoanhthu/bcdoanhthuxe/bcdoanhthuxe.html'+`?v=${global.Version}`,
                        //         url: '/bao-cao-doanh-thu-xe',
                        //         parent:'bcdoanhthu',
                        //         tag:{
                        //             title:'Báo cáo doanh thu xe',
                        //             icon:'icon-pie-chart3',
                        //             groupId:'taxi',
                        //             index:2
                        //         }
                        //     }
                        // });
                        sysRouter.configureStates({
                            name: 'bcdoanhthu.doanhthudoi',
                            config: {
                                controller: Taximanage.Bcdoanhthu.Bcdoanhthudoi.BcdoanhthudoiController,
                                templateUrl: '/components/taximanage/bcdoanhthu/bcdoanhthudoi/bcdoanhthudoi.html'+`?v=${global.Version}`,
                                url: '/bao-cao-doanh-thu-doi-xe',
                                parent:'bcdoanhthu',
                                tag:{
                                    title:'Báo cáo doanh thu đội xe',
                                    icon:' icon-stats-bars4',
                                    groupId:'taxi',
                                    index:1
                                }
                            }
                        });
                        sysRouter.configureStates({
                            name: 'bcdoanhthu.doanhthutaixe',
                            config: {
                                controller: Taximanage.Bcdoanhthu.Bcdoanhthutaixe.BcdoanhthutaixeController,
                                templateUrl: '/components/taximanage/bcdoanhthu/bcdoanhthutaixe/bcdoanhthutaixe.html'+`?v=${global.Version}`,
                                url: '/bao-cao-doanh-thu-tai-xe',
                                parent:'bcdoanhthu',
                                tag:{
                                    title:'Báo cáo doanh thu tài xế',
                                    icon:'icon-user-tie',
                                    groupId:'taxi',
                                    index:3
                                }
                            }
                        });

                    }
                }
                AdModule.run(BcdoanhthuRouteModel);
            }
        }
    }
}