module App {
    export module Components {
        export module Taximanage {
            export module MapSun {
                export interface IMapSunScope extends ng.IScope {
                    map: any;
                    areaManager: any;
                    pointManager: any;
                    distanceManager: any;
                    getdirectionManager: any;
                    cpnSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.ICompany>; // công ty
                    groupSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IGroup>; // đội xe

                    gridCarHetPhi: App.Shared.Directives.GridviewControl;
                    gridListTaxiCars: App.Shared.Directives.GridviewControl;


                    stateSelectSettings: any;
                    stateTagSelect: (event: any) => any;
                    StateCar: any;
                    stateId: any;

                    count_cars: any;
                    // Object
                    cartaxiManager: App.Shared.Library.CarTaxiManager;
                    timerGetDevice: any;
                    companyName: any;

                    autocompleteOptions: any;
                    location_cpn: any;
                    check_location: any;
                    place: any;
                    companyId: any;
                    click_searchcar: () => any;
                    carjourneyreports_Serial: any;
                    car_autocomplete_Serial: any;
                    count_carstate: any;
                    sumcar: any;
                    myChart_maptaxi: any;
                    init: () => any;


                    //khai báo biến hiển thị tiện ích
                    showtienich: (name: string, value: boolean) => any;
                    showdsxe: boolean; // hiển thị ds xe
                    showtocdo: boolean; // hiển thị thông báo tốc độ
                    showdiem: boolean; // hiển thị điểm
                    showvung: boolean; // hiển thị vùng
                    showchuthich: boolean;
                    showtglt: boolean; // hiển thị thông báo xe quá thời gian liên tục
                    showtimdiachi: boolean; // hiển thị thông báo xe quá thời gian liên tục
                    showdthutiendongho: boolean; // hiển thị thông báo xe quá thời gian liên tục


                    doanhthubinhquan: any;
                    doanhthutong: any;
                    hieusuat: number;
                    xetrongvung: number;
                    xengoaivung: number;

                    buttons: any;
                    myVar: string;
                    showeventbutton: (event: string) => any;
                    cartaxi_array: any;
                    clickselect: (item: any) => void;
                    sordBy: (name: string) => void;
                    propertyName: string;
                    reverse: boolean;

                    //danh sách biến hiển thị thông báo bên ngoài bản đồ
                    cartaxioverspeed: any;
                    cartaxisos: any;
                    cartaxiovertime: any;
                    carvipham: any;
                    carcanhbao: any;
                    carchuachotca: any;
                    AdMap: any;
                    isshowbuttonmenu: boolean;


                    titlematGPS: string;
                    titlematdongho: string;
                    titlematlienlac: string;
                    isBs: boolean;
                    showsos: boolean;
                    showbusy: boolean;
                    sochobando: boolean;
                    soluongxedangxem: number;

                    popupOptions: any;
                    visiblePopup: boolean;

                    dtTimeSelect: App.Shared.Gui.IDateTimeControl;
                    viewData: string;
                    loading: boolean;
                    btViewClick: () => void;
                    latdiemngangqua: number;
                    lngdiemngangqua: number;
                    textBoxBanKinh: any;
                    giatriBanKinhValue: number;
                    chartOptions: any;
                    tiendonghoDataSource: any;
                    chartTienDongHoOptions: any;
                }

                export class MapSunController {
                    public static $inject = ['$scope', 'language', 'global', '$http', '$q', 'api', '$timeout', '$interval', 'orderByFilter'];

                    constructor($scope: IMapSunScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService,
                                $http: ng.IHttpService, public $q: ng.IQService, api: App.Shared.Api.IApi,
                                $timeout: ng.ITimeoutService, $interval: ng.IIntervalService, orderByFilter: ng.IFilterOrderBy) {

                        $('[data-popup="tooltip"]').tooltip();
                        $('#controlMap1').boxWidget({
                            animationSpeed: 500,
                            collapseTrigger: '#my-collapse-button-trigger',
                            removeTrigger: '#my-remove-button-trigger',
                            collapseIcon: 'fa-minus',
                            expandIcon: 'fa-plus',
                            removeIcon: 'fa-times'
                        });


                        var timereset = $interval(() => {
                            $interval.cancel(timereset);
                            if (window.location.pathname == '/ban-do-taxi-II') {
                                location.reload();
                                window.location.href = `http://${window.location.host}/ban-do-taxi-II`;
                            }
                            //30 min =1800000
                        }, 1800000);


                        function hidetrangthaixe() {
                            $('#controlMap1').hide();
                            $('#giaithich').hide();
                        }

                        function hidenTienich() {
                            $('#thongkenhanh').hide();
                            $("#danhsachvung").hide();
                            $("#danhsachdiem").hide();
                            $("#danhsachvipham").hide();
                            $("#danhsachchuachot").hide();
                            $("#danhsachcanhbao").hide();
                            $("#danhsachviphamtocdo").hide();
                            $("#xequathoigian").hide();
                            $("#cauhinh").hide();
                            $("#xehetphi").hide();
                            $("#distance_top").hide();
                            $("#danhsachsos").hide();
                        }

                        //khai báo hiển thị bản đồ
                        // $scope.reverse = true;
                        $scope.showdsxe = true;
                        $scope.isshowbuttonmenu = false;
                        $scope.isBs = false;
                        $scope.showtocdo = (localStorage.getItem('tocdo') !== null && localStorage.getItem('tocdo') !== undefined && localStorage.getItem('tocdo') !== '') ? (localStorage.getItem('tocdo') === 'true' ? true : false) : true;
                        $scope.showdiem = false;
                        $scope.showvung = false;
                        $scope.showchuthich = true;// mặc định hiển thị
                        $scope.showtimdiachi = true;// mặc định hiển thị
                        $scope.showsos = false;// mặc định hiển thị
                        $scope.showbusy = false;// mặc định hiển thị
                        $scope.sochobando = false;// mặc định hiển thị
                        $scope.showdthutiendongho = false;// mặc định hiển thị
                        $scope.showtglt = (localStorage.getItem('thoigianlt') !== null && localStorage.getItem('thoigianlt') !== undefined && localStorage.getItem('thoigianlt') !== '') ? (localStorage.getItem('thoigianlt') === 'true' ? true : false) : true;// mặc định hiển thị

                        $scope.viewData = 'Xem';
                        $scope.loading = false;

                        $scope.cpnSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.ICompany>('drCompanyTaxiMap', 'Display', 'Id');

                        // Set chiều dài cho combobox
                        $scope.cpnSelectSettings.title = '';
                        $scope.cpnSelectSettings.setting.width = '100%';
                        $scope.cpnSelectSettings.setting.checkboxes = false;
                        $scope.cpnSelectSettings.setting.filterable = true;
                        $scope.cpnSelectSettings.setting.filterPlaceHolder = 'Tìm công ty...';
                        $scope.cpnSelectSettings.setting.placeHolder = 'Chọn công ty...';


                        // Khai báo combobox đội xe
                        $scope.groupSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('drGroupTaxiMap', 'Name', 'Id');
                        $scope.groupSelectSettings.title = '';
                        $scope.groupSelectSettings.setting.width = '100%';
                        $scope.groupSelectSettings.setting.checkboxes = false;
                        $scope.groupSelectSettings.setting.filterable = true;
                        $scope.groupSelectSettings.setting.filterPlaceHolder = 'Tìm đội xe...';
                        $scope.groupSelectSettings.setting.placeHolder = 'Chọn đội xe...';


                        // $scope.companyId_ = 11;
                        $scope.companyId = {};
                        $scope.location_cpn = [];
                        $scope.check_location = {};
                        $scope.autocompleteOptions = {
                            componentRestrictions: {country: 'VN'},
                            types: ['geocode']
                        };
                        $scope.place = '';
                        $scope.count_cars = {
                            state0: 0,//tất cả xe
                            state1: 0, // xe có khách
                            state2: 0, // xe không khách
                            state3: 0, // xe đỗ
                            state4: 0, // xe mất GPS
                            state5: 0, // xe mất liên lạc
                            state6: 0, // xe mất đồng hồ
                            state7: 0, // xe bận
                            state8: 0, // xe dừng
                        };


                        var contextMenu: any = null;
                        /*Hàm hủy controller*/
                        $scope.$on("$destroy", () => {
                            if (timereset !== null) {
                                $interval.cancel(timereset);
                            }
                            $scope.cpnSelectSettings.setting.jqxDropDownList('destroy');
                            $scope.cartaxiManager.clearAllCars();
                        });

                        /**
                         * Hàm xây dựng layout
                         */
                        function buildLayout() {
                            $scope.titlematGPS = 'Xe mất GPS';
                            $scope.titlematdongho = 'Xe mất đồng hồ';
                            $scope.titlematlienlac = 'Xe mất liên lạc';


                            /*Khai báo drop-list trạng thái*/
                            var state: any = [
                                {
                                    id: 0,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #F4A460'></span> - Tất cả"
                                },
                                {
                                    id: 1,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #3465aa'></span> - Có khách"
                                },
                                {
                                    id: 2,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #dc0030'></span> - Không khách 4 chỗ"
                                },
                                {
                                    id: 3,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #EB2271'></span> - Không khách 7 chỗ"
                                },
                                {
                                    id: 4,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #09a275'></span> - Mất GPS"
                                },
                                {
                                    id: 5,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #2a2a2a'></span> - Mất liên lạc"
                                },
                                // {
                                //     id: 6,
                                //     name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #2c3e50'></span> - Mất đồng hồ"
                                // },
                                // {
                                //     id: 7,
                                //     name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #e74c3c'></span> - Bận"
                                // },
                                // {
                                //     id: 8,
                                //     name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #f2b701'></span> - Dừng"
                                // }
                                ];
                            $scope.stateSelectSettings = {
                                width: '100%',
                                height: '40px',
                                placeHolder: 'Trạng thái xe',
                                displayMember: "name",
                                source: state,
                            };

                            /*Xử lý hàm chuyển trạng thái */
                            $scope.stateId = state[0];
                            $scope.stateTagSelect = (event) => {

                                // console.log($scope.groupSelectSettings.itemSelect().Name);
                                try {
                                    if (event.args) {
                                        var item = event.args.item;
                                        if (item) {
                                            $scope.stateSelectSettings.valueMember = item.originalItem.id;
                                            if (!$scope.groupSelectSettings.ready) {
                                                return;
                                            }
                                            $scope.StateCar($scope.stateSelectSettings.valueMember, $scope.groupSelectSettings.itemSelect().Name);
                                        }
                                    }
                                } catch (e) {
                                    console.error(e);
                                }
                            };

                            $scope.clickselect = (item: any) => {
                                $('#controlMap1').boxWidget('collapse');
                                item.focus();
                            };

                            $scope.propertyName = 'SoHieu_';
                            $scope.sordBy = (name) => {
                                // console.log('chay order');
                                $scope.reverse = (name !== null && $scope.propertyName === name)
                                    ? !$scope.reverse : false;
                                $scope.propertyName = name;
                                // $scope.cartaxi_array = orderByFilter($scope.cartaxi_array, $scope.propertyName, $scope.reverse);
                            };


                            //Khai báo danh sách vùng


                            var cellClass_gridCarCare: any = (row: any, dataField: any, cellText: any, rowData: any) => {
                                var cellValue: any = rowData[dataField];
                                var dateString: string = rowData['EndTime_show']; // Oct 23
                                var dateParts: any = dateString.split("/");
                                //định dạng format chuỗi 'dd/mm/yyyy' thành object ngày
                                var dateObject: any = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                                var cellValueEndtime: any = Math.floor((dateObject - Date.now()) / 86400000);
                                switch (dataField) {
                                    case "DisplayId":
                                        if (cellValueEndtime < 0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                    case "STT":
                                        if (cellValueEndtime < 0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                    case "Note":
                                        if (cellValueEndtime < 0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                    case "EndTime_show":
                                        if (cellValueEndtime < 0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                }

                            };
                            // list danh sách xe quan tâm
                            $scope.gridCarHetPhi = new App.Shared.Directives.GridviewControl("gridCarHetPhi");
                            $scope.gridCarHetPhi.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            /*Tắt tính năng xuất Excel*/
                            $scope.gridCarHetPhi.isExport = true;
                            $scope.gridCarHetPhi.columns = [];
                            $scope.gridCarHetPhi.height = "300px";
                            $scope.gridCarHetPhi.columnsHeight = 40;

                            $scope.gridCarHetPhi.columns.push({
                                text: 'STT',
                                dataField: 'STT',
                                type: 'number',
                                align: 'center',
                                width: '35px',
                                cellClassName: cellClass_gridCarCare
                            });
                            $scope.gridCarHetPhi.columns.push({
                                text: 'Số tài - Biển số',
                                dataField: 'DisplayId',
                                type: 'string',
                                align: 'center',
                                width: '120px',
                                cellClassName: cellClass_gridCarCare
                            });
                            $scope.gridCarHetPhi.columns.push({
                                text: 'Thời gian',
                                dataField: 'EndTime_show',
                                type: 'string',
                                align: 'center',
                                width: '90px',
                                cellClassName: cellClass_gridCarCare
                            });
                            $scope.gridCarHetPhi.columns.push({
                                text: 'Thông báo',
                                dataField: 'Note',
                                type: 'string',
                                align: 'center',
                                cellClassName: cellClass_gridCarCare
                            });


                            /*Chỉnh hiệu ứng cho trang bản đồ*/
                            var effect: string = 'fold';
                            var options: any = {direction: ''};
                            var duration: any = 800;


                            hidenTienich();
                            hidetrangthaixe();
                            if ($scope.showdsxe) {
                                console.log('show tiện ích');
                                shownTienich();
                            }

                            function shownTienich() {
                                console.log('show trạng thái');
                                $('#controlMap1').show();
                            }


                            $('#toolboxButton').click(function () {
                                $('#toolboxButton').hide();
                                $('.slide-toggle').show(700);
                                options = {direction: 'left'};
                                $('#controlMap1').toggle(effect, options, duration);
                            });
                            // Hiện bảng tiện ích
                            $(".slide-toggle").click(function () {
                                $('.slide-toggle').hide();
                                options = {direction: 'left'};
                                $('#controlMap1').toggle(effect, options, duration);
                                // $scope.gridListCars.refresh();
                                $('#toolboxButton').show(700);
                            });
                        }

                        buildLayout();


                        if (global.IndexReadly.GetValue() == true) {
                            var t1 = setInterval(() => {
                                clearInterval(t1);
                                buildController();
                            }, 500);
                        }
                        global.IndexReadly.Watch = (n: any) => {
                            var t = setInterval(() => {
                                clearInterval(t);
                                buildController();
                            }, 500);

                        };
                        return;

                        // hàm xử lý của map
                        function buildController() {
                            var vbdID = "Vietbando";
                            var vbdType = new google.maps.ImageMapType({
                                getTileUrl: function (xy, z) {
                                    return "http://mt3.google.com/vt/lyrs=m&z=" + z + "&x=" + xy.x + "&y=" + xy.y; // vietbando
                                },
                                tileSize: new google.maps.Size(256, 256),
                                opacity: 1,
                                maxZoom: 21, minZoom: 0,
                                name: "Adsun",
                                alt: "Bản đồ Việt Nam từ adsun.vn",
                                isPng: true
                            });
                            $scope.map = new google.maps.Map(document.getElementById("maptaxi132"), {
                                center: new google.maps.LatLng(10.777256, 106.6053314),
                                scrollwheel: true,
                                zoom: 13,
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                disableDefaultUI: true,
                                mapTypeControl: false,
                                mapTypeControlOptions: {
                                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                                },
                                backgroundColor: 'hsla(0, 0%, 0%, 0)',
                                panControl: true,
                                panControlOptions: {
                                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                                },
                                zoomControl: true,
                                minZoom: 6,

                                streetViewControl: false,
                                zoomControlOptions: {
                                    style: google.maps.ZoomControlStyle.DEFAULT,
                                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                                },
                                disablePanMomentum: true,
                                // styles: myStyles
                            });

                            $scope.map.mapTypes.set(vbdID, vbdType);
                            $scope.map.setMapTypeId(vbdID);


                            var cauhinhtienich: any = global.Account.GetValue().CauHinhDoi;

                            if (cauhinhtienich !== null && cauhinhtienich !== "") {
                                if (cauhinhtienich !== '{hoptacxa:false}') {
                                    var data = JSON.parse(cauhinhtienich);
                                    if (data !== undefined) {
                                        if (data.showbs !== undefined && data.showbs === true) {
                                            $scope.isBs = true;
                                        }
                                        if (data.tienichbando !== undefined) {
                                            if (data.tienichbando) {
                                                $scope.isshowbuttonmenu = true;
                                            }
                                        } else {
                                            $scope.isshowbuttonmenu = true;
                                        }
                                    } else {
                                        $scope.isshowbuttonmenu = true;
                                    }
                                }

                            } else {
                                $scope.isshowbuttonmenu = true;
                            }


                            var dsxe = /** @type {HTMLInputElement} */(
                                document.getElementById('controlMap'));
                            $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(dsxe);
                            var dstienich = /** @type {HTMLInputElement} */(
                                document.getElementById('Tienich_map'));
                            $scope.map.controls[google.maps.ControlPosition.TOP_RIGHT].push(dstienich);

                            var dsgiaithich = /** @type {HTMLInputElement} */(
                                document.getElementById('giaithich'));
                            $scope.map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(dsgiaithich);


                            var Panel_listcarwarmtaxi = /** @type {HTMLInputElement} */(
                                document.getElementById('Panel_listcarwarmtaxi'));
                            $scope.map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(Panel_listcarwarmtaxi);


                            var map_ready_idle = false;
                            /*sau khi map load xong thì chạy event này*/
                            google.maps.event.addListener($scope.map, 'idle', (mouseEvent: any) => {
                                map_ready_idle = true;
                                $('#map_loading').hide();
                                $("#maptaxi132").show();


                                // Buildlayout();
                                // gọi hàm này dể hiển thị map đề phòng trường hơp map trắng phau ko có hình ảnh
                                // google.maps.event.trigger($scope.map, 'resize');
                                $timeout(() => {

                                    google.maps.event.trigger($scope.map, 'resize');
                                }, 500);
                            });


                            google.maps.event.addListener($scope.map, 'bounds_changed', (e: any) => {
                                // google.maps.event.trigger($scope.map, 'resize');
                                map_ready_idle = false;
                                // console.log('sự kiện thay đổi map khi zoom',e);
                            });
                            var oldcompanyId: number;
                            var eventRightClickOnMapId: google.maps.MapsEventListener;
                            $scope.cpnSelectSettings.loadPromise(api.GetAllCompanyByType(), null, $q).then(() => {
                                if (localStorage.getItem('companyIdtaxi') !== '' && localStorage.getItem('companyIdtaxi') !== null) {
                                    // console.log(localStorage.getItem('companyIdtaxi'));
                                    let companyidtaxi: any = localStorage.getItem('companyIdtaxi');
                                    oldcompanyId = companyidtaxi;
                                    $scope.cpnSelectSettings.setItemDefault((m: any) => m.Id === companyidtaxi * 1);
                                } else {
                                    $scope.cpnSelectSettings.setting.jqxDropDownList('selectIndex', 0);
                                }
                            });

                            //Sự kiện chọn công ty
                            $scope.cpnSelectSettings.customEvent.onSelectOnItem = (index: number, val: App.Shared.Entity.ICompany) => {

                                $('#toolboxButton').show();

                                if (val.Id == 10075) {
                                    $scope.titlematGPS = 'Xe tạm nghỉ';
                                    $scope.titlematdongho = 'Xe nghỉ kinh doanh';
                                    $scope.titlematlienlac = ' Xe đang bảo dưỡng, sửa chữa';
                                } else {
                                    $scope.titlematGPS = 'Xe mất GPS';
                                    $scope.titlematdongho = 'Xe mất đồng hồ';
                                    $scope.titlematlienlac = 'Xe mất liên lạc';
                                }

                                localStorage.setItem('companyIdtaxi', `${val.Id}`,);

                                var first = Array<App.Shared.Entity.IGroup>();
                                // đẩy 1 group làm thông tin chọn tất cả
                                first.push({
                                    Id: -1,
                                    Name: 'Tất cả đội xe',
                                    CompanyId: val.Id
                                });


                                $scope.groupSelectSettings.loadPromise(api.GetAllGroupByCompanyId(val.Id), first, $q).then(() => {

                                    if (sessionStorage.getItem('groupId') !== undefined && sessionStorage.getItem('groupId') !== null && sessionStorage.getItem('groupId') !== '') {
                                        let groupname: any = sessionStorage.getItem('groupId');
                                        // oldcompanyId = companyidtaxi;
                                        $scope.groupSelectSettings.setItemDefault((m: any) => m.Name === groupname);
                                    } else {
                                        $scope.groupSelectSettings.setting.jqxDropDownList('selectIndex', 0);
                                    }
                                });

                                if (val.Lat !== undefined && val.Lng !== undefined) {
                                    $scope.map.setCenter(new google.maps.LatLng(val.Lat, val.Lng));
                                }


                                CheckHienthiTrangthai(val.Id);

                                $scope.companyName = val.Name;

                                /*Khai báo tạo chuột phải bản đồ và các chức năng*/

                                /*Khởi tạo biến chứa tìm đường*/
                                $scope.getdirectionManager = new App.Shared.Library.Getdirection($scope.map, $q);


                                /*Khởi tạo lớp quản lý context menu
                                 * Thông sô truyền vào sẽ bổ sung thêm khi cần
                                 * mặc định hiện tại chỉ có lớp map*/
                                if (contextMenu == null)
                                    contextMenu = new App.Shared.Library.MenuMap({
                                        map: $scope.map,
                                    });

                                contextMenu.contextMenu.setMapInstance($scope.map);

                                /*Chuột phải vào bản đồ*/
                                if (eventRightClickOnMapId != null || eventRightClickOnMapId != undefined)
                                    google.maps.event.removeListener(eventRightClickOnMapId);
                                eventRightClickOnMapId = google.maps.event.addListener($scope.map, 'rightclick', (mouseEvent: any) => {
                                    // console.log(`map right click`);
                                    if (($scope.map as any).FisrtRightClick == true) {
                                        {
                                            ($scope.map as any).FisrtRightClick = false;
                                            return;
                                        }
                                    }
                                    contextMenu.contextMenu.show(mouseEvent.latLng);
                                });

                                oldcompanyId = val.Id;

                            };

                            function CheckHienthiTrangthai(companyId: number) {

                                $http.get(global.ReportHost + `api/Company/GetSettingById/${companyId}`)
                                    .then((res: any) => {
                                        if (res.data.Status === 0) {
                                            return;
                                        }
                                        $scope.showsos = res.data.Setting.ChaySos;

                                        if (res.data.Setting.CauHinhCty !== "") {
                                            var result = JSON.parse(res.data.Setting.CauHinhCty);
                                            console.log(result);
                                            $scope.showbusy = (result.busystatus !== undefined) ? result.busystatus : false;
                                            $scope.sochobando = (result.sochobando !== undefined) ? result.sochobando : false;
                                        } else {
                                            $scope.sochobando = false;
                                            $scope.showbusy = false;
                                        }
                                    }).catch((e) => {
                                    console.log(e)
                                })
                            }


                            // Sự kiện chọn đội xe
                            $scope.groupSelectSettings.customEvent.onSelectOnItem = (index: number, val: App.Shared.Entity.IGroup) => {
                                //todo: đoạn này comment khi up code lên route 1
                                /*Tạo dữ liệu xe trên map*/
                                sessionStorage.setItem('groupId', val.Name);
                                if ($scope.cartaxiManager !== undefined) {
                                    $scope.cartaxiManager.clearAllCars();
                                    // $scope.AdMap.clearLayer();
                                } else {

                                    $scope.cartaxiManager = new Shared.Library.CarTaxiManager({
                                        map: $scope.map,
                                        q: $q,
                                        $http: $http,
                                        global
                                    });
                                    App.Shared.Library.GlobalCarTaxiManager = $scope.cartaxiManager;
                                    // $scope.cartaxiManager.onAddLayer = (serial: string, layer: any) => {
                                    //     $scope.AdMap.addLayer(serial, layer);
                                    // };

                                }

                                $scope.cartaxiManager.getAllCarByCompanyId(val.CompanyId, $http, global,
                                    $scope.gridListTaxiCars, $scope.companyName, val.Id, true)
                                    .then((s: boolean) => {
                                        //FilterGroup($scope.groupSelectSettings.itemSelect().Name);
                                        $('#controlMap').show(300);
                                        $(`.style-bs`).addClass('xebluesun');
                                        console.log($(`.style-bs`));
                                        $scope.StateCar($scope.stateSelectSettings.valueMember, $scope.groupSelectSettings.itemSelect().Name);
                                        // google.maps.event.trigger($scope.map, 'resize');
                                    });


                                $scope.cartaxiManager.ConnectTaxiSocket(val.CompanyId, val.Id);

                                // $scope.cartaxiManager.onUpdateLayer = (serial: string, car: any) => {
                                //     $scope.AdMap.updateLayer(serial, car);
                                // };


                                $scope.cartaxiManager.onStatictis = (st: any) => {
                                    console.log(st);
                                    if (st.StopOverTime) {
                                        $(`#bs_${st.Serial}`).addClass('xeredsun');
                                    } else {
                                        $(`#bs_${st.Serial}`).addClass('xebluesun');
                                    }


                                    $scope.StateCar($scope.stateSelectSettings.valueMember, val.Name);

                                };

                                $scope.cartaxiManager.onClickCarOnMap = (ev: any) => {
                                    $('#tablecarmap tr').each(function () {

                                        var customerId = $(this).find("#sohieu").text();

                                        if (customerId == ev.SoHieu) {
                                            var index = $(this).index();
                                            // Get row position by index
                                            var ypos = $('#tablecarmap tr:eq(' + index + ')').offset().top;

                                            // Go to row
                                            $('#divtablecar').animate({
                                                scrollTop: $('#divtablecar').scrollTop() + ypos - 340
                                            }, 500);
                                            // console.log(index);
                                            $(this).focus();
                                            $(this).addClass("selected").siblings().removeClass("selected");
                                        }
                                    });

                                };

                                // /*Danh sách xe sắp hết hạn phí*/
                                api.GetDevicesByExpireDate($scope.cpnSelectSettings.itemSelect().Id, 1).then((data: any) => {
                                    $scope.gridCarHetPhi.data = data;
                                    if (data.length > 0) {
                                        $.each($scope.gridCarHetPhi.data, (ind: number, val: any) => {
                                            var day: any = Math.floor((Date.parse(val.EndTime) - Date.now()) / 86400000);
                                            if (day < 0) {
                                                val.Note = `Đã quá hạn ${day * -1} ngày`;
                                            } else {
                                                val.Note = `Còn ${day} ngày`;
                                            }
                                            // val.Note = `Xe sẽ hết hạn trong <span class="text-danger">${Math.floor((Date.parse(val.EndTime) - Date.now())/86400000)}</span> ngày`;
                                            val.EndTime_show = global.AdapterSignDate(val.EndTime);
                                        });
                                        $('#xehetphi').show();
                                        $('#close_xehetphi').click(function () {
                                            $('#xehetphi').hide();
                                        });


                                        $scope.gridCarHetPhi.refresh();
                                    } else {
                                        $('#xehetphi').hide();
                                        // $('#modal_xehetphi').modal('hide');
                                        $scope.gridCarHetPhi.clear();
                                    }
                                });

                                // Xử lý autocomplete search xe
                                var currencies: any = [];
                                var currencies_Bs: any = [];
                                $.each($scope.cartaxiManager.getCarForGroup(val.Name), (index: number, val: any) => {
                                    currencies.push({value: `${val.SoHieu} - ${val.Bs}`, data: val.Serial});
                                });

                                CarAutocomplete(currencies);
                                $('#car_journeyreports').autocomplete({
                                    lookup: currencies,
                                    onSelect: function (suggestion: any) {
                                        $scope.carjourneyreports_Serial = suggestion.data;
                                    },

                                });

                            };


                            $('#modal_xehetphi').on('shown.bs.modal', () => {
                                $scope.gridCarHetPhi.refresh();
                            });


                            function CarAutocomplete(currencies: any) {
                                $('#autocomplete').autocomplete({
                                    lookup: currencies,
                                    onSelect: function (suggestion: any) {
                                        // $scope.car_autocomplete_Serial = suggestion.data;

                                        var cars_check = $scope.cartaxiManager.where((car) => {
                                            return car.serial == suggestion.data;
                                        });
                                        if (cars_check.length !== 0) {
                                            cars_check[0].focus();
                                            $scope.car_autocomplete_Serial = cars_check[0];
                                        }
                                        // else {
                                        //     $scope.car_autocomplete_Serial = null;
                                        // }
                                    },
                                }).on('keypress', (event: any) => {
                                    if (event.keyCode == 13) {
                                        console.log('tim xe', event);
                                        var cars_check = $scope.cartaxiManager.where((car) => {
                                            return car.SoHieu == event.target.value;
                                        });
                                        if (cars_check.length !== 0) {
                                            cars_check[0].focus();
                                            $scope.car_autocomplete_Serial = cars_check[0];
                                        } else {
                                            alert('Không có số hiệu cần tìm');
                                            return;
                                        }

                                    }
                                });

                            }

                            // Xử lý  kick vào tìm xe
                            $scope.click_searchcar = () => {
                                if ($('#autocomplete').val() == undefined || $('#autocomplete').val() == '') {
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Vui lòng chọn xe!!",
                                        buttons: {
                                            cancle: {
                                                label: "Hủy",
                                                className: "btn-danger",
                                            }
                                        }
                                    });
                                } else {
                                    var cars_check = $scope.cartaxiManager.where((car) => {
                                        return car.SoHieu == $scope.car_autocomplete_Serial.SoHieu;
                                    });
                                    if (cars_check.length !== 0) {
                                        cars_check[0].focus();
                                    } else {
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Không có xe cần tìm!!",
                                            buttons: {
                                                cancle: {
                                                    label: "Hủy",
                                                    className: "btn-danger",
                                                }
                                            }
                                        });
                                    }
                                }
                            };

                            /*Hàm xử lý group*/
                            function FilterGroup(groupname: string) {
                                var cargroup = $scope.cartaxiManager.getCarForGroup(groupname);
                                $.each(cargroup, (index: number, value: any) => {
                                    value.ForceHide = false;
                                    value.setMap($scope.map);

                                });
                                $.each($scope.cartaxiManager.where((c) => {
                                    return (c.DeviceGroup !== groupname);
                                }), (index: number, value: any) => {
                                    value.ForceHide = true;
                                    value.setMap(null);
                                });
                            }

                            /*Hàm xử lý trạng thái*/
                            $scope.StateCar = (state: number, groupname: string) => {
                                $scope.count_carstate = 0;
                                // lấy danh sách xe tất cả
                                var car_state0 = $scope.cartaxiManager.getAllCars();
                                // danh sách xe có khách
                                var car_state1 = $scope.cartaxiManager.getCarForCoKhach(groupname);
                                // danh sách xe không khách
                                // var car_state2 = $scope.cartaxiManager.getCar4Or5Cho();
                                var car_state2 = $scope.cartaxiManager.getCarKhongKhach4ChoSun();
                                console.log(car_state2);
                                // danh sách xe đỗ
                                var car_state3 = $scope.cartaxiManager.getCarKhongKhach7ChoSun();
                                // danh sách xe mất GPS
                                var car_state4 = $scope.cartaxiManager.getCarLostGps(groupname);
                                // danh sách xe mất liên lạc
                                var car_state5 = $scope.cartaxiManager.getCarLostSign(groupname);
                                // danh sách xe mất đồng hồ
                                var car_state6 = $scope.cartaxiManager.getCarLostDongHo(groupname);
                                // danh sách xe bận
                                var car_state7 = $scope.cartaxiManager.getCarForBusy(groupname);
                                // danh sách xe dừng
                                var car_state8 = $scope.cartaxiManager.getCarDung(groupname);
                                $scope.soluongxedangxem = $scope.cartaxiManager.getCarContainInbounds($scope.map.getBounds()).length;

                                // đếm số lượng các xe
                                CountCarState(car_state0, car_state1, car_state2, car_state3, car_state4, car_state5, car_state6, car_state7, car_state8);
                                // return;
                                switch (state) {
                                    case 0: // tất cả các trạng thái
                                    {

                                        $.each(car_state0, (index: number, val: any) => {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);
                                            $scope.sumcar = index + 1;
                                        });
                                        $scope.count_carstate = $scope.sumcar;
                                        $scope.cartaxi_array = car_state0;

                                        break;
                                    }
                                    case 1: // xe có khách
                                    {
                                        $scope.count_carstate = $scope.count_cars.state1;
                                        //hiển thị xe đang di chuyển lên bản đồ
                                        $.each(car_state1, (index: number, val: any) => {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);

                                        });
                                        //ẩn các xe không có khách
                                        $.each($scope.cartaxiManager.where((c) => {
                                            return !(c.MeterOk && c.Gps && c.MeterStatus);

                                        }), (index: number, val: any) => {
                                            val.ForceHide = true;
                                            val.setMap(null);
                                        });
                                        $scope.cartaxi_array = car_state1;
                                        break;
                                    }
                                    case 2: //xe không khách
                                    {
                                        $scope.count_carstate = $scope.count_cars.state2;
                                        console.log(car_state2);
                                        //hiển thị xe không khách
                                        $.each(car_state2, (index: number, val: any) => {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);
                                        });
                                        //ẩn đi các xe không dừng đỗ
                                        $.each($scope.cartaxiManager.where((c) => {
                                            return !(!c.LostGsm && c.MeterOk && c.Gps &&
                                                !c.MeterStatus && (c.SoGhe === 4 || c.SoGhe === 5 || c.SoGhe === 0))
                                        }), (index: number, val: any) => {
                                            val.ForceHide = true;
                                            val.setMap(null);
                                        });
                                        $scope.cartaxi_array = car_state2;
                                        break;
                                    }
                                    case 3: // ds xe đỗ
                                    {

                                        $scope.count_carstate = $scope.count_cars.state3;
                                        /*hiển thị các thiết bị tắt máy*/
                                        $.each(car_state3, (index: number, val: any) => {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);
                                        });
                                        /*ẩn thiết bị không cần thiết đi*/
                                        var invisible = $scope.cartaxiManager.where((car) => {
                                            return !(!car.LostGsm && car.MeterOk && car.Gps && !car.MeterStatus && car.SoGhe === 7);
                                        });
                                        $.each(invisible, (index: number, val: any) => {
                                            val.ForceHide = true;
                                            val.setMap(null);
                                        });
                                        $scope.cartaxi_array = car_state3;

                                        break;
                                    }
                                    case 4: // xe mất GPS
                                    {
                                        $scope.count_carstate = $scope.count_cars.state4;
                                        $.each(car_state4, (index: number, val: any) => {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);
                                        });

                                        $.each($scope.cartaxiManager.where((c) => {
                                            return c.Gps;
                                        }), (index: number, val: any) => {
                                            val.ForceHide = true;
                                            val.setMap(null);
                                        });
                                        $scope.cartaxi_array = car_state4;

                                        break;
                                    }
                                    case 5: // xe mất liên lạc
                                    {
                                        // đếm số xe hiển thị trạng thái mất liên lạc -> hiển thị trên tổng số xe
                                        $scope.count_carstate = $scope.count_cars.state5;
                                        $.each(car_state5, (index: number, val: any) => {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);
                                        });
                                        $.each($scope.cartaxiManager.where((c) => {
                                            return !c.LostGsm;
                                        }), (index: number, val: any) => {
                                            val.ForceHide = true;
                                            val.setMap(null);

                                        });
                                        $scope.cartaxi_array = car_state5;

                                        break;
                                    }
                                    case 6: // xe mất đồng hồ
                                    {
                                        //đếm số xe hiển thị trạng thái quá tốc độ
                                        $scope.count_carstate = $scope.count_cars.state6;
                                        $.each(car_state6, (index: number, val: any) => {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);

                                        });
                                        $.each($scope.cartaxiManager.where((c) => {
                                            return !(!c.LostGsm && !c.MeterOk);
                                        }), (index: number, val: any) => {

                                            val.ForceHide = true;
                                            val.setMap(null);

                                        });
                                        $scope.cartaxi_array = car_state6;

                                        break;
                                    }
                                    case 7: // xe bận
                                    {
                                        //đếm số xe hiển thị trạng thái xe bận
                                        $scope.count_carstate = $scope.count_cars.state7;
                                        $.each(car_state7, (index: number, val: any) => {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);

                                        });
                                        $.each($scope.cartaxiManager.where((car) => {
                                            return !(!car.LostGsm && car.MeterOk && car.Gps && !car.MeterStatus && car.IsBusy);
                                        }), (index: number, val: any) => {
                                            val.ForceHide = true;
                                            val.setMap(null);

                                        });
                                        $scope.cartaxi_array = car_state7;

                                        break;
                                    }
                                    case 8: // xe dừng
                                    {
                                        //đếm số xe hiển thị trạng thái xe bận
                                        $scope.count_carstate = $scope.count_cars.state8;
                                        $.each(car_state8, (index: number, val: any) => {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);

                                        });
                                        $.each($scope.cartaxiManager.where((car) => {
                                            return !(car.MeterOk && !car.MeterStatus && car.Speed == 0 && car.KeyStatus);
                                        }), (index: number, val: any) => {
                                            val.ForceHide = true;
                                            val.setMap(null);
                                        });
                                        $scope.cartaxi_array = car_state8;
                                        break;
                                    }
                                }

                                // var markers = [];
                                // for (var i = 0; i < $scope.cartaxi_array.length; i++) {
                                //     // console.log($scope.cartaxi_array[i]);
                                //     var latLng = new google.maps.LatLng($scope.cartaxi_array[i].latlng.lat(),
                                //         $scope.cartaxi_array[i].latlng.lng());
                                //     var marker = new google.maps.Marker({'position': latLng});
                                //     markers.push(marker);
                                // }
                                // var markerCluster = new MarkerClusterer($scope.map, $scope.cartaxi_array, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

                                $scope.pieDatasource = [
                                    {
                                        name: "Xe có khách",
                                        value: $scope.count_cars.state1,
                                        colorTest: '#1DB2F5'
                                    }, {
                                        name: "Xe chạy đón khách",
                                        value: $scope.count_cars.state2 - $scope.count_cars.state3 - $scope.count_cars.state8,
                                        colorTest: '#FFC720'
                                    },
                                    {
                                        name: "Xe không khách",
                                        value: $scope.count_cars.state2 + $scope.count_cars.state3,
                                        colorTest: '#dc0030'
                                    }
                                ];

                                var listcartien = $scope.cartaxiManager.getCarForGroup(groupname);

                                $scope.tiendonghoDataSource = listcartien.map(s => {
                                    return {
                                        arg: `${s.SoHieu}`,
                                        val: s.TotalMoney
                                    }
                                });

                                // $scope.tiendonghoDataSource = $scope.cartaxiManager.get
                            };

                            function SortData(value: any) {
                                $.each(value, (ind: number, val: any) => {
                                    val.SoHieu_ = parseInt(val.SoHieu.replace(/^\D+/g, ''));
                                });
                            }

                            /*Hàm tính số xe theo trạng thái*/
                            function CountCarState(state0: any, state1: any, state2: any, state3: any, state4: any, state5: any, state6: any, state7: any, state8: any) {
                                if (state0.length != 0) {
                                    $.each(state0, (index: number, val: any) => {
                                        // tất cả xe
                                        $scope.count_cars.state0 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state0 = 0;
                                }
                                if (state1.length != 0) {
                                    $.each(state1, (index: number, val: any) => {
                                        // xe di chuyển
                                        $scope.count_cars.state1 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state1 = 0;
                                }
                                if (state2.length != 0) {
                                    $.each(state2, (index: number, val: any) => {
                                        // xe dừng đổ
                                        $scope.count_cars.state2 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state2 = 0;
                                }
                                if (state3.length != 0) {
                                    $.each(state3, (index: number, val: any) => {
                                        // xe dừng đổ
                                        $scope.count_cars.state3 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state3 = 0;
                                }
                                if (state4.length != 0) {
                                    $.each(state4, (index: number, val: any) => {
                                        // xe mất GPS
                                        $scope.count_cars.state4 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state4 = 0;
                                }
                                if (state5.length != 0) {
                                    $.each(state5, (index: number, val: any) => {
                                        // xe mất GPS
                                        $scope.count_cars.state5 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state5 = 0;
                                }
                                if (state6.length != 0) {
                                    $.each(state6, (index: number, val: any) => {
                                        // xe mất GPS
                                        $scope.count_cars.state6 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state6 = 0;
                                }
                                if (state7.length != 0) {
                                    $.each(state7, (index: number, val: any) => {
                                        // xe mất GPS
                                        $scope.count_cars.state7 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state7 = 0;
                                }
                                if (state8.length != 0) {
                                    $.each(state8, (index: number, val: any) => {
                                        // xe mất GPS
                                        $scope.count_cars.state8 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state8 = 0;
                                }

                            }

                            /*Hàm chuyển thời gian*/
                            function Adaptertimeupdate(x: any) {
                                $.each(x, (index: number, val: any) => {
                                    val.tt = val.ClientSend.slice(11);
                                });
                            }


                            /*Xử lý sự kiện trên thanh màu*/
                            function CheckStateCar(x: number) {
                                if ($scope.cpnSelectSettings.itemSelect().Id != undefined) {
                                    $scope.stateSelectSettings.jqxDropDownList('selectIndex', x);
                                } else {
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Xin chọn công ty !!!",
                                        buttons: {
                                            cancle: {
                                                label: "Hủy",
                                                className: "btn-danger",
                                            }
                                        }
                                    });
                                }
                            }


                            $('#totalcar_taxi').click(() => {
                                CheckStateCar(0);

                            });
                            $('#carrun_taxi').click(() => {
                                CheckStateCar(1);

                            });
                            $('#carstop_taxi').click(() => {
                                CheckStateCar(3);
                            });
                            $('#cartaxido').click(() => {
                                CheckStateCar(2);
                            });
                            $('#cartaxilostgps').click(() => {
                                CheckStateCar(4);
                            });
                            $('#cartaxilostsign').click(() => {
                                CheckStateCar(5);
                            });
                            $('#cartaxilostdongho').click(() => {
                                CheckStateCar(6);
                            });
                            $('#cartaxibusy').click(() => {
                                CheckStateCar(7);
                            });
                            $('#cartaxistop').click(() => {
                                CheckStateCar(8);
                            });

                            // xử lý hiệu ứng màu của row table
                            $("#tablecarmap").delegate("tbody>tr", "click", function () {
                                $(this).addClass("selected").siblings().removeClass("selected");
                            });
                        }
                    }
                }

                AdModule.controller('MapSunController', MapSunController);
            }
        }
    }
}
