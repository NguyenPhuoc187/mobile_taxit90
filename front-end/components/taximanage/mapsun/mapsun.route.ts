module App{
    export module Components{
        export module Taximanage{
            export module MapSun{
                export class MapSunRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper, global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'mapsun',
                            config: {
                                controller: Taximanage.MapSun.MapSunController,
                                templateUrl: '/components/taximanage/mapsun/mapsun.html'+`?v=${global.Version}`,
                                url: '/ban-do-taxi-II',
                                tag:{
                                    title:'Bản đồ taxi II',
                                    icon:'icon-map4',
                                    groupId:'taxi',
                                    index:0
                                }
                            }
                        });
                    }
                }
                AdModule.run(MapSunRouteModel);
            }
        }
    }
}
