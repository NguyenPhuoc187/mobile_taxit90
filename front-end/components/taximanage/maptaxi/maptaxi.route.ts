/**
 * Created by NPPRO on 25/11/2016.
 */
module App{
    export module Components{
        export module Taximanage{
            export module Maptaxi{
                export class MaptaxiRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper, global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'maptaxi',
                            config: {
                                controller: Taximanage.Maptaxi.MaptaxiController,
                                templateUrl: '/components/taximanage/maptaxi/maptaxi.html'+`?v=${global.Version}`,
                                url: '/map-ban-do-taxi',
                                tag:{
                                    title:'Bản đồ taxi',
                                    icon:'icon-map4',
                                    groupId:'taxi',
                                    index:0
                                }
                            }
                        });
                    }
                }
                AdModule.run(MaptaxiRouteModel);
            }
        }
    }
}