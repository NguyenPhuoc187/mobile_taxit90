/**
 * Created by NPPRO on 25/11/2016.
 */
/// <reference path="../../../../tstyping/chartjs/chart.d.ts"/>
module App{
    export module Components{
        export module Taximanage{
            export module Maptaxi{
                export interface IMaptaxiScope extends ng.IScope{
                    map:any;
                    areaManager:any;
                    pointManager:any;
                    distanceManager:any;
                    getdirectionManager:any;
                    cpnSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.ICompany>; // công ty
                    groupSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IGroup>; // đội xe

                    gridCarHetPhi: App.Shared.Directives.GridviewControl;
                    gridListTaxiCars: App.Shared.Directives.GridviewControl;
                    gridDsdiemtaxi: App.Shared.Directives.GridviewControl;
                    gridDsvungtaxi: App.Shared.Directives.GridviewControl;
                    gridDsviphamtaxi: App.Shared.Directives.GridviewControl;
                    gridDsxechuachotcataxi: App.Shared.Directives.GridviewControl;
                    gridDscanhbaotaxi: App.Shared.Directives.GridviewControl;

                    gridCarOverSpeed_taxi: App.Shared.Directives.GridviewControl; //ds xe quá tốc đố
                    gridCarQuaThoiGian: App.Shared.Directives.GridviewControl; //ds xe quá tốc đố


                    stateSelectSettings:any;
                    stateTagSelect:(event:any)=>any;
                    StateCar:any;
                    stateId:any;
                    dtTimeSelectMap:any;
                    count_cars:any;
                    // Object
                    cartaxiManager: App.Shared.Library.CarTaxiManager;
                    timerGetDevice:any;
                    companyName:any;

                    autocompleteOptions:any;
                    location_cpn:any;
                    check_location:any;
                    place:any;
                    companyId:any;
                    click_searchcar:()=>any;
                    carjourneyreports_Serial:any;
                    car_autocomplete_Serial:any;
                    count_carstate:any;
                    sumcar:any;
                    myChart_maptaxi:any;
                    init:()=>any;


                    //khai báo biến hiển thị tiện ích
                    showtienich:(name:string,value:boolean)=>any;
                    showdsxe:boolean; // hiển thị ds xe
                    showtocdo:boolean; // hiển thị thông báo tốc độ
                    showdiem:boolean; // hiển thị điểm
                    showvung:boolean; // hiển thị vùng
                    showchuthich:boolean;
                    showtglt:boolean; // hiển thị thông báo xe quá thời gian liên tục
                    showtimdiachi:boolean; // hiển thị thông báo xe quá thời gian liên tục


                    doanhthubinhquan:any;
                    doanhthutong:any;
                    hieusuat:number;
                    xetrongvung:number;
                    xengoaivung:number;

                    buttons:any;
                    myVar:string;
                    showeventbutton:(event:string) => any;
                    cartaxi_array:any;
                    clickselect:(item:any)=>void;
                    sordBy:(name:string) => void;
                    propertyName:string;
                    reverse:boolean;

                    //danh sách biến hiển thị thông báo bên ngoài bản đồ
                    cartaxioverspeed:any;
                    cartaxisos:any;
                    cartaxiovertime:any;
                    carvipham:any;
                    carcanhbao:any;
                    carchuachotca:any;
                    AdMap:any;
                    isshowbuttonmenu:boolean;


                    titlematGPS:string;
                    titlematdongho:string;
                    titlematlienlac:string;

                }
                export class MaptaxiController{
                    public static $inject = ['$scope','language','global', '$http','$q','api','$timeout','$interval','orderByFilter'];
                    constructor($scope: IMaptaxiScope, language: App.Shared.LanguageService, global: App.Shared.GlobalService,
                                $http: ng.IHttpService,public $q:ng.IQService,api:App.Shared.Api.IApi,$timeout:ng.ITimeoutService, $interval:ng.IIntervalService,orderByFilter: ng.IFilterOrderBy){
                        // var timereset_taxi = setInterval(()=>{
                        //     clearInterval(timereset_taxi);
                        //     location.reload();
                        // },1800000);
                        $('#controlMap1').boxWidget({
                            animationSpeed: 500,
                            collapseTrigger: '#my-collapse-button-trigger',
                            removeTrigger: '#my-remove-button-trigger',
                            collapseIcon: 'fa-minus',
                            expandIcon: 'fa-plus',
                            removeIcon: 'fa-times'
                        });
                        $('[data-popup="tooltip"]').tooltip();
                        var timeshowquathoigian:any;
                        var snd = new Audio("../../assets/music/dieu_xe.mp3"); // buffers automatically when created
                        var timereset = $interval(()=>{
                            $interval.cancel(timereset);
                            if(window.location.pathname == '/map-ban-do-taxi') {
                                location.reload();
                                window.location.href = `http://${window.location.host}/map-ban-do-taxi`;
                            }
                            //30 min =1800000
                        },1800000);

                        timeshowquathoigian = $interval(function(){
                            // $interval.cancel(timeshowquathoigian);
                            // clearInterval(timeshowquathoigian);
                            // console.log('chay timeout');
                            // console.log($scope.cartaxiovertime);
                            if($scope.cartaxiovertime !== undefined){
                                if($scope.cartaxiovertime.length > 0){
                                    $('#xequathoigian').show();
                                }else{
                                    $('#xequathoigian').hide();
                                }
                            }
                            // if($scope.groupSelectSettings.itemSelect() !== undefined){
                            //     if($scope.carManager !== undefined){
                            //         $scope.cartaxisos = $scope.cartaxiManager.getCarSOS($scope.groupSelectSettings.itemSelect().Name);
                            //         if($scope.cartaxisos !== undefined){
                            //             if($scope.cartaxisos.length > 0){
                            //                 snd.play();
                            //                 $('#danhsachsos').show();
                            //                 // $('#xequathoigian').show();
                            //             }else{
                            //                 snd.pause();
                            //                 snd.currentTime = 0;
                            //                 $('#danhsachsos').hide();
                            //                 // $('#xequathoigian').hide();
                            //             }
                            //         }
                            //     }
                            //
                            // }

                            // if($scope.gridCarQuaThoiGian.data !== undefined){
                            //     if($scope.gridCarQuaThoiGian.data.length > 0){
                            //
                            //         if($('#xequathoigian').is(':visible')) {
                            //             $scope.gridCarQuaThoiGian.refresh();
                            //         }
                            //     }
                            // }else{
                            //
                            // }
                            //5 min = 300000
                        },20000);


                        function hidetrangthaixe(){
                            $('#controlMap1').hide();
                            // $('#giaithich').hide();
                            $('#toolboxButton').hide();
                        }
                        function hidenTienich() {

                            $('#thongkenhanh').hide();
                            $("#danhsachvung").hide();
                            $("#danhsachdiem").hide();
                            $("#danhsachvipham").hide();
                            $("#danhsachchuachot").hide();
                            $("#danhsachcanhbao").hide();
                            $("#danhsachviphamtocdo").hide();
                            $("#xequathoigian").hide();
                            $("#cauhinh").hide();
                            $("#distance_top").hide();
                            $("#danhsachsos").hide();
                        }


                        //khai báo hiển thị bản đồ
                        // $scope.reverse = true;
                        $scope.showdsxe = true;
                        $scope.isshowbuttonmenu = false;
                        $scope.showtocdo = (localStorage.getItem('tocdo') !== null && localStorage.getItem('tocdo') !== undefined && localStorage.getItem('tocdo') !== '')?(localStorage.getItem('tocdo')=== 'true'?true:false):true;
                        $scope.showdiem = false;
                        $scope.showvung = false;
                        $scope.showchuthich = true;// mặc định hiển thị
                        $scope.showtimdiachi = true;// mặc định hiển thị
                        $scope.showtglt = (localStorage.getItem('thoigianlt') !== null && localStorage.getItem('thoigianlt') !== undefined && localStorage.getItem('thoigianlt') !== '')?(localStorage.getItem('thoigianlt')=== 'true'?true:false):true;// mặc định hiển thị


                        $scope.doanhthubinhquan = 0;
                        $scope.doanhthutong = 0;
                        $scope.hieusuat = 0;

                        // $scope.myChart_maptaxi = {};
                        $scope.cpnSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.ICompany>('drCompanyTaxiMap','Display','Id');
                        // Set chiều dài cho combobox
                        $scope.cpnSelectSettings.title = '';
                        $scope.cpnSelectSettings.setting.width = '100%';
                        $scope.cpnSelectSettings.setting.height = 40;
                        $scope.cpnSelectSettings.setting.checkboxes = false;
                        $scope.cpnSelectSettings.setting.filterable = true;
                        $scope.cpnSelectSettings.setting.filterPlaceHolder = 'Tìm công ty...';
                        $scope.cpnSelectSettings.setting.placeHolder = 'Chọn công ty...';


                        // Khai báo combobox đội xe
                        $scope.groupSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('drGroupTaxiMap', 'Name', 'Id');
                        $scope.groupSelectSettings.title = '';
                        $scope.groupSelectSettings.setting.width = '100%';
                        $scope.groupSelectSettings.setting.height = 40;
                        $scope.groupSelectSettings.setting.checkboxes = false;
                        $scope.groupSelectSettings.setting.filterable = true;
                        $scope.groupSelectSettings.setting.filterPlaceHolder = 'Tìm đội xe...';
                        $scope.groupSelectSettings.setting.placeHolder = 'Chọn đội xe...';



                        //Khai báo lịch
                        $scope.dtTimeSelectMap = new App.Shared.Gui.DateTimeControl('dtTimeSelectMasterTaxiMap');
                        $scope.dtTimeSelectMap.titleTime = '';
                        $scope.dtTimeSelectMap.open = 'right';
                        $scope.dtTimeSelectMap.limitday = 2;

                        // $scope.companyId_ = 11;
                        $scope.companyId = {};
                        $scope.location_cpn = [];
                        $scope.check_location = {};
                        $scope.autocompleteOptions = {
                            componentRestrictions: {country: 'VN'},
                            types: ['geocode']
                        };
                        $scope.place = '';
                        $scope.count_cars = {
                            state0: 0,//tất cả xe
                            state1: 0, // xe có khách
                            state2: 0, // xe không khách
                            state3: 0, // xe đỗ
                            state4: 0, // xe mất GPS
                            state5: 0, // xe mất liên lạc
                            state6: 0, // xe mất đồng hồ
                            state7: 0, // xe bận
                            state8: 0, // xe dừng
                        };
                        var contextMenu:any = null;
                        /*Hàm hủy controller*/
                        $scope.$on("$destroy", ()=> {
                            if(timereset !== null){
                                $interval.cancel(timereset);
                            }
                            if(timeshowquathoigian !== null){
                                $interval.cancel(timeshowquathoigian);
                            }

                            if ($scope.timerGetDevice != null || $scope.timerGetDevice != undefined){
                                // $interval.cancel($scope.timerGetDevice);
                                clearInterval($scope.timerGetDevice);
                            }
                            // clearInterval(timereset_taxi);
                            // $scope.cpnSelectSettings.jqxDropDownList('destroy');
                            $scope.cpnSelectSettings.setting.jqxDropDownList('destroy');
                            $scope.cartaxiManager.clearAllCars();
                        });
                        /**
                         * Hàm xây dựng layout
                         */
                        function buildLayout() {

                            $scope.myVar = 'closed';
                            $scope.buttons = [
                                {
                                    label: 'Cấu hình',
                                    event:'cauhinh',
                                    icon: 'icon-cog6',
                                },{
                                    label: 'Xe chưa chốt ca trong ngày',
                                    event:'danhsachchuachot',
                                    icon: 'icon-car2'
                                },{
                                    label: 'Thống kê nhanh',
                                    event:'thongkenhanh',
                                    icon: 'icon-chart'
                                },{
                                    label: 'Cảnh báo',
                                    event:'danhsachcanhbao',
                                    icon: 'icon-warning'
                                },{
                                    label: 'Danh sách xe vi phạm',
                                    event:'dsxevipham',
                                    icon: 'icon-notification2'

                                },{
                                    label: 'Danh sách vùng',
                                    event:'listArea',
                                    icon: 'icon-pie-chart4',
                                },
                                {
                                    label: 'Danh sách điểm',
                                    event:'listPoint',
                                    icon: 'icon-location4'
                                },
                                // ,{
                                //     label: 'Thông tin xe',
                                //     event:'searchCar',
                                //     icon: 'icon-search4'
                                // }
                            ];
                            $scope.showeventbutton = (name:string) =>{
                                var effect:string = 'fold';
                                var options:any = {direction: ''};
                                var duration:any = 800;
                                switch (name){
                                    case 'listArea':
                                        options = {
                                            direction: 'left',
                                            complete: ()=> {
                                                //Load dữ liệu vùng
                                                $scope.areaManager.checkCarInArea($scope.cartaxiManager.getCarForGroup($scope.groupSelectSettings.itemSelect().Name));
                                                var data_area:any = [];
                                                try {
                                                    $.each($scope.areaManager.areaArray, (index:number, value:any)=> {
                                                        data_area.push({Sxe: value.value.carContains});
                                                    });
                                                    $scope.gridDsvungtaxi.data = $scope.areaManager.areaArray;
                                                    // console.log('log vùng');
                                                    $.each($scope.gridDsvungtaxi.data, (ind:number, val:any)=> {
                                                        val.count_cars = data_area[ind].Sxe;
                                                    });
                                                    // console.log($scope.gridDsvungtaxi.data);
                                                    $scope.gridDsvungtaxi.refresh();
                                                } catch (e) {
                                                    console.log('lỗi ko có dữ liệu Sxe');
                                                }

                                            }
                                        };

                                        hidenTienich();
                                        $('#danhsachvung').toggle(effect, options, duration);

                                        $('#close_danhsachvung').click(function () {
                                            $('#danhsachvung').hide();
                                        });
                                        break;
                                    case 'listPoint':
                                        options = {
                                            direction: 'left',
                                            complete: ()=> {
                                                /*Lấy danh sách điểm*/
                                                $scope.pointManager.checkCarInPoint($scope.cartaxiManager.getCarForGroup($scope.groupSelectSettings.itemSelect().Name));
                                                var data_point:any = [];
                                                // console.log($scope.pointManager);
                                                $.each($scope.pointManager.pointArray, (index:number, value:any)=> {
                                                    data_point.push({countcars_point: value.value.countInPoint});
                                                });
                                                $scope.gridDsdiemtaxi.data = $scope.pointManager.pointArray;
                                                $.each($scope.gridDsdiemtaxi.data, (ind:number, val:any)=> {
                                                    val.countcars_point = data_point[ind].countcars_point;
                                                });
                                                $scope.gridDsdiemtaxi.refresh();
                                            }
                                        };

                                        $scope.show_allpoint = (eventname:any) => {

                                            if(eventname.originalEvent.srcElement.checked){
                                                $scope.pointManager.show(true);
                                            } else {
                                                $scope.pointManager.show(false);
                                            }

                                        };
                                        hidenTienich();
                                        $('#danhsachdiem').toggle(effect, options, duration);
                                        $('#close_danhsachdiem').click(function () {
                                            $('#danhsachdiem').hide();
                                        });
                                        break;

                                    case 'dsxevipham':
                                        options = {
                                            direction: 'left',
                                            complete: ()=> {
                                                // $scope.gridDsviphamtaxi.refresh();
                                            }
                                        };
                                        hidenTienich();
                                        $('#danhsachvipham').toggle(effect, options, duration);

                                        $('#close_danhsachvipham').click(function () {
                                            $('#danhsachvipham').hide();
                                        });
                                        break;
                                    case 'danhsachchuachot':
                                        options = {
                                            direction: 'left',
                                            complete: ()=> {

                                                // $scope.gridDsxechuachotcataxi.refresh();
                                            }
                                        };
                                        hidenTienich();
                                        $('#danhsachchuachot').toggle(effect, options, duration);
                                        $('#close_danhsachchuachot').click(function () {
                                            $('#danhsachchuachot').hide();
                                        });
                                        break;
                                    case 'thongkenhanh':
                                        options = {
                                            direction: 'left',
                                            complete: ()=> {
                                                if($scope.cartaxiManager !== undefined){
                                                    var data_thongke = $scope.cartaxiManager.getCarForGroup($scope.groupSelectSettings.itemSelect().Name);
                                                    if(data_thongke.length > 0){
                                                        Sugar.Number.setOption('decimal',',');
                                                        Sugar.Number.setOption('thousands','.');

                                                        let tong:number = 0;
                                                        $.each(data_thongke,(ind:number, val:any)=>{
                                                            tong = tong + val.TotalMoney;
                                                        });
                                                        var binhquan: any =  Sugar.Number(Math.round(tong / $scope.sumcar)).format(0);
                                                        $scope.doanhthutong = Sugar.Number(tong).format(0).raw;
                                                        $scope.doanhthubinhquan = binhquan.raw;
                                                        $scope.hieusuat = Math.round(($scope.count_cars.state1/ $scope.sumcar)*100);

                                                    }
                                                    $scope.$apply();
                                                    VeBieuDo();
                                                }

                                            }
                                        };
                                        hidenTienich();
                                        $('#thongkenhanh').toggle(effect, options, duration);
                                        $('#close_thongkenhanh').click(function () {
                                            $('#thongkenhanh').hide();
                                        });
                                        break;

                                    case 'danhsachcanhbao':
                                        options = {
                                            direction: 'left',
                                            complete: ()=> {
                                                // $scope.gridDscanhbaotaxi.refresh();
                                            }
                                        };
                                        hidenTienich();
                                        $('#danhsachcanhbao').toggle(effect, options, duration);
                                        $('#close_danhsachcanhbao').click(function () {
                                            $('#danhsachcanhbao').hide();
                                        });
                                        break;
                                    case 'cauhinh':
                                        hidenTienich();
                                        $('#cauhinh').toggle(effect, options, duration);
                                        $('#close_cauhinh').click(function () {
                                            $('#cauhinh').hide();
                                        });
                                        break;
                                    // case 'searchCar':
                                    //     $('#Panel_listcar').toggle(effect, options, duration);
                                    //     $('#close_rightpanel_listcar').click(function () {
                                    //         $('#Panel_listcar').hide();
                                    //     });
                                    //     break;
                                }
                            };

                            $scope.titlematGPS = 'Xe mất GPS';
                            $scope.titlematdongho = 'Xe mất đồng hồ';
                            $scope.titlematlienlac = 'Xe mất liên lạc';


                            $scope.$on('$viewContentLoaded', ()=> {
                                //Here your view content is fully loaded !!


                            });
                            /*Khai báo drop-list trạng thái*/
                            var state:any = [
                                {
                                    id: 0,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #F4A460'></span> - Tất cả"
                                },
                                {
                                    id: 1,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #3465aa'></span> - Có khách"
                                },
                                {
                                    id: 2,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #dc0030'></span> - Không khách"
                                },
                                {
                                    id: 3,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #EB2271'></span> - Đỗ"
                                },
                                {
                                    id: 4,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #09a275'></span> - Mất GPS"
                                },
                                {
                                    id: 5,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #2a2a2a'></span> - Mất liên lạc"
                                },
                                {
                                    id: 6,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #2c3e50'></span> - Mất đồng hồ"
                                },
                                {
                                    id: 7,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #e74c3c'></span> - Bận"
                                },
                                {
                                    id: 8,
                                    name: "<span style='width:4px; height4px; margin-right:2px ;border: 4px solid #f2b701'></span> - Dừng"
                                }];
                            $scope.stateSelectSettings = {
                                width: '100%',
                                height: '40px',
                                placeHolder: 'Trạng thái xe',
                                displayMember: "name",
                                source: state,
                            };

                            /*Xử lý hàm chuyển trạng thái */
                            $scope.stateId = state[0];
                            $scope.stateTagSelect = (event) => {
                                try{
                                    if (event.args) {
                                        var item = event.args.item;
                                        if (item) {
                                            $scope.stateSelectSettings.valueMember = item.originalItem.id;
                                            $scope.StateCar($scope.stateSelectSettings.valueMember, $scope.groupSelectSettings.itemSelect().Name);
                                        }
                                    }
                                }
                                catch (e){
                                    //console.error(e);
                                }
                            };

                            $scope.clickselect = (item:any)=>{
                                $('#controlMap1').boxWidget('collapse');
                                // console.log('chay hieu ung');
                                item.focus();
                                // item.CallbackOnClick();
                            };

                            $scope.propertyName = 'SoHieu_';
                            $scope.sordBy = (name) => {
                                console.log('chay order');
                                $scope.reverse = (name !== null && $scope.propertyName === name)
                                    ? !$scope.reverse : false;
                                $scope.propertyName = name;
                                // $scope.cartaxi_array = orderByFilter($scope.cartaxi_array, $scope.propertyName, $scope.reverse);
                            };



                            /*Khai báo ds điểm*/
                            $scope.gridDsdiemtaxi = new App.Shared.Directives.GridviewControl("gridDsdiemtaxi");
                            $scope.gridDsdiemtaxi.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            //Tắt tính năng excel
                            $scope.gridDsdiemtaxi.isExport = true;
                            $scope.gridDsdiemtaxi.columns = [];
                            $scope.gridDsdiemtaxi.height = "310px";
                            $scope.gridDsdiemtaxi.columnsHeight = 40;
                            $scope.gridDsdiemtaxi.columns.push({
                                text: 'Tên điểm',
                                dataField: 'Name',
                                type: 'string',
                                align: 'center',
                                width: '80px',
                            });
                            $scope.gridDsdiemtaxi.columns.push({
                                text: 'Chú thích',
                                dataField: 'speed',
                                type: 'string',
                                align: 'center',
                                width: '100px',
                            });
                            $scope.gridDsdiemtaxi.columns.push({text: 'Số xe', dataField: 'countcars_point', type: 'string', align: 'center', width: 'auto'});


                            $scope.gridDsdiemtaxi.event.onDoubleClick = (ne:any) => {
                                //console.log(ne);
                                var point:any = $scope.pointManager.get(ne.Name);
                                point.show();
                                point.focus();
                                $("#Panel_listPoint").hide();
                                //area.polygons.setMap(map);
                            };

                            //Khai báo danh sách vùng
                            $scope.gridDsvungtaxi = new App.Shared.Directives.GridviewControl("gridDsvungtaxi");
                            $scope.gridDsvungtaxi.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            //Tắt tính năng excel
                            $scope.gridDsvungtaxi.isExport = true;
                            $scope.gridDsvungtaxi.columns = [];
                            $scope.gridDsvungtaxi.height = "310px";
                            $scope.gridDsvungtaxi.columnsHeight = 30;
                            $scope.gridDsvungtaxi.columns.push({
                                text: 'Tên vùng',
                                dataField: 'Name',
                                type: 'string',
                                align: 'center',
                                width: '80px',
                            });
                            $scope.gridDsvungtaxi.columns.push({
                                text: 'Chú thích',
                                dataField: 'speed',
                                type: 'string',
                                align: 'center',
                                width: '100px',
                            });
                            $scope.gridDsvungtaxi.columns.push({text: 'Số xe', dataField: 'count_cars', type: 'string', align: 'center', width: 'auto'});

                            $scope.gridDsvungtaxi.event.onDoubleClick = (ne:any) => {
                                var area:any = $scope.areaManager.get(ne.Name);
                                area.show();
                                area.focus();
                                // $('#Panel_listAreas').hide();
                            };











                            var cellClass_gridCarCare:any = (row:any, dataField:any, cellText:any, rowData:any)=> {
                                var cellValue:any = rowData[dataField];
                                var dateString:string = rowData['EndTime_show']; // Oct 23
                                var dateParts:any = dateString.split("/");
                                //định dạng format chuỗi 'dd/mm/yyyy' thành object ngày
                                var dateObject:any = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                                var cellValueEndtime:any = Math.floor((dateObject - Date.now())/86400000);
                                switch (dataField) {
                                    case "DisplayId":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                    case "STT":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                    case "Note":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                    case "EndTime_show":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                }

                            };
                            // list danh sách xe quan tâm
                            $scope.gridCarHetPhi = new App.Shared.Directives.GridviewControl("gridCarHetPhi");
                            $scope.gridCarHetPhi.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            /*Tắt tính năng xuất Excel*/
                            $scope.gridCarHetPhi.isExport = true;
                            $scope.gridCarHetPhi.columns = [];
                            $scope.gridCarHetPhi.height = "300px";
                            $scope.gridCarHetPhi.columnsHeight = 40;

                            $scope.gridCarHetPhi.columns.push({
                                text: 'STT',
                                dataField: 'STT',
                                type: 'number',
                                align: 'center',
                                width: '35px',
                                cellClassName: cellClass_gridCarCare
                            });
                            $scope.gridCarHetPhi.columns.push({
                                text: 'Số tài - Biển số',
                                dataField: 'DisplayId',
                                type: 'string',
                                align: 'center',
                                width: '120px',
                                cellClassName: cellClass_gridCarCare
                            });
                            $scope.gridCarHetPhi.columns.push({
                                text: 'Thời gian',
                                dataField: 'EndTime_show',
                                type: 'string',
                                align: 'center',
                                width: '90px',
                                cellClassName: cellClass_gridCarCare
                            });
                            $scope.gridCarHetPhi.columns.push({
                                text: 'Thông báo',
                                dataField: 'Note',
                                type: 'string',
                                align: 'center',
                                cellClassName: cellClass_gridCarCare
                            });





                            /*Chỉnh hiệu ứng cho trang bản đồ*/
                            var effect:string = 'fold';
                            var options:any = {direction: ''};
                            var duration:any = 800;





                            /*Tắt đo khoảng cách*/
                            $('#close_distance').click(function () {
                                $("#distance_top").hide();
                                document.getElementById("menu_item_distance").innerHTML =
                                    '<span><i class="icon-move-horizontal"></i> Đo khoảng cách</span>';
                                $scope.distanceManager.deleteDistance();

                            });

                            if($scope.showdsxe){
                                shownTienich();
                            }
                            if($scope.showtocdo){
                                $('#danhsachviphamtocdo').show();
                            }
                            if($scope.showtglt){
                                $('#xequathoigian').show();
                            }
                            if ($scope.showchuthich){
                                $('#giaithich').show();
                            }
                            if ($scope.showtimdiachi){
                                $('#search_location_taxi').show();
                            }



                            hidenTienich();
                            hidetrangthaixe();
                            $('#giaithich').hide();
                            function shownTienich() {
                                $('#controlMap1').show();
                                $('#toolboxButton').show();
                            }



                            $('#toolboxButton').click(function () {
                                $('#toolboxButton').hide();
                                $('.slide-toggle').show(700);
                                options = {direction: 'left'};
                                $('#controlMap1').toggle(effect, options, duration);
                            });
                            // Hiện bảng tiện ích
                            $(".slide-toggle").click(function () {
                                $('.slide-toggle').hide();
                                options = {direction: 'left'};
                                $('#controlMap1').toggle(effect, options, duration);
                                // $scope.gridListCars.refresh();
                                $('#toolboxButton').show(700);
                            });

                            // $('#thongkenhanh').on('shown.bs.collapse', function () {
                            //
                            //     VeBieuDo();
                            //     // do something…
                            //     // paintChart();
                            //     // if($scope.myChart_maptaxi !== undefined){
                            //     //     $scope.myChart_maptaxi.destroy();
                            //     // }
                            //     // var ctx = $('#tkxetaximap');
                            //     // $scope.myChart_maptaxi = new Chart(ctx, {
                            //     //     type: 'pie',
                            //     //     data :{
                            //     //         labels: [
                            //     //             "Xe có khách",
                            //     //             "Xe chạy đón khách",
                            //     //             "Xe chờ tại điểm",
                            //     //             "Xe đỗ ngoài"
                            //     //         ],
                            //     //         datasets: [
                            //     //             {
                            //     //                 data: [300, 50, 100,60],
                            //     //                 backgroundColor: [
                            //     //                     "#FF6384",
                            //     //                     "#36A2EB",
                            //     //                     "#FFCE56",
                            //     //                     "#2ecc71"
                            //     //                 ],
                            //     //                 hoverBackgroundColor: [
                            //     //                     "#FF6384",
                            //     //                     "#36A2EB",
                            //     //                     "#FFCE56"
                            //     //                 ]
                            //     //             }]
                            //     //     }
                            //     // });
                            // });
                            // $('#danhsachdiem').on('shown.bs.collapse', function () {
                            //     /*Lấy danh sách điểm*/
                            //     $scope.pointManager.checkCarInPoint($scope.cartaxiManager.getCarForGroup($scope.groupSelectSettings.itemSelect().Name));
                            //     var data_point:any = [];
                            //     // console.log($scope.pointManager);
                            //     $.each($scope.pointManager.pointArray, (index:number, value:any)=> {
                            //         data_point.push({countcars_point: value.value.countInPoint});
                            //     });
                            //     $scope.gridDsdiemtaxi.data = $scope.pointManager.pointArray;
                            //     $.each($scope.gridDsdiemtaxi.data, (ind:number, val:any)=> {
                            //         val.countcars_point = data_point[ind].countcars_point;
                            //     });
                            //     $scope.gridDsdiemtaxi.refresh();
                            // });


                            // $('#danhsachvung').on('shown.bs.collapse', function () {
                            //     $scope.areaManager.checkCarInArea($scope.cartaxiManager.getCarForGroup($scope.groupSelectSettings.itemSelect().Name));
                            //     var data_area:any = [];
                            //     try {
                            //         $.each($scope.areaManager.areaArray, (index:number, value:any)=> {
                            //             data_area.push({Sxe: value.value.carContains});
                            //         });
                            //         $scope.gridDsvungtaxi.data = $scope.areaManager.areaArray;
                            //         console.log('log vùng');
                            //         $.each($scope.gridDsvungtaxi.data, (ind:number, val:any)=> {
                            //             val.count_cars = data_area[ind].Sxe;
                            //         });
                            //         console.log($scope.gridDsvungtaxi.data);
                            //         $scope.gridDsvungtaxi.refresh();
                            //     } catch (e) {
                            //         console.log('lỗi ko có dữ liệu Sxe');
                            //     }
                            // });
                            // $('#danhsachvipham').on('shown.bs.collapse', function () {
                            //
                            //     $scope.gridDsviphamtaxi.refresh();
                            // });
                            // $('#xechuachottrongngay').on('shown.bs.collapse', function () {
                            //     // AdapterDanhsachchotca($scope.gridDsxechuachotcataxi.data);
                            //     $scope.gridDsxechuachotcataxi.refresh();
                            // });

                            $("#showmodalthongke").click(function (){
                                $("#modal_tienich").modal('show');
                            });
                            // $('#modal_tienich').on('shown.bs.collapse', function () {
                            //
                            //     $scope.gridDscanhbaotaxi_modal.refresh();
                            // });

                            // $('#luachon').on('shown.bs.collapse', function () {
                            //     // do something…
                            //     // paintChart();
                            //     if(!$scope.showdsxe){
                            //         hidenTienich();
                            //     }
                            //
                            // });
                            $scope.showtienich = (name,value)=>{
                                switch (name){
                                    case 'dsxe':
                                        if(!value){
                                            hidetrangthaixe();
                                        }else{
                                            shownTienich();
                                        }
                                        $scope.showdsxe = value;
                                        break;
                                    case 'tocdo':
                                        localStorage.setItem('tocdo',`${value}`);
                                        if(!value){
                                            $('#danhsachviphamtocdo').hide();
                                        }else{
                                            $('#danhsachviphamtocdo').show();
                                        }
                                        $scope.showdiachi = value;
                                        break;
                                    case 'chuthich':
                                        if(!value){
                                            $('#giaithich').hide();
                                        }else{
                                            $('#giaithich').show();
                                        }
                                        break;
                                    case 'diem':
                                        if(!value){
                                            $scope.pointManager.show(false);
                                        }else{
                                            $scope.pointManager.show(true);
                                        }
                                        break;
                                    case 'vung':
                                        if(!value){
                                            $scope.areaManager.show(false);
                                        }else{
                                            $scope.areaManager.show(true);
                                        }
                                        break;
                                    case 'thoigianlt':
                                        localStorage.setItem('thoigianlt',`${value}`);
                                        if(!value){
                                            $('#xequathoigian').hide();
                                        }else{
                                            $('#xequathoigian').show();
                                        }
                                        break;
                                    case 'timkiemdiachi':
                                        if(!value){
                                            $("#search_location_taxi").hide();
                                        }else{
                                            $("#search_location_taxi").show();
                                        }
                                        break;

                                }
                                // $scope.$apply();
                            }


                        }

                        buildLayout();

                        if(global.IndexReadly.GetValue()==true){
                            var t1 = setInterval(()=> {
                                clearInterval(t1);
                                buildController();
                            }, 500);
                        }
                        global.IndexReadly.Watch=(n:any)=> {
                            var t = setInterval(()=> {
                                clearInterval(t);
                                buildController();
                            }, 500);

                        };
                        return;

                        // hàm xử lý của map
                        function buildController(){
                            // var myStyles =[
                            //     {
                            //         featureType: "poi",
                            //         elementType: "labels",
                            //         stylers: [
                            //             { visibility: "off" }
                            //         ]
                            //     }
                            // ];
                            var vbdID = "Vietbando";
                            var vbdType = new google.maps.ImageMapType({
                                getTileUrl: function (xy, z) {
                                    return "http://mt3.google.com/vt/lyrs=m&z=" + z + "&x=" + xy.x + "&y=" + xy.y; // vietbando
                                },
                                tileSize: new google.maps.Size(256, 256),
                                opacity: 1,
                                maxZoom: 21, minZoom: 0,
                                name: "Adsun",
                                alt: "Bản đồ Việt Nam từ adsun.vn",
                                isPng: true
                            });
                            $scope.map = new google.maps.Map(document.getElementById("maptaxi132"), {
                                center: new google.maps.LatLng(10.777256, 106.6053314),
                                scrollwheel: true,
                                zoom: 13,
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                disableDefaultUI: true,
                                mapTypeControl: false,
                                mapTypeControlOptions: {
                                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                                    position: google.maps.ControlPosition.TOP_RIGHT
                                },
                                backgroundColor: 'hsla(0, 0%, 0%, 0)',
                                panControl: true,
                                panControlOptions: {
                                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                                },
                                zoomControl: true,
                                streetViewControl: false,
                                zoomControlOptions: {
                                    style: google.maps.ZoomControlStyle.DEFAULT,
                                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                                },
                                // disablePanMomentum: true,
                                // styles: myStyles

                            });
                            $scope.map.mapTypes.set(vbdID, vbdType);
                            $scope.map.setMapTypeId(vbdID);

                            // console.log(global.Account.GetValue());
                            var cauhinhtienich:any = global.Account.GetValue().CauHinhDoi;
                            // console.log('cau hinh tien ich',cauhinhtienich);
                            if(cauhinhtienich !== null && cauhinhtienich !== ""){
                                var data = JSON.parse(cauhinhtienich);
                                if(data !== undefined){
                                   if(data.tienichbando !== undefined){
                                       if(data.tienichbando){
                                            $scope.isshowbuttonmenu = true;
                                       }
                                   }else{
                                       $scope.isshowbuttonmenu = true;
                                   }
                                }else{
                                    $scope.isshowbuttonmenu = true;
                                }
                            }else{
                                $scope.isshowbuttonmenu = true;
                            }

                            // $scope.AdMap= global.BuildNewMapCar('maptaxi132',$scope.map);
                            //
                            //
                            // $scope.AdMap.getImageName=(v:any)=>{
                            //
                            //     if(v.IsSos){
                            //         v.gifframeindex++;
                            //         if(v.gifframeindex == 1){
                            //             return 'xesostarget0' ;
                            //         }else{
                            //             v.gifframeindex=0;
                            //             return 'xesostarget1';
                            //         }
                            //     }
                            //     if(v.OverSpeed){
                            //         v.gifframeindex++;
                            //         if(v.gifframeindex == 1){
                            //             return 'xequatocdotarget0' ;
                            //         }else{
                            //             v.gifframeindex=0;
                            //             return 'xequatocdotarget1';
                            //         }
                            //     }
                            //
                            //     if(v.IsTargetCar){
                            //         if (v.LostGsm) {
                            //             v.gifframeindex++;
                            //             if(v.gifframeindex == 1){
                            //                 return 'xelosttarget0' ;
                            //             }else{
                            //                 v.gifframeindex=0;
                            //                 return 'xelosttarget1';
                            //             }
                            //         } else {
                            //             if (!v.MeterOk) {
                            //                 //trạng thái có đồng hồ hay không
                            //                 v.gifframeindex++;
                            //                 if(v.gifframeindex == 1){
                            //                     return 'xematdonghotarget0' ;
                            //                 }else{
                            //                     v.gifframeindex=0;
                            //                     return 'xematdonghotarget1';
                            //                 }
                            //             } else {
                            //                 //trạng thái có đồng hồ
                            //                 if (!v.Gps) {
                            //                     // if(this.OverSpeed){
                            //                     //     $(`canvas#c_${this.serial}`).addClass('xequatocdo');
                            //                     // }
                            //                     v.gifframeindex++;
                            //                     if(v.gifframeindex == 1){
                            //                         return 'xelostgpstarget0' ;
                            //                     }else{
                            //                         v.gifframeindex=0;
                            //                         return 'xelostgpstarget1';
                            //                     }
                            //                 } else {
                            //                     if (v.MeterStatus) {
                            //                         //trường hợp quá tốc độ
                            //                         if (v.OverSpeed) {
                            //                             v.gifframeindex++;
                            //                             if(v.gifframeindex == 1){
                            //                                 return 'xequatocdotarget0' ;
                            //                             }else{
                            //                                 v.gifframeindex=0;
                            //                                 return 'xequatocdotarget1';
                            //                             }
                            //
                            //                             // return 'cokhach';
                            //                         } else {
                            //                             v.gifframeindex++;
                            //                             if(v.gifframeindex == 1){
                            //                                 return 'cartarget0' ;
                            //                             }else{
                            //                                 v.gifframeindex=0;
                            //                                 return 'cartarget1';
                            //                             }
                            //                         }
                            //                         //nếu có khách
                            //                         //return '../../assets/images/carIcon/run_car.png';
                            //                     } else {
                            //                         //nếu ko khách
                            //                         if (v.IsBusy) {
                            //
                            //                             // $(`canvas#c_${this.serial}`).addClass('xequatocdo');
                            //                             return 'kokhach';
                            //                         } else {
                            //                             if (v.Speed !== 0) {
                            //                                 //nếu vận tốc khác không và không có khách
                            //                                 v.gifframeindex++;
                            //                                 if(v.gifframeindex == 1){
                            //                                     return 'xekhongkhachtarget0' ;
                            //                                 }else{
                            //                                     v.gifframeindex=0;
                            //                                     return 'xekhongkhachtarget1';
                            //                                 }
                            //
                            //                                 // return 'kokhach';
                            //                             } else {
                            //                                 if (v.KeyStatus) {
                            //                                     //xe dừng
                            //                                     v.gifframeindex++;
                            //                                     if(v.gifframeindex == 1){
                            //                                         return 'xedungtarget0' ;
                            //                                     }else{
                            //                                         v.gifframeindex=0;
                            //                                         return 'xedungtarget1';
                            //                                     }
                            //                                 } else if (!v.KeyStatus) {
                            //                                     //xe đỗ
                            //                                     v.gifframeindex++;
                            //                                     if(v.gifframeindex == 1){
                            //                                         return 'xedotarget0' ;
                            //                                     }else{
                            //                                         v.gifframeindex=0;
                            //                                         return 'xedotarget1';
                            //                                     }
                            //
                            //                                 }
                            //
                            //                             }
                            //                         }
                            //
                            //                     }
                            //
                            //
                            //                 }
                            //
                            //             }
                            //
                            //         }
                            //     }else{
                            //         if (v.LostGsm) {
                            //             return 'lostgsm';
                            //         } else {
                            //             if (!v.MeterOk) {
                            //                 //trạng thái có đồng hồ hay không
                            //                 return 'matdongho';
                            //             } else {
                            //                 //trạng thái có đồng hồ
                            //                 if (!v.Gps) {
                            //                     // if(this.OverSpeed){
                            //                     //     $(`canvas#c_${this.serial}`).addClass('xequatocdo');
                            //                     // }
                            //                     return 'lostgps';
                            //                 } else {
                            //                     if (v.MeterStatus) {
                            //                         if (v.OverSpeed) {
                            //                             // $(`canvas#c_${v.serial}`).addClass('xequatocdo');
                            //                             return 'cokhach';
                            //                         } else {
                            //                             // $(`canvas#c_${v.serial}`).removeClass('xequatocdo');
                            //                             return 'cokhach';
                            //                         }
                            //                         //nếu có khách
                            //                         //return '../../assets/images/carIcon/run_car.png';
                            //                     } else {
                            //                         //nếu ko khách
                            //                         if (v.IsBusy) {
                            //                             $('canvas.xetarget').removeClass('xetarget');
                            //                             // $(`canvas#c_${this.serial}`).addClass('xequatocdo');
                            //                             return 'kokhach';
                            //                         } else {
                            //                             if (v.Speed !== 0) {
                            //                                 //nếu vận tốc khác không và không có khách
                            //                                 return 'kokhach';
                            //                             } else {
                            //                                 if (v.KeyStatus) {
                            //                                     //xe dừng
                            //                                     return 'xedung';
                            //                                 } else if (!v.KeyStatus) {
                            //                                     //xe đỗ
                            //                                     return 'xedo';
                            //                                 }
                            //
                            //                             }
                            //                         }
                            //
                            //                     }
                            //
                            //
                            //                 }
                            //
                            //             }
                            //
                            //         }
                            //
                            //     }
                            //
                            //     // return 'cokhach';
                            // };
                            //
                            // $scope.AdMap.onLeftClick=(v:any)=>{
                            //     v.onClick();
                            //
                            // };
                            // $scope.AdMap.onRightClick=(v:any)=>{
                            //     v.onRightClick();
                            // };




                            var dsxe = /** @type {HTMLInputElement} */(
                                document.getElementById('controlMap'));
                            $scope.map.controls[google.maps.ControlPosition.LEFT_TOP].push(dsxe);
                            var dstienich = /** @type {HTMLInputElement} */(
                                document.getElementById('tienich'));
                            $scope.map.controls[google.maps.ControlPosition.RIGHT_TOP].push(dstienich);

                            var dsgiaithich = /** @type {HTMLInputElement} */(
                                document.getElementById('giaithich'));
                            $scope.map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(dsgiaithich);
                            var khoangcach = /** @type {HTMLInputElement} */(
                                document.getElementById('distance_top'));
                            $scope.map.controls[google.maps.ControlPosition.TOP_CENTER].push(khoangcach);
                            var searchlocation = /** @type {HTMLInputElement} */(
                                document.getElementById('search_location_taxi'));
                            $scope.map.controls[google.maps.ControlPosition.TOP_CENTER].push(searchlocation);

                            var Panel_listcarwarmtaxi = /** @type {HTMLInputElement} */(
                                document.getElementById('Panel_listcarwarmtaxi'));
                            $scope.map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(Panel_listcarwarmtaxi);

                            // var labeltienich = /** @type {HTMLInputElement} */(
                            //     document.getElementById('labeltienich'));
                            // $scope.map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(labeltienich);


                            var map_ready_idle = false;
                            /*sau khi map load xong thì chạy event này*/
                            google.maps.event.addListener($scope.map, 'idle', (mouseEvent:any)=> {
                                map_ready_idle = true;
                                $('#map_loading').hide();
                                $("#maptaxi132").show();
                                // Buildlayout();
                                // gọi hàm này dể hiển thị map đề phòng trường hơp map trắng phau ko có hình ảnh
                                google.maps.event.trigger($scope.map, 'resize');
                                $timeout(()=>{

                                    google.maps.event.trigger($scope.map, 'resize');
                                },500);
                            });
                            $('#controlMap1').show();
                            $('#toolboxButton').show();
                            $('#giaithich').show();

                            google.maps.event.addListener($scope.map, 'bounds_changed', ()=> {
                                google.maps.event.trigger($scope.map, 'resize');
                                map_ready_idle = false;
                            });
                            var oldcompanyId:number;
                            var eventRightClickOnMapId:google.maps.MapsEventListener;
                            $scope.cpnSelectSettings.loadPromise(api.GetAllCompanyByType(), null,$q).then(()=>{
                                if(localStorage.getItem('companyIdtaxi') !== '' && localStorage.getItem('companyIdtaxi') !== null){
                                    // console.log(localStorage.getItem('companyIdtaxi'));
                                    let companyidtaxi:any = localStorage.getItem('companyIdtaxi');
                                    oldcompanyId = companyidtaxi;
                                    $scope.cpnSelectSettings.setItemDefault((m:any)=>m.Id === companyidtaxi*1);
                                }else{
                                    $scope.cpnSelectSettings.setting.jqxDropDownList('selectIndex',0);
                                }
                            });
                            //Sự kiện chọn công ty
                            $scope.cpnSelectSettings.customEvent.onSelectOnItem = (index:number, val:App.Shared.Entity.ICompany)=> {
                                if(val.Id == 10075){
                                    $scope.titlematGPS = 'Xe tạm nghỉ';
                                    $scope.titlematdongho = 'Xe nghỉ kinh doanh';
                                    $scope.titlematlienlac = ' Xe đang bảo dưỡng, sửa chữa';
                                }else{
                                    $scope.titlematGPS = 'Xe mất GPS';
                                    $scope.titlematdongho = 'Xe mất đồng hồ';
                                    $scope.titlematlienlac = 'Xe mất liên lạc';
                                }
                                // console.log('hello');
                                localStorage.setItem('companyIdtaxi',`${val.Id}`,);

                                var first = Array<App.Shared.Entity.IGroup>();
                                // đẩy 1 group làm thông tin chọn tất cả
                                first.push({
                                    Id: -1,
                                    Name: 'Tất cả đội xe',
                                    CompanyId: val.Id
                                });
                                $scope.groupSelectSettings.loadPromise(api.GetAllGroupByCompanyId(val.Id), first,$q).then(()=>{
                                    // console.log(sessionStorage.getItem('groupId'));

                                    if(sessionStorage.getItem('groupId') !== undefined && sessionStorage.getItem('groupId') !== null && sessionStorage.getItem('groupId') !== ''){
                                        // console.log(localStorage.getItem('companyIdtaxi'));
                                        let groupname:any = sessionStorage.getItem('groupId');
                                        // oldcompanyId = companyidtaxi;
                                        $scope.groupSelectSettings.setItemDefault((m:any)=>m.Name === groupname);
                                    }else{
                                        $scope.groupSelectSettings.setting.jqxDropDownList('selectIndex',0);
                                    }
                                });
                                $scope.map.setCenter(new google.maps.LatLng(val.Lat, val.Lng));



                                $scope.companyName = val.Name;

                                /*Khai báo tạo chuột phải bản đồ và các chức năng*/

                                if ($scope.areaManager != null || $scope.areaManager != undefined) {
                                    $scope.areaManager.clear();
                                }
                                else {
                                    $scope.areaManager = new App.Shared.Library.AreaManage($scope.map, $q, val.Id, $http, global);
                                }

                                $scope.areaManager.load(val.Id);

                                if ($scope.pointManager != null || $scope.pointManager != undefined) {
                                    $scope.pointManager.clear();

                                }
                                else
                                    $scope.pointManager = new App.Shared.Library.PointManage($scope.map, $q, val.Id, $http, global);

                                $scope.pointManager.load(val.Id);

                                if ($scope.distanceManager != null || $scope.distanceManager != undefined) {
                                    $scope.distanceManager.deleteDistance();
                                }
                                else
                                    $scope.distanceManager = new App.Shared.Library.DistanceMapManage($scope.map, $q);

                                /*Khởi tạo biến chứa tìm đường*/
                                $scope.getdirectionManager = new App.Shared.Library.Getdirection($scope.map, $q);

                                /*Khởi tạo lớp quản lý context menu
                                 * Thông sô truyền vào sẽ bổ sung thêm khi cần
                                 * mặc định hiện tại chỉ có lớp map*/
                                if (contextMenu == null)
                                    contextMenu = new App.Shared.Library.MenuMap({
                                        map: $scope.map,
                                        manageArea: $scope.areaManager,
                                        managePoint: $scope.pointManager,
                                        manageMapDistance: $scope.distanceManager,
                                        getdirection: $scope.getdirectionManager
                                    });

                                contextMenu.contextMenu.setMapInstance($scope.map);

                                /*Chuột phải vào bản đồ*/
                                if (eventRightClickOnMapId != null || eventRightClickOnMapId != undefined)
                                    google.maps.event.removeListener(eventRightClickOnMapId);
                                eventRightClickOnMapId = google.maps.event.addListener($scope.map, 'rightclick', (mouseEvent:any)=> {
                                    // console.log(`map right click`);
                                    if (($scope.map as any).FisrtRightClick == true) {
                                        {
                                            ($scope.map as any).FisrtRightClick = false;
                                            return;
                                        }
                                    }
                                    contextMenu.contextMenu.show(mouseEvent.latLng);
                                });


                                oldcompanyId = val.Id;
                                clearInterval($scope.timerGetDevice);
                                // $scope.timerGetDevice = setInterval(()=> {
                                //     console.log('clear ');
                                //     if (!map_ready_idle) return;
                                //     if (oldcompanyId != val.Id) {
                                //         console.log(`map : Clear all car of company id ${oldcompanyId}`);
                                //         oldcompanyId = val.Id;
                                //         // thay đổi công ty thì clear xe trên bản đồ
                                //         $scope.cartaxiManager.clearAllCars();
                                //     }
                                //     //$.holdReady(true);
                                //     $scope.cartaxiManager.getAllCarByCompanyId(oldcompanyId, $http, global, $scope.gridListTaxiCars, $scope.companyName,$scope.groupSelectSettings.itemSelect().Id).then((s:boolean)=> {
                                //         // console.log($scope.groupSelectSettings.itemSelect().Name);
                                //         //FilterGroup($scope.groupSelectSettings.itemSelect().Name);
                                //         $scope.StateCar($scope.stateSelectSettings.valueMember, $scope.groupSelectSettings.itemSelect().Name);
                                //         // $.holdReady(false);
                                //     });
                                //
                                //
                                //     let gridCarquatocdo:any = $scope.cartaxiManager.getCarOverSpeed($scope.groupSelectSettings.itemSelect().Name);
                                //
                                //     if(gridCarquatocdo.length > 0){
                                //         // checklengthcaroverspeed = gridCarquatocdo.length;
                                //         $scope.gridCarOverSpeed_taxi.data = gridCarquatocdo;
                                //         $scope.gridCarOverSpeed_taxi.refresh();
                                //         $('#danhsachviphamtocdo').show();
                                //         // $('#Panel_listcarwarmtaxi').show();
                                //         if($('#danhsachviphamtocdo').is(':visible')) {
                                //             $scope.gridCarOverSpeed_taxi.refresh();
                                //         }
                                //     }else{
                                //         $scope.gridCarOverSpeed_taxi.clear();
                                //         $('#danhsachviphamtocdo').hide();
                                //     }
                                //
                                //     // lấy dữ liệu xe quá thời gian liên tục
                                //     let gridQuathoigian:any = $scope.cartaxiManager.getCarQuathoigian($scope.groupSelectSettings.itemSelect().Name);
                                //     GetDataQuaThoiGian();
                                //
                                //
                                // }, 10000);
                            };
                            // Sự kiện chọn đội xe
                            $scope.groupSelectSettings.customEvent.onSelectOnItem = (index:number, val:App.Shared.Entity.IGroup)=> {
                                //todo: đoạn này comment khi up code lên route 1
                                /*Tạo dữ liệu xe trên map*/
                                sessionStorage.setItem('groupId',val.Name);
                                if ($scope.cartaxiManager != null || $scope.cartaxiManager != undefined){
                                    $scope.cartaxiManager.clearAllCars();
                                    // $scope.AdMap.clearLayer();
                                } else {
                                    $scope.cartaxiManager = new Shared.Library.CarTaxiManager({map: $scope.map, q: $q, $http: $http, global});
                                    App.Shared.Library.GlobalCarTaxiManager = $scope.cartaxiManager;
                                    // $scope.cartaxiManager.onAddLayer = (serial:string,layer:any) => {
                                    //     $scope.AdMap.addLayer(serial,layer);
                                    // };
                                }

                                $scope.cartaxiManager.getAllCarByCompanyId(val.CompanyId, $http, global, $scope.gridListTaxiCars, $scope.companyName,
                                    val.Id, false).then((s:boolean)=> {
                                    //FilterGroup($scope.groupSelectSettings.itemSelect().Name);

                                    $scope.StateCar($scope.stateSelectSettings.valueMember, $scope.groupSelectSettings.itemSelect().Name);
                                    google.maps.event.trigger($scope.map, 'resize');
                                });

                                $scope.cartaxiManager.ConnectTaxiSocket(val.CompanyId,val.Id);
                                // $scope.cartaxiManager.onUpdateLayer = (serial:string,car:any) => {
                                //     $scope.AdMap.updateLayer(serial,car);
                                // };

                                $scope.cartaxioverspeed = $scope.cartaxiManager.getCarOverSpeed(val.Name);
                                // GetSoS(val.Name);

                                $scope.cartaxiovertime = $scope.cartaxiManager.getCarQuathoigian(val.Name);


                                //danh sách xe vi phạm
                                $scope.carvipham = $scope.cartaxiManager.getCarViPham(val.Name);
                                AdapterDanhsachvipham($scope.carvipham);

                                // danh sách xe cảnh báo
                                $scope.carcanhbao = $scope.cartaxiManager.getCarCanhBao($scope.groupSelectSettings.itemSelect().Name);
                                AdapterDanhsachcanhbao($scope.carcanhbao);
                                // console.log($scope.carvipham);

                                $scope.carchuachotca = $scope.cartaxiManager.getCarChuaChotCa(val.Name);
                                AdapterDanhsachchotca($scope.carchuachotca);

                                $scope.cartaxiManager.onStatictis =(st:any)=>{
                                    console.log(st);

                                    // console.log('chay socket');
                                    // chạy quá tốc độ
                                    GetOverSpeed(val.Name);
                                    // GetSoS(val.Name);
                                    // chạy báo quá thời gian liên tục
                                    GetDataQuaThoiGian(val.Name);
                                    $scope.StateCar($scope.stateSelectSettings.valueMember, val.Name);
                                    // if(st !== undefined && st !== null){
                                    //
                                    //
                                    //     // console.log($scope.gridListCars);
                                    //     $scope.$apply();
                                    // }else{
                                    //     console.log('ko chay socket');
                                    //     // location.reload();
                                    // }

                                };

                                $scope.cartaxiManager.onClickCarOnMap=(ev:any) => {
                                    $('#tablecarmap tr').each(function() {

                                        var customerId = $(this).find("#sohieu").text();

                                        if(customerId == ev.SoHieu){
                                            var index = $(this).index();
                                            // Get row position by index
                                            var ypos = $('#tablecarmap tr:eq('+index+')').offset().top;

                                            // Go to row
                                            $('#divtablecar').animate({
                                                scrollTop: $('#divtablecar').scrollTop()+ypos - 340
                                            }, 500);
                                            // console.log(index);
                                            $(this).focus();
                                            $(this).addClass("selected").siblings().removeClass("selected");
                                        }
                                    });

                                    // console.log($('#tablecarmap #sohieu').text());
                                    // console.log(ev);
                                    // ev.removeinfo();
                                    // $scope.clickselect(ev);
                                    // console.log($('#tablecarmap>tbody>tr>td').val());
                                    // $("#tablecarmap").delegate("tbody>tr", "click", function() {
                                    //     $(this).addClass("selected").siblings().removeClass("selected");
                                    // });
                                };

                                // lấy thông tin trạng thái
                                // if (val.Id == -1) {// hiển thị tất cả các xe trong công ty
                                //     $scope.StateCar($scope.stateSelectSettings.valueMember, val.Name);
                                // }
                                // else {// hiểm thị các xe trong đội xe đã chọn
                                //     // $scope.carSelectSettings.loadlocaldatamap($scope.carManager.getCarForGroup(val.Name),$q);
                                //     if ($scope.cartaxiManager !== null || $scope.cartaxiManager !== undefined) {
                                //         // FilterGroup(val.Name);
                                //         $scope.StateCar($scope.stateSelectSettings.valueMember, val.Name);
                                //     }
                                // }

                                // var markerCluster = new MarkerClusterer($scope.map, $scope.cartaxiManager.all,
                                //     {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

                                // /*Danh sách xe sắp hết hạn phí*/

                                api.GetDevicesByExpireDate($scope.cpnSelectSettings.itemSelect().Id, 1).then((data:any)=> {
                                    $scope.gridCarHetPhi.data = data;
                                    if(data.length > 0){
                                        $.each($scope.gridCarHetPhi.data, (ind:number, val:any)=> {
                                            var day:any = Math.floor((Date.parse(val.EndTime) - Date.now())/86400000);
                                            if(day < 0){
                                                val.Note = `Đã quá hạn ${day * -1} ngày`;
                                            }else{
                                                val.Note = `Còn ${day} ngày`;
                                            }
                                            // val.Note = `Xe sẽ hết hạn trong <span class="text-danger">${Math.floor((Date.parse(val.EndTime) - Date.now())/86400000)}</span> ngày`;
                                            val.EndTime_show = global.AdapterSignDate(val.EndTime);

                                        });

                                        $('#modal_xehetphi').modal('show');
                                        // setTimeout(()=>{
                                        //     $('#modal_xehetphi').modal('hide');
                                        // },2000)
                                        // $scope.gridCarHetPhi.refresh();
                                    }else{
                                        $('#modal_xehetphi').modal('hide');
                                        $scope.gridCarHetPhi.clear();
                                    }

                                });


                                //Danh sách xe cảnh bảo khi ra khỏi vùng

                                // var gridCarWarm_data:any = $scope.carManager.getAlertMessage();
                                // ConvertDataZoneReport(gridCarWarm_data);
                                // $scope.gridCarWarm.data = gridCarWarm_data;
                                // if(gridCarWarm_data.length >0){
                                //     $('#Panel_listcarwarm').show();
                                //     $('#close_rightpanel_listcarwarm').click(function () {
                                //         $('#Panel_listcarwarm').hide();
                                //
                                //     });
                                //     $scope.gridCarWarm.refresh();
                                // }


                                //todo:code đang chạy, tạm thời comment
                                //Ds xe quá tốc độ
                                // var cartaxioverspeed:any = $scope.cartaxiManager.getCarOverSpeed($scope.groupSelectSettings.itemSelect().Name);
                                // if(cartaxioverspeed.length > 0){
                                //     $scope.gridCarOverSpeed_taxi.data = cartaxioverspeed;
                                //     $scope.gridCarOverSpeed_taxi.refresh();
                                //     $('#danhsachviphamtocdo').show();
                                //     if($('#danhsachviphamtocdo').is(':visible')) {
                                //         $scope.gridCarOverSpeed_taxi.refresh();
                                //     }
                                // }else{
                                //     $('#danhsachviphamtocdo').hide();
                                //     $scope.gridCarOverSpeed_taxi.clear();
                                // }
                                // $('#close_danhsachviphamtocdo').click(()=>{
                                //     $('#danhsachviphamtocdo').hide();
                                // });


                                // Xử lý autocomplete search xe
                                var currencies:any = [];
                                var currencies_Bs:any = [];
                                $.each($scope.cartaxiManager.getCarForGroup(val.Name), (index:number, val:any)=> {
                                    currencies.push({value: `${val.SoHieu} - ${val.Bs}`, data: val.Serial});
                                });

                                CarAutocomplete(currencies);
                                $('#car_journeyreports').autocomplete({
                                    lookup: currencies,
                                    onSelect: function (suggestion:any) {
                                        $scope.carjourneyreports_Serial = suggestion.data;
                                    },

                                });



                                // var data_vipham = $scope.cartaxiManager.getCarViPham($scope.groupSelectSettings.itemSelect().Name);
                                // if(data_vipham.length > 0){
                                //     $scope.gridDsviphamtaxi.data = data_vipham;
                                //     AdapterDanhsachvipham($scope.gridDsviphamtaxi.data);
                                //     $scope.gridDsviphamtaxi.refresh();
                                // }else{
                                //     $scope.gridDsviphamtaxi.clear();
                                // }

                                // var data_canhbao = $scope.cartaxiManager.getCarCanhBao($scope.groupSelectSettings.itemSelect().Name);
                                // if(data_canhbao.length > 0){
                                //     $scope.gridDscanhbaotaxi.data = data_canhbao;
                                //     AdapterDanhsachcanhbao($scope.gridDscanhbaotaxi.data);
                                //     $scope.gridDscanhbaotaxi.refresh();
                                //
                                // }else{
                                //     $scope.gridDscanhbaotaxi.clear();
                                //
                                // }
                                //

                                // var data_chuachotca = $scope.cartaxiManager.getCarChuaChotCa(val.Name);
                                // if(data_chuachotca.length > 0){
                                //     // AdapterDanhsachchotca(data_chuachotca);
                                //     // console.log(data_chuachotca);
                                //     $scope.gridDsxechuachotcataxi.data = data_chuachotca;
                                //     AdapterDanhsachchotca($scope.gridDsxechuachotcataxi.data);
                                //     $scope.gridDsxechuachotcataxi.refresh();
                                // }else{
                                //     $scope.gridDsxechuachotcataxi.clear();
                                // }

                                var data_xetrongvung = $scope.cartaxiManager.getCarTrongVung(val.Name);
                                if(data_xetrongvung.length >0){
                                    $scope.xetrongvung = data_xetrongvung.length;
                                }

                                var data_xengoaivung = $scope.cartaxiManager.getCarNgoaiVung(val.Name);
                                if(data_xengoaivung.length >0){
                                    $scope.xengoaivung = data_xengoaivung.length;
                                }
                                // var data_thongke = $scope.cartaxiManager.getCarForGroup(val.Name);
                                // if(data_thongke.length > 0){
                                //     Sugar.Number.setOption('decimal',',');
                                //     Sugar.Number.setOption('thousands','.');
                                //
                                //     let tong:number = 0;
                                //     $.each(data_thongke,(ind:number, val:any)=>{
                                //         tong = tong + val.TotalMoney;
                                //     });
                                //     var binhquan: any =  Sugar.Number(Math.round(tong / $scope.sumcar)).format(0);
                                //     $scope.doanhthutong = Sugar.Number(tong).format(0).raw;
                                //     $scope.doanhthubinhquan = binhquan.raw;
                                //     $scope.hieusuat = Math.round(($scope.count_cars.state1/ $scope.sumcar)*100);
                                //
                                // }
                                // $('#danhsachvipham').on('shown.bs.collapse', function () {
                                //
                                // });
                                // $scope.$apply();
                                // VeBieuDo();
                            };


                            /**
                             * Hàm get dữ liệu xe quá thời gian
                             * @param {string} groupname tên đội xe
                             * @constructor
                             */
                            function GetDataQuaThoiGian(groupname:string){

                                $scope.cartaxiovertime = $scope.cartaxiManager.getCarQuathoigian(groupname);
                                AdapterQuathoigian($scope.cartaxiovertime);
                                if($scope.cartaxiovertime.length == 0){
                                    $('#xequathoigian').hide();
                                }
                            }
                            $('#close_xequathoigian').click(()=>{
                                $('#xequathoigian').hide();
                            });

                            //settimeout hiển thị danh sách báo thời gian liên tục


                            $('#modal_xehetphi').on('shown.bs.modal',()=>{
                                $scope.gridCarHetPhi.refresh();
                            });

                            function GetOverSpeed(groupname:string){
                                $scope.cartaxioverspeed = $scope.cartaxiManager.getCarOverSpeed(groupname);
                                if($scope.cartaxioverspeed.length >0){
                                    $('#danhsachviphamtocdo').show();
                                }else{
                                    $('#danhsachviphamtocdo').hide();
                                }

                                $('#close_danhsachviphamtocdo').click(()=>{
                                    $('#danhsachviphamtocdo').hide();
                                });
                            }


                            function GetXeViPham(groupname:string){
                                $scope.carvipham = $scope.cartaxiManager.getCarViPham($scope.groupSelectSettings.itemSelect().Name);
                                if($scope.carvipham.length > 0){
                                    // console.log($scope.carvipham);
                                    // $scope.gridDsviphamtaxi.data = data_vipham;
                                    // AdapterDanhsachvipham($scope.gridDsviphamtaxi.data);
                                    // $scope.gridDsviphamtaxi.refresh();
                                }else{
                                    // $scope.gridDsviphamtaxi.clear();
                                }
                            }


                            function CarAutocomplete(currencies:any) {
                                $('#autocomplete').autocomplete({
                                    lookup: currencies,
                                    onSelect: function (suggestion:any) {
                                        // $scope.car_autocomplete_Serial = suggestion.data;

                                        var cars_check = $scope.cartaxiManager.where((car)=> {
                                            return car.serial == suggestion.data;
                                        });
                                        if (cars_check.length !== 0) {
                                            cars_check[0].focus();
                                            $scope.car_autocomplete_Serial = cars_check[0];
                                        }
                                        // else {
                                        //     $scope.car_autocomplete_Serial = null;
                                        // }
                                    },
                                }).on('keypress', (event:any)=> {
                                    if (event.keyCode == 13) {
                                        console.log('tim xe', event);
                                        var cars_check = $scope.cartaxiManager.where((car)=> {
                                            return car.SoHieu == event.target.value;
                                        });
                                        if (cars_check.length !== 0) {
                                            cars_check[0].focus();
                                            $scope.car_autocomplete_Serial = cars_check[0];
                                        }else{
                                            alert('Không có số hiệu cần tìm');
                                            return;
                                        }
                                        // else {
                                        //     $scope.car_autocomplete_Serial = null;
                                        //     bootbox.dialog({
                                        //         title: "Thông Báo",
                                        //         message: "Không có xe cần tìm!!",
                                        //         buttons: {
                                        //             cancle: {
                                        //                 label: "Hủy",
                                        //                 className: "btn-danger",
                                        //             }
                                        //         }
                                        //     });
                                        //
                                        // }
                                        // $(this).unbind(event);
                                    }
                                });

                            }

                            // Xử lý  kick vào tìm xe
                            $scope.click_searchcar = () => {
                                if ($('#autocomplete').val() == undefined || $('#autocomplete').val() == '') {
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Vui lòng chọn xe!!",
                                        buttons: {
                                            cancle: {
                                                label: "Hủy",
                                                className: "btn-danger",
                                            }
                                        }
                                    });
                                } else {
                                    var cars_check = $scope.cartaxiManager.where((car)=> {
                                        return car.SoHieu == $scope.car_autocomplete_Serial.SoHieu;
                                    });
                                    if (cars_check.length !== 0) {
                                        cars_check[0].focus();
                                    } else {
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Không có xe cần tìm!!",
                                            buttons: {
                                                cancle: {
                                                    label: "Hủy",
                                                    className: "btn-danger",
                                                }
                                            }
                                        });
                                    }
                                }
                            };



                            // /*Hàm xử lý group*/
                            // function FilterGroup(groupname:string) {
                            //     var cargroup = $scope.cartaxiManager.getCarForGroup(groupname);
                            //     $.each(cargroup, (index:number, value:any)=> {
                            //         value.ForceHide = false;
                            //         value.setMap($scope.map);
                            //
                            //     });
                            //     $.each($scope.cartaxiManager.where((c)=> {
                            //         return (c.DeviceGroup !== groupname);
                            //     }), (index:number, value:any)=> {
                            //         value.ForceHide = true;
                            //         value.setMap(null);
                            //     });
                            // }
                            //
                            // /*Hàm xử lý trạng thái*/
                            // $scope.StateCar=(state:number, groupname:string)=> {
                            //     $scope.count_carstate = 0;
                            //     // lấy danh sách xe tất cả
                            //     var car_state0 = $scope.cartaxiManager.getAllCars();
                            //     // danh sách xe có khách
                            //     var car_state1 = $scope.cartaxiManager.getCarForCoKhach(groupname);
                            //     // danh sách xe không khách
                            //     var car_state2 = $scope.cartaxiManager.getCarForKhongKhach(groupname);
                            //     // danh sách xe đỗ
                            //     var car_state3 = $scope.cartaxiManager.getCarDo(groupname);
                            //     // danh sách xe mất GPS
                            //     var car_state4 = $scope.cartaxiManager.getCarLostGps(groupname);
                            //     // danh sách xe mất liên lạc
                            //     var car_state5 = $scope.cartaxiManager.getCarLostSign(groupname);
                            //     // danh sách xe mất đồng hồ
                            //     var car_state6 = $scope.cartaxiManager.getCarLostDongHo(groupname);
                            //     // danh sách xe bận
                            //     var car_state7 = $scope.cartaxiManager.getCarForBusy(groupname);
                            //     // danh sách xe dừng
                            //     var car_state8 = $scope.cartaxiManager.getCarDung(groupname);
                            //
                            //     // đếm số lượng các xe
                            //     CountCarState(car_state0, car_state1, car_state2, car_state3, car_state4, car_state5,car_state6, car_state7, car_state8);
                            //     // return;
                            //     switch (state) {
                            //         case 0: // tất cả các trạng thái
                            //         {
                            //
                            //             $.each(car_state0, (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = false;
                            //                 $scope.sumcar = index + 1;
                            //             });
                            //             $scope.count_carstate = $scope.sumcar;
                            //             $scope.cartaxi_array = car_state0;
                            //             // console.log(car_state0);
                            //             SortData($scope.cartaxi_array);
                            //             break;
                            //         }
                            //         case 1: // xe có khách
                            //         {
                            //             $scope.count_carstate = $scope.count_cars.state1;
                            //             //hiển thị xe đang di chuyển lên bản đồ
                            //             $.each(car_state1, (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = false;
                            //             });
                            //             //ẩn các xe không có khách
                            //             $.each($scope.cartaxiManager.where((c)=> {
                            //                 return !(c.MeterOk && c.Gps && c.MeterStatus);
                            //
                            //             }), (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = true;
                            //             });
                            //             $scope.cartaxi_array = car_state1;
                            //             // SortData($scope.cartaxi_array);
                            //             break;
                            //         }
                            //         case 2: //xe không khách
                            //         {
                            //             $scope.count_carstate = $scope.count_cars.state2;
                            //             //hiển thị xe không khách
                            //             $.each(car_state2, (index:number, val:any)=> {
                            //                 val.Layer.ForceHide  = false;
                            //             });
                            //             //ẩn đi các xe không dừng đỗ
                            //             $.each($scope.cartaxiManager.where((c)=> {
                            //                 return !(c.MeterOk && c.Gps && !c.MeterStatus);
                            //             }), (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = true;
                            //             });
                            //             $scope.cartaxi_array = car_state2;
                            //             break;
                            //         }
                            //         case 3: // ds xe đỗ
                            //         {
                            //
                            //             // /*danh sách xe mở máy ( không được phép  hiển thị trên map)*/
                            //             // var invisibleCars = $scope.cartaxiManager.where((c)=> {
                            //             //     if (c.LostGsm) {
                            //             //         return c.LostGsm;
                            //             //     } else {
                            //             //         return c.KeyStatus;
                            //             //     }
                            //             // });
                            //             $scope.count_carstate = $scope.count_cars.state3;
                            //             /*hiển thị các thiết bị tắt máy*/
                            //             $.each(car_state3, (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = false;
                            //             });
                            //             /*ẩn thiết bị không cần thiết đi*/
                            //             var invisible = $scope.cartaxiManager.where((car)=> {
                            //                 return !(car.MeterOk && !car.MeterStatus && car.Speed==0 && !car.KeyStatus);
                            //             });
                            //             $.each(invisible, (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = true;
                            //             });
                            //             $scope.cartaxi_array = car_state3;
                            //
                            //             break;
                            //         }
                            //         case 4: // xe mất GPS
                            //         {
                            //             $scope.count_carstate = $scope.count_cars.state4;
                            //             $.each(car_state4, (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = false;
                            //             });
                            //
                            //             $.each($scope.cartaxiManager.where((c)=> {
                            //                 return c.Gps;
                            //             }), (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = true;
                            //             });
                            //             $scope.cartaxi_array = car_state4;
                            //
                            //             break;
                            //         }
                            //         case 5: // xe mất liên lạc
                            //         {
                            //             // đếm số xe hiển thị trạng thái mất liên lạc -> hiển thị trên tổng số xe
                            //             $scope.count_carstate = $scope.count_cars.state5;
                            //             $.each(car_state5, (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = false;
                            //
                            //
                            //             });
                            //             $.each($scope.cartaxiManager.where((c)=> {
                            //                 return !c.LostGsm;
                            //             }), (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = true;
                            //
                            //             });
                            //             $scope.cartaxi_array = car_state5;
                            //
                            //             break;
                            //         }
                            //         case 6: // xe mất đồng hồ
                            //         {
                            //             //đếm số xe hiển thị trạng thái quá tốc độ
                            //             $scope.count_carstate = $scope.count_cars.state6;
                            //             $.each(car_state6, (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = false;
                            //
                            //
                            //             });
                            //             $.each($scope.cartaxiManager.where((c)=> {
                            //                 return !(!c.LostGsm && !c.MeterOk);
                            //             }), (index:number, val:any)=> {
                            //
                            //                 val.Layer.ForceHide = true;
                            //
                            //             });
                            //             $scope.cartaxi_array = car_state6;
                            //
                            //             break;
                            //         }
                            //         case 7: // xe bận
                            //         {
                            //             //đếm số xe hiển thị trạng thái xe bận
                            //             $scope.count_carstate = $scope.count_cars.state7;
                            //             $.each(car_state7, (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = false;
                            //
                            //             });
                            //             $.each($scope.cartaxiManager.where((car)=> {
                            //                 return !(!car.LostGsm && car.MeterOk && car.Gps && !car.MeterStatus && car.IsBusy);
                            //             }), (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = true;
                            //
                            //             });
                            //             $scope.cartaxi_array = car_state7;
                            //
                            //             break;
                            //         }
                            //         case 8: // xe dừng
                            //         {
                            //             //đếm số xe hiển thị trạng thái xe bận
                            //             $scope.count_carstate = $scope.count_cars.state8;
                            //             $.each(car_state8, (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = false;
                            //
                            //             });
                            //             $.each($scope.cartaxiManager.where((car)=> {
                            //                 return !(car.MeterOk && !car.MeterStatus && car.Speed==0 && car.KeyStatus);
                            //             }), (index:number, val:any)=> {
                            //                 val.Layer.ForceHide = true;
                            //             });
                            //             $scope.cartaxi_array = car_state6;
                            //
                            //             break;
                            //         }
                            //     }
                            //
                            //     // var markers = [];
                            //     // for (var i = 0; i < $scope.cartaxi_array.length; i++) {
                            //     //     // console.log($scope.cartaxi_array[i]);
                            //     //     var latLng = new google.maps.LatLng($scope.cartaxi_array[i].latlng.lat(),
                            //     //         $scope.cartaxi_array[i].latlng.lng());
                            //     //     var marker = new google.maps.Marker({'position': latLng});
                            //     //     markers.push(marker);
                            //     // }
                            //     // var markerCluster = new MarkerClusterer($scope.map, $scope.cartaxi_array, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
                            // };

                            /*Hàm xử lý group*/
                            function FilterGroup(groupname:string) {
                                var cargroup = $scope.cartaxiManager.getCarForGroup(groupname);
                                $.each(cargroup, (index:number, value:any)=> {
                                    value.ForceHide = false;
                                    value.setMap($scope.map);

                                });
                                $.each($scope.cartaxiManager.where((c)=> {
                                    return (c.DeviceGroup !== groupname);
                                }), (index:number, value:any)=> {
                                    value.ForceHide = true;
                                    value.setMap(null);
                                });
                            }

                            /*Hàm xử lý trạng thái*/
                            $scope.StateCar=(state:number, groupname:string)=> {
                                $scope.count_carstate = 0;
                                // lấy danh sách xe tất cả
                                var car_state0 = $scope.cartaxiManager.getAllCars();
                                // danh sách xe có khách
                                var car_state1 = $scope.cartaxiManager.getCarForCoKhach(groupname);
                                // danh sách xe không khách
                                var car_state2 = $scope.cartaxiManager.getCarForKhongKhach(groupname);
                                // danh sách xe đỗ
                                var car_state3 = $scope.cartaxiManager.getCarDo(groupname);
                                // danh sách xe mất GPS
                                var car_state4 = $scope.cartaxiManager.getCarLostGps(groupname);
                                // danh sách xe mất liên lạc
                                var car_state5 = $scope.cartaxiManager.getCarLostSign(groupname);
                                // danh sách xe mất đồng hồ
                                var car_state6 = $scope.cartaxiManager.getCarLostDongHo(groupname);
                                // danh sách xe bận
                                var car_state7 = $scope.cartaxiManager.getCarForBusy(groupname);
                                // danh sách xe dừng
                                var car_state8 = $scope.cartaxiManager.getCarDung(groupname);

                                // đếm số lượng các xe
                                CountCarState(car_state0, car_state1, car_state2, car_state3, car_state4, car_state5,car_state6, car_state7, car_state8);
                                // return;
                                switch (state) {
                                    case 0: // tất cả các trạng thái
                                    {
                                        $.each(car_state0, (index:number, val:any)=> {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);
                                            $scope.sumcar = index + 1;
                                        });
                                        $scope.count_carstate = $scope.sumcar;
                                        $scope.cartaxi_array = car_state0;

                                        break;
                                    }
                                    case 1: // xe có khách
                                    {
                                        $scope.count_carstate = $scope.count_cars.state1;
                                        //hiển thị xe đang di chuyển lên bản đồ
                                        $.each(car_state1, (index:number, val:any)=> {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);

                                        });
                                        //ẩn các xe không có khách
                                        $.each($scope.cartaxiManager.where((c)=> {
                                            return !(c.MeterOk && c.Gps && c.MeterStatus);

                                        }), (index:number, val:any)=> {
                                            val.ForceHide = true;
                                            val.setMap(null);
                                        });
                                        $scope.cartaxi_array = car_state1;


                                        break;
                                    }
                                    case 2: //xe không khách
                                    {
                                        $scope.count_carstate = $scope.count_cars.state2;
                                        //hiển thị xe không khách
                                        $.each(car_state2, (index:number, val:any)=> {
                                            if (val.getMap() == null) {
                                                val.ForceHide = false;
                                                val.setMap($scope.map);
                                            }

                                        });
                                        //ẩn đi các xe không dừng đỗ
                                        $.each($scope.cartaxiManager.where((c)=> {
                                            return !(c.MeterOk && c.Gps && !c.MeterStatus);
                                        }), (index:number, val:any)=> {
                                            val.ForceHide = true;
                                            val.setMap(null);
                                        });
                                        $scope.cartaxi_array = car_state2;

                                        break;
                                    }
                                    case 3: // ds xe đỗ
                                    {

                                        // /*danh sách xe mở máy ( không được phép  hiển thị trên map)*/
                                        // var invisibleCars = $scope.cartaxiManager.where((c)=> {
                                        //     if (c.LostGsm) {
                                        //         return c.LostGsm;
                                        //     } else {
                                        //         return c.KeyStatus;
                                        //     }
                                        // });
                                        $scope.count_carstate = $scope.count_cars.state3;
                                        /*hiển thị các thiết bị tắt máy*/
                                        $.each(car_state3, (index:number, val:any)=> {
                                            if (val.getMap() == null) {
                                                val.ForceHide = false;
                                                val.setMap($scope.map);
                                            }
                                        });
                                        /*ẩn thiết bị không cần thiết đi*/
                                        var invisible = $scope.cartaxiManager.where((car)=> {
                                            return !(car.MeterOk && !car.MeterStatus && car.Speed==0 && !car.KeyStatus);

                                        });
                                        $.each(invisible, (index:number, val:any)=> {
                                            val.ForceHide = true;
                                            val.setMap(null);
                                        });
                                        $scope.cartaxi_array = car_state3;

                                        break;
                                    }
                                    case 4: // xe mất GPS
                                    {
                                        $scope.count_carstate = $scope.count_cars.state4;
                                        $.each(car_state4, (index:number, val:any)=> {

                                            val.ForceHide = false;
                                            val.setMap($scope.map);

                                        });

                                        $.each($scope.cartaxiManager.where((c)=> {
                                            return c.Gps;
                                        }), (index:number, val:any)=> {
                                            val.ForceHide = true;
                                            val.setMap(null);
                                        });
                                        $scope.cartaxi_array = car_state4;

                                        break;
                                    }
                                    case 5: // xe mất liên lạc
                                    {
                                        // đếm số xe hiển thị trạng thái mất liên lạc -> hiển thị trên tổng số xe
                                        $scope.count_carstate = $scope.count_cars.state5;
                                        $.each(car_state5, (index:number, val:any)=> {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);

                                        });
                                        $.each($scope.cartaxiManager.where((c)=> {
                                            return !c.LostGsm;
                                        }), (index:number, val:any)=> {
                                            val.ForceHide = true;
                                            val.setMap(null);
                                        });
                                        $scope.cartaxi_array = car_state5;

                                        break;
                                    }
                                    case 6: // xe mất đồng hồ
                                    {
                                        //đếm số xe hiển thị trạng thái quá tốc độ
                                        $scope.count_carstate = $scope.count_cars.state6;
                                        $.each(car_state6, (index:number, val:any)=> {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);

                                        });
                                        $.each($scope.cartaxiManager.where((c)=> {
                                            return !(!c.LostGsm && !c.MeterOk);
                                        }), (index:number, val:any)=> {

                                            val.ForceHide = true;
                                            val.setMap(null);
                                        });
                                        $scope.cartaxi_array = car_state6;

                                        break;
                                    }
                                    case 7: // xe bận
                                    {
                                        //đếm số xe hiển thị trạng thái xe bận
                                        $scope.count_carstate = $scope.count_cars.state7;
                                        $.each(car_state7, (index:number, val:any)=> {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);

                                        });
                                        $.each($scope.cartaxiManager.where((car)=> {
                                            return !(!car.LostGsm && car.MeterOk && car.Gps && !car.MeterStatus && car.IsBusy);
                                        }), (index:number, val:any)=> {

                                            val.ForceHide = true;
                                            val.setMap(null);
                                        });
                                        $scope.cartaxi_array = car_state7;

                                        break;
                                    }
                                    case 8: // xe dừng
                                    {
                                        //đếm số xe hiển thị trạng thái xe bận
                                        $scope.count_carstate = $scope.count_cars.state8;
                                        $.each(car_state8, (index:number, val:any)=> {
                                            val.ForceHide = false;
                                            val.setMap($scope.map);

                                        });
                                        $.each($scope.cartaxiManager.where((car)=> {
                                            return !(car.MeterOk && !car.MeterStatus && car.Speed==0 && car.KeyStatus);
                                        }), (index:number, val:any)=> {

                                            val.ForceHide = true;
                                            val.setMap(null);
                                        });
                                        $scope.cartaxi_array = car_state6;

                                        break;
                                    }
                                }

                                // var markers = [];
                                // for (var i = 0; i < $scope.cartaxi_array.length; i++) {
                                //     // console.log($scope.cartaxi_array[i]);
                                //     var latLng = new google.maps.LatLng($scope.cartaxi_array[i].latlng.lat(),
                                //         $scope.cartaxi_array[i].latlng.lng());
                                //     var marker = new google.maps.Marker({'position': latLng});
                                //     markers.push(marker);
                                // }
                                // var markerCluster = new MarkerClusterer($scope.map, $scope.cartaxi_array, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
                            };


                            function SortData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                   val.SoHieu_ = parseInt(val.SoHieu.replace( /^\D+/g, ''));
                                });
                            }

                            /*Hàm tính số xe theo trạng thái*/
                            function CountCarState(state0:any, state1:any, state2:any, state3:any, state4:any, state5:any, state6:any, state7:any, state8:any) {
                                if (state0.length != 0) {
                                    $.each(state0, (index:number, val:any)=> {
                                        // tất cả xe
                                        $scope.count_cars.state0 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state0 = 0;
                                }
                                if (state1.length != 0) {
                                    $.each(state1, (index:number, val:any)=> {
                                        // xe di chuyển
                                        $scope.count_cars.state1 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state1 = 0;
                                }
                                if (state2.length != 0) {
                                    $.each(state2, (index:number, val:any)=> {
                                        // xe dừng đổ
                                        $scope.count_cars.state2 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state2 = 0;
                                }
                                if (state3.length != 0) {
                                    $.each(state3, (index:number, val:any)=> {
                                        // xe dừng đổ
                                        $scope.count_cars.state3 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state3 = 0;
                                }
                                if (state4.length != 0) {
                                    $.each(state4, (index:number, val:any)=> {
                                        // xe mất GPS
                                        $scope.count_cars.state4 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state4 = 0;
                                }
                                if (state5.length != 0) {
                                    $.each(state5, (index:number, val:any)=> {
                                        // xe mất GPS
                                        $scope.count_cars.state5 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state5 = 0;
                                }
                                if (state6.length != 0) {
                                    $.each(state6, (index:number, val:any)=> {
                                        // xe mất GPS
                                        $scope.count_cars.state6 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state6 = 0;
                                }
                                if (state7.length != 0) {
                                    $.each(state7, (index:number, val:any)=> {
                                        // xe mất GPS
                                        $scope.count_cars.state7 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state7 = 0;
                                }
                                if (state8.length != 0) {
                                    $.each(state8, (index:number, val:any)=> {
                                        // xe mất GPS
                                        $scope.count_cars.state8 = index + 1;
                                    });
                                } else {
                                    $scope.count_cars.state8 = 0;
                                }

                            }

                            /*Hàm chuyển thời gian*/
                            function Adaptertimeupdate(x:any) {
                                $.each(x, (index:number, val:any)=> {
                                    val.tt = val.ClientSend.slice(11);
                                });
                            }

                            /*Xử lý sự kiện trên thanh màu*/
                            function CheckStateCar(x:number) {
                                if ($scope.cpnSelectSettings.itemSelect().Id != undefined) {
                                    $scope.stateSelectSettings.jqxDropDownList('selectIndex', x);
                                } else {
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Xin chọn công ty !!!",
                                        buttons: {
                                            cancle: {
                                                label: "Hủy",
                                                className: "btn-danger",
                                            }
                                        }
                                    });
                                }
                            }

                            /*Hàm xử lý data danh sách vi phạm*/
                            function AdapterDanhsachvipham(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    if(val.OverSpeed){
                                        val.OverSpeed_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.OverSpeed_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(val.StopOverTime){
                                        val.StopOverTime_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.StopOverTime_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(val.ChichXung > 0){
                                        val.ChichXung_show = '<span class="icon-checkbox-unchecked"></span>';

                                    }else{
                                        val.ChichXung_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                });
                            }
                            function AdapterDanhsachcanhbao(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    if(val.LostGsm){
                                        val.LostGsm_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.LostGsm_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(!val.Gps){
                                        val.Gps_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.Gps_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(!val.MeterOk){
                                        val.MeterOk_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.MeterOk_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                })
                            }
                            function AdapterQuathoigian(value:any){
                                $.each(value,(ind:number, val:any)=>{
                                    val.DisplayId = `${val.Id} - ${val.Bs}`;
                                    if(val.TgLaiXe >= 225){
                                        val.TgLaiXe_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.TgLaiXe_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(val.TongThoiGian >= 585){
                                        val.TongThoiGian_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        val.TongThoiGian_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }

                                });
                            }




                            $('#totalcar_taxi').click(()=> {
                                CheckStateCar(0);

                            });
                            $('#carrun_taxi').click(()=> {
                                CheckStateCar(1);

                            });
                            $('#carstop_taxi').click(()=> {
                                CheckStateCar(2);
                            });
                            $('#cartaxido').click(()=> {
                                CheckStateCar(3);
                            });
                            $('#cartaxilostgps').click(()=> {
                                CheckStateCar(4);
                            });
                            $('#cartaxilostsign').click(()=> {
                                CheckStateCar(5);
                            });
                            $('#cartaxilostdongho').click(()=> {
                                CheckStateCar(6);
                            });
                            $('#cartaxibusy').click(()=> {
                                CheckStateCar(7);
                            });
                            $('#cartaxistop').click(()=> {
                                CheckStateCar(8);
                            });

                            // xử lý hiệu ứng màu của row table
                            $("#tablecarmap").delegate("tbody>tr", "click", function() {
                                $(this).addClass("selected").siblings().removeClass("selected");
                            });


                            var input:any = /** @type {!HTMLInputElement} */(
                                document.getElementById('address-input'));


                            // var types = document.getElementById('type-selector');
                            // this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                            // this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

                            var autocomplete:any = new google.maps.places.Autocomplete(input);
                            autocomplete.bindTo('bounds', $scope.map);

                            var infowindow = new google.maps.InfoWindow();
                            var markerOptions:any = {
                                map: $scope.map,
                                anchorPoint: new google.maps.Point(0, -29)
                            };
                            var marker:any = new google.maps.Marker(markerOptions);

                            autocomplete.addListener('place_changed',()=> {
                                infowindow.close();
                                document.getElementById('searchdirections').className='close-button';
                                $('#searchdirections').parent().click(()=>{
                                    //alert('hello-nút xóa');
                                    var input:any = $("#address-input").val("");
                                    marker.setVisible(false);
                                    infowindow.close();
                                    document.getElementById('searchdirections').className='searchbox-directions';
                                });
                                marker.setVisible(false);
                                var place = autocomplete.getPlace();
                                if (!place.geometry) {
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Vui lòng chọn lại!!",
                                        buttons: {
                                            cancle: {
                                                label: "Đồng ý",
                                                className: "btn-success",
                                            },

                                        }
                                    });
                                    return;
                                }

                                // If the place has a geometry, then present it on a map.
                                if (place.geometry.viewport) {
                                    $scope.map.fitBounds(place.geometry.viewport);
                                } else {
                                    $scope.map.setCenter(place.geometry.location);
                                    $scope.map.setZoom(11);  // Why 17? Because it looks good.
                                }
                                marker.setIcon(/** @type {google.maps.Icon} */({
                                    url: place.icon,
                                    size: new google.maps.Size(71, 71),
                                    origin: new google.maps.Point(0, 0),
                                    anchor: new google.maps.Point(17, 34),
                                    scaledSize: new google.maps.Size(35, 35)
                                }));
                                marker.setPosition(place.geometry.location);
                                marker.setVisible(true);

                                var address = '';
                                if (place.address_components) {
                                    address = [
                                        (place.address_components[0] && place.address_components[0].short_name || ''),
                                        (place.address_components[1] && place.address_components[1].short_name || ''),
                                        (place.address_components[2] && place.address_components[2].short_name || '')
                                    ].join(' ');
                                }

                                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                                infowindow.open($scope.map, marker);

                            });



                        }
                        function AdapterDanhsachchotca(value:any){
                            $.each(value,(ind:number, val:any)=>{
                                val.ThoiGianChotCa_show = global.AdapterDate(val.ThoiGianChotCa);
                            });
                        }
                        function VeBieuDo(){
                            if($scope.myChart_maptaxi !== undefined){
                                $scope.myChart_maptaxi.destroy();
                            }
                            var ctx = $('#tkxetaximap');
                            $scope.myChart_maptaxi = new Chart(ctx, {
                                type: 'pie',
                                data :{
                                    labels: [
                                        "Xe có khách",
                                        "Xe chạy đón khách",
                                        "Xe chờ tại điểm",
                                        "Xe đỗ ngoài"
                                    ],
                                    datasets: [
                                        {
                                            data: [$scope.count_cars.state1, $scope.count_cars.state2 - $scope.count_cars.state3 - $scope.count_cars.state8, $scope.xetrongvung,$scope.xengoaivung],
                                            backgroundColor: [
                                                "#36A2EB",
                                                "#FF6384",
                                                "#FFCE56",
                                                "#2ecc71"
                                            ],
                                            hoverBackgroundColor: [
                                                "#36A2EB",
                                                "#FF6384",
                                                "#FFCE56",
                                                "#2ecc71"
                                            ]
                                        }]
                                }
                            });
                        }
                    }
                }
                AdModule.controller('MaptaxiController', MaptaxiController);
            }
        }
    }
}
