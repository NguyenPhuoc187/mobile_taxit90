
/**
 * Created by user on 04/11/2016.
 */

/// <reference path="../../tstyping/angularjs/angular-cookies.d.ts" />
/// <reference path="../shared/Entity/account.e.ts" />
module App {
    export module Components {
        // thông tin menu
        import IState = angular.ui.IState;
        import IAccount = App.Shared.Entity.IAccount;
        import IRole = App.Shared.Entity.IRole;
        export interface IMenu {
            // thông tin menu con
            Child: Array<IMenu>;
            // Tên hiển thị
            Text : string;
            // đường dẫn (route name)
            Link : string;
            // icon kèm theo
            Style : string;
            // menu cha chứa nó
            Parent? : IMenu;
            // tên menu
            Name:string;
        }
        // nhóm menu
        export interface IMenuGroup {
            // tên hiển thị
            Text : string;
            // icon kèm theo
            Style: string;
            // danh sách các menu chứa trong nó
            Menu : Array<IMenu>;
            // id của nhóm menu
            Id:string;
        }

        // thông tin message
        export interface IUserMessage {
            // dường dẫn imge hoặc icon kèm theo message
            Image:string;
            // tiêu đề message
            MessageTitle:string;
            // thời gian
            LastTime:string;
            //id của message
            Id:number;
        }


        export interface IIndexScope extends ng.IScope {
            Title:any; /*Thông tin tiêu đề trang*/
            Logo:{
                LogoMobileName:string,
                LogoMainName:string
            };
            FastNotify:Array<string>;
            UserProfile:{
                Avatar:string,
                UserName:string,
                Level:number,
                GroupUser:string,
                Phone?: string,
                DisplayName: string,
                Theme?: number,
                ShowLevel:string,
            };
            Warning : Array<IUserMessage>;
            Conversation : Array<IUserMessage>;
            MenuGroup : Array<IMenuGroup>;
            CurrentMenu : IMenu;
            GetMenuPath:()=>Array<IMenu>;
            AllMenu:Array<IMenu>;
            logout:() => any;
            get_menu: any;
            initMenu:(name:string)=>void;/*load tooltip menu*/
            initTopNav:(name:string)=>void;/*load group menu*/
            isMap: boolean;
            changepass:() => any;
            passold:string;
            passnew:string;
            phone:string;
            pass_confirm: string;
            newFormAccept:() => any;
            infoacc:() => any;
            show_level:string;
            click_open_menu:() => any;
            Title_Alert: string; // Tiêu đề thông báo
            Content_Alert: string ;// Nội dung thông báo
            Enable_Alert: boolean;
            timerloadalert:any;
            updateinfoacc: () => any;
            InfoAccount: string;
            PhoneAccount: string;
            updateinfoFormAccept: () => any;
            select:(item:any)=>any;
            isActive:(item:any)=>any;
            isActiveChild:(item:any)=>any;
            selectChild:(item:any)=>any;
            selected:any;
            selectedchild:any;
            hostdown:string;
            showedthongbao:boolean;
            clickcheckbox:(value:any)=>any;
            linkdownloadthongbao:string;
            searchMenu:()=>any;
            clicktargetmenu:()=>any;
        }

        export class IndexController {
            public static $inject = ['$scope', '$state', '$sce', 'global', '$cookieStore', '$http'];

            constructor(public $scope:IIndexScope, public $state:ng.ui.IStateService, public $sce:ng.ISCEService, public global:App.Shared.IGlobalService, public $cookieStore:ng.cookies.ICookieStoreService,
                        public $http:ng.IHttpService) {
                $.jqx.theme = "blue-theme";
                this.$scope.Logo = {
                    LogoMobileName: 'ADSUN',
                    // LogoMainName: 'ADSUN-MAIN'
                    LogoMainName: "<img src='../assets/images/logo-Adsun.png' class='img_logo'/>"
                };

                var kiemtra_login:string = this.$cookieStore.get('Token');
                // console.log(global.Token.GetValue());
                if (kiemtra_login === undefined || kiemtra_login === null) {
                    window.location.href = "/login.html";
                    return;
                }

                this.global.Token.SetValue(this.$cookieStore.get('Token'));
                if(this.global.Account.GetValue()!=undefined)

                    this.buildController();

                this.global.Account.Watch = (a:any)=> {
                    this.buildController();
                };
                
                this.$scope.select = (item)=>{
                    this.$scope.selected = item;
                    if(item.Child.length == 0){
                        this.$scope.selectChild(null);

                    }
                };

                this.$scope.isActive = (item) => {
                    // console.log(item);
                    return this.$scope.selected === item;

                };

                this.$scope.selectChild = (item)=>{
                    this.$scope.selectedchild = item;
                };
                this.$scope.isActiveChild = (item) => {
                    return this.$scope.selectedchild === item;
                };
                this.$scope.clickcheckbox = (value:any)=>{
                    this.$scope.showedthongbao = value;
                    localStorage.setItem('thongbaodaxem',`${this.$scope.showedthongbao}`);
                };


                this.$scope.clicktargetmenu = () => {
                    $('body').removeClass('sidebar-open');
                };
                console.debug('Load Index Controller');
                return;
                // lay token
                //todo: check token

            }

            //search menu
            MenuAutocomplete(currencies:any) {
                $('#ac-custom').autocomplete({
                    // source: currencies


                    lookup: currencies,
                    onSelect: (suggestion:any)=>{
                        // $scope.car_autocomplete_Serial = suggestion.data;
                        console.log(suggestion);
                        this.$state.go(suggestion.data);
                        // var cars_check = $scope.carManager.where((car)=> {
                        //     return car.serial == suggestion.data;
                        // });
                        // if (cars_check.length !== 0) {
                        //     cars_check[0].focus();
                        // } else {
                        //     // $scope.car_autocomplete_Serial = null;
                        // }
                    },
                });
                //     .on('keypress', (event:any)=> {
                //     if (event.keyCode == 13) {
                //         console.log('show popup');
                //         // var cars_check = $scope.carManager.where((car)=> {
                //         //     return car.Bs == event.target.value;
                //         // });
                //         // if (cars_check.length !== 0) {
                //         //     cars_check[0].focus();
                //         // } else {
                //         //     // $scope.car_autocomplete_Serial = null;
                //         //     bootbox.dialog({
                //         //         title: "Thông Báo",
                //         //         message: "Không có xe cần tìm!!",
                //         //         buttons: {
                //         //             cancle: {
                //         //                 label: "Hủy",
                //         //                 className: "btn-danger",
                //         //             }
                //         //         }
                //         //     });
                //         //
                //         // }
                //         $(this).unbind(event);
                //     }
                // });
            }

            buildController() {

                var timeCheckSearchMenu:NodeJS.Timer = null;
                var lastCheckMenu = false;
                $.jqx.theme = "blue-theme";

                $('.newsticker').newsTicker({
                    row_height: 20,
                    max_rows: 1,
                    speed: 600,
                    direction: 'up',
                    duration: 4000,
                    autostart: 1,
                    pauseOnHover: 0
                });
                $('.newsticker').css('display','block');

                this.$scope.initMenu = (name: string) => {
                    /*Load tooltip menu khi ở chế độ menu icon*/

                    lastCheckMenu = true;
                    if (timeCheckSearchMenu == null) {
                        timeCheckSearchMenu = setInterval(() => {
                            if (lastCheckMenu) lastCheckMenu = false;
                            else {
                                clearInterval(timeCheckSearchMenu);
                                var tmp:any = [];
                                for(let i= 0; i< this.$scope.get_menu.length;i++){
                                    if(this.$scope.get_menu[i].Child.length > 0){

                                        for(var j=0; j < this.$scope.get_menu[i].Child.length; j++){
                                            tmp.push({value:this.$scope.get_menu[i].Child[j].Text, data: this.$scope.get_menu[i].Child[j].Name});
                                        }

                                    }else{
                                        tmp.push({value:this.$scope.get_menu[i].Text, data: this.$scope.get_menu[i].Name});
                                    }


                                }

                                this.$scope.searchMenu = () => {
                                    this.MenuAutocomplete(tmp);
                                };
                                this.global.IndexReadly.SetValue(true);

                            }
                        }, 500);
                    }
                };



                // this.global.IndexReadly.SetValue(true);
                //chỉnh theme cho jqw-datable
                // $.jqx.theme = "office";
                // console.log(`hien thi state ${this.$state.$current}`);
                // console.log(this.$state.$current);
                //thay đổi nút hiện ẩn menu chính
                // this.$scope.click_open_menu = () => {
                //     if (document.getElementById("toggle").className == "active") {
                //         document.getElementById("icon_open_menu").className = " entypo-left-open-big";
                //         document.getElementById("icon_open_menu").title = "Thu nhỏ menu";
                //     } else {
                //         document.getElementById("icon_open_menu").className = " entypo-right-open-big";
                //         document.getElementById("icon_open_menu").title = "Hiển thị menu";
                //
                //     }
                // };
                // this.$scope.initMenu = (name:string)=> {
                //     /*Load tooltip menu khi ở chế độ menu icon*/
                //
                //     lastCheckMenu = true;
                //     if (timeCheckSearchMenu == null) {
                //         timeCheckSearchMenu = setInterval(()=> {
                //             if (lastCheckMenu) lastCheckMenu = false;
                //             else {
                //                 clearInterval(timeCheckSearchMenu);
                //
                //                 /*show hoặc hide menu con*/
                //                 $('.topnav').accordionze({
                //                     accordionze: true,
                //                     speed: 500,
                //                     closedSign: '<img src="assets/themes/apricotbootstrap3/assets/img/plus.png">',
                //                     openedSign: '<img src="assets/themes/apricotbootstrap3/assets/img/minus.png">'
                //                 });
                //
                //                 /*show hoặc hide menu group*/
                //                 $('.title_menu').click(function () {
                //                     $(this).nextAll(".group_menu").slideToggle();
                //                 });
                //
                //                 $('input.id_search').quicksearch('#menu-showhide li, .menu-left-nest li');
                //                 console.log("Load menu complete");
                //                 $('.tooltip-tip-x').tooltipster({
                //                     position: 'right'
                //                 });
                //
                //                 $('.tooltip-tip').tooltipster({
                //                     position: 'right',
                //                     animation: 'slide',
                //                     theme: '.tooltipster-shadow',
                //                     delay: 1,
                //                     offsetX: '-12px',
                //                     onlyOne: true
                //
                //                 });
                //                 $('.tooltip-tip2').tooltipster({
                //                     position: 'right',
                //                     animation: 'slide',
                //                     offsetX: '-12px',
                //                     theme: '.tooltipster-shadow',
                //                     onlyOne: true
                //
                //                 });
                //                 $('.tooltip-top').tooltipster({
                //                     position: 'top'
                //                 });
                //                 $('.tooltip-right').tooltipster({
                //                     position: 'right'
                //                 });
                //                 $('.tooltip-left').tooltipster({
                //                     position: 'left'
                //                 });
                //                 $('.tooltip-bottom').tooltipster({
                //                     position: 'bottom'
                //                 });
                //                 $('.tooltip-reload').tooltipster({
                //                     position: 'right',
                //                     theme: '.tooltipster-white',
                //                     animation: 'fade'
                //                 });
                //                 $('.tooltip-fullscreen').tooltipster({
                //                     position: 'left',
                //                     theme: '.tooltipster-white',
                //                     animation: 'fade'
                //                 });
                //                 // thay đổi màu icon khi khách hàng chọn giao diện ban đầu
                //                 $(".icon_Sidebar").css({
                //                     "background": `url('../assets/themes/apricotbootstrap3/assets/img/small-bg${this.$scope.UserProfile.Theme}.jpg') repeat`
                //                 });
                //                 //hiển thị màu khi khách hàng đã chọn
                //                 $("body").css(
                //                     {
                //                         "background": `url('assets/themes/apricotbootstrap3/assets/img/bg${this.$scope.UserProfile.Theme}.jpg')repeat `,
                //                         "background-size": "100%",
                //                         "display": 'block'
                //                     });
                //
                //                 this.global.IndexReadly.SetValue(true);
                //             }
                //         }, 500);
                //     }
                // };
                /*Tìm đường dẫn bản đồ*/
                if (window.location.pathname !== '/' && window.location.pathname !== '/index.html') {
                    this.$scope.isMap = true;

                } else {
                    this.$scope.isMap = false;
                }


                this.$scope.initTopNav = (name:string)=> {

                };


                this.$scope.MenuGroup = [];
                this.$scope.AllMenu = Array<IMenu>();
                this.$scope.hostdown = 'http://'+window.location.host +'//ExelTemplate/MAU KHAI BAO TT KICH HOAT GSHT ADSUN.doc';
                this.$scope.linkdownloadthongbao = 'http://'+window.location.host +'//ExelTemplate/TBVEQUYTRINHKHAITHACGSHT8_2017.pdf';

                //todo: lay thong tin tai khoan

                //this.$http.get(this.global.AuthHost + 'User/GetRole').success((data:any)=> {
                ////console.log(data);
                //    this.global.Account.SetValue({
                //    Level: data.Level,
                //    Roles: Array<IRole>(),
                //    Username: data.Username,
                //    GroupUser: data.GroupUser,
                //    DisplayName: data.DisplayName,
                //    Phone: data.Phone,
                //    Theme: data.Theme
                //});
                var acc = this.global.Account.GetValue<IAccount>();
                localStorage.setItem('accinfo',acc.DisplayName);
                localStorage.setItem('accLevel',`${acc.Level}`);
                // this.$cookieStore.put('Acc',acc);
                //if (data.Funcs != null && data.Funcs != undefined)
                //    $.each(data.Funcs, (indx:number, val:any)=> {
                //        acc.Roles.push({
                //            Name: val.Name,
                //            Description: val.Description
                //        })
                //    });

                // Create Group Menu Default
                this.CreateGroupMenu();
                // build menu
                this.BuildMenu();
                //this.GetMenu(); post menu lên server

                /*Create Default Scope Value*/
                this.global.PageTite.Watch = (ne:any)=> {
                    this.$scope.Title = ne;
                    //$scope.$apply();
                };
                this.$scope.Title = this.global.PageTite.GetValue();
                //global.PageTite.SetValue('ADSUN');

                // this.$scope.FastNotify = [
                //     'Tổng đài CSKH: 1900 54 54 56',
                //     'Tổng đài CSKH: (028) 73 05 77 99'
                // ];
                // $('#nt-title').newsTicker({
                //     row_height: 18,
                //     max_rows: 1,
                //     duration: 60000,
                //     pauseOnHover: 0
                // });

                /*Lấy thông tin username đăng nhập*/
                this.$scope.UserProfile = {
                    Avatar: 'assets/images/Businessman-96.png',
                    UserName: acc.Username,
                    GroupUser: acc.GroupUser,
                    Level: acc.Level,
                    DisplayName: acc.DisplayName,
                    Phone: acc.Phone,
                    Theme: acc.Theme,
                    ShowLevel: this.ConvertLevel(acc.Level),
                };

                // if(this.$scope.UserProfile.Phone === ""){
                //     bootbox.dialog({
                //         title: "Thông Báo",
                //         message: "<h4 class='text-danger'>Vui lòng cập nhật đầy đủ thông tin bảo mật tài khoản. Xin cảm ơn!</h4>",
                //         buttons: {
                //             confirm: {
                //                 label: "Đồng ý",
                //                 className: "btn-success",
                //                 callback: () => {
                //                     this.$state.go('infomationuser');
                //                     // window.location.href = '/cap-nhat-thong-tin-tai-khoan';
                //                 }
                //             },
                //             cancle:{
                //                 label: "Bỏ qua",
                //                 className: "btn-default",
                //             }
                //         }
                //     });
                // }

                // });

                /*Lấy đường dẫn của menu hiện tại*/
                this.$scope.GetMenuPath = ()=> {
                    var tmp:Array<IMenu> = Array();
                    if (this.$scope.CurrentMenu != undefined) {
                        var menu = this.$scope.CurrentMenu;
                        tmp.push(menu);
                        while (menu.Parent != null) {
                            // push null đễ vẽ ra dấu '>'
                            tmp.push(null);
                            menu = this.$scope.CurrentMenu.Parent;
                            tmp.push(menu);
                        }
                    }
                    // đổi ngược thứ tự của các phần tử
                    return tmp.reverse();
                };

                // cài đặt event thay đổi đường dẫn menu
                this.global.MenuSelect.Watch = (ne:any)=> {
                    var m = this.$scope.AllMenu.filter((v)=> {
                        return v.Name === ne;
                    });
                    if (m != null && m.length > 0)
                        this.$scope.CurrentMenu = m[0];
                };

                var title_old:string = '';
                //Xuất thông báo từ server gửi về
                this.$scope.timerloadalert = setInterval(()=> {
                    title_old = this.$scope.Title_Alert;
                    this.$http.get(this.global.AuthHost + 'api/SysMessage/Get').then((rep:any)=> {
                        // console.log(rep);
                        this.$scope.Title_Alert = rep.data.Title;
                        this.$scope.Content_Alert = rep.data.Content;
                        if (rep.data.Enable == true) {
                            if (title_old !== this.$scope.Title_Alert) {
                                //$('#myModal_alert').modal('show');
                                bootbox.dialog({
                                    title: `${this.$scope.Title_Alert}`,
                                    message: `${this.$scope.Content_Alert}!!!`,
                                    buttons: {
                                        cancle: {
                                            label: "Đồng ý",
                                            className: "btn-danger",
                                        }
                                    }
                                });
                            }
                        }
                    });
                }, 10000);


                //Khi ấn nút logout
                this.$scope.logout = () => {
                    var token = this.$cookieStore.get('Token');
                    var req:any = {
                        method: 'POST',
                        url: 'http://auth.adsun.vn/User/LogOut'
                    };
                    var status:any = {};
                    this.$http(req).success((data) => {
                        var status = data;
                    });
                    this.$cookieStore.remove('Token');
                    localStorage.removeItem('companyIdmapxekhach');
                    localStorage.removeItem('companyIdquantri');
                    localStorage.removeItem('companyIdtaxi');

                    //console.log($cookieStore.get('Token'));
                    window.location.href = "/login.html";


                };

                /*Khi nhấn nút thông tin*/
                this.$scope.infoacc = () => {
                    switch (this.$scope.UserProfile.Level) {
                        case 0:
                            this.$scope.show_level = '<label class="label label-danger" style="font-size: 17px">Root</label>';
                            break;
                        case 1:
                            this.$scope.show_level = '<label class="label label-warning" style="font-size: 17px">Administrator</label>';
                            break;
                        case 2:
                            this.$scope.show_level = '<label class="label label-success" style="font-size: 17px">Quản trị công ty</label>';
                            break;
                        case 3:
                            this.$scope.show_level = '<label class="label label-info" style="font-size: 17px">Tài khoản thường</label>';
                            break;
                    }
                    $('#myModal_acc').modal('show');
                };

                /*Khi nhấn nút đổi mật khẩu*/
                //todo: kiểm tra bên back end api đúng chưa
                this.$scope.changepass = () => {
                    this.$scope.phone = this.$scope.UserProfile.Phone;
                    $('#myModal_acc').modal('hide');
                    $('#myModal_changepass').modal('show');
                    this.$scope.newFormAccept = () => {

                        if(this.$scope.passold === this.$cookieStore.get('password')){
                            if (this.$scope.passnew == this.$scope.pass_confirm) {
                                var req:any = {
                                    method: 'POST',
                                    url: `http://auth.adsun.vn/User/ChangePass?password=${this.$scope.pass_confirm}`
                                };
                                this.$http(req).success((data:any) => {
                                    //console.log(data);
                                    if (data.Status === 1) {
                                        this.$cookieStore.remove('password');
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Thay đổi mật khẩu thành công. Xin đăng nhập lại!",
                                            buttons: {
                                                confirm: {
                                                    label: "OK",
                                                    className: "btn-danger",
                                                    callback: () => {
                                                        var req:any = {
                                                            method: 'POST',
                                                            url: 'http://auth.adsun.vn/User/LogOut'
                                                        };
                                                        var status:any = {};
                                                        this.$http(req).success((data) => {
                                                            var status = data;
                                                        });
                                                        window.location.href = "/login.html";
                                                    }
                                                }
                                            }
                                        });

                                    }

                                });

                            }else{
                                bootbox.dialog({
                                    title: "Thông Báo",
                                    message: "Mật khẩu mới không trùng nhau. Vui lòng nhập lại!!!",
                                    buttons: {
                                        cancle: {
                                            label: "Quay lại",
                                            className: "btn-default",
                                            callback:()=>{
                                                this.$scope.changepass();
                                            }
                                        }
                                    }
                                });
                            }
                        }else{
                            bootbox.dialog({
                                title: "Thông Báo",
                                message: "Mật khẩu cũ không đúng. Vui lòng nhập lại",
                                buttons: {
                                    cancle: {
                                        label: "Quay lại",
                                        className: "btn-default",
                                        callback:()=>{
                                            this.$scope.changepass();
                                        }
                                    }
                                }
                            });
                        }
                        $('#myModal_changepass').modal('hide');
                    };
                };
                //this.global.Account.Watch = (ne:IAccount) => {
                   // this.$scope.UserProfile.DisplayName = ne.DisplayName;
                //};
                /*Nút cập nhật thông tin */
                this.$scope.updateinfoacc = () => {
                    // window.location.href = '/cap-nhat-thong-tin-tai-khoan';
                    // $('#myModal_updateinfo').modal('show');
                    // $('#myModal_acc').modal('hide');
                    // this.$scope.InfoAccount = this.$scope.UserProfile.DisplayName;
                    // this.$scope.PhoneAccount = this.$scope.UserProfile.Phone;
                    //
                    // this.$scope.updateinfoFormAccept = () => {
                    //     var req:any = {
                    //         Phone: this.$scope.PhoneAccount,
                    //         DisplayName: this.$scope.InfoAccount,
                    //     };
                    //     this.$http.put(this.global.AuthHost + 'User/ChangeInfo', req).then((rep:any)=> {
                    //         //console.log(rep);
                    //         if (rep.data.Status == 1) {
                    //             bootbox.dialog({
                    //                 title: "Thông Báo",
                    //                 message: "Cập nhật thông tin tài khoản thành công. Nhấn đồng ý để đăng nhập lại !!!",
                    //                 buttons: {
                    //                     confirm: {
                    //                         label: "Đồng ý",
                    //                         className: "btn-success",
                    //                         callback: () => {
                    //
                    //                             this.$scope.UserProfile.DisplayName = rep.config.data.DisplayName;
                    //                             this.$scope.UserProfile.Phone = rep.config.data.Phone;
                    //                             this.$scope.$apply();
                    //                             this.$scope.logout();
                    //
                    //                         },
                    //                     },
                    //                     cancle: {
                    //                         label: "Hủy",
                    //                         className: "btn-danger",
                    //                     },
                    //                 }
                    //             });
                    //         } else {
                    //             bootbox.dialog({
                    //                 title: "Thông Báo",
                    //                 message: "Cập nhật tài khoản không thành công!!!",
                    //                 buttons: {
                    //                     cancle: {
                    //                         label: "Hủy",
                    //                         className: "btn-danger",
                    //                     }
                    //                 }
                    //             });
                    //         }
                    //         $('#myModal_updateinfo').modal('hide');
                    //     });
                    // };
                };





            //    javascript xử lý ảnh

            }
            private ConvertLevel(level:number){
                var result:string = '';
                switch (level){
                    case 0:
                        result = '<label class="label label-danger" style="font-size: 17px">Root</label>';
                        break;
                    case 1:
                        result = '<label class="label label-warning" style="font-size: 17px">Administrator</label>';
                        break;
                    case 2:
                        result = '<label class="label label-success" style="font-size: 17px">Quản trị công ty</label>';
                        break;
                    case 3:
                        result = '<label class="label label-info" style="font-size: 17px">Tài khoản thường</label>';
                        break;

                }
                return result;
            }



            /*Lọc các menu theo quyền hạn của tài khoản*/
            private FilterMenuWithAccount(menus:Array<IState>):Array<IState> {
                var account = this.global.Account.GetValue<IAccount>();
                if (account.Level == 0) return menus;
                var result = Array<IState>();

                $.each(menus, (indx:number, val:IState)=> {
                    if (account.Roles != null){
                        var func=account.Roles.filter((m:IRole)=> {
                            return m.Name == val.name;
                        });
                        if(func.length>0){
                            // val.data=val.data|{};
                            val.tag.access=func[0].Access;
                            result.push(val);
                        }
                    }
                });
                return result;
            }

            /*Xây dựng hệ thống menu theo thông tin từ các file .route.ts*/
            private BuildMenu() {
                // lấy toàn bộ thông tin menu đã khai báo trong các file route
                var tmp = this.$state.get().filter((v:any,idx:number)=>{
                    if (v.show == undefined){
                        return true
                    } else{
                        return v.show;
                    }
                    
                }).sort((v1:IState, v2:IState):number=> {
                    if (v1.tag == undefined) {
                        return 1;
                    }
                    if (v2.tag == undefined) {
                        return 1;
                    }
                    if (v1.tag.index == undefined) {
                        return 1;
                    }
                    if (v2.tag.index == undefined) {
                        return 1;
                    }

                    return v1.tag.index - v2.tag.index;

                    // if(v1.tag == undefined || v2.tag == undefined) {
                    //     return 0;
                    // }else{
                    //     if(v1.tag.index == undefined || v2.tag.index == undefined){
                    //         return 0;
                    //     }else{
                    //
                    //     }
                    // }

                });
                // console.log(tmp);
                this.global.GetTmp.SetValue(tmp);
                var menus = this.FilterMenuWithAccount(tmp);

                // console.log(menus);

                // console.log(menus);

                var menu = Array<IMenu>();

                // load root menu
                menus.forEach((m)=> {
                    if (m.name !== "") {
                        if (( m.parent == 'undefined' || m.parent == null )) {
                            var tmp:IMenu;
                            tmp = {
                                Text: m.tag.title,
                                Child: Array<IMenu>(),
                                Link: m.url.toString(),
                                Style: m.tag.icon,
                                Name: m.name
                                // content: $sce.trustAsHtml(m.settings.before + m.title),
                                // nav: m.settings.nav
                            };
                            menu.push(tmp);
                            /*Tìm nhóm menu tương ứng luôn*/
                            var gmenu = this.FindGroupMenu(m.tag.groupId);
                            if (gmenu != null)
                                gmenu.Menu.push(tmp);
                            this.$scope.AllMenu.push(tmp);
                        }
                    }
                });
                // load child menu
                menus.forEach((m)=> {
                    if (m.name !== "") {
                        if (m.parent !== null || m.parent != 'undefined') {
                            var parentIndex = this.MenuIndexOfName(m.parent);

                            var tmp:IMenu = {
                                Text: m.tag.title,
                                Child: Array<IMenu>(),
                                Link: m.url.toString(),
                                Style: m.tag.icon,
                                Name: m.name,
                                Parent: this.$scope.AllMenu[parentIndex]
                                //content: $sce.trustAsHtml(m.settings.before+m.title+ m.settings.after ),
                                //nav: m.settings.nav
                            };
                            if (parentIndex > -1) {
                                this.$scope.AllMenu[parentIndex].Child.push(tmp)
                            }
                            else {
                                ////if(m.customParent == undefined)
                                //{
                                //    var tmpP = {
                                //        name: m.parent,
                                //        submenu: [tmp]
                                //    };
                                //    allMenu.push(tmpP);
                                //}
                            }
                            this.$scope.AllMenu.push(tmp);
                        }
                    }
                });
                //menu.sort(function(a,b){return a.nav- b.nav;});
                menu.forEach((m)=> {
                    // m.submenu.sort(function(a:any,b:any){return a.nav- b.nav;})
                });
                this.$scope.get_menu = menu;
                this.global.GetMenu.SetValue(this.$scope.get_menu);


                if (!this.$scope.isMap) {
                    if (menu.filter((m:IMenu)=> {
                            return m.Name == "maptaxi"
                        }).length != 0) {
                        this.$state.go('maptaxi');
                        //window.location.href = "/ban-do";
                    }else if(menu.filter((m:IMenu)=> {
                            return m.Name == "maptaxi"
                        }).length != 0){
                        this.$state.go('maptaxi');
                    }

                }
            }

            /*Tìm nhóm menu tương úng*/
            private FindGroupMenu(id:string):IMenuGroup {

                var tmp = this.$scope.MenuGroup.filter((m)=> {
                    return m.Id == id;
                });
                if (tmp == null || tmp == undefined || tmp.length == 0)
                    return null;
                return tmp[0];
            }

            private MenuIndexOfName(name:any):number {
                for (var i = 0; i < this.$scope.AllMenu.length; i++) {
                    if (this.$scope.AllMenu[i].Name === name)
                        return i;
                }
                return -1;
            }

            // private GetMenu():void{
            //
            //     for(var i = 0; i< this.$scope.AllMenu.length;i++){
            //         var req: any = {
            //             method: 'POST',
            //             url: 'http://auth.adsun.vn/Permission/NewPermission',
            //             headers: {token: this.$cookieStore.get('Token')},
            //             data: {Name: this.$scope.AllMenu[i].Name,
            //                 Description: this.$scope.AllMenu[i].Text}
            //         };
            //         this.$http(req).then((data)=>{
            //             console.log(data);
            //         });
            //     }
            // }

            private CreateGroupMenu():void {
                //todo: tạo các nhóm menu theo thiết kế ở đây, sau đó gán các menu với các id group tương ứng
                this.$scope.MenuGroup.push({
                    Text: 'Quản trị taxi',
                    Style: 'fa fa-automobile',
                    Menu: [],
                    Id: 'taxi'
                });
                // this.$scope.MenuGroup.push({
                //     Text: 'Theo dõi xe',
                //     Style: 'fa fa-automobile',
                //     Menu: [],
                //     Id: 'system'
                // });

                this.$scope.MenuGroup.push({
                    Text: 'Báo cáo',
                    Style: 'fa fa-server',
                    Menu: [],
                    Id:'report'
                });
                this.$scope.MenuGroup.push({
                    Text: 'Quản lý chung',
                    Style: 'fa fa-list',
                    Menu: [],
                    Id: 'general'
                });

                // this.$scope.MenuGroup.push({
                //     Text: 'Quản trị',
                //     Style: 'fa fa-cogs',
                //     Menu: [],
                //     Id: 'manager'
                // });

            }
        }

        AdModule.controller('IndexController', IndexController);
    }
}