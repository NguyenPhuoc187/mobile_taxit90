module App{
    export module Components{
        export module Enterprisemanage{
            export module ReportEnterprise{
                export class JourneyreportsRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper,global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'reportenterprise.journeyreports',
                            config: {
                                controller: Enterprisemanage.ReportEnterprise.Journeyreports.JourneyreportsController,
                                templateUrl: '/components/enterprisemanage/reportenterprise/journeyreports/journeyreports.html'+`?v=${global.Version}`,
                                url: '/bao-cao-hanh-trinh',
                                tag:{
                                    title:'Báo cáo hành trình',
                                    icon:'icon-merge',
                                    groupId:'report',
                                    index:0
                                }
                            }
                        });
                    }
                }
                AdModule.run(JourneyreportsRouteModel);
            }
        }
    }
}