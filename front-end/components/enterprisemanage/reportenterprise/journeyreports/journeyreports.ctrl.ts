/**
 * Created by phamn on 15/04/2016.
 */

module App{
    export module Components{
        export module Enterprisemanage{
            export module ReportEnterprise{
                export module Journeyreports {
                    export interface IJourneyreportsScope extends ng.IScope {
                        gridJourney: App.Shared.Directives.GridviewControl;
                        $parent: IReportEnterpriseScope;
                    }
                    export class JourneyreportsController{
                        public static $inject = ['$scope','$http','global','language','api' ];

                        constructor ($scope: IJourneyreportsScope, public $http: ng.IHttpService, public global: App.Shared.GlobalService,public language:App.Shared.LanguageService, public api:App.Shared.Api.IApi){
                            // $scope.$parent.checkbox_drop = false;
                            // $scope.$parent.showdevices = true;
                            // $scope.$parent.time31 = true;

                            $scope.$parent.checkbox_drop = true; // bật tính năng chọn xe từng chiếc
                            $scope.$parent.checkbox_nhienlieu = false; // hiển thị chọn xe theo nhiên liệu
                            $scope.$parent.checkbox_dropcar = false; // chọn xe theo check box
                            $scope.$parent.showdevices = true; // chọn hiển thị cả ô xe
                            $scope.$parent.time31 = false; // không hiển thị lịch chọn 2 ngày


                            $scope.gridJourney = new App.Shared.Directives.GridviewControl("gridJourney");
                            $scope.gridJourney.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridJourney.filename_excel="Báo cáo hành trình";
                            $scope.gridJourney.columns = [];
                            $scope.gridJourney.height = window.innerHeight - 170;
                            $scope.gridJourney.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            $scope.gridJourney.isAltRows=true;
                            $scope.gridJourney.isFilter=true;
                            $scope.gridJourney.isExport=true;
                            $scope.gridJourney.isFilterMode= 'simple';
                            $scope.gridJourney.columns.push({ text: 'STT', dataField: 'STT', type: 'string', align: 'center', width:'35px', });
                            $scope.gridJourney.columns.push({ text: 'Thời điểm', dataField: 'TimeUpdate', type: 'string', align: 'center', width:'75px',filterable:true,  });
                            $scope.gridJourney.columns.push({ text: 'Tọa độ', dataField: 'Position', type: 'string', align: 'center', width:'80px'  });
                            $scope.gridJourney.columns.push({ text: 'Địa chỉ', dataField: 'Address', type: 'string', align: 'center', width:'auto'  });
                            // $scope.gridJourney.columns.push({ text: 'Ghi chú', dataField: 'Note', type: 'string', align: 'center', width:'auto'  });
                            //$scope.gridJourney.data = m;
                            /*Khi nhấn nút Xem*/
                            $scope.$parent.btViewClick = () => {
                                var starttime_xphut  = `${$scope.$parent.dtTimeStartSelect2ngay.GetTimeClockDefault()} ${$scope.$parent.dtTimeStartSelect2ngay.GetPickerDate_upserver()}`;
                                var endtime = `${$scope.$parent.dtTimeEndSelect2ngay.GetTimeClockDefault()} ${$scope.$parent.dtTimeEndSelect2ngay.GetPickerDate_upserver()}`;
                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined) {
                                    if ($scope.$parent.modelSelectSettings.itemSelect() != undefined){
                                        language.showLoading(false,"");
                                        api.GetDeviceTripBySerial($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.modelSelectSettings.itemSelect().Serial,starttime_xphut, endtime).then((data:Array<any>)=>{
                                            language.showLoading(true,"");
                                            // console.log(data);
                                            AdapterLocation(data);
                                            $scope.gridJourney.data = data;
                                            // AdapterLocation($scope.gridJourney.data);
                                            $scope.gridJourney.refresh();
                                        });
                                        // $http.get(global.ReportHost + `/api/Qc31Report/GetDeviceTripBySerial?companyId=${$scope.$parent.cpnSelectSettings.valueMember}&serial=${$scope.$parent.modelSelectSettings.valueMember}&beginTime=${global.ChangeTime($scope.timeStart)}&endTime=${global.ChangeTime($scope.timeEnd)}`).then((rep:any) => {
                                        //     language.showLoading(true,"");
                                        //     $.each(rep.data.DeviceTripLogList, function (index, value) {
                                        //         AdapterLocation(value);
                                        //         value.STT=index +1;
                                        //
                                        //     });
                                        //     $scope.gridJourney.data = rep.data.DeviceTripLogList;
                                        //     $scope.gridJourney.refresh();
                                        // });
                                    }
                                    else {
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Vui lòng chọn xe !!",
                                            buttons: {
                                                cancle: {
                                                    label: "Quay lại",
                                                    className: "btn-primary",
                                                }
                                            }
                                        });
                                    }
                                }else{
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Vui lòng chọn công ty !!",
                                        buttons: {
                                            cancle: {
                                                label: "Quay lại",
                                                className: "btn-primary",
                                            }
                                        }
                                    });
                                }
                            };
                            /*Hàm chuyển hiển thị Location và địa chỉ*/
                            function AdapterLocation (value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT=ind +1;
                                    val.Position = global.ChangeLocation(val.Location);

                                    if(val.Location.Address == null){
                                        val.Address = 'Không có địa chỉ';
                                    }else{
                                        val.Address = `${val.Location.Address}`;
                                    }

                                    val.TimeUpdate = AdapterDate(val.TimeUpdate);
                                    if(val.Type=='Trace'){
                                        val.Note = '<label class="label label-success" style="font-size:13px">Xe chạy bình thường</label>';
                                    }
                                    else if(val.Type=='Pause'){
                                        val.Note = '<label class="label label-success" style="font-size:13px">Xe dừng</label>';
                                    }
                                    else if(val.Type=='LostGps'){
                                        val.Note = '<label class="label label-danger" style="font-size:13px">Xe mất Gps</label>';
                                    }
                                    else if(val.Type=='LostGsm'){
                                        val.Note = '<label class="label label-danger" style="font-size:13px">Xe mất gsm</label>';
                                    }
                                    else if(val.Type=='OverSpeed'){
                                        val.Note = '<label class="label label-danger" style="font-size:13px">Xe quá tốc độ</label>';
                                    }
                                    else if(val.Type=='OffKey'){
                                        val.Note = '<label class="label label-warning" style="font-size:13px">Tắt máy</label>';
                                    }
                                    else if(val.Type=='OnKey'){
                                        val.Note = '<label class="label label-primary" style="font-size:13px">Mở máy</label>';
                                    }
                                    else if(val.Type=='Door'){
                                        val.Note = '<label class="label label-primary" style="font-size:13px">Xe đóng mở cửa</label>';
                                    }
                                    else{
                                        val.Note = `<label class="label label-danger" style="font-size:13px">Khác</label>`;
                                    }
                                });

                            }
                            //hàm chuyen thời gian
                            function AdapterDate(time:any){
                                var year:string = time.slice(0,4);
                                var month:string = time.slice(5,7);
                                var date:string = time.slice(8,10);

                                var min:string = time.slice(11);

                                var tt:any = min +' ' +date + '/' + month + '/' + year ;
                                return tt;
                            }
                        }
                    }
                    AdModule.controller('JourneyreportsController',JourneyreportsController);
                }
            }
        }
    }
}