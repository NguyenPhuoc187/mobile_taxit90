module App{
    export module Components{
        export module Enterprisemanage{
            export module ReportEnterprise{
                export class TcdbreportRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper,global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'reportenterprise.tcdbreport',
                            config: {
                                controller: Enterprisemanage.ReportEnterprise.Tcdbreport.TcdbreportController,
                                templateUrl: '/components/enterprisemanage/reportenterprise/tcdbreport/tcdbreport.html'+`?v=${global.Version}`,
                                url: '/bao-cao-truyen-du-lieu-duong-bo',
                                tag:{
                                    title:'Báo cáo truyền TCĐB',
                                    icon:'icon-satellite-dish2',
                                    groupId:'report',
                                    index:19
                                }
                            }
                        });
                    }
                }
                AdModule.run(TcdbreportRouteModel);
            }
        }
    }
}