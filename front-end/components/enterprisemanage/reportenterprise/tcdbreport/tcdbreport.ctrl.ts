/**
 * Created by Tuantv on 05/24/2016.
 */
module App{
    export module Components{
        export module Enterprisemanage{
            export module ReportEnterprise{
                export module Tcdbreport{

                    export interface ITcdbreportScope extends ng.IScope {
                        gridTruyenTongcuc: App.Shared.Directives.GridviewControl;
                        $parent: IReportEnterpriseScope // biến $parrent hỗ trợ của angular gọi biến scope cha
                        // checkUseKmCo:boolean;
                    }
                    export class TcdbreportController{
                        public static $inject = ['$scope','$http','global','language','$q', 'api'];

                        constructor ($scope: ITcdbreportScope, public $http: ng.IHttpService, public global: App.Shared.GlobalService,public language:App.Shared.LanguageService,$q:ng.IQService, public api: App.Shared.Api.IApi) {
                            $scope.$parent.checkbox_drop = false; // bật tính năng chọn xe từng chiếc
                            $scope.$parent.checkbox_nhienlieu = false; // hiển thị chọn xe theo nhiên liệu
                            $scope.$parent.checkbox_dropcar = true; // chọn xe theo check box
                            $scope.$parent.showdevices = true; // chọn hiển thị cả ô xe
                            $scope.$parent.time31 = true; // không hiển thị lịch chọn 2 ngày


                            $scope.gridTruyenTongcuc = new App.Shared.Directives.GridviewControl("gridTruyenTongcuc");
                            $scope.gridTruyenTongcuc.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            /*Bật tính năng tính tổng các cột*/
                            $scope.gridTruyenTongcuc.isshowAggregates = true;
                            $scope.gridTruyenTongcuc.filename_excel="Báo cáo truyền dữ liệu TCĐB";
                            $scope.gridTruyenTongcuc.columns = [];
                            $scope.gridTruyenTongcuc.height = window.innerHeight - 180;
                            $scope.gridTruyenTongcuc.columnsHeight = 40;
                            //bật tính năng lọc dữ liệu
                            $scope.gridTruyenTongcuc.isAltRows=true;
                            $scope.gridTruyenTongcuc.isFilter=true;
                            $scope.gridTruyenTongcuc.isExport=true;
                            $scope.gridTruyenTongcuc.isFilterMode= 'simple';
                            $scope.gridTruyenTongcuc.columns.push({ text: 'STT', dataField: 'STT', type: 'number', align: 'center', width:"35px",pinned:true });
                            $scope.gridTruyenTongcuc.columns.push({ text: 'Biển số', dataField: 'Bs', type: 'string', align: 'center', width:"85px",filterable:true,});
                            $scope.gridTruyenTongcuc.columns.push({ text: 'Thời gian', dataField: 'DateTime', type: 'date', align: 'center', width:"85px",filterable:true, cellsFormat:'dd/MM/yyyy'});
                            $scope.gridTruyenTongcuc.columns.push({ text: 'Số bản tin ADSUN đã truyền TCĐB', dataField: 'Total', type: 'number', align: 'center', width:"130px",filterable:true,className:'gridbaocao' });
                            $scope.gridTruyenTongcuc.columns.push({ text: 'Ghi chú', dataField: 'Ghichu', type: 'string', align: 'center', width:"auto",filterable:true});


                            $scope.$parent.btViewClick = () => {
                                var starttime_xphut  = `${$scope.$parent.dtTimeStartSelect.GetTimeClockDefault()} ${$scope.$parent.dtTimeStartSelect.GetPickerDate_upserver()}`;
                                var endtime = `${$scope.$parent.dtTimeEndSelect.GetTimeClockDefault()} ${$scope.$parent.dtTimeEndSelect.GetPickerDate_upserver()}`;
                                language.showLoading(false,"");
                                // trường hợp đội xe chọn khác tất cả
                                if($scope.$parent.groupSelectSettings.itemSelect().Id !== -1){
                                    // trường hợp có chọn xe
                                    if($scope.$parent.modelSelectSettings_multi.valueSelects().length > 0){
                                        // chọn tất cả xe
                                        if($scope.$parent.modelSelectSettings_multi.valueSelects()[0] === '-1'){
                                            GetDataWithListSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,starttime_xphut,endtime,$scope.$parent.modelSelectSettings_multi.valueSelects().slice(1));
                                        }else{
                                            GetDataWithListSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,starttime_xphut,endtime,$scope.$parent.modelSelectSettings_multi.valueSelects())
                                        }
                                    }else{
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Vui lòng chọn xe !!",
                                        });
                                        return;
                                    }
                                }
                                // trường hợp đội xe chọn tất cả
                                else{
                                    if($scope.$parent.modelSelectSettings_multi.valueSelects().length > 0){
                                        // chọn tất cả xe
                                        if($scope.$parent.modelSelectSettings_multi.valueSelects()[0] === '-1'){
                                            GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id,starttime_xphut,endtime);
                                        }else{
                                            GetDataWithListSerial($scope.$parent.cpnSelectSettings.itemSelect().Id,starttime_xphut,endtime,$scope.$parent.modelSelectSettings_multi.valueSelects())
                                        }
                                    }else{
                                        GetDataWithCompany($scope.$parent.cpnSelectSettings.itemSelect().Id,starttime_xphut,endtime);
                                    }
                                }

                            };

                            function GetDataWithCompany(companyId:number,beginTime:string,endTime:string){
                                $http.get(global.ReportHost + `api/Qc31Report/GeneralCompanyReportByCompany?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`).then((rep:any) => {
                                    language.showLoading(true,"");
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            $scope.gridTruyenTongcuc.data = rep.data.GeneralCompanyReportList;
                                            ConvertData($scope.gridTruyenTongcuc.data);
                                            $scope.gridTruyenTongcuc.refresh();
                                        }
                                    }
                                });
                            }
                            function GetDataWithListSerial(companyId:number,beginTime:string,endTime:string, listserial:any){
                                language.showLoading(true,"");
                                $http.post(global.ReportHost + `api/Qc31Report/GeneralCompanyReportBySerial?companyId=${companyId}&beginTime=${beginTime}&endTime=${endTime}`,listserial).then((rep:any) => {
                                    if(rep !== undefined){
                                        if(rep.data.Status == 1){
                                            $scope.gridTruyenTongcuc.data = rep.data.GeneralCompanyReportList;
                                            ConvertData($scope.gridTruyenTongcuc.data);
                                            $scope.gridTruyenTongcuc.refresh();
                                        }
                                    }
                                });
                            }

                            function ConvertData(value:any){
                                $.each(value,(ind:number,val:any)=>{
                                    val.STT = ind + 1;
                                    $.each(val.TruyenTongCuc,(index:number,value:any)=>{
                                        if(value.Id === "7770b6d6-10c0-42a2-a9d7-9fda8069bef3" ){
                                            val.Total = value.Total;
                                        }
                                    });
                                });
                            }



                            
                        }
                    }
                    AdModule.controller('TcdbreportController',TcdbreportController);
                }
            }
        }
    }
}