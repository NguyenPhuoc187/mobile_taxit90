module App{
    export module Components{
        export module Enterprisemanage{
            export module ReportEnterprise{
                export class LostdatareportRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper,global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'reportenterprise.lostdatareport',
                            config: {
                                controller: Enterprisemanage.ReportEnterprise.Lostdatareport.LostdatareportController,
                                templateUrl: '/components/enterprisemanage/reportenterprise/lostdatareport/lostdatareport.html'+`?v=${global.Version}`,
                                url: '/bao-cao-mat-du-lieu',
                                tag:{
                                    title:'Báo cáo mất dữ liệu',
                                    icon:'icon-connection',
                                    groupId:'report',
                                    index:11
                                }
                            }
                        });
                    }
                }
                AdModule.run(LostdatareportRouteModel);
            }
        }
    }
}