/**
 * Created by phamn on 30/08/2016.
 */
module App{
    export module Components{
        export module Enterprisemanage{
            export module ReportEnterprise{
                export module Lostdatareport{
                    export interface ILostdatareportScope extends ng.IScope {

                        gridLostDataReport: App.Shared.Directives.GridviewControl;
                        $parent: IReportEnterpriseScope;
                    }
                    export class LostdatareportController{
                        public static $inject = ['$scope','$http', 'global','language','api'];

                        constructor ($scope: ILostdatareportScope, public $http: ng.IHttpService, public global: App.Shared.GlobalService,public language:App.Shared.LanguageService, public api: App.Shared.Api.IApi){
                            // $scope.$parent.checkbox_drop = true;
                            // $scope.$parent.showdevices = true;
                            // $scope.$parent.time31 = true;

                            $scope.$parent.checkbox_drop = false; // bật tính năng chọn xe từng chiếc
                            $scope.$parent.checkbox_nhienlieu = false; // hiển thị chọn xe theo nhiên liệu
                            $scope.$parent.checkbox_dropcar = true; // chọn xe theo check box
                            $scope.$parent.showdevices = true; // chọn hiển thị cả ô xe
                            $scope.$parent.time31 = true; // không hiển thị lịch chọn 2 ngày


                            $scope.gridLostDataReport = new App.Shared.Directives.GridviewControl("gridLostSignReport");
                            $scope.gridLostDataReport.filename_excel="Chi tiết mất tín hiệu";
                            $scope.gridLostDataReport.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridLostDataReport.columns = [];
                            $scope.gridLostDataReport.height = window.innerHeight - 150;
                            $scope.gridLostDataReport.columnsHeight = 40;

                            //bật tính năng lọc dữ liệu
                            $scope.gridLostDataReport.isAltRows=true;
                            $scope.gridLostDataReport.isFilter=true;
                            $scope.gridLostDataReport.isExport=true;
                            $scope.gridLostDataReport.isFilterMode= 'simple';
                            $scope.gridLostDataReport.columns.push({ text: 'STT', dataField: 'Stt', type: 'string', align: 'center',width:'35px',pinned:true });
                            //$scope.gridLostSignReport.columns.push({ text: 'Serial', dataField: 'Serial', type: 'string', align: 'center',width:'50px' });
                            $scope.gridLostDataReport.columns.push({ text: 'Biển số', dataField: 'Bs', type: 'string', align: 'center',width:'100px',filterable:true});
                            $scope.gridLostDataReport.columns.push({ text: 'TG bắt đầu', dataField: 'BeginTime', type: 'string', align: 'center',width:'110px' });
                            $scope.gridLostDataReport.columns.push({ text: 'TG kết thúc', dataField: 'EndTime', type: 'string', align: 'center',width:'110px' });
                            $scope.gridLostDataReport.columns.push({ text: 'Kéo dài', dataField: 'TimeTotal', type: 'string', align: 'center',width:'80px' });
                            $scope.gridLostDataReport.columns.push({ text: 'Địa điểm bắt đầu', dataField: 'PositionStart', type: 'string', align: 'center',width:'150px' });
                            $scope.gridLostDataReport.columns.push({ text: 'Địa điểm kết thúc', dataField: 'PositionEnd', type: 'string', align: 'center',width:'150px' });
                            $scope.gridLostDataReport.columns.push({ text: 'Trạng thái', dataField: 'CurrentState', type: 'string', align: 'center',width:'auto',filterable:true });


                            /*Khi nhấn nút View*/
                            $scope.$parent.btViewClick = () =>{
                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined){
                                    var starttime_xphut  = `${$scope.$parent.dtTimeStartSelect.GetTimeClockDefault()} ${$scope.$parent.dtTimeStartSelect.GetPickerDate_upserver()}`;
                                    var endtime = `${$scope.$parent.dtTimeEndSelect.GetTimeClockDefault()} ${$scope.$parent.dtTimeEndSelect.GetPickerDate_upserver()}`;
                                    if($scope.$parent.modelSelectSettings_multi.valueSelects().length !== 0 && $scope.$parent.modelSelectSettings_multi.valueSelects() !== undefined) {
                                        if($scope.$parent.modelSelectSettings_multi.valueSelects()[0] == '-1'){

                                            language.showLoading(false,"");
                                            $http.post(global.ReportHost + `api/Device/LostDataDevice?companyId=${$scope.$parent.cpnSelectSettings.itemSelect().Id}&beginTime=${starttime_xphut}&endTime=${endtime}`,$scope.$parent.modelSelectSettings_multi.allValue()).then((rep:any)=>{
                                                language.showLoading(true,"");
                                                $scope.gridLostDataReport.data = rep.data.LostDataDeviceList;
                                                AdapterData($scope.gridLostDataReport.data);
                                                $scope.gridLostDataReport.refresh();
                                            });
                                        }else{
                                            language.showLoading(false,"");
                                            $http.post(global.ReportHost + `api/Device/LostDataDevice?companyId=${$scope.$parent.cpnSelectSettings.itemSelect().Id}&beginTime=${starttime_xphut}&endTime=${endtime}`,$scope.$parent.modelSelectSettings_multi.valueSelects()).then((rep:any)=>{
                                                language.showLoading(true,"");
                                                $scope.gridLostDataReport.data = rep.data.LostDataDeviceList;
                                                AdapterData($scope.gridLostDataReport.data);
                                                $scope.gridLostDataReport.refresh();
                                            });
                                        }

                                    }else {
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Vui lòng chọn xe !!",
                                            buttons: {
                                                cancle: {
                                                    label: "Quay lại",
                                                    className: "btn-primary",
                                                }
                                            }
                                        });
                                        $scope.gridLostDataReport.clear();
                                    }

                                } else {
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Vui lòng chọn công ty !!",
                                        buttons: {
                                            cancle: {
                                                label: "Quay lại",
                                                className: "btn-primary",
                                            }
                                        }
                                    });
                                }
                            };
                            /*Hàm chuyển hiển thị Location và địa chỉ*/
                            function AdapterLocation (value:any){
                                value.AdrressStart = `${value.BeginLocation.Address}`;

                                value.AdrressEnd = `${value.EndLocation.Address}`;
                                if(value.DeviceStatusType=='Trace'){
                                    value.DeviceStatusType = `<label class="label label-success" style="font-size:13px">${'Xe chạy bình thường'}</label>`;
                                }
                                if(value.DeviceStatusType=='Pause'){
                                    value.DeviceStatusType = `<label class="label label-success" style="font-size:13px">${'Xe dừng'}</label>`;
                                }
                                if(value.DeviceStatusType=='LostGps'){
                                    value.DeviceStatusType = `<label class="label label-danger" style="font-size:13px">${'Xe mất Gps'}</label>`;
                                }
                                if(value.DeviceStatusType=='LostGsm'){
                                    value.DeviceStatusType = `<label class="label label-danger" style="font-size:13px">${'Xe mất gsm'}</label>`;
                                }
                                if(value.DeviceStatusType=='LostMemory'){
                                    value.DeviceStatusType = `<label class="label label-danger" style="font-size:13px">${'Không thẻ nhớ'}</label>`;
                                }

                            }

                            function AdapterData (tmp:any){
                                $.each(tmp, (index:number,value:any)=>{
                                    // value.AddressStart = `${value.BeginLocation.Address}`;
                                    // value.AddressEnd = `${value.EndLocation.Address}`;
                                    value.Stt=index + 1;
                                    // value.Km = value.DistanceGps / 1000  ;
                                    value.BeginTime = global.AdapterDate(value.BeginTime);
                                    value.EndTime = global.AdapterDate(value.EndTime);

                                    value.PositionStart = global.ChangeLocation(value.BeginPoint);
                                    value.PositionEnd = global.ChangeLocation(value.EndPoint);
                                });

                            }
                        }
                    }
                    AdModule.controller('LostdatareportController',LostdatareportController);
                }
            }
        }
    }
}