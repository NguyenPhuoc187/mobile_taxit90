/**
 * Created by NPPOPC on 22/03/2017.
 */
module App{
    export module Components{
        export module Enterprisemanage{
            export module ReportEnterprise{
                export module ParkingEnterprise {
                    export interface IParkingEnterpriseScope extends ng.IScope {
                        gridParkingEnterprise: App.Shared.Directives.GridviewControl;
                        $parent: IReportEnterpriseScope;
                        frommin: number;
                        tomin:number;
                        isThoigian:boolean;
                    }
                    export class ParkingEnterpriseController{
                        public static $inject = ['$scope','$rootScope','$http','global','language','api'];
                        constructor ($scope: IParkingEnterpriseScope,public $rootScope: App.Components.Enterprisemanage.Qcvn31.IQcvn31Scope, public $http: ng.IHttpService, public global: App.Shared.GlobalService,public language:App.Shared.LanguageService, public api: App.Shared.Api.IApi){
                            $rootScope.showComboboxGroupCar = false;
                            $rootScope.hideComboboxGroupCar = false;

                            $scope.$parent.checkbox_drop = true; // bật tính năng chọn xe từng chiếc
                            $scope.$parent.checkbox_nhienlieu = false; // hiển thị chọn xe theo nhiên liệu
                            $scope.$parent.checkbox_dropcar = false; // chọn xe theo check box
                            $scope.$parent.showdevices = true; // chọn hiển thị cả ô xe
                            $scope.$parent.time31 = true; // không hiển thị lịch chọn 2 ngày


                            $scope.gridParkingEnterprise = new App.Shared.Directives.GridviewControl("gridParkingEnterprise");
                            $scope.gridParkingEnterprise.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridParkingEnterprise.filename_excel="Báo cáo dừng đỗ";
                            $scope.gridParkingEnterprise.columns = [];
                            $scope.gridParkingEnterprise.height = window.innerHeight - 170;
                            $scope.gridParkingEnterprise.columnsHeight = 60;
                            //bật tính năng lọc dữ liệu
                            $scope.gridParkingEnterprise.isAltRows=true;
                            $scope.gridParkingEnterprise.isFilter=true;
                            $scope.gridParkingEnterprise.isExport=true;
                            $scope.gridParkingEnterprise.isFilterMode= 'simple';
                            //var m = [{Stt: '23',Bs: '259347592375',DriverName: 'Nguyễn văn B',Gplx: '09123222',type: 'xe bus',Timestop: '259347592375' } ];
                            $scope.gridParkingEnterprise.columns.push({ text: 'STT', dataField: 'Stt', type: 'string', align: 'center',width:"40px",pinned:true });
                            $scope.gridParkingEnterprise.columns.push({ text: 'Biển số', dataField: 'Bs', type: 'string', align: 'center',width:"80px",filterable:true });
                            $scope.gridParkingEnterprise.columns.push({ text: 'Tài xế', dataField: 'DriverName', type: 'string', align: 'center',width:"120px",filterable:true });
                            $scope.gridParkingEnterprise.columns.push({ text: 'GPLX', dataField: 'Gplx', type: 'string', align: 'center',width:"80px",filterable:true });
                            $scope.gridParkingEnterprise.columns.push({ text: 'Loại hình <br> kinh doanh', dataField: 'Type_mess', type: 'string', align: 'center',width:"80px",filterable:true});
                            $scope.gridParkingEnterprise.columns.push({ text: 'Thời điểm dừng đỗ', dataField: 'BeginTime', type: 'string', align: 'center',width:"90px" });
                            $scope.gridParkingEnterprise.columns.push({ text: 'Thời gian dừng đỗ ', dataField: 'PauseTime', type: 'string', align: 'center',width:"70px" });
                            // $scope.gridParkingEnterprise.columns.push({ text: 'Tọa độ dừng', dataField: 'Toado', type: 'string', align: 'center',width:"165px" });
                            $scope.gridParkingEnterprise.columns.push({ text: 'Địa điểm dừng đỗ', dataField: 'Address', type: 'string', align: 'center',width:"auto" });
                            $scope.isThoigian = false;
                            $scope.frommin = 0;
                            $scope.tomin = 0;

                            //$scope.gridParkingEnterprise.data = m;
                            $scope.$parent.checkboxes_combobox = false;
                            /*Khi nhấn nút Xem*/
                            $scope.$parent.btViewClick = () => {
                                var starttime_xphut  = `${$scope.$parent.dtTimeStartSelect.GetTimeClockDefault()} ${$scope.$parent.dtTimeStartSelect.GetPickerDate_upserver()}`;
                                var endtime = `${$scope.$parent.dtTimeEndSelect.GetTimeClockDefault()} ${$scope.$parent.dtTimeEndSelect.GetPickerDate_upserver()}`;
                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined) {
                                    if ($scope.$parent.modelSelectSettings.itemSelect() !== undefined){
                                        if($scope.frommin <= $scope.tomin){
                                            language.showLoading(false,"");
                                            api.GetDevicePauseBySerial($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.modelSelectSettings.itemSelect().Serial,starttime_xphut, endtime).then((data:any)=>{
                                                language.showLoading(true,"");
                                                $scope.gridParkingEnterprise.data = FilterData(data,$scope.tomin,$scope.frommin, $scope.isThoigian);
                                                AdapterLocation($scope.gridParkingEnterprise.data);
                                                $scope.gridParkingEnterprise.refresh();

                                            });
                                        }else{
                                            bootbox.dialog({
                                                title: "Thông Báo",
                                                message: "Vui lòng chọn đúng khoảng phút dừng đỗ!!",
                                                buttons: {
                                                    cancle: {
                                                        label: "Quay lại",
                                                        className: "btn-primary",
                                                    }
                                                }
                                            });
                                        }

                                    }
                                    else {
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Vui lòng chọn xe !!",
                                            buttons: {
                                                cancle: {
                                                    label: "Quay lại",
                                                    className: "btn-primary",
                                                }
                                            }
                                        });
                                    }

                                }else{
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Vui lòng chọn công ty !!",
                                        buttons: {
                                            cancle: {
                                                label: "Quay lại",
                                                className: "btn-primary",
                                            }
                                        }
                                    });
                                }
                            };
                            /*Hàm chuyển hiển thị Location*/
                            function AdapterLocation (value:any){
                                $.each(value, (idx:number,val:any)=>{
                                    val.Toado = global.ChangeLocation(val.Point);
                                    val.Address=val.Point.Address;
                                    val.Stt=idx+1;

                                    //chuyển đổi hiện thị lên grid-view ở loại hình kinh doanh
                                    val.Type_mess = global.AdpterChangeLoaiHinhKinhDoanh(val.ActivityType);
                                    val.BeginTime = global.AdapterDate(val.BeginTime);
                                });
                            }
                            function FilterData(value:any, tomin:number, frommin:number,isThoigian:boolean){
                                var result:any = [];
                                if(!isThoigian){
                                    return value;
                                }else{
                                    $.each(value,(ind:number,val:any)=>{
                                        let min:any = (new Date(val.EndTime).getTime() - new Date(val.BeginTime).getTime())/60000;
                                        if(min < tomin && min > frommin){
                                            result.push(val);
                                        }
                                    });
                                    return result;
                                }

                            }
                        }
                    }
                    AdModule.controller('ParkingEnterpriseController',ParkingEnterpriseController);
                }
            }
        }
    }
}