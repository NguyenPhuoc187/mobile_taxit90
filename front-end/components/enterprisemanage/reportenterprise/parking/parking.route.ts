module App{
    export module Components{
        export module Enterprisemanage{
            export module ReportEnterprise{
                export class ParkingEnterpriseRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper,global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'reportenterprise.parking',
                            config: {
                                controller: Enterprisemanage.ReportEnterprise.ParkingEnterprise.ParkingEnterpriseController,
                                templateUrl: '/components/enterprisemanage/reportenterprise/parking/parking.html'+`?v=${global.Version}`,
                                url: '/bao-cao-dung-do',
                                tag:{
                                    title:'Báo cáo dừng đỗ',
                                    icon:'icon-file-text',
                                    groupId:'report',
                                    index:15
                                }
                            }
                        });
                    }
                }
                AdModule.run(ParkingEnterpriseRouteModel);
            }
        }
    }
}