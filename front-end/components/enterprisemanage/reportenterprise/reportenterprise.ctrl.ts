/**
 * Created by phamn on 14/04/2016.
 */

module App{
    export module Components{
        export module Enterprisemanage{
            export module ReportEnterprise{
                import IGroup = App.Shared.Entity.IGroup;
                export interface IReportEnterpriseScope extends ng.IScope {
                    /*Khai báo thuộc tính drop-down*/
                    cpnSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.ICompany>;
                    groupSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IGroup>;
                    modelSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IDevice>;
                    modelNhienlieuSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IDevice>;
                    dtTimeSelect:App.Shared.Gui.IDateTimeControl;
                    dtTimeSelect2day:App.Shared.Gui.IDateTimeControl;
                    modelSelectSettings_multi: App.Shared.Gui.IDropDown<App.Shared.Entity.IDevice>
                    locationSelectSettings: any;
                    pointSelectSettings:any;
                    btViewClick:() => any;
                    /*Khai báo thuộc tính của langugae*/
                    viewData:string;
                    checkbox_drop: boolean;
                    checkbox_nhienlieu: boolean;
                    checkbox_dropcar: boolean;
                    showdevices:boolean;
                    location_drop:boolean;
                    time31: boolean;

                    dtTimeStartSelect:any;
                    dtTimeEndSelect:any;

                    dtTimeStartSelect2ngay:any;
                    dtTimeEndSelect2ngay:any;
                }
                export class ReportEnterpriseController{
                    public static $inject = ['$scope','language','global', '$http', '$timeout','api','$q'];

                    constructor ($scope: IReportEnterpriseScope,public language:App.Shared.LanguageService, public global: App.Shared.GlobalService, public $http: ng.IHttpService, public $timeout: ng.ITimeoutService,
                    api:App.Shared.Api.IApi, $q: ng.IQService){

                        $scope.checkbox_drop = true;
                        $scope.checkbox_nhienlieu = false;
                        $scope.checkbox_dropcar = false;
                        $scope.showdevices = true;
                        $scope.time31 = true;
                        $scope.viewData=language.gridLg().viewData;
                        // Khai báo mới combobox truyền vào 3 thông số : tên định dạng của combobox, hiển thị tên trường, giá trị chọn
                        $scope.cpnSelectSettings =new App.Shared.Gui.DropDown<App.Shared.Entity.ICompany>('cbCompanyEnterpriseMaster','Display','Id');
                        // Tiêu đề trên combobox
                        $scope.cpnSelectSettings.title=language.gridLg().company;
                        // Set chiều dài cho combobox
                        $scope.cpnSelectSettings.setting.width = '100%';
                        $scope.cpnSelectSettings.setting.checkboxes = false;
                        $scope.cpnSelectSettings.setting.filterPlaceHolder = 'Tìm công ty...';

                        $scope.groupSelectSettings=new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('cbGroupEnterpriseMaster','Name','Id');
                        $scope.groupSelectSettings.title=language.gridLg().group;
                        $scope.groupSelectSettings.setting.width = '100%';
                        $scope.groupSelectSettings.setting.checkboxes = false;
                        $scope.groupSelectSettings.setting.filterPlaceHolder = 'Tìm đội xe...';

                        $scope.modelSelectSettings=new App.Shared.Gui.DropDown<App.Shared.Entity.IDevice>('cbDeviceEnterpriseMaster','Bs','Serial');
                        $scope.modelSelectSettings.title=language.gridLg().car;
                        $scope.modelSelectSettings.setting.width = '100%';
                        $scope.modelSelectSettings.setting.placeHolder = 'Chọn xe';
                        $scope.modelSelectSettings.setting.checkboxes = false;


                        $scope.modelNhienlieuSelectSettings=new App.Shared.Gui.DropDown<App.Shared.Entity.IDevice>('cbDeviceNhienlieuEnterpriseMaster','Bs','Serial');
                        $scope.modelNhienlieuSelectSettings.title=language.gridLg().car;
                        $scope.modelNhienlieuSelectSettings.setting.width = '100%';
                        $scope.modelNhienlieuSelectSettings.setting.placeHolder = 'Chọn xe';
                        $scope.modelNhienlieuSelectSettings.setting.checkboxes = false;

                        $scope.modelSelectSettings_multi = new App.Shared.Gui.DropDown<App.Shared.Entity.IDevice>('dropDeviceMutiEnterpriseMaster','Bs','Serial');
                        $scope.modelSelectSettings_multi.title=language.gridLg().car;
                        $scope.modelSelectSettings_multi.setting.width = '100%';
                        $scope.modelSelectSettings_multi.setting.placeHolder = 'Chọn xe';

                        $scope.dtTimeSelect=new App.Shared.Gui.DateTimeControl('dtTimeSelectEnterpriseMaster');
                        $scope.dtTimeSelect.titleTime=language.gridLg().titleTime;

                        $scope.dtTimeSelect2day=new App.Shared.Gui.DateTimeControl('dtTimeSelectEnterpriseMaster2day');
                        $scope.dtTimeSelect2day.titleTime=language.gridLg().titleTime;
                        $scope.dtTimeSelect2day.limitday = 1;

                        // $scope.dtTimeSelect.limitday = 2;


                        // Dropdown điểm
                        $scope.pointSelectSettings = new App.Shared.Gui.DropDown<any>('dropPointEnterpriseMaster','Name','Id');
                        $scope.pointSelectSettings.title= 'Điểm';
                        $scope.pointSelectSettings.setting.width = '100%';
                        $scope.pointSelectSettings.setting.placeHolder = 'Chọn điểm';
                        $scope.pointSelectSettings.setting.filterPlaceHolder = 'Tìm điểm ...';

                        // Dropdown vùng
                        $scope.locationSelectSettings = new App.Shared.Gui.DropDown<any>('dropLocationEnterpriseMaster','Name','Id');
                        $scope.locationSelectSettings.title= 'Vùng';
                        $scope.locationSelectSettings.setting.width = '100%';
                        $scope.locationSelectSettings.setting.placeHolder = 'Chọn vùng';
                        $scope.locationSelectSettings.setting.filterPlaceHolder = 'Tìm vùng ...';

                        $scope.dtTimeStartSelect= new App.Shared.Gui.DateTimeControl('dtTimeSelectStartMobileReportEnterpriseTaxi');
                        $scope.dtTimeStartSelect.titleTime='';
                        $scope.dtTimeStartSelect.minday = 0;
                        // var min = $scope.dtTimeEndSelect.GetPickerDate();

                        $scope.dtTimeEndSelect=new App.Shared.Gui.DateTimeControl('dtTimeSelectEndMobileReportEnterpriseTaxi');
                        $scope.dtTimeEndSelect.titleTime='';
                        $scope.dtTimeEndSelect.minday = -3;
                        //hiển thị button quay về
                        $scope.dtTimeStartSelect.valuetime_default = '00:00';
                        $scope.dtTimeEndSelect.valuetime_default = '23:59';
                        $scope.dtTimeStartSelect.onSelect = () =>{
                            // console.log('alo');
                            $scope.dtTimeEndSelect.SetMinDatePicker($scope.dtTimeStartSelect.GetPickerDate_setmin());
                        };



                        $scope.dtTimeStartSelect2ngay= new App.Shared.Gui.DateTimeControl('dtTimeSelectStartMobileReportEnterpriseTaxi2');
                        $scope.dtTimeStartSelect2ngay.titleTime='';
                        $scope.dtTimeStartSelect2ngay.minday = 0;
                        // var min = $scope.dtTimeEndSelect.GetPickerDate();

                        $scope.dtTimeEndSelect2ngay=new App.Shared.Gui.DateTimeControl('dtTimeSelectEndMobileReportEnterpriseTaxi2');
                        $scope.dtTimeEndSelect2ngay.titleTime='';
                        $scope.dtTimeEndSelect2ngay.minday = -2;
                        $scope.dtTimeEndSelect2ngay.limitday = 1;
                        //hiển thị button quay về
                        $scope.dtTimeStartSelect2ngay.valuetime_default = '00:00';
                        $scope.dtTimeEndSelect2ngay.valuetime_default = '23:59';
                        $scope.dtTimeStartSelect2ngay.onSelect = () =>{
                            // console.log('alo');
                            $scope.dtTimeEndSelect2ngay.SetMinDatePicker($scope.dtTimeStartSelect2ngay.GetPickerDate_setmin());
                        };





                        // load công ty 2 thông số truyền vào : dữ liệu lấy về, vị trí truyền vào ban đầu
                        // api.GetAllCompany().then((data:any)=>{
                        //     var result_promise=$q.defer();
                        //     //todo: thêm dạng promise để lấy dữ liệu được
                        //     var result:any = [];
                        //     $.each(data,(ind,val)=>{
                        //         if(val.Id !== 18){
                        //             result.push(val);
                        //         }
                        //    });
                        //     $scope.cpnSelectSettings.loaddataarraywithapi(result_promise.promise,$q,true);
                        //     result_promise.resolve(result);
                        // });
                        // $scope.cpnSelectSettings.load(api.GetAllCompany(),null, true);
                        $scope.cpnSelectSettings.loadPromise(api.GetAllCompany(), null,$q).then(()=>{
                            // console.log(localStorage.getItem('companyIdmapxekhach'));
                            if(localStorage.getItem('companyIdquantri') !== '' && localStorage.getItem('companyIdquantri') !== null){
                                console.log(localStorage.getItem('companyIdquantri'));
                                let companyid:any = localStorage.getItem('companyIdquantri');
                                $scope.cpnSelectSettings.setItemDefault((m:any)=>m.Id === companyid*1);
                            }else{
                                $scope.cpnSelectSettings.setting.jqxDropDownList('selectIndex',0);
                            }

                        });


                        // cài đặt sự kiện chọn công ty
                        $scope.cpnSelectSettings.customEvent.onSelectOnItem=(index:number,val:App.Shared.Entity.ICompany)=>{
                            localStorage.setItem('companyIdquantri',`${val.Id}`,);
                            var first=Array<App.Shared.Entity.IGroup>();
                            // đẩy 1 group làm thông tin chọn tất cả
                            first.push( {
                                Id: -1,
                                Name: 'Tất cả',
                                CompanyId:val.Id
                            });

                            var first_zone =Array<any>();
                            // đẩy 1 group làm thông tin chọn tất cả
                            first_zone.push( {
                                Id: -1,
                                Name: 'Tất cả',
                            });
                            var first_point =Array<any>();
                            // đẩy 1 group làm thông tin chọn tất cả
                            first_point.push( {
                                Id: -1,
                                Name: 'Tất cả',
                            });
                            $scope.groupSelectSettings.load(api.GetAllGroupByCompanyId(val.Id),first, true);
                            $scope.pointSelectSettings.load(api.GetPOintByCompany(val.Id),first_point);
                            $scope.locationSelectSettings.load(api.GetZoneByCompany(val.Id),first_zone);
                        };
                        // cài đặt sự kiện chọn đội xe
                        $scope.groupSelectSettings.customEvent.onSelectOnItem=(index:number,val:IGroup)=>{
                            // if($scope.modelSelectSettings.itemSelect() !== undefined){
                            //     $scope.modelSelectSettings.setting.jqxComboBox('unselectItem',$scope.modelSelectSettings.itemSelect().Serial);
                            // }
                            // console.log($scope.modelSelectSettings.itemSelect());
                            var firstdevice=Array<App.Shared.Entity.IDevice>();
                            // đẩy 1 group làm thông tin chọn tất cả
                            firstdevice.push( {
                                Serial: '-1',
                                Bs: 'Tất cả',
                                
                            });
                            if(val.Id == -1){// hiển thị tất cả các xe trong công ty

                                api.GetAllDeviceByCompanyId(val.CompanyId).then((data:any)=>{
                                    var result: any = [];
                                    $scope.modelSelectSettings.setting.source = data;
                                    $scope.modelSelectSettings_multi.setting.source = data;
                                    $.each(data,(ind:number,val:any)=>{
                                        if(val.IsNhienLieu){
                                            result.push(val);
                                        }
                                    });
                                    $scope.modelNhienlieuSelectSettings.setting.source = result;
                                });

                                // $scope.modelSelectSettings.load(api.GetAllDeviceByCompanyId(val.CompanyId),null,false);
                                // $scope.modelSelectSettings_multi.load(api.GetAllDeviceByCompanyId(val.CompanyId),firstdevice,false);
                                // if($scope.modelSelectSettings.itemSelect() !== undefined){
                                //     $scope.modelSelectSettings.itemSelect().Serial = undefined;
                                // }
                            }
                            else{// hiểm thị các xe trong đội xe đã chọn
                                api.GetAllDeviceByGroupId(val.CompanyId,val.Id).then((data:any)=>{
                                    var result: any = [];
                                    $scope.modelSelectSettings.setting.source = data;
                                    $scope.modelSelectSettings_multi.setting.source = data;
                                    $.each(data,(ind:number,val:any)=>{
                                        if(val.IsNhienLieu){
                                            result.push(val);
                                        }
                                    });
                                    $scope.modelNhienlieuSelectSettings.setting.source = result;
                                });

                                // $scope.modelSelectSettings.load(api.GetAllDeviceByGroupId(val.CompanyId,val.Id),null,false);
                                // $scope.modelSelectSettings_multi.load(api.GetAllDeviceByGroupId(val.CompanyId,val.Id),firstdevice,false);
                                // if($scope.modelSelectSettings.itemSelect() !== undefined){
                                //     $scope.modelSelectSettings.itemSelect().Serial = undefined;
                                // }

                            }
                        };
                        $scope.locationSelectSettings.customEvent.onCheckChange = (check:boolean, val:any)=>{
                            if(val.originalItem.Id === -1){
                                if(check){
                                    $scope.locationSelectSettings.setting.jqxDropDownList('checkAll');
                                }else{
                                    $scope.locationSelectSettings.setting.jqxDropDownList('uncheckAll');
                                }
                            }
                        };
                        $scope.pointSelectSettings.customEvent.onCheckChange = (check: boolean, val: any)=>{
                            if(val.originalItem.Id === -1){
                                if(check){
                                    $scope.pointSelectSettings.setting.jqxDropDownList('checkAll');
                                }else{
                                    $scope.pointSelectSettings.setting.jqxDropDownList('uncheckAll');
                                }
                            }
                        };
                        $scope.modelSelectSettings_multi.customEvent.onCheckChange = (check:boolean, val:any)=>{
                            if(val.originalItem.Serial === '-1'){
                                if(check){
                                    $scope.modelSelectSettings_multi.setting.jqxDropDownList('checkAll');
                                }else{
                                    $scope.modelSelectSettings_multi.setting.jqxDropDownList('uncheckAll');
                                }
                            }
                        };
                    }
                }
                AdModule.controller('ReportEnterpriseController',ReportEnterpriseController);
            }
        }
    }
}