/**
 * Created by phamn on 14/04/2016.
 */
interface JQuery {
    daterangepicker(a:any): JQuery;
}
module App{
    export module Components{
        export module Enterprisemanage{
            export module ReportEnterprise{
                export class ReportEnterpriseRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper,global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                           name: 'reportenterprise',
                           config: {
                               controller: Enterprisemanage.ReportEnterprise.ReportEnterpriseController,
                               templateUrl: '/components/enterprisemanage/reportenterprise/reportenterprise.html'+`?v=${global.Version}`,
                               url: '/bao-cao-doanh-nghiep',
                               tag:{
                                   title:'Báo cáo Doanh Nghiệp',
                                   icon:'icon-office',
                                   groupId:'report',
                                   index:0
                               },
                               show:false
                           }

                        });
                        // Báo cáo tổng hợp
                        // sysRouter.configureStates({
                        //     name: 'reportenterprise.generalreport',
                        //     config: {
                        //         controller: Enterprisemanage.ReportEnterprise.Generalreport.GeneralreportController,
                        //         templateUrl: '/components/enterprisemanage/reportenterprise/generalreport/generalreport.html'+`?v=${global.Version}`,
                        //         url: '/bao-cao-tong-hop',
                        //         parent:'reportenterprise',
                        //         tag:{
                        //             title:'Báo cáo tổng hợp',
                        //             icon:'icon-magazine',
                        //             groupId:'report',
                        //             index:1
                        //         }
                        //     }
                        // });

                        // sysRouter.configureStates({
                        //     name: 'reportenterprise.journeyreports',
                        //     config: {
                        //         controller: Enterprisemanage.ReportEnterprise.Journeyreports.JourneyreportsController,
                        //         templateUrl: '/components/enterprisemanage/reportenterprise/journeyreports/journeyreports.html'+`?v=${global.Version}`,
                        //         url: '/bao-cao-hanh-trinh',
                        //         parent:'reportenterprise',
                        //         tag:{
                        //             title:'Báo cáo hành trình',
                        //             icon:'icon-merge',
                        //             groupId:'report',
                        //             index:0
                        //         }
                        //     }
                        // });
                        // sysRouter.configureStates({
                        //     name: 'reportenterprise.camerafollow',
                        //     config: {
                        //         controller: Enterprisemanage.ReportEnterprise.Camerafollow.CamerafollowController,
                        //         templateUrl: '/components/enterprisemanage/reportenterprise/camerafollow/camerafollow.html'+`?v=${global.Version}`,
                        //         url: '/theo-doi-camera',
                        //         parent:'reportenterprise',
                        //         tag:{
                        //             title:'Theo dõi camera',
                        //             icon:'fa fa-camera',
                        //             groupId:'report',
                        //             index:17
                        //         }
                        //     }
                        // });
                        // sysRouter.configureStates({
                        //     name: 'reportenterprise.motorjourney',
                        //     config: {
                        //         controller: Enterprisemanage.ReportEnterprise.Carjourney.CarjourneyController,
                        //         templateUrl: '/components/enterprisemanage/reportenterprise/motorjourney/motorjourney.html',
                        //         url: '/hanh-trinh-xe',
                        //         parent:'reportenterprise',
                        //         tag:{
                        //             title:'Hành Trình xe',
                        //             icon:'glyphicon glyphicon-transfer'
                        //         }
                        //     }
                        // });




                        // Tổng hợp tiêu thụ nhiên liệu

                        // sysRouter.configureStates({
                        //     name: 'reportenterprise.lostdatareport',
                        //     config: {
                        //         controller: Enterprisemanage.ReportEnterprise.Lostdatareport.LostdatareportController,
                        //         templateUrl: '/components/enterprisemanage/reportenterprise/lostdatareport/lostdatareport.html'+`?v=${global.Version}`,
                        //         url: '/bao-cao-mat-du-lieu',
                        //         parent:'reportenterprise',
                        //         tag:{
                        //             title:'Báo cáo mất dữ liệu',
                        //             icon:'icon-connection',
                        //             index:11
                        //         }
                        //     }
                        // });

                        // sysRouter.configureStates({
                        //     name: 'reportenterprise.parking',
                        //     config: {
                        //         controller: Enterprisemanage.ReportEnterprise.ParkingEnterprise.ParkingEnterpriseController,
                        //         templateUrl: '/components/enterprisemanage/reportenterprise/parking/parking.html'+`?v=${global.Version}`,
                        //         url: '/bao-cao-dung-do',
                        //         parent:'reportenterprise',
                        //         tag:{
                        //             title:'Báo cáo dừng đỗ',
                        //             icon:'icon-file-text',
                        //             index:15
                        //         }
                        //     }
                        // });
                        // sysRouter.configureStates({
                        //     name: 'reportenterprise.tcdbreport',
                        //     config: {
                        //         controller: Enterprisemanage.ReportEnterprise.Tcdbreport.TcdbreportController,
                        //         templateUrl: '/components/enterprisemanage/reportenterprise/tcdbreport/tcdbreport.html'+`?v=${global.Version}`,
                        //         url: '/bao-cao-truyen-du-lieu-duong-bo',
                        //         parent:'reportenterprise',
                        //         tag:{
                        //             title:'Báo cáo truyền TCĐB',
                        //             icon:'icon-satellite-dish2',
                        //             index:19
                        //         }
                        //     }
                        // });
                    }
                }
                AdModule.run(ReportEnterpriseRouteModel);
            }
        }
    }
}