/**
 * Created by phamn on 15/04/2016.
 */

module App{
    export module Components{
        export module Enterprisemanage{
            export module ReportEnterprise{
                export module Generalreport {
                    export interface IGeneralreportScope extends ng.IScope {
                        gridGeneralReport: App.Shared.Directives.GridviewControl;
                        $parent: IReportEnterpriseScope;
                        checkUseKmCo:boolean;
                        
                    }
                    export class GeneralreportController{
                        public static $inject = ['$scope','$http','global','language','api'];

                        constructor ($scope: IGeneralreportScope,
                                     public $http: ng.IHttpService,
                                     public global: App.Shared.GlobalService,
                                     public language:App.Shared.LanguageService,
                                     api:App.Shared.Api.IApi){

                            // $scope.$parent.checkbox_drop = true;
                            // $scope.$parent.showdevices = true;
                            // $scope.$parent.time31 = true;


                            $scope.$parent.checkbox_drop = false; // bật tính năng chọn xe từng chiếc
                            $scope.$parent.checkbox_nhienlieu = false; // hiển thị chọn xe theo nhiên liệu
                            $scope.$parent.checkbox_dropcar = true; // chọn xe theo check box
                            $scope.$parent.showdevices = true; // chọn hiển thị cả ô xe
                            $scope.$parent.time31 = true; // không hiển thị lịch chọn 2 ngày


                            $scope.gridGeneralReport = new App.Shared.Directives.GridviewControl("gridGeneralReport");
                            $scope.gridGeneralReport.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridGeneralReport.filename_excel="Báo cáo tổng hợp";
                            $scope.gridGeneralReport.columns = [];
                            $scope.gridGeneralReport.height = window.innerHeight - 170;
                            $scope.gridGeneralReport.columnsHeight = 60;
                            //bật tính năng lọc dữ liệu
                            $scope.gridGeneralReport.isAltRows=true;
                            $scope.gridGeneralReport.isFilter=true;
                            $scope.gridGeneralReport.isFilterMode= 'simple';
                            $scope.gridGeneralReport.isExport= true;
                            $scope.gridGeneralReport.columns.push({ text: 'STT', dataField: 'STT', type: 'string', align: 'center',width:'35px',pinned:true });
                            $scope.gridGeneralReport.columns.push({ text: 'Ngày tháng', dataField: 'DateTime', type: 'string', align: 'center',width:'88px',filterable:true, });
                            $scope.gridGeneralReport.columns.push({ text: 'Biển số', dataField: 'Bs', type: 'string', align: 'center',width:'80px',filterable:true });
                            $scope.gridGeneralReport.columns.push({ text: 'Loại hình <br/> kinh doanh', dataField: 'Type_mess', type: 'string', align: 'center',width:'70px',filterable:true,className:'gridbaocao' });

                            $scope.gridGeneralReport.columns.push({ text: 'Thời gian <br/> lăn bánh', dataField: 'RunTimeSpan', type: 'string', align: 'center' ,width:'70px',filterable:true,className:'gridbaocao' });
                            // $scope.gridGeneralReport.columns.push({ text: 'Thời gian dừng', dataField: 'PauseTimeSpan', type: 'string', align: 'center' ,width:'120px' });
                            $scope.gridGeneralReport.columns.push({ text: 'Km<br/>(gps)', dataField: 'Distance', type: 'number', align: 'center' ,width:'50px',filterable:true,className:'gridbaocao' });
                            $scope.gridGeneralReport.columns.push({ text: 'Km Cơ', dataField: 'DistanceCo', type: 'number', align: 'center' ,width:'50px',filterable:true,hidden:true,className:'gridbaocao' });
                            $scope.gridGeneralReport.columns.push({ text: 'SL dừng đỗ', dataField: 'PauseSum', type: 'string', align: 'center',width:'50px',filterable:true,className:'gridbaocao'  });
                            $scope.gridGeneralReport.columns.push({ text: 'SL quá tốc độ', dataField: 'OverSpeedSum', type: 'number', align: 'center',width:'50px',filterable:true,className:'gridbaocao' });
                            $scope.gridGeneralReport.columns.push({ text: 'Sl mở cửa', dataField: 'OpenDoorSum', type: 'number', align: 'center',width:'50px',filterable:true,className:'gridbaocao' });
                            $scope.gridGeneralReport.columns.push({ text: 'Sl mở máy lạnh', dataField: 'MayLanhCount', type: 'number', align: 'center',width:'70px',filterable:true,className:'gridbaocao' });
                            $scope.gridGeneralReport.columns.push({ text: 'Thời gian mở máy lạnh', dataField: 'TotalMoMayLanh', type: 'number', align: 'center',width:'80px',filterable:true,className:'gridbaocao' });
                            $scope.gridGeneralReport.columns.push({ text: 'SL quá thời gian <br/> liên tục(4h)', dataField: 'Run4hSum', type: 'number', align: 'center', width:'80px',filterable:true,className:'gridbaocao'});
                            $scope.gridGeneralReport.columns.push({ text: 'SL quá thời gian <br/> trong ngày', dataField: 'Run10hSum', type: 'number', align: 'center',width:'70px',filterable:true,className:'gridbaocao' });
                            $scope.gridGeneralReport.columns.push({ text: 'Nhiên liệu <br/>tiêu thụ định mức(lít)', dataField: 'FuelUse', type: 'number', align: 'center',width:'auto',filterable:true,className:'gridbaocao' });

                            // bật chế độ multiselect cho thiết bị
                            $scope.$parent.btViewClick = () => {
                                var starttime_xphut  = `${$scope.$parent.dtTimeStartSelect.GetTimeClockDefault()} ${$scope.$parent.dtTimeStartSelect.GetPickerDate_upserver()}`;
                                var endtime = `${$scope.$parent.dtTimeEndSelect.GetTimeClockDefault()} ${$scope.$parent.dtTimeEndSelect.GetPickerDate_upserver()}`;
                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined) {
                                    $http.get(global.ReportHost + `api/Company/GetSettingById/${$scope.$parent.cpnSelectSettings.itemSelect().Id}`).then((rep:any)=>{
                                        console.log(rep);
                                       $scope.checkUseKmCo = rep.data.Setting.SudungKmCo;
                                    });
                                    if($scope.$parent.modelSelectSettings_multi.valueSelects().length !== 0 && $scope.$parent.modelSelectSettings_multi.valueSelects() !== undefined){
                                        if($scope.$parent.modelSelectSettings_multi.valueSelects()[0] == '-1'){
                                            language.showLoading(false,"");
                                            api.GetGeneralCompanyReportBySerial($scope.$parent.modelSelectSettings_multi.allValue(),
                                                $scope.$parent.cpnSelectSettings.itemSelect().Id,starttime_xphut,endtime).then((data:Array<any>)=>{
                                                language.showLoading(true,"");
                                                console.log(data);
                                                $scope.gridGeneralReport.data = data;
                                                AdapterData($scope.gridGeneralReport.data);
                                                $scope.gridGeneralReport.refresh();
                                                if($scope.checkUseKmCo){
                                                    $scope.gridGeneralReport.showcolumn('DistanceCo');
                                                }

                                            });
                                            // language.showLoading(false,"");
                                            // api.GetGeneralCompanyReportBySerial([$scope.$parent.modelSelectSettings_multi.itemSelect().Serial],
                                            //     $scope.$parent.cpnSelectSettings.itemSelect().Id,$scope.$parent.dtTimeSelect.begin(),$scope.$parent.dtTimeSelect.end()).then((data:Array<any>)=>{
                                            //     language.showLoading(true,"");
                                            //     console.log(data);
                                            //     $scope.gridGeneralReport.data = data;
                                            //     AdapterData($scope.gridGeneralReport.data);
                                            //     $scope.gridGeneralReport.refresh();
                                            // });
                                        }else {
                                            language.showLoading(false,"");
                                            api.GetGeneralCompanyReportBySerial($scope.$parent.modelSelectSettings_multi.valueSelects(),
                                                $scope.$parent.cpnSelectSettings.itemSelect().Id,starttime_xphut,endtime).then((data:Array<any>)=>{
                                                language.showLoading(true,"");
                                                console.log(data);
                                                $scope.gridGeneralReport.data = data;
                                                AdapterData($scope.gridGeneralReport.data);
                                                $scope.gridGeneralReport.refresh();
                                                if($scope.checkUseKmCo){
                                                    $scope.gridGeneralReport.showcolumn('DistanceCo');
                                                }
                                            });
                                        }

                                    }else{
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Vui lòng chọn xe !!",
                                            buttons: {
                                                cancle: {
                                                    label: "Quay lại",
                                                    className: "btn-primary",
                                                }
                                            }
                                        });
                                    }
                                }else{
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Vui lòng chọn công ty !!",
                                        buttons: {
                                            cancle: {
                                                label: "Quay lại",
                                                className: "btn-primary",
                                            }
                                        }
                                    });
                                }
                            };

                            function AdapterData (value:any){
                                $.each(value,(index:number, val:any)=> {
                                    var SplitTime = val.DateTime.split('T');
                                    // value.DateTime = SplitTime[0];
                                    //chuyển thời gian
                                    var year = SplitTime[0].slice(0,4);
                                    var month = SplitTime[0].slice(5,7);
                                    var day = SplitTime[0].slice(8);
                                    val.DateTime = day + '-' + month + '-' + year;
                                    val.Type_mess = global.AdpterChangeLoaiHinhKinhDoanh(val.ActivityType);
                                    val.STT = index+1;
                                });

                            }
                        }
                    }
                    AdModule.controller('GeneralreportController',GeneralreportController);
                }
            }
        }
    }
}