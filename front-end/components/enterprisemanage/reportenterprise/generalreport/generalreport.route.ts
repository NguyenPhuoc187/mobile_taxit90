module App{
    export module Components{
        export module Enterprisemanage{
            export module ReportEnterprise{
                export class GeneralreportRouteModel{
                    public static $inject = ['sysRouter','global'];
                    constructor(sysRouter: Shared.IRouterHelper,global: App.Shared.GlobalService){
                        sysRouter.configureStates({
                            name: 'reportenterprise.generalreport',
                            config: {
                                controller: Enterprisemanage.ReportEnterprise.Generalreport.GeneralreportController,
                                templateUrl: '/components/enterprisemanage/reportenterprise/generalreport/generalreport.html'+`?v=${global.Version}`,
                                url: '/bao-cao-tong-hop',
                                tag:{
                                    title:'Báo cáo tổng hợp',
                                    icon:'icon-magazine',
                                    groupId:'report',
                                    index:1
                                }
                            }
                        });
                    }
                }
                AdModule.run(GeneralreportRouteModel);
            }
        }
    }
}