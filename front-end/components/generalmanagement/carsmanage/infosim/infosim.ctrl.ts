/**
 * Created by phamn on 14/04/2016.
 */
    ///<reference path="../../../../../tstyping/angularjs/angular.d.ts"/>
module App{
    export module Components{
        export module Generalmanagement{
            export module Carsmanage{
                export module Infosim{
                    export interface IInfosimScope extends ng.IScope{
                        gridInfosim: App.Shared.Directives.GridviewControl;
                        $parent: ICarsmanageScope;
                        timerloadata: any;
                    }
                    export class InfosimController{
                        public static $inject = ['$scope', 'global', '$http','$rootScope','language','api'];

                        constructor($scope: IInfosimScope, public global: App.Shared.GlobalService, public $http: ng.IHttpService,public $rootScope: App.Components.Generalmanagement.Carsmanage.ICarsmanageScope,
                                    public language:App.Shared.LanguageService, public api:App.Shared.Api.IApi){
                            $rootScope.hideComboboxGroupCar = false;
                            language.showLoading(true,"");
                            $scope.gridInfosim = new App.Shared.Directives.GridviewControl("gridInfosim");
                            $scope.gridInfosim.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridInfosim.filename_excel="Báo cáo thông tin sim";
                            $scope.gridInfosim.columns = [];
                            $scope.gridInfosim.height = window.innerHeight - 200;
                            $scope.gridInfosim.columnsHeight = 60;
                            //bật tính năng lọc dữ liệu
                            $scope.gridInfosim.isAltRows=true;
                            $scope.gridInfosim.isFilter=true;
                            $scope.gridInfosim.isFilterMode= 'advanced';
                            $scope.gridInfosim.columns.push({ text: 'STT', dataField: 'Stt', type: 'string', align: 'center', width:'50px' });
                            //$scope.gridInfosim.columns.push({ text: 'Id', dataField: 'ID', type: 'string', align: 'center', width:'50px' });
                            //$scope.gridInfosim.columns.push({ text: 'Serial', dataField: 'Serial', type: 'string', align: 'center', width:'50px' });
                            $scope.gridInfosim.columns.push({ text: 'Biển số xe', dataField: 'Bs', type: 'string', align: 'center',width:'150px',filterable:true });
                            $scope.gridInfosim.columns.push({ text: 'Số điện thoại', dataField: 'Phone_mess', type: 'string', align: 'center', width:'150px',filterable:true });
                            $scope.gridInfosim.columns.push({ text: 'Thời gian cập nhật <br/>số điện thoại', dataField: 'PhoneUpdate', type: 'string', align: 'center', width:'150px',className:'gridbaocao'});
                            $scope.gridInfosim.columns.push({ text: 'Số tiền', dataField: 'Money', type: 'string', align: 'center', width: '150px' });
                            $scope.gridInfosim.columns.push({ text: 'Thời gian cập nhật <br/> số tiền', dataField: 'MoneyUpdate', type: 'string', align: 'center', width: 'auto',className:'gridbaocao' });
                            
                            // Bật tắt layout thời gian trên trang master, true: là tắt,
                            $scope.$parent.range_thoigian = true;
                            /*Hàm chuyển dữ liệu chọn xe thành mảng serial để post lên server*/
                            // function AdapterSerial (a:any){
                            //     var a: any = $scope.$parent.dropDownListInstance.getCheckedItems();
                            //     var result: any = [];
                            //     for(var i =0; i< a.length; i++){
                            //         result.push(a[i].originalItem.Serial);
                            //     }
                            //     return result;
                            // }

                            /*Loading data mặc định khi kick vào menu*/
                            $scope.timerloadata = setInterval(()=>{
                                    if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined){
                                        // $scope.$parent.modelSelectSettings.setting.jqxComboBox('clearSection');
                                        language.showLoading(false,"");
                                        api.GetSimInfoByCompany($scope.$parent.cpnSelectSettings.itemSelect().Id).then((data:any)=>{
                                            language.showLoading(true,"");
                                            //todo: chưa có dữ liệu để test, nếu có sẽ chỉnh sửa lại grid-view sau.
                                            //todo: đã thực hiện lấy dữ liệu ok
                                            //$scope.$parent.modelSelectSettings.setting.jqxComboBox('unselectItem', item ); ');
                                            $scope.gridInfosim.data = data;
                                            AdapterData($scope.gridInfosim.data);
                                            $scope.gridInfosim.refresh();
                                        });

                                    }
                                clearInterval($scope.timerloadata);
                            },300);
                            /*Khi nhấn nút view*/
                            $scope.$parent.btViewClick = () => {
                                // kiểm tra xem có chọn công ty chưa
                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined) {
                                    //console.log($scope.$parent.dropDownListInstance.getCheckedItems());
                                        /*Lấy log theo công ty và số serial */
                                        if($scope.$parent.groupSelectSettings.itemSelect().Id !== -1){
                                            if($scope.$parent.modelSelectSettings.valueSelects().length !== 0 && $scope.$parent.modelSelectSettings.valueSelects() !== undefined){
                                                language.showLoading(false,"");
                                                api.PostSimInfoBySerial($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.modelSelectSettings.valueSelects()).then((data:any)=>{
                                                    language.showLoading(true,"");
                                                    $scope.gridInfosim.data = data;
                                                    AdapterData($scope.gridInfosim.data);
                                                    $scope.gridInfosim.refresh();
                                                });
                                            }else{
                                                language.showLoading(false,"");

                                                api.GetSimInfoByGroupId($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.groupSelectSettings.itemSelect().Id).then((data:any)=>{
                                                    language.showLoading(true,"");
                                                    $scope.gridInfosim.data = data;
                                                    AdapterData($scope.gridInfosim.data);
                                                    $scope.gridInfosim.refresh();
                                                });
                                            }
                                        }else{
                                            if($scope.$parent.modelSelectSettings.valueSelects().length !== 0 && $scope.$parent.modelSelectSettings.valueSelects() !== undefined){
                                                language.showLoading(false,"");
                                                api.PostSimInfoBySerial($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.modelSelectSettings.valueSelects()).then((data:any)=>{
                                                    language.showLoading(true,"");
                                                    $scope.gridInfosim.data = data;
                                                    AdapterData($scope.gridInfosim.data);
                                                    $scope.gridInfosim.refresh();
                                                });
                                            }else{



                                                language.showLoading(false,"");
                                                api.GetSimInfoByCompany($scope.$parent.cpnSelectSettings.itemSelect().Id).then((data:any)=>{
                                                    language.showLoading(true,"");
                                                    //todo: chưa có dữ liệu để test, nếu có sẽ chỉnh sửa lại grid-view sau.
                                                    //todo: đã thực hiện lấy dữ liệu ok
                                                    $scope.gridInfosim.data = data;
                                                    AdapterData($scope.gridInfosim.data);
                                                    $scope.gridInfosim.refresh();
                                                });
                                            }
                                        }

                                } else {
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Không có dữ liệu hiển thị. Vui lòng chọn công ty !!",
                                        buttons: {
                                            cancle: {
                                                label: "Quay lại",
                                                className: "btn-primary",
                                            }
                                        }
                                    });
                                }
                            };



                            function AdapterData (tmp:any){
                                $.each(tmp, (index:number,value:any)=>{
                                    value.Stt=index + 1;
                                    value.PhoneUpdate = global.AdapterDate(value.PhoneUpdate);
                                    value.MoneyUpdate = global.AdapterDate(value.MoneyUpdate);
                                    value.Phone_mess = global.ApdaterChangePhone(value.Phone);

                                });

                            }

                           
                        }
                    }
                    AdModule.controller('InfosimController', InfosimController);
                }
            }
        }

    }
}