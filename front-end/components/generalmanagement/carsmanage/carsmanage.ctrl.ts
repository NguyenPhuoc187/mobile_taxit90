
module App {
    export module Components {
        export module Generalmanagement{
            export module Carsmanage {
                export interface ICarsmanageScope extends ng.IScope {
                    /*Khai báo thuộc tính drop-down*/
                    cpnSelectSettings :App.Shared.Gui.IDropDown<App.Shared.Entity.ICompany>;
                    groupSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IGroup>;
                    modelSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IDevice>;
                    btViewClick:() => any;
                    range_thoigian:boolean;
                    dtTimeSelect:App.Shared.Gui.IDateTimeControl;
                    /*Khai báo thuộc tính của langugae*/
                    viewData:string;
                    hideComboboxGroupCar:any;

                }

                export class CarsmanageController {
                    public static $inject = ['$scope','language', '$http', 'global','api','$q'];

                    constructor($scope:ICarsmanageScope, public language:App.Shared.LanguageService, public $http: ng.IHttpService, public global: App.Shared.GlobalService,
                                public api:App.Shared.Api.IApi,public $q: ng.IQService) {

                        $scope.viewData=language.gridLg().viewData;

                        // Khai báo mới combobox truyền vào 3 thông số : tên định dạng của combobox, hiển thị tên trường, giá trị chọn
                        $scope.cpnSelectSettings =new App.Shared.Gui.DropDown<App.Shared.Entity.ICompany>('cbCompanyCarManageMaster','Display','Id');
                        // Tiêu đề trên combobox
                        $scope.cpnSelectSettings.title= language.gridLg().company;
                        // Set chiều dài cho combobox
                        $scope.cpnSelectSettings.setting.width = '100%';
                        $scope.cpnSelectSettings.setting.checkboxes = false;
                        $scope.cpnSelectSettings.setting.filterPlaceHolder = 'Tìm công ty...';

                        // Khai báo combobox đội xe
                        $scope.groupSelectSettings=new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('cbGroupCarManageMaster','Name','Id');
                        $scope.groupSelectSettings.title=language.gridLg().group;
                        $scope.groupSelectSettings.setting.width = '100%';
                        $scope.groupSelectSettings.setting.checkboxes = false;
                        $scope.groupSelectSettings.setting.filterPlaceHolder = 'Tìm đội xe...';

                        // Khai báo combobox xe
                        $scope.modelSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IDevice>('cbDeviceCarManageMaster','Bs','Serial');
                        $scope.modelSelectSettings.title=language.gridLg().car;
                        $scope.modelSelectSettings.setting.width = '100%';
                        $scope.modelSelectSettings.setting.placeHolder = 'Chọn xe';


                        //Khai báo lịch
                        $scope.dtTimeSelect=new App.Shared.Gui.DateTimeControl('dtTimeSelectCarManageMaster');
                        $scope.dtTimeSelect.titleTime=language.gridLg().titleTime;


                        // load công ty 2 thông số truyền vào : dữ liệu lấy về, vị trí truyền vào ban đầu
                        // $scope.cpnSelectSettings.load(api.GetAllCompany(),null,true);
                        $scope.cpnSelectSettings.loadPromise(api.GetAllCompany(), null,$q).then(()=>{
                            // console.log(localStorage.getItem('companyIdmapxekhach'));
                            if(localStorage.getItem('companyIdquantri') !== '' && localStorage.getItem('companyIdquantri') !== null){
                                console.log(localStorage.getItem('companyIdquantri'));
                                let companyid:any = localStorage.getItem('companyIdquantri');
                                $scope.cpnSelectSettings.setItemDefault((m:any)=>m.Id === companyid*1);
                            }else{
                                $scope.cpnSelectSettings.setting.jqxDropDownList('selectIndex',0);
                            }

                        });
                        //Sự kiện chọn công ty
                        $scope.cpnSelectSettings.customEvent.onSelectOnItem=(index:number,val:App.Shared.Entity.ICompany)=>{
                            localStorage.setItem('companyIdquantri',`${val.Id}`,);
                            var first=Array<App.Shared.Entity.IGroup>();
                            // đẩy 1 group làm thông tin chọn tất cả
                            first.push( {
                                Id: -1,
                                Name: 'Tất cả',
                                CompanyId:val.Id
                            });

                            $scope.groupSelectSettings.load(api.GetAllGroupByCompanyId(val.Id),first, true);

                        };

                        // cài đặt sự kiện chọn đội xe
                        $scope.groupSelectSettings.customEvent.onSelectOnItem=(index:number,val:App.Shared.Entity.IGroup)=>{
                            var firstdevice=Array<App.Shared.Entity.IDevice>();
                            // đẩy 1 group làm thông tin chọn tất cả
                            firstdevice.push( {
                                Serial: '-1',
                                Bs: 'Tất cả',

                            });
                            if(val.Id == -1){// hiển thị tất cả các xe trong công ty
                                $scope.modelSelectSettings.load(api.GetAllDeviceByCompanyId(val.CompanyId),firstdevice);

                            }
                            else{// hiểm thị các xe trong đội xe đã chọn
                                $scope.modelSelectSettings.load(api.GetAllDeviceByGroupId(val.CompanyId,val.Id),firstdevice);

                            }
                        };
                        $scope.modelSelectSettings.customEvent.onCheckChange = (check:boolean, val:any)=>{
                            if(val.originalItem.Serial === '-1'){
                                if(check){
                                    $scope.modelSelectSettings.setting.jqxDropDownList('checkAll');
                                }else{
                                    $scope.modelSelectSettings.setting.jqxDropDownList('uncheckAll');
                                }
                            }
                        };

                    }
                }

                AdModule.controller('CarsmanageController', CarsmanageController);
            }
        }

    }
}