module App {
    export module Components {
        export module Generalmanagement{
            export module Drivermanage {
                export class ListcarsRouterModel {
                    public static $inject=['sysRouter','global'];
                    constructor(sysRouter:Shared.IRouterHelper,global: App.Shared.GlobalService) {

                        sysRouter.configureStates({
                            name: 'carsmanage.listcars',
                            config: {
                                controller: Generalmanagement.Carsmanage.Listcars.ListcarsController,
                                templateUrl: '/components/generalmanagement/carsmanage/listcars/listcars.html'+`?v=${global.Version}`,
                                url: '/Danh-sach-xe',
                                //abstract:true,// trang master
                                tag:{
                                    title:'Danh sách xe',
                                    icon:'icon-car2',
                                    groupId:'general',
                                    index:0
                                }
                            }
                        });

                    }
                }
                AdModule.run(ListcarsRouterModel);
            }
        }

    }
}