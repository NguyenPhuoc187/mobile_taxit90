/**
 * Created by NPPRO on 20/02/2017.
 */

    ///<reference path="../../../../../tstyping/angularjs/angular.d.ts"/>
module App{
    export module Components{
        export module Generalmanagement{
            export module Carsmanage{
                export module Listcars{

                    export interface ISysListCar{
                        Type?: string; // loại hình kinh doanh
                        ModelName?: string; // loại xe
                        Bs?: string; // biển số
                        VinSerial?: string; // số vin
                        GroupName?: string; // đội xe
                        PhoneOfDevice ?: string; // số đt
                        Seat?: number; // số ghế
                        Sgtvt?: string; // sở gtvt
                        EndTime?:string;
                        Note?: string;
                        Serial?: string;
                        Id ?: string;
                        CompanyId?: number;
                        GroupId?: number;
                        CreateTime?:string;
                        MoneyTime?: string;
                        LimitSpeed?: number;
                        BgtTranport?:boolean;
                        Stt?: number;
                        FuelPer100Km: number;
                        OwnerPhone:string;
                    }
                    export interface IListcarsScope extends ng.IScope{
                        gridListCars: App.Shared.Directives.GridviewControl;
                        /*Khai báo thuộc tính của langugae*/
                        titleCompany:string;
                        titleGroup: string;
                        titleCar:string;
                        titleTime:string;
                        titlePhone:string;
                        titleIdCar:string;
                        titleBienSo:string;
                        titleVin:string;
                        titleSerial:string;
                        titleNumberSeat:string;
                        titleNote:string;
                        titleAdd:string;
                        viewData:string;


                        $parent: ICarsmanageScope;

                        editFormAccept:() => any;
                        funEdit: ISysListCar;

                        modelnameSelectSettings: App.Shared.Gui.ICombobox<App.Shared.Entity.IModelCar>; // thiết lập của loại xe
                        // typeSelectSettings: App.Shared.Gui.ICombobox<any>; // thiết lập của loại hình kinh doanh

                        sogtvtSelectSettings: App.Shared.Gui.ICombobox<any>; // thiết lập SGTVT

                        grouplistcarSelectSettings: App.Shared.Gui.ICombobox<App.Shared.Entity.IGroup>;

                        companyid_car: number;
                        isOkbinding:boolean;
                        groupcarname : string;
                        groupid_postserver:number;
                        timerloadlistcar: any;

                        checktaxi:boolean;
                        modongho:()=>any;
                        khoadongho:()=>any;

                    }
                    export class ListcarsController{
                        public static $inject = ['$scope','$rootScope','$http','global','language','api','$q'];

                        constructor($scope: IListcarsScope,public $rootScope: App.Components.Generalmanagement.Carsmanage.ICarsmanageScope,
                                    public $http: ng.IHttpService, public global: App.Shared.GlobalService,
                                    public language:App.Shared.LanguageService,
                                    public api:App.Shared.Api.IApi,
                                    $q:ng.IQService){
                            // Bật tắt layout thời gian trên trang master, true: là tắt,
                            language.showLoading(true,"");
                            $scope.$parent.range_thoigian = true;
                            //chuyển đổi ngôn ngữ
                            $scope.titleCompany=language.gridLg().company;
                            $scope.titleGroup=language.gridLg().group;
                            $scope.titleCar=language.gridLg().car;
                            $scope.titleTime=language.gridLg().titleTime;
                            $scope.titlePhone=language.gridLg().titlePhone;
                            $scope.titleIdCar=language.gridLg().titleIdCar;
                            $scope.titleBienSo=language.gridLg().titleBienSo;
                            $scope.titleAdd=language.gridLg().titleAdd;
                            $scope.titleVin=language.gridLg().titleVin;
                            $scope.titleSerial=language.gridLg().titleSerial;
                            $scope.titleNumberSeat=language.gridLg().titleNumberSeat;
                            $scope.titleNote=language.gridLg().titleNote;
                            $scope.viewData=language.gridLg().viewData;

                            $scope.checktaxi =false;

                            $scope.gridListCars = new App.Shared.Directives.GridviewControl("gridListCars");
                            $scope.gridListCars.selectionModeinfo = 'none'; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridListCars.filename_excel="Danh sách xe";
                            $scope.gridListCars.columns = [];
                            $scope.gridListCars.height = window.innerHeight - 200;
                            //bật tính năng lọc dữ liệu
                            $scope.gridListCars.isAltRows=true;
                            $scope.gridListCars.isFilter=true;
                            $scope.gridListCars.isFilterMode= 'advanced';
                            $scope.gridListCars.isExport = true;


                            var cellClass:any = (row:any, dataField:any, cellText:any, rowData:any)=> {
                                var cellValue:any = rowData[dataField];

                                var dateString:any = rowData['EndTime']; // Oct 23
                                // console.log(dateString);

                                // var dateParts:any = dateString.split("/");
                                //định dạng format chuỗi 'dd/mm/yyyy' thành object ngày
                                // var dateObject:any = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                                var cellValueEndtime:any = Math.floor((dateString - Date.now())/86400000);

                                switch (dataField) {
                                    case "EndTime":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "Stt":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "Bs":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "Id":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "Serial":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "VinSerial":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "FuelPer100Km":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "Type_mess":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "Sgtvt":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "GroupName":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "LimitSpeed":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "CreateTime":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "MoneyTime":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "Seat":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                    case "ModelName":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                    case "PhoneOfDevice":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "OwnerPhone":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "IsCamera_show":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "IsNhienLieu_show":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;
                                    case "BgtTranport_show":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {

                                            return "yellow";
                                        }
                                        break;
                                    case "ThoiGianPhatGanNhat_show":
                                        if (cellValueEndtime <  0) {
                                            return "red";
                                        }
                                        if (cellValueEndtime < 31) {
                                            return "yellow";
                                        }
                                        break;

                                }

                            };


                            $scope.gridListCars.columns.push({ text: 'STT', dataField: 'Stt', type: 'number', align: 'center', width:'35px',cellClassName: cellClass, pinned:true  });
                            $scope.gridListCars.columns.push({ text: 'Biển số', dataField: 'Bs', type: 'string', align: 'center',width:'80px',filterable:true,cellClassName: cellClass  });
                            $scope.gridListCars.columns.push({ text: 'Số hiệu xe', dataField: 'Id', type: 'string', align: 'center',width:'54px',filterable:true,cellClassName: cellClass   });
                            // $scope.gridListCars.columns.push({ text: 'Số serial', dataField: 'Serial', type: 'string', align: 'center',width:'83px',filterable:true,cellClassName: cellClass   });
                            // $scope.gridListCars.columns.push({ text: 'Mã VIN', dataField: 'VinSerial', type: 'string', align: 'center',width:'150px',filterable:true,cellClassName: cellClass   });
                            $scope.gridListCars.columns.push({ text: 'Định mức NL (lít/100Km)', dataField: 'FuelPer100Km', type: 'number', align: 'center',width:'73px',filterable:true, cellClassName: cellClass,className:'gridtaxidetail_height40_2row'  });
                            $scope.gridListCars.columns.push({ text: 'Loại Hình KD', dataField: 'Type_mess', type: 'string', align: 'center',width:'100px',filterable:true,cellClassName: cellClass   });
                            $scope.gridListCars.columns.push({ text: 'Sở GTVT', dataField: 'Sgtvt', type: 'string', align: 'center',width:'98px',filterable:true,cellClassName: cellClass   });
                            $scope.gridListCars.columns.push({ text: 'Đội xe', dataField: 'GroupName', type: 'string', align: 'center',width:'100px',filterable:true ,cellClassName: cellClass  });
                            $scope.gridListCars.columns.push({ text: 'SĐT Thiết bị', dataField: 'PhoneOfDevice', type: 'string', align: 'center',width:'98px',filterable:true,cellClassName: cellClass   });
                            $scope.gridListCars.columns.push({ text: 'SĐT Khách hàng', dataField: 'OwnerPhone', type: 'string', align: 'center',width:'98px',filterable:true,cellClassName: cellClass   });
                            $scope.gridListCars.columns.push({ text: 'Loại xe', dataField: 'ModelName', type: 'string', align: 'center',width:'92px',cellClassName: cellClass  });
                            $scope.gridListCars.columns.push({ text: 'Tải trọng/Số ghế', dataField: 'Seat', type: 'string', align: 'center',width:'59px',cellClassName: cellClass,className:'gridtaxidetail_height40_2row'  });
                            //$scope.gridListCars.columns.push({ text: 'Định mức nhiên liệu', dataField: 'nhienlieu', type: 'string', align: 'center',width:'150px' });
                            $scope.gridListCars.columns.push({ text: 'Tốc độ giới hạn', dataField: 'LimitSpeed', type: 'string', align: 'center',width:'56px',filterable:true,cellClassName: cellClass   });
                            $scope.gridListCars.columns.push({ text: 'Thời gian cập nhật gần nhất', dataField: 'ThoiGianPhatGanNhat_show', type: 'date', align: 'center',width:'90px',cellClassName: cellClass,className:'gridtaxidetail_height40_2row',cellsFormat: 'HH:mm dd/MM/yyyy'});
                            $scope.gridListCars.columns.push({ text: 'Ngày tạo', dataField: 'CreateTime', type: 'date', align: 'center',width:'90px',cellClassName: cellClass,
                                cellsFormat: 'dd/MM/yyyy'
                            });
                            $scope.gridListCars.columns.push({ text: 'Ngày đóng tiền', dataField: 'MoneyTime', type: 'date', align: 'center' ,width:'100px',cellClassName: cellClass
                                ,cellsFormat: 'dd/MM/yyyy'
                            });
                            $scope.gridListCars.columns.push({ text: 'Ngày hết phí', dataField: 'EndTime', type: 'date', align: 'center',width:'90px',cellClassName: cellClass
                                ,cellsFormat: 'dd/MM/yyyy'
                            });
                            $scope.gridListCars.columns.push({ text: 'Camera', dataField: 'IsCamera_show', type: 'date', align: 'center',width:'61px',cellClassName: cellClass  });
                            $scope.gridListCars.columns.push({ text: 'Cảm biến NL', dataField: 'IsNhienLieu_show', type: 'string', align: 'center',width:'61px',cellClassName: cellClass  });
                            $scope.gridListCars.columns.push({ text: 'Truyền TCĐB', dataField: 'BgtTranport_show', type: 'string', align: 'center',width:'auto',cellClassName: cellClass  });
                            // $scope.gridListCars.columns.push({ text: 'Ngày hết phí', dataField: 'EndTime', type: 'string', align: 'center',width:'90px',cellClassName: cellClass  });
                            $scope.gridListCars.columnsHeight = 60;
                            //Bật tính năng chỉnh sửa
                            $scope.gridListCars.behavior.isEdit = false;
                            $scope.gridListCars.behavior.isNew = false;
                            $scope.gridListCars.behavior.isDelete = false;

                            // $scope.typeSelectSettings = new App.Shared.Gui.Combobox<any>('cbTypeListCar','Text','Type');
                            // $scope.typeSelectSettings.title = '';
                            // $scope.typeSelectSettings.setting.width = '100%';
                            // $scope.typeSelectSettings.setting.placeHolder = 'Chọn loại hình kinh doanh';

                            // Khai báo sở loại xe
                            $scope.modelnameSelectSettings = new App.Shared.Gui.Combobox<App.Shared.Entity.IModelCar>('cbModelCarListCar','Name','Name');
                            $scope.modelnameSelectSettings.title = '';
                            $scope.modelnameSelectSettings.setting.width = '100%';


                            //Khai báo sở GTVT
                            $scope.sogtvtSelectSettings = new App.Shared.Gui.Combobox<any>('cbSoGTVTListCar','Name','Name');
                            $scope.sogtvtSelectSettings.title = '';
                            $scope.sogtvtSelectSettings.setting.placeHolder = 'Chọn Sở GTVT';
                            $scope.sogtvtSelectSettings.setting.width = '100%';

                            //Khai báo đội xe
                            $scope.grouplistcarSelectSettings = new App.Shared.Gui.Combobox<App.Shared.Entity.IGroup>('cbgroupListCar','Name','Id');
                            $scope.grouplistcarSelectSettings.title ='';
                            $scope.grouplistcarSelectSettings.setting.width = '100%';


                            $scope.timerloadlistcar = setInterval(()=>{
                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined){
                                    var first=Array<App.Shared.Entity.IGroup>();
                                    first.push( {
                                        Id: 0,
                                        Name: 'Không chọn đội',
                                        CompanyId:$scope.$parent.cpnSelectSettings.itemSelect().Id
                                    });
                                    // $scope.typeSelectSettings.loadlocaldata(global.SourceLoaiHinhKinhDoanh,$q);
                                    $scope.sogtvtSelectSettings.loadlocaldata(global.SourceSoGTVT,$q);
                                    $scope.grouplistcarSelectSettings.load(api.GetAllGroupByCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id),first);
                                    $scope.modelnameSelectSettings.load(api.GetAllModelCar(),null);
                                    language.showLoading(false,"");// hiện thị vòng tròn loading
                                    api.GetAllDeviceByCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id).then((data:any)=>{
                                        language.showLoading(true,"");
                                        // console.log(rep_ser);
                                        $scope.gridListCars.data = data;
                                        AdapterData($scope.gridListCars.data);
                                        $scope.gridListCars.refresh();
                                    });
                                    clearInterval($scope.timerloadlistcar);
                                }

                                //console.log('loop chỗ nào ');
                            },800);
                            // return;


                            /**
                             * Khai báo biến chứa dữ liệu cập nhật đầu tiên
                             * */
                            $scope.funEdit = {
                                Type: '', // loại hình kinh doanh
                                ModelName: '', // loại xe
                                Bs: '', // biển số
                                VinSerial: '', // số vin
                                GroupName: '', // đội xe
                                PhoneOfDevice : '', // số đt
                                Seat: 0, // số ghế
                                Sgtvt: '', // sở gtvt
                                FuelPer100Km: 0,
                                OwnerPhone:'',
                            };
                            $rootScope.hideComboboxGroupCar = true;
                            /*Khi nhấn nút view*/
                            $scope.$parent.btViewClick = () => {

                                if($scope.$parent.cpnSelectSettings.itemSelect() !== undefined) {
                                    var first=Array<App.Shared.Entity.IGroup>();
                                    first.push( {
                                        Id: 0,
                                        Name: 'Không chọn đội',
                                        CompanyId:$scope.$parent.cpnSelectSettings.itemSelect().Id
                                    });
                                    $scope.companyid_car = $scope.$parent.cpnSelectSettings.itemSelect().Id;
                                    $scope.grouplistcarSelectSettings.load(api.GetAllGroupByCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id),first);
                                    //console.log($scope.$parent.dropDownListInstance.getCheckedItems());
                                    //kiểm tra xem có chọn group ko
                                    //nếu có chọn group
                                    if($scope.$parent.groupSelectSettings.itemSelect() !== undefined){
                                        //nếu không chọn vào tất cả
                                        if($scope.$parent.groupSelectSettings.itemSelect().Id !== -1){
                                            language.showLoading(false,"");// hiện thị vòng tròn loading
                                            api.GetAllDeviceByGroupId($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.groupSelectSettings.itemSelect().Id).then((data:any)=>{
                                                language.showLoading(true,"");
                                                // console.log(rep_ser);
                                                $scope.gridListCars.data = data;
                                                AdapterData($scope.gridListCars.data);
                                                $scope.gridListCars.refresh();
                                                // /**
                                                //  * Đoạn check xem cột đó có hiển thị ko
                                                //  */
                                                // if($scope.$parent.cpnSelectSettings.itemSelect().Id == 11){
                                                //     $scope.gridListCars.showcolumn('EndTime_show');
                                                //     // $scope.gridListCars.refresh();
                                                // }
                                            });

                                        } else {
                                            language.showLoading(false,"");// hiện thị vòng tròn loading
                                            api.GetAllDeviceByCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id).then((data:any)=>{
                                                language.showLoading(true,"");
                                                // console.log(rep_ser);
                                                $scope.gridListCars.data = data;
                                                AdapterData($scope.gridListCars.data);
                                                $scope.gridListCars.refresh();

                                            });
                                        }


                                    } else {
                                        language.showLoading(false,"");// hiện thị vòng tròn loading
                                        api.GetAllDeviceByGroupId($scope.$parent.cpnSelectSettings.itemSelect().Id, $scope.$parent.groupSelectSettings.itemSelect().Id).then((data:any)=>{
                                            language.showLoading(true,"");
                                            // console.log(rep_ser);
                                            $scope.gridListCars.data = data;
                                            AdapterData($scope.gridListCars.data);
                                            $scope.gridListCars.refresh();
                                        });

                                    }

                                } else {
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Không có dữ liệu hiển thị. Vui lòng chọn công ty !!",
                                        buttons: {
                                            cancle: {
                                                label: "Quay lại",
                                                className: "btn-primary",
                                            }
                                        }
                                    });
                                }
                            };

                            /*Khi kick vào cập nhật*/
                            $scope.gridListCars.event.onEdit = (ne:any, old:any)=> {
                                // console.log(ne);
                                // Load công ty mặc định dùng hàm loadpromise
                                if($scope.$parent.cpnSelectSettings.itemSelect().CompanyType == 1){
                                    $scope.checktaxi = true;
                                }else{
                                    $scope.checktaxi = false;
                                }

                                $scope.funEdit = {
                                    Type: ne.Type, // loại hình kinh doanh
                                    ModelName: ne.ModelName, // loại xe
                                    Bs: ne.Bs, // biển số
                                    VinSerial: ne.VinSerial, // số vin
                                    PhoneOfDevice : ne.PhoneOfDevice, // số đt
                                    Seat: ne.Seat, // số ghế
                                    Sgtvt: ne.Sgtvt, // sở gtvt
                                    CompanyId: ne.CompanyId,
                                    EndTime: ne.EndTime,
                                    Note: '',
                                    Serial: ne.Serial,
                                    Id : ne.Id,
                                    GroupId:ne.GroupId,
                                    CreateTime:ne.CreateTime,
                                    MoneyTime: ne.MoneyTime,
                                    LimitSpeed: ne.LimitSpeed,
                                    BgtTranport: ne.BgtTranport,
                                    Stt: ne.Stt,
                                    FuelPer100Km: ne.FuelPer100Km,
                                    GroupName:ne.GroupName,
                                    OwnerPhone:ne.OwnerPhone
                                };
                                // $scope.grouplistcarSelectSettings
                                //     .loadPromise(api.GetAllGroupByCompanyId($scope.$parent.cpnSelectSettings.itemSelect().Id),null,$q)
                                //     .then(()=>{
                                //
                                //     });

                                $scope.grouplistcarSelectSettings.setItemDefault(m => m.Id == ne.GroupId);
                                $scope.sogtvtSelectSettings.setItemDefault(n => n.Name == ne.Sgtvt);
                                $scope.modelnameSelectSettings.setItemDefault(m=>m.Name == ne.ModelName);
                                // $scope.typeSelectSettings.setItemDefault(m => m.Type == ne.Type);
                                $("#myModal").modal('show');
                                /*Lấy thông tin đội xe*/
                                $scope.$apply();

                            };

                            /*Khi nhấn nút xác nhận*/
                            $scope.editFormAccept = () => {
                                //kiểm tra người dùng có chọn group không
                                if($scope.grouplistcarSelectSettings.itemSelect() == undefined){
                                    $scope.groupid_postserver = 0;
                                }else {
                                    $scope.groupid_postserver = $scope.grouplistcarSelectSettings.itemSelect().Id;
                                }
                                //kiểm tra dữ liệu chọn loại hình kinh doanh trường hợp không chọn gì cả
                                // if($scope.typeSelectSettings.itemSelect() == undefined){
                                //     $scope.typeSelectSettings.itemSelect().Type = '';
                                // }
                                //kiểm tra dữ liệu chọn loại xe trường hợp không chọn gì cả
                                if($scope.modelnameSelectSettings.itemSelect() == undefined){
                                    $scope.modelnameSelectSettings.itemSelect().Name = '';
                                }
                                // kiểm tra dữ liệu chọn group name
                                // if($scope.groupcarname == undefined){
                                //     $scope.groupcarname = '';
                                // }
                                //kiểm tra dữ liệu chọn đội xe name
                                var req: any = [{
                                    // Type: ($scope.typeSelectSettings.itemSelect()===undefined)?'':$scope.typeSelectSettings.itemSelect().Type, // loại hình kinh doanh
                                    Type: $scope.funEdit.Type, // loại hình kinh doanh
                                    ModelName: ($scope.modelnameSelectSettings.itemSelect()===undefined)?'':$scope.modelnameSelectSettings.itemSelect().Name, // loại xe
                                    // ModelName: $scope.funEdit.ModelName, // loại xe
                                    Bs: $scope.funEdit.Bs, // biển số
                                    VinSerial: $scope.funEdit.VinSerial, // số vin
                                    GroupName: ($scope.grouplistcarSelectSettings.itemSelect()===undefined)?'':$scope.grouplistcarSelectSettings.itemSelect().Name, // đội xe
                                    PhoneOfDevice: $scope.funEdit.PhoneOfDevice, // số đt
                                    Seat: $scope.funEdit.Seat,// số ghế
                                    Sgtvt: ($scope.sogtvtSelectSettings.itemSelect()===undefined)?'':$scope.sogtvtSelectSettings.itemSelect().Name, // sở gtvt
                                    EndTime: $scope.funEdit.EndTime,
                                    Note: $scope.funEdit.Note,
                                    Serial: $scope.funEdit.Serial,
                                    Id: $scope.funEdit.Id,
                                    CompanyId: $scope.funEdit.CompanyId,
                                    GroupId: $scope.groupid_postserver,
                                    CreateTime: $scope.funEdit.CreateTime,
                                    MoneyTime: $scope.funEdit.MoneyTime,
                                    LimitSpeed: $scope.funEdit.LimitSpeed,
                                    BgtTranport: $scope.funEdit.BgtTranport,
                                    Stt: $scope.funEdit.Stt,
                                    FuelPer100Km: $scope.funEdit.FuelPer100Km,
                                    OwnerPhone:$scope.funEdit.OwnerPhone
                                }];
                                //language.showLoading(false,"");
                                api.PutUpdateDeviceList($scope.$parent.cpnSelectSettings.itemSelect().Id, req).then((data:any)=>{
                                    console.log(data);
                                    if(data.length > 0){
                                        for(var i=0;i<$scope.gridListCars.data.length; i++){
                                            if($scope.gridListCars.data[i].Bs == $scope.funEdit.Bs){
                                                $scope.gridListCars.data[i] = data[0];
                                                AdapterData($scope.gridListCars.data);
                                                $scope.gridListCars.refreshupdate();
                                                // $scope.gridListCars.refresh();
                                                break;
                                            }
                                        }
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật thiết bị thành công !!",
                                            buttons: {
                                                cancle: {
                                                    label: "Đồng ý",
                                                    className: "btn-success",
                                                    // callback:()=>{
                                                    //     for(var i=0;i<$scope.gridListCars.data.length; i++){
                                                    //         if($scope.gridListCars.data[i].Bs == $scope.funEdit.Bs){
                                                    //             $scope.gridListCars.data[i] = data[0];
                                                    //             AdapterData($scope.gridListCars.data);
                                                    //             $scope.gridListCars.refreshupdate();
                                                    //             // $scope.gridListCars.refresh();
                                                    //             break;
                                                    //         }
                                                    //     }
                                                    //
                                                    //     // $scope.$parent.btViewClick();
                                                    // }
                                                }
                                            }
                                        });
                                        $("#myModal").modal('hide');
                                    }else{
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật không thành công. Xin thử lại !!",
                                            buttons: {
                                                cancle: {
                                                    label: "Quay lại",
                                                    className: "btn-danger",
                                                }
                                            }
                                        });
                                        $("#myModal").modal('show');
                                    }


                                });
                                $("#myModal").modal('hide');

                            };


                            //Khi nhấn nút mở khóa đồng hồ
                            // $scope.modongho = () => {
                            //     Laptrinh("CO_ON",$scope.funEdit.Serial);
                            // };
                            // $scope.khoadongho = () => {
                            //     Laptrinh("KO_OFF",$scope.funEdit.Serial);
                            // };

                            function AdapterData (tmp:any){
                                $.each(tmp, (index:number,value:any)=>{
                                    value.Stt = index + 1;
                                    // if (value.EndTime == null) {
                                    //     value.EndTime_show = '';
                                    // } else {
                                    //     value.EndTime_show = global.AdapterSignDate(value.EndTime);
                                    // }
                                    // if (value.MoneyTime == null) {
                                    //     value.MoneyTime_show = '';
                                    // } else {
                                    //     value.MoneyTime_show = global.AdapterSignDate(value.MoneyTime);
                                    // }
                                    // if (value.CreateTime == null) {
                                    //     value.CreateTime_show = '';
                                    // } else {
                                    //     value.CreateTime_show = global.AdapterSignDate(value.CreateTime);
                                    // }
                                    if(value.IsCamera){
                                        value.IsCamera_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        value.IsCamera_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(value.IsNhienLieu){
                                        value.IsNhienLieu_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        value.IsNhienLieu_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if(value.BgtTranport){
                                        value.BgtTranport_show = '<span class="icon-checkbox-checked"></span>';

                                    }else{
                                        value.BgtTranport_show = '<span class="icon-checkbox-unchecked"></span>';
                                    }
                                    if (value.ThoiGianPhatGanNhat == null) {
                                        value.ThoiGianPhatGanNhat_show = '';
                                    } else {
                                        // var year:string = value.ThoiGianPhatGanNhat.slice(0,4);
                                        // var month:string = value.ThoiGianPhatGanNhat.slice(5,7);
                                        // var date:any = value.ThoiGianPhatGanNhat.slice(8,10);
                                        // var hour:any = (value.ThoiGianPhatGanNhat.slice(11,13))*1 - 7 ;
                                        // var min:any = value.ThoiGianPhatGanNhat.slice(14);
                                        // console.log(hour);
                                        // if(hour < 0){
                                        //     value.ThoiGianPhatGanNhat_show = `${year}-${month}-${date*1 - 1}T${hour * -1}:${min}`;
                                        // }else{
                                        //     value.ThoiGianPhatGanNhat_show = `${year}-${month}-${date}T${hour}:${min}`;
                                        // }
                                        value.ThoiGianPhatGanNhat_show = value.ThoiGianPhatGanNhat + '+07:00';
                                    }
                                    // value.ThoiGianPhatGanNhat = global.AdapterDate(value.ThoiGianPhatGanNhat);
                                    // hiển thị loại hình kinh doanh ở grid-view
                                    value.Type_mess = global.AdpterChangeLoaiHinhKinhDoanh(value.Type);
                                    value.PhoneOfDevice = global.ApdaterChangePhone(value.PhoneOfDevice);

                                });

                            }



                        }
                    }
                    AdModule.controller('ListcarsController', ListcarsController);
                }
            }
        }

    }
}