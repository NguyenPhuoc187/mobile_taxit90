module App {
    export module Components {
        export module Generalmanagement{
            export module Carsmanage {
                export class CarsmanageRouterModel {
                    public static $inject=['sysRouter','global'];
                    constructor(sysRouter:Shared.IRouterHelper,global: App.Shared.GlobalService) {

                        sysRouter.configureStates({
                            name: 'carsmanage',
                            config: {
                                controller: Generalmanagement.Carsmanage.CarsmanageController,
                                templateUrl: '/components/generalmanagement/carsmanage/carsmanage.html'+`?v=${global.Version}`,
                                url: '/quan-ly-xe',
                                abstract:true,// trang master
                                tag:{
                                    title:'Quản lý xe',
                                    icon:'icon-car2',
                                    groupId:'general',
                                    index:0
                                },
                                show:false
                            }
                        });


                        // sysRouter.configureStates({
                        //     name: 'carsmanage.listcars',
                        //     config: {
                        //         controller: Generalmanagement.Carsmanage.Listcars.ListcarsController,
                        //         templateUrl: '/components/generalmanagement/carsmanage/listcars/listcars.html'+`?v=${global.Version}`,
                        //         //title: 'ABC',
                        //         url: '/danh-sach-xe',
                        //         parent:'carsmanage',
                        //         tag:{
                        //             title:'Danh sách xe',
                        //             icon:'fa fa-list-ul',
                        //             index:0
                        //         }
                        //     }
                        // });

                        sysRouter.configureStates({
                            name: 'carsmanage.schedulecars',
                            config: {
                                controller: Generalmanagement.Carsmanage.Schedulecars.SchedulecarsController,
                                templateUrl: '/components/generalmanagement/carsmanage/schedulecars/schedulecars.html'+`?v=${global.Version}`,
                                //title: 'ABC',
                                url: '/lich-trinh-xe',
                                parent:'carsmanage',
                                tag:{
                                    title:'Lịch trình xe',
                                    icon:'icon-map',
                                    index:2
                                }
                            }
                        });
                        sysRouter.configureStates({
                            name: 'carsmanage.infosim',
                            config: {
                                controller: Generalmanagement.Carsmanage.Infosim.InfosimController,
                                templateUrl: '/components/generalmanagement/carsmanage/infosim/infosim.html'+`?v=${global.Version}`,
                                url: '/thong-tin-sim',
                                parent:'carsmanage',
                                tag:{
                                    title:'Thông tin sim',
                                    icon:'icon-bookmarks',
                                    index:1
                                }
                            }
                        });

                    }
                }
                AdModule.run(CarsmanageRouterModel);
            }
        }

    }
}