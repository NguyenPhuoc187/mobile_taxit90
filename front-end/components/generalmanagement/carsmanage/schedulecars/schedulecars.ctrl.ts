/**
 * Created by phamn on 14/04/2016.
 */
    ///<reference path="../../../../../tstyping/angularjs/angular.d.ts"/>
module App{
    export module Components{
        export module Generalmanagement{
            export module Carsmanage{
                export module Schedulecars{
                    export interface ISchedulecarsScope extends ng.IScope{
                        gridSchedulecars: App.Shared.Directives.GridviewControl;
                        /*Khai báo thuộc tính của langugae*/
                        titleCompany:string;
                        titleGroup: string;
                        titleCar:string;
                        titleTime:string;
                        titlePhone:string;
                        titleIdCar:string;
                        titleBienSo:string;
                        titleContent:string;
                        viewData:string;
                        cbTagSelectSetting: any;
                    }
                    export class SchedulecarsController{
                        public static $inject = ['$scope','language','$rootScope'];

                        constructor($scope: ISchedulecarsScope, public language:App.Shared.LanguageService,public $rootScope: App.Components.Generalmanagement.Carsmanage.ICarsmanageScope ){
                            $rootScope.hideComboboxGroupCar = false;
                            $scope.titleCompany=language.gridLg().company;
                            $scope.titleGroup=language.gridLg().group;
                            $scope.titleCar=language.gridLg().car;
                            $scope.titleTime=language.gridLg().titleTime;
                            $scope.titlePhone=language.gridLg().titlePhone;
                            $scope.titleIdCar=language.gridLg().titleIdCar;
                            $scope.titleBienSo=language.gridLg().titleBienSo;
                            $scope.titleContent=language.gridLg().titleContent;
                            $scope.viewData=language.gridLg().viewData;
                            $scope.$on('$viewContentLoaded', function(){
                                //Here your view content is fully loaded !!
                                var element=$('#reservationtime');
                                element.daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });
                            });
                            $scope.cbTagSelectSetting = {
                                height: 25,
                                width: 150
                            };
                            //dữ liệu lên gridview
                            $scope.gridSchedulecars = new App.Shared.Directives.GridviewControl("gridSchedulecars");
                            //$scope.gridSchedulecars.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                            $scope.gridSchedulecars.filename_excel="Báo cáo Lịch Trình Xe";
                            $scope.gridSchedulecars.columns = [];
                            $scope.gridSchedulecars.height = window.innerHeight - 250;
                            var m = [{STT: '55',timeStart: '259347592375',AddressStart: '23',AddressEnd: '11' ,km: '234235',Hours: '259347' } ];
                            //$scope.gridSchedulecars.columns.push({ text: 'STT', dataField: 'STT', type: 'string', align: 'center', width:'50px' });
                            $scope.gridSchedulecars.columns.push({ text: 'Số hiệu', dataField: 'timeStart', type: 'string', align: 'center',width:'150px' });
                            $scope.gridSchedulecars.columns.push({ text: 'Thời gian bắt đầu', dataField: 'AddressStart', type: 'string', align: 'center', width:'250px' });
                            $scope.gridSchedulecars.columns.push({ text: 'Thời gian kết thúc', dataField: 'timeEnd', type: 'string', align: 'center', width:'250px'});
                            $scope.gridSchedulecars.columns.push({ text: 'Nội dung', dataField: 'AddressEnd', type: 'string', align: 'center', width: 'auto' });
                            $scope.gridSchedulecars.data = m;
                        }
                    }
                    AdModule.controller('SchedulecarsController', SchedulecarsController);
                }
            }
        }

    }
}