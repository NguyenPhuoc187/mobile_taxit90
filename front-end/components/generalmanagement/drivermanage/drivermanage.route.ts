/**
 * Created by Tuantv on 05/17/2016.
 */
module App {
    export module Components {
        export module Generalmanagement{
            export module Drivermanage {
                export class DrivermanageRouterModel {
                    public static $inject=['sysRouter','global'];
                    constructor(sysRouter:Shared.IRouterHelper,global: App.Shared.GlobalService) {

                        sysRouter.configureStates({
                            name: 'drivermanage',
                            config: {
                                controller: Generalmanagement.Drivermanage.DrivermanageController,
                                templateUrl: '/components/generalmanagement/drivermanage/drivermanage.html'+`?v=${global.Version}`,
                                url: '/Danh-sach-tai',
                                //abstract:true,// trang master
                                tag:{
                                    title:'DS & Lập trình tài xế',
                                    icon:'icon-user-check',
                                    groupId:'general',
                                    index:1
                                }
                            }
                        });

                    }
                }
                AdModule.run(DrivermanageRouterModel);
            }
        }

    }
}