/**
 * Created by Tuantv on 05/17/2016.
 */
module App {
    export module Components {
        export module Generalmanagement{
            export module Drivermanage {

                export interface ISysDriver {
                    Id?: number;
                    Name: string;
                    Gplx: string;
                    Mnv: string;
                    CompanyId?: number;
                    GroupId?: number;
                    Cmnd: string;
                    Address:string;
                    Born: number;
                    Male:any;
                    CreateDateOfGplx:any;
                    EndDateOfGplx:any;
                    AddressOfGplx: string,
                    Phone1: string,
                    Phone2: string





                }
                export interface IDrivermanageScope extends ng.IScope {
                    gridDriver: App.Shared.Directives.GridviewControl;
                    cpnSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.ICompany>;
                    // cpnTagSelect: (event:any) => any;
                    drdSelectSettings: any;
                    drdTagSelect: (event:any) => any;
                    titleCompany: string;
                    viewData: string;
                    btViewClick:() => any;
                    // groupSelectSettings: any;
                    // groupTagSelect: (event:any) => any;
                    funNew: ISysDriver;
                    isNew: boolean;
                    Id_delete: any;
                    Id_update: any;
                    IdCompany_delete:any;
                    newFormAccept: (event:any) => any;
                    timeCreatInputSettings:any;
                    timeEndInputSettings:any;
                    timeCreat:any;
                    timeEnd:any;
                    laptrinhRFID: App.Components.Adsunmanage.Developdevices.ISysConfig;
                    carSelectSettings:App.Shared.Gui.IDropDown<App.Shared.Entity.IDevice>;
                    // carSelect:(event:any) => any;
                    RFIDFormAccept:()=>any;
                    companyId:any;
                    cpnInstance:any;
                    timerselectcpn:any;

                    init:()=>any;


                    //    Lập trình tài xế
                    cpntaixeSelectSettings:App.Shared.Gui.IDropDown<App.Shared.Entity.ICompany>;
                    groupSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IGroup>;
                    modelSelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IDevice>;
                    driver2SelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IDriver>;
                    driver1SelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IDriver>;
                    driver3SelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IDriver>;
                    driver4SelectSettings: App.Shared.Gui.IDropDown<App.Shared.Entity.IDriver>;
                    btShowClick: (event:any) => any;
                    btConfigClick:(event:any) => any;
                    //khai báo checkbook
                    checkboxModellistDriver1:any;
                    checkboxModellistDriver2:any;
                    checkboxModellistDriver3:any;
                    checkboxModellistDriver4:any;
                    // khai báo tài xế1
                    NameDriver1:any;
                    IdDriver1:any;
                    PhoneDriver1:any;
                    GPLXDriver1:any;
                    // khai báo tài xế2
                    NameDriver2:any;
                    IdDriver2:any;
                    PhoneDriver2:any;
                    GPLXDriver2:any;
                    // khai báo tài xế3
                    NameDriver3:any;
                    IdDriver3:any;
                    PhoneDriver3:any;
                    GPLXDriver3:any;
                    // khai báo tài xế4
                    NameDriver4:any;
                    IdDriver4:any;
                    PhoneDriver4:any;
                    GPLXDriver4:any;
                    gridDevelopDriverDevices:App.Shared.Directives.GridviewControl;
                    Timeupdate:string;
                    MessageSetup:string;
                    thongsolaptrinh:any;
                }

                export class DrivermanageController {
                    public static $inject = ['$scope', '$http', 'global','language','api','$q'];

                    constructor($scope:IDrivermanageScope, public $http: ng.IHttpService, public global: App.Shared.GlobalService,
                                public language:App.Shared.LanguageService, public api: App.Shared.Api.IApi,public $q: ng.IQService) {
                        // $scope.$on('$viewContentLoaded', ()=>{
                        //     loaddatetime();
                        // });
                        // window.addEventListener("load", () => {
                        //     loaddatetime();
                        // });
                        // function loaddatetime(){
                        //     var element=$('#CreateDateOfGplx');
                        //     element.daterangepicker(global.SingleDate);
                        //     $('#EndDateOfGplx').daterangepicker(global.SingleDate);
                        // }
                        // $scope.init = () => {
                        //         var element=$('#CreateDateOfGplx');
                        //         element.daterangepicker(global.SingleDate);
                        //
                        // };
                        $scope.timeCreatInputSettings =
                        {
                            width: 200,
                            height: 30,
                            formatString: 'd'
                        };
                        $scope.timeCreat = new Date();
                        $scope.timeEndInputSettings =
                        {
                            width: 200,
                            height: 30,
                            formatString: 'd'
                        };
                        $scope.timeEnd = new Date();
                        $scope.titleCompany=language.gridLg().company;
                        $scope.viewData=language.gridLg().viewData;

                        $scope.cpnSelectSettings =new App.Shared.Gui.DropDown<App.Shared.Entity.ICompany>('cbCompanyDriver','Display','Id');
                        // Tiêu đề trên combobox
                        $scope.cpnSelectSettings.title= language.gridLg().company;
                        // Set chiều dài cho combobox
                        $scope.cpnSelectSettings.setting.width = '100%';
                        $scope.cpnSelectSettings.setting.filterPlaceHolder = 'Tìm công ty...';
                        $scope.cpnSelectSettings.setting.placeHolder = 'Chọn công ty';
                        $scope.cpnSelectSettings.setting.checkboxes = false;

                        $scope.carSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IDevice>('cbDeviceDriver','Bs','Serial');
                        $scope.carSelectSettings.title='';
                        $scope.carSelectSettings.setting.width = '100%';
                        $scope.carSelectSettings.setting.placeHolder = 'Chọn xe';
                        $scope.carSelectSettings.setting.checkboxes = false;


                        // load công ty 2 thông số truyền vào : dữ liệu lấy về, vị trí truyền vào ban đầu
                        // $scope.cpnSelectSettings.load(api.GetAllCompany(),null,true);
                        $scope.cpnSelectSettings.loadPromise(api.GetAllCompany(), null,$q).then(()=>{
                            // console.log(localStorage.getItem('companyIdmapxekhach'));
                            if(localStorage.getItem('companyIdquantri') !== '' && localStorage.getItem('companyIdquantri') !== null){
                                console.log(localStorage.getItem('companyIdquantri'));
                                let companyid:any = localStorage.getItem('companyIdquantri');
                                $scope.cpnSelectSettings.setItemDefault((m:any)=>m.Id === companyid*1);
                            }else{
                                $scope.cpnSelectSettings.setting.jqxDropDownList('selectIndex',0);
                            }

                        });
                        $scope.cpnSelectSettings.customEvent.onSelectOnItem = (ind:number, val:App.Shared.Entity.ICompany) =>{
                            localStorage.setItem('companyIdquantri',`${val.Id}`,);
                            language.showLoading(false,"");
                            api.GetAllDriverByCompany(val.Id).then((data:any)=>{
                                language.showLoading(true,"");
                               $scope.gridDriver.data = data;
                               AdapterData($scope.gridDriver.data);
                               $scope.gridDriver.refresh();
                           });

                            $scope.carSelectSettings.load(api.GetAllDeviceByCompanyId(val.Id),null,false);
                        };

                        // $scope.timerselectcpn = setInterval(()=>{
                        //     /*todo: Lấy thông tin toàn bộ công ty*/
                        //     $http.get(global.ReportHost +'api/Company/GetAllCompany').then((rep:any)=>{
                        //         //console.log(rep);
                        //         $scope.cpnSelectSettings.source = rep.data.CompanyList;
                        //         //$scope.drdSelectSettings.source = rep.data.CompanyList;
                        //
                        //         $('#'+$scope.cpnInstance.element.id).on('bindingComplete',  (event)=> {
                        //             console.log('binding ok');
                        //             //$scope.cpnSelectSettings.selectedIndex =  0;
                        //             $scope.cpnSelectSettings.jqxComboBox('selectIndex', 0);
                        //             //alert('hello');
                        //             $http.get(global.ReportHost + `/api/Driver/GetDriverByCompany?companyId=${$scope.cpnSelectSettings.valueMember}`).then((rep:any) => {
                        //                 console.log(rep);
                        //                 $.each(rep.data.driverList, function (index, value) {
                        //                     Gioitinh(value);
                        //                 });
                        //                 $scope.gridDriver.data = rep.data.driverList;
                        //                 AdapterData($scope.gridDriver.data);
                        //                 $scope.gridDriver.refresh();
                        //             });
                        //         });
                        //
                        //     });
                        //     clearInterval($scope.timerselectcpn);
                        // },500);


                        $scope.gridDriver = new App.Shared.Directives.GridviewControl("gridDriver");
                        $scope.gridDriver.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                        $scope.gridDriver.filename_excel="Danh sách tài xế";

                        /*Hiển thị dữ liệu grid-view*/
                        $scope.gridDriver.columns = [];
                        $scope.gridDriver.height = window.innerHeight - 220;
                        $scope.gridDriver.columnsHeight = 30;
                        $scope.gridDriver.isExport = true;
                        //bật tính năng lọc dữ liệu
                        $scope.gridDriver.isAltRows=true;
                        $scope.gridDriver.isFilter=true;
                        $scope.gridDriver.isFilterMode= 'advanced';
                        $scope.gridDriver.columns.push({ text: 'STT', dataField: 'Stt', type: 'string', align: 'center', width:'35px',pinned:true });
                        $scope.gridDriver.columns.push({ text: 'Tên tài xế', dataField: 'Name', type: 'string', align: 'center',width:'100px',filterable:true  });
                        $scope.gridDriver.columns.push({ text: 'MNV', dataField: 'Mnv', type: 'string', align: 'center', width:'80px'});
                        $scope.gridDriver.columns.push({ text: 'Biển số', dataField: 'Bs', type: 'string', align: 'center', width: '90px',filterable:true  });
                        $scope.gridDriver.columns.push({ text: 'Gplx', dataField: 'Gplx', type: 'string', align: 'center', width:'110px',filterable:true  });
                        // $scope.gridDriver.columns.push({ text: 'Giới tính', dataField: 'FOM', type: 'string', align: 'center', width: '100px' });
                        //$scope.gridDriver.columns.push({ text: 'CMND', dataField: 'Cmnd', type: 'string', align: 'center', width:'120px'});
                        //$scope.gridDriver.columns.push({ text: 'Nơi sinh', dataField: 'Address', type: 'string', align: 'center', width:'150px'});
                        $scope.gridDriver.columns.push({ text: 'Năm sinh', dataField: 'Born', type: 'string', align: 'center', width: '80px' });
                        $scope.gridDriver.columns.push({ text: 'Ngày cấp', dataField: 'CreateDateOfGplx', type: 'string', align: 'center', width:'82px'});
                        $scope.gridDriver.columns.push({ text: 'Ngày hết hạn', dataField: 'EndDateOfGplx', type: 'string', align: 'center', width: '82px' });
                        $scope.gridDriver.columns.push({ text: 'Nơi cấp', dataField: 'AddressOfGplx', type: 'string', align: 'center', width: '80px' });
                        $scope.gridDriver.columns.push({ text: 'SĐT', dataField: 'Phone1', type: 'string', align: 'center', width: 'auto' });
                        //$scope.gridDriver.columns.push({ text: 'SĐT 2', dataField: 'Phone2', type: 'string', align: 'center', width: '150px' });
                        //$scope.gridDriver.columns.push({ text: 'id công ty', dataField: 'CompanyId', type: 'string', align: 'center', width: 'auto' });
                        /*Chức năng thêm, xóa, sửa*/
                        $scope.gridDriver.behavior.isNew = false;
                        $scope.gridDriver.behavior.isEdit = true;
                        $scope.gridDriver.behavior.isDelete = false;

                        $scope.gridDriver.behavior.isRFID = true;
                        

                        // $scope.cpnTagSelect = (event) => {
                        //     var item = event.args.item;
                        //     //console.log(item);
                        //     if (item) {
                        //         $scope.cpnSelectSettings.valueMember = item.originalItem.Id;
                        //         $http.get(global.ReportHost +'api/Group/GetGroupByCompany?companyId='+$scope.cpnSelectSettings.valueMember).then((rep:any)=>{
                        //             // console.log(rep);
                        //             $scope.groupSelectSettings.source = rep.data.GroupList;
                        //         });
                        //     }
                        // };



                        // /*Khai báo drop-down list công ty trên bảng thêm mới*/
                        // $scope.drdSelectSettings = {};
                        // $scope.drdSelectSettings.displayMember = "Name";
                        // $scope.drdTagSelect = (event) => {
                        //     var item = event.args.item;
                        //     //console.log(item);
                        //     if (item) {
                        //         $scope.drdSelectSettings.valueMember = item.originalItem.Id;
                        //         /*todo: Lấy thông tin nhom xe theo công ty*/
                        //
                        //     }
                        // };
                        /*Khai báo drop-down list đội xe*/
                        // $scope.groupSelectSettings = {};
                        // $scope.groupSelectSettings.displayMember = "Name";
                        // $scope.groupTagSelect = (event) => {
                        //     var item = event.args.item;
                        //     if (item) {
                        //         $scope.groupSelectSettings.valueMember = item.originalItem.Id;
                        //         $scope.funNew.GroupId= $scope.groupSelectSettings.valueMember;
                        //     }
                        // };

                        /*Khi nhấn nút Xem*/
                        $scope.btViewClick = () => {
                            language.showLoading(false,"");
                            if($scope.cpnSelectSettings.itemSelect() != undefined) {
                                api.GetAllDriverByCompany($scope.cpnSelectSettings.itemSelect().Id).then((data:any)=>{
                                    language.showLoading(true,"");
                                    $scope.gridDriver.data = data;
                                    AdapterData($scope.gridDriver.data);
                                    $scope.gridDriver.refresh();
                                });

                            }else{
                                language.showLoading(true,"");
                                bootbox.dialog({
                                    title: "Thông Báo",
                                    message: "Vui lòng chọn công ty !!",
                                    buttons: {
                                        cancle: {
                                            label: "Quay lại",
                                            className: "btn-primary",
                                        }
                                    }
                                });
                            }
                        };

                        /*Khai báo giá trị biến ban đầu*/
                        $scope.funNew = {
                            Name: '',
                            Gplx: '',
                            Mnv: '',
                            Born: 0,
                            Cmnd: '',
                            Address:'',
                            Male:true,
                            CreateDateOfGplx:'',
                            EndDateOfGplx:'',
                            AddressOfGplx: '',
                            Phone1: '',
                            Phone2: ''


                    };
                        /* todo: Thêm mới danh sách tài
                         =============================================*/
                        $scope.gridDriver.event.onNew = (ne:any) => {
                            $scope.isNew = true;
                            $scope.funNew = {
                                Name: '',
                                Gplx: '',
                                Mnv: '',
                                CompanyId: 0,
                                GroupId: 0,
                                Born: 0,
                                Cmnd: '',
                                Address:'',
                                Male:true,
                                CreateDateOfGplx:'',
                                EndDateOfGplx:'',
                                AddressOfGplx: '',
                                Phone1: '',
                                Phone2: ''
                            };
                            $('#EndDateOfGplx').daterangepicker({
                                singleDatePicker: true,
                                showDropdowns: true,
                                format: 'DD/MM/YYYY',
                                opens:'left',
                                drops: 'up',
                                locale: { cancelLabel: 'Hủy',
                                    applyLabel: 'Đồng ý',
                                    fromLabel:'Từ ngày',
                                    toLabel:'Đến ngày',
                                    daysOfWeek: ['<label class="text-danger">CN</label>', 'T2', 'T3', 'T4', 'T5', 'T6','T7'],
                                    monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
                                    firstDay: 1

                                },
                            });
                            $('#CreateDateOfGplx').daterangepicker({
                                singleDatePicker: true,
                                showDropdowns: true,
                                format: 'DD/MM/YYYY',
                                opens:'left',
                                drops: 'up',
                                locale: { cancelLabel: 'Hủy',
                                    applyLabel: 'Đồng ý',
                                    fromLabel:'Từ ngày',
                                    toLabel:'Đến ngày',
                                    daysOfWeek: ['<label class="text-danger">CN</label>', 'T2', 'T3', 'T4', 'T5', 'T6','T7'],
                                    monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
                                    firstDay: 1

                                },
                            });
                            $scope.$apply();
                            $('#myModal').modal('show');

                        };

                        /*Kết thúc Thêm mới tài xế
                         =============================================*/

                        /* todo: Cập nhật tài xế
                         =============================================*/
                        $scope.gridDriver.event.onEdit = (ne:any,old:any) => {
                            $scope.isNew = false;
                            //console.log(ne);
                            $scope.Id_update = ne.Id;
                            $scope.funNew.Id = ne.Id;
                            $scope.funNew.Name=ne.Name;
                            $scope.funNew.Gplx=ne.Gplx;
                            $scope.funNew.Mnv=ne.Mnv;
                            $scope.funNew.Born=ne.Born;
                            $scope.funNew.Cmnd= ne.Cmnd;
                            $scope.funNew.Address=ne.Address;
                            $scope.funNew.Male=ne.Male;
                            $scope.funNew.CreateDateOfGplx=ne.CreateDateOfGplx;
                            $scope.funNew.EndDateOfGplx=ne.EndDateOfGplx;
                            $scope.funNew.AddressOfGplx=ne.AddressOfGplx;
                            $scope.funNew.Phone1=ne.Phone1;
                            $scope.funNew.Phone2=ne.Phone2;
                            $('#EndDateOfGplx').daterangepicker({
                                singleDatePicker: true,
                                showDropdowns: true,
                                format: 'DD/MM/YYYY',
                                opens:'left',
                                drops: 'up',
                                locale: { cancelLabel: 'Hủy',
                                    applyLabel: 'Đồng ý',
                                    fromLabel:'Từ ngày',
                                    toLabel:'Đến ngày',
                                    daysOfWeek: ['<label class="text-danger">CN</label>', 'T2', 'T3', 'T4', 'T5', 'T6','T7'],
                                    monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
                                    firstDay: 1

                                },
                                startDate: $scope.funNew.EndDateOfGplx
                            });
                            $('#CreateDateOfGplx').daterangepicker({
                                singleDatePicker: true,
                                showDropdowns: true,
                                format: 'DD/MM/YYYY',
                                opens:'left',
                                drops: 'up',
                                locale: { cancelLabel: 'Hủy',
                                    applyLabel: 'Đồng ý',
                                    fromLabel:'Từ ngày',
                                    toLabel:'Đến ngày',
                                    daysOfWeek: ['<label class="text-danger">CN</label>', 'T2', 'T3', 'T4', 'T5', 'T6','T7'],
                                    monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
                                    firstDay: 1

                                },
                                startDate: $scope.funNew.CreateDateOfGplx
                            });
                            // for(var i = 0; i< $scope.drdSelectSettings.source.length;i++){
                            //     if($scope.drdSelectSettings.source[i].Id == ne.CompanyId){
                            //         $scope.drdSelectSettings.jqxDropDownList('selectIndex',i);
                            //         $scope.drdSelectSettings.jqxDropDownList({disabled:true});
                            //         break;
                            //     }
                            // }

                            $scope.$apply();
                            $('#myModal').modal('show');

                        };

                        /*Kết thúc cập nhật đội xe
                         =============================================*/
                        /* todo: Xóa đội xe
                         =============================================*/
                        $scope.gridDriver.event.onDelete = (old:any) => {
                            //console.log(old);
                            $scope.Id_delete = old.Id;
                            $scope.IdCompany_delete = old.CompanyId;
                            bootbox.dialog({
                                title: 'Thông báo',
                                message: 'Bạn muốn xóa tài xế này',
                                buttons: {
                                    cancel: {
                                        label: 'Hủy',
                                        className: "btn-danger",
                                    },
                                    confirm: {
                                        label: 'Đồng ý',
                                        className: "btn-success",
                                        callback: () => {
                                            var req:any = {
                                                method: 'DELETE',
                                                url: global.ReportHost +`/api/Driver/DeleteDriver?companyId=${$scope.IdCompany_delete}&driverId=${$scope.Id_delete}`,

                                            };
                                            $http(req).success((data:any, status:any, header:any, config:any) => {

                                                console.log(data);
                                                if (data.Status === 1) {
                                                    // $http.get(global.ReportHost + `/api/Driver/GetDriverByCompany?companyId=${$scope.IdCompany_delete}`).then((rep:any) => {
                                                    //     //  $.each(rep.data.driverList, function (index, value) {
                                                    //     //     Gioitinh(value);
                                                    //     // });
                                                    //     $scope.gridDriver.data = rep.data.driverList;
                                                    //     AdapterData($scope.gridDriver.data);
                                                    //     $scope.gridDriver.refresh();
                                                    // });
                                                    api.GetAllDriverByCompany($scope.cpnSelectSettings.itemSelect().Id).then((data:any)=>{
                                                        $scope.gridDriver.data = data;
                                                        AdapterData($scope.gridDriver.data);
                                                        $scope.gridDriver.refresh();
                                                    });
                                                }

                                            });

                                        }
                                    }
                                },
                                animate: true
                            });
                        };

                        /*Kết thúc xóa đội xe
                         =============================================*/

                        /*Ghi thẻ RFID
                         =============================================*/
                        $scope.gridDriver.event.onRFID = (ne:any) => {
                            console.log(ne);
                            $scope.laptrinhRFID = {
                                CSetup:null,
                                ISetup:null,
                                LSetup:null,
                                QSetup:null,
                                SSetup:null,
                                TSetup:null,
                                XSetup:null,
                                PSetup:null,
                                FuelType :null,
                                Command:null,
                                Username:null
                            };

                            $scope.laptrinhRFID.LSetup= {
                                Drivers: [
                                    {
                                        "Name": ne.Name,
                                        "Gplx": ne.Gplx,
                                        "Phone": ne.Phone1,
                                        "Id": ne.Id
                                    }
                                ]
                            };
                            $('#myModal_RFID').modal('show');
                        };

                        /*Nhấn nút xác nhận trong RFID*/
                        $scope.RFIDFormAccept = () => {
                            language.showLoading(false,"");
                            var req:any = {
                                method: 'POST',
                                url: global.ReportHost + `/api/SetupDevice/SetupDevive?idCompany=${$scope.cpnSelectSettings.itemSelect().Id}&serial=${$scope.carSelectSettings.itemSelect().Serial}`,
                                //headers: {token: global.Token.GetValue()},
                                data: {
                                    CSetup: $scope.laptrinhRFID.CSetup,
                                    ISetup: $scope.laptrinhRFID.ISetup,
                                    LSetup: $scope.laptrinhRFID.LSetup,
                                    QSetup: $scope.laptrinhRFID.QSetup,
                                    SSetup: $scope.laptrinhRFID.SSetup,
                                    TSetup: $scope.laptrinhRFID.TSetup,
                                    XSetup: $scope.laptrinhRFID.XSetup,
                                    PSetup: $scope.laptrinhRFID.PSetup,
                                    FuelType: $scope.laptrinhRFID.FuelType,
                                    Command: $scope.laptrinhRFID.Command,
                                    Username: $scope.laptrinhRFID.Username
                                }
                            };
                            $http(req).success((data:any, status:any, header:any, config:any)=> {
                                language.showLoading(true,"");
                                console.log(data);
                                if(data.Status==1)
                                {
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Ghi thẻ RFID thành công",
                                        buttons: {
                                            cancle: {
                                                label: "Đồng ý",
                                                className: "btn-success",
                                            }
                                        }
                                    });
                                }
                                else{
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Ghi thẻ RFID không thành công. Xin thử lại !!",
                                        buttons: {
                                            cancle: {
                                                label: "Quay lại",
                                                className: "btn-danger",
                                            }
                                        }
                                    });
                                }

                            });
                            $('#myModal_RFID').modal('hide');
                        };


                        /*todo: Khi nhấn nút xác nhận
                         =============================================*/
                        $scope.newFormAccept = (event) => {
                            language.showLoading(false,"");
                            if($scope.isNew){
                                var req:any = {
                                    method: 'POST',
                                    url: global.ReportHost + 'api/Driver/AddDriver',
                                    data: {
                                        Name: $scope.funNew.Name,
                                        Gplx: $scope.funNew.Gplx,
                                        Mnv: $scope.funNew.Mnv,
                                        CompanyId: $scope.cpnSelectSettings.itemSelect().Id,
                                        GroupId: 0,
                                        Born: $scope.funNew.Born,
                                        Address:$scope.funNew.Address,
                                        Male:true,
                                        CreateDateOfGplx:global.ChangeSignDate_Server($scope.funNew.CreateDateOfGplx),
                                        EndDateOfGplx:global.ChangeSignDate_Server($scope.funNew.EndDateOfGplx),
                                        AddressOfGplx:$scope.funNew.AddressOfGplx,
                                        Phone1:$scope.funNew.Phone1,
                                        Phone2: $scope.funNew.Phone2,
                                        Cmnd: $scope.funNew.Cmnd
                                    }
                                };
                                $http(req).success((data:any, status:any, header:any, config:any) => {
                                    language.showLoading(true,"");
                                    if(data.Status == 1){
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Thêm tài xế thành công!!",
                                            buttons: {
                                                cancle: {
                                                    label: "Đồng ý",
                                                    className: "btn-success",
                                                    callback:()=>{
                                                        GetDataDriver();
                                                    }
                                                }
                                            }
                                        });
                                    }else{
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Thêm tài xế không thành công. Vui lòng thử lại!!",
                                            buttons: {
                                                cancle: {
                                                    label: "Quay lại",
                                                    className: "btn-danger",

                                                }
                                            }
                                        });
                                    }

                                });
                                $('#myModal').modal('hide');
                            }else{
                                var req_capnhat:any = {
                                    method: 'PUT',
                                    url: global.ReportHost + 'api/Driver/UpdateDriver',
                                    data: {
                                        Id: $scope.funNew.Id,
                                        Name: $scope.funNew.Name,
                                        Gplx: $scope.funNew.Gplx,
                                        Mnv: $scope.funNew.Mnv,
                                        CompanyId: $scope.cpnSelectSettings.itemSelect().Id,
                                        GroupId: 0,
                                        Born: $scope.funNew.Born,
                                        Address:$scope.funNew.Address,
                                        CreateDateOfGplx:global.ChangeSignDate_Server($scope.funNew.CreateDateOfGplx),
                                        EndDateOfGplx:global.ChangeSignDate_Server($scope.funNew.EndDateOfGplx),
                                        AddressOfGplx:$scope.funNew.AddressOfGplx,
                                        Phone1:$scope.funNew.Phone1,
                                        Phone2: $scope.funNew.Phone2,
                                        Cmnd: $scope.funNew.Cmnd,
                                        Male:true
                                    }
                                };
                                //console.log(req_capnhat);
                                $http(req_capnhat).success((data:any, status:any, header:any, config:any) => {
                                    language.showLoading(true,"");
                                    // $http.get(global.ReportHost + `/api/Driver/GetDriverByCompany?companyId=${$scope.cpnSelectSettings.valueMember}`).then((rep:any) => {
                                    //     $.each(rep.data.driverList, function (index, value) {
                                    //         Gioitinh(value);
                                    //     });
                                    //     $scope.gridDriver.data = rep.data.driverList;
                                    //     AdapterData($scope.gridDriver.data);
                                    //     $scope.gridDriver.refresh();
                                    // });
                                    if(data.Status == 1){
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật tài xế thành công!!",
                                            buttons: {
                                                cancle: {
                                                    label: "Đồng ý",
                                                    className: "btn-success",
                                                    callback:()=>{
                                                        GetDataDriver();
                                                    }
                                                }
                                            }
                                        });
                                    }else{
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật tài xế không thành công. Vui lòng thử lại!!",
                                            buttons: {
                                                cancle: {
                                                    label: "Quay lại",
                                                    className: "btn-danger",
                                                }
                                            }
                                        });
                                    }

                                });
                                $('#myModal').modal('hide');
                            }
                        };


                        //Lập trình tài xế


                        $scope.gridDevelopDriverDevices = new App.Shared.Directives.GridviewControl("gridDevelopDriverDevices");
                        $scope.gridDevelopDriverDevices.selectionModeinfo = global.GridViewMode; //bật chế độ custom cho data table để copy từng cột
                        $scope.gridDevelopDriverDevices.filename_excel="Danh sách tài xế lập trình ";
                        $scope.gridDevelopDriverDevices.columns = [];
                        $scope.gridDevelopDriverDevices.height = window.innerHeight - 350;
                        $scope.gridDevelopDriverDevices.columnsHeight = 40;
                        $scope.gridDevelopDriverDevices.isExport = false;

                        $scope.gridDevelopDriverDevices.isFilterMode= 'advanced';
                        $scope.gridDevelopDriverDevices.columns.push({ text: 'ID', dataField: 'Id', type: 'string', align: 'center',filterable:true,width:'150px' });
                        $scope.gridDevelopDriverDevices.columns.push({ text: 'Tài xế', dataField: 'Name', type: 'string', align: 'center',filterable:true,width:'250px' });
                        $scope.gridDevelopDriverDevices.columns.push({ text: 'GPLX', dataField: 'Gplx', type: 'string', align: 'center',filterable:true,width:'250px' });
                        $scope.gridDevelopDriverDevices.columns.push({ text: 'Phone', dataField: 'Phone', type: 'string', align: 'center',filterable:true,width:'auto' });



                        /*Khai báo combobox*/
                        // Khai báo mới combobox truyền vào 3 thông số : tên định dạng của combobox, hiển thị tên trường, giá trị chọn
                        $scope.cpntaixeSelectSettings =new App.Shared.Gui.DropDown<App.Shared.Entity.ICompany>('cbCompanyDevelopDevices','Display','Id');
                        // Tiêu đề trên combobox
                        $scope.cpntaixeSelectSettings.title= language.gridLg().company;
                        // Set chiều dài cho combobox
                        $scope.cpntaixeSelectSettings.setting.width = 330;
                        $scope.cpntaixeSelectSettings.setting.placeHolder = 'Chọn công ty...';
                        $scope.cpntaixeSelectSettings.setting.filterPlaceHolder = 'Tìm công ty...';
                        $scope.cpntaixeSelectSettings.setting.checkboxes = false;

                        // Khai báo combobox đội xe
                        $scope.groupSelectSettings=new App.Shared.Gui.DropDown<App.Shared.Entity.IGroup>('cbGroupDevelopDevices','Name','Id');
                        $scope.groupSelectSettings.title=language.gridLg().group;
                        $scope.groupSelectSettings.setting.width = 150;
                        $scope.groupSelectSettings.setting.placeHolder = 'Chọn đội xe...';
                        $scope.groupSelectSettings.setting.filterPlaceHolder = 'Tìm đội xe...';
                        $scope.groupSelectSettings.setting.checkboxes = false;

                        // Khai báo combobox xe
                        $scope.modelSelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IDevice>('cbDeviceDevelopDevices','Display','Serial');
                        $scope.modelSelectSettings.title=language.gridLg().car;
                        $scope.modelSelectSettings.setting.width = 250;
                        $scope.modelSelectSettings.setting.checkboxes = false;

                        //Khai báo tài xế
                        //Tài xế 1
                        $scope.driver1SelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IDriver>('drDriver1QCVN31Master','Name','Id');
                        $scope.driver1SelectSettings.setting.width = 230;
                        $scope.driver1SelectSettings.title = '';
                        $scope.driver1SelectSettings.setting.placeHolder = 'Chọn tài xế 1';
                        $scope.driver1SelectSettings.setting.filterPlaceHolder = 'Tìm tài xế';
                        $scope.driver1SelectSettings.setting.checkboxes = false;

                        // Tài xế 2
                        $scope.driver2SelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IDriver>('drDriver2QCVN31Master','Name','Id');
                        $scope.driver2SelectSettings.setting.width = 230;
                        $scope.driver2SelectSettings.title = '';
                        $scope.driver2SelectSettings.setting.placeHolder = 'Chọn tài xế 2';
                        $scope.driver2SelectSettings.setting.filterPlaceHolder = 'Tìm tài xế';
                        $scope.driver2SelectSettings.setting.checkboxes = false;


                        // Tài xế 3
                        $scope.driver3SelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IDriver>('drDriver3QCVN31Master','Name','Id');
                        $scope.driver3SelectSettings.setting.width = 230;
                        $scope.driver3SelectSettings.title = '';
                        $scope.driver3SelectSettings.setting.placeHolder = 'Chọn tài xế 3';
                        $scope.driver3SelectSettings.setting.filterPlaceHolder = 'Tìm tài xế';
                        $scope.driver3SelectSettings.setting.checkboxes = false;



                        // Tài xế 4
                        $scope.driver4SelectSettings = new App.Shared.Gui.DropDown<App.Shared.Entity.IDriver>('drDriver4QCVN31Master','Name','Id');
                        $scope.driver4SelectSettings.setting.width = 230;
                        $scope.driver4SelectSettings.title = '';
                        $scope.driver4SelectSettings.setting.placeHolder = 'Chọn tài xế 4';
                        $scope.driver4SelectSettings.setting.filterPlaceHolder = 'Tìm tài xế';
                        $scope.driver4SelectSettings.setting.checkboxes = false;

                        /*combo-box công ty
                         * =======================================*/

                        // load công ty 2 thông số truyền vào : dữ liệu lấy về, vị trí truyền vào ban đầu
                        // $scope.cpntaixeSelectSettings.load(api.GetAllCompany(),null,true);
                        $scope.cpntaixeSelectSettings.loadPromise(api.GetAllCompany(), null,$q).then(()=>{
                            // console.log(localStorage.getItem('companyIdmapxekhach'));
                            if(localStorage.getItem('companyIdquantri') !== '' && localStorage.getItem('companyIdquantri') !== null){
                                console.log(localStorage.getItem('companyIdquantri'));
                                let companyid:any = localStorage.getItem('companyIdquantri');
                                $scope.cpntaixeSelectSettings.setItemDefault((m:any)=>m.Id === companyid*1);
                            }else{
                                $scope.cpntaixeSelectSettings.setting.jqxDropDownList('selectIndex',0);
                            }

                        });
                        //Sự kiện chọn công ty
                        $scope.cpntaixeSelectSettings.customEvent.onSelectOnItem=(index:number,val:App.Shared.Entity.ICompany)=>{

                            var first=Array<App.Shared.Entity.IGroup>();
                            // đẩy 1 group làm thông tin chọn tất cả
                            first.push( {
                                Id: -1,
                                Name: 'Tất cả',
                                CompanyId:val.Id
                            });

                            $scope.groupSelectSettings.load(api.GetAllGroupByCompanyId(val.Id),first,true);
                            $scope.driver1SelectSettings.load(api.GetAllDriverByCompany(val.Id),null);
                            $scope.driver2SelectSettings.load(api.GetAllDriverByCompany(val.Id),null);
                            $scope.driver3SelectSettings.load(api.GetAllDriverByCompany(val.Id),null);
                            $scope.driver4SelectSettings.load(api.GetAllDriverByCompany(val.Id),null);
                        };
                        // cài đặt sự kiện chọn đội xe
                        $scope.groupSelectSettings.customEvent.onSelectOnItem=(index:number,val:App.Shared.Entity.IGroup)=>{
                            if(val.Id == -1){// hiển thị tất cả các xe trong công ty
                                $scope.modelSelectSettings.load(api.GetAllDeviceByCompanyId(val.CompanyId),null,true);

                            }
                            else{// hiểm thị các xe trong đội xe đã chọn
                                $scope.modelSelectSettings.load(api.GetAllDeviceByGroupId(val.CompanyId,val.Id),null,true);

                            }
                        };
                        $scope.driver1SelectSettings.customEvent.onSelectOnItem = (index:number, val: App.Shared.Entity.IDriver)=>{
                            $scope.NameDriver1 = val.Name;
                            $scope.GPLXDriver1 = val.Gplx;
                            $scope.PhoneDriver1 = val.Phone1;
                            $scope.IdDriver1 = val.Id;
                            $scope.$apply();
                        };
                        $scope.driver2SelectSettings.customEvent.onSelectOnItem = (index:number, val: App.Shared.Entity.IDriver)=>{
                            $scope.NameDriver2 = val.Name;
                            $scope.GPLXDriver2 = val.Gplx;
                            $scope.PhoneDriver2 = val.Phone1;
                            $scope.IdDriver2 = val.Id;
                            $scope.$apply();
                        };
                        $scope.driver3SelectSettings.customEvent.onSelectOnItem = (index:number, val: App.Shared.Entity.IDriver)=>{
                            $scope.NameDriver3 = val.Name;
                            $scope.GPLXDriver3 = val.Gplx;
                            $scope.PhoneDriver3 = val.Phone1;
                            $scope.IdDriver3 = val.Id;
                            $scope.$apply();
                        };
                        $scope.driver4SelectSettings.customEvent.onSelectOnItem = (index:number, val: App.Shared.Entity.IDriver)=>{
                            $scope.NameDriver4 = val.Name;
                            $scope.GPLXDriver4 = val.Gplx;
                            $scope.PhoneDriver4 = val.Phone1;
                            $scope.IdDriver4 = val.Id;
                            $scope.$apply();
                        };

                        //set mặc định
                        function Setdefault(){


                            //tài xế 1
                            $scope.NameDriver1 = null;
                            $scope.GPLXDriver1 = null;
                            $scope.PhoneDriver1 = null;
                            $scope.IdDriver1 = null;

                            //tài xế 2
                            $scope.NameDriver2 = null;
                            $scope.GPLXDriver2 = null;
                            $scope.PhoneDriver2 = null;
                            $scope.IdDriver2 = null;

                            //tài xế 3
                            $scope.NameDriver3 = null;
                            $scope.GPLXDriver3 = null;
                            $scope.PhoneDriver3 = null;
                            $scope.IdDriver3 = null;

                            //tài xế 3
                            $scope.NameDriver4 = null;
                            $scope.GPLXDriver4 = null;
                            $scope.PhoneDriver4 = null;
                            $scope.IdDriver4 = null;

                            $scope.Timeupdate = null;
                            $scope.MessageSetup="";
                            $scope.checkboxModellistDriver1=false;
                            $scope.checkboxModellistDriver2=false;
                            $scope.checkboxModellistDriver3=false;
                            $scope.checkboxModellistDriver4=false;

                            // alert($scope.checkboxModelfw);

                        }
                        // bấm nút xem thông tin

                        $scope.btShowClick = () => {
                            Setdefault();
                            if($scope.cpnSelectSettings.itemSelect() !== undefined) {
                                if ($scope.modelSelectSettings.itemSelect() != undefined){
                                    api.GetDeviceSetupInfo($scope.cpnSelectSettings.itemSelect().Id,$scope.modelSelectSettings.itemSelect().Serial).then((data:any)=>{
                                        console.log(data);
                                        if(data.length>0) {

                                            // Tài xế 1
                                            $scope.NameDriver1 = data[0].LSetup.Drivers[0].Name;
                                            $scope.GPLXDriver1 = data[0].LSetup.Drivers[0].Gplx;
                                            $scope.PhoneDriver1 = data[0].LSetup.Drivers[0].Phone;
                                            $scope.IdDriver1 = data[0].LSetup.Drivers[0].Id;
                                            $scope.driver1SelectSettings.setItemDefault(m=>m.Id === data[0].LSetup.Drivers[0].Id);

                                            // Tài xế 2
                                            $scope.NameDriver2 = data[0].LSetup.Drivers[1].Name;
                                            $scope.GPLXDriver2 = data[0].LSetup.Drivers[1].Gplx;
                                            $scope.PhoneDriver2 = data[0].LSetup.Drivers[1].Phone;
                                            $scope.IdDriver2 = data[0].LSetup.Drivers[1].Id;
                                            $scope.driver2SelectSettings.setItemDefault(m=>m.Id === data[0].LSetup.Drivers[1].Id);


                                            // Tài xế 3
                                            $scope.NameDriver3 = data[0].LSetup.Drivers[2].Name;
                                            $scope.GPLXDriver3 = data[0].LSetup.Drivers[2].Gplx;
                                            $scope.PhoneDriver3 = data[0].LSetup.Drivers[2].Phone;
                                            $scope.IdDriver3 = data[0].LSetup.Drivers[2].Id;
                                            $scope.driver3SelectSettings.setItemDefault(m=>m.Id === data[0].LSetup.Drivers[2].Id);

                                            // Tài xế 4
                                            $scope.NameDriver4 = data[0].LSetup.Drivers[3].Name;
                                            $scope.GPLXDriver4 = data[0].LSetup.Drivers[3].Gplx;
                                            $scope.PhoneDriver4 = data[0].LSetup.Drivers[3].Phone;
                                            $scope.IdDriver4 = data[0].LSetup.Drivers[3].Id;
                                            $scope.driver3SelectSettings.setItemDefault(m=>m.Id === data[0].LSetup.Drivers[2].Id);

                                            $scope.Timeupdate = "Thời gian lập trình sau cùng: " + global.AdapterDate(data[0].TimeUpdate);
                                            $scope.gridDevelopDriverDevices.data = data[0].LSetup.Drivers;
                                            $scope.gridDevelopDriverDevices.refresh();

                                        }
                                        else {
                                            $scope.Timeupdate="Thiết bị này chưa được lập trình";
                                        }
                                    });
                                }
                                else {
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Vui lòng chọn xe !!",
                                        buttons: {
                                            cancle: {
                                                label: "Quay lại",
                                                className: "btn-primary",
                                            }
                                        }
                                    });
                                }

                            }else{
                                bootbox.dialog({
                                    title: "Thông Báo",
                                    message: "Vui lòng chọn công ty !!",
                                    buttons: {
                                        cancle: {
                                            label: "Quay lại",
                                            className: "btn-primary",
                                        }
                                    }
                                });
                            }
                        };

                        // lập trình
                        // bấm nút cấu hình thông tin
                        //var acc:<IAccount> = global.Account.GetValue();
                        $scope.btConfigClick = () => {


                            // var req:any = {
                            //     CSetup: null,
                            //     ISetup: null,
                            //     LSetup: null,
                            //     QSetup: null,
                            //     SSetup: null,
                            //     TSetup: null,
                            //     XSetup: null,
                            //     PSetup: null,
                            //     FuelType: null,
                            //     Command: null,
                            //     Username: null
                            // };
                            getconfig();
                            $scope.MessageSetup="Hệ thống đang gửi thông tin lập trình";
                            language.showLoading(false,"");
                            api.PostSetupDevices($scope.cpnSelectSettings.itemSelect().Id,$scope.modelSelectSettings.itemSelect().Serial,$scope.thongsolaptrinh).then((data:any)=>{
                                language.showLoading(true,"");
                                if(data === 'OK'){
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Lập trình xuống thiết bị thành công!!",
                                        buttons: {
                                            cancle: {
                                                label: "Đồng ý",
                                                className: "btn-success",
                                            }
                                        }
                                    });
                                    $scope.MessageSetup="Lập trình thành công";
                                }else{
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Lập trình xuống thiết bị thất bại!!",
                                        buttons: {
                                            cancle: {
                                                label: "Quay lại",
                                                className: "btn-danger",
                                            }
                                        }
                                    });
                                    $scope.MessageSetup="Lập trình thất bại";
                                }
                            });

                        };
                        // lấy thông số lập trình





                        function getconfig() {
                            $scope.thongsolaptrinh = {
                                CSetup: null,
                                ISetup: null,
                                LSetup: {
                                    Drivers: []
                                },
                                QSetup: null,
                                SSetup: null,
                                TSetup: null,
                                XSetup: null,
                                PSetup: null,
                                FuelType: null,
                                Command: null,
                                Username: null
                            };
                            var acc: any = global.Account.GetValue();
                            console.log(acc);

                            var driver1: any = {
                                Name: $scope.NameDriver1,
                                Gplx: $scope.GPLXDriver1,
                                Phone: $scope.PhoneDriver1,
                                Id: $scope.IdDriver1
                            };
                            var driver2: any = {
                                Name: $scope.NameDriver2,
                                Gplx: $scope.GPLXDriver2,
                                Phone: $scope.PhoneDriver2,
                                Id: $scope.IdDriver2
                            };
                            var driver3: any = {
                                Name: $scope.NameDriver3,
                                Gplx: $scope.GPLXDriver3,
                                Phone: $scope.PhoneDriver3,
                                Id: $scope.IdDriver3
                            };
                            var driver4: any = {
                                Name: $scope.NameDriver4,
                                Gplx: $scope.GPLXDriver4,
                                Phone: $scope.PhoneDriver4,
                                Id: $scope.IdDriver4
                            };

                            if ($scope.checkboxModellistDriver1) {
                                $scope.thongsolaptrinh.LSetup.Drivers.push(driver1);
                            }
                            if ($scope.checkboxModellistDriver2) {
                                $scope.thongsolaptrinh.LSetup.Drivers.push(driver2);
                            }
                            if ($scope.checkboxModellistDriver3) {
                                $scope.thongsolaptrinh.LSetup.Drivers.push(driver3);
                            }
                            if ($scope.checkboxModellistDriver4) {
                                $scope.thongsolaptrinh.LSetup.Drivers.push(driver4);
                            }

                            $scope.thongsolaptrinh.Username = acc.Username;


                        }

                        /*Kết thúc nút xác nhận
                         =============================================*/
                        /*Hàm chuyển hiển thị Location*/
                        function AdapterData (tmp:any){
                            $.each(tmp, (index:number,value:any)=>{
                                value.Stt=index + 1;
                                value.EndDateOfGplx = global.AdapterSignDate(value.EndDateOfGplx);
                                value.CreateDateOfGplx = global.AdapterSignDate(value.CreateDateOfGplx);
                                // if(value.Male==true) {
                                //     value.FOM = `<label class="label label-success" style="font-size:13px">Nam</label>`;
                                // }else {
                                //     value.FOM = `<label class="label label-success" style="font-size:13px">Nữ</label>`;
                                // }
                            });

                        }

                        function GetDataDriver(){
                            api.GetAllDriverByCompany($scope.cpnSelectSettings.itemSelect().Id).then((data:any)=>{
                                $scope.gridDriver.data = data;
                                AdapterData($scope.gridDriver.data);
                                $scope.gridDriver.refresh();
                            });
                        }



                        $('.nav-tabs a#ghithetaixe').on('shown.bs.tab', function (event:any){
                            $scope.gridDriver.refresh();
                        });
                        $('.nav-tabs a#laptrinhtaixe').on('shown.bs.tab', function (event:any){
                            $scope.gridDevelopDriverDevices.refresh();
                        });
                    }
                }

                AdModule.controller('DrivermanageController', DrivermanageController);
            }
        }

    }
}