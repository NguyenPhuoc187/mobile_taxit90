/**
 * Created by phamn on 04/10/2016.
 */
module App{
    export module Components{
        export module Generalmanagement{
            export module Infomationuser{
                export interface IInfouser{
                    displayname:string;
                    phoneAccount:string;
                    groupuser:string;
                    showlevel:string;
                    username:string;
                }
                export interface InfomationuserScope extends ng.IScope{
                    user: IInfouser;
                    passold:string;
                    passnew:string;
                    pass_confirm:string;
                    updatepassFormAccept:()=>any;
                    updateinfoFormAccept:()=>any;
                    checkchangpass:boolean;
                    displayname:string;
                    InfoAccount:string;
                    PhoneAccount:string;

                }
                export class InfomationuserController{
                    public static $inject=['$scope','api','global','$http','$cookieStore'];
                    constructor($scope: InfomationuserScope, public api: App.Shared.Api.IApi,
                                public global: App.Shared.GlobalService, public $http:ng.IHttpService, public $cookieStore:ng.cookies.ICookieStoreService ) {
                        $scope.user = {
                            displayname:'',
                            phoneAccount:'',
                            groupuser:'',
                            showlevel:'Root',
                            username:'',
                        };
                        $scope.checkchangpass = false;
                        $("#collapseExample").on('shown.bs.collapse', function(){
                            $scope.checkchangpass = true;
                        });
                        $("#collapseExample").on('hidden.bs.collapse', function(){
                            $scope.passold = null;
                            $scope.passnew = null;
                            $scope.pass_confirm = null;
                            $scope.checkchangpass = false;
                        });
                        $http.get(global.AuthHost + 'User/GetRole').then((rep:any)=> {
                            console.log(rep);
                            // var userdata = rep.data;
                            $scope.PhoneAccount = rep.data.Phone;
                            $scope.InfoAccount = rep.data.DisplayName;
                            $scope.user = {
                                displayname:rep.data.DisplayName,
                                phoneAccount:rep.data.Phone,
                                groupuser:rep.data.GroupUser,
                                showlevel:ConvertLevel(rep.data.Level),
                                username:rep.data.Username,
                            };

                        });
                        var passold = $cookieStore.get('password');
                        // $scope.updatepassFormAccept = () => {
                        //     if ($scope.passold === passold) {
                        //         if ($scope.passnew === $scope.pass_confirm) {
                        //             var req:any = {
                        //                 method: 'POST',
                        //                 url: `http://auth.adsun.vn/User/ChangePass?password=${$scope.pass_confirm}`
                        //             };
                        //             $http(req).success((data:any) => {
                        //                 //console.log(data);
                        //                 if (data.Status === 1) {
                        //                     bootbox.dialog({
                        //                         title: "Thông Báo",
                        //                         message: "Thay đổi mật khẩu thành công. Xin đăng nhập lại!",
                        //                         buttons: {
                        //                             confirm: {
                        //                                 label: "OK",
                        //                                 className: "btn-danger",
                        //                                 callback: () => {
                        //                                     var req:any = {
                        //                                         method: 'POST',
                        //                                         url: 'http://auth.adsun.vn/User/LogOut'
                        //                                     };
                        //                                     var status:any = {};
                        //                                     $http(req).success((data) => {
                        //                                         var status = data;
                        //                                     });
                        //                                     window.location.href = "/login.html";
                        //                                 }
                        //                             }
                        //                         }
                        //                     });
                        //
                        //                 }
                        //
                        //             });
                        //         }
                        //     }
                        // };
                        $scope.updateinfoFormAccept = ()=> {
                            if($scope.PhoneAccount !== undefined && $scope.InfoAccount !== undefined){
                                var req:any = {
                                    Phone: $scope.PhoneAccount,
                                    DisplayName: $scope.InfoAccount,
                                };
                                $http.put(this.global.AuthHost + 'User/ChangeInfo', req).then((rep:any)=> {
                                    if (rep.data.Status == 1) {
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật thông tin tài khoản thành công!!",

                                        });
                                        $scope.user.displayname = rep.config.data.DisplayName;
                                        $scope.user.phoneAccount = rep.config.data.Phone;
                                        $scope.$apply();
                                    } else {
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: "Cập nhật tài khoản không thành công!!!",

                                        });
                                    }
                                });

                            }else{
                                bootbox.dialog({
                                    title: "Thông Báo",
                                    message: "Vui lòng nhập đúng định dạng số điện thoại và thông tin hiển thị",

                                });
                            }


                        };


                        $scope.updatepassFormAccept = () => {
                            if ($scope.passold === passold) {
                                if ($scope.passnew === $scope.pass_confirm) {
                                    var req:any = {
                                        method: 'POST',
                                        url: `http://auth.adsun.vn/User/ChangePass?password=${$scope.pass_confirm}`
                                    };
                                    $http(req).success((data:any) => {
                                        //console.log(data);
                                        if (data.Status === 1) {
                                            bootbox.dialog({
                                                title: "Thông Báo",
                                                message: "Thay đổi mật khẩu thành công. Xin đăng nhập lại!",
                                                buttons: {
                                                    confirm: {
                                                        label: "Đồng ý",
                                                        className: "btn-success",
                                                        callback: () => {
                                                            var req:any = {
                                                                method: 'POST',
                                                                url: 'http://auth.adsun.vn/User/LogOut'
                                                            };
                                                            var status:any = {};
                                                            $http(req).success((data) => {
                                                                var status = data;
                                                            });
                                                            window.location.href = "/login.html";
                                                        }
                                                    }
                                                }
                                            });

                                        }else{
                                            bootbox.dialog({
                                                title: "Thông Báo",
                                                message: "Thay đổi mật khẩu không thành công. Vui lòng liên hệ với quản trị viên!!!",
                                                buttons: {
                                                    cancle: {
                                                        label: "Quay lại",
                                                        className: "btn-danger",
                                                    }
                                                }
                                            });
                                        }

                                    });
                                }else{
                                    bootbox.dialog({
                                        title: "Thông Báo",
                                        message: "Mật khẩu mới không trùng nhau. Vui lòng nhập lại!!!",
                                        buttons: {
                                            cancle: {
                                                label: "Quay lại",
                                                className: "btn-primary",
                                            }
                                        }
                                    });
                                }
                            }else{
                                bootbox.dialog({
                                    title: "Thông Báo",
                                    message: "Mật khẩu cũ không đúng. Vui lòng nhập lại!!!",
                                    buttons: {
                                        cancle: {
                                            label: "Quay lại",
                                            className: "btn-primary",
                                        }
                                    }
                                });
                            }
                        }

                    function ConvertLevel(level:number){
                            var result:string = '';
                            switch (level){
                                case 0:
                                    result = '<label class="label label-danger" style="font-size: 17px">Root</label>';
                                    break;
                                case 1:
                                    result = '<label class="label label-warning" style="font-size: 17px">Administrator</label>';
                                    break;
                                case 2:
                                    result = '<label class="label label-success" style="font-size: 17px">Quản trị công ty</label>';
                                    break;
                                case 3:
                                    result = '<label class="label label-info" style="font-size: 17px">Tài khoản thường</label>';
                                    break;

                            }
                            return result;
                        }

                    }
                }
                AdModule.controller('InfomationuserController', InfomationuserController);
            }
        }
    }
}