/**
 * Created by phamn on 04/10/2016.
 */
module App{
    export module Components{
        export module Generalmanagement{
            export module Infomationuser{
                export class InfomationuserRouterModel{
                    public static $inject=['sysRouter'];
                    constructor(sysRouter:Shared.IRouterHelper) {
                        sysRouter.configureStates({
                            name: 'infomationuser',
                            config: {
                                controller: Generalmanagement.Infomationuser.InfomationuserController,
                                templateUrl: '/components/generalmanagement/infomationuser/infomationuser.html',
                                url: '/cap-nhat-thong-tin-tai-khoan',
                                tag:{
                                    title:'Cập nhật thông tin tài khoản',
                                    icon:'maki-commerical-building',
                                    groupId:'general'
                                },
                                show: false
                            }
                        });
                    }
                }
                AdModule.run(InfomationuserRouterModel);
            }
        }
    }
}