/**
 * Created by phamn on 29/07/2016.
 */
declare var MobileDetect: MobileDetect;
var app = angular.module('Loginapp',['LocalStorageModule','ngCookies','ngSanitize']);
app.controller('loginCtrl',['$scope','$http','localStorageService','$cookieStore','$sce',function ($scope:any,$http:any,localStorageService:any,$cookieStore:any,$sce:any) {

    var url_dev:string = 'http://auth.adsun.vn/';
    var url = 'http://auth.adsun.vn/';
    console.log(window.location.host);
    var md = new MobileDetect(window.navigator.userAgent);

// code ban đầu
//     if(md.mobile()!=null&&md.tablet()==null && window.location !== 'http://m.' + window.location.host){
//         window.location.href='http://m.'+window.location.host +'/login.html';
//         return;
//     }

    if(localStorageService.cookie.get('username') == undefined){
        $scope.username = $cookieStore.get('username');
    }else{
        $scope.username = localStorageService.cookie.get('username');
    }


    if(localStorageService.cookie.get('password') == undefined){
        $scope.password = $cookieStore.get('password');
    }else{
        $scope.password = localStorageService.cookie.get('password');
    }

    if(localStorageService.cookie.get('remember') == undefined){
        $scope.remember = $cookieStore.get('remember');
    }else{
        $scope.remember = localStorageService.cookie.get('remember');
    }

    if(localStorageService.get('Token')!== null){
        $http.get(url +'Auth/CheckToken?token='+ $cookieStore.get('Token')).then(function (rep:any) {

            if(rep.data.Status == 1){
//                 if(md.mobile()!=null){
// //                        if(window.location == 'http://m.dinhvi.adsun.vn/'){
// //                            window.location.href='http://m.dinhvi.adsun.vn/index.html';
// //                            return;
// //                        }else if(window.location == 'http://m.hvt.adsun.vn/') {
// //                            window.location.href='http://m.hvt.adsun.vn/index.html';
// //                            return;
// //                        }
//                     window.location.href='http://m.'+ window.location.host + '/index.html';
//                     return;
//                 } else{
//                     window.location.href='http://'+ window.location.host + '/index.html';
//                 }
                window.location.href='http://'+ window.location.host + '/index.html';
            }
        });
    }
    $scope.login = function () {
        var posting = {username: $scope.username, pwd:$scope.password};
        console.log('hostname',window.location.hostname);
        $http.post(url + 'Auth/Login',posting).success(function (data:any, status:any, header:any, config:any) {
            console.log(data);
            $scope.token = data;
            if($scope.token.Status == 1){
                localStorageService.cookie.set('Token',$scope.token.Token,365);
                $cookieStore.put('Token',$scope.token.Token);
                $cookieStore.put('username',$scope.username);
                $cookieStore.put('password',$scope.password);
                if($scope.remember === true){
                    localStorageService.cookie.set('username',$scope.username,365);
                    localStorageService.cookie.set('password', $scope.password,365);
                    localStorageService.cookie.set('remember', $scope.remember,365);

                    // $cookieStore.put('username',$scope.username);
                    // $cookieStore.put('password', $scope.password);
                    // $cookieStore.put('remember', $scope.remember);

                } else{
                    localStorageService.cookie.remove('username');
                    localStorageService.cookie.remove('password');
                    localStorageService.cookie.remove('remember');

                    // $cookieStore.remove('username');
                    // $cookieStore.remove('password');
                    // $cookieStore.remove('remember');
                }
                window.location.href = "/index.html";
            }
            else{

                $('.login_error').css({"display": "block", "-webkit-animation": "fadeAnimation 5s"});
            }
        });


    };
    // nếu host không phải trang chính thì không đi get tin tức
    if(window.location.host === 'dinhvi.adsun.vn'){
        var x = setTimeout(()=>{
            $http.get(url_dev + 'api/Auth/Forward?url=' + 'http://adsun.vn/tin-tuc/tin-noi-bat-102/').then((rep:any)=>{
                var el = rep.data;
                var search = el.replace(/onerror="this.src=/gi,'');
                var found = $(search).find('.show-pro');
                found.find('.fix-footer').remove().end();
                found.find('.phantrang').remove().end();
                found.find('img').attr('src',(i:any, val:any)=>{
                    return "http://adsun.vn/" + val;
                });
                found.find('a').attr('href',(i:any, val:any)=>{
                    return "http://adsun.vn/" + val;
                });
                found.find('a').attr('target','_blank');
                $scope.dataweb = found[0].innerHTML;

            });
        },500);
    }

    setTimeout(()=>{
        if(sessionStorage.getItem('xemthongbao')=== null){
            GetDataThongbao();
        }
    },400);

    function GetDataThongbao(){
        $http.get(url + `Auth/GetThongBao`).then((rep:any)=>{
            console.log(rep);
            if(rep.data.Status == 1){
                $scope.listthongbao = rep.data.ThongBaos;
                var lent = rep.data.ThongBaos.length;
                $scope.thongbao = $sce.trustAsHtml(rep.data.ThongBaos[lent-1].NoiDung);
                $scope.tieude = rep.data.ThongBaos[lent-1].MessageTitle;
                $('#modalthongbao').modal('show');
            }
        });

    }
    $scope.readthongbao = (event:any) => {
        console.log(event);
        sessionStorage.setItem('xemthongbao',`${event.currentTarget.checked}`);
    };

}]);
