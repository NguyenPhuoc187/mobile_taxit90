/**
 * Created by Tuantv on 04/19/2016.
 */
module App{
    export module Shared{
        //export module LanguageService{
            export interface IMapLang{
                titleMenu:string;
                noteBoxCar:{
                    map_nameNote:string;
                    map_nameLostGps:string;
                    map_nameCarPause:string;
                    map_nameCarStop:string;
                    map_nameCarRun:string;
                    map_nameCarOutOfService:string;
                    map_nameCountCar:string;
                    map_nameCloseBoxWarning:string;
                    map_nameCarShow:string;
                    map_nameCar:string;
                };
                mapControl:{
                    map_control_nameListCar:string;
                    map_control_nameListZone:string;
                    map_control_nameListPoint:string;
                    map_control_nameListCarOverSpeed:string;
                    gridViewCar:{
                        bienSo:string;
                        speed:string;
                        status:string;
                    };
                    map_control_hide:string;
                    map_control_show:string;
                    gridViewPoint:{
                        namePoint:string;
                        radius:string;
                        count: string;
                    };
                    gridViewZone:{
                        nameZone:string;
                        speed:string;
                        count: string;
                    };
                };
                map_nameBtSelectCompany:string;
                map_nameTextBoxSearch:string;
                map_overSpeedWarning:string;
                infoWindow:{
                    titleName:string;
                    trip:string;
                    bs:string;
                    status:string;
                    speed:string;
                    km:string;
                    voltage:string;
                    ac:string;
                    overSpeed:string;
                    licence:string;
                    transport:string;
                    engine:string;
                    kmTrip:string;
                    flue:string;
                    gsm:string;
                    door:string;
                    temp:string;
                    stop:string;
                    dOpen:string;
                    overContinuous:string;
                    overDay:string;
                    continuous:string;
                    drivingDay:string;
                    name:string;
                    address:string;
                    lastUpdate:string;
                    onStatus:string;
                    offStatus:string;
                    openStatus:string;
                    closeStatus:string;
                    yesStatus:string;
                    noStatus:string;
                    messageAddressStatus:string;
                };
                contextMenu:{
                    latitude:string;
                    longitude:string;
                    point:string;
                    findCar:string;
                    viewPoint:string;
                    viewAddress:string;
                    calculate:string;
                    cancelCalculate:string;
                    addZone:string;
                    addPoint:string;
                    cancelZone:string;
                    comeBack:string;
                    clear:string;
                };

            }
            export interface IbgtReportLang{
                titleMenuMain: string;
                allLogOfCarView: {
                    titleMenu:string;
                };
                allLogOfDriverView:{
                    titleMenu:string;
                };
                invalidSpeed:{
                    titleMenu: string;
                };
                overTimeByCar:{
                    titleMenu: string;
                };
                overTimeByDriver:{
                    titleMenu: string;
                };
                speedOfCar:{
                    titleMenu: string;
                };
                stopOrRun:{
                    titleMenu: string;
                };
                distance:{
                    titleMenu:string;
                };
            }
            export interface IinputDataLang{
                titleMenuMain: string;
                carsType: {
                    titleMenu: string;
                };
                company:{
                    titleMenu:string;
                };
                dmBaotri:{
                    titleMenu:string;
                };
                listCar:{
                    titleMenu:string;
                };
                listDriver:{
                    titleMenu:string;
                };
                listGroup:{
                    titleMenu:string;
                };
                tableFlue:{
                    titleMenu:string;
                };
            }
            export interface ImanagementLang{
                titleMenuMain: string;
                createAccount:{
                    titleMenu:string;
                    name:string;
                    userName:string;
                    pass:string;
                    check:string;
                    confirmPass:string;
                    typeAccount:string;
                    company:string;
                    group:string;
                    permission:string;
                    create:string;
                    titleForm:string;
                    car:string;
                };
                listAccount:{
                    titleMenu:string;
                };
                programing:{
                    titleMenu:string;
                };
                registerController:{
                    titleMenu: string;
                };
                inputManual:{
                    titleMenu: string;
                }
            }
            export interface IreportLang{
                titleMenuMain: string;
                area:{
                    titleMenu: string;
                };
                camera:{
                    titleMenu:  string;
                    lbTime: string;
                    ttCamera: string;
                    timeCamera: string;
                };
                chart:{
                    titleMenuMain: string;
                    flue:{
                        titleMenu: string;
                    };
                    temperature:{
                        titleMenu: string;
                    }
                };
                distance:{
                    titleMenu: string;
                };
                trip:{
                    titleMenu: string;
                    infoCar: string;
                    time: string;
                    location:  string;
                    speed: string;
                    engine: string;
                    door:  string;
                    voltage: string;
                    gsm: string;
                    temp: string;
                    tripDistanceGsm: string;
                    tripDistanceClock: string;
                    totalDistanceGsm: string;
                    totalDistanceClock: string;
                    recode: string;
                    flue: string;
                    onStatus: string;
                    offStatus: string;
                    openStatus: string;
                    closeStatus: string;
                    yesStatus: string;
                    noStatus: string;
                    pointStart: string;
                    pointEnd: string;
                };
                sim:{
                    titleMenu:  string;
                },
                sessionCar:{
                    titleMenu:  string;
                };
                maintenance:{
                    titleMenuMain: string;
                    infoBt:{
                        titleMenu: string;
                    };
                    lsBaoTri:{
                        titleMenu: string;
                    },
                    infoInsure:{
                        titleMenu: string;
                    };
                    lsInsure:{
                        titleMenu: string;
                    };
                    infoTest:{
                        titleMenu: string;
                    };
                    lsTest:{
                        titleMenu: string;
                    };
                };
                log:{
                    titleMenuMain: string;
                    logAccount:{
                        titleMenu: string;
                    };
                    logCar:{
                        titleMenu: string;
                    }
                };
                event:{
                    titleMenu: string;
                };
            }
            export interface IhelpLang{
                titleMenuMain: string;
                use: {
                    titleMenu: string;
                };
                cost: {
                    titleMenu: string;
                };
            }
            export interface IAllMessageLang {
                warnCompany: string;
                warnGroup:string;
                warnTime:string;
                warnCar:string;
                warnLoadData:string;
                warnLoadPicture:string;
            }
            export interface IgridLgLang{
                search: string;
                noDataAdd:string;
                noData:string;
                countRow:string;
                company:string;
                group:string;
                car:string;
                titleTimeStart:string;
                titleTimeEnd:string;
                viewData:string;
                titleDriver:string;
                excel:string;
                mesDelete:string;
                resultAgree:string;
                resultCancel:string;
                mesWarning:string;
                titleTime:string;
                titlePhone:string,
                titleIdCar:string,
                titleBienSo:string,
                titleVin:string,
                titleSerial:string,
                titleNumberSeat:string,
                titleNote:string,
                titleAdd:string,
                titleContent:string,
                titleModel:string,
                carsType: {
                    titleMenu: string;
                };
            }
            export interface ItopNavLang {
                titleNameTopNav: string;
                logOut:  string;
                changePass: string;
                welcome: string;
            }
            export interface ILanguageService{
                //this.changeLanguage(a,b):void;
                map():IMapLang;
                bgtReport():IbgtReportLang;
                inputData():IinputDataLang;
                management():ImanagementLang;
                report():IreportLang;
                help(): IhelpLang;
                gridLg():IgridLgLang;
                AllMessage():IAllMessageLang;
                topNav():ItopNavLang;
            }

            export class LanguageService implements ILanguageService{
                private language:string="VN";
                private changeLanguage(a:string,b:string):string{
                    switch (this.language){
                        case "EN": return a;
                        case "VN":return b;
                        default : return b;
                    }
               }
                public showLoading(isClose:boolean,status:any){
                    if (isClose === false) {
                        //"<img src='http://www.shaadimagic.com/assets/image/loader.gif' style='height:110px;'/>"
                        var html = "<div id='veil' style='z-index: 5000'></div><div id='loading'>" +
                                "<button class='btn btn-lg btn-loading-map' id='global-loading'>" +
                                "<i class='icon-spinner3 spinner'></i> Đang tải dữ liệu..." +
                                "</button>" +
                            "</div>";
                        // html += "</div>";
                        // $(".btn-loading").remove();
                        $(".page-container").append(html);
                    }else{
                       $('#veil').remove();
                       $('#loading').remove();
                    }
                /*nếu isClose = false*/
                /*nếu dang load ding thì thôi*/
                /*chưa load thì load lên ( add 1 element vào thẻ body)*/
                /*nếu isClose == true*/
                /*ản loading (xoá element)*/
                }
                //trang chủ map
                public map():IMapLang{
                return {
                    titleMenu:this.changeLanguage("Map","Bản đồ"),
                    noteBoxCar:{
                        map_nameNote:this.changeLanguage("Note","Ghi chú"),
                        map_nameLostGps:this.changeLanguage("Lost GPS","Mất GPS"),
                        map_nameCarPause:this.changeLanguage("Pause","Dừng"),
                        map_nameCarStop:this.changeLanguage("Stopping","Đỗ"),
                        map_nameCarRun:this.changeLanguage("Moving","Đang chạy"),
                        map_nameCarOutOfService:this.changeLanguage("Out of service","Mất Liên Lạc"),
                        map_nameCountCar:this.changeLanguage("Cars Total","Tổng xe"),
                        map_nameCloseBoxWarning:this.changeLanguage("Close warning","Tắt cảnh báo"),
                        map_nameCarShow:this.changeLanguage("Show Cars","Xe hiển thị"),
                        map_nameCar:this.changeLanguage("car","xe")
                    },
                    mapControl:{
                        map_control_nameListCar:this.changeLanguage("Car List","Danh sách xe"),
                        map_control_nameListZone:this.changeLanguage("Zone List","Danh sách vùng"),
                        map_control_nameListPoint:this.changeLanguage("Point List","Danh sách điểm"),
                        map_control_nameListCarOverSpeed:this.changeLanguage("OverSpeed Car list","Danh sách xe quá tốc độ"),
                        gridViewCar:{
                            bienSo:this.changeLanguage("license plate","Biển số xe"),
                            speed:this.changeLanguage("Speed","Vận tốc"),
                            status:this.changeLanguage("Status","Tt")
                        },
                        map_control_hide:this.changeLanguage("Hide","Ẩn"),
                        map_control_show:this.changeLanguage("Show","Hiển thị"),
                        gridViewPoint:{
                            namePoint:this.changeLanguage("Point Name","Tên Điểm"),
                            radius:this.changeLanguage("Radius","Bán kính"),
                            count: this.changeLanguage("Total Car","Sl xe")
                        },
                        gridViewZone:{
                            nameZone:this.changeLanguage("Zone Name","Tên Vùng"),
                            speed:this.changeLanguage("Max speed","Tốc độ qđ"),
                            count: this.changeLanguage("Total Car","Sl xe")
                        }
                    },
                    map_nameBtSelectCompany:this.changeLanguage("Select company","Chọn công ty"),
                    map_nameTextBoxSearch:this.changeLanguage("input address to find...","nhập địa chỉ cần tìm"),
                    map_overSpeedWarning:this.changeLanguage("Overspeed car warning", "Cảnh báo xe quá tốc độ"),
                    infoWindow:{
                        titleName:this.changeLanguage("Car Information","Thông tin xe"),
                        trip:this.changeLanguage("Last journey","Hành trình xe gần nhất"),
                        bs:this.changeLanguage("Pl No.","Bs"),
                        status:this.changeLanguage("Status","Trạng thái"),
                        speed:this.changeLanguage("Speed","Tốc độ"),
                        km:this.changeLanguage("Km(day)","Km(ngày)"),
                        voltage:this.changeLanguage("Voltage","Điện áp"),
                        ac:this.changeLanguage("A.C.","Máy lạnh"),
                        overSpeed:this.changeLanguage("Over speed","Quá tốc độ"),
                        licence:this.changeLanguage("Licence","GPLX"),
                        transport:this.changeLanguage("Transport Department","Sở GTVT"),
                        engine:this.changeLanguage("Engine","máy"),
                        kmTrip:this.changeLanguage("Km(trip)","Km cuốc"),
                        flue:this.changeLanguage("Flue","Mức xăng"),
                        gsm:this.changeLanguage("GSM","Mức sóng"),
                        door:this.changeLanguage("Door","Cửa xe"),
                        temp:this.changeLanguage("Temp","Nhiệt độ"),
                        stop:this.changeLanguage("Stops","Dừng đỗ"),
                        dOpen:this.changeLanguage("Door opens","SL mở cửa"),
                        overContinuous:this.changeLanguage("Over continuous driving","Quá tg liên tục"),
                        overDay:this.changeLanguage("Over day driving","Quá tg trong ngày"),
                        continuous:this.changeLanguage("Over continuous driving","Thời gian lx liên tục"),
                        drivingDay:this.changeLanguage("Over day driving","Thời gian lx trong ngày"),
                        name:this.changeLanguage("Driver Name","Tên tài xế"),
                        address:this.changeLanguage("Address","Khu vực"),
                        lastUpdate:this.changeLanguage("Last Update","Cập nhật"),
                        onStatus:this.changeLanguage("On","Mở"),
                        offStatus:this.changeLanguage("Off","Tắt"),
                        openStatus:this.changeLanguage("Open","Mở"),
                        closeStatus:this.changeLanguage("Close","Đóng"),
                        yesStatus:this.changeLanguage("Ok","có"),
                        noStatus:this.changeLanguage("No","Không"),
                        messageAddressStatus:this.changeLanguage("Could not find Address","Không tìm thấy địa chỉ")
                    },
                    contextMenu:{
                        latitude:this.changeLanguage("Latitude","Vĩ độ"),
                        longitude:this.changeLanguage("Longitude","Kinh độ"),
                        point:this.changeLanguage("Position","Toạ độ hiện tại"),
                        findCar:this.changeLanguage("Search car","Tìm xe"),
                        viewPoint:this.changeLanguage("View current position","Xem toạ độ"),
                        viewAddress:this.changeLanguage("View Address","Xem địa chỉ"),
                        calculate:this.changeLanguage("Calculate distance","Tính khoảng cách"),
                        cancelCalculate:this.changeLanguage("Cancel calculate distance","Tính khoảng cách"),
                        addZone:this.changeLanguage("Add zone","Thêm vùng"),
                        addPoint:this.changeLanguage("Add point","thêm điểm"),
                        cancelZone:this.changeLanguage("Cancel add zone","Huỷ thêm vùng"),
                        comeBack:this.changeLanguage("Come back center","Quay về trung tâm"),
                        clear:this.changeLanguage("Clear","Làm sạch bản đồ")
                    }
                }}
                //báo cáo bộ giao thông vận tải
                public bgtReport():IbgtReportLang{
                return {
                    titleMenuMain: this.changeLanguage("QCVN31's reports","Báo Cáo BGTVT"),
                    allLogOfCarView: {
                        titleMenu: this.changeLanguage("Synthetic report of car", "Báo cáo tổng hợp theo xe")
                    },
                    allLogOfDriverView:{
                        titleMenu:this.changeLanguage("Synthetic report of Driver","Báo cáo tổng hợp theo tài xế")
                    },
                    invalidSpeed:{
                        titleMenu: this.changeLanguage("OverSpeed report","Báo cáo quá tốc độ")
                    },
                    overTimeByCar:{
                        titleMenu: this.changeLanguage("Driving time report of car","Thời gian lái xe theo xe")
                    },
                    overTimeByDriver:{
                        titleMenu: this.changeLanguage("Driving time report of Driver","Thời gian lái xe theo tài xế")
                    },
                    speedOfCar:{
                        titleMenu: this.changeLanguage("Car's Speed report","Báo cáo vận tốc theo xe")
                    },
                    stopOrRun:{
                        titleMenu: this.changeLanguage("Parking/stopping report","Báo cáo dừng đỗ")
                    },
                    distance:{
                        titleMenu: this.changeLanguage("Distance report","Báo cáo Quãng đường")
                    }
                }
            }
                //phần nhập liệu:
                public inputData():IinputDataLang{
                return{
                    titleMenuMain: this.changeLanguage("Input Data","Nhập liệu"),
                    carsType: {
                        titleMenu: this.changeLanguage("Car Type","Loại xe")
                    },
                    company:{
                        titleMenu:this.changeLanguage("Company info","Thông tin xe")
                    },
                    dmBaotri:{
                        titleMenu:this.changeLanguage("Assess maintenance","Định mức bảo trì")
                    },
                    listCar:{
                        titleMenu:this.changeLanguage("Cars List","Danh sách xe")
                    },
                    listDriver:{
                        titleMenu:this.changeLanguage("Drivers List","Danh sách tài xế")
                    },
                    listGroup:{
                        titleMenu:this.changeLanguage("Groups List","Danh sách Đội xe")
                    },
                    tableFlue:{
                        titleMenu:this.changeLanguage("Table Flue","Bảng mẫu mức xăng")
                    }
                }
            }
                // phần quản lý
                public management():ImanagementLang{
                return{
                    titleMenuMain: this.changeLanguage("Manage","Quản Trị"),
                    createAccount:{
                        titleMenu:this.changeLanguage("Create Account","Tạo tài khoản"),
                        name:this.changeLanguage("Name","Tên người dùng"),
                        userName:this.changeLanguage("Username","Tên đăng nhập"),
                        pass:this.changeLanguage("Password","Mật khẩu"),
                        check:this.changeLanguage("Check","Kiểm tra"),
                        confirmPass:this.changeLanguage("Confirm your password","Mật khẩu"),
                        typeAccount:this.changeLanguage("Account Types","Loại Tài Khoản"),
                        company:this.changeLanguage("Company","Công ty quản lý"),
                        group:this.changeLanguage("Group","Đội xe Quản lý"),
                        permission:this.changeLanguage("Permissions","Quyền hạn truy cập"),
                        create:this.changeLanguage("Create Account","Tạo tài khoản"),
                        titleForm:this.changeLanguage("Sign up","Tạo tài khoản đăng nhập"),
                        car:this.changeLanguage("Car","Xe quản lý")
                    },
                    listAccount:{
                        titleMenu: this.changeLanguage("Account List","Danh sách tài khoản")
                    },
                    programing:{
                        titleMenu: this.changeLanguage("programing","Lập trình")
                    },
                    registerController:{
                        titleMenu: this.changeLanguage("Manage System Function ","Quản lý chức năng hệ thống")
                    },
                    inputManual:{
                        titleMenu: this.changeLanguage("Create Manual","Nhập liệu hướng dẫn sử dụng")
                    }

                }
            }
                //Phần báo cáo
                public report():IreportLang{
                return{
                    titleMenuMain: this.changeLanguage("Supervision","Theo dõi - Quản lý"),
                    area:{
                        titleMenu: this.changeLanguage("Zones/Points","Quản lý vùng, điểm")
                    },
                    camera:{
                        titleMenu: this.changeLanguage("Camera Pictures","Theo dõi camera"),
                        lbTime:this.changeLanguage("Time","Thời gian"),
                        ttCamera:this.changeLanguage("Order Camera","Thứ tự camera"),
                        timeCamera:this.changeLanguage("Time","Thời gian chụp")
                    },
                    chart:{
                        titleMenuMain:this.changeLanguage("Charts","Đồ thị"),
                        flue:{
                            titleMenu: this.changeLanguage("Flue chart","Đồ thị nhiên liệu")
                        },
                        temperature:{
                            titleMenu: this.changeLanguage("Temperature chart","Đồ thị nhiệt độ")
                        }
                    },
                    distance:{
                        titleMenu:this.changeLanguage("trips distance report", "báo cáo quãng đường")
                    },
                    trip:{
                        titleMenu:this.changeLanguage("journey tracking","Hành trình"),
                        infoCar:this.changeLanguage("Info car","Thông tin xe"),
                        time:this.changeLanguage("Time","Thời gian"),
                        location: this.changeLanguage("Location","Toạ độ"),
                        speed:this.changeLanguage("Speed","Vận tốc"),
                        engine:this.changeLanguage("Engine Status","Trạng thái máy"),
                        door: this.changeLanguage("Door Status","Trạng thái cửa"),
                        voltage:this.changeLanguage("Voltage","Điện áp"),
                        gsm:this.changeLanguage("GSM","Mức sóng"),
                        temp:this.changeLanguage("Temp","Nhiệt độ"),
                        tripDistanceGsm:this.changeLanguage("Distance Trip(GSM)","Đi được qđ(GSM)"),
                        tripDistanceClock:this.changeLanguage("Distance Trip(clock)","Đi được qđ(xung)"),
                        totalDistanceGsm:this.changeLanguage("Total distance(GSM)","Quảng đường(GSM)"),
                        totalDistanceClock:this.changeLanguage("Total distance(clock)","Quảng đường(xung)"),
                        recode:this.changeLanguage("Total recode","Tổng recode"),
                        flue:this.changeLanguage("Flue","Mức xăng"),
                        onStatus:this.changeLanguage("On","Mở"),
                        offStatus:this.changeLanguage("Off","Tắt"),
                        openStatus:this.changeLanguage("Open","Mở"),
                        closeStatus:this.changeLanguage("Close","Đóng"),
                        yesStatus:this.changeLanguage("Ok","có"),
                        noStatus:this.changeLanguage("No","Không"),
                        pointStart:this.changeLanguage("Start","Điểm đầu"),
                        pointEnd:this.changeLanguage("End","Điểm cuối")
                    },
                    sim:{
                        titleMenu: this.changeLanguage("sim balance","Tài khoản sim")
                    },
                    sessionCar:{
                        titleMenu: this.changeLanguage("Short trips report"," Báo cáo cuốc xe")
                    },
                    maintenance:{
                        titleMenuMain:this.changeLanguage("Monitor car","Hoạt động bảo dưỡng"),
                        infoBt:{
                            titleMenu:this.changeLanguage("Info maintenance","thông tin bảo trì")
                        },
                        lsBaoTri:{
                            titleMenu:this.changeLanguage("Log maintenance","Lịch sử bảo trì")
                        },
                        infoInsure:{
                            titleMenu:this.changeLanguage("Info insurance","thông tin bảo hiểm")
                        },
                        lsInsure:{
                            titleMenu:this.changeLanguage("Log insurance","Lịch sử bảo hiểm")
                        },
                        infoTest:{
                            titleMenu:this.changeLanguage("Info validation","thông tin kiểm định")
                        },
                        lsTest:{
                            titleMenu:this.changeLanguage("Log validation","Lịch sử kiểm định")
                        }
                    },
                    log:{
                        titleMenuMain:this.changeLanguage("logs view","Thống kê log"),
                        logAccount:{
                            titleMenu:this.changeLanguage("account log", "theo dõi log tài khoản")
                        },
                        logCar:{
                            titleMenu:this.changeLanguage("Cars Log","Theo dõi log xe")
                        }
                    },
                    event:{
                        titleMenu:this.changeLanguage("vehicle events","Theo dõi sự kiện")
                    }
                }
            }
                //phần hướng dẫn
                public help(): IhelpLang {
                    return {
                        titleMenuMain: this.changeLanguage("help", "Hướng dẫn"),
                        use: {
                            titleMenu: this.changeLanguage("usage", "Hướng dẫn sử dụng")
                        },
                        cost: {
                            titleMenu: this.changeLanguage("terms of service", "Quy định đóng phí")
                        }
                    }
                }
                public gridLg():IgridLgLang {
                    return {
                        search: this.changeLanguage("Search...", "Tìm kiếm..."),
                        noDataAdd: this.changeLanguage("No Data (Click here to add data)", "KHÔNG CÓ DỮ LIỆU (nhấp vào đây để thêm dữ liệu)"),
                        noData: this.changeLanguage("No Data", "KHÔNG CÓ DỮ LIỆU"),
                        countRow: this.changeLanguage("The number of rows", "Số dòng hiển thị"),
                        company: this.changeLanguage("Company", "Công ty"),
                        group: this.changeLanguage("Group", "Đội xe"),
                        car: this.changeLanguage("Cars", "Xe"),
                        titleTimeStart: this.changeLanguage("Start time", "Thời gian bắt đầu"),
                        titleTimeEnd: this.changeLanguage("End time", "Thời gian kết thúc"),
                        viewData: this.changeLanguage("View", "Xem"),
                        titleDriver: this.changeLanguage("Drivers", "Tài xế"),
                        excel: this.changeLanguage("Export excel", "Xuất excel"),
                        mesDelete: this.changeLanguage("Are you really delete?", "Bạn có thực sự muốn xoá?"),
                        resultAgree: this.changeLanguage("Agree", "Đồng ý"),
                        resultCancel: this.changeLanguage("Cancel", "Huỷ"),
                        mesWarning: this.changeLanguage("Warning", "Cảnh báo"),
                        titleTime: this.changeLanguage("Time", "Thời gian:"),
                        titlePhone: this.changeLanguage("Phone number", "Số điện thoại"),
                        titleIdCar: this.changeLanguage("Id Car", "Số hiệu xe"),
                        titleBienSo:this.changeLanguage("license plate","Biển số xe"),
                        titleVin:this.changeLanguage("Vin number","Số Vin"),
                        titleSerial:this.changeLanguage("Serial number","Số serial"),
                        titleNumberSeat:this.changeLanguage("Count Seat","Số Ghế"),
                        titleNote:this.changeLanguage("Note","Ghi chú"),
                        titleAdd:this.changeLanguage("Add","Thêm"),
                        titleContent: this.changeLanguage("Contain","Nội dung"),
                        titleModel: this.changeLanguage("Model name","Tên Model"),
                        carsType: {
                            titleMenu: this.changeLanguage("Car Type","Loại xe")
                        }

                    }
                }
                public AllMessage():IAllMessageLang {
                    return {
                        warnCompany: this.changeLanguage("Please select company...", "Vui lòng chọn công ty cần xem"),
                        warnGroup: this.changeLanguage("Please select Car Group...", "Vui lòng chọn đội xe"),
                        warnTime: this.changeLanguage("Please select Time", "Vui lòng chọn Thời gian cần xem"),
                        warnCar: this.changeLanguage("Please select Car...", "Vui lòng chọn xe...."),
                        warnLoadData: this.changeLanguage("Waiting load data....", "Vui lòng đợi load thông tin"),
                        warnLoadPicture: this.changeLanguage("Waiting load picture...", "Vui lòng đợi load hình ảnh..."),

                    }
                }
                public topNav(): ItopNavLang{
                return {
                    titleNameTopNav: this.changeLanguage("Service information","Thông Tin khách hàng"),
                    logOut: this.changeLanguage("Log out","Đăng xuất"),
                    changePass: this.changeLanguage("Change Password","Đổi mật khẩu"),
                    welcome: this.changeLanguage("Welcome","Chào")
                }
            }
                constructor(){
                    //đổi ngôn ngữ
                   
                    //trang chủ





                }
            }


        //}
        AdModule.service('language',LanguageService);
    }
}