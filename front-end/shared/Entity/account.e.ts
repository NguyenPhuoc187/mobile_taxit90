/**
 * Created by phamn on 25/04/2016.
 */
module App{
    export module Shared{
        export module Entity{
            export interface IRole{
                Name:string,
                Description:string,
                Access:number
            }
            export interface IAccount {
                Level:number,
                Roles:Array<IRole>,
                Username:string,
                Avatar?:string,
                GroupUser:string,
                DisplayName?:string,
                Phone ?: string,
                Theme?: number
            }
        }
    }
}