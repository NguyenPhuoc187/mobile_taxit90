/**
 * Created by user on 07/20/2016.
 */
///<reference path='GpsLocation.ts'/>
module App{
    export module Shared{
        export module Entity{
            export interface ICompany{
                Id:number;
                Name:string;
                AddressCompany?:string;
                Description?:string;
                CreateTime?:string;
                DataCenterId?:number;
                Lat?:number;
                Lng?:number;
                Address?:string;
                DbId?:number;
                Display?:string;
                CompanyType?:number;
            }
        }
    }
}