/**
 * Created by user on 07/21/2016.
 */
module App{
    export module Shared{
        export module Entity{
            export interface IDevice{
                Id?:number;
                CompanyId?:number;
                GroupId?:number;
                BgtTranport?:boolean;
                Bs?:string;
                CreateTime?:string;
                EndTime?:string;
                FuelPer100Km?:string;
                GroupName?:string;
                LimitSpeed?:number;
                ModelName?:string;
                MoneyTime?:string;
                PhoneOfDevice?:string;
                Seat?:number;
                Serial?:string;
                Sgtvt?:string;
                SimNgoai?:boolean;
                Type?:string;
                TypeValue?:number;
                Version?:string;
                VinSerial?:string;
                Display?:string;
                OwnerPhone?:string;
                DeviceNote?:string;
                IsNhienLieu?: boolean;
                IsCamera?: boolean;
                NotView?: boolean;
                DisplayId?:string;
                ThoiGianPhatGanNhat?:string;


            }
        }
    }
}