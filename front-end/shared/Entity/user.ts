/**
 * Created by phamn on 09/08/2016.
 */
module App{
    export module Shared{
        export module Entity{
            export interface IUser {
                Username: string,
                Pwd: string,
                IsBlock: boolean,
                GroupUserId: string,
                CompanyIds: Array<number>,
                DeviceIds: Array<any>,
                Level: number,
                DisplayName: string,
                Phone: string,
            }
        }
    }
}