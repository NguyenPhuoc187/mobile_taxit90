/**
 * Created by phamn on 03/11/2016.
 */
module App{
    export module Shared{
        export module Entity{
            export interface IBangGiaTuyen{
                Id: number;
                ModelName?: string;
                BeginLocation?: {
                    Lat:number,
                    Lng:number,
                    Address: string
                },
                EndLocation?: {
                    Lat: number,
                    Lng: number,
                    Address: string
                },
                TimeRun?: string;
                KmRun?: number;
                MoneyRun?: number;
                MoneyEveryKm?:number;
                MoneyEveryTime?: number;
                CompanyId?: number;
                CompanyName?: string;
                DeviceGroup?: number;
                GroupName?: string;
                BeginAddressLocation?:string;
                EndAddressLocation?:string;
            }
        }
    }
}