/**
 * Created by phamn on 25/07/2016.
 */
module App{
    export module Shared{
        export module Entity{
            export interface IDriver{
                Id?: number,
                Name?: string,
                Gplx?: string,
                Mnv?: string,
                CompanyId?: number,
                GroupId?: number,
                Born?: number,
                Cmnd?: string,
                Address?: string,
                Male?: boolean,
                CreateDateOfGplx?: string,
                EndDateOfGplx?: string,
                AddressOfGplx?: string,
                Phone1?: string,
                Phone2?: string,
                Bs?: string,
                UserId?: string,
                DisplayName:string,
            }
        }
    }
}