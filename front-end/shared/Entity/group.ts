/**
 * Created by user on 07/20/2016.
 */
module App{
    export module Shared{
        export module Entity{
            export interface IGroup{
                Id:number;
                Name:string;
                CompanyId?:number;
            }
        }
    }
}