/**
 * Created by phamn on 27/07/2016.
 */

module App{
    export module Shared{
        export module Entity{
            export interface IModelCar{
                Name: string,
                Sheat: number,
                Xilanh: number,
                KmDaoLop: number,
                KmThayVo: number,
                KmThayNhot: number,
                KmThayLocDau: number,
                KmThayLocGio: number,
                KmThayLocNhot: number,
                TypeValue: number
            }
        }
    }
}