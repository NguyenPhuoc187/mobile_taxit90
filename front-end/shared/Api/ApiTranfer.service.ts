/**
 * Created by user on 07/20/2016.
 */
    ///<reference path="../Entity/Company.ts"/>
module App{
        export module Shared{
            export module Api{

                export interface IApi{
                    GetAllCompany():ng.IPromise<Array<App.Shared.Entity.ICompany>>;
                    GetAllCompanyByType():ng.IPromise<Array<App.Shared.Entity.ICompany>>;

                    GetAllCompanyById(id:number):ng.IPromise<App.Shared.Entity.ICompany>;

                    GetAllGroupByCompanyId(id:number):ng.IPromise<Array<App.Shared.Entity.IGroup>>;

                    GetAllDeviceByCompanyId(id:number):ng.IPromise<Array<App.Shared.Entity.IDevice>>;
                    GetAllDeviceByGroupId(companyId:number,groupId:number):ng.IPromise<Array<App.Shared.Entity.IDevice>>;

                    GetAllDriverByCompany(idCompany:number):ng.IPromise<Array<App.Shared.Entity.IDriver>>;
                    GetAllModelCar():ng.IPromise<Array<App.Shared.Entity.IModelCar>>;

                    GetDeviceStartById(idCompany:number):ng.IPromise<Array<any>>;
                    GetDeviceByCompanyKhachChuyen(idCompany:number):ng.IPromise<Array<any>>;

                    GetBangGiaTuyen(CompanyId:number,groupId:number, modelName:string):ng.IPromise<Array<App.Shared.Entity.IBangGiaTuyen>>;

            //#region Enterprise Report
                    GetGeneralCompanyReportBySerial(serials:Array<string>,idCompany:number,begin:string,end:string):ng.IPromise<Array<any>>;
                    //báo cáo hành trình
                    GetDeviceTripBySerial(idCompany:number,serial:string,begin: string, end:string):ng.IPromise<Array<any>>;
                /*Báo cáo cuốc xe
                ======================*/
                    //Báo cáo cuốc xe theo serial
                    GetDeviceSessionLogBySerial(idCompany:number, serial:string, begin:string, end:string):ng.IPromise<Array<any>>;
                    //Báo cáo cuốc xe theo groupid
                    GetDeviceSessionLogByGroupId(idCompany:number, groupId:number, begin:string, end:string):ng.IPromise<Array<any>>;
                    //Báo cáo cuốc xe theo công ty
                    GetDeviceSessionLog(idCompany:number, begin:string, end:string):ng.IPromise<Array<any>>;
                /*=====================
                * end Báo cáo cuốc xe*/

                /*Theo dõi vận tốc
                * ===================*/
                    GetSpeedDeivcesTripBySerial(idCompany:number, serial:string, begin:string, end:string):ng.IPromise<Array<any>>;
                /*=====================
                 * end Báo cáo cuốc xe*/

                /*thống kê mất tín hiệu*/
                    GetLogManagerDeviceTraceLogBySerial(idCompany:number,begin:string,end:string,serial:string):ng.IPromise<Array<any>>;
                /*chi tiết mất tín hiệu
                * =======================*/
                    // Theo công ty
                    GetLogManagerDeviceTraceSheetByCompany(idCompany:number,begin:string,end:string):ng.IPromise<Array<any>>;
                    // Theo đội xe
                    GetLogManagerDeviceTraceSheetByGroup(idCompany:number,groupId:number, begin:string,end:string):ng.IPromise<Array<any>>;
                    // Theo Serial
                    GetLogManagerDeviceTraceSheetBySerial(idCompany:number,begin:string,end:string, serial:Array<string>):ng.IPromise<Array<any>>;
                /*Theo dõi camera*/
                    GetLogManagerCameraLogBySerial(idCompany:number,serial:string, begin:string,end:string):ng.IPromise<Array<any>>;
                /*Biểu đồ nhiên liệu*/
                    //Theo serial
                    PostLogManagerRefuelLogBySerial(idCompany:number,seriallist:Array<string>, begin:string, end:string):ng.IPromise<Array<any>>;
                    //Theo công ty
                    GetLogManagerRefuelLogByCompany(idCompany:number, begin:string, end:string):ng.IPromise<Array<any>>;
                    // Theo đội xe
                    GetLogManagerRefuelLogByGroupId(idCompany:number, groupId:number, begin:string, end:string):ng.IPromise<Array<any>>;
            //#endregion

            //# báo cáo QCVN31
                /*Báo cáo quá tốc độ*/
                    //Theo công ty
                    GetOverSpeedLogByCompany(idCompany:number,begin:string, end:string):ng.IPromise<Array<any>>;
                    //Theo đội xe
                    GetOverSpeedLogByGroup(idCompany:number,groupId:number, begin:string, end:string):ng.IPromise<Array<any>>;
                    //Theo xe
                    GetOverSpeedLogBySerial(idCompany:number, begin:string, end:string,serial:Array<string>):ng.IPromise<Array<any>>;
                /*Báo cáo Dừng đỗ*/
                    //Theo xe
                    GetDevicePauseBySerial(idCompany:number, serial:string, begin:string, end:string):ng.IPromise<Array<any>>;
                /*Báo cáo tốc độ xe*/
                    GetDeviceSpeedBySerial(idCompany:number, serial:string, begin:string, end:string):ng.IPromise<Array<any>>;
                /*Báo cáo thời gian lái xe */
                    GetDriverSessionByCompany(idCompany:number, begin:string, end:string):ng.IPromise<Array<any>>;
                /*Báo cáo tổng hợp theo xe*/
                    PostGeneralReportBySerial(idCompany:number, begin:string, end:string, listserial:Array<any>):ng.IPromise<Array<any>>;
                /*Báo cáo tổng hợp theo tài xế*/
                    PostGeneralReportByDriver(idCompany:number, begin:string, end:string, driverIdList:Array<any>):ng.IPromise<Array<any>>;

            //#Danh sách vùng điểm
                /*Vùng*/
                    GetZoneByCompany(idCompany:number):ng.IPromise<Array<any>>;
                /*Điểm*/
                    GetPOintByCompany(idCompany:number):ng.IPromise<Array<any>>;
            //#Quản lý xe
                /*Cập nhật xe*/
                    PutUpdateDeviceList(idCompany:number, deviceslist:Array<any>):ng.IPromise<Array<any>>;
                /*Thông tin sim*/
                    //Theo công ty
                    GetSimInfoByCompany(idCompany:number):ng.IPromise<Array<any>>;
                    //Theo đội xe
                    GetSimInfoByGroupId(idCompany:number, groupId:number):ng.IPromise<Array<any>>;
                    //Theo Serial
                    PostSimInfoBySerial(idCompany:number, seriallist:Array<any>):ng.IPromise<Array<any>>;

            //# Bản đồ
                /*Danh sách xe hết hạn*/
                    GetDevicesByExpireDate(idCompany:number, daynumber:number):ng.IPromise<Array<any>>;
            //# Quản lý ADSUN
                /*Lập trình thiết bị*/
                    GetDeviceSetupInfo(idCompany:number, serial:string):ng.IPromise<Array<any>>;
                    PostSetupDevices(idCompany:number, serial:string, setup:any):ng.IPromise<string>;
                    PostSetupDeviceOnCompany(idCompany:number, seriallist:any):ng.IPromise<string>;
            //#Quản trị
                /*User*/
                    GetUserByCompany(idCompany:number):ng.IPromise<Array<App.Shared.Entity.IUser>>;
                    GetAllUser():ng.IPromise<Array<App.Shared.Entity.IUser>>;

            //Thống kê 10 ngày
                    GetDevice10DayRequest(begin:string, end:string, name:string):ng.IPromise<Array<any>>;

            //Get DataCenter
                    GetDataCenter():ng.IPromise<Array<any>>;

            //Get cách tính tiền trong taxi
                    GetCachTinhTien(idCompany:number):ng.IPromise<Array<any>>;
                    GetCachKettoanca(idCompany:number):ng.IPromise<Array<any>>;
                }
                export enum HttpMethod{
                    POST,
                    PUT,
                    GET,
                    DEL
                }
                export class ApiTranfer implements IApi{
                    /*Lấy dữ liệu Báo cáo doanh nghiệp
                    ========================================*
                     */
                    //báo cáo tổng hợp
                    GetGeneralCompanyReportBySerial(serials:Array<string>, idCompany:number, begin:string, end:string):angular.IPromise<Array<any>> {
                        var result=this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.POST,`${this.global.ReportHost}api/Qc31Report/GeneralCompanyReportBySerial?companyId=
                        ${idCompany}&beginTime=${begin}&endTime=${end}`,serials).then((data:any)=>{
                            if(data.Status==0) {
                                result.resolve(Array<any>());
                                return;
                            }
                            else{
                                var cList=Array<any>();
                                $.each(data.GeneralCompanyReportList,(idx:number,m:any)=>{
                                    cList.push(m);
                                });
                                result.resolve(cList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //báo cáo hành trình
                    GetDeviceTripBySerial(idCompany:number,serial:string,begin: string, end:string):angular.IPromise<Array<any>>{
                        var result=this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/Qc31Report/GetDeviceTripBySerial?companyId=${idCompany}&serial=${serial}&beginTime=${begin}&endTime=${end}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DeviceTripLogList, (idx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                           result.reject();
                        });
                        return result.promise;
                    }
                    /*Báo cáo cuốc xe
                    ===========================*/
                    //Theo serial
                    GetDeviceSessionLogBySerial(idCompany:number, serial:string, begin:string, end:string):angular.IPromise<Array<any>>{
                        var result= this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/LogManager/DeviceSessionLogBySerial?companyId=${idCompany}&beginTime=${begin}&endTime=${end}&serial=${serial}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DeviceSessionLogList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Theo groupId
                    GetDeviceSessionLogByGroupId(idCompany:number, groupId:number, begin:string, end:string):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/LogManager/DeviceSessionLogByGroupId?companyId=${idCompany}&groupId=${groupId}&beginTime=${begin}&endTime=${end}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DeviceSessionLogList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Theo CompanyId
                    GetDeviceSessionLog(idCompany:number, begin:string, end:string):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/LogManager/DeviceSessionLog?companyId=${idCompany}&beginTime=${begin}&endTime=${end}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DeviceSessionLogList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }

                    /*Báo cáo theo dõi vận tốc*/
                    GetSpeedDeivcesTripBySerial(idCompany:number, serial:string, begin:string, end:string):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/Device/GetDeviceTripBySerial?companyId=${idCompany}&serial=${serial}&beginTime=${begin}&endTime=${end}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DeviceTripList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    /*Báo cáo chi tiết mất tín hiệu*/
                    GetLogManagerDeviceTraceLogBySerial(idCompany:number, begin:string, end:string,serial:string):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/LogManager/DeviceTraceLogBySerial?companyId=${idCompany}&beginTime=${begin}&endTime=${end}&serial=${serial}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DeviceTraceLogList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    /*Báo cáo thống kê mất tín hiệu
                    * ==============================*/
                    //Theo công ty
                    GetLogManagerDeviceTraceSheetByCompany(idCompany:number,begin:string,end:string):angular.IPromise<Array<App.Shared.Entity.IDevice>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/LogManager/DeviceTraceSheetByCompany?companyId=${idCompany}&beginTime=${begin}&endTime=${end}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DeviceTraceSheetList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Theo đội xe
                    GetLogManagerDeviceTraceSheetByGroup(idCompany:number,groupId:number,begin:string,end:string):angular.IPromise<Array<App.Shared.Entity.IDevice>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/LogManager/DeviceTraceSheetByGroup?companyId=${idCompany}&groupId=${groupId}&beginTime=${begin}&endTime=${end}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DeviceTraceSheetList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Theo serial
                    GetLogManagerDeviceTraceSheetBySerial(idCompany:number,begin:string,end:string, serial:Array<string>):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.POST, `${this.global.ReportHost}api/LogManager/DeviceTraceSheetBySerial?companyId=${idCompany}&beginTime=${begin}&endTime=${end}`,serial).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DeviceTraceSheetList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    /*Theo dõi camera*/
                    GetLogManagerCameraLogBySerial(idCompany:number, serial:string, begin:string,end:string):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/LogManager/CameraLogBySerial?companyId=${idCompany}&serial=${serial}&beginTime=${begin}&endTime=${end}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.CameraLogList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    /*Biểu đồ nhiên liệu*/
                    //Theo serial
                    PostLogManagerRefuelLogBySerial(idCompany:number,seriallist:Array<string>, begin:string, end:string):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.POST, `${this.global.ReportHost}api/LogManager/TtNhienLieuBySerial?companyId=${idCompany}&beginTime=${begin}&endTime=${end}`,seriallist).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var DList_send:any;
                                var dList = Array<any>();
                                $.each(rep.BcNhienLieuTranfers, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                DList_send = {DungTichBinh: rep.DungTichBinh, dataList:dList};
                                result.resolve(DList_send);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Theo đội xe
                    GetLogManagerRefuelLogByGroupId(idCompany:number,groupId:number, begin:string, end:string):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/LogManager/RefuelLogByGroupId?companyId=${idCompany}&groupId=${groupId}&beginTime=${begin}&endTime=${end}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.RefueLogList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Theo công ty
                    GetLogManagerRefuelLogByCompany(idCompany:number, begin:string, end:string):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/LogManager/RefuelLogByGroupId?companyId=${idCompany}&beginTime=${begin}&endTime=${end}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.RefueLogList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                /*Báo cáo QCVN31*/
                    //Báo cáo quá tốc độ
                    //Theo công ty
                    GetOverSpeedLogByCompany(idCompany:number, begin:string, end:string): angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/Qc31Report/GetOverSpeedLog?idCompany=${idCompany}&beginTime=${begin}&endTime=${end}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.OverSpeedLogList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Theo đội xe
                    GetOverSpeedLogByGroup(idCompany:number,groupId:number, begin:string, end:string): angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/Qc31Report/GetOverSpeedByGroup?companyId=${idCompany}&groupId=${groupId}&beginTime=${begin}&endTime=${end}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.OverSpeedLogList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Theo xe
                    GetOverSpeedLogBySerial(idCompany:number,begin:string, end:string,serial:Array<string>): angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.POST, `${this.global.ReportHost}api/Qc31Report/GetOverSpeedBySerial?companyId=${idCompany}&beginTime=${begin}&endTime=${end}`,serial).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.OverSpeedLogList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Báo cáo dừng đỗ
                    GetDevicePauseBySerial(idCompany:number, serial:string, begin:string, end:string): angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/Qc31Report/GetDevicePauseBySerial?companyId=${idCompany}&serial=${serial}&beginTime=${begin}&endTime=${end}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DevicePauseLogList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Báo cáo tốc độ xe
                    GetDeviceSpeedBySerial(idCompany:number, serial:string, begin:string, end:string):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/Qc31Report/GetDeviceSpeedBySerial?companyId=${idCompany}&serial=${serial}&beginTime=${begin}&endTime=${end}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DeviceSpeedLogList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Báo cáo thời gian lái xe
                    GetDriverSessionByCompany(idCompany:number, begin:string, end:string):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/Qc31Report/GetDriverSessionByCompany?companyId=${idCompany}&beginTime=${begin}&endTime=${end}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DriverSessionLogList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Báo cáo tổng hợp theo xe
                    PostGeneralReportBySerial(idCompany:number, begin:string, end:string, listserial:Array<any>):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.POST, `${this.global.ReportHost}api/Qc31Report/GeneralReportBySerial?companyId=${idCompany}&beginTime=${begin}&endTime=${end}`, listserial).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.GeneralReportList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Báo cáo tổng hợp theo tài xế
                    PostGeneralReportByDriver(idCompany:number, begin:string, end:string, driverIdList:Array<any>):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.POST, `${this.global.ReportHost}api/Qc31Report/GeneralReportByDriver?companyId=${idCompany}&beginTime=${begin}&endTime=${end}`, driverIdList).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.GeneralReportList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Cập nhật danh sách xe
                    PutUpdateDeviceList(idCompany:number, deviceslist:Array<any>):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.PUT, `${this.global.ReportHost}api/Device/UpdateDeviceList?companyId=${idCompany}`, deviceslist).then((rep:any)=>{
                            console.log(rep);
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                // var dList = Array<any>();
                                // $.each(rep.GeneralReportList, (inx:number,m:any)=>{
                                //     dList.push(m);
                                // });
                                result.resolve(deviceslist);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }

                    //Thông tin sim
                    GetSimInfoByCompany(idCompany:number):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/Sim/SimInfoByCompany?companyId=${idCompany}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.SimInfoList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Theo đội xe
                    GetSimInfoByGroupId(idCompany:number, groupId:number):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/Sim/SimInfoByGroupId?companyId=${idCompany}&groupId=${groupId}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.SimInfoList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Theo serial
                    PostSimInfoBySerial(idCompany:number, seriallist:Array<any>):angular.IPromise<Array<App.Shared.Entity.IDevice>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.POST, `${this.global.ReportHost}api/Sim/SimInfoBySerial?companyId=${idCompany}`,seriallist).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.SimInfoList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    //Danh sách xe hết hạn phí
                    GetDevicesByExpireDate(idCompany:number, daynumber:number):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}device/GetByExpireDate?companyId=${idCompany}&dayNumber=${daynumber}`).then((rep:any)=>{
                            //console.log(rep);
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DeviceList, (inx:number,m:any)=>{
                                    dList.push({
                                        STT: inx + 1,
                                        DisplayId: `${m.Id} - ${m.Bs}`,
                                        Serial: m.Serial,
                                        EndTime: m.EndTime,
                                        // Note: `Xe sẽ hết hạn trong <span class="text-danger">${Math.floor((Date.parse(m.EndTime) - Date.now())/86400000)}</span> ngày`,
                                    });
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }

                //#Quản lý ADSUN
                    //Lập trình thiết bị
                    GetDeviceSetupInfo(idCompany:number, serial:string):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/SetupDevice/GetDeviceSetupInfo?companyId=${idCompany}&serial=${serial}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DeviceSetupInfoList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                console.log(dList);
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    PostSetupDevices(idCompany:number, serial:string, setup:any):angular.IPromise<string>{
                        var result = this.$q.defer<string>();
                        this.Forward(HttpMethod.POST, `${this.global.ReportHost}api/SetupDevice/SetupDevive?idCompany=${idCompany}&serial=${serial}`, setup).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve('Fail');
                                return;
                            }else{
                                result.resolve('OK');
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    PostSetupDeviceOnCompany(idCompany:number, setup:any):angular.IPromise<string>{
                        var result = this.$q.defer<string>();
                        this.Forward(HttpMethod.POST, `${this.global.ReportHost}api/SetupDevice/SetupDeviceOnCompany?companyId=${idCompany}`, setup).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve('Fail');
                                return;
                            }else{
                                // var dList = Array<any>();
                                // $.each(rep.DeviceSetupInfoList, (inx:number,m:any)=>{
                                //     dList.push(m);
                                // });
                                // console.log(dList);
                                result.resolve('OK');
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
            //Thống kê 10 ngày
                    GetDevice10DayRequest(begin:string, end:string, name:string):angular.IPromise<Array<any>>{
                        var result = this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/Tool/Device10DayRequest?beginTime=${begin}&endTime=${end}&type=${name}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.Device10DayList, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
            //#Quản trị
                /*User*/
                    GetUserByCompany(idCompany:number):angular.IPromise<Array<App.Shared.Entity.IUser>>{
                        var result=this.$q.defer<Array<App.Shared.Entity.IUser>>();
                        this.Forward(HttpMethod.GET,`${this.global.AuthHost}Administration/GetUserByCompany?companyId=${idCompany}`).then((rep:any)=>{
                            if(rep.Status==0) {
                                result.resolve(Array<App.Shared.Entity.IUser>());
                                return;
                            }
                            else{
                                var cList=Array<App.Shared.Entity.IUser>();
                                $.each(rep.AccountTranfers,(idx:number,m:any)=>{
                                    cList.push(m);
                                });
                                result.resolve(cList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    GetAllUser():angular.IPromise<Array<App.Shared.Entity.IUser>>{
                        var result=this.$q.defer<Array<App.Shared.Entity.IUser>>();
                        this.Forward(HttpMethod.GET,`${this.global.AuthHost}Administration/GetAllUser`).then((rep:any)=>{
                            if(rep.Status==0) {
                                result.resolve(Array<App.Shared.Entity.IUser>());
                                return;
                            }
                            else{
                                var cList=Array<App.Shared.Entity.IUser>();
                                $.each(rep.AccountTranfers,(idx:number,m:any)=>{
                                    cList.push(m);
                                });
                                result.resolve(cList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
            /*Lấy thông tin dữ liệu cho các combobox*/
                    GetAllDeviceByGroupId(companyId:number, groupId:number):angular.IPromise<Array<App.Shared.Entity.IDevice>> {
                        var result=this.$q.defer<Array<App.Shared.Entity.IDevice>>();
                        this.Forward(HttpMethod.GET,`${this.global.ReportHost}api/Device/GetByGroupId?companyId=${companyId}&groupId=${groupId}`).then((data:any)=>{
                            if(data.Status==0) {
                                result.resolve(Array<App.Shared.Entity.IDevice>());
                                return;
                            }
                            else{
                                var cList=Array<App.Shared.Entity.IDevice>();
                                $.each(data.DeviceList,(idx:number,m:any)=>{
                                    cList.push(this.BuildDevice(m));
                                });
                                result.resolve(cList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }

                    GetAllDeviceByCompanyId(id:number):angular.IPromise<Array<App.Shared.Entity.IDevice>> {
                        var result=this.$q.defer<Array<App.Shared.Entity.IDevice>>();
                        this.Forward(HttpMethod.GET,`${this.global.ReportHost}api/Device/GetByCompanyId?idCompany=${id}`).then((data:any)=>{
                            if(data.Status==0) {
                                result.resolve(Array<App.Shared.Entity.IDevice>());
                                return;
                            }
                            else{
                                var cList=Array<App.Shared.Entity.IDevice>();
                                if(data.DeviceList.length !== 0){
                                    $.each(data.DeviceList,(idx:number,m:any)=>{
                                        cList.push(this.BuildDevice(m));
                                    });

                                }else{
                                    cList=[];
                                }
                                result.resolve(cList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }

                    GetDeviceStartById(idCompany:number):angular.IPromise<Array<any>>{
                        var result=this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET,`${this.global.ReportHost}api/Device/GetDeviceStateStart?companyId=${idCompany}`).then((data:any)=>{
                            if(data.Status==0) {
                                result.resolve(Array<any>());
                                return;
                            }
                            else{
                                var cList=Array<any>();
                                if(data.DeviceList.length !== 0){
                                    $.each(data.listDevice,(idx:number,m:any)=>{
                                        cList.push(m);
                                    });

                                }else{
                                    cList=[];
                                }
                                result.resolve(cList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    GetDeviceByCompanyKhachChuyen(idCompany:number):ng.IPromise<Array<any>>{
                        var result=this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET,`${this.global.ReportHost}api/Device/GetDeviceStateStart?companyId=${idCompany}`).then((data:any)=>{
                            if(data.Status==0) {
                                result.resolve(Array<any>());
                                return;
                            }
                            else{
                                var cList=Array<any>();
                                if(data.listDevice.length !== 0){
                                    $.each(data.listDevice,(idx:number,m:any)=>{
                                        cList.push(m);
                                    });

                                }else{
                                    cList=[];
                                }
                                result.resolve(cList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }

                    GetAllGroupByCompanyId(id:number):ng.IPromise<Array<App.Shared.Entity.IGroup>> {
                        var result=this.$q.defer<Array<App.Shared.Entity.IGroup>>();
                        this.Forward(HttpMethod.GET,`${this.global.ReportHost}api/Group/GetGroupByCompany?companyId=${id}`).then((data:any)=>{
                            if(data.Status==0) {
                                result.resolve(Array<App.Shared.Entity.IGroup>());
                                return;
                            }
                            else{
                                var cList=Array<App.Shared.Entity.IGroup>();
                                if(data.GroupList.length !== 0){
                                    $.each(data.GroupList,(idx:number,m:any)=>{
                                        cList.push({
                                            Id:m.Id,
                                            Name:m.Name,
                                            CompanyId:m.CompanyId
                                        });
                                    });
                                }else{
                                    cList = [];
                                }

                                result.resolve(cList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    GetAllCompany():ng.IPromise<Array<App.Shared.Entity.ICompany>> {
                        var result=this.$q.defer<Array<App.Shared.Entity.ICompany>>();
                        this.Forward(HttpMethod.GET,`${this.global.ReportHost}api/Company/GetAllCompany`).then((data:any)=>{
                            if(data.Status==0) {
                                result.resolve(Array<App.Shared.Entity.ICompany>());
                                return;
                            }
                            else{
                                var cList=Array<App.Shared.Entity.ICompany>();
                                $.each(data.CompanyList,(idx:number,m:any)=>{

                                        cList.push({
                                            Id:m.Id,
                                            Name:m.Name,
                                            AddressCompany:m.AddressCompany,
                                            Description:m.Description,
                                            CreateTime:m.CreateTime,
                                            Lat:m.Location.Lat,
                                            Lng:m.Location.Lng,
                                            Address:m.Location.Address,
                                            DataCenterId:m.DataCenterId,
                                            CompanyType:m.CompanyType,
                                            DbId:m.DbId,
                                            Display:`${m.Id} - ${m.Name}`
                                        });


                                });
                                result.resolve(cList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }


                    GetAllCompanyByType():ng.IPromise<Array<App.Shared.Entity.ICompany>>{
                        var result=this.$q.defer<Array<App.Shared.Entity.ICompany>>();
                        this.Forward(HttpMethod.GET,`${this.global.ReportHost}api/Company/GetAllCompany`).then((data:any)=>{
                            if(data.Status==0) {
                                result.resolve(Array<App.Shared.Entity.ICompany>());
                                return;
                            }
                            else{
                                var cList=Array<App.Shared.Entity.ICompany>();
                                $.each(data.CompanyList,(idx:number,m:any)=>{
                                    if(m.CompanyType == 1){
                                        cList.push({
                                            Id:m.Id,
                                            Name:m.Name,
                                            AddressCompany:m.AddressCompany,
                                            Description:m.Description,
                                            CreateTime:m.CreateTime,
                                            Lat:m.Location.Lat,
                                            Lng:m.Location.Lng,
                                            Address:m.Location.Address,
                                            DataCenterId:m.DataCenterId,
                                            CompanyType: m.CompanyType,
                                            DbId:m.DbId,
                                            Display:`${m.Id} - ${m.Name}`
                                        });
                                    }
                                });
                                result.resolve(cList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }

                    GetAllCompanyById(id:number):ng.IPromise<App.Shared.Entity.ICompany> {

                        return undefined;
                    }
                    GetAllDriverByCompany(idCompany:number):ng.IPromise<Array<App.Shared.Entity.IDriver>>{
                        var result=this.$q.defer<Array<App.Shared.Entity.IDriver>>();
                        this.Forward(HttpMethod.GET,`${this.global.ReportHost}api/Driver/GetDriverByCompany?companyId=${idCompany}`).then((data:any)=>{
                            if(data.Status==0) {
                                result.resolve(Array<App.Shared.Entity.IDriver>());
                                return;
                            }
                            else{
                                var cList=Array<App.Shared.Entity.IDriver>();
                                $.each(data.driverList,(idx:number,m:any)=>{
                                    cList.push({
                                        Id: m.Id,
                                        Name: m.Name,
                                        Gplx: m.Gplx,
                                        Mnv: m.Mnv,
                                        CompanyId: m.CompanyId,
                                        GroupId: m.GroupId,
                                        Born: m.Born,
                                        Cmnd: m.Cmnd,
                                        Address: m.Address,
                                        Male: m.Male,
                                        CreateDateOfGplx: m.CreateDateOfGplx,
                                        EndDateOfGplx: m.EndDateOfGplx,
                                        AddressOfGplx: m.AddressOfGplx,
                                        Phone1: m.Phone1,
                                        Phone2: m.Phone2,
                                        Bs: m.Bs,
                                        UserId: m.UserId,
                                        SoTai: m.SoTai,
                                        DisplayName:`${m.Mnv} - ${m.Name}`,
                                    });
                                });
                                result.resolve(cList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }

                    /*Lấy thông tin loại xe*/
                    GetAllModelCar():ng.IPromise<Array<App.Shared.Entity.IModelCar>>{
                        var result=this.$q.defer<Array<App.Shared.Entity.IModelCar>>();
                        this.Forward(HttpMethod.GET,`${this.global.ReportHost}api/DeviceModel/GetAll`).then((rep:any)=>{
                            if(rep.Status==0) {
                                result.resolve(Array<App.Shared.Entity.IModelCar>());
                                return;
                            }
                            else{
                                var dList=Array<App.Shared.Entity.IModelCar>();
                                $.each(rep.DeviceModelList, (inx:number,m:any)=>{
                                    // dList.push(m);
                                    if(m.IsMayChuTaxi){
                                        dList.push(m);
                                    }

                                });
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }

                    /*Danh sách vùng điểm*/
                        //Vùng
                    GetZoneByCompany(idCompany:number):ng.IPromise<Array<any>>{
                        var result=this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/CheckZone/GetCheckZoneByCompany?companyId=${idCompany}`).then((rep:any)=>{
                            // todo: bật lại khi  update server
                            // if(rep.Status == 0){
                            //     result.resolve(Array<any>());
                            //     return;
                            // }else{
                            //     var dList = Array<any>();
                            //     $.each(rep.CheckZoneList, (inx:number,m:any)=>{
                            //         dList.push(m);
                            //     });
                            //     console.log(rep.CheckZoneList);
                            //     result.resolve(dList);
                            // }


                            var dList = Array<any>();
                            $.each(rep.CheckZoneList, (inx:number,m:any)=>{
                                dList.push(m);
                            });
                            console.log(rep.CheckZoneList);
                            result.resolve(dList);

                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;

                    }
                        //Điểm
                    GetPOintByCompany(idCompany:number):ng.IPromise<Array<any>>{
                        var result=this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/GpsCheckPoint/GpsCheckPointByCompany?companyId=${idCompany}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.GpsCheckPointTranfers, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                // console.log(dList);
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    GetBangGiaTuyen(CompanyId:number,groupId:number, modelName:string):ng.IPromise<Array<App.Shared.Entity.IBangGiaTuyen>>{
                        var result=this.$q.defer<Array<App.Shared.Entity.IBangGiaTuyen>>();
                        this.Forward(HttpMethod.GET,`${this.global.ReportHost}api/InputTaxi/PriceLineLamda?companyId=${CompanyId}&deviceGroupId=${groupId}&modelName=${modelName}`).then((data:any)=>{
                            if(data.Status==0) {
                                result.resolve(Array<App.Shared.Entity.IBangGiaTuyen>());
                                return;
                            }
                            else{
                                var cList_end: any;
                                var arraynamebegin:any = [];
                                var namebegin:any = [];
                                var cList=Array<App.Shared.Entity.IBangGiaTuyen>();
                                $.each(data.PriceLineList,(idx:number,m:any)=>{
                                    cList.push({
                                        Id:m.Id,
                                        BeginAddressLocation:`${m.BeginLocation.Address}`,
                                        EndAddressLocation: `${m.EndLocation.Address}`,
                                        TimeRun:m.TimeRun,
                                        KmRun:m.KmRun,
                                        MoneyRun:m.MoneyRun,
                                        MoneyEveryKm:m.MoneyEveryKm,
                                        MoneyEveryTime:m.MoneyEveryTime,
                                    });
                                    cList.push({
                                        Id:m.Id,
                                        BeginAddressLocation:`${m.EndLocation.Address}`,
                                        EndAddressLocation: `${m.BeginLocation.Address}`,
                                        TimeRun:m.TimeRun,
                                        KmRun:m.KmRun,
                                        MoneyRun:m.MoneyRun,
                                        MoneyEveryKm:m.MoneyEveryKm,
                                        MoneyEveryTime:m.MoneyEveryTime,
                                    });
                                    arraynamebegin.push(m.BeginLocation.Address,m.EndLocation.Address);

                                });
                                //lọc các tên trùng trong mảng tên địa chỉ bắt đầu
                                var unique = arraynamebegin.filter(function(elem:any, index:number, self:any) {
                                    return index == self.indexOf(elem);
                                });
                                $.each(unique,(ind:number,val:any)=>{
                                    namebegin.push({Name: val, BeginAddressLocation:val});
                                });
                                cList_end = {datasosanh: cList, datanamebegin:namebegin};
                                // console.log(unique);
                                result.resolve(cList_end);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    GetCachTinhTien(idCompany:number):ng.IPromise<Array<any>>{
                        var result=this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/Company/CachTinhTienCuoc/${idCompany}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.ListPluginTranfers, (inx:number,m:any)=>{
                                    console.log(m);

                                    dList.push({Description: m.Description,Id: (m.Id).toUpperCase(),Name:m.Name});
                                });
                                // console.log(dList);
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }
                    GetCachKettoanca(idCompany:number):ng.IPromise<Array<any>>{
                        var result=this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/Company/CachTinhTienCa/${idCompany}`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.ListPluginTranfers, (inx:number,m:any)=>{
                                    dList.push({Description: m.Description,Id: (m.Id).toUpperCase(),Name:m.Name});
                                });
                                // console.log(dList);
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }


                    GetDataCenter():ng.IPromise<Array<any>>{
                        var result=this.$q.defer<Array<any>>();
                        this.Forward(HttpMethod.GET, `${this.global.ReportHost}api/System/GetAllDataCenterInfo`).then((rep:any)=>{
                            if(rep.Status == 0){
                                result.resolve(Array<any>());
                                return;
                            }else{
                                var dList = Array<any>();
                                $.each(rep.DataCenterInfos, (inx:number,m:any)=>{
                                    dList.push(m);
                                });
                                console.log(dList);
                                result.resolve(dList);
                            }
                        }).catch(()=>{
                            result.reject();
                        });
                        return result.promise;
                    }

                    Forward(method:HttpMethod,url:string,data:any=null):ng.IPromise<any>{
                        var result=this.$q.defer<any>();
                        switch (method){
                            case HttpMethod.POST:
                                this.$http.post(url,data).then((db:any)=>{
                                    result.resolve(db.data);
                                }).catch(()=>{
                                    result.reject();
                                });
                                break;
                            case HttpMethod.GET:
                                this.$http.get(url).then((db:any)=>{
                                    result.resolve(db.data);
                                }).catch(()=>{
                                    result.reject();
                                });
                                break;
                            case HttpMethod.DEL:
                                this.$http.delete(url,data).then((db:any)=>{
                                    result.resolve(db.data);
                                }).catch(()=>{
                                    result.reject();
                                });
                                break;
                            case HttpMethod.PUT:
                                this.$http.put(url,data).then((db:any)=>{
                                    result.resolve(db.data);
                                }).catch(()=>{
                                    result.reject();
                                });
                                break;
                            default:
                                throw "ApiTranfer : không xác định được http method";
                        }
                        return result.promise;
                    }

                    BuildDevice(m:any):App.Shared.Entity.IDevice{
                        return {
                            Id: m.Id,
                            CompanyId: m.CompanyId,
                            GroupId: m.GroupId,
                            BgtTranport: m.BgtTranport,
                            IsCamera: m.IsCamera,
                            IsNhienLieu: m.IsNhienLieu,
                            Bs: m.Bs,
                            CreateTime: m.CreateTime,
                            EndTime: m.EndTime,
                            FuelPer100Km: m.FuelPer100Km,
                            GroupName: m.GroupName,
                            LimitSpeed: m.LimitSpeed,
                            ModelName: m.ModelName,
                            MoneyTime: m.MoneyTime,
                            PhoneOfDevice: m.PhoneOfDevice,
                            OwnerPhone: m.OwnerPhone,
                            Seat: m.Seat,
                            Serial: m.Serial,
                            Sgtvt: m.Sgtvt,
                            SimNgoai: m.SimNgoai,
                            Type: m.Type,
                            TypeValue: m.TypeValue,
                            Version: m.Version,
                            VinSerial: m.VinSerial,
                            DeviceNote: m.DeviceNote,
                            Display: `${m.Bs} - ${m.Serial}`,
                            DisplayId: `${m.Id} - ${m.Bs}`,
                            ThoiGianPhatGanNhat:m.ThoiGianPhatGanNhat,
                            NotView:m.NotView
                        }
                    }

                    public static $inject=['$http','$q','global'];
                    constructor(public $http:ng.IHttpService,public $q:ng.IQService,public global:App.Shared.GlobalService){

                    }
                }

                AdModule.service('api',ApiTranfer);
            }
        }
}