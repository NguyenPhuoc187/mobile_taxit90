/**
 * Created by user on 04/27/2016.
 */
/*Quản lý các thông tin gưởi từ client lên server*/

/// <reference path="../../tstyping/bootbox/bootbox.d.ts" />
module App{
    export module Shared{
        export class RequestFilter implements ng.IHttpInterceptor {
            /*thêm các thông tin chung trước khi request lên server*/
            request = (config: ng.IRequestConfig): ng.IPromise<ng.IRequestConfig> => {
                // console.log();
                if(config.url.toLocaleUpperCase().indexOf('MAPS.GOOGLEAPIS.COM')>0){
                    return;
                }
                // thêm các trường vào header vào request gưởi lên server
                config.headers['token'] = this.global.Token.GetValue<string>();
                return this.$q.when(config);
            };

            /*bắt các lỗi xảy ra sau khi request*/
            responseError = (err: ng.IHttpPromiseCallbackArg<Object>) => {

                if (err.status == 401) {// hết hạn token
                    window.location.href = "/login.html";
                    return;
                }
                if(err.status==500){
                    console.error(`Server lỗi : ${err.data}`);
                    return;
                }
                //console.log(inject);
            };
            static $inject = ['$q','$location','global'];
            constructor(private $q:ng.IQService, private $location:ng.ILocationService,private global:IGlobalService) {

            }
        }

        AdModule.service('requestFilter',RequestFilter);

        /*inject server vào hệ thống request của angular*/
        AdModule.config(['$httpProvider',($httpProvider:ng.IHttpProvider)=>{
            $httpProvider.interceptors.push('requestFilter');
            if (!$httpProvider.defaults.headers.get) {
                $httpProvider.defaults.headers.get = {} as any;
            }
            //// bỏ phần load thông tin trên cache của IE
            //$httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
            //// extra
            //$httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
            //$httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
        }]);
    }
}