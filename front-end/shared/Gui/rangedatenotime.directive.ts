/**
 * Created by phamn on 08/09/2016.
 */
module App{
    export module Shared{
        export module Gui{
            export class RangeDateNoTimeControl extends DateTimeControl{
                constructor(id:string){
                   super(id);
                    this.open = 'right';
                }
                loadtime() {
                    //Here your view content is fully loaded !!
                    var tomorrow = new Date();
                    var day:any = tomorrow.getDate();
                    var month: any = tomorrow.getMonth() + 1;

                    var year: any = tomorrow.getFullYear();

                    var config_time: string = day + '/' + month + '/' + year;

                    $('.demo i').click(function () {
                        $(this).parent().find('input').click();
                    });
                    var element = $(`#${this.id}`);
                    element.daterangepicker({
                        format: 'DD/MM/YYYY',
                        opens:this.open,
                        locale: { cancelLabel: 'Hủy',
                            applyLabel: 'Đồng ý',
                            separator: "-",
                            fromLabel:'Từ ngày',
                            toLabel:'Đến ngày',
                            daysOfWeek: ['<label class="text-danger">CN</label>', 'T2', 'T3', 'T4', 'T5', 'T6','T7'],
                            monthNames: ['Tháng 1 - ', 'Tháng 2 - ', 'Tháng 3 - ', 'Tháng 4 - ', 'Tháng 5 - ', 'Tháng 6 - ', 'Tháng 7 - ', 'Tháng 8 - ', 'Tháng 9 - ', 'Tháng 10 - ', 'Tháng 11 - ', 'Tháng 12 - '],
                            firstDay: 1

                        },
                        
                    });

                }
                changeTime(time:any):string{
                    var year:string = time.slice(6,10);
                    var month:string = time.slice(3,5);
                    var date:string = time.slice(0,2);
                    var tt:any = month + '/' + date + '/' + year ;
                    return tt;
                }
                begin():string {
                    var mang:any = this.GetTimeString().split(' - ');
                    return this.changeTime(mang[0]);
                }

                end():string {
                    var mang:any = this.GetTimeString().split(' - ');
                    return this.changeTime(mang[1]);
                }
                GetTimeString(){
                    if(this.value !== undefined){
                        return this.value;
                    }else{
                        return this.value_default;
                    }

                }
            }
            export class RangeDateNoTimeController extends DateTimeController{
                constructor($scope:IDateTimeScope,global:App.Shared.GlobalService){
                    super($scope,global);
                    $scope.id.value_default= global.getnowdaynotime();

                }
            }
            export function rangedatenotimeDirective($templateCache:ng.ITemplateCacheService, $compile:ng.ICompileService, $http:ng.IHttpService): ng.IDirective {
                return {
                    // bindToController: true,
                    controller: ['$scope','global', RangeDateNoTimeController],
                    name: "rangedateNotime",
                    restrict: "EA",
                    //transclude: true,// cho phép include đoán html nằm trong thẻ grid-view
                    templateUrl:'./shared/Gui/rangedatenotime.html',
                    scope: {
                        id:"="
                    },
                    replace: true// không được phép xóa element nằm trong thẻ grid-view
                }
            }
            AdModule.directive('rangedateNotime',rangedatenotimeDirective);
        }
    }
}