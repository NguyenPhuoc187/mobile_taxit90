/**
 * Created by phamn on 26/08/2016.
 */
/// <reference path="../../../tstyping/angularjs/angular.d.ts" />
module App {
    export module Shared {
        export module Gui {
            export interface IDropDownConfig<T>{
                autoOpen?:boolean;
                autoItemsHeight?:boolean;
                autoDropDownHeight?:boolean;
                animationType?:string;
                checkboxes?:boolean;
                closeDelay?:number;
                disabled?:boolean;
                displayMember?:string;
                dropDownHorizontalAlignment?:string;
                dropDownVerticalAlignment?:string;
                dropDownHeight?:number;
                dropDownWidth?:number;
                enableSelection?:boolean;
                enableBrowserBoundsDetection?:boolean;
                enableHover?:boolean;
                filterable?:boolean;
                filterHeight?:number;
                filterDelay?:number;
                filterPlaceHolder?:string;
                height?:number|string;
                incrementalSearch?:boolean;
                incrementalSearchDelay?:number;
                itemHeight?:number;
                openDelay?:number;
                placeHolder	?:string;
                popupZIndex	?:number;
                rtl?:boolean;
                renderer?:Function;
                selectionRenderer?:Function;
                searchMode?:string;
                scrollBarSize?:number;
                source?:any;
                selectedIndex?:number;
                template?:string;
                theme?:string;
                valueMember?:string;
                width?:number|string;
                change?:(event:any)=>void;
                select?:(event:any)=>void;
                close?:(event: any)=>void;
                unselect?:(event:any)=>void;
                jqxDropDownList?:any;
                apply?():void;
                checkChange?:(event:any) => void;
            }
            export interface IDropDownCustomEvent<T>{
                onCheckChange?:(check:boolean,val:T)=>void;
                onSelect?:(index:number, val:Array<T>)=>void;
                onSelectOnItem?:(index:number, val:T)=>void;
                onSelectCheck?:(event:any)=>void;
                onSelectUser?:(check:boolean, val:Array<T>)=> void;

            }
            export interface IDropDown<T>{
                setting:IDropDownConfig<T>;
                instance:any;
                title:string;
                customEvent?:IDropDownCustomEvent<T>;
                loadPromise:(api:ng.IPromise<Array<T>>,firstValue:Array<T>,$q:ng.IQService)=>ng.IPromise<void>;
                loadPromise_localdata:(source:ng.IPromise<Array<T>>,$q: ng.IQService,selectfirstValue?:boolean,firstValue?:Array<T>) =>ng.IPromise<void>;
                load:(api:ng.IPromise<Array<T>>,firstValue:Array<T>,selectfirstValue?:boolean)=>void;
                ready:boolean;
                id:string;
                itemSelect():T;
                // itemChecked()?:Array<T>;
                valueSelects():Array<any>;
                allValue():Array<any>;
                setModeCheckbox(val:boolean):void;
                itemsMutiSelect(): Array<any>;
                setItemDefault:(where:(v:T)=>boolean)=> any;
                // setItemDefault_xacnhanthietbi:(where:(v:T)=>boolean)=> any;
                setItemDefaultChecked:(where:(v:T)=>boolean)=> any;
                setItemDefaultChecked_Group:(where:(v:T)=>boolean)=> any;
                loadlocaldata_drop:(source:ng.IPromise<Array<T>>,$q: ng.IQService,selectfirstValue?:boolean,firstValue?:Array<T>) =>ng.IPromise<void>;
                loadlocaldata_dropuser:(source:Array<T>,$q: ng.IQService,selectfirstValue?:boolean,firstValue?:Array<T>)=>ng.IPromise<void>;
                loadlocaldata_dropnhienlieu:(source:Array<T>,$q: ng.IQService,selectfirstValue?:boolean,firstValue?:Array<T>)=>ng.IPromise<void>;
            }
            export class DropDown<T> implements IDropDown<T>{
                private isPaste:boolean;
                loadPromise(api:ng.IPromise<Array<T>>, firstValue:Array<T>, $q:angular.IQService):ng.IPromise<void>{
                    var result=$q.defer<void>();
                    this.loadDeault(api,firstValue,$q).then(()=>result.resolve());
                    return result.promise;
                }
                loadPromise_localdata(source:ng.IPromise<Array<T>>,$q:angular.IQService,selectfirstValue?:boolean,firstValue?:Array<T>):ng.IPromise<void>{
                    var result=$q.defer<void>();
                    this.loadlocaldata_drop(source,$q,false,null).then(()=>result.resolve());
                    return result.promise;
                }
                setModeCheckbox(val:boolean):void {
                    this.setting.checkboxes=val;
                    //this.instance.refresh();
                    //this.instance.render();
                }
                _itSelect:T;
                itemSelect():T{
                    if(this._itSelect==undefined) return undefined;
                    //var inputs=$(`#dropdownlistContent${this.id} input`);
                    //if(inputs.length==0) return undefined;
                    //var input=inputs[0];
                    //if(this._itSelect[this.setting.displayMember]==input.value)
                    //    return this._itSelect;
                    //this._itSelect=undefined;
                    return this._itSelect;
                };
                _itChecked:Array<T>;


                allValue():Array<any>{
                    var result=Array<any>();
                    $.each(this.setting.source._source,(i,val)=>{
                        result.push(val[this.setting.valueMember]);
                    });
                    return result;
                }
                valueSelects():Array<any>{
                    var checks=this.instance.getCheckedItems();
                    var result=Array<any>();
                    $.each(checks,(i,val)=>{
                        result.push(val.value);
                    });
                    return result;
                }
                itemsMutiSelect():Array<any>{
                    var checks = this.instance.getSelectedItems();
                    var result = Array<any>();
                    $.each(checks,(i,val)=>{
                        result.push(val.value);
                    });
                    return result;
                }

                setting:App.Shared.Gui.IDropDownConfig<T>;
                instance:any;
                title:string;
                id:string;
                $q:ng.IQService;
                customEvent:IDropDownCustomEvent<T>;

                private loadDeault(api:ng.IPromise<Array<T>>, firstValue:Array<T>,$q:ng.IQService=null, selectfirstValue = false):ng.IPromise<void>|any{
                    var result=$q!=null?$q.defer():null;
                    // console.log(this.id);
                    var timeout=setInterval(()=>{
                        // console.log('chay ham load default lien tuc');
                        if(!this.ready||this.instance==undefined||this.instance.element==undefined){
                            return;
                        }
                        clearInterval(timeout);
                        api.then((c:Array<any>)=> {
                        var tmp = Array<any>();
                        if (firstValue != undefined && firstValue != null) {
                            $.each(firstValue, (i:number, v:any)=> {
                                tmp.push(v);
                            })
                        }
                        if(c.length !== 0){
                            $.each(c, (i:number, v:any)=> {
                                tmp.push(v);
                            });
                        }
                        this.setting.source = new $.jqx.dataAdapter(tmp);

                        $('#' + this.instance.element.id).on('bindingComplete', (event:any)=> {
                            if(selectfirstValue){
                                this.setting.jqxDropDownList('selectIndex', 0);
                                // console.log(`binding complete ${this.id}`);
                            }
                            if(result!=null) {
                                var timeout_result=setInterval(()=>{
                                    clearInterval(timeout_result);
                                    result.resolve();
                                },50);

                            }
                        });
                        this.setting.apply();
                    });
                    },100);
                    if(result!=null)
                    return result.promise;

                }
                loadlocaldata_drop(source:ng.IPromise<Array<T>>,$q: ng.IQService =null,selectfirstValue = false,firstValue:Array<T> = null):ng.IPromise<void>|any{
                    var result=$q!=null?$q.defer():null;

                    var timeout=setInterval(()=>{
                        if(!this.ready||this.instance==undefined||this.instance.element==undefined) {
                            clearInterval(timeout);
                            return;
                        }
                        source.then((c:Array<any>)=> {
                            var tmp = Array<any>();
                            if (firstValue != undefined && firstValue != null) {
                                $.each(firstValue, (i, v)=> {
                                    tmp.push(v);
                                })
                            }
                            $.each(c, (i, v)=> {
                                tmp.push(v);
                            });

                            this.setting.source = new $.jqx.dataAdapter(tmp);

                            $('#' + this.instance.element.id).on('bindingComplete', (event)=> {
                                if(selectfirstValue){
                                    this.setting.jqxDropDownList('checkIndex', 1);
                                }
                                if(result!=null) {
                                   var timeout1=setInterval(()=>{
                                        clearInterval(timeout1);
                                        result.resolve();
                                    },50);

                                }
                            });
                            this.setting.apply();
                        });
                        clearInterval(timeout);
                    },100);
                    if(result!=null)
                        return result.promise;

                }
                loadlocaldata_dropuser(source:Array<T>,$q: ng.IQService =null,selectfirstValue = false,firstValue:Array<T> = null):ng.IPromise<void>|any{
                    var result=$q!=null?$q.defer():null;

                    // var timeout=setInterval(()=>{
                    //     if(!this.ready||this.instance==undefined||this.instance.element==undefined) {
                    //         clearInterval(timeout);
                    //         return;
                    //     }
                    //     source.then((c:Array<any>)=> {
                    //         var tmp = Array<any>();
                    //         if (firstValue != undefined && firstValue != null) {
                    //             $.each(firstValue, (i, v)=> {
                    //                 tmp.push(v);
                    //             })
                    //         }
                    //         $.each(c, (i, v)=> {
                    //             tmp.push(v);
                    //         });
                    //
                    //         this.setting.source = new $.jqx.dataAdapter(tmp);
                    //
                    //         $('#' + this.instance.element.id).on('bindingComplete', (event)=> {
                    //             if(selectfirstValue){
                    //                 this.setting.jqxDropDownList('checkIndex', 1);
                    //             }
                    //             if(result!=null) {
                    //                 timeout=setInterval(()=>{
                    //                     clearInterval(timeout);
                    //                     result.resolve();
                    //                 },50);
                    //
                    //             }
                    //         });
                    //         this.setting.apply();
                    //     });
                    //     clearInterval(timeout);
                    // },100);
                    // if(result!=null)
                    //     return result.promise;


                    var timeout = setInterval(()=>{
                        if(!this.ready||this.instance==undefined||this.instance.element==undefined){
                            clearInterval(timeout);
                            return;
                        }
                        var tmp = Array<any>();
                        if (firstValue != undefined && firstValue != null) {
                            $.each(firstValue, (i:number, v:any)=> {
                                // console.log('chay lien tuc');
                                tmp.push(v);
                            })
                        }
                        $.each(source, (i:number, v:any)=> {
                            tmp.push(v);
                        });
                        this.setting.source = new $.jqx.dataAdapter(tmp);
                        $('#' + this.instance.element.id).on('bindingComplete', (event:any)=> {
                            if(selectfirstValue){
                                this.setting.jqxDropDownList('checkIndex', 0);
                                this.setting.jqxDropDownList('selectIndex',0);
                            }
                            if(result!=null) {
                                timeout=setInterval(()=>{
                                    clearInterval(timeout);
                                    result.resolve();
                                },50);

                            }
                            this.setting.apply();
                        });
                        clearInterval(timeout);

                    },100);
                    if(result!=null)
                        return result.promise;
                }

                loadlocaldata_dropnhienlieu(source:Array<T>,$q: ng.IQService =null,selectfirstValue = false,firstValue:Array<T> = null):ng.IPromise<void>|any{
                    var result=$q!=null?$q.defer():null;

                    // var timeout=setInterval(()=>{
                    //     if(!this.ready||this.instance==undefined||this.instance.element==undefined) {
                    //         clearInterval(timeout);
                    //         return;
                    //     }
                    //     source.then((c:Array<any>)=> {
                    //         var tmp = Array<any>();
                    //         if (firstValue != undefined && firstValue != null) {
                    //             $.each(firstValue, (i, v)=> {
                    //                 tmp.push(v);
                    //             })
                    //         }
                    //         $.each(c, (i, v)=> {
                    //             tmp.push(v);
                    //         });
                    //
                    //         this.setting.source = new $.jqx.dataAdapter(tmp);
                    //
                    //         $('#' + this.instance.element.id).on('bindingComplete', (event)=> {
                    //             if(selectfirstValue){
                    //                 this.setting.jqxDropDownList('checkIndex', 1);
                    //             }
                    //             if(result!=null) {
                    //                 timeout=setInterval(()=>{
                    //                     clearInterval(timeout);
                    //                     result.resolve();
                    //                 },50);
                    //
                    //             }
                    //         });
                    //         this.setting.apply();
                    //     });
                    //     clearInterval(timeout);
                    // },100);
                    // if(result!=null)
                    //     return result.promise;


                    var timeout = setInterval(()=>{
                        if(!this.ready||this.instance==undefined||this.instance.element==undefined){
                            clearInterval(timeout);
                            return;
                        }
                        var tmp = Array<any>();
                        if (firstValue != undefined && firstValue != null) {
                            $.each(firstValue, (i:number, v:any)=> {
                                // console.log('chay lien tuc');
                                tmp.push(v);
                            })
                        }
                        $.each(source, (i:number, v:any)=> {
                            tmp.push(v);
                        });
                        this.setting.source = new $.jqx.dataAdapter(tmp);
                        $('#' + this.instance.element.id).on('bindingComplete', (event:any)=> {
                            if(selectfirstValue){
                                this.setting.jqxDropDownList('checkIndex', 0);
                                this.setting.jqxDropDownList('selectIndex',0);
                            }
                            if(result!=null) {
                                timeout=setInterval(()=>{
                                    clearInterval(timeout);
                                    result.resolve();
                                },50);

                            }
                            this.setting.apply();
                        });
                        clearInterval(timeout);

                    },100);
                    if(result!=null)
                        return result.promise;
                }
                load(api:ng.IPromise<Array<T>>, firstValue:Array<T>,selectfirstValue:boolean) {
                    this.loadDeault(api,firstValue,null,selectfirstValue);

                }
                ready:boolean;
                setItemDefault(where:(v:T)=>boolean){
                    var result=Array<any>();
                    $.each(this.setting.source._source,(i:number,val:any)=>{
                        if(where(val))
                            result.push(i);
                    });
                    if(result.length>0){
                        this.setting.jqxDropDownList('selectIndex',result[0]);
                        this.setting.apply();
                        // console.log(this.itemSelect());
                    }else{
                        this.setting.jqxDropDownList('clearSelection');
                    }
                }
                setItemDefault_xacnhanthietbi(where:(v:T)=>boolean){
                    var result=Array<any>();
                    $.each(this.setting.source._source.localdata,(i:number,val:any)=>{
                        if(where(val))
                            result.push(i);
                    });
                    if(result.length>0){
                        this.setting.jqxDropDownList('selectIndex',result[0]);
                        this.setting.apply();
                        // console.log(this.itemSelect());
                    }else{
                        this.setting.jqxDropDownList('clearSelection');
                    }
                }
                setItemDefaultChecked(where:(v:T)=>boolean){
                    var result=Array<any>();
                    $.each(this.setting.source._source,(i:number,val:any)=>{
                        if(where(val))
                            result.push(i);
                    });
                    if(result.length>0){
                        this.setting.jqxDropDownList('checkIndex',result[0]);
                        this.setting.apply();
                    }else{
                        this.setting.jqxDropDownList('clearSelection');
                    }
                }
                setItemDefaultChecked_Group(where:(v:T)=>boolean){
                    var result=Array<any>();
                    $.each(this.setting.source,(i:number,val:any)=>{
                        if(where(val))
                            result.push(i);
                    });
                    if(result.length>0){
                        this.setting.jqxDropDownList('checkIndex',result[0]);
                        this.setting.apply();
                    }
                    else{
                        // console.log('bỏ chọn xe');
                        this.setting.jqxDropDownList('clearSelection');
                    }
                }
                installEventPaste(){
                    //dropdownlistContentcbGroupQCVN31Master
                    var timeout = setInterval(()=>{
                        if(!this.ready||this.instance==undefined||this.instance.element==undefined) return;
                        $(`#dropdownlistContent${this.id} input`).on('paste',(e:any)=>{

                            e.preventDefault();
                            var text = (e.originalEvent || e).clipboardData.getData('text/plain') || prompt('Paste something..');
                            //window.document.execCommand('insertText', false, text);
                            // lấy được text => tìm ra item trong source

                            var result=Array<any>();
                            $.each(this.setting.source._source,(i,val)=>{
                                if(val[this.setting.displayMember]==text)
                                    result.push(val);
                            });
                            if(result.length>0)
                            {
                                this._itSelect=result[0];
                                this.isPaste=true;
                                //this.instance.selectItem(result[0]);
                                //this.setting.apply();
                            }
                            window.document.execCommand('insertText', true, text);
                        });
                        clearInterval(timeout);
                    },100);
                }
                constructor(id:string,displayMember:string,valueMember:string) {
                    this.setting = {};
                    this.instance = {};
                    this.customEvent={};
                    this.title = "abc";
                    this.isPaste=false;
                    // this.setting.width=250;
                    this.setting.height=40;
                    this.setting.searchMode ='containsignorecase';
                    this.setting.filterable = true;
                    this.setting.filterPlaceHolder = "Tìm xe...";
                    this.setting.checkboxes = true;
                    this.setting.popupZIndex = 999999;
                    // this.setting.autoComplete =true;
                    this.ready=false;
                    this.id=id;
                    this.setting.displayMember=displayMember;
                    this.setting.valueMember=valueMember;

                    this.installEventPaste();
                    var oldtimeload = 0; // load thời gian ban đầu
                    // this.setting.source._source[index]
                    // this.setting.change=(event:any)=>{
                    //     // if(event.timeStamp - oldtimeload<10||this.setting.checkboxes==true){
                    //     //     return;
                    //     // }
                    //     // oldtimeload = event.timeStamp;
                    //
                    //     if(
                    //         this.isPaste==true){
                    //
                    //         this.isPaste=false;
                    //         return;
                    //     }
                    //     var index:any = event.args!=undefined?event.args.index:-1;
                    //
                    //     console.log(index);
                    //     // var item=index==-1?undefined:this.setting.source._source[index];
                    //     var item:any = {};
                    //     if(index == -1){
                    //         item = undefined;
                    //     }else{
                    //         var tmp:any = this.setting.jqxComboBox('getSelectedItem');
                    //         item = tmp.originalItem;
                    //     }
                    //     this._itSelect=item;
                    //     if(this.customEvent.onChange!=undefined&&this.customEvent.onChange!=null)
                    //     {
                    //         if(this._itSelect!=undefined)
                    //         {
                    //             var timeout = setInterval(()=>{
                    //                 clearInterval(timeout);
                    //                 this.customEvent.onChange(index,item)
                    //             },100);
                    //         }
                    //     }
                    // };
                    this.setting.select = (event:any)=>{
                        if(!this.setting.checkboxes){
                            if(event.timeStamp - oldtimeload<500){
                                return;
                            }
                            oldtimeload = event.timeStamp;
                        }
                        if(
                            this.isPaste==true){

                            this.isPaste=false;
                            return;
                        }

                        // var item = event.args.item;
                        // var args = event.args;
                        // console.log(args.item.checked);
                        var index:any = event.args!=undefined?event.args.index:-1;
                        var items:any = [];
                        // if(index == -1){
                        //     items = [];
                        // }else {

                        //xử lý chọn 1 item
                        var item:any = {};
                        if(event.args.item !== undefined){
                            var tmp:any = this.setting.jqxDropDownList('getSelectedItem');
                            // console.log(tmp);
                            if(tmp !== null){
                                item = tmp.originalItem;
                            }

                        }
                        this._itSelect = item;


                        // chọn nhiều items checksbox
                        try{
                            if (event.args.item.checked == true) {
                                var tmp:any = this.setting.jqxDropDownList('getCheckedItems');
                                $.each(tmp, (ind:number, val:any)=> {
                                    items.push(val.originalItem);
                                });
                                // console.log('checked');

                            } else {
                                var tmp:any = this.setting.jqxDropDownList('getCheckedItems');
                                $.each(tmp, (ind:number, val:any)=> {
                                    items.push(val.originalItem);
                                });
                                // console.log('unchecked');
                            }
                        }catch(e){

                        }

                        // }
                        this._itChecked = items;
                        if (this.customEvent.onSelect != undefined && this.customEvent.onSelect != null) {
                            var timeout = setInterval(()=> {
                                clearInterval(timeout);
                                this.customEvent.onSelect(index, items)
                            }, 100);
                        }
                        if (this.customEvent.onSelectCheck != undefined && this.customEvent.onSelectCheck != null) {
                            var timeout = setInterval(()=> {
                                clearInterval(timeout);
                                this.customEvent.onSelectCheck(event);
                            }, 100);
                        }
                        if (this.customEvent.onSelectUser != undefined && this.customEvent.onSelectUser != null) {
                            var timeout_seluser = setInterval(()=> {
                                clearInterval(timeout_seluser);
                                this.customEvent.onSelectUser(event.args.item.checked, items)
                            }, 100);
                        }
                        if (this.customEvent.onSelectOnItem != undefined && this.customEvent.onSelectOnItem != null) {
                            var timeouts = setInterval(()=> {
                                clearInterval(timeouts);
                                this.customEvent.onSelectOnItem(index, item);
                            }, 100);
                            // this.customEvent.onSelectOnItem(index, item);
                        }



                    };

                    this.setting.checkChange = (event:any) =>{
                        // if(event.timeStamp - oldtimeload<500||this.setting.checkboxes==true){
                        //     return;
                        // }
                        // oldtimeload = event.timeStamp;
                        if (event.args) {
                            var item = event.args.item;
                            var value = item.value;
                            var label = item.label;
                            var checked = item.checked;
                        }
                        if (this.customEvent.onCheckChange != undefined && this.customEvent.onCheckChange != null) {
                            var timeout = setInterval(()=> {
                                clearInterval(timeout);
                                this.customEvent.onCheckChange(checked, item)
                            }, 100);
                        }
                    };


                }
            }
            export interface IDropdownScope extends ng.IScope{
                id:any;
            }
            export class DropDownController{
                constructor($scope:IDropdownScope,$q:ng.IQService){
                    $scope.id.ready=true;
                    $scope.id.$q=$q;
                }
            }
            export function dropdownDirective($templateCache:ng.ITemplateCacheService, $compile:ng.ICompileService, $http:ng.IHttpService): ng.IDirective {
                return {
                    // bindToController: true,
                    controller: ['$scope','$q', DropDownController],
                    name: "dropdown",
                    restrict: "EA",
                    //transclude: true,// cho phép include đoán html nằm trong thẻ grid-view
                    templateUrl:'./shared/Gui/dropdownlist.html',
                    scope: {
                        id:"="
                    },
                    replace: true// không được phép xóa element nằm trong thẻ grid-view
                }
            }
            AdModule.directive('dropdown',dropdownDirective);
        }
    }
}