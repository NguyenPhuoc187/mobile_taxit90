/**
 * Created by user on 07/21/2016.
 */

/// <reference path="../../../tstyping/angularjs/angular.d.ts" />
module App {
    export module Shared {
        export module Gui {
            import Promise = Q.Promise;
            export interface IComboboxConfig<T>{
                animationType?:	string;
                autoComplete?:	boolean;
                autoOpen?:	boolean;
                autoItemsHeight?:	boolean;
                autoDropDownHeight?:	boolean;
                closeDelay?:	number;
                checkboxes?:	boolean;
                disabled?:	boolean;
                displayMember?:	string;
                dropDownHorizontalAlignment?:	string;
                dropDownVerticalAlignment?:	string;
                dropDownHeight?:	number;
                dropDownWidth?:	number;
                enableHover?:	boolean;
                enableSelection?:	boolean;
                enableBrowserBoundsDetection?:	boolean;
                height?:	number|string;
                itemHeight?:	number;
                multiSelect?:	boolean;
                minLength?:	number;
                openDelay?:	number;
                popupZIndex?:	number;
                placeHolder?:	string;
                remoteAutoComplete?:	boolean;
                remoteAutoCompleteDelay?:	number;
                renderer?:	Function;
                renderSelectedItem?:	Function;
                rtl?:	boolean;
                selectedIndex?:	number;
                showArrow?:	boolean;
                showCloseButtons?:	boolean;
                searchMode?:	string;
                search?:	Function;
                source?:	any;
                scrollBarSize?:	number;
                template?:	string;
                theme?:	string;
                validateSelection?:	Function;
                valueMember?:	string;
                width?:number|string;
                change?:(event:any)=>void;
                select?:(event:any)=>void;
                close?:(event: any)=>void;
                unselect?:(event:any)=>void;
                jqxComboBox?:any;
                apply?():void;
            }
            export interface IComboboxCustomEvent<T>{
                onChange?:(index:number,val:T)=>void;
                onSelect?:(index:number, val:Array<T>)=>void;

            }
            export interface ICombobox<T>{
                setting:IComboboxConfig<T>;
                instance:any;
                title:string;
                customEvent?:IComboboxCustomEvent<T>;
                loadPromise:(api:ng.IPromise<Array<T>>,firstValue:Array<T>,$q:ng.IQService)=>ng.IPromise<void>;
                load:(api:ng.IPromise<Array<T>>,firstValue:Array<T>,selectfirstValue?:boolean)=>void;
                ready:boolean;
                id:string;
                itemSelect():T;
                // itemChecked()?:Array<T>;
                valueSelects():Array<any>;
                allValue():Array<any>;
                setModeCheckbox(val:boolean):void;
                itemsMutiSelect(): Array<any>;
                setItemDefault:(where:(v:T)=>boolean)=> any;
                setItemDefaultChecked:(where:(v:T)=>boolean)=> any;
                loadlocaldata:(source:any,$q: ng.IQService,selectfirstValue?:boolean,firstValue?:Array<T>) =>ng.IPromise<void>;
                loaddataarraywithapi:(source:ng.IPromise<Array<T>>,$q: ng.IQService,selectfirstValue?:boolean,firstValue?:Array<T>) =>ng.IPromise<void>;
            }
            export class Combobox<T> implements ICombobox<T>{
                private isPaste:boolean;
                loadPromise(api:ng.IPromise<Array<T>>, firstValue:Array<T>, $q:angular.IQService):ng.IPromise<void>{
                    var result=$q.defer<void>();
                    this.loadDeault(api,firstValue,$q).then(()=>result.resolve());
                    return result.promise;
                }
                setModeCheckbox(val:boolean):void {
                    this.setting.checkboxes=val;
                    //this.instance.refresh();
                    //this.instance.render();
                }
                _itSelect:T;
                itemSelect():T{
                    if(this._itSelect==undefined) return undefined;


                    //var inputs=$(`#dropdownlistContent${this.id} input`);
                    //if(inputs.length==0) return undefined;
                    //var input=inputs[0];
                    //if(this._itSelect[this.setting.displayMember]==input.value)
                    //    return this._itSelect;
                    //this._itSelect=undefined;
                    return this._itSelect;
                };
                _itChecked:Array<T>;
                // itemChecked():Array<T>{
                //     if(this._itChecked.length == 0) return undefined;
                //     //var inputs=$(`#dropdownlistContent${this.id} input`);
                //     //if(inputs.length==0) return undefined;
                //     //var input=inputs[0];
                //     //if(this._itSelect[this.setting.displayMember]==input.value)
                //     //    return this._itSelect;
                //     //this._itSelect=undefined;
                //     return this._itChecked;
                // }

                allValue():Array<any>{
                    var result=Array<any>();
                    $.each(this.setting.source._source,(i:number,val:any)=>{
                        result.push(val[this.setting.valueMember]);
                    });
                    return result;
                }
                valueSelects():Array<any>{
                    var checks=this.instance.getCheckedItems();
                    var result=Array<any>();
                    $.each(checks,(i:number,val:any)=>{
                        result.push(val.value);
                    });
                    return result;
                }
                itemsMutiSelect():Array<any>{
                    var checks = this.instance.getSelectedItems();
                    var result = Array<any>();
                    $.each(checks,(i:number,val:any)=>{
                        result.push(val.value);
                    });
                    return result;
                }

                setting:App.Shared.Gui.IComboboxConfig<T>;
                instance:any;
                title:string;
                id:string;
                $q:ng.IQService;
                customEvent:IComboboxCustomEvent<T>;
                
                private loadDeault(api:ng.IPromise<Array<T>>, firstValue:Array<T>,$q:ng.IQService=null, selectfirstValue = true):ng.IPromise<void>|any{
                    var result=$q!=null?$q.defer():null;

                    var timeout=setInterval(()=>{
                        if(!this.ready||this.instance==undefined||this.instance.element==undefined) return;
                        api.then((c:Array<any>)=> {
                            var tmp = Array<any>();
                            if (firstValue != undefined && firstValue != null) {
                                $.each(firstValue, (i, v)=> {
                                    tmp.push(v);
                                })
                            }
                            $.each(c, (i, v)=> {
                                tmp.push(v);
                            });

                            this.setting.source = new $.jqx.dataAdapter(tmp);

                            $('#' + this.instance.element.id).on('bindingComplete', (event)=> {
                                if(selectfirstValue){
                                    this.setting.jqxComboBox('selectIndex', 0);
                                }
                                if(result!=null) {
                                    timeout=setInterval(()=>{
                                        clearInterval(timeout);
                                        result.resolve();
                                    },50);

                                }
                            });
                            this.setting.apply();
                        });
                        clearInterval(timeout);
                    },100);
                    if(result!=null)
                    return result.promise;
                }
                //load data với mảng có trong project
                loadlocaldata(source:Array<T>,$q: ng.IQService =null,selectfirstValue = false,firstValue:Array<T> = null):ng.IPromise<void>|any{
                    var result=$q!=null?$q.defer():null;
                    if(source.length == 0 ){
                        return;
                    }else{
                        var timeout = setInterval(()=>{
                            if(!this.ready || this.instance==undefined || this.instance.element==undefined){
                                clearInterval(timeout);
                                // console.log('trong if');
                                return;
                            }

                            var tmp = Array<any>();
                            if (firstValue != undefined && firstValue != null) {
                                $.each(firstValue, (i, v)=> {
                                    tmp.push(v);
                                })
                            }
                            $.each(source, (i, v)=> {
                                tmp.push(v);
                            });
                            // console.log(`id của ${this.instance.element.id}`);
                            this.setting.source = new $.jqx.dataAdapter(tmp);
                            $('#' + this.instance.element.id).on('bindingComplete', (event)=> {
                                if(selectfirstValue){
                                    this.setting.jqxComboBox('selectIndex', 0);
                                }

                                // console.log(`ngoài if ${this.instance.element.id}`);
                                if(result!=null) {
                                    var timeout=setInterval(()=>{
                                        clearInterval(timeout);
                                        result.resolve();
                                    },50);

                                }
                                this.setting.apply();
                            });
                            clearInterval(timeout);
                        },100);
                    }

                    if(result!=null)
                    return result.promise;
                }

                loadlocaldatamap(source:Array<T>,$q: ng.IQService =null,selectfirstValue = false,firstValue:Array<T> = null):ng.IPromise<void>|any{
                    var result=$q!=null?$q.defer():null;
                    if(source.length == 0 ){
                        return;
                    }else{
                        var timeout = setInterval(()=>{
                            if(!this.ready || this.instance==undefined || this.instance.element==undefined){
                                console.log('trong if');
                                clearInterval(timeout);
                                return;
                            }

                            var tmp = Array<any>();
                            if (firstValue != undefined && firstValue != null) {
                                $.each(firstValue, (i, v)=> {
                                    tmp.push(v);
                                })
                            }
                            $.each(source, (i, v)=> {
                                tmp.push(v);
                            });
                            console.log(`id của ${this.instance.element.id}`);
                            this.setting.source = new $.jqx.dataAdapter(tmp);
                            $('#' + this.instance.element.id).on('bindingComplete', (event)=> {
                                if(selectfirstValue){
                                    this.setting.jqxComboBox('selectIndex', 0);
                                }

                                // console.log(`ngoài if ${this.instance.element.id}`);
                                if(result!=null) {
                                    var timeout=setInterval(()=>{
                                        clearInterval(timeout);
                                        result.resolve();
                                    },50);

                                }
                                this.setting.apply();
                            });
                            clearInterval(timeout);
                        },100);
                    }

                    if(result!=null)
                        return result.promise;
                }


                //load data về rồi push vào mảng sau đó gán vào source
                loaddataarraywithapi(source:ng.IPromise<Array<T>>,$q: ng.IQService =null,selectfirstValue = false,firstValue:Array<T> = null):ng.IPromise<void>|any{
                    var result=$q!=null?$q.defer():null;
                    var timeout=setInterval(()=>{
                        if(!this.ready||this.instance==undefined||this.instance.element==undefined){
                            console.log('if loaddata');
                            clearInterval(timeout);
                            return;
                        }
                        source.then((c:Array<any>)=> {
                            var tmp = Array<any>();
                            if (firstValue != undefined && firstValue != null) {
                                $.each(firstValue, (i, v)=> {
                                    tmp.push(v);
                                })
                            }
                            $.each(c, (i, v)=> {
                                tmp.push(v);
                            });

                            this.setting.source = new $.jqx.dataAdapter(tmp);

                            $('#' + this.instance.element.id).on('bindingComplete', (event)=> {
                                if(selectfirstValue){
                                    this.setting.jqxComboBox('selectIndex', 0);
                                }
                                if(result!=null) {
                                    timeout=setInterval(()=>{
                                        clearInterval(timeout);
                                        result.resolve();
                                    },50);

                                }
                            });
                            this.setting.apply();
                        });
                        clearInterval(timeout);
                    },100);
                    if(result!=null)
                        return result.promise;
                }

                load(api:ng.IPromise<Array<T>>, firstValue:Array<T>,selectfirstValue:boolean) {
                    this.loadDeault(api,firstValue,null,selectfirstValue);

                }
                ready:boolean;
                setItemDefault(where:(v:T)=>boolean){
                    var result=Array<any>();
                    // console.log(this.setting.source);
                    $.each(this.setting.source._source,(i:number,val:any)=>{
                        if(where(val))
                            result.push(i);
                    });
                    if(result.length>0){
                        this.setting.jqxComboBox('selectIndex',result[0]);
                        this.setting.apply();
                        // console.log(this.itemSelect());
                    }else{
                        this.setting.jqxComboBox('clearSelection');
                    }
                }
                setItemDefaultChecked(where:(v:T)=>boolean){
                    var result=Array<any>();
                    $.each(this.setting.source._source,(i,val)=>{
                        if(where(val))
                            result.push(i);
                    });
                    // for(var i =0; i< result.length; i++){
                    //     this.setting.jqxComboBox('checkIndex',i);
                    //
                    // }
                    if(result.length>0){

                        this.setting.jqxComboBox('checkIndex',result[0]);
                        this.setting.apply();
                        // console.log(this.itemSelect());
                    }else{
                        this.setting.jqxComboBox('clearSelection');
                    }
                }
                installEventPaste(){
                    //dropdownlistContentcbGroupQCVN31Master
                    var timeout = setInterval(()=>{
                        if(!this.ready||this.instance==undefined||this.instance.element==undefined) return;
                        $(`#dropdownlistContent${this.id} input`).on('paste',(e:any)=>{

                            e.preventDefault();
                            var text = (e.originalEvent || e).clipboardData.getData('text/plain') || prompt('Paste something..');
                            //window.document.execCommand('insertText', false, text);
                            // lấy được text => tìm ra item trong source

                            var result=Array<any>();
                            $.each(this.setting.source._source,(i,val)=>{
                                if(val[this.setting.displayMember]==text)
                                    result.push(val);
                            });
                            if(result.length>0)
                            {
                                this._itSelect=result[0];
                                this.isPaste=true;
                                //this.instance.selectItem(result[0]);
                                //this.setting.apply();
                            }
                            window.document.execCommand('insertText', true, text);
                        });
                        clearInterval(timeout);
                    },100);
                }
                constructor(id:string,displayMember:string,valueMember:string) {
                    this.setting = {};
                    this.instance = {};
                    this.customEvent={};
                    this.title = "abc";
                    this.isPaste=false;
                    // this.setting.width=250;
                    this.setting.height=25;
                    this.setting.searchMode ='containsignorecase';
                    this.setting.autoComplete =true;
                    this.ready=false;
                    this.id=id;
                    this.setting.displayMember=displayMember;
                    this.setting.valueMember=valueMember;

                    this.installEventPaste();
                    var oldtimeload = 0; // load thời gian ban đầu
                    // this.setting.source._source[index]
                    // this.setting.change=(event:any)=>{
                    //     // if(event.timeStamp - oldtimeload<10||this.setting.checkboxes==true){
                    //     //     return;
                    //     // }
                    //     // oldtimeload = event.timeStamp;
                    //
                    //     if(
                    //         this.isPaste==true){
                    //
                    //         this.isPaste=false;
                    //         return;
                    //     }
                    //     var index:any = event.args!=undefined?event.args.index:-1;
                    //
                    //     console.log(index);
                    //     // var item=index==-1?undefined:this.setting.source._source[index];
                    //     var item:any = {};
                    //     if(index == -1){
                    //         item = undefined;
                    //     }else{
                    //         var tmp:any = this.setting.jqxComboBox('getSelectedItem');
                    //         item = tmp.originalItem;
                    //     }
                    //     this._itSelect=item;
                    //     if(this.customEvent.onChange!=undefined&&this.customEvent.onChange!=null)
                    //     {
                    //         if(this._itSelect!=undefined)
                    //         {
                    //             var timeout = setInterval(()=>{
                    //                 clearInterval(timeout);
                    //                 this.customEvent.onChange(index,item)
                    //             },100);
                    //         }
                    //     }
                    // };
                    this.setting.select = (event:any)=>{
                        // if(event.timeStamp - oldtimeload<500||this.setting.checkboxes==true){
                        //     return;
                        // }
                        // oldtimeload = event.timeStamp;
                        if(
                            this.isPaste==true){

                            this.isPaste=false;
                            return;
                        }
                            // var item = event.args.item;
                            // var args = event.args;
                            // console.log(args.item.checked);
                        var index:any = event.args!=undefined?event.args.index:-1;
                        var items:any = [];
                        if(index == -1){
                            items = [];
                        }else {
                            if (event.args.item.checked == true) {
                                var tmp:any = this.setting.jqxComboBox('getCheckedItems');
                                $.each(tmp, (ind, val)=> {
                                    items.push(val.originalItem);
                                });
                                // console.log('checked');

                            } else {
                                var tmp:any = this.setting.jqxComboBox('getCheckedItems');
                                $.each(tmp, (ind, val)=> {
                                    items.push(val.originalItem);
                                });
                                // console.log('unchecked');
                            }
                        }
                        this._itChecked = items;
                        if (this.customEvent.onSelect != undefined && this.customEvent.onSelect != null) {
                            var timeout = setInterval(()=> {
                                clearInterval(timeout);
                                this.customEvent.onSelect(index, items)
                            }, 100);
                        }

                        //xử lý select
                        var item:any = {};

                        if(index == -1){
                            item = this._itSelect;
                            return
                        }else{
                            var tmp:any = this.setting.jqxComboBox('getSelectedItem');
                            item = tmp.originalItem;
                        }
                        this._itSelect=item;
                        if(this.customEvent.onChange!=undefined&&this.customEvent.onChange!=null)
                        {
                            if(this._itSelect!=undefined)
                            {
                                var timeout = setInterval(()=>{
                                    clearInterval(timeout);
                                    this.customEvent.onChange(index,item)
                                },100);
                            }
                        }


                    };


                }
            }
            export interface IComboboxScope extends ng.IScope{
                id:any;
            }
            export class ComboboxController{
                constructor($scope:IComboboxScope,$q:ng.IQService){
                    $scope.id.ready=true;
                    $scope.id.$q=$q;
                }
            }
            export function comboboxDirective($templateCache:ng.ITemplateCacheService, $compile:ng.ICompileService, $http:ng.IHttpService): ng.IDirective {
                return {
                    // bindToController: true,
                    controller: ['$scope','$q', ComboboxController],
                    name: "combobox",
                    restrict: "EA",
                    //transclude: true,// cho phép include đoán html nằm trong thẻ grid-view
                    templateUrl:'./shared/Gui/combobox.html',
                    scope: {
                        id:"="
                    },
                    replace: true// không được phép xóa element nằm trong thẻ grid-view
                }
            }
            AdModule.directive('combobox',comboboxDirective);
        }
    }
}