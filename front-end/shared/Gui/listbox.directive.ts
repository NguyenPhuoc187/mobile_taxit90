/**
 * Created by NPPRO on 22/11/2016.
 */
/// <reference path="../../../tstyping/angularjs/angular.d.ts" />
module App{
    export module Shared{
        export module Gui{
            export interface IListBoxConfig<T>{
                autoOpen?:boolean;
                autoItemsHeight?:boolean;
                animationType?:string;
                checkboxes?:boolean;
                closeDelay?:number;
                disabled?:boolean;
                displayMember?:string;
                dropDownHorizontalAlignment?:string;
                dropDownVerticalAlignment?:string;
                dropDownHeight?:number;
                dropDownWidth?:number;
                enableSelection?:boolean;
                enableBrowserBoundsDetection?:boolean;
                enableHover?:boolean;
                filterable?:boolean;
                filterHeight?:number;
                filterDelay?:number;
                filterPlaceHolder?:string;
                height?:number|string;
                incrementalSearch?:boolean;
                incrementalSearchDelay?:number;
                itemHeight?:number;
                openDelay?:number;
                placeHolder	?:string;
                popupZIndex	?:number;
                rtl?:boolean;
                renderer?:Function;
                selectionRenderer?:Function;
                searchMode?:string;
                scrollBarSize?:number;
                source?:any;
                selectedIndex?:number;
                template?:string;
                theme?:string;
                valueMember?:string;
                width?:number|string;
                change?:(event:any)=>void;
                select?:(event:any)=>void;
                close?:(event: any)=>void;
                unselect?:(event:any)=>void;
                jqxListBox?:any;
                apply?():void;
                checkChange?:(event:any) => void;
            }
            export interface IListBoxCustomEvent<T>{
                onCheckChange?:(check:boolean,val:T)=>void;
                onSelect?:(index:number, val:Array<T>)=>void;
                onSelectOnItem?:(index:number, val:T)=>void;
                onSelectCheck?:(event:any)=>void;
                onSelectUser?:(check:boolean, val:Array<T>)=> void;

            }
            export interface IListBox<T>{
                setting:IListBoxConfig<T>;
                instance:any;
                title:string;
                customEvent?:IListBoxCustomEvent<T>;
                loadPromise:(api:ng.IPromise<Array<T>>,firstValue:Array<T>,$q:ng.IQService)=>ng.IPromise<void>;
                load:(api:ng.IPromise<Array<T>>,firstValue:Array<T>,selectfirstValue?:boolean)=>void;
                ready:boolean;
                id:string;
                itemSelect():T;
                // itemChecked()?:Array<T>;
                valueSelects():Array<any>;
                allValue():Array<any>;
                setModeCheckbox(val:boolean):void;
                itemsMutiSelect(): Array<any>;
                setItemDefault:(where:(v:T)=>boolean)=> any;
                setItemDefaultChecked:(where:(v:T)=>boolean)=> any;
                setItemDefaultChecked_Group:(where:(v:T)=>boolean)=> any;
                loadlocaldata_drop:(source:ng.IPromise<Array<T>>,$q: ng.IQService,selectfirstValue?:boolean,firstValue?:Array<T>) =>ng.IPromise<void>;
            }
            export class ListBox<T> implements ListBox<T>{
                private isPaste:boolean;

                ready:boolean;
                setting:App.Shared.Gui.IListBoxConfig<T>;
                instance:any;
                title:string;
                id:string;
                $q:ng.IQService;
                customEvent:IListBoxCustomEvent<T>;
                loadlocaldata_dropuser(source:Array<T>,$q: ng.IQService =null,firstValue:Array<T> = null):ng.IPromise<void>|any{
                    var result=$q!=null?$q.defer():null;

                    // var timeout=setInterval(()=>{
                    //     if(!this.ready||this.instance==undefined||this.instance.element==undefined) {
                    //         clearInterval(timeout);
                    //         return;
                    //     }
                    //     source.then((c:Array<any>)=> {
                    //         var tmp = Array<any>();
                    //         if (firstValue != undefined && firstValue != null) {
                    //             $.each(firstValue, (i, v)=> {
                    //                 tmp.push(v);
                    //             })
                    //         }
                    //         $.each(c, (i, v)=> {
                    //             tmp.push(v);
                    //         });
                    //
                    //         this.setting.source = new $.jqx.dataAdapter(tmp);
                    //
                    //         $('#' + this.instance.element.id).on('bindingComplete', (event)=> {
                    //             if(selectfirstValue){
                    //                 this.setting.jqxDropDownList('checkIndex', 1);
                    //             }
                    //             if(result!=null) {
                    //                 timeout=setInterval(()=>{
                    //                     clearInterval(timeout);
                    //                     result.resolve();
                    //                 },50);
                    //
                    //             }
                    //         });
                    //         this.setting.apply();
                    //     });
                    //     clearInterval(timeout);
                    // },100);
                    // if(result!=null)
                    //     return result.promise;


                    var timeout = setInterval(()=>{
                        if(!this.ready||this.instance==undefined||this.instance.element==undefined){
                            clearInterval(timeout);
                            return;
                        }
                        var tmp = Array<any>();
                        if (firstValue != undefined && firstValue != null) {
                            $.each(firstValue, (i:number, v:any)=> {
                                console.log('chay lien tuc');
                                tmp.push(v);
                            })
                        }
                        $.each(source, (i:number, v:any)=> {
                            tmp.push(v);
                        });
                        this.setting.source = new $.jqx.dataAdapter(tmp);
                        $('#' + this.instance.element.id).on('bindingComplete', (event:any)=> {

                            if(result!=null) {
                                timeout=setInterval(()=>{
                                    clearInterval(timeout);
                                    result.resolve();
                                },50);

                            }
                            this.setting.apply();
                        });
                        clearInterval(timeout);

                    },100);
                    if(result!=null)
                        return result.promise;
                }

                constructor(id:string,displayMember:string,valueMember:string) {
                    this.setting = {};
                    this.instance = {};
                    this.customEvent={};
                    this.title = "abc";
                    // this.isPaste=false;
                    // this.setting.width=250;
                    this.setting.height=25;
                    // this.setting.searchMode ='containsignorecase';
                    // this.setting.filterable = true;
                    // this.setting.filterPlaceHolder = "Tìm xe...";
                    // this.setting.checkboxes = true;
                    // this.setting.popupZIndex = 999999;
                    // this.setting.autoComplete =true;
                    this.ready=false;
                    this.id=id;
                    this.setting.displayMember=displayMember;
                    this.setting.valueMember=valueMember;

                    // this.installEventPaste();
                    var oldtimeload = 0; // load thời gian ban đầu
                    // this.setting.source._source[index]
                    // this.setting.change=(event:any)=>{
                    //     // if(event.timeStamp - oldtimeload<10||this.setting.checkboxes==true){
                    //     //     return;
                    //     // }
                    //     // oldtimeload = event.timeStamp;
                    //
                    //     if(
                    //         this.isPaste==true){
                    //
                    //         this.isPaste=false;
                    //         return;
                    //     }
                    //     var index:any = event.args!=undefined?event.args.index:-1;
                    //
                    //     console.log(index);
                    //     // var item=index==-1?undefined:this.setting.source._source[index];
                    //     var item:any = {};
                    //     if(index == -1){
                    //         item = undefined;
                    //     }else{
                    //         var tmp:any = this.setting.jqxComboBox('getSelectedItem');
                    //         item = tmp.originalItem;
                    //     }
                    //     this._itSelect=item;
                    //     if(this.customEvent.onChange!=undefined&&this.customEvent.onChange!=null)
                    //     {
                    //         if(this._itSelect!=undefined)
                    //         {
                    //             var timeout = setInterval(()=>{
                    //                 clearInterval(timeout);
                    //                 this.customEvent.onChange(index,item)
                    //             },100);
                    //         }
                    //     }
                    // };
                    this.setting.select = (event:any)=>{
                        if(!this.setting.checkboxes){
                            if(event.timeStamp - oldtimeload<500){
                                return;
                            }
                            oldtimeload = event.timeStamp;
                        }
                        if(
                            this.isPaste==true){

                            this.isPaste=false;
                            return;
                        }

                        // var item = event.args.item;
                        // var args = event.args;
                        // console.log(args.item.checked);
                        var index:any = event.args!=undefined?event.args.index:-1;
                        var items:any = [];
                        // if(index == -1){
                        //     items = [];
                        // }else {

                        //xử lý chọn 1 item
                        var item:any = {};
                        if(event.args.item !== undefined){
                            var tmp:any = this.setting.jqxListBox('getSelectedItem');
                            // console.log(tmp);
                            if(tmp !== null){
                                item = tmp.originalItem;
                            }

                        }
                        this._itSelect = item;


                        // chọn nhiều items checksbox
                        try{
                            if (event.args.item.checked == true) {
                                var tmp:any = this.setting.jqxListBox('getCheckedItems');
                                $.each(tmp, (ind:number, val:any)=> {
                                    items.push(val.originalItem);
                                });
                                // console.log('checked');

                            } else {
                                var tmp:any = this.setting.jqxListBox('getCheckedItems');
                                $.each(tmp, (ind:number, val:any)=> {
                                    items.push(val.originalItem);
                                });
                                // console.log('unchecked');
                            }
                        }catch(e){

                        }

                        // }
                        this._itChecked = items;
                        if (this.customEvent.onSelect != undefined && this.customEvent.onSelect != null) {
                            var timeout = setInterval(()=> {
                                clearInterval(timeout);
                                this.customEvent.onSelect(index, items)
                            }, 100);
                        }
                        if (this.customEvent.onSelectCheck != undefined && this.customEvent.onSelectCheck != null) {
                            var timeout = setInterval(()=> {
                                clearInterval(timeout);
                                this.customEvent.onSelectCheck(event);
                            }, 100);
                        }
                        if (this.customEvent.onSelectUser != undefined && this.customEvent.onSelectUser != null) {
                            var timeout_seluser = setInterval(()=> {
                                clearInterval(timeout_seluser);
                                this.customEvent.onSelectUser(event.args.item.checked, items)
                            }, 100);
                        }
                        if (this.customEvent.onSelectOnItem != undefined && this.customEvent.onSelectOnItem != null) {
                            var timeouts = setInterval(()=> {
                                clearInterval(timeouts);
                                this.customEvent.onSelectOnItem(index, item);
                            }, 100);
                            // this.customEvent.onSelectOnItem(index, item);
                        }



                    };

                    this.setting.checkChange = (event:any) =>{
                        // if(event.timeStamp - oldtimeload<500||this.setting.checkboxes==true){
                        //     return;
                        // }
                        // oldtimeload = event.timeStamp;
                        if (event.args) {
                            var item = event.args.item;
                            var value = item.value;
                            var label = item.label;
                            var checked = item.checked;
                        }
                        if (this.customEvent.onCheckChange != undefined && this.customEvent.onCheckChange != null) {
                            var timeout = setInterval(()=> {
                                clearInterval(timeout);
                                this.customEvent.onCheckChange(checked, item)
                            }, 100);
                        }
                    };


                }
            }
            export interface IListBoxScope extends ng.IScope{
                id:any;
            }
            export class ListBoxController{
                constructor($scope:IListBoxScope,$q:ng.IQService){
                    $scope.id.ready=true;
                    $scope.id.$q=$q;
                }
            }
            export function listboxDirective($templateCache:ng.ITemplateCacheService, $compile:ng.ICompileService, $http:ng.IHttpService): ng.IDirective {
                return {
                    // bindToController: true,
                    controller: ['$scope','$q', ListBoxController],
                    name: "listbox",
                    restrict: "EA",
                    //transclude: true,// cho phép include đoán html nằm trong thẻ grid-view
                    templateUrl:'./shared/Gui/listbox.html',
                    scope: {
                        id:"="
                    },
                    replace: true// không được phép xóa element nằm trong thẻ grid-view
                }
            }
            AdModule.directive('listbox',listboxDirective);
        }
    }
}