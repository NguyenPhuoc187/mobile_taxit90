/**
 * Created by phamn on 19/08/2016.
 */
module App {
    export module Shared {
        export module Gui {

            export interface IDateTimeControl {
                titleTime:string;
                id:string;
                value_default:any;
                init():void;
                value: any;
                open:string;
                limitday:number;
                minday?:any;
                picker:any;
                inputpicker:any;
                onSelect:()=>void;
                valuetime: string;
                valuetime_default:string
                GetTimeClockDefault:() =>any;
            }

            export class DateTimeControl implements IDateTimeControl{
                onSelect:()=>void;

                GetPickerDate_upserver():string{
                    this.inputpicker = $(`#${this.id}`).pickadate().pickadate('picker');
                    var picker:any = this.inputpicker.get('select','mm/dd/yyyy');
                    // console.log(this.inputpicker);
                    return picker;
                }
                GetPickerDate_setmin():string{
                    this.inputpicker = $(`#${this.id}`).pickadate().pickadate('picker');
                    var picker:any = this.inputpicker.get('select','dd/mm/yyyy');
                    // console.log(this.inputpicker);

                    return picker;
                }
                SetMinDatePicker(a:string){
                    this.inputpicker = $(`#${this.id}`).pickadate().pickadate('picker');
                    var picker:any = this.inputpicker.set('min',`${a}`);
                }


                changeTime_CarJourney(time:any):string{
                    var year:string = time.slice(12,16);
                    var month:string = time.slice(9,11);
                    var date:string = time.slice(6,8);

                    var hour:string = time.slice(0,5);

                    var tt:any = hour +':00 ' +month + '-' + date + '-' + year ;
                    return tt;
                }

                changeTime(time:any):string{
                    var year:string = time.slice(12,16);
                    var month:string = time.slice(9,11);
                    var date:string = time.slice(6,8);

                    var hour:string = time.slice(0,5);

                    var tt:any = hour +' ' +month + '/' + date + '/' + year ;
                    return tt;
                }

                titleTime:string;
                id:string;
                open:string;
                limitday:number;
                minday:any;
                value_default:any;
                value:any;
                picker:any;
                inputpicker:any;
                valuetime: string;
                valuetime_default:string;


                GetTimeClockDefault(){
                    if($(`#${this.id}_time`).val() !== null){
                        return $(`#${this.id}_time`).val();
                    }else{
                        return this.valuetime_default;
                    }

                }
                init(){
                    this.loadtime();
                }
                loadtime() {
                    //Here your view content is fully loaded !!
                    // var tomorrow = new Date();
                    // //tomorrow.setDate(tomorrow.getDate() - 1);
                    // var day:any = tomorrow.getDate();
                    // var month: any = tomorrow.getMonth() + 1;
                    // console.log(month);
                    // var year: any = tomorrow.getFullYear();
                    //
                    // var config_time: string = '23:59' + day + '/' + month + '/' + year;
                    // console.log(config_time);
                    $('.demo i').click(function () {
                        $(this).parent().find('input').click();
                    });
                    $(`#${this.id}`).pickadate({

                        monthsFull: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
                        monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                        weekdaysFull: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                        weekdaysShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
                        showMonthsShort: false,
                        showWeekdaysFull: false,

                        firstDay: 1,

                        format: 'dd-mm-yyyy',
                        formatSubmit: 'mm/dd/yyyy',
                        hiddenPrefix: undefined,
                        hiddenSuffix: '_submit',
                        hiddenName: true,

                        min: this.minday,
                        // `true` sets it to today. `false` removes any limits.
                        max: true,

                        /*button*/
                        today: 'Hôm nay',
                        clear: 'Xóa',
                        close: 'Đóng',
                        onSet: (value:any)=> {
                            console.log(value);
                            if(this.onSelect !==null && this.onSelect !== undefined ){
                                this.onSelect();
                                // console.log(value);
                            }

                        },

                    });
                    $(`#${this.id}_time`).clockpicker({
                        placement: 'bottom',
                        align: 'right',
                        donetext: 'Đồng ý',
                        autoclose: true,
                        default: this.valuetime_default
                    });
                    $(`#${this.id}_time`).val(this.valuetime_default);

                }
                constructor(id:string){

                    this.id=id;
                    // this.picker = $(`#${this.id}`).pickadate('get').default;
                    this.open = 'left';
                    this.limitday = 31;
                    this.minday = -7;
                }
            }

            export interface IDateTimeScope extends ng.IScope {
                id:any;
                id_time:any;
            }

            export class DateTimeController{
                constructor($scope:IDateTimeScope,global:App.Shared.GlobalService){
                    $scope.id.value_default= global.getnowday();
                }
            }

            export function datetimeDirective($templateCache:ng.ITemplateCacheService, $compile:ng.ICompileService, $http:ng.IHttpService): ng.IDirective {
                return {
                    // bindToController: true,
                    controller: ['$scope','global', DateTimeController],
                    name: "datetime",
                    restrict: "EA",
                    //transclude: true,// cho phép include đoán html nằm trong thẻ grid-view
                    templateUrl:'./shared/Gui/datetime.html',
                    scope: {
                        id:"="
                    },
                    replace: true// không được phép xóa element nằm trong thẻ grid-view
                }
            }
            AdModule.directive('datetime',datetimeDirective);
        }
    }
}