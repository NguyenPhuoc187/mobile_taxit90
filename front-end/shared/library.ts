/**
 * Created by phamn on 19/04/2016.
 */
interface HTMLElement{
    currentStyle:any;
    XLS:any;
}
interface JQuery {
    DataTable(options?: any): JQuery;
    jqxComboBox:(p:string,p1:any)=> JQuery;
    jqxDataTable:(a:any)=>JQuery;
    tooltipster:(p:any)=>JQuery;
    accordionze:(p:any)=>JQuery;
    mThumbnailScroller: any;
    quicksearch:(a1:string,a2:string)=>JQuery;
    datetimepicker:(p:any)=>JQuery;
    timepicker:(p:any)=>JQuery;
    autocomplete:(p:any)=>JQuery;
    livequery:() =>JQuery;
    jqxFileUpload:(a:any)=>JQuery;
    jGrowl:()=>JQuery;

}

//declare var DataTable: JQueryStatic;
interface JQueryStatic{
    jqx:any;
    c3:any;
    easing:any;
}

interface EventTarget{
    getAttribute:any;
}
interface HTMLElement{
    getContext(val:string):any;
   
}

interface Window {
    InfoBox:any;
}
