module App {
    export module Shared {
        export module Library {
            export var GlobalCarTaxiManager: CarTaxiManager;

            export interface MapConfig {
                map: google.maps.Map | any;
                q: ng.IQService;
            }

            export interface CarConfig extends MapConfig {
                $http: ng.IHttpService;
                global?: GlobalService;
            }

            export class CarTaxiManager {
                public isOk: boolean = false;
                private cfg: CarConfig;
                private allCars: IDictionary<string, CarTaxiView>;
                private listenerArray: Array<any>;
                private connection: any = null;
                private eventBoundChange: google.maps.MapsEventListener;

                private statictis: any;

                private isSunTaxi: boolean;


                private xhttp = new XMLHttpRequest();

                constructor(config: CarConfig) {
                    this.cfg = config;
                    this.allCars = new Dictionary<string, CarTaxiView>();
                    this.listenerArray = Array<any>();
                    this.installEvent();

                }

                public onStatictis: (st: any) => void;

                public ConnectTaxiSocket(companyId: number, grId: number) {
                    if (this.cfg.global != null && this.cfg.global != undefined) {
                        var groupid: number;
                        if (this.connection != null)
                            this.connection.stop();
                        this.connection = $.hubConnection(this.cfg.global.ReportHost);

                        this.connection.qs = {
                            'token': this.cfg.global.Token.GetValue(),
                            'companyId': companyId,
                            'groupId': grId
                        };
                        var proxy = this.connection.createHubProxy('DeviceStatusHub');
                        var seft = this;
                        // đếm số lượng trạng thái xe
                        proxy.on("UpdateTaxi", (car: any) => {
                            // console.log(car);
                            seft.onUpdateDevice(car);
                        });

                        console.log(proxy);
                        this.connection.start().then(function () {
                            console.log('Connect Ok');
                            groupid = grId;
                            return proxy;

                        }).fail(function () {
                            console.error('Connect Fail');
                        });
                        // this.connection.bind('onDisconnect',(e:any,data:any)=>{
                        //     this.connection.reconnecting();
                        // });
                        this.connection.stateChanged((change: any) => {
                            if (change.newState === $.signalR.connectionState.reconnecting) {

                                console.log('Re-connecting');
                            } else if (change.newState === $.signalR.connectionState.connected) {
                                console.log('The server is online');
                            } else if (change.newState === $.signalR.connectionState.disconnected) {
                                console.log('disconnected');
                                this.connection.reconnecting();
                                // if(groupid !== grId){
                                //     location.reload();
                                //     this.connection.reconnecting();
                                // }

                            }
                        });
                        this.connection.disconnected((t: any) => {
                            console.log(proxy.state);
                            // console.log(t.state);
                            // console.log(t);
                            // console.log('Connect disconect');
                            // location.reload();
                            // setTimeout(function() {
                            //     this.connection.reconnecting();
                            //     // location.reload();
                            //     // this.connection.start().then(function () {
                            //     //     console.log('Connect Ok');
                            //     //
                            //     //     return proxy;
                            //     //
                            //     // }).fail(function () {
                            //     //     console.error('Connect Fail');
                            //     // });
                            // }, 5000); // Restart connection after 5 seconds.
                            // this.connection.reconnecting();
                        });

                    }
                }

                private onUpdateDevice(value: any) {
                    if (this.allCars.ContainKey(value.Serial)) {
                        var oldvalue = this.allCars.Get(value.Serial);
                        // (tmp, value);

                        let newvalue = {
                            SoHieu: value.SoHieu, // số hiệu
                            Bs: value.Bs, // biển số
                            Serial: value.Serial, // số serial
                            Location: {
                                Lat: value.Location.Lat,
                                Lng: value.Location.Lng,
                                Address: value.Location.Address
                            },
                            Angle: value.Angle, // số góc
                            ClientSend: value.ClientSend, // ngày tháng cập nhật
                            Sgtvt: value.Sgtvt, // sở GTVT
                            CompanyId: value.CompanyId, //mã công ty
                            CompanyName: value.CompanyName, // tên công ty
                            DeviceGroup: value.DeviceGroup, // tên đội xe
                            Model: value.Model, // tên model
                            Speed: value.Speed, // tốc độ
                            TotalSession: value.TotalSession, // số lượng cuốc xe
                            Power: value.Power, //điện áp bình
                            GSMSignal: value.GSMSignal,// tín hiệu GMS
                            KeyStatus: value.KeyStatus,// trạng thái chìa khóa
                            AirMachine: value.AirMachine, // trạng thái máy lạnh
                            HongNgoai: value.HongNgoai, // trạng thái hồng ngoại
                            Phone: value.Phone,// số dt thiết bị
                            Money: value.Money,// tài khoản sim

                            //thông tin đồng hồ
                            TotalKmUse: value.TotalKmUse, // tổng km đã sử dụng khi có khách
                            TongKMDongHo: value.TongKMDongHo, // tổng km theo đồng hồ
                            TongKMGps: value.TongKMGps, // tổng km theo gps
                            TongKMCoKhachGps: value.TongKMCoKhachGps,// tổng km theo GPS
                            TotalSession0Km: value.TotalSession0Km, // tổng km theo cuốc 0Km
                            TotalMoney: value.TotalMoney, // tổng tiền
                            ThoiGianDung: value.ThoiGianDung, // thời gian dừng


                            //thông tin cuốc đang chạy
                            CurrentMeterKm: value.CurrentMeterKm, // km đang chạy
                            MoneySession: value.MoneySession, // tiền trong cuốc
                            ThoiGianCho: value.ThoiGianCho, // thời gian chờ khách
                            ThoiGianNhanKhach: value.ThoiGianNhanKhach, // thời gian nhận khách

                            //thông tin tài xế
                            DriverName: value.DriverName, // tài xế
                            DriverPhoneNumber: value.DriverPhoneNumber, // điện thoại tài xế
                            Gplx: value.Gplx,/*Giấy phép lái xe*/
                            SoGhe: oldvalue.SoGhe,/*Giấy phép lái xe*/
                            TgLaiXe: value.TgLaiXe,// thời gian lái xe
                            TongThoiGian: value.TongThoiGian, // tổng thời gian
                            OverTimeLCount: value.OverTimeLCount, // số lần quá thời gian liên tục

                            //thông tin trạng thái
                            IsBusy: value.IsBusy, // trạng thái bận
                            LostGsm: value.LostGsm, // mất GSM
                            Gps: value.Gps,// trạng thái GPS
                            MeterOk: value.MeterOk, // trạng thái đồng hồ
                            MeterStatus: value.MeterStatus,
                            StopOverTime: value.StopOverTime, // trạng thái xe dừng lâu
                            OverSpeed: value.OverSpeed, // quá tốc độ
                            ChichXung: value.ChichXung, //  chích xung
                            ThoiGianChotCa: value.ThoiGianChotCa, // thời gian chốt ca
                            XeTrongVung: value.XeTrongVung,
                            timeUpdateconvert: value.timeUpdateconvert,
                        };
                        this.FilterStateCars(oldvalue, newvalue);
                        oldvalue.load(newvalue);
                        // tmp.load({
                        //     isOverSpeed: value.isOverSpeed, /*xe có quá tốc độ hay ko?*/
                        //     Bs: value.Bs, /*Biển số xe*/
                        //     statusCar: value.statusCar, /* trạng thái xe*/
                        //     speed: value.speed, /*Tốc độ xe*/
                        //     kmOnDay: value.KmNgay, /*số km trong ngày*/
                        //     dienAp: value.dienAp, /*thông số Điện áp*/
                        //     statusGps: value.statusGps, /*Trạng thái Gps*/
                        //     trangThaiMay: value.trangThaiMay, /*Trạng thái máy*/
                        //     trangThaiMayLanh: value.trangThaiMayLanh, /*Trạng thái máy lạnh*/
                        //     soGtvt: value.soGtvt, /*Tên sở giao thông vận tải*/
                        //     kmCuoc: value.kmCuoc, /*Km cuốc*/
                        //     mucXang: value.mucXang, /*mức xăng*/
                        //     mucSong: value.mucSong, /*Mức sóng - cường độ trường*/
                        //     cuaXe: value.cuaXe, /*Trạng thái đóng mở của xe*/
                        //     dungDo: value.dungDo, /*trạng thái dừng đỗ*/
                        //     Id: value.Id, /*id xe*/
                        //     Gplx: value.Gplx, /*Giấy phép lái xe*/
                        //     nhietDo: value.nhietDo, /*nhiệt độ*/
                        //     address: value.address, /*Địa chỉ*/
                        //     timeUpdate: value.timeUpdate, /*Thời gian cập nhật*/
                        //     tglxtn: value.tglxtn, /*Thời gian lái xe liên tục*/
                        //     tglxlt: value.tglxlt, /*Thời gian lái xe trong ngày*/
                        //     nameDriver: value.nameDriver, /*Tên tài xế*/
                        //     qtgtn: value.qtgtn, /*Quá thời gian liên tục*/
                        //     qtglt: value.qtglt, /*quá thời gian trong ngày*/
                        //     moCua: value.moCua, /*số lần mở cửa*/
                        //     Location: {
                        //         Lat: value.Location.Lat,
                        //         Lng: value.Location.Lng,
                        //         Address: ''
                        //     },
                        //     Serial: value.Serial,
                        //     bienSo: value.Bienso,
                        //     Angle: value.Angle,
                        //     type: value.type,
                        //     pausecount: value.pausecount,
                        //     opendoorcount: value.opendoorcount,
                        //     overspeedcount: value.overspeedcount,
                        //     vin: value.vin,
                        //     money: value.money,
                        //     phone: value.phone,
                        //     modeltype: value.modeltype,
                        //     statusplusmemory: value.plusmemory,
                        //     sheats: value.sheat,
                        //     lostgsm: value.lostgsm,
                        //     group: value.group,
                        //     typeValue: value.typeValue,
                        //     displayName: '',
                        //     EndTime: value.EndTime,
                        //     MessageAlert:value.MessageAlert,
                        //     TimePause:value.TimePause,
                        //     PauseValue:value.PauseValue,
                        // });

                        this.processonlyCarTaxiViewMap(oldvalue);
                        //this.processCarViewOnMap();
                    }

                }

                public FilterStateCars(oldvalue: any, newvalue: any) {
                    //với trường hợp xe chạy
                    // var count_xe_chay = this.getCarForDrive(newvalue.groupId).length;
                    // if(!newvalue.lostgsm){
                    //     // không mất liên lạc
                    //     if(oldvalue.dungDo !== newvalue.dungDo){
                    //         if(!newvalue.dungDo){
                    //             this.statictis.CarForDrive ++;
                    //             this.statictis.CarForStop --;
                    //         }else{
                    //             this.statictis.CarForDrive --;
                    //             this.statictis.CarForStop ++;
                    //         }
                    //         // count_xe_chay = count_xe_chay + 1;
                    //     }
                    // }
                    // this.statictis.carnew = newvalue;
                    // this.statictis.CarForDrive = count_xe_chay;
                    // this.xechay = count_xe_chay;
                    // console.log(this.statictis.xechay);
                    // this.statictis.carnew = newvalue;
                    if (this.onStatictis != undefined)
                        this.onStatictis(newvalue);

                }

                private installEvent() {
                    if (this.eventBoundChange != null)
                        google.maps.event.removeListener(this.eventBoundChange);
                    if (this.cfg.map !== undefined)
                        this.eventBoundChange = google.maps.event.addListener(this.cfg.map, "center_changed", () => {
                            window.setTimeout(() => {

                                // console.log('chay timeout view xe tren map');
                                this.processCarTaxiViewOnMap();
                            }, 2000);

                        });
                }


                private processonlyCarTaxiViewMap(car: CarTaxiView) {
                    if (this.cfg.map !== undefined) {
                        var bound = this.cfg.map.getBounds();
                        if (bound == undefined) return;
                        if (bound.contains(car.latlng) && !car.ForceHide) {
                            if (car.getMap() == null) {
                                car.setMap(this.cfg.map);
                                // if(car.viewInfo() != null){
                                //     car.moveinfo();
                                // }
                            }
                            car.rotate();
                        } else {
                            if (car.getMap() != null) {
                                car.setMap(null);
                            }
                        }
                    }
                }

                private processCarTaxiViewOnMap(): void {
                    // console.log('ve xe');
                    // return;
                    if (this.cfg.map !== undefined) {
                        var bound = this.cfg.map.getBounds();
                        if (bound == undefined) return;
                        angular.forEach(this.allCars.Values(), (car) => {
                            if (bound.contains(car.latlng) && !car.ForceHide) {
                                if (car.getMap() == null) {
                                    car.setMap(this.cfg.map);
                                    // if(car.viewInfo() != null){
                                    //     car.moveinfo();
                                    // }
                                }
                                car.rotate();
                            } else {
                                if (car.getMap() != null) {
                                    car.setMap(null);
                                }
                            }

                        });
                    }
                }

                clearAllCars() {
                    angular.forEach(this.getAllCars(), (c) => {
                        console.log('clear all car');
                        c.setMap(null);
                        c.removeinfo();

                    });
                    this.allCars.Clear();
                };


                private defer: any = null;

                /*Load thông tin thiết bị từ server về*/
                public getAllCarByCompanyId(companyId: number,
                                            $http: ng.IHttpService,
                                            global: App.Shared.GlobalService,
                                            grid: App.Shared.Directives.GridviewControl,
                                            companyName: string,
                                            groupId: number, isSunTaxi: boolean): angular.IPromise<boolean> {

                    this.isSunTaxi = isSunTaxi;
                    //if(this.defer==null) -- bỏ đoạn này để defer đc tạo mới liên tục
                    this.defer = this.cfg.q.defer<boolean>();
                    if (this.isOk == true) {
                        console.log('dang lay danh sach xe ! doi xi');
                        this.defer.resolve(false);
                        return this.defer.promise;
                    } else {
                        this.isOk = true;
                        if (this.xhttp == null)
                            this.xhttp = new XMLHttpRequest();
                        if (this.xhttp.onreadystatechange == null || this.xhttp.onreadystatechange == undefined)
                            this.xhttp.onreadystatechange = () => {
                                if (this.xhttp.readyState == 4 && this.xhttp.status == 200) {
                                    var data = JSON.parse(this.xhttp.responseText);
                                    // console.log(data.listTaxiStatus);
                                    $.each(data.listTaxiStatus, (index: number, value: any) => {
                                        try {
                                            value.tt = value.ClientSend.slice(11);
                                            // value.qtglt = 0;
                                            value.Angle += 176;

                                            var acc: any = global.Account.GetValue();
                                            if (this.allCars.ContainKey(value.Serial)) {

                                                var tmp = this.allCars.Get(value.Serial);
                                                tmp.load({

                                                    SoHieu: value.SoHieu, // số hiệu
                                                    Bs: value.Bs, // biển số
                                                    Serial: value.Serial, // số serial
                                                    Location: {
                                                        Lat: value.Location.Lat,
                                                        Lng: value.Location.Lng,
                                                        Address: value.Location.Address
                                                    },
                                                    Angle: value.Angle, // số góc
                                                    ClientSend: value.ClientSend, // ngày tháng cập nhật
                                                    Sgtvt: value.Sgtvt, // sở GTVT
                                                    CompanyId: value.CompanyId, //mã công ty
                                                    CompanyName: value.CompanyName, // tên công ty
                                                    DeviceGroup: value.DeviceGroup, // tên đội xe
                                                    Model: value.Model, // tên model
                                                    Speed: value.Speed, // tốc độ
                                                    TotalSession: value.TotalSession, // số lượng cuốc xe
                                                    Power: value.Power, //điện áp bình
                                                    GSMSignal: value.GSMSignal,// tín hiệu GMS
                                                    KeyStatus: value.KeyStatus,// trạng thái chìa khóa
                                                    AirMachine: value.AirMachine, // trạng thái máy lạnh
                                                    HongNgoai: value.HongNgoai, // trạng thái hồng ngoại
                                                    Phone: value.Phone,// số dt thiết bị
                                                    Money: value.Money,// tài khoản sim

                                                    //thông tin đồng hồ
                                                    TotalKmUse: value.TotalKmUse, // tổng km đã sử dụng khi có khách
                                                    TongKMDongHo: value.TongKMDongHo, // tổng km theo đồng hồ
                                                    TongKMGps: value.TongKMGps, // tổng km theo gps
                                                    TongKMCoKhachGps: value.TongKMCoKhachGps,// tổng km theo GPS
                                                    TotalSession0Km: value.TotalSession0Km, // tổng km theo cuốc 0Km
                                                    TotalMoney: value.TotalMoney, // tổng tiền
                                                    ThoiGianDung: value.ThoiGianDung, // thời gian dừng


                                                    //thông tin cuốc đang chạy
                                                    CurrentMeterKm: value.CurrentMeterKm, // km đang chạy
                                                    MoneySession: value.MoneySession, // tiền trong cuốc
                                                    ThoiGianCho: value.ThoiGianCho, // thời gian chờ khách
                                                    ThoiGianNhanKhach: value.ThoiGianNhanKhach, // thời gian nhận khách

                                                    //thông tin tài xế
                                                    DriverName: value.DriverName, // tài xế
                                                    DriverPhoneNumber: value.DriverPhoneNumber, // điện thoại tài xế
                                                    Gplx: value.Gplx,/*Giấy phép lái xe*/
                                                    SoGhe: value.SoGhe,/*Giấy phép lái xe*/
                                                    TgLaiXe: value.TgLaiXe,// thời gian lái xe
                                                    TongThoiGian: value.TongThoiGian, // tổng thời gian
                                                    OverTimeLCount: value.OverTimeLCount, // số lần quá thời gian liên tục

                                                    //thông tin trạng thái
                                                    IsBusy: value.IsBusy, // trạng thái bận
                                                    LostGsm: value.LostGsm, // mất GSM
                                                    Gps: value.Gps,// trạng thái GPS
                                                    MeterOk: value.MeterOk, // trạng thái đồng hồ
                                                    MeterStatus: value.MeterStatus,
                                                    StopOverTime: value.StopOverTime, // trạng thái xe dừng lâu
                                                    OverSpeed: value.OverSpeed, // quá tốc độ
                                                    ChichXung: value.ChichXung, //  chích xung
                                                    ThoiGianChotCa: value.ThoiGianChotCa, // thời gian chốt ca
                                                    XeTrongVung: value.XeTrongVung,
                                                    timeUpdateconvert: value.timeUpdateconvert,
                                                    isSunTaxi: isSunTaxi
                                                });

                                            } else {
                                                var car: any = new Shared.Library.CarTaxiView(this.cfg);
                                                car.companyId = companyId;
                                                // car.companyName = companyName;
                                                car.load({
                                                    // isOverSpeed: value.isOverSpeed, /*xe có quá tốc độ hay ko?*/
                                                    SoHieu: value.SoHieu, // số hiệu
                                                    Bs: value.Bs, // biển số
                                                    Serial: value.Serial, // số serial
                                                    Location: {
                                                        Lat: value.Location.Lat,
                                                        Lng: value.Location.Lng,
                                                        Address: value.Location.Address
                                                    },
                                                    Angle: value.Angle, // số góc
                                                    ClientSend: value.ClientSend, // ngày tháng cập nhật
                                                    Sgtvt: value.Sgtvt, // sở GTVT
                                                    CompanyId: value.CompanyId, //mã công ty
                                                    CompanyName: value.CompanyName, // tên công ty
                                                    DeviceGroup: value.DeviceGroup, // tên đội xe
                                                    Model: value.Model, // tên model
                                                    Speed: value.Speed, // tốc độ
                                                    TotalSession: value.TotalSession, // số lượng cuốc xe
                                                    Power: value.Power, //điện áp bình
                                                    GSMSignal: value.GSMSignal,// tín hiệu GMS
                                                    KeyStatus: value.KeyStatus,// trạng thái chìa khóa
                                                    AirMachine: value.AirMachine, // trạng thái máy lạnh
                                                    HongNgoai: value.HongNgoai, // trạng thái hồng ngoại
                                                    Phone: value.Phone,// số dt thiết bị
                                                    Money: value.Money,// tài khoản sim

                                                    //thông tin đồng hồ
                                                    TotalKmUse: value.TotalKmUse, // tổng km đã sử dụng khi có khách
                                                    TongKMDongHo: value.TongKMDongHo, // tổng km theo đồng hồ
                                                    TongKMGps: value.TongKMGps, // tổng km theo gps
                                                    TongKMCoKhachGps: value.TongKMCoKhachGps,// tổng km theo GPS
                                                    TotalSession0Km: value.TotalSession0Km, // tổng km theo cuốc 0Km
                                                    TotalMoney: value.TotalMoney, // tổng tiền
                                                    ThoiGianDung: value.ThoiGianDung, // thời gian dừng


                                                    //thông tin cuốc đang chạy
                                                    CurrentMeterKm: value.CurrentMeterKm, // km đang chạy
                                                    MoneySession: value.MoneySession, // tiền trong cuốc
                                                    ThoiGianCho: value.ThoiGianCho, // thời gian chờ khách
                                                    ThoiGianNhanKhach: value.ThoiGianNhanKhach, // thời gian nhận khách

                                                    //thông tin tài xế
                                                    DriverName: value.DriverName, // tài xế
                                                    DriverPhoneNumber: value.DriverPhoneNumber, // điện thoại tài xế
                                                    Gplx: value.Gplx,/*Giấy phép lái xe*/
                                                    SoGhe: value.SoGhe,/*Giấy phép lái xe*/
                                                    TgLaiXe: value.TgLaiXe,// thời gian lái xe
                                                    TongThoiGian: value.TongThoiGian, // tổng thời gian
                                                    OverTimeLCount: value.OverTimeLCount, // số lần quá thời gian liên tục

                                                    //thông tin trạng thái
                                                    IsBusy: value.IsBusy, // trạng thái bận
                                                    LostGsm: value.LostGsm, // mất GSM
                                                    Gps: value.Gps,// trạng thái GPS
                                                    MeterOk: value.MeterOk, // trạng thái đồng hồ
                                                    MeterStatus: value.MeterStatus,
                                                    StopOverTime: value.StopOverTime, // trạng thái xe dừng lâu
                                                    OverSpeed: value.OverSpeed, // quá tốc độ
                                                    ChichXung: value.ChichXung, //  chích xung
                                                    ThoiGianChotCa: value.ThoiGianChotCa, // thời gian chốt ca
                                                    XeTrongVung: value.XeTrongVung,
                                                    timeUpdateconvert: value.timeUpdateconvert
                                                });
                                                //console.log(`add device : ${car.Bs}`);
                                                this.allCars.Add(car.Serial, car);
                                                car.CallbackOnClick = () => {
                                                    $.each(this.getAllCars(), (idx: number, val: CarTaxiView) => {
                                                        val.removeinfo();
                                                        // this.onClickCarOnMap(val);
                                                    });
                                                    this.onClickCarOnMap(car);
                                                }
                                            }
                                        } catch (e) {
                                            console.log('loi trang thai');
                                        }

                                    });
                                    //
                                    this.processCarTaxiViewOnMap();
                                    ///*load thông tin từ server*/
                                    ///*load lên bản đồ*/
                                    this.isOk = false;
                                    //// grid.data = rep.data.Datas;
                                    ////grid.refresh();
                                    delete this.xhttp.responseText;
                                    delete this.xhttp.response;
                                    delete this.xhttp.onreadystatechange;
                                    delete this.xhttp;
                                    this.xhttp = null;
                                    data = null;
                                    this.defer.resolve(true);
                                }

                                if (this.xhttp != undefined && this.xhttp != null && this.xhttp.status == 401)// hết hạn token
                                    window.location.href = "/login.html";
                            };
                        //todo: mở lên khi server up lên route 1
                        // if(groupId !== -1){
                        //     this.xhttp.open("GET", global.ReportHost + `api/Device/GetDeviceStatusByGroupId?companyId=${companyId}&groupId=${groupId}`, false);
                        //
                        // }else{
                        //     this.xhttp.open("GET", global.ReportHost + `api/Device/GetDeviceStatusByCompanyId?companyId=${companyId}`, false);
                        // }
                        console.log('lay group0');
                        this.xhttp.open("GET", global.ReportHost + `api/StatusTaxi/StatusDeviceTaxi?companyId=${companyId}&groupId=${groupId}`, false);
                        this.xhttp.setRequestHeader("token", global.Token.GetValue<string>());
                        //  this.xhttp.responseType = 'json';
                        this.xhttp.setRequestHeader("Content-Type", "application/json");
                        this.xhttp.setRequestHeader("Accept", "application/json");
                        // this.xhttp.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                        this.xhttp.send(null);
                        //delete this.xhttp;
                        //this.xhttp = null;
                        //$http.get(global.ReportHost + `api/Device/GetDeviceStatusByCompanyId?companyId=${companyId}`,{cache :false}).then((rep:any)=> {
                        //    this.isOk = false;
                        //    this.defer.resolve(true);
                        //});

                    }
                    return this.defer.promise;
                }

                public getAllCars() {
                    return this.allCars.Values();
                }


                // public resetOption(serial:string,type:string){
                //     var car=this.allCars.Get(serial);
                //     if(car!=null||car!=undefined){
                //         car.resettozero(type);
                //     }
                // }
                // carDistanceView (id,subtime){
                // var car=this.allCars.get(id);
                // if(car !=null){
                //     /*tính thời gian cần hiển thị */
                //     /*thời gian hiện tại + subtime*/
                //     var end=new Date();
                //
                //     var begin=new Date();
                //     begin.setTime(end.getTime()+subtime*60*1000);
                //     this.cfg.manageCarTrip.load(id
                //         ,(begin.getMonth() +1 )+"/"+begin.getDate()+"/"+begin.getFullYear()+" "+begin.getHours()+":"+begin.getMinutes()+":"+begin.getSeconds(),
                //         (end.getMonth()+1)+"/"+end.getDate()+"/"+end.getFullYear()+" "+end.getHours()+":"+end.getMinutes()+":"+end.getSeconds()
                //     );
                // }

                //};
                //TODO: thêm các hàm thông kê ở đây
                public where(where: (car: CarTaxiView) => boolean): Array<CarTaxiView> {
                    var result = Array<CarTaxiView>();
                    angular.forEach(this.getAllCars(), function (car) {
                        if (where(car))
                            result.push(car);
                    });
                    return result;
                }

                public getCarContainInbounds(bounds: google.maps.LatLngBounds) {
                    if (bounds == null || bounds == undefined)
                        return [];
                    return this.where(function (c) {
                        return bounds.contains(c.latlng);
                    });
                }

                // //xe quá tốc độ
                public getCarOverSpeed(groupname: string) {
                    return this.where((car: CarTaxiView) => {
                        return car.OverSpeed;
                    });
                    // console.log('đội xe');
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=>{
                    //         if(car.DeviceGroup === groupname){
                    //             return car.OverSpeed;
                    //         }
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=>{
                    //         return car.OverSpeed;
                    //     });
                    // }

                }


                /*xe mất liên lạc*/
                public getCarLostSign(groupname: string) {
                    return this.where((car: CarTaxiView) => {
                        return car.LostGsm;
                    });
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=> {
                    //         if(car.DeviceGroup === groupname){
                    //             return car.LostGsm;
                    //         }
                    //
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=> {
                    //         return car.LostGsm;
                    //     });
                    // }


                }

                //lấy danh sách xe theo gps
                public getCarLostGps(groupname: string) {
                    return this.where((car: CarTaxiView) => {
                        return !car.Gps;
                    });
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=> {
                    //         if(car.DeviceGroup === groupname){
                    //             return !car.Gps;
                    //         }
                    //
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=> {
                    //         return !car.Gps;
                    //     });
                    // }

                }

                //lấy danh sách xe theo mất đồng hồ
                public getCarLostDongHo(groupname: string) {
                    return this.where((car: CarTaxiView) => {
                        return !car.LostGsm && !car.MeterOk;
                    });
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=> {
                    //         if(car.DeviceGroup === groupname){
                    //             return !car.LostGsm && !car.MeterOk;
                    //         }
                    //
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=> {
                    //         return !car.LostGsm && !car.MeterOk;
                    //     });
                    // }

                }

                // lấy xe theo đỗ
                public getCarDo(groupname: string) {
                    return this.where((car: CarTaxiView) => {
                        return car.MeterOk && !car.MeterStatus && car.Speed == 0 && !car.KeyStatus;
                    });
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=> {
                    //         if(car.DeviceGroup === groupname){
                    //             return car.MeterOk && !car.MeterStatus && car.Speed==0 && !car.KeyStatus;
                    //         }
                    //
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=> {
                    //         return car.MeterOk && !car.MeterStatus && car.Speed==0 && !car.KeyStatus;
                    //     });
                    // }

                }

                // lấy xe theo dừng
                public getCarDung(groupname: string) {
                    return this.where((car: CarTaxiView) => {
                        return car.MeterOk && !car.MeterStatus && car.Speed == 0 && car.KeyStatus;
                    });
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=> {
                    //         if(car.DeviceGroup === groupname){
                    //             return car.MeterOk && !car.MeterStatus && car.Speed==0 && car.KeyStatus;
                    //         }
                    //
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=> {
                    //         return car.MeterOk && !car.MeterStatus && car.Speed==0 && car.KeyStatus;
                    //     });
                    // }


                }


                // lấy xe theo Biển số
                public getCarForBs(bs: string) {

                    return this.where((car: CarTaxiView) => {
                        return car.Bs == bs;
                    });
                }

                // lấy xe theo serial
                public getCarForSerial(ser: string) {
                    return this.where((car: CarTaxiView) => {
                        return car.serial == ser;
                    });
                }

                // lấy xe theo có khách
                public getCarForCoKhach(groupname: string) {
                    return this.where((car: CarTaxiView) => {
                        return !car.LostGsm && car.MeterOk && car.Gps && car.MeterStatus;
                    });
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=> {
                    //         if(car.DeviceGroup === groupname){
                    //             return !car.LostGsm && car.MeterOk && car.Gps && car.MeterStatus;
                    //         }
                    //
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=> {
                    //             return !car.LostGsm && car.MeterOk && car.Gps && car.MeterStatus;
                    //     });
                    // }


                }

                // lấy xe không khách
                public getCarForKhongKhach(groupname: string) {
                    return this.where((car: CarTaxiView) => {
                        return !car.LostGsm && car.MeterOk && car.Gps && !car.MeterStatus;
                    });
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=> {
                    //         if(car.DeviceGroup == groupname){
                    //             return !car.LostGsm && car.MeterOk && car.Gps && !car.MeterStatus;
                    //         }
                    //
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=> {
                    //         return !car.LostGsm && car.MeterOk && car.Gps && !car.MeterStatus;
                    //     });
                    // }


                }

                public getCarKhongKhach4ChoSun() {
                    return this.where((car: CarTaxiView) => {
                        return !car.LostGsm && car.MeterOk && car.Gps &&
                            !car.MeterStatus && (car.SoGhe === 4 || car.SoGhe === 5 || car.SoGhe === 0);
                    });
                }

                public getCarKhongKhach7ChoSun() {
                    return this.where((car: CarTaxiView) => {
                        return !car.LostGsm && car.MeterOk && car.Gps && !car.MeterStatus && car.SoGhe === 7;
                    });
                }

                // lấy xe bận
                public getCarForBusy(groupname: string) {
                    return this.where((car: CarTaxiView) => {
                        return !car.LostGsm && car.MeterOk && car.Gps && !car.MeterStatus && car.IsBusy;
                    });
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=> {
                    //         if(car.DeviceGroup == groupname){
                    //             return !car.LostGsm && car.MeterOk && car.Gps && !car.MeterStatus && car.IsBusy;
                    //         }
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=> {
                    //         return !car.LostGsm && car.MeterOk && car.Gps && !car.MeterStatus && car.IsBusy;
                    //     });
                    // }


                }

                //lấy danh sách xe vi phạm
                public getCarViPham(groupname: string) {
                    return this.where((car: CarTaxiView) => {
                        return car.OverSpeed || car.StopOverTime || car.ChichXung > 0;

                    });
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=> {
                    //         if(car.DeviceGroup == groupname){
                    //             return car.OverSpeed || car.StopOverTime || car.ChichXung > 0;
                    //         }
                    //
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=> {
                    //         return car.OverSpeed || car.StopOverTime || car.ChichXung > 0;
                    //
                    //     });
                    // }

                }

                //lấy ds xe trong vùng
                public getCarTrongVung(groupname: string) {
                    return this.where((car: CarTaxiView) => {
                        return car.XeTrongVung;
                    });
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=>{
                    //         if(car.DeviceGroup == groupname){
                    //             return car.XeTrongVung;
                    //         }
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=>{
                    //         return car.XeTrongVung;
                    //     });
                    // }


                }

                //lấy xe ngoài vùng
                public getCarNgoaiVung(groupname: string) {
                    return this.where((car: CarTaxiView) => {
                        return !car.XeTrongVung;
                    });
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=>{
                    //         if(car.DeviceGroup == groupname){
                    //             return !car.XeTrongVung;
                    //         }
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=>{
                    //         return !car.XeTrongVung;
                    //     });
                    // }


                }


                public getCarChuaChotCa(groupname: string) {

                    return this.where((car: CarTaxiView) => {
                        // return Math.floor((Date.parse(car.ThoiGianChotCa) - Date.now())/86400000) < 0;
                        var d = new Date(car.ThoiGianChotCa);
                        var today = new Date();

                        if (d.getMonth() - today.getMonth() == 0) {
                            return d.getDate() - today.getDate() < 0;
                        } else if (d.getMonth() - today.getMonth() < 0) {
                            return Math.floor((Date.parse(car.ThoiGianChotCa) - Date.now()) / 86400000) < 0;
                        }

                    });
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=> {
                    //         if(car.DeviceGroup == groupname){
                    //             // console.log(Date.parse(car.ThoiGianChotCa));
                    //             var d = new Date(car.ThoiGianChotCa);
                    //             var today = new Date();
                    //
                    //             if(d.getMonth() - today.getMonth() == 0){
                    //                 return d.getDate() - today.getDate() < 0;
                    //             }else if(d.getMonth() - today.getMonth() < 0){
                    //                 return Math.floor((Date.parse(car.ThoiGianChotCa) - Date.now())/86400000) < 0;
                    //             }
                    //
                    //         }
                    //
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=> {
                    //         // return Math.floor((Date.parse(car.ThoiGianChotCa) - Date.now())/86400000) < 0;
                    //         var d = new Date(car.ThoiGianChotCa);
                    //         var today = new Date();
                    //
                    //         if(d.getMonth() - today.getMonth() == 0){
                    //             return d.getDate() - today.getDate() < 0;
                    //         }else if(d.getMonth() - today.getMonth() < 0){
                    //             return Math.floor((Date.parse(car.ThoiGianChotCa) - Date.now())/86400000) < 0;
                    //         }
                    //
                    //     });
                    // }

                }

                public getCarCanhBao(groupname: string) {
                    return this.where((car: CarTaxiView) => {
                        return car.LostGsm || !car.Gps || !car.MeterOk;
                    });
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=> {
                    //         if(car.DeviceGroup == groupname){
                    //             return car.LostGsm || !car.Gps || !car.MeterOk;
                    //         }
                    //
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=> {
                    //         return car.LostGsm || !car.Gps || !car.MeterOk;
                    //     });
                    // }

                }

                //lấy danh sách xe quá thời gian liên tục
                public getCarQuathoigian(groupname: string) {
                    return this.where((car: CarTaxiView) => {
                        return car.TgLaiXe >= 225 || car.TongThoiGian >= 585;
                    });
                    // if(groupname !== 'Tất cả đội xe'){
                    //     return this.where((car:CarTaxiView)=> {
                    //        if(car.DeviceGroup == groupname){
                    //            return car.TgLaiXe >= 225 || car.TongThoiGian >= 585;
                    //        }
                    //     });
                    // }else{
                    //     return this.where((car:CarTaxiView)=> {
                    //         return car.TgLaiXe >= 225 || car.TongThoiGian >= 585;
                    //     });
                    // }
                }

                // lấy xe theo id xe
                // public getCarForId(ser:string) {
                //     return this.where((car:CarTaxiView)=> {
                //         return car.id == ser;
                //     });
                // }
                public getCarForGroup(group: string) {
                    if (group !== 'Tất cả đội xe') {
                        return this.where((car: CarTaxiView) => {
                            return (car.DeviceGroup == group);
                        });
                    } else {
                        return this.allCars.Values();
                    }


                }

                public CreateMenu(menu: any) {

                }

                public cartrip(x: number) {
                    alert('hello');
                }

                public onClickCarOnMap: (evt: any) => void;


                // public getAlertMessage(){
                //     return this.where((car:CarTaxiView)=>{
                //         return car.MessageAlert !== null;
                //     });
                // }
            }
        }
    }
}
