/**
 * Created by Tuantv on 05/19/2016.
 */
module App{
    export module Shared{
        export module Library{
            export class TripView {

                private tripEventInfo:any;
                private drawTrip:any;
                private ListPointStop:any;
                private map:any;
                private oldLatLng:any;
                constructor(config:MapConfig){
                    this.tripEventInfo=[];/*thông tin sự kiện trên chặng đường đã đi*/
                    this.drawTrip =[];
                    this.ListPointStop=[];
                    this.map=config.map;
                }

                push=(val:any)=> {
                    if (this.oldLatLng != undefined) {

                        var iconsetngs = {
                            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                            scale: 2
                        };
                        console.log();
                        var chuoi=[{
                            repeat: '300px', //CHANGE THIS VALUE TO CHANGE THE DISTANCE BETWEEN ARROWS
                            icon: iconsetngs,
                            offset: '50%'}];
                        if(val.speed <5 || (val.index % 3 != 0))
                        {
                            chuoi=null;
                        }
                        var line = new google.maps.Polyline({
                            geodesic: true,
                            strokeColor: '#f80003',
                            strokeOpacity: 0.7,
                            strokeWeight: 4,
                            icons: chuoi
                        });
                        //tạo hiệu ứng cho animation google map
                        // this.animateCircle(line);

                        line.setMap(this.map);
                        var path = line.getPath();
                        path.push(this.oldLatLng);
                        path.push(val.latLng);
                        if(val.pointEnd!=null)
                        {
                            if(val.pointEnd.status==0) {
                                var markerStop = new google.maps.Marker({
                                    position: val.pointEnd.latLng,
                                    map: this.map,
                                    icon: '../../assets/images/carIcon/Stop.png',
                                    title: "Dừng: "+val.pointEnd.title
                                });
                                markerStop.setMap(this.map);
                                this.ListPointStop.push(markerStop);
                            }
                            if(val.pointEnd.status==1) {
                                var markerStop = new google.maps.Marker({
                                    position: val.pointEnd.latLng,
                                    map: this.map,
                                    icon: '../../assets/images/carIcon/Off.png',
                                    title: "Đỗ: "+val.pointEnd.title
                                });
                                markerStop.setMap(this.map);
                                this.ListPointStop.push(markerStop);
                            }

                        }
                        else{this.ListPointStop.push(null);}
                        this.drawTrip.push(line);
                    }
                    this.oldLatLng = val.latLng;
                };
                length=()=> {
                    return this.drawTrip.length;
                };

                pop=()=> {
                   var line = this.drawTrip.pop();
                   var point=this.ListPointStop.pop();
                   var tmp = line.getPath();
                   this.oldLatLng = tmp.getAt(0);
                   line.setMap(null);
                   if(point!=null) {
                       point.setMap(null);
                   }
                   //delete line;
               };

                current=(index:any)=> {
                    if (index > this.drawTrip.length)
                        return false;
                    for (var i = 0; i < this.drawTrip.length - index; i++) {
                        this.pop();
                    }
                };

                clear=()=> {
                    var len = this.drawTrip.length;
                    for (var i = 0; i < len; i++) {
                        this.pop();
                    }
                    this.oldLatLng = undefined;
                };

                dispose=()=> {
                    this.clear();
                };



            }

        }
    }
}