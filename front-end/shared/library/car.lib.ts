/**
 * Created by user on 04/27/2016.
 */
/// <reference path="../library.ts"/>


function globalOnclick(serial:string, type:string) {
    //alert(`click device ${serial} --- ${type}`);
    //console.log(App.Shared.Library.GlobalCarManager);
    App.Shared.Library.GlobalCarManager.resetOption(serial, type);
}

module App {
    export module Shared {
        export module Library {
            export interface ICar {
                isOverSpeed:any;/*xe có quá tốc độ hay ko?*/
                bienSo:any;/*Biển số xe*/
                statusCar:any;/* trạng thái xe*/
                speed:any;/*Tốc độ xe*/
                kmOnDay:any;/*số km trong ngày*/
                dienAp:any;/*thông số Điện áp*/
                statusGps:any;/*Trạng thái Gps*/
                trangThaiMay:any;/*Trạng thái máy*/
                trangThaiMayLanh:any;/*Trạng thái máy lạnh*/
                soGtvt:any;/*Tên sở giao thông vận tải*/
                kmCuoc:any;/*Km cuốc*/
                mucXang:any;/*mức xăng*/
                mucSong:any;/*Mức sóng - cường độ trường*/
                cuaXe:any;/*Trạng thái đóng mở của xe*/
                dungDo:any;/*trạng thái dừng đỗ*/
                Id:any;/*id xe*/
                Gplx:any;/*Giấy phép lái xe*/
                nhietDo:any;/*nhiệt độ*/
                address:any;/*Địa chỉ*/
                timeUpdate:any;/*Thời gian cập nhật*/
                tglxtn:any;/*Thời gian lái xe liên tục*/
                tglxlt:any;/*Thời gian lái xe trong ngày*/
                nameDriver:any;/*Tên tài xế*/
                qtgtn:any;/*Quá thời gian liên tục*/
                qtglt:any;/*quá thời gian trong ngày*/
                moCua:any;/*số lần mở cửa*/
                Location:{
                    Lat:number,
                    Lng:number,
                    Address:string
                },
                Serial:string;
                Bs:string;
                Angle:number,
                tt?: string;
                vin:string;//mã vin
                sheats:number;//tải trọng hoặc số ghế
                phone:string;// số điện thoại
                money:string;//số tiền trong sim
                overspeedcount:number;//số lần quá tốc độ
                pausecount:number;// số lần dừng đỗ
                statusplusmemory?:boolean;// trạng thái thẻ nhớ
                opendoorcount?:number;//số lần đóng mở cửa
                modeltype?:string;//loại xe
                type?:string;// loại hình kinh doanh (Taxi,Car ... )
                lostgsm?: boolean;
                // companyId?:number;
                // companyName?:string;
                group: string;
                typeValue:number;
                EndTime:any;
                displayName: string;
                MessageAlert:string;
                TimePause:string;
                PauseValue:any;
                groupId:number;
                timeUpdateconvert:string;
            }
            export interface IMaintenance {
                KmDaoLopCurrent: number;
                KmDaoLopLimit: number;
                KmThayVoCurrent:number;
                KmThayVoLimit: number;
                KmThayNhotCurrent: number;
                KmThayNhotLimit: number;
                KmThayLocDauCurrent: number;
                KmThayLocDauLimit: number;
                KmThayLocGioCurrent:number;
                KmThayLocGioLimit: number;
                KmThayLocNhotCurrent: number;
                KmThayLocNhotLimit: number;

            }
            export class CarView extends BaseCar {
                public CallbackOnClick:()=>void;
                public MenuRight:IMaintenance;
                private info:any;
                public isOverSpeed:any;
                /*xe có quá tốc độ hay ko?*/
                public Bs:any;
                /*Biển số xe*/
                public statusCar:any;
                /* trạng thái xe*/
                public speed:any;
                /*Tốc độ xe*/
                public kmOnDay:any;
                /*số km trong ngày*/
                public dienAp:any;
                /*thông số Điện áp*/
                public statusGps:any;
                /*Trạng thái Gps*/
                public trangThaiMay:any;
                /*Trạng thái máy*/
                public trangThaiMayLanh:any;
                /*Trạng thái máy lạnh*/
                public soGtvt:any;
                /*Tên sở giao thông vận tải*/
                public kmCuoc:any;
                /*Km cuốc*/
                public mucXang:any;
                /*mức xăng*/
                public mucSong:any;
                /*Mức sóng - cường độ trường*/
                public cuaXe:any;
                /*Trạng thái đóng mở của xe*/
                public dungDo:any;
                /*trạng thái dừng đỗ*/
                public id:any;
                /*id xe*/
                public Gplx:any;
                /*Giấy phép lái xe*/
                public nhietDo:any;
                /*nhiệt độ*/
                public address:any;
                /*Địa chỉ*/
                public timeUpdate:any;
                public timeUpdateconvert:string;
                /*Thời gian cập nhật*/
                public tglxtn:any;
                /*Thời gian lái xe liên tục*/
                public tglxlt:any;
                /*Thời gian lái xe trong ngày*/
                public nameDriver:any;
                /*Tên tài xế*/
                public qtgtn:any;
                /*Quá thời gian liên tục*/
                public qtglt:any;
                /*quá thời gian trong ngày*/
                public moCua:any;
                /*số lần mở cửa*/
                vin:string;//mã vin
                sheats:number;//tải trọng hoặc số ghế
                typeValue:number;// đơn vị tải trọng hoặc số ghế
                phone:string;// số điện thoại
                money:string;//số tiền trong sim
                overspeedcount:number;//số lần quá tốc độ
                pausecount:number;// số lần dừng đỗ
                statusplusmemory:boolean;// trạng thái thẻ nhớ
                opendoorcount:number;//số lần đóng mở cửa
                modeltype:string;//loại xe
                group:string; // đội xe
                EndTime:any; // thời gian hết hạn
                type:string;// loại hình kinh doanh (Taxi,Car ... )
                lostgsm:boolean; // tín hiệu GSM
                displayName: string;// hiển thị thông tin tên user trên chuột phải
                public ForceHide:boolean;
                /*Bắt buộc ẩn trên map*/
                public isRightClick:boolean;
                private isLatlngModify:boolean = true;
                //id công ty
                public companyId:number;
                // tên công ty
                public companyName:string;
                TimePause:string;
                PauseValue:any;
                check_right:boolean;
                serial:string; // serial của xe
                Location:any;
                MessageAlert:string;
                groupId:number;


                constructor(config:CarConfig) {
                    super(config);
                    this.info = new window.InfoBox({
                        boxStyle: {opacity: 1},
                        disableAutoPan: false,
                        maxWidth: 0,
                        pixelOffset: new google.maps.Size(-140, 0),
                        zIndex: null,
                        // closeBoxMargin: "19px 17px 2px 2px",
                        closeBoxLeft: "298px",
                        closeBoxTop: "-2px",
                        closeBoxWidth: "25px",
                        closeBoxHeight: "25px",
                        closeBoxURL: "../../assets/images/Close_Window_48.png",
                        infoBoxClearance: new google.maps.Size(1, 1),
                        isHidden: false,
                        pane: "floatPane",
                        enableEventPropagation: false,

                    });
                    this.isRightClick = false;
                    this.ForceHide = false;
                    this.onClick = ()=> {
                        this.isRightClick = false;
                        if (this.CallbackOnClick != null && this.CallbackOnClick != undefined)
                            this.CallbackOnClick();
                        this.viewInfo();

                    };
                    this.onRightClick = () => {
                        console.log('car-right');
                        this.cfg.map.FisrtRightClick = true;
                        this.isRightClick = true;
                        this.check_right = true;
                        this.viewRightInfo();
                        //alert('hello');
                    };
                    this.MenuRight = {
                        KmDaoLopCurrent: 0,
                        KmDaoLopLimit: 0,
                        KmThayVoCurrent: 0,
                        KmThayVoLimit: 0,
                        KmThayNhotCurrent: 0,
                        KmThayNhotLimit: 0,
                        KmThayLocDauCurrent: 0,
                        KmThayLocDauLimit: 0,
                        KmThayLocGioCurrent: 0,
                        KmThayLocGioLimit: 0,
                        KmThayLocNhotCurrent: 0,
                        KmThayLocNhotLimit: 0,
                    };


                }

                private viewInfo() {

                    this.updateInfo(true);
                    console.log(this.latlng);
                    if(this.info.getPosition() !== this.latlng){
                        this.info.setPosition(this.info.getPosition());
                    }
                        this.info.setPosition(this.latlng);


                    this.info.setMap(this.cfg.map);
                    // console.log(this.info.getPosition());
                }

                private viewRightInfo() {

                    this.buildInfoDeviceRight().then((html_right)=> {
                        this.info.setContent(html_right);
                        this.moveinfo();
                    });
                    this.info.setPosition(this.latlng);
                    this.info.setMap(this.cfg.map);
                }

                public load(obj:ICar) {
                    var oldLatLng = this.latlng;
                    this.latlng = new google.maps.LatLng(obj.Location.Lat, obj.Location.Lng);
                    this.serial = obj.Serial;
                    this.Location = obj.Location;
                    this.deviceLabel = obj.Bs;
                    this.id = obj.Id;
                    this.Bs = obj.Bs;
                    if (oldLatLng == null) {
                        this.iconInfo.angle = obj.Angle;
                        if (this.address != undefined)
                            this.isLatlngModify = true;
                    }
                    else {
                        this.isLatlngModify = (oldLatLng.lat() != this.latlng.lat() && oldLatLng.lng() != this.latlng.lng());
                        if (this.isLatlngModify)
                            this.iconInfo.angle = google.maps.geometry.spherical.computeHeading(oldLatLng, this.latlng);
                        if (this.address == undefined)
                            this.isLatlngModify = true;
                    }
                    this.speed = obj.speed;
                    this.dienAp = obj.dienAp;
                    this.timeUpdate = obj.timeUpdate;
                    this.statusGps = obj.statusGps;
                    this.trangThaiMay = obj.trangThaiMay;
                    this.trangThaiMayLanh = obj.trangThaiMayLanh;
                    this.soGtvt = obj.soGtvt;
                    this.kmCuoc = obj.kmCuoc;
                    this.mucXang = obj.mucXang;
                    this.mucSong = obj.mucSong;
                    this.moCua = obj.moCua;
                    this.dungDo = obj.dungDo;
                    this.cuaXe = obj.cuaXe;
                    this.qtglt = obj.qtglt;
                    this.qtgtn = obj.qtgtn;
                    this.nameDriver = obj.nameDriver;
                    this.tglxlt = obj.tglxlt;
                    this.tglxtn = obj.tglxtn;
                    this.timeUpdateconvert = obj.timeUpdate.slice(11);
                    // this.address=obj.Location.Address;
                    this.nhietDo = obj.nhietDo;
                    this.Gplx = obj.Gplx;
                    this.vin = obj.vin;
                    this.sheats = obj.sheats;
                    this.typeValue = obj.typeValue;
                    this.phone = obj.phone;
                    this.money = obj.money;
                    this.modeltype = obj.modeltype;
                    this.group = obj.group;
                    this.type = obj.type;
                    this.overspeedcount = obj.overspeedcount;
                    this.opendoorcount = obj.opendoorcount;
                    this.pausecount = obj.pausecount;
                    this.statusplusmemory = obj.statusplusmemory;
                    this.lostgsm = obj.lostgsm;
                    this.displayName = obj.displayName;
                    this.EndTime = obj.EndTime;
                    this.kmOnDay=obj.kmOnDay;
                    this.MessageAlert = obj.MessageAlert;
                    this.TimePause = obj.TimePause;
                    this.PauseValue = obj.PauseValue;
                    this.groupId = obj.groupId;
                    this.iconInfo.icon.src = this.getIcon();
                    /*cập nhật luôn thông tin nếu người sử dụng đã click cho hiển thị bảng thông tin*/
                    this.updateInfo(false);
                }

                public focus() {
                    if (this.CallbackOnClick != null && this.CallbackOnClick != undefined){
                        this.CallbackOnClick();

                    }
                    $('canvas.xetarget').removeClass('xetarget');
                    $(`canvas#c_${this.serial}`).addClass('xetarget');

                    this.cfg.map.setCenter(this.latlng);// chuyển trung tâm bản đồ tới vị trí thiết bị
                    this.viewInfo();//hiển thị bảng thông tin
                    console.log('chay');

                }

                private updateInfo(state:boolean) {
                    if (this.info.getMap() != null || state) {
                        if (this.isRightClick) {
                            // this.buildInfoDeviceRight().then((html_right)=>{
                            //
                            //     this.info.setContent(html_right);
                            //     this.moveinfo();
                            // });
                            this.viewRightInfo();
                        } else {
                            this.buildInfoDevice().then((html)=> {
                                this.info.setContent(html);
                                this.moveinfo();
                            });
                            // this.build2().then((html)=>{
                            //     this.info.setContent(html);
                            //     this.moveinfo();
                            // });
                        }

                    }
                }

                public removeinfo() {

                    this.info.setMap(null);
                }

                private AdapterDate(time:any) {
                    var year:string = time.slice(0, 4);
                    var month:string = time.slice(5, 7);
                    var date:string = time.slice(8, 10);

                    var min:string = time.slice(11);

                    var tt:any = date + '-' + month + '-' + year + ' ' + min;
                    return tt;
                }
                private AdapterDateNoTime(time:any) {
                    var year:string = time.slice(0, 4);
                    var month:string = time.slice(5, 7);
                    var date:string = time.slice(8, 10);

                    var min:string = time.slice(11);

                    var tmp:any = date + '-' + month + '-' + year ;
                    return tmp;
                }

                // hàm dịch chuyển thông báo theo xe
                private moveinfo() {
                    // kiểm tra xem có nội dung chưa và có hiển thị trên map hay không
                    if (this.info != null && this.info.getMap() != null) {

                        this.info.setPosition(this.latlng);
                    }
                }


                private buildRow(cells:Array<any>) {
                    var html = '';
                    html += '<tr>';
                    cells.forEach((m, i)=> {
                        html += this.buildCell(m.title, m.value, m.colspan,m.titleWidth);
                    });
                    return html + '</tr>'
                }

                private buildCell(title:string, value:any, colspan:number,titleWidth:number):string {
                    return `<td style="padding-left:5px " colspan="${colspan}"><table>
                    <tr><td style="${(titleWidth!=null&&titleWidth!=undefined)? 'width:'+titleWidth+'px':''}">${title} </td><td>${value}</td></tr>
                    </table>
                    </td>`;
                }

                private buildInfoDevice():Q.IPromise<string> {
                    var defer = this.cfg.q.defer<string>();
                    var hienthimucsong:string = '';
                    if (this.mucSong <= 15) {
                        hienthimucsong = 'Yếu'; //mức sóng từ < 15
                    } else if (this.mucSong > 15 && this.mucSong <= 20) { // mức sóng từ 15 ~ 20
                        hienthimucsong = 'Tốt';
                    } else {
                        hienthimucsong = 'Rất tốt'; //mức sóng từ 20~30
                    }
                    let switchon = '<div class="onoffswitch"><label class="onoffswitch-label" > <span class="onoffswitch-inner-on"></span> <span class="onoffswitch-switch" style="right: 5px!important;"></span> </label> </div>';
                    let switchoff = '<div class="onoffswitch"> <label class="onoffswitch-label" > <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label> </div>';

                    var tmp = `<table class='divTable' style='width: 320px; height: 380px;'>`;
                    tmp += this.buildRow([{
                        title: '<span style="color: #cc3341">THÔNG TIN XE</span>',
                        value: '',
                        colspan: 2,
                        titleWidth:120
                    }]);

                    tmp += this.buildRow([
                        {
                            title: 'Biển số:',
                            value: `<span style="color: #d65c66">${this.Bs}</span>`,
                            colspan: 1,
                            titleWidth:80
                        },{
                            title: 'Vận tốc:',
                            value: `<span style="color: #d65c66">${this.speed} Km/h</span>`,
                            colspan: 1,
                            titleWidth:80
                        },
                    ]);
                    tmp += this.buildRow([{
                        title: 'Sở GTVT:',
                        value: (this.soGtvt == null)? ' ' : this.soGtvt,
                        colspan: 1,
                        titleWidth:80
                    },{
                            title: 'Cửa xe:',
                            value: this.cuaXe ? 'Đóng' : 'Mở',
                            colspan: 1,
                            titleWidth:80
                        }]);
                    // tmp += this.buildRow([{
                    //     title: 'Cửa xe:',
                    //     value: this.cuaXe ? 'Đóng' : 'Mở',
                    //     colspan: 1,
                    //     titleWidth:80
                    // }]);

                    tmp += this.buildRow([{
                        title: 'GPS:',
                        value: this.statusGps ? 'Có' : 'Không',
                        colspan: 1,
                        titleWidth:80
                    }, {
                        title: 'GSM:',
                        value: hienthimucsong,
                        colspan: 1,
                        titleWidth:80
                    }]);
                    tmp += this.buildRow([{
                        title: 'Điện áp:',
                        value: this.dienAp,
                        colspan: 1,
                        titleWidth:80
                    },{
                        title: 'Loại xe:',
                        value: (this.modeltype == null) ? '' : this.modeltype,
                        colspan: 1,
                        titleWidth:80
                    }]);
                    // tmp += this.buildRow([{
                    //     title: 'Loại xe:',
                    //     value: this.modeltype,
                    //     colspan: 1,
                    //     titleWidth:60
                    // }]);

                    tmp += this.buildRow([{
                        title: 'Km Cuốc:',
                        value: `${(this.kmCuoc / 1000).toFixed(1)} Km`,
                        colspan: 1,
                        titleWidth:80
                    }, {
                        title: 'Km Ngày:',
                        value: `${(this.kmOnDay/1000).toFixed(1)} Km`,
                        colspan: 1,
                        titleWidth:80
                    }]);
                    tmp += this.buildRow([{
                        title: 'SL Dừng đỗ:',
                        value: this.pausecount,
                        colspan: 1,
                        titleWidth:80
                    }, {
                        title: 'SL Mở cửa:',
                        value: this.opendoorcount,
                        colspan: 1,
                        titleWidth:80
                    }]);


                    tmp += this.buildRow([{
                        title: 'Máy:',
                        value: this.trangThaiMay ? switchon: switchoff,
                        colspan: 1,
                        titleWidth:80
                    }, {
                        title: 'Điều hòa:',
                        value: this.trangThaiMay ? (this.trangThaiMayLanh ? switchon : switchoff): switchoff,
                        colspan: 1,
                        titleWidth:80
                    }]);



                    tmp += this.buildRow([{
                        title: '<span style="color: #cc3341">THÔNG TIN TÀI XẾ</span>',
                        value: '',
                        colspan: 2,
                        titleWidth:120
                    }]);

                    tmp += this.buildRow([{
                        title: 'Tài xế:',
                        value: this.nameDriver,
                        colspan: 2,
                        titleWidth:130
                    }]);
                    tmp += this.buildRow([{
                        title: 'GPLX:',
                        value: this.Gplx,
                        colspan: 2,
                        titleWidth:130
                    }]);

                    tmp += this.buildRow([{
                        title: 'SL Quá tốc độ:',
                        value: this.overspeedcount,
                        colspan: 1,
                        titleWidth:130
                    }]);

                    tmp += this.buildRow([{
                        title: 'SL Quá tg trong ngày:',
                        value: this.qtgtn,
                        colspan: 2,
                        titleWidth:130
                    }]);
                    tmp += this.buildRow([{
                        title: 'SL Quá tg liên tục:',
                        value: this.qtglt,
                        colspan: 2,
                        titleWidth:130
                    }]);


                    tmp += this.buildRow([{
                        title: 'TG lái xe liên tục:',
                        value: this.AdapterTime(this.tglxlt),
                        colspan: 2,
                        titleWidth:130
                    }]);
                    tmp += this.buildRow([{
                        title: 'TG lái xe trong ngày:',
                        value: this.AdapterTime(this.tglxtn),
                        colspan: 2,
                        titleWidth:130
                    }]);
                    if(this.PauseValue !== "" && this.PauseValue >= "1" && this.speed == 0){
                        tmp += this.buildRow([{
                            title: 'Thời điểm xe dừng:',
                            value: this.AdapterDate(this.TimePause),
                            colspan: 2,
                            titleWidth:130
                        }]);
                        tmp += this.buildRow([{
                            title: 'Số phút xe dừng:',
                            value: this.AdapterTime(Math.floor(this.PauseValue *1)),
                            colspan: 2,
                            titleWidth:130
                        }]);
                    }
                    if (this.isLatlngModify) {
                        this.cfg.$http.get(`http://geocode.adsun.vn/Geocode/GetAddress?lat=${this.latlng.lat()}&lng=${this.latlng.lng()}`).then((data:any)=> {
                            // console.log();
                            this.address = data.data.Address;

                            // bùa vị trí
                            // if (this.latlng.lat() <= 10.8280213 &&this.latlng.lat() >= 10.8271361 &&this.latlng.lng()>= 106.711809 && this.latlng.lng() <= 106.7137885)
                            //     this.address="79 quốc lộ 13, Hiệp Bình Chánh, Thủ Đức, Hồ Chí Minh, Vietnam";

                            tmp += this.buildRow([{
                                title: 'Vị trí:',
                                value: this.address !== undefined ? this.address: '',
                                colspan: 2,
                                titleWidth:70
                            }]);
                            tmp += this.buildRow([{
                                title: 'Cập nhật:',
                                value: this.AdapterDate(this.timeUpdate),
                                colspan: 2,
                                titleWidth:70
                            }]);
                            tmp += this.buildRow([{
                                title: 'TG hết phí sử dụng TB:',
                                value: `<span style="color: #d65c66">${this.AdapterDateNoTime(this.EndTime)}</span>`,
                                colspan: 2,
                                titleWidth:130
                            }]);
                            tmp += '</table>';
                            defer.resolve(tmp);
                            
                        });
                    } else {
                        tmp += this.buildRow([{
                            title: 'Vị trí:',
                            value: this.address !== undefined ? this.address: '',
                            colspan: 2,
                            titleWidth:70
                        }]);
                        tmp += this.buildRow([{
                            title: 'Cập nhật:',
                            value: this.AdapterDate(this.timeUpdate),
                            colspan: 2,
                            titleWidth:70
                        }]);
                        tmp += this.buildRow([{
                            title: 'TG hết phí sử dụng TB:',
                            value: `<span class="text-danger">${this.AdapterDateNoTime(this.EndTime)}</span>`,
                            colspan: 2,
                            titleWidth:150
                        }]);
                        tmp += '</table>';
                        defer.resolve(tmp);

                    }

                    return defer.promise;
                }
                private settingcarmap(){
                    $('#settingcar').click(()=>{
                        console.log('run cap nhat');
                    });
                    // console.log('chay');
                    // $('#settingcarmap').modal('show');
                }

                private build2(): Q.IPromise<string>{
                    var defer = this.cfg.q.defer<string>();
                    var tmp =
                '<div class="panel panel-flat"> ' +
                    '<div class="panel-heading"> ' +
                        '<h6 class="panel-title">Highlighted tabs<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6> ' +
                        '<div class="heading-elements">' +
                        '<ul class="icons-list">' +
                            '<li><a data-action="collapse"></a></li>' +
                            '<li><a data-action="reload"></a></li>' +
                            '<li><a data-action="close"></a></li>' +
                        '</ul>' +
                        '</div>' +
                    '</div>' +
                    '<div class="panel-body">' +
                        '<div class="tabbable">' +
                        '<ul class="nav nav-tabs nav-tabs-highlight">' +
                        '<li class=""><a href="#highlight-tab1" data-toggle="tab" class="legitRipple" aria-expanded="false">Active</a></li>' +
                        '<li class="active"><a href="#highlighted-tab2" data-toggle="tab" class="legitRipple" aria-expanded="true">Inactive</a></li>' +
                        '<li class="dropdown">' +
                    '</div>'+
                '</div>';
                    defer.resolve(tmp);
                    return defer.promise;

                }

                public AdapterTime(value:number){
                    var tmp = '';
                    if(value < 60){
                        tmp = `${value} phút`;
                    }else{

                        var min:number = (value%60);
                        var hour:number = (value - min)/60;
                        tmp = `${hour} giờ ${min.toFixed(0)} phút`;
                        // tmp = `${hour}`;
                    }
                    return tmp;
                }

                /*Hàm reset giá trị trên menu phải*/
                public resettozero(type:string) {
                    var ReportHost:string = 'http://route1.adsun.vn/';
                    switch (type) {
                        case 'DaoLop':
                            var req = {
                                method: 'PUT',
                                url: ReportHost + `api/Maintenance/ResetMaintenanceBySerial?companyId=${this.companyId}&serial=${this.serial}&optionName=DaoLop`,
                            };
                            this.cfg.$http(req).then((rep:any)=> {

                                this.MenuRight.KmDaoLopCurrent = 0;
                                this.check_right = true;
                                this.updateInfo(true);
                            });

                            break;
                        case 'ThayLocDau':
                            var req = {
                                method: 'PUT',
                                url: ReportHost + `api/Maintenance/ResetMaintenanceBySerial?companyId=${this.companyId}&serial=${this.serial}&optionName=ThayLocDau`,
                            };
                            this.cfg.$http(req).then((rep:any)=> {

                                this.MenuRight.KmThayLocDauCurrent = 0;
                                this.check_right = true; // thực hiện đi cập nhật lại menu phải
                                this.updateInfo(true);
                            });

                            break;
                        case 'ThayVo':
                            var req = {
                                method: 'PUT',
                                url: ReportHost + `api/Maintenance/ResetMaintenanceBySerial?companyId=${this.companyId}&serial=${this.serial}&optionName=ThayVo`,
                            };
                            this.cfg.$http(req).then((rep:any)=> {

                                this.MenuRight.KmThayVoCurrent = 0;
                                this.check_right = true;
                                this.updateInfo(true);
                            });
                            break;
                        case 'ThayNhot':
                            var req = {
                                method: 'PUT',
                                url: ReportHost + `api/Maintenance/ResetMaintenanceBySerial?companyId=${this.companyId}&serial=${this.serial}&optionName=ThayNhot`,
                            };
                            this.cfg.$http(req).then((rep:any)=> {

                                this.MenuRight.KmThayNhotCurrent = 0;
                                this.check_right = true;
                                this.updateInfo(true);
                            });
                            break;
                        case 'ThayLocGio':
                            var req = {
                                method: 'PUT',
                                url: ReportHost + `api/Maintenance/ResetMaintenanceBySerial?companyId=${this.companyId}&serial=${this.serial}&optionName=ThayLocGio`,
                            };
                            this.cfg.$http(req).then((rep:any)=> {

                                this.MenuRight.KmThayLocGioCurrent = 0;
                                this.check_right = true;
                                this.updateInfo(true);
                            });
                            break;
                        case 'ThayLocNhot':
                            var req = {
                                method: 'PUT',
                                url: ReportHost + `api/Maintenance/ResetMaintenanceBySerial?companyId=${this.companyId}&serial=${this.serial}&optionName=ThayLocNhot`,
                            };
                            this.cfg.$http(req).then((rep:any)=> {

                                this.MenuRight.KmThayLocNhotCurrent = 0;
                                this.check_right = true;
                                this.updateInfo(true);
                            });
                            break;
                    }
                }

                private buildInfoDeviceRight():Q.IPromise<string> {
                    let endtime = this.GetDateTimeCurrent();
                    let starttime = this.GetStartDateTime();
                    var defer = this.cfg.q.defer<string>();
                    var html_right = `<table class='divTable' style='width: 320px; height: 200px;'>`;
                    html_right += this.buildRow([{
                        title: '<span style="color: #cc3341">THÔNG TIN XE</span>',
                        value: '',
                        colspan: 2,
                        titleWidth:120
                    }]);
                    html_right += this.buildRow([{
                        title: 'Biển số:',
                        value: `<span style="color:#d65c66 ">${this.Bs}</span>`,
                        colspan: 1,
                        titleWidth:70
                    },{
                        title: 'Đội xe:',
                        value: this.group,
                        colspan: 1,
                        titleWidth:70
                    }]);

                    html_right += this.buildRow([{
                        title: 'Số ĐT TB:',
                        value: this.phone,
                        colspan: 1,
                        titleWidth:70
                    }, {
                        title: 'Serial:',
                        value: this.serial,
                        colspan: 1,
                        titleWidth:70
                    }]);
                    html_right += this.buildRow([{
                        title: 'Tài khoản sim:',
                        value: this.money,
                        colspan: 2,
                        titleWidth:100
                    }]);
                    html_right += this.buildRow([{
                        title: `<a class="btn btn-primary" href='bao-cao-doanh-nghiep/bao-cao-tong-hop/${this.companyId}/${this.serial}/${this.GetDay()}/${this.GetMonth()}/${this.GetYear()}' target="_blank" >XEM BÁO CÁO</a>`,
                        value: '',
                        colspan: 1,
                        titleWidth:70
                    },{
                        title: `<a class="btn btn-primary" href='/hanh-trinh-xe/${this.companyId}/${this.serial}/${starttime}/${endtime}' target="_blank">XEM HÀNH TRÌNH</a>`,
                        value: '',
                        colspan: 1,
                        titleWidth:70
                    }]);

                    html_right += '</table>';
                    defer.resolve(html_right);
                    return defer.promise;
                }

                public GetDay(){
                    let starttime:any = new Date();
                    let daynow:any = starttime.getDate() > 9? starttime.getDate(): `0${starttime.getDate()}`;
                    return daynow;
                }
                public GetMonth(){
                    let starttime:any = new Date();
                    let monthnow:any = (starttime.getMonth() + 1) > 9? starttime.getMonth() + 1: `0${starttime.getMonth() + 1}`;
                    return monthnow;
                }
                public GetYear(){
                    let starttime:any = new Date();
                    let yearnow:any = starttime.getFullYear();
                    return yearnow;
                }

                public GetDateTimeCurrent(){
                    let starttime:any = new Date();
                    let hournow:any = starttime.getHours() > 9? starttime.getHours(): `0${starttime.getHours()}`;
                    let minnow:any = starttime.getMinutes() > 9? starttime.getMinutes(): `0${starttime.getMinutes()}`;

                    let daynow:any = starttime.getDate() > 9? starttime.getDate(): `0${starttime.getDate()}`;
                    let monthnow:any = (starttime.getMonth() + 1) > 9? starttime.getMonth() + 1: `0${starttime.getMonth() + 1}`;
                    let yearnow:any = starttime.getFullYear();

                    let tmp = `${hournow}:${minnow}:00 ${monthnow}-${daynow}-${yearnow}`;
                    return tmp;
                }
                public GetStartDateTime(){
                    let starttime:any = new Date();
                    let hournow:any = starttime.getHours() > 9? starttime.getHours(): `0${starttime.getHours()}`;
                    let minnow:any = starttime.getMinutes() > 9? starttime.getMinutes(): `0${starttime.getMinutes()}`;

                    let daynow:any = starttime.getDate() > 9? starttime.getDate(): `0${starttime.getDate()}`;
                    let monthnow:any = (starttime.getMonth() + 1) > 9? starttime.getMonth() + 1: `0${starttime.getMonth() + 1}`;
                    let yearnow:any = starttime.getFullYear();

                    let tmp = `00:00:00 ${monthnow}-${daynow}-${yearnow}`;
                    return tmp;
                }
                /*lấy thông tin hình anh tùy theo mỗi trạng thái thiết bị*/
                private getIcon():any {


                    if (this.lostgsm) {
                        return '../../assets/images/carIcon/break_car.png';
                    } else {
                        if (!this.statusGps) {
                            return '../../assets/images/carIcon/Lostgps_car.png';
                        } else if (!this.dungDo) {
                            return '../../assets/images/carIcon/run_car.png';
                        }
                        else if (!this.trangThaiMay) {
                            return '../../assets/images/carIcon/Lock_car.png';
                        }
                        else if (this.dungDo) {
                            return '../../assets/images/carIcon/Stop_car.png';
                        } else if (this.isOverSpeed) {
                            return '../../assets/images/carIcon/b_car.gif';
                        }
                    }

                    // if(this.lostgsm){
                    //     return '../../assets/images/carIcon/break_car.png';
                    // } else if(!this.statusGps){
                    //     return '../../assets/images/carIcon/Lostgps_car.png';
                    // }else if(!this.dungDo){
                    //     return '../../assets/images/carIcon/run_car.png';
                    // }
                    // else if(!this.trangThaiMay){
                    //     return '../../assets/images/carIcon/Lock_car.png';
                    // }
                    // else if(this.dungDo){
                    //     return '../../assets/images/carIcon/Stop_car.png';
                    // }else if(this.isOverSpeed){
                    //     return '../../assets/images/carIcon/b_car.gif';
                    // }else
                    //     // return '../../assets/images/carIcon/run_car.png';
                    //     console.log('chưa co icon truong hop ngoai le');

                }

                public setMap(map:google.maps.Map) {
                    if (map == null)
                        super.setMap(map);
                    else if (map.getBounds().contains(this.latlng) && this.getMap() == null)//kiểm tra xem xe có nằm trong khu vực hiển thị hay không
                        super.setMap(map);
                    //else
                    //if(this.getMap()!=null)
                    //    super.setMap(null);
                }
            }

        }
    }
}