/**
 * Created by user on 04/26/2016.
 */
/// <reference path="../../../tstyping/googlemaps/google.maps.d.ts"/>
/// <reference path="../library.ts"/>

module App{
    export module Shared{
        export module Library{

            export class BaseCar extends google.maps.OverlayView {
                protected cfg:CarConfig;
                private classStyle:any;
                public latlng:google.maps.LatLng;
                protected iconInfo:any;
                public serial:any;
                protected deviceLabel:any;
                private htmlDeviceOnMap:any;
                private div_:any;
                protected onClick:()=>void;
                protected onRightClick:()=>void;

                constructor(config:CarConfig) {
                    super();
                    this.cfg = config ;
                    //khai báo tên cho label trên marker
                    this.classStyle = 'label-marker';
                    this.latlng = null;
                    this.iconInfo = {
                        icon: new Image(),
                        angle: 0,
                        size: {
                            width: 38,
                            height: 38
                        }
                    };

                    this.serial = "";
                    /*serial của thiết bị*/
                    this.deviceLabel = "";
                    /*thông tin thiết bị hiển trên bản đồ (số serial, số tài , biển số )*/
                    this.htmlDeviceOnMap = "";
                    /*thông tin html của thiết bị trên map*/
                }

                draw() {
                    try {
                        var projection = this.getProjection();
                        if (projection != null) {
                            var position = projection.fromLatLngToDivPixel(this.latlng);
                            var div = this.div_;
                            div.style.left = (position.x - 16) + 'px';
                            div.style.top = (position.y - 16) + 'px';
                            div.style.display = 'block';
                        }
                    } catch (e) {
                        console.debug("Car Draw : " + e.message);
                    }

                }

                onRemove() {
                    if(this.div_==null||this.div_==undefined)
                        console.log(`OnRemove element div null : ${this.serial} `);
                    //this.div_.parentNode.removeChild(this.div_);
                    $('#parent_c_'+this.serial).remove();
                    //console.log(`OnRemove : ${this.serial} `);
                }

                onAdd() {
                    try {
                        //console.log(`onAdd : ${this.serial} `);
                        var div = document.createElement("div");
                        div.id='parent_c_'+this.serial;
                        div.style.position = "absolute";
                        //div.innerHTML = '<div  class="' + this.classStyle + '">' + this.htmlDeviceOnMap + '</div>';

                        var parentDiv=document.createElement('div');
                        parentDiv.className=this.classStyle;




                        /*tạo ra một canvas chứa chứa hình ảnh của thiết bị*/

                        var canvas=document.createElement('canvas');
                        var context = canvas.getContext('2d');
                        canvas.id='c_'+this.serial;
                        canvas.width=this.iconInfo.size.width;
                        canvas.height=this.iconInfo.size.height;
                        canvas.style.zIndex="999999";
                        canvas.style.position = 'absolute';

                        var make_base = ()=>{
                            var base_image = new Image();
                            base_image.src = '../../assets/images/loading.jpg';
                            base_image.className = 'loader';
                            base_image.onload = function(){
                                context.drawImage(base_image, 0, -30);

                            }
                        };

                        canvas.onclick=(ev:MouseEvent)=>{
                            if(this.onClick!=undefined&&this.onClick!=null) {
                                // var a = document.getElementById('c_'+this.serial).style;
                                // a.animation = 'spin 2s linear infinite';
                                // a.webkitAnimation = 'spin 2s linear infinite';
                                //
                                // console.log(ev);
                                ev.stopPropagation();


                                // make_base();
                                this.onClick();

                                // context.restore();
                            }

                            $('canvas.xetarget').removeClass('xetarget');
                            canvas.className='xetarget';
                        };


                        canvas.onmousemove=(ev:MouseEvent)=>{
                            var a = document.getElementById('c_'+this.serial).style;
                            if (a.cursor = 'default')
                            {
                                a.cursor = 'pointer';
                            }

                        };
                        canvas.oncontextmenu=(ev:MouseEvent)=>{
                            console.log('car right click');
                            // sự kiện kick chuột phải vào chiếc xe
                            if(this.onRightClick != undefined && this.onRightClick() != null){
                                this.onRightClick();
                            }
                        };

                        /*tạo biển số*/
                        var bsDiv=document.createElement('div');
                        bsDiv.id='bs_'+this.serial;
                        bsDiv.className='style-bs';
                        bsDiv.innerHTML=this.deviceLabel;

                        parentDiv.appendChild(canvas);
                        parentDiv.appendChild(bsDiv);
                        div.appendChild(parentDiv);
                        this.div_ = div;

                        this.getPanes().floatShadow.appendChild(div);
                        this.rotate();
                    } catch (e) {
                        console.debug("CarOnAdd : " + e.message);
                    }

                }

                // hàm xoay icon
                rotate() {
                    try {
                        this.draw();
                        if (this.iconInfo.icon.src == undefined || this.iconInfo.icon.src == null) {
                            console.log(`rotate: icon == null: ${this.serial}`);
                            return;
                        }
                        var a = 2 * Math.PI / 360 * this.iconInfo.angle;
                        var cosa = Math.cos(a);
                        var sina = Math.sin(a);
                        var element = document.getElementById('c_' + this.serial);
                        if (element == null || element == undefined) {
                            return;
                        }
                        var canvas = element.getContext('2d');
                        if (canvas == null) console.debug("Error Rotate");
                        canvas.clearRect(0, 0, this.iconInfo.size.width, this.iconInfo.size.height);     // clear the canvas
                        canvas.save();                   // save the canvas state
                        canvas.rotate(a);            // rotate the canvas
                        canvas.translate((this.iconInfo.size.width/2) * sina + (this.iconInfo.size.width/2) * cosa, (this.iconInfo.size.height/2) * cosa - (this.iconInfo.size.height/2) * sina); // translate the canvas 16,16 in the rotated axes
                        canvas.drawImage(this.iconInfo.icon, -(this.iconInfo.size.width/2), -(this.iconInfo.size.height/2));   // plot the car
                        canvas.restore();                // restore the canvas state, to undo the rotate and translate
                    }
                    catch (e) {
                        console.error("rotate " + this.iconInfo.icon.src + e.message);
                    }
                }
            }
        }
    }
}
