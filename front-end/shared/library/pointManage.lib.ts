/**
 * Created by phamn on 22/05/2016.
 */
/// <reference path="point.lib.ts" />
module App{
    export module Shared{
        export module Library{
            export class PointManage{
                companyId:number;
                mainMap:any;
                pointArray:any;
                point_Arrays:any;
                draw_circle:any;
                choice_marker:any;
                isShow: boolean;
                isLoadComplete:boolean;
                listenerArray:any;
                q:any;
                acc:any;
                geocoder:any;
                constructor(map:google.maps.Map,$q:ng.IQService, _companyId:number, public $http: ng.IHttpService, public global: App.Shared.GlobalService){
                    this.mainMap = map;
                    this.$http = $http;
                    this.companyId=_companyId;
                    this.q=$q;
                    this.pointArray=[];
                    this.point_Arrays=[];
                    this.draw_circle = null;
                    this.choice_marker=null;
                    this.isShow=false;
                    this.isLoadComplete=false;
                    this.listenerArray=[];
                    this.global.ReportHost = global.ReportHost;
                    this.geocoder = new google.maps.Geocoder();
                    this.acc = this.global.Account.GetValue();

                }
                addListener(id:any,name:any,callback:any){
                    var check=true;
                    angular.forEach(this.listenerArray,function(it){
                        if(it.id==id&&it.name==name)
                            check=false;
                    });
                    if(check)
                        this.listenerArray.push({id:id,name:name,callback:callback});
                }
                /*lấy thông tin menu từ server*/
                load(companyId:any){
                    // if(this.pointArray.length > 0){
                    //     this.clear();
                    // }
                    // if(this.point_Arrays.length > 0){
                    //     this.clearhanhtrinh();
                    // }


                    this.companyId = companyId;
                    var defer=this.q.defer();


                    /*lấy thông tin từ server*/
                    this.$http.get(this.global.ReportHost + `api/GpsCheckPoint/GpsCheckPointByCompany?companyId=${this.companyId}`).then((rep:any)=>{
                        this.isLoadComplete = false;
                        // console.log(rep.data.GpsCheckPointTranfers);
                        angular.forEach(rep.data.GpsCheckPointTranfers,(d:any)=>{
                            var id= d.Id;
                            var position=new google.maps.LatLng(d.Location.Lat,d.Location.Lng);
                            var p = new App.Shared.Library.Point(this.mainMap,d.Radius,position,d.Id,d.Name,d);
                            // p.marker.setMap(this.mainMap);
                            this.add(id,p);
                            this.point_Arrays.push({Id:d.Id,Name:d.Name, Radius:d.Radius, value:p});
                        });
                        activeListenerEvent(this);
                        defer.resolve(this.point_Arrays);
                        this.isLoadComplete=true;
                    });
                    return defer.promise;
                }


                //lấy thông tin điểm trạm thu phí
                

                clear(){
                    while(this.pointArray.length>0){
                        var p=this.pointArray.pop().value;
                        p.hide();
                        p.dispose();
                    }

                }

                clearhanhtrinh(){
                    while(this.point_Arrays.length>0){
                        var p=this.point_Arrays.pop().value;
                        p.hide();
                        p.dispose();

                    }

                }

                get(Name:any){
                    var result:any;
                    angular.forEach(this.pointArray,function(obj){
                        if(obj.value.name==Name)
                            result=obj.value;
                    });
                    return result;
                }
                getAllPoint(){
                    return this.pointArray;
                }
                show(val:any){
                    console.log('hiển thị điểm');
                    this.isShow=val;
                    angular.forEach(this.pointArray,function(p){
                        if(val){

                            p.value.show();
                            p.value.showinfowindow();
                        } else{
                            p.value.hide();
                            p.value.closeinfo();
                        }

                    })
                }
                
                //xóa điểm
                pointDelete(p:any){
                    /*call back server*/
                    bootbox.dialog({
                        title:"<h3 class='text-center'>Xoá điểm</h3>",
                        message: "Ban có muốn xoá: " + p.name,
                        buttons:{
                            success: {
                                label: "<span class='glyphicon glyphicon-ok'></span>"+"&nbspĐồng ý",
                                className: "btn-success",
                                callback:()=>{
                                    p.hide();
                                    p.dispose();
                                    var index = 0;
                                    angular.forEach(this.pointArray,(val:any, i:any) =>{
                                        if (val.id === p.id) {
                                            this.$http.delete(this.global.ReportHost + `api/GpsCheckPoint/DeleteGpsCheckPoint?companyId=${this.companyId}&gpsCheckPointId=${p.id}`).then((rep:any) =>{
                                                // console.log(rep);
                                            });
                                        }
                                    });
                                }
                            },
                            main: {
                                label: "<span class='glyphicon glyphicon-remove'></span>"+"&nbspHủy",
                                className: "btn-danger",
                                callback: function() {

                                }
                            }
                        }
                    });

                };




                // gọi hàm xoá lên server
                callbackDeletePoint(id:any,index:any){
                    var res:any = [id];

                // this.ul.processMethodApi(this.ul.global.api.map.point.deletePoint,null,{
                //     companyId:this.companyId,
                //     pointId:id
                // }).then(function(data){
                //     $this.pointArray.splice(index, 1);
                //     $this.ul.message.warning('xoá thành công');
                //
                //     activeListenerEvent($this);
                // },function(status){
                //     $this.ul.message.warning('xoá không thành công');
                // });
                }


            /*Hàm cập nhật*/
                pointEdit(p:any){
                /*call back server*/
                    var index:any = 0;
                    angular.forEach(this.pointArray,(val:any, i:any)=> {
                        if (val.id === p.id) {
                            var infoPoint:any={Name: p.marker.getTitle(),Radius: p.cricle.getRadius()};
                            this.showFormInfo(p,null).then((data:any)=>{
                                if(data.posNew === undefined)
                                {
                                    var res={
                                        CompanyId:this.companyId,
                                        Id:p.id,
                                        Location: p.value.Location,
                                        Name:data.Name,
                                        Description:data.Description,
                                        Radius:data.Radius
                                    };
                                    // console.log(res);
                                    //this.callBackServerUpdate(res,p.id);
                                    this.$http.put(this.global.ReportHost + `api/GpsCheckPoint/UpdateGpsCheckPoint?companyId=${this.companyId}`,res).then((rep:any)=>{
                                        // console.log(rep);
                                    });
                                }
                                else
                                {

                                    this.getAddress(data.posNew).then((mess:any)=>{
                                        var location={Lat:data.posNew.lat(),Lng:data.posNew.lng(),Address:mess};
                                        var obj={CompanyId: this.companyId,Location:location,Name:data.Name,Radius:data.Radius,Description: data.Description, Id:p.id};
                                        this.$http.put(this.global.ReportHost + `api/GpsCheckPoint/UpdateGpsCheckPoint?companyId=${this.companyId}`, obj).then((rep:any)=>{
                                            // console.log(rep);
                                        });
                                    });
                                }
                                p.editPointCircle(data.Name,data.Radius,data.Description);
                            });
                        }
                    });
                }
                
                //thêm điểm mới trên server
                newPoint(position:any){

                    /*hiển thị form nhập dữ liệu*/
                    this.showFormInfo(null,position).then((data:any)=>{
                        /*lấy thông tin địa chỉ sau khi người dùng đã nhập xong */
                        this.getAddress(position).then((mess:any)=>{
                            var location={Lat:position.lat(),Lng:position.lng(),Address:mess};
                            var data_req={CompanyId: this.companyId, Location:location , Name: data.Name, Radius:data.Radius, Description: data.Description};
                            // console.log(data_req);
                            this.$http.post(this.global.ReportHost + `api/GpsCheckPoint/AddGpsCheckPoint?companyId=${this.companyId}`,data_req ).then((rep:any)=>{
                                // console.log(rep);
                                var p:any = new App.Shared.Library.Point(this.mainMap,data.Radius,position,rep.data.AddGpsCheckPointId,data.Name,data_req);
                                this.isShow=true;
                                this.add(rep.data.AddGpsCheckPointId,p);

                                // /*báo cho người sử dung biết là đã tạo điểm thành công*/
                                // $this.ul.message.warning('thêm thành công');
                            });
                        });
                    });
                }
                
                add(id:any,p:any){
                    p.onDelete=this.pointDelete.bind(this);
                    p.onEdit=this.pointEdit.bind(this);
                    this.pointArray.push({id:id,value:p, slXe:0,Name:p.name,Radius: p.radius });
                    activeListenerEvent(this);
                    if(this.isShow)
                        p.show();
                    else
                        p.hide();
                };
                
                checkCarInPoint(carArray:any){
                    angular.forEach(this.pointArray,(ar:any)=>{
                        ar.value.checkCarInPoint(carArray);
                    });
                    activeListenerEvent(this);
                };
                showFormInfo(val:any,p:any){
                    var defer=this.q.defer();
                    if(val== null || val == undefined)
                    {
                        bootbox.dialog({
                            title:"<h3 class='text-center'>Thêm điểm</h3>",
                            message: buildHtmlNew(null),
                            buttons:{
                                success: {
                                    label: "<span class='glyphicon glyphicon-ok'></span>"+"&nbspĐồng ý",
                                    className: "btn-success",
                                    callback: function() {
                                        var result={Name:$('#managePoint-idName').val(),Radius:parseFloat($('#managePoint-radius').val()),Description: $('#managePoint-description').val()};

                                        defer.resolve(result);
                                    }
                                },
                                main: {
                                    label: "<span class='glyphicon glyphicon-remove'></span>"+"&nbspHuỷ",
                                    className: "btn-danger",
                                    callback: function() {
                                        defer.reject();
                                    }
                                }
                            }
                        });

                    }
                    else{
                        bootbox.dialog({
                            title:"<h3 class='text-center'>Cập nhật điểm</h3>",
                            message: buildHtmlNew(val),
                            buttons:{
                                success: {
                                    label: "<span class='glyphicon glyphicon-ok'></span>"+"&nbspĐồng ý",
                                    className: "btn-success",
                                    callback: function() {
                                        var result={Name:$('#managePoint-idName').val(),Radius:parseFloat($('#managePoint-radius').val()), posNew : val.positionDrag, Description: $('#managePoint-description').val()};
                                        val.position=val.positionDrag;
                                        defer.resolve(result);
                                    }
                                },
                                main: {
                                    label: "<span class='glyphicon glyphicon-remove'></span>"+"&nbspHủy",
                                    className: "btn-danger",
                                    callback: function() {
                                        // set lại vị trí cho điểm
                                        val.cricle.setCenter(val.position);
                                        val.marker.setPosition(val.position);
                                        defer.reject();
                                    }
                                }
                            }
                        });

                        $('#managePoint-idName').val(val.marker.getTitle());
                        $('#managePoint-radius').val(val.cricle.getRadius());
                    }
                    return defer.promise;

                };
                getAddress(p:any){
                    var defer=this.q.defer();
                    this.geocoder.geocode({'latLng': p},(results:any, status:any)=> {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                defer.resolve(results[0].formatted_address);
                            } else {
                                defer.resolve("");
                            }
                        } else {
                            defer.resolve("");
                        }
                    });
                    return defer.promise;
                }
                showInfo(mes:any,tit:any){
                    bootbox.dialog({
                        message: mes,
                        title: tit,
                        buttons: {
                            danger: {
                                label: "<span class='glyphicon glyphicon-ok'></span>"+"&nbspĐồng ý",
                                className: "btn-success",
                                callback: function() {

                                }
                            }
                        }
                    });
                }
            }

            // nội dung thêm mới điểm
            function buildHtmlNew(infoPoint:any){
                var html = "";
                html = "<form class='form-horizontal bucket-form' role='form'>" +
                            "<div class='form-group'>" +
                                "<label class='col-sm-3 control-label'>Tên điểm:</label>" +
                                "<div class='col-sm-6'>" +
                                    "<input type='text' placeholder='Nhập tên điểm' class='form-control' id='managePoint-idName' required>" +
                                "</div>" +
                            "</div>" +
                            "<div class='form-group'>" +
                                "<label class='col-sm-3 control-label'>Bán kính:</label>" +
                                "<div class='col-sm-6'>" +
                                    "<input type='text' placeholder='Nhập bán kính (m)' class='form-control' id='managePoint-radius' required>" +
                                "</div>" +
                            "</div>" +
                            "<div class='form-group'>" +
                                "<label class='col-sm-3 control-label'>Ghi chú:</label>" +
                                "<div class='col-sm-6'>" +
                                    "<input type='text' placeholder='Nhập mô tả' class='form-control' id='managePoint-description' required>" +
                                "</div>" +
                            "</div>" +
                    "</form>";
                return html;
            }
        }
    }
}
