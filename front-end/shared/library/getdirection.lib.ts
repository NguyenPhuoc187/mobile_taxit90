/**
 * Created by phamn on 22/06/2016.
 */
module App{
    export module Shared{
        export module Library{
            export class Getdirection{
                map: any;
                directionsRendererOptions: any;
                directionsRenderer:any;
                directionsService:any;
                markerOptions:any;
                originMarker:any;
                destinationMarker:any;
                geocoder:any;
                marker:any;
                origin_place_id:any;
                destination_place_id: any;
                travel_mode: any;

                constructor(map_: google.maps.Map,$q:ng.IQService){
                    this.map = map_;
                    this.markerOptions={};
                    this.directionsRendererOptions={};
                    this.directionsRendererOptions.draggable=true;
                    this.directionsRendererOptions.hideRouteList=true;
                    this.directionsRendererOptions.suppressMarkers=false;
                    this.directionsRendererOptions.preserveViewport=false;
                    this.directionsRenderer = new google.maps.DirectionsRenderer(this.directionsRendererOptions);
                    this.directionsService = new google.maps.DirectionsService();
                    this.geocoder = new google.maps.Geocoder();
                    this.marker = {};
                    this.origin_place_id = {};
                    this.destination_place_id = {};
                    this.travel_mode = google.maps.TravelMode.WALKING;
                }
                DirectionOrigin(point:any){
                    // this.markerOptions.icon='http://www.google.com/intl/en_ALL/mapfiles/markerA.png';

                    this.markerOptions.map=null;
                    this.markerOptions.position=new google.maps.LatLng(0, 0);
                    this.markerOptions.title='Directions origin';
                    this.originMarker = new google.maps.Marker(this.markerOptions);
                    this.originMarker.setPosition(point);
                    if(!this.originMarker.getMap()){
                        this.originMarker.setMap(this.map);
                    }
                }
                DirectionDestination(point:any){

                    // this.markerOptions.icon='http://www.google.com/intl/en_ALL/mapfiles/markerB.png';
                    this.markerOptions.title='Directions destination';
                    this.destinationMarker =new google.maps.Marker(this.markerOptions);
                    this.destinationMarker.setPosition(point);
                    if(!this.destinationMarker.getMap()){
                        this.destinationMarker.setMap(this.map);
                    }
                }
                // Getdirection(){
                //     var directionsRequest:any={};
                //     directionsRequest.destination=this.destinationMarker.getPosition();
                //     directionsRequest.origin= this.originMarker.getPosition();
                //     directionsRequest.travelMode=google.maps.TravelMode.DRIVING;
                //
                //     this.directionsService.route(directionsRequest,(result:any, status:any)=>{
                //         if(status===google.maps.DirectionsStatus.OK){
                //             //	hide the origin and destination markers as the DirectionsRenderer will render Markers itself
                //             this.originMarker.setMap(null);
                //             this.destinationMarker.setMap(null);
                //             this.directionsRenderer.setDirections(result);
                //             this.directionsRenderer.setMap(this.map);
                //             //	hide all but the 'Clear directions' menu item
                //             document.getElementById('clearDirectionsItem').style.display='block';
                //             document.getElementById('directionsDestinationItem').style.display='none';
                //             document.getElementById('directionsOriginItem').style.display='none';
                //             document.getElementById('getDirectionsItem').style.display='none';
                //         } else {
                //
                //             bootbox.dialog({
                //                 title: "Thông Báo",
                //                 message: "Vị trí chỉ đường không đúng. Xin chọn vị trí phù hợp!!",
                //                 buttons: {
                //                     cancle: {
                //                         label: "Đồng ý",
                //                         className: "btn-success",
                //                         callback:()=>{
                //                             this.originMarker.setMap(null);
                //                             this.destinationMarker.setMap(null);
                //                         }
                //                     },
                //
                //                 }
                //             });
                //         }
                //     });
                // }
                Cleardirection(){
                    this.directionsRenderer.setMap(null);
                    //	set CSS styles to defaults
                    document.getElementById('clearDirectionsItem').style.display='';
                    // document.getElementById('directionsDestinationItem').style.display='';
                    // document.getElementById('directionsOriginItem').style.display='';
                    document.getElementById('getDirectionsItem').style.display='';
                    $("#right-panel").hide();
                    $("#right-panel-hightwayzone").hide();

                }

                Searchdirection(){
                    var input:any = /** @type {!HTMLInputElement} */(
                        document.getElementById('address-input'));


                    // var types = document.getElementById('type-selector');
                    // this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                    // this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

                    var autocomplete:any = new google.maps.places.Autocomplete(input);
                    autocomplete.bindTo('bounds', this.map);

                    var infowindow = new google.maps.InfoWindow();
                    this.markerOptions = {
                        map: this.map,
                        anchorPoint: new google.maps.Point(0, -29)
                    };
                    this.marker = new google.maps.Marker(this.markerOptions);

                    autocomplete.addListener('place_changed',()=> {
                        infowindow.close();
                        document.getElementById('searchdirections').className='close-button';
                        $('#searchdirections').parent().click(()=>{
                                //alert('hello-nút xóa');
                            var input:any = $("#address-input").val("");
                            this.marker.setVisible(false);
                            infowindow.close();
                            document.getElementById('searchdirections').className='searchbox-directions';
                        });
                        this.marker.setVisible(false);
                        var place = autocomplete.getPlace();
                        if (!place.geometry) {
                            bootbox.dialog({
                                title: "Thông Báo",
                                message: "Vui lòng chọn lại!!",
                                buttons: {
                                    cancle: {
                                        label: "Đồng ý",
                                        className: "btn-success",
                                    },

                                }
                            });
                            return;
                        }

                        // If the place has a geometry, then present it on a map.
                        if (place.geometry.viewport) {
                            this.map.fitBounds(place.geometry.viewport);
                        } else {
                            this.map.setCenter(place.geometry.location);
                            this.map.setZoom(17);  // Why 17? Because it looks good.
                        }
                        this.marker.setIcon(/** @type {google.maps.Icon} */({
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(35, 35)
                        }));
                        this.marker.setPosition(place.geometry.location);
                        this.marker.setVisible(true);

                        var address = '';
                        if (place.address_components) {
                            address = [
                                (place.address_components[0] && place.address_components[0].short_name || ''),
                                (place.address_components[1] && place.address_components[1].short_name || ''),
                                (place.address_components[2] && place.address_components[2].short_name || '')
                            ].join(' ');
                        }

                        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                        infowindow.open(this.map, this.marker);

                    });

                    // if(document.getElementById('searchdirections').className== 'close-button'){
                    //
                    // }

                }

                Getdirection(){
                    this.directionsRenderer.setMap(this.map);
                    this.directionsRenderer.setPanel(document.getElementById('right-panel'));
                    this.directionsRenderer.setPanel(document.getElementById('right-panel-hightwayzone'));

                    var origin_input:any = document.getElementById('origin-input');
                    var destination_input:any = document.getElementById('destination-input');

                    var origin_autocomplete:any = new google.maps.places.Autocomplete(origin_input);
                    origin_autocomplete.bindTo('bounds', this.map);
                    var destination_autocomplete:any = new google.maps.places.Autocomplete(destination_input);
                    destination_autocomplete.bindTo('bounds', this.map);

                    function expandViewportToFitPlace(map:any,place:any){
                        if (place.geometry.viewport) {
                            map.fitBounds(place.geometry.viewport);
                        } else {
                            map.setCenter(place.geometry.location);
                            map.setZoom(17);
                        }
                    }

                    origin_autocomplete.addListener('place_changed',()=> {
                        var place:any = origin_autocomplete.getPlace();
                        if (!place.geometry) {
                            window.alert("Autocomplete's returned place contains no geometry");
                            return;
                        }
                        expandViewportToFitPlace(this.map, place);

                        // If the place has a geometry, store its place ID and route if we have
                        // the other place ID
                        this.origin_place_id = place.place_id;
                        route(this.origin_place_id, this.destination_place_id,this.travel_mode, this.directionsService, this.directionsRenderer);
                    });

                    destination_autocomplete.addListener('place_changed',()=> {
                        var place:any = destination_autocomplete.getPlace();
                        if (!place.geometry) {
                            window.alert("Autocomplete's returned place contains no geometry");
                            return;
                        }
                        expandViewportToFitPlace(this.map, place);
                        $("#right-panel").show();
                        $("#right-panel-hightwayzone").show();
                        document.getElementById('clearDirectionsItem').style.display='block';
                        document.getElementById('getDirectionsItem').style.display='none';

                        // If the place has a geometry, store its place ID and route if we have
                        // the other place ID
                        this.destination_place_id = place.place_id;
                        route(this.origin_place_id, this.destination_place_id, this.travel_mode, this.directionsService, this.directionsRenderer);
                    });
                    function route(origin_place_id:any, destination_place_id:any, travel_mode:any, directionsService:any, directionsRenderer:any) {
                        if (!origin_place_id || !destination_place_id) {
                            return;
                        }
                        directionsService.route({
                            origin: {'placeId': origin_place_id},
                            destination: {'placeId': destination_place_id},
                            travelMode: travel_mode
                        },(response:any, status:any)=> {
                            if (status === google.maps.DirectionsStatus.OK) {
                                directionsRenderer.setDirections(response);
                            } else {
                                window.alert('Directions request failed due to ' + status);
                            }
                        });
                    }
                    
                }


            }
            
        }
    }
}