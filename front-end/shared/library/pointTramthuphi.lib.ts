/**
 * Created by phamn on 29/08/2016.
 */
module App{
    export module Shared{
        export module Library{
            export class PointTramthuphi extends Point{
                constructor(map_:google.maps.Map,rad:any, pos:any, id:any, name:any,_data:any,address:any){
                    super(map_,rad,pos,id,name,_data);
                }
                hidemenu(){
                    this.marker.setMap(this.map);
                    this.cricle.setMap(this.map);
                    // this.menu.setMap(null);
                    this.marker.setDraggable(false);

                }
                show(){
                    var infowindow = new google.maps.InfoWindow({
                        content: this.marker.getTitle(),
                        position: this.marker.getPosition()
                    });

                    this.marker.setMap(this.map);
                    this.marker.addListener('click', function() {
                        infowindow.open(this.map, this.marker);
                    });
                    this.cricle.setMap(this.map);
                    ///infowindow.setMap(this.map);
                    this.initialize();
                }

            }
        }

    }

}