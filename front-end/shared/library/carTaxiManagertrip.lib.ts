/**
 * Created by NPPRO on 28/12/2016.
 */

/// <reference path="../library.ts"/>
module App{
    export module Shared{
        export module Library{
            export class CarTaxiManagerTrip {
                protected cfg:CarConfig;
                private listArray:Array<any>;
                private onChange:(event:any)=>any;// sự kiện xảy ra khi thay đổi record
                private current=0;/*lưu trữ vị trí hiện tại khi cho xe chạy trong hành trình*/
                private autoPlay:any;
                private mode:any;
                public totalCarKmTrip:number;// tổng km hành trình theo xung
                public totalRecord:number;// tổng recode hành trìhh
                private carObject:any;
                private tripView:any;
                private speedPlay:any;
                private lockDraw:boolean;
                private listStop:Array<any>;
                public xematchiakhoa:boolean;
                //this.carObject = new carUtility.baseCar(this.cfg);// đối tượng hiển thị trên bản đồ
                //this.tripView = new carUtility.tripView(this.cfg.map);/*vẽ hành trình lên bản đồ*/
                constructor(config:CarConfig){
                    this.listArray=[];
                    this.listStop=[];
                    this.cfg = config;
                    this.onChange=undefined;// sự kiện xảy ra khi thay đổi record
                    this.carObject = new App.Shared.Library.BaseCar(this.cfg);// đối tượng hiển thị trên bản đồ
                    this.tripView = new App.Shared.Library.TripViewTaxi(this.cfg);/*vẽ hành trình lên bản đồ*/
                    this.current=0;/*lưu trữ vị trí hiện tại khi cho xe chạy trong hành trình*/
                    this.autoPlay;
                    this.mode;
                    this.totalCarKmTrip=0;// tổng km hành trình theo xung
                    this.totalRecord=0;// tổng recode hành trình
                    this.speedPlay=1000;
                    this.lockDraw=false;
                }
                /*load thông tin hành trình từ trên api xuống*/
                /*load thông tin hành trình từ trên api xuống*/
                load = (data:any,serial:string,CarId:any)=> {
                    var defer:any;
                    if (this.cfg.q != undefined)
                        defer = this.cfg.q.defer();
                    var self = this;
                    this.totalRecord = 0;
                    this.totalCarKmTrip = 0;
                    this.clear();
                    this.pause();
                    this.listStop=[];
                    var tongkmCardi = 0;
                    var tongkmGpsdi = 0;
                    // kiểm tra record đầu tiên cho thời gian dừng đỗ
                    var checkrecoder=false;
                    var timeStartStop="";
                    var latLng:any;
                    var oldLatLng:any;
                    var latLngcurren:any;
                    var StatusStopStart:any;
                    var indexPaint:any;
                    //lưu km đầu tiên
                    var kmStart:any;
                    var tongkmtichluy:any;
                    //lưu tọa độ để loại bỏ lúc tắt máy
                    var latLngPaint:any;
                    angular.forEach(data, function (record, idx) {
                        if(idx==0)
                        {
                            kmStart=record.TotalDistance;
                            tongkmtichluy=0;
                        }
                        else{
                            tongkmtichluy=record.TotalDistance-kmStart;
                        }

                        if (self.listArray.length > 0) {
                            tongkmCardi = record.TotalCarKm - self.listArray[0].totalCarKm;
                            tongkmGpsdi = record.TotalGpsKm - self.listArray[0].totalGpsKm;
                        }
                        var angle = 0;
                        latLngcurren=new google.maps.LatLng(record.Location.Lat, record.Location.Lng);
                        if (oldLatLng ==null || oldLatLng ==undefined) {
                            angle = 0;
                            oldLatLng = latLngcurren;
                        }
                        else {
                            try{

                                angle = google.maps.geometry.spherical.computeHeading(oldLatLng,latLngcurren);
                                oldLatLng=latLngcurren;
                                console.log(angle);
                            }
                            catch(ex){
                                console.log(ex);
                                angle=record.Angle;
                            }
                        }

                        if(record.Location.Lat!=0) {
                            var imageurl="../../assets/images/carIcon/run_car.png";
                            if (record.GpsStatus == false) {
                                imageurl = '../../assets/images/carIcon/Lostgps_car.png';
                            }
                            else {
                                if (!record.StatusDevice) {
                                    imageurl = '../../assets/images/carIcon/Stop_car.png';
                                }
                                else {
                                    imageurl = '../../assets/images/carIcon/run_car.png';
                                }

                            }
                            //kiểm tra vận tốc và mở máy để lưu tọa độ
                            if(record.Speed>0 && record.MachineStatus==true)
                            {
                                latLngPaint =new google.maps.LatLng(record.Location.Lat, record.Location.Lng);
                            }
                            if(record.Speed==0 && record.MachineStatus==true)
                            {
                                if(checkrecoder==false ){
                                    latLngPaint =new google.maps.LatLng(record.Location.Lat, record.Location.Lng);
                                }
                                else{
                                    latLngPaint =latLngPaint;
                                }
                            }
                            if(record.MachineStatus==false)
                            {

                                if(checkrecoder==false ){
                                    latLngPaint =new google.maps.LatLng(record.Location.Lat, record.Location.Lng);
                                }
                                else{
                                    latLngPaint =latLngPaint;
                                }


                            }

                            self.listArray.push({
                                serial: serial,
                                CarId: CarId,
                                latLng: latLngPaint,
                                angle: angle,
                                Speed: record.Speed,
                                Distance: tongkmtichluy,
                                TotalDistance: record.TotalDistance,
                                GpsStatus: record.GpsStatus?"Có":"Không",
                                StatusMeter: record.StatusMeter?"Có":"Không",
                                HongNgoai: record.HongNgoai?"Có Khách":"Không Khách",
                                DoorStatus: record.DoorStatus?"Đóng":"Mở",
                                AirMachineStatus: record.AirMachineStatus?"Mở":"Tắt",
                                Fuel: record.Fuel,
                                Power: record.Power,
                                MachineStatus:record.MachineStatus?"Mở":"Tắt",
                                Tempernature: record.Tempernature,
                                TimeUpdate: self.AdapterDate(record.TimeUpdate),
                                kmCarGo: tongkmCardi,
                                kmGpsGo: tongkmGpsdi,
                                index: self.listArray.length,
                                imageData:imageurl,
                                Address:record.Location.Address,
                                pointEnd:null,
                                StatusDevice:record.StatusDevice,
                                CurentMoneySession:record.CurentMoneySession,
                                CurrentMeterKm:record.CurrentMeterKm,
                                TongCuoc:record.TongCuoc,
                                ThoiGianCho:record.ThoiGianCho,
                                GsmSignal:record.GsmSignal,
                                TotalKm:record.TotalKm,
                                SumBanTin: data.length,
                            });
                            //xet các điểm dừng
                            var Pointend:Array<any>=[];
                            if(record.Speed==0 && record.MachineStatus==true)
                            {
                                if(checkrecoder==false ){
                                    StatusStopStart=0;
                                    indexPaint=self.listArray.length-1;
                                    checkrecoder=true;
                                    timeStartStop=record.TimeUpdate;
                                    latLng =new google.maps.LatLng(record.Location.Lat, record.Location.Lng);
                                }
                            }
                            if(record.Speed > 0 && record.MachineStatus==true)
                            {
                                if(checkrecoder==true ){
                                    checkrecoder=false;
                                    var timeSpane=self.ChangeTimeSpan(timeStartStop,record.TimeUpdate);
                                    if(timeSpane >0) {
                                        self.listStop.push({latLng: latLng, title:  timeSpane+" Phút. Thời điểm: "+self.AdapterDate(timeStartStop),status:StatusStopStart,index:indexPaint,statusKhach: record.CurentMoneySession})
                                    }

                                }

                            }
                            if(record.MachineStatus==false)
                            {

                                if(checkrecoder==false ){
                                    StatusStopStart=1;
                                    indexPaint=self.listArray.length-1;
                                    checkrecoder=true;
                                    timeStartStop=record.TimeUpdate;
                                    latLng =new google.maps.LatLng(record.Location.Lat, record.Location.Lng);
                                }
                                else{
                                    StatusStopStart=1;
                                }


                            }

                        }
                    });

                    angular.forEach(self.listStop, function (value, iex) {
                        self.listArray[value.index].pointEnd=value;
                    });

                    var distance_tmp=0;
                    var latlng_tmp:any=null;
                    var lostKeyCount=0;
                    angular.forEach(data,function (r, i) {
                        var tmp=new google.maps.LatLng(r.Location.Lat, r.Location.Lng);
                        if(latlng_tmp!=null){
                            distance_tmp+=google.maps.geometry.spherical.computeDistanceBetween (tmp, latlng_tmp);
                        }
                        latlng_tmp=tmp;
                        if(!r.MachineStatus)
                            lostKeyCount++;
                    });
                    if(lostKeyCount==data.length&&distance_tmp>2000)
                        this.xematchiakhoa=true;
                    else this.xematchiakhoa=false;

                    self.setMode.bind(self);
                    self.setMode("system");
                    self.totalCarKmTrip = self.listArray[self.listArray.length - 1].totalCarKm - self.listArray[0].totalCarKm;
                    self.totalRecord = self.listArray.length;
                    if(defer!=undefined)
                    {
                        defer.resolve(self);
                    }
                    if(defer!=undefined)
                        return defer.promise;
                    return undefined;

                };
                //hàm chuyển thời gian
                AdapterDate=(time:any)=>{
                    var year:string = time.slice(0,4);
                    var month:string = time.slice(5,7);
                    var date:string = time.slice(8,10);

                    var min:string = time.slice(11,19);

                    var tt:any = min +' ' +date + '/' + month + '/' + year ;
                    return tt;
                }
                //hàm tính thời gian
                ChangeTimeSpan=(dateS:any,dateE:any)=> {
                    var date1 = new Date(dateS);
                    var date2 = new Date(dateE);
                    var diff = date2.getTime() - date1.getTime();

                    var days = Math.floor(diff / (1000 * 60 * 60 * 24));
                    diff -= days * (1000 * 60 * 60 * 24);

                    var hours = Math.floor(diff / (1000 * 60 * 60));
                    diff -= hours * (1000 * 60 * 60);

                    var mins = Math.floor(diff / (1000 * 60));
                    diff -= mins * (1000 * 60);

                    var seconds = Math.floor(diff / (1000));
                    diff -= seconds * (1000);


                    var resuflt:any = (hours*60) + mins;
                    return resuflt;
                }
                setMode=(mode:any)=> {

                    this.tripView.clear();
                    if (this.listArray.length > 0) {
                        this.carObject.latlng = this.listArray[0].latLng;
                        this.carObject.iconInfo.icon.src = '../../assets/images/carIcon/Stop_car.png';
                        this.carObject.serial = this.listArray[0].serial;
                        this.carObject.deviceLabel = this.listArray[0].CarId;
                        this.carObject.iconInfo.angle = this.listArray[0].angle;
                        //this.carObject.buildCarViewHtml();
                        this.carObject.setMap(this.cfg.map);
                        this.tripView.push(this.listArray[0]);
                    }
                    this.current = 0;

                };
                clear=()=> {
                    this.pause();
                    this.tripView.clear();

                    this.carObject.setMap(null);
                    var len = this.listArray.length;
                    for (var i = 0; i < len; i++)
                        this.listArray.pop();
                };
                //chức năng next
                next=()=> {
                    this.current++;
                    if (this.current >= this.listArray.length) {
                        this.current = this.listArray.length - 1;
                        this.pause();
                    }
                    this.draw(this, "next", 0);
                };
                //chức năng prev
                prev=()=> {
                    this.current--;
                    if (this.current <= 0)
                        this.current = 0;
                    this.draw(this, "prev", 0);
                };
                //chức năng dừng (pause)
                pause=()=> {
                    if (this.autoPlay != undefined) {
                        clearInterval(this.autoPlay);
                        this.autoPlay = undefined;
                    }
                };
                // chức năng play
                play=()=> {

                    this.pause();
                    var self = this;
                    this.autoPlay = setInterval(function () {
                        self.next();
                    }, this.speedPlay)
                };
                setSpeed=(s:any)=>{
                    this.speedPlay = s <= 100 ? 100 : s;
                    if (this.autoPlay != undefined)
                        this.play();
                };
                seek=(index:any)=> {
                    if (index > this.listArray.length || index < 0) return false;
                    this.current = index;
                    this.tripView.clear();
                    this.draw(this, "seek", index);
                };
                replay=()=> {
                    this.current = 0;
                    this.tripView.clear();
                    this.draw(this, "seek", 0);
                };
                getCurrentRecord=()=> {
                    return this.listArray.length > this.current ? this.listArray[this.current] : null;
                };
                //ham onchange
                onchangeRecode=(obj:any)=> {
                    return obj;
                };
                //hàm lấy hình
                //getimage=(speed:number,gps:boolean)=>{
                //    var urlimage = '';
                //    if (gps == false) {
                //        urlimage = '../../assets/images/carIcon/Lostgps_car.png';
                //    }
                //    else {
                //        if (speed <= 0) {
                //            urlimage = '../../assets/images/carIcon/Stop_car.png';
                //        }
                //        else {
                //            urlimage = '../../assets/images/carIcon/run_car.png';
                //        }
                //
                //    }
                //    return urlimage;
                //}
                draw(obj:any,to:any,seek:any){

                    while (this.lockDraw);
                    this.lockDraw = true;
                    try {

                        /*call hàm event */
                        var current = obj.getCurrentRecord(); // lấy dữ liệu hiện tại
                        if (current == undefined || current == null) obj.pause();
                        if (obj.onChange != undefined)
                            obj.onChange(current);
                        obj.carObject.latlng = current.latLng;
                        obj.carObject.iconInfo.icon.src = current.imageData;
                        obj.carObject.iconInfo.angle = current.angle;

                        // var marker = new SlidingMarker({
                        //     position: obj.carObject.latlng,
                        //     map: obj.cfg.map,
                        //     title: "I'm sliding marker",
                        //     duration: 2000,
                        //     easing: "easeOutExpo"
                        // });

                        if (obj.carObject.getMap() == null) obj.carObject.setMap(obj.cfg.map);
                        obj.carObject.rotate();
                        obj.carObject.draw();
                        switch (to) {
                            case "next":
                                obj.tripView.push(current);
                                break;
                            case "prev":
                                obj.tripView.pop();
                                break;
                            case "seek":
                                if (obj.tripView.length() > seek)
                                    obj.tripView.current(seek);
                                else {
                                    for (var i = obj.tripView.length(); i < seek; i++) {
                                        obj.tripView.push(obj.listArray[i]);
                                    }
                                }
                                break;
                        }
                    }
                    catch (message) {
                        console.log(message);
                    }
                    this.lockDraw = false;

                }

            }

        }
    }
}