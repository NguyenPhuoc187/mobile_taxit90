/**
 * Created by phamn on 23/05/2016.
 */

module App{
    export module Shared{
        export module Library{
            export class  DistanceMapManage {
                map:any;
                polylineDistance:any;
                markers:any;
                eventClickMapDistant:any;
                eventDoubleClickMapDistant:any;
                eventChangeMouse:any;
                pointOld:any;
                distancekm:number;
                sttClickDistace :number;
                constructor(map_: google.maps.Map,$q:ng.IQService){
                    this.map = map_;
                    //this.pointOld = null;
                    this.distancekm = 0;
                    this.sttClickDistace = 0;
                    this.markers = [];
                }
                createPolyline() {
                    this.pointOld = null;
                this.eventChangeMouse = google.maps.event.addListener(this.map, 'mousemove',(event:any)=> {
                    this.map.setOptions({draggableCursor: 'crosshair'});
                });
                var polyOptions:any = {
                    strokeColor: '#000000',
                    strokeOpacity: 1.0,
                    strokeWeight: 3
                };

                this.polylineDistance = new google.maps.Polyline(polyOptions);
                this.polylineDistance.setMap(this.map);

                // Add a listener for the click event
                this.eventClickMapDistant = google.maps.event.addListener(this.map, 'click',(event:any) =>{
                    // console.log("click do khoang cach");

                    //ghi nhận thao tác click đo khoảng cách
                    this.sttClickDistace = this.sttClickDistace + 1;
                    if (this.pointOld === null) {
                        this.pointOld = event.latLng;

                    }
                    else {
                        this.addMarker(event.latLng);
                        this.polylineDistance.getPath().push(event.latLng);
                        this.countDistance(event.latLng);
                    }


                });

                // this.eventDoubleClickMapDistant = google.maps.event.addListener(this.map, 'dblclick',(event:any)=>{
                //    var mess='Khoảng cách đo được'+ this.distancekm;
                //    bootbox.dialog({
                //    message: mess,
                //    title: 'Thông tin đo khoảng cách',
                //    buttons: {
                //        danger: {
                //            label: "<span class='glyphicon glyphicon-ok'></span>"+"&nbspVẽ tiếp",
                //            className: "btn-danger",
                //            callback: function() {
                //            }
                //        },
                //        main: {
                //            label: "<span class='glyphicon glyphicon-remove'></span>"+"&nbspThoát",
                //            className: "btn-danger",
                //            callback: function() {
                //               this.deleteDistance();
                //                $(".map_infoDistance").hide();
                //            }
                //        }
                //    }
                //    });
                // });
             }
                addMarker(location:any){
                    var marker = new google.maps.Marker({
                        position: location,
                        map: this.map
                    });
                    this.markers.push(marker);
                }
                setMapOnMarker(map:any){
                    for (var i = 0; i < this.markers.length; i++) {
                        this.markers[i].setMap(map);
                    }
                }
                removeAllMarker(){
                    this.setMapOnMarker(null);
                }
                deleteDistance() {
                    if(this.polylineDistance!=null||this.polylineDistance!=undefined) {
                        this.polylineDistance.setMap(null);
                        delete this.polylineDistance;
                        this.distancekm = 0;
                        this.sttClickDistace = 0;
                        this.pointOld = null;
                        this.removeAllMarker();
                        google.maps.event.removeListener(this.eventChangeMouse);
                        google.maps.event.removeListener(this.eventClickMapDistant);
                        $('#infoDistance').val(this.distancekm);
                        //google.maps.event.removeListener(this.eventDoubleClickMapDistant);
                        this.map.setOptions({draggableCursor: 'default'});
                    }
                }
                countDistance(po:any) {
                    if (this.sttClickDistace > 2) {
                        this.distancekm = this.distancekm + Math.round(google.maps.geometry.spherical.computeDistanceBetween(this.pointOld, po));

                    }
                    $('#infoDistance').val(this.distancekm + ' (m)');
                    this.pointOld = po;
                }
            }
        }
    }
}




















