/**
 * Created by NPPRO on 22/11/2016.
 */
module App{
    export module Shared{
        export module Library{
            export class AreaTimdothatlac extends Area{
                eventMutiClick:any;
                Name:string;
                area:any;
                bermudaTriangle:any;
                line:any;
                firstClickLatLng:any;
                Point:any;
                constructor(map_:google.maps.Map){
                    super(map_);
                }
                infoArea(){
                    this.eventChangeMouse=google.maps.event.addListener(this.map,'mousemove',(event:any) =>{
                        this.map.setOptions({ draggableCursor: 'crosshair' });

                        // if(this.poly !== null){
                        //     var path = this.poly.getPath();
                        //     if(path.length == 1){
                        //         path.push(event.latLng);
                        //     }else if(path.length > 1){
                        //         path.pop();
                        //         path.push(event.latLng);
                        //     }
                        // }
                    });
                    var polyOptions = {
                        strokeColor: '#FF6B6B',
                        strokeOpacity: 1.0,
                        strokeWeight: 3
                    };
                    this.poly = new google.maps.Polyline(polyOptions);
                    this.poly.setMap(this.map);

                    var firstClick=false;
                    // Add a listener for the click event
                    this.eventClickMap=google.maps.event.addListener(this.map, 'click',(event:any)=>{

                        // if(!firstClick){firstClick=true ; return;}
                        this.polyline = this.poly.getPath();

                        // Because path is an MVCArray, we can simply append a new coordinate
                        // and it will automatically appear.
                        this.polyline.push(event.latLng);

                    });

                    this.eventClickPolyline=google.maps.event.addListener(this.poly,'click',(event:any)=>{
                        // giải phóng sự kiện click map
                        this.add(this.polyline);
                        google.maps.event.removeListener(this.eventClickMap);
                        //giải phóng sự kiện đổi con trỏ chuột
                        google.maps.event.removeListener(this.eventChangeMouse);
                        // đổi trạng thái chuột về mặc định
                        this.map.setOptions({ draggableCursor: 'default' });
                        // document.getElementById("menu_item_addArea").innerHTML = 'Thêm vùng';
                        // hiển thị bảng thêm điểm
                        this.poly.setMap(null);
                        this.show();
                        this.onShow(this);
                    });

                }
                add(polyline:any){
                    // this.menu=this.createMenuArea();
                    // this.menu.setMapInstance(this.map);
                    this.polygons = new google.maps.Polygon({
                        paths: polyline,
                        strokeColor: '#FF6B6B',
                        strokeOpacity: 0.8,
                        strokeWeight: 3,
                        fillColor: '#FF6B6B',
                        fillOpacity: 0.35
                    });

                    // this.infoWindowArea = new google.maps.InfoWindow();
                    // this.eventClickPolygon=google.maps.event.addListener(this.polygons,'click',(eve:any) =>{
                    //     this.infoWindowArea.setPosition(eve.latLng);
                    //     this.infoWindowArea.open(this.map);
                    // });
                    //bắt sự kiện nhấn chuột phải hiển thị menu delete
                    // this.eventRightClickPolygon=google.maps.event.addListener(this.polygons,'rightclick',(event:any)=>{
                    //     this.menu.show(event.latLng);
                    // });
                    // this.menu.menuEventTrigger= (name:any) =>{
                    //     switch (name) {
                    //         case 'deleteArea':
                    //             this.onDelete(this);
                    //             break;
                    //         case 'hideArea':
                    //             this.hide();
                    //             break;
                    //     }
                    //     this.menu.hide();
                    // };
                }
                delete(){
                    this.poly.setMap(null);
                    google.maps.event.removeListener(this.eventRightClickPolygon);
                    google.maps.event.removeListener(this.eventClickPolygon);
                    google.maps.event.removeListener(this.eventClickMap);
                    google.maps.event.removeListener(this.eventChangeMouse);
                    this.map.setOptions({ draggableCursor: 'default' });
                };


            }
        }
    }
}