var AdMap;
(function (AdMap) {
    var textBackgroundName = '__text';
    function Map(elemnetId, mapInstance) {
        // hook lớp canvas vào googlemap
        var seft = this;
        this.mapInstance = mapInstance;
        this.getImageName = null;
        this.onLeftClick = null;
        this.onRightClick = null;
        this.layers = {};
        this.images = {};
        this.backCanvas = document.createElement("canvas");
        this.backContext = this.backCanvas.getContext('2d');
        this.getLayerText = null;
        this.mainCanvasId = '_mapProc' +elemnetId ;
        this.backContext.font = '13px Roboto';
        elemnetId = '#' + elemnetId;
        var tWait = setInterval(function () {
            var gMapElement = $(elemnetId+' div:first div:first div:first');
            if (gMapElement.length == 0)
                return;
            clearInterval(tWait);
            var eMap = $('.gm-style');
            $(window).resize(function () {
                eMap = $('.gm-style');
                if(eMap == null || eMap == undefined)
                    return;
                seft.setRect(eMap.width(), eMap.height());
                seft.backCanvas.width = eMap.width();
                seft.backCanvas.height = eMap.height();
            });
            gMapElement.append("<canvas id='" + seft.mainCanvasId + "' height='" + eMap.height() + "' width='" + eMap.width() + "' style='z-index: 1;position: absolute; opacity: 1;'></canvas>");
            seft.backCanvas.width = eMap.width();
            seft.backCanvas.height = eMap.height();

            requestAnimationFrame(loop);
        }, 1000);
        // cài đặt các sự kiện
        google.maps.event.addListener(mapInstance, 'mousemove', function (e) {
            var layer = getLayerByPoit(e.pixel);
            if (layer != null)
                mapInstance.setOptions({ draggableCursor: 'pointer' });
            else
                mapInstance.setOptions({ draggableCursor: 'url(http://maps.google.com/mapfiles/openhand.cur), move' });
        });
        google.maps.event.addListener(mapInstance, 'click', function (e) {
            var layer = getLayerByPoit(e.pixel);
            if (layer != null && seft.onLeftClick != null)
                seft.onLeftClick(layer.tag);
        });
        google.maps.event.addListener(mapInstance, 'rightclick', function (e) {
            var layer = getLayerByPoit(e.pixel);
            if (layer != null && seft.onRightClick != null)
                seft.onRightClick(layer.tag);
        });
        google.maps.event.addListener(mapInstance, 'drag', function (e) {
            render(seft);
        });
        google.maps.event.addListener(mapInstance, 'zoom_changed', function (e) {
            render(seft);
        });
        function getLayerByPoit(p) {
            var bound = mapInstance.getBounds();
            var result = null;
            $.each(seft.layers,
                function(i, v) {
                    var layer = v.val;
                    if (!bound.contains(layer.latLng))
                        return true;
                    if (layer.ForceHide)
                        return true;
                    if (layer.pixel == null || layer.pixel == undefined)
                        return true;
                    if (layer.pixel.x - 16 < p.x &&
                        layer.pixel.x + 16 > p.x &&
                        layer.pixel.y - 16 < p.y &&
                        layer.pixel.y + 16 > p.y) {
                        result = layer;
                        return false;
                    }
                    return true;
                });

            return result;
        }
        var timertick = 0;
        function loop(time) {
            if (time - timertick < 100) {
                requestAnimationFrame(loop);
                return;
            }
            timertick = time;
            // console.log('render' + new Date());
            render(seft);
            requestAnimationFrame(loop);
        }
    }
    Map.prototype.setRect = function (width:any, height:any) {
        if(width == null && height == null)
            return;
        $('#' + this.mainCanvasId + '')[0].height = height;
        $('#' + this.mainCanvasId + '')[0].width = width;
    };
    Map.prototype.getLayer = function (id) {
        //for (var i = 0; i < this.layers.length; i++) {
        //    if (i < this.layers.length && this.layers[i].id === id)
        //        return this.layers[i].val;
        //}
        return this.layers[id];
    };
    Map.prototype.addLayer = function (id, val) {
        val.redraw = true;
        val.ForceHide = false;
        val.textPixel = this.backContext.measureText(val.text).width;
        val.image=this.getImage(0, val);
        this.layers[id] = {
            id: id,
            val: val
        };
    };
    Map.prototype.updateLayer = function (id, val) {
        var old = this.getLayer(id);
        if (old != null) {
            var oldAngle = old.angle;
            old = old.val;
            if (old.latLng.lat() != val.latLng.lat() && old.latLng.lng() != val.latLng.lng())
                old.angle = parseInt(google.maps.geometry.spherical.computeHeading(old.latLng, val.latLng));
            old.latLng = val.latLng;
            old.text = val.text;
            old.redraw = true;
            old.tag = val.tag;
            old.textPixel = this.backContext.measureText(old.text).width;
            old.image = this.getImage(oldAngle,old);
        } //else this.addLayer(id, val);
    };
    Map.prototype.registerImage = function (name, image) {
        this.images[name] = {};
        this.images[name].image = image;
    };
    Map.prototype.registerBackground = function (image) {
        this.images[textBackgroundName] = image;
    };
    Map.prototype.deleteLayer = function (id) {
        delete this.layers[id];
    };
    Map.prototype.clearLayer = function () {
        this.layers={};
    };
    Map.prototype.getImage=function(angle,val) {
        if (this.getImageName == null)
            return null;
        var gName = this.getImageName(val.tag);
        if (gName == null)
            return null;
        if (typeof this.images[gName][val.angle] === "undefined") {
            this.images[gName][val.angle] = buildImageWithAngle(this.images[gName].image, val.angle);
        }

        return this.images[gName][val.angle];
    };
    Map.prototype.renderLayer = function (val) {
        var pt = latLngToPixel(val.latLng, this.mapInstance);
        val.pixel = pt;
        var len = val.textPixel + 1;
        // console.log(len /2);

        var image=this.getImage(0,val);
        if (image== null || image==undefined)
            return;
        this.backContext.drawImage(image, 0, 0 ,32, 32, pt.x - 16, pt.y - 16, 32, 32);
        // vẽ thông tin text
        if (this.images[textBackgroundName] == null)
            return;
        this.backContext.drawImage(this.images[textBackgroundName], pt.x - Math.round(len / 2), pt.y + 17, len, 14);
        this.backContext.fillStyle = "#fff";
        this.backContext.fillText(val.text, pt.x - len / 2, pt.y + 28);
    };
    var isRender = false;
    function render(_this) {
        if (isRender)
            return;
        isRender = true;
        // vẽ các xe trên bản đồ lên đây
        if (_this.backContext == null || _this.backContext == undefined)
            _this.backContext = _this.backCanvas.getContext('2d');
        // clear
        _this.backContext.clearRect(0, 0, _this.backContext.canvas.width, _this.backContext.canvas.height);
        _this.backContext.translate(0, 0);
        _this.backContext.textAlign = 'start';
        _this.backContext.font = '13px Roboto';
        // render từng thiết bị
        var bound = _this.mapInstance.getBounds();
        try {
            $.each(_this.layers, function (i, v) {
                if (v.val.redraw == false)
                    return;
                if (v.val.ForceHide == true)
                    return;
                if (bound != null && bound != undefined && !bound.contains(v.val.latLng))
                    return;
                if (v.val.redraw) {
                    _this.renderLayer(v.val);
                    //v.val.redraw=false;
                }
            });
        } catch (e) {

        }

        // coppy từ backbuffer lên cái chính
        var c = document.getElementById(_this.mainCanvasId);
        if (c == null || c === 'undefined') {
            //requestAnimationFrame(function(){render(_this)});
            isRender = false;
            return;
        }
        var ctx = c.getContext("2d");
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ctx.drawImage(_this.backCanvas, 0, 0);
        //var tmp = new Image();
        //tmp.src = _this.backContext.canvas.toDataURL("image/png");
        isRender = false;
        //requestAnimationFrame(function(){render(_this)});
    }
    function fixAngle(angle) {
        if (angle == null || angle === "")
            angle = 0;
        if (angle < 0)
            angle = Math.abs(angle) + 90+45;
        angle = Math.round(angle);
        if (angle >= 360)
            angle = angle - 360;
        return angle;
    }
    function buildCanvasWithAngle(img, angle) {
        //return img;
        var tmpCanvas = document.createElement("canvas").getContext('2d');
        tmpCanvas.height = 32;
        tmpCanvas.width = 32;
        var a = 2 * Math.PI / 360 * angle;
        var cosa = Math.cos(a);
        var sina = Math.sin(a);
        tmpCanvas.clearRect(0, 0, 32, 32); // clear the canvas
        tmpCanvas.save(); // save the canvas state
        tmpCanvas.rotate(a); // rotate the canvas
        tmpCanvas.translate((16) * sina + (16) * cosa, (16) * cosa - (16) * sina); // translate the canvas 16,16 in the rotated axes
        tmpCanvas.drawImage(img, -16, -16); // plot the car
        tmpCanvas.restore();
        return tmpCanvas;
    }
    function buildImageWithAngle(img, angle) {
        //return img;
        var tmpCanvas = document.createElement("canvas").getContext('2d');
        var a = 2 * Math.PI / 360 * angle;
        var cosa = Math.cos(a);
        var sina = Math.sin(a);
        tmpCanvas.clearRect(0, 0, 32, 32); // clear the canvas
        tmpCanvas.save(); // save the canvas state
        tmpCanvas.rotate(a); // rotate the canvas
        tmpCanvas.translate((16) * sina + (16) * cosa, (16) * cosa - (16) * sina); // translate the canvas 16,16 in the rotated axes
        tmpCanvas.drawImage(img, -16, -16); // plot the car
        tmpCanvas.restore();
        var tmp = new Image();
        tmp.src = tmpCanvas.canvas.toDataURL("image/png");
        return tmp;
    }
    function latLngToPixel(latLng, map) {
        var projection = map.getProjection();
        var bounds = map.getBounds();
        var topRight = projection.fromLatLngToPoint(bounds.getNorthEast());
        var bottomLeft = projection.fromLatLngToPoint(bounds.getSouthWest());
        var scale = Math.pow(2, map.getZoom());
        var worldPoint = projection.fromLatLngToPoint(latLng);
        return { x: Math.floor((worldPoint.x - bottomLeft.x) * scale) + 1, y: Math.floor((worldPoint.y - topRight.y) * scale) + 1 };
    }
    AdMap.map = Map;
})(AdMap || (AdMap = {}));
