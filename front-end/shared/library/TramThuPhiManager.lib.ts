/**
 * Created by phamn on 29/08/2016.
 */
module App{
    export module Shared{
        export module Library{
            export class TramThuPhiManager extends PointManage{
                constructor(map:google.maps.Map,$q:ng.IQService, _companyId:number, public $http: ng.IHttpService, public global: App.Shared.GlobalService){
                    super(map,$q,_companyId,$http,global);

                }
                load(companyId:any){
                    var defer=this.q.defer();
                    /*làm sạch danh sách chứa*/
                    this.clear();

                    /*lấy thông tin từ server*/
                    this.$http.get('http://geocode.adsun.vn/api/CheckZone/GetAllTollbooth').then((rep:any)=>{
                        this.isLoadComplete = false;
                        // console.log(rep.data.GpsCheckPointTranfers);
                        angular.forEach(rep.data.TollboothPointList,(d:any)=>{
                            var id= d.Id;
                            var position=new google.maps.LatLng(d.Lat,d.Lng);
                            var p = new App.Shared.Library.PointTramthuphi(this.mainMap,d.Radius,position,d.Id,d.Name,d,d.Address);
                            if(this.acc.Level < 2){
                                p.marker.setMap(this.mainMap);

                                this.add(id,p);
                                p.show();
                            }else{
                                p.hidemenu();
                            }
                        });
                        activeListenerEvent(this);
                        defer.resolve(this.pointArray);
                        this.isLoadComplete=true;
                    });
                    return defer.promise;
                }
                pointDelete(p:any){
                    if(this.acc.Level < 2){
                        bootbox.dialog({
                            title:"<h3 class='text-center'>Xoá điểm</h3>",
                            message: "Ban có muốn xoá: " + p.name,
                            buttons:{
                                success: {
                                    label: "<span class='glyphicon glyphicon-ok'></span>"+"&nbspĐồng ý",
                                    className: "btn-success",
                                    callback:()=>{
                                        p.hide();
                                        p.dispose();
                                        var index = 0;
                                        angular.forEach(this.pointArray,(val:any, i:any) =>{
                                            if (val.id === p.id) {
                                                this.$http.delete(`http://geocode.adsun.vn/api/CheckZone/DeleteTollbooth?idTollbooth=${p.id}`).then((rep:any) =>{
                                                    console.log(rep);
                                                });
                                            }
                                        });
                                    }
                                },
                                main: {
                                    label: "<span class='glyphicon glyphicon-remove'></span>"+"&nbspHủy",
                                    className: "btn-danger",
                                    callback: function() {

                                    }
                                }
                            }
                        });
                    }else{
                        bootbox.dialog({
                            title: "Thông Báo",
                            message: "Bạn không đủ quyền để xóa trạm thu phí này !!",
                            buttons: {
                                cancle: {
                                    label: "Hủy",
                                    className: "btn-danger",
                                }
                            }
                        });
                    }
                }
                //cập nhật điểm cho trạm thu phí
                pointEdit(p:any){
                    if(this.acc.Level < 2){
                        var index:any = 0;
                        angular.forEach(this.pointArray,(val:any, i:any)=> {
                            if (val.id === p.id) {
                                var infoPoint:any={Name: p.marker.getTitle(),Radius: p.cricle.getRadius()};
                                this.showFormInfo(p,null).then((data:any)=>{
                                    if(data.posNew === undefined)
                                    {
                                        var res={
                                            Id:p.id,
                                            Lat:p.Lat,
                                            Lng:p.Lng,
                                            Name:data.Name,
                                            Description:data.Description,
                                            Radius:data.Radius
                                        };
                                        // console.log(res);
                                        //this.callBackServerUpdate(res,p.id);
                                        this.$http.post(`http://geocode.adsun.vn/api/CheckZone/UpdateTollbooth`,res).then((rep:any)=>{
                                            console.log(rep);
                                        });

                                    }
                                    else
                                    {

                                        this.getAddress(data.posNew).then((mess:any)=>{
                                            // var location={Lat:data.posNew.lat(),Lng:data.posNew.lng(),Address:mess};
                                            var obj={Lat:data.posNew.lat(),Lng:data.posNew.lng(),Address:mess,Name:data.Name,Radius:data.Radius,Description: data.Description, Id:p.id};
                                            this.$http.post(`http://geocode.adsun.vn/api/CheckZone/UpdateTollbooth`,obj).then((rep:any)=>{
                                                console.log(rep);
                                            });
                                        });
                                    }
                                    p.editPointCircle(data.Name,data.Radius,data.Description);
                                });
                            }
                        });
                    }else{
                        bootbox.dialog({
                            title: "Thông Báo",
                            message: "Bạn không đủ quyền để cập nhật trạm thu phí này !!",
                            buttons: {
                                cancle: {
                                    label: "Hủy",
                                    className: "btn-danger",
                                }
                            }
                        });
                    }
                }
                newPoint(position:any){
                    if(this.acc.Level < 2){
                        /*hiển thị form nhập dữ liệu*/
                        this.showFormInfo(null,position).then((data:any)=>{
                            /*lấy thông tin địa chỉ sau khi người dùng đã nhập xong */
                            this.getAddress(position).then((mess:any)=>{
                                var location={Lat:position.lat(),Lng:position.lng(),Address:mess};
                                var data_req={Lat:position.lat(),Lng:position.lng(),Address:mess ,Name: data.Name, Radius:data.Radius, Description: data.Description};
                                // console.log(data_req);
                                this.$http.post('http://geocode.adsun.vn/api/CheckZone/AddTollboothPoint',data_req ).then((rep:any)=>{
                                    if(rep.data.Status == 1){
                                        var p:any = new App.Shared.Library.Point(this.mainMap,data.Radius,position,rep.data.AddGpsCheckPointId,data.Name,data_req);
                                        this.isShow=true;
                                        this.add(rep.data.AddGpsCheckPointId,p);
                                        this.$http.get(this.global.ReportHost + 'api/Tool/LoadTollbooth').then((rep:any)=>{
                                           console.log('load tram thu phi ok');
                                        });
                                    }else{
                                        bootbox.dialog({
                                            title: "Thông Báo",
                                            message: 'Tạo trạm thu phí mới không thành công',
                                            buttons: {
                                                cancle: {
                                                    label: "Quay lại",
                                                    className: "btn-danger",
                                                }
                                            },
                                        });
                                    }

                                    // /*báo cho người sử dung biết là đã tạo điểm thành công*/
                                    // $this.ul.message.warning('thêm thành công');
                                });
                            });
                        });
                    }

                }
                add(id:any,p:any){
                    // if(this.acc.Level < 2){
                        p.onDelete=this.pointDelete.bind(this);
                        p.onEdit=this.pointEdit.bind(this);
                        this.pointArray.push({id:id,value:p, slXe:0,Name:p.name,Radius: p.radius });
                        activeListenerEvent(this);
                        if(this.isShow)
                            p.show();
                        else
                            p.hide();
                    // }else{
                    //     bootbox.dialog({
                    //         title: "Thông Báo",
                    //         message: "Bạn không đủ quyền để tạo mới trạm thu phí!!",
                    //         buttons: {
                    //             cancle: {
                    //                 label: "Hủy",
                    //                 className: "btn-danger",
                    //             }
                    //         }
                    //     });
                    // }

                }
            }
        }
    }
}