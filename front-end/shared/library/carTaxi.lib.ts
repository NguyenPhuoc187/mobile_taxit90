/**
 * Created by NPPRO on 07/12/2016.
 */
/// <reference path="../../shared/library.ts"/>
/// <reference path="../../../tstyping/Sugar/Sugar.d.ts"/>
// function xemhanhtrinh(companyId:number, serial:any,value:any) {
//
//     App.Shared.Library.GlobalCarTaxiManager.viewHanhtrinh(companyId,serial,value);
// }

module App{
    export module Shared{
        export module Library{
            export interface ICarTaxi {
                //thông tin xe
                SoHieu:string; // số hiệu
                isSunTaxi:boolean; // số hiệu
                Bs:string; // biển số
                Serial:string; // số serial
                Location:{
                    Lat:number,
                    Lng:number,
                    Address:string
                },
                Angle:number; // số góc
                ClientSend:string; // ngày tháng cập nhật
                Sgtvt:string; // sở GTVT
                CompanyId:number; //mã công ty
                CompanyName:string; // tên công ty
                DeviceGroup:string; // tên đội xe
                Model:string; // tên model
                Speed:number; // tốc độ
                TotalSession:number; // số lượng cuốc xe
                Power:number; //điện áp bình
                GSMSignal:number; // tín hiệu GMS
                KeyStatus:boolean; // trạng thái chìa khóa
                AirMachine:boolean; // trạng thái máy lạnh
                HongNgoai:boolean; // trạng thái hồng ngoại
                Phone:string; // số điện thoại
                Money:string; // tài khoản simk


                //thông tin đồng hồ
                TotalKmUse:number; // tổng km đã sử dụng khi có khách
                TongKMDongHo:number; // tổng km theo đồng hồ
                TongKMCoKhachGps:number;// tổng km có khách theo GPS
                TotalSession0Km:number; // tổng km theo cuốc 0Km
                TotalMoney:number; // tổng tiền
                ThoiGianDung:number; // thời gian dừng
                TongKMGps:number // tổng km GPS


                //thông tin cuốc đang chạy
                CurrentMeterKm :number; // km đang chạy
                MoneySession:number; // tiền trong cuốc
                ThoiGianCho :string; // thời gian chờ khách
                ThoiGianNhanKhach: string; // thời gian nhận khách

                //thông tin tài xế
                DriverName: string; // tài xế
                DriverPhoneNumber: string; // điện thoại tài xế
                Gplx: string;/*Giấy phép lái xe*/
                SoGhe: number;/*Giấy phép lái xe*/
                TgLaiXe: number; // thời gian lái xe
                TongThoiGian: number; // tổng thời gian
                OverTimeLCount:number; // số lần quá thời gian liên tục
                timeUpdateconvert:string;

                //thông tin trạng thái
                IsBusy:boolean; // trạng thái bận
                LostGsm:boolean; // mất GSM
                Gps:boolean;// trạng thái GPS
                MeterOk:boolean; // trạng thái đồng hồ
                MeterStatus:boolean; // trạng thái đồng hồ có khách hay không
                StopOverTime:boolean; // trạng thái xe dừng lâu
                OverSpeed:boolean; // quá tốc độ
                ChichXung:number; //  chích xung
                ThoiGianChotCa: string; // thời gian chốt ca
                // IsSos:boolean; // tín hiệu sos
                XeTrongVung:boolean;
                Layer?:any;
                // Sự kiện ấn chuột vào xe
                IsTargetCar?:boolean;


            }
            export interface IMaintenanceTaxi {
                KmDaoLopCurrent: number;
                KmDaoLopLimit: number;
                KmThayVoCurrent:number;
                KmThayVoLimit: number;
                KmThayNhotCurrent: number;
                KmThayNhotLimit: number;
                KmThayLocDauCurrent: number;
                KmThayLocDauLimit: number;
                KmThayLocGioCurrent:number;
                KmThayLocGioLimit: number;
                KmThayLocNhotCurrent: number;
                KmThayLocNhotLimit: number;

            }


            export class CarTaxiView extends BaseCar{
                public CallbackOnClick:()=>void;
                public MenuRight:IMaintenance;
                private info:any;
                public SoHieu:string; // số hiệu
                public isSunTaxi:boolean; // số hiệu
                public Bs:string; // biển số
                public Serial:string; // số serial
                public Location:{
                    Lat:number,
                    Lng:number,
                    Address:string
                };
                public Angle:number; // số góc
                public ClientSend:string; // ngày tháng cập nhật
                public Sgtvt:string; // sở GTVT
                public CompanyId:number; //mã công ty
                public CompanyName:string; // tên công ty
                public DeviceGroup:string; // tên đội xe
                public Model:string; // tên model
                public Speed:number; // tốc độ
                public TotalSession:number; // số lượng cuốc xe
                public Power:number; //điện áp bình
                public GSMSignal:number; // tín hiệu GMS
                public KeyStatus:boolean; // trạng thái chìa khóa
                public AirMachine:boolean; // trạng thái máy lạnh
                public HongNgoai:boolean; // trạng thái hồng ngoại
                public Phone:string; // số điện thoại thiết bị
                public Money:string; //tài khoản sim


                //thông tin đồng hồ
                public TotalKmUse:number; // tổng km đã sử dụng khi có khách
                public TongKMDongHo:number; // tổng km theo đồng hồ
                public TongKMCoKhachGps:number;// tổng km theo GPS có khách
                public TotalSession0Km:number; // tổng km theo cuốc 0Km
                public TotalMoney:number; // tổng tiền
                public ThoiGianDung:number; // thời gian dừng
                public TongKMGps:number; // tổng km theo gps

                //thông tin cuốc đang chạy
                public CurrentMeterKm :number; // km đang chạy
                public MoneySession:number; // tiền trong cuốc
                public ThoiGianCho :string; // thời gian chờ khách
                public ThoiGianNhanKhach: string; // thời gian nhận khách

                //thông tin tài xế
                public DriverName: string; // tài xế
                public DriverPhoneNumber: string; // điện thoại tài xế
                public Gplx: string;/*Giấy phép lái xe*/
                public SoGhe: number;/*Giấy phép lái xe*/
                public TgLaiXe: number; // thời gian lái xe
                public TongThoiGian: number; // tổng thời gian
                public OverTimeLCount:number; // số lần quá thời gian liên tục
                public timeUpdateconvert:string;

                //thông tin trạng thái
                public IsBusy:boolean; // trạng thái bận
                public LostGsm:boolean; // mất GSM
                public Gps:boolean;// trạng thái GPS
                public MeterOk:boolean; // trạng thái đồng hồ
                public MeterStatus:boolean;//trạng thái có khách
                public StopOverTime:boolean; // trạng thái xe dừng lâu
                public OverSpeed:boolean; // quá tốc độ
                public ChichXung:number; //  chích xung
                public ThoiGianChotCa: string; // thời gian chốt ca
                public XeTrongVung:boolean; // check xe trong vùng
                // public IsSos:boolean; // check xe trong vùng

                //biến tạo thêm
                public address:any; // địa chỉ
                public isRightClick:boolean;
                public ForceHide:boolean;
                public check_right:boolean;
                public isLatlngModify:boolean;
                public Layer?:any;
                // Sự kiện ấn chuột vào xe
                public IsTargetCar:boolean = false;



                public gifframeindex:number = 0;
                public infowindow:any;
                public acccauhinh:any;
                hidenInfo:boolean;

                constructor(config:CarConfig){
                    super(config);
                    this.hidenInfo = true;
                    this.info = new window.InfoBox({
                        boxStyle: {opacity: 1},
                        disableAutoPan: false,
                        maxWidth: 0,
                        // pixelOffset: new google.maps.Size(20, -200),
                        pixelOffset: new google.maps.Size(-140, 0),
                        zIndex: null,
                        // closeBoxMargin: "19px 17px 2px 2px",
                        closeBoxLeft: "210px",
                        closeBoxTop: "-2px",
                        closeBoxWidth: "25px",
                        closeBoxHeight: "25px",
                        closeBoxURL: "../../assets/images/Close_Window_48.png",
                        infoBoxClearance: new google.maps.Size(1, 1),
                        isHidden: false,
                        pane: "floatPane",
                        enableEventPropagation: false,

                    });
                    this.isRightClick = false;
                    this.ForceHide = false;
                    this.onClick = ()=> {
                        this.isRightClick = false;
                        if (this.CallbackOnClick != null && this.CallbackOnClick != undefined)
                            this.CallbackOnClick();
                        this.viewInfo();
                        this.cfg.map.setCenter(this.latlng);

                    };
                    this.onRightClick = () => {
                        // console.log('car-right');
                        this.cfg.map.FisrtRightClick = true;
                        this.isRightClick = true;
                        this.check_right = true;
                        this.viewRightInfo();
                        //alert('hello');
                    };
                    this.MenuRight = {
                        KmDaoLopCurrent: 0,
                        KmDaoLopLimit: 0,
                        KmThayVoCurrent: 0,
                        KmThayVoLimit: 0,
                        KmThayNhotCurrent: 0,
                        KmThayNhotLimit: 0,
                        KmThayLocDauCurrent: 0,
                        KmThayLocDauLimit: 0,
                        KmThayLocGioCurrent: 0,
                        KmThayLocGioLimit: 0,
                        KmThayLocNhotCurrent: 0,
                        KmThayLocNhotLimit: 0,
                    };
                    this.infowindow =  new google.maps.InfoWindow();
                    this.acccauhinh = this.cfg.global.Account.GetValue();

                }
                public moveInfoClickHeader(){
                    let check_click = true; // flat kết thúc sự kiện move

                    this.cfg.map.addListener('mousemove',(e:any) =>{

                        // console.log(e);
                        if(check_click){
                            this.info.setPosition(e.latLng);
                        }
                        // this.div_.setPoseition()
                    });
                    this.cfg.map.addListener('click', (e:any)=>{
                        this.info.setPosition(e.latLng);
                        check_click =false;

                    });
                }

                private viewInfo() {
                    this.IsTargetCar = true;
                    this.updateInfo(true);
                    // console.log(this.latlng);
                    if(this.info.getPosition() !== this.latlng){
                        this.info.setPosition(this.info.getPosition());
                    }
                    this.info.setPosition(this.latlng);
                    this.info.setMap(this.cfg.map);

                }

                private viewRightInfo() {

                    this.buildInfoDeviceRight().then((html_right)=> {
                        this.info.setContent(html_right);
                        this.moveinfo();
                    });
                    this.info.setPosition(this.latlng);
                    this.info.setMap(this.cfg.map);
                }

                public load(obj:ICarTaxi) {
                    var oldLatLng = this.latlng;
                    this.latlng = new google.maps.LatLng(obj.Location.Lat, obj.Location.Lng);
                    this.Serial = obj.Serial;
                    this.serial = obj.Serial;
                    this.Bs = obj.Bs;
                    this.Location = obj.Location;
                    if (oldLatLng == null) {
                        this.iconInfo.angle = obj.Angle;
                        if (this.address != undefined)
                            this.isLatlngModify = true;
                    }
                    else {
                        this.isLatlngModify = (oldLatLng.lat() != this.latlng.lat() && oldLatLng.lng() != this.latlng.lng());
                        if (this.isLatlngModify)
                            this.iconInfo.angle = google.maps.geometry.spherical.computeHeading(oldLatLng, this.latlng);
                        if (this.address == undefined)
                            this.isLatlngModify = true;
                    }

                    // thông tin xe
                    this.SoHieu = obj.SoHieu;
                    this.isSunTaxi = obj.isSunTaxi;
                    this.deviceLabel = obj.SoHieu;
                    this.ClientSend = obj.ClientSend;
                    this.Sgtvt = obj.Sgtvt;
                    this.Model = obj.Model;
                    this.Speed = obj.Speed;
                    this.Power = obj.Power;
                    this.KeyStatus = obj.KeyStatus;
                    this.AirMachine = obj.AirMachine;
                    this.HongNgoai = obj.HongNgoai;
                    this.Phone = obj.Phone;
                    this.Money = obj.Money;
                    this.DeviceGroup = obj.DeviceGroup;
                    this.TotalSession = obj.TotalSession;
                    this.Gps = obj.Gps;
                    this.LostGsm = obj.LostGsm;
                    this.GSMSignal = obj.GSMSignal;
                    this.CompanyId = obj.CompanyId;
                    this.timeUpdateconvert = obj.ClientSend.slice(11);

                    //thông tin đồng hồ
                    this.TotalKmUse = obj.TotalKmUse;
                    this.TongKMDongHo = obj.TongKMDongHo;
                    this.TongKMGps = obj.TongKMGps;
                    this.TongKMCoKhachGps = obj.TongKMCoKhachGps;
                    this.TotalSession0Km = obj.TotalSession0Km;
                    this.TotalMoney = obj.TotalMoney;
                    this.ThoiGianDung = obj.ThoiGianDung;
                    this.MeterOk = obj.MeterOk;
                    this.MeterStatus = obj.MeterStatus;
                    this.IsBusy = obj.IsBusy;
                    this.StopOverTime = obj.StopOverTime; // trạng thái xe dừng lâu
                    this.OverSpeed = obj.OverSpeed; // quá tốc độ
                    this.ChichXung = obj.ChichXung; //  chích xung
                    this.ThoiGianChotCa = obj.ThoiGianChotCa;// thời gian chốt ca
                    this.XeTrongVung = obj.XeTrongVung;
                    // this.IsSos = obj.IsSos;

                    // thông tin cuốc xe chạy
                    this.CurrentMeterKm = obj.CurrentMeterKm;
                    this.MoneySession = obj.MoneySession;
                    this.ThoiGianCho = obj.ThoiGianCho;
                    this.ThoiGianNhanKhach = obj.ThoiGianNhanKhach;

                    //thông tin tài xế
                    this.DriverName = obj.DriverName;
                    this.DriverPhoneNumber = obj.DriverPhoneNumber;
                    this.Gplx = obj.Gplx;
                    this.SoGhe = obj.SoGhe;
                    this.TgLaiXe = obj.TgLaiXe;
                    this.TongThoiGian = obj.TongThoiGian;
                    this.OverTimeLCount = obj.OverTimeLCount;


                    if(this.isSunTaxi) {
                        this.iconInfo.icon.src = this.getIconTaxiSun();
                    } else {
                        this.iconInfo.icon.src = this.getIconTaxi();
                    }

                    /*cập nhật luôn thông tin nếu người sử dụng đã click cho hiển thị bảng thông tin*/
                    this.updateInfo(false);
                    this.updateInfowindow(false);
                }

                public focus() {
                    if (this.CallbackOnClick != null && this.CallbackOnClick != undefined){
                        this.CallbackOnClick();

                    }

                    this.cfg.map.setCenter(this.latlng);// chuyển trung tâm bản đồ tới vị trí thiết bị
                    this.cfg.map.setZoom(15);
                    this.viewInfo();//hiển thị bảng thông tin
                    // console.log('chay');
                }

                public viewinfomoremap(map:any){
                    this.IsTargetCar = true;
                    this.updateInfowindow(true);
                    // var content =  this.buildInfoDeviceMoreMap();

                    if(this.infowindow.getPosition() !== this.latlng){
                        this.infowindow.setPosition(this.infowindow.getPosition());
                    }
                    this.infowindow.setPosition(this.latlng);
                    this.infowindow.open(map);
                    // console.log(this.info.getPosition());
                }

                public focusmoremap(map:any){
                    if (this.CallbackOnClick != null && this.CallbackOnClick != undefined){
                        this.CallbackOnClick();
                    }

                    map.setCenter(this.latlng);// chuyển trung tâm bản đồ tới vị trí thiết bị
                    map.setZoom(15);
                    this.viewinfomoremap(map);//hiển thị bảng thông tin
                }

                public onClickCarMoreMap(map:any){
                    this.viewinfomoremap(map);
                }

                private updateInfo(state:boolean) {
                    if (this.info.getMap() != null || state) {
                        // console.log('cap nhat bang trang thai');
                        if (this.isRightClick) {
                            // this.buildInfoDeviceRight().then((html_right)=>{
                            //
                            //     this.info.setContent(html_right);
                            //     this.moveinfo();
                            // });
                            // this.viewRightInfo();
                        } else {
                            this.buildInfoDevice().then((html)=> {
                                this.info.setContent(html);

                                //"<div><input type='button' onclick='$(\".rInfo\").removeClass(\"rInfo\"); console.log($(\".rInfo\"));' value='...' id='more-button'></div>" +
                                var btShowInfoElement = document.createElement('a');
                                //btShowInfoElement.type='button';
                                btShowInfoElement.className = 'more-button';
                                if (this.hidenInfo)
                                    btShowInfoElement.innerHTML = '<i class="fa fa-angle-double-down fa-2x" aria-hidden="true"></i>';
                                else
                                    btShowInfoElement.innerHTML = '<i class="fa fa-angle-double-up fa-2x" aria-hidden="true"></i>';
                                btShowInfoElement.onclick = ()=> {
                                    if (this.hidenInfo == true) {
                                        $('.rInfo-hiden').removeClass('rInfo-hiden').addClass('rInfo-show');
                                        this.hidenInfo = false;
                                        $(btShowInfoElement).html('<i class="fa fa-angle-double-up fa-2x" aria-hidden="true"></i>');
                                    }
                                    else {

                                        $('.rInfo-show').removeClass('rInfo-show').addClass('rInfo-hiden');
                                        $(btShowInfoElement).html('<i class="fa fa-angle-double-down fa-2x" aria-hidden="true"></i>');
                                        this.hidenInfo = true;
                                    }

                                };
                                //this.info.div_.childNodes[1].appendChild(btShowInfoElement);
                                if($('.divTable').length!=0)
                                    $('.divTable').append(btShowInfoElement);
                                else{
                                    var t=setInterval(()=>{
                                        if($('.divTable').length!=0){
                                            $('.divTable').append(btShowInfoElement);
                                            clearInterval(t);
                                        }
                                    },50);
                                }
                                this.moveinfo();
                            });
                        }

                    }
                }

                private updateInfowindow(state:boolean) {
                    if (this.infowindow.getMap() != null || state) {
                        this.buildInfoDeviceMoreMap().then((html)=> {
                            this.infowindow.setContent(html);
                            this.moveinfowindow();
                        });

                    }
                }

                private moveinfowindow(){
                    if (this.infowindow != null && this.infowindow.getMap() != null) {

                        this.infowindow.setPosition(this.latlng);
                    }
                }
                public removeinfowindow(){
                    if (this.infowindow != null && this.infowindow.getMap() != null) {

                        this.infowindow.close();
                    }
                }



                public removeinfo() {
                    this.IsTargetCar = false;
                    this.info.setMap(null);
                }

                private AdapterDate(time:any) {
                    var year:string = time.slice(0, 4);
                    var month:string = time.slice(5, 7);
                    var date:string = time.slice(8, 10);

                    var min:string = time.slice(11);

                    var tt:any = date + '-' + month + '-' + year + ' ' + min;
                    return tt;
                }
                private AdapterDateNoTime(time:any) {
                    var year:string = time.slice(0, 4);
                    var month:string = time.slice(5, 7);
                    var date:string = time.slice(8, 10);

                    var min:string = time.slice(11);

                    var tmp:any = date + '-' + month + '-' + year ;
                    return tmp;
                }

                // hàm dịch chuyển thông báo theo xe
                private moveinfo() {
                    // kiểm tra xem có nội dung chưa và có hiển thị trên map hay không
                    if (this.info != null && this.info.getMap() != null) {

                        this.info.setPosition(this.latlng);
                    }
                }


                private buildRow(cells:Array<any>,show:boolean=false) {

                    var html = '';
                    if(!show)
                        html += `<tr class="${(this.hidenInfo ? 'rInfo-hiden' : 'rInfo-show')} ">`;
                    else
                        html+=`<tr>`;
                    cells.forEach((m, i)=> {
                        html += this.buildCell(m.title, m.value, m.colspan,m.titleWidth);
                    });
                    return html + '</tr>';

                    // var html = '';
                    // html += '<tr>';
                    // cells.forEach((m, i)=> {
                    //     html += this.buildCell(m.title, m.value, m.colspan,m.titleWidth);
                    // });
                    // return html + '</tr>'
                }

                private buildCell(title:string, value:any, colspan:number,titleWidth:number):string {
                    return `<td style="padding-left:5px; width: 145px; " colspan="${colspan}"><table>
                    <tr><th style="${(titleWidth!=null&&titleWidth!=undefined)? 'width:'+titleWidth+'px':''}">${title} </th><td>${value}</td></tr>
                    </table>
                    </td>`;
                }



                private buildInfoDevice():Q.IPromise<string> {
                    var defer = this.cfg.q.defer<string>();
                    var hienthimucsong:string = '';
                    if (this.GSMSignal > 0  && this.GSMSignal <= 15) {
                        hienthimucsong = 'Yếu'; //mức sóng từ < 15
                    } else if (this.GSMSignal > 15 && this.GSMSignal <= 20) { // mức sóng từ 15 ~ 20
                        hienthimucsong = 'Tốt';
                    } else if(this.GSMSignal > 20){
                        hienthimucsong = 'Rất tốt'; //mức sóng từ 20~30
                    }
                    let switchon = '<div class="onoffswitch"><label class="onoffswitch-label" > <span class="onoffswitch-inner-on"></span> <span class="onoffswitch-switch" style="right: 5px!important;"></span> </label> </div>';
                    let switchoff = '<div class="onoffswitch"> <label class="onoffswitch-label" > <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label> </div>';

                    var tmp = `<table class='divTable' style='width: 230px; min-height: 150px; max-height: 320px; font-size: 11px'>`;

                    // tmp += this.buildRow([{
                    //     title: '<span class="">Xe:</span>',
                    //     value: `<span class="" style="width: 150px">${this.SoHieu}/${this.Bs}</span>`,
                    //     colspan: 2,
                    //     titleWidth:50
                    // }]);
                    tmp += this.buildRow([{
                        title: '<span class="text-danger">THÔNG TIN XE</span>',
                        value: '',
                        colspan: 2,
                        titleWidth:120
                    }]);

                    //xét trường hợp có check hiển thị biển số hay không
                    if(this.acccauhinh.CauHinhDoi !== null && this.acccauhinh.CauHinhDoi !== ""){
                        var data = JSON.parse(this.acccauhinh.CauHinhDoi);
                        // console.log(data);
                        if(data.bienso !== undefined){
                            if(data.bienso){
                                tmp += this.buildRow([{
                                    title: 'Số tài:',
                                    value: `<span class="text-danger">${this.SoHieu}</span>`,
                                    colspan: 1,
                                    titleWidth:60
                                },
                                    // {
                                    //     title: 'Biển số:',
                                    //     value: `<span class="text-danger">${this.Bs}</span>`,
                                    //     colspan: 1,
                                    //     titleWidth:60
                                    // },
                                ],true);

                                tmp += this.buildRow([{
                                    title: 'Biển số:',
                                    value: `<span class="text-danger">${this.Bs}</span>`,
                                    colspan: 1,
                                    titleWidth:60
                                }],true);
                            }else{
                                tmp += this.buildRow([{
                                    title: 'Số tài:',
                                    value: `<span class="text-danger">${this.SoHieu}</span>`,
                                    colspan: 1,
                                    titleWidth:60
                                }],true);
                            }
                        }else{
                            tmp += this.buildRow([{
                                title: 'Số tài:',
                                value: `<span class="text-danger">${this.SoHieu}</span>`,
                                colspan: 1,
                                titleWidth:60
                            },
                                // {
                                //     title: 'Biển số:',
                                //     value: `<span class="text-danger">${this.Bs}</span>`,
                                //     colspan: 1,
                                //     titleWidth:60
                                // },
                            ],true);
                            tmp += this.buildRow([{
                                title: 'Biển số:',
                                value: `<span class="text-danger">${this.Bs}</span>`,
                                colspan: 1,
                                titleWidth:60
                            }],true);

                        }
                    }else{
                        tmp += this.buildRow([{
                            title: 'Số tài:',
                            value: `<span class="text-danger">${this.SoHieu}</span>`,
                            colspan: 1,
                            titleWidth:60
                        },
                            // {
                            //     title: 'Biển số:',
                            //     value: `<span class="text-danger">${this.Bs}</span>`,
                            //     colspan: 1,
                            //     titleWidth:60
                            // },
                        ],true);
                        tmp += this.buildRow([{
                            title: 'Biển số:',
                            value: `<span class="text-danger">${this.Bs}</span>`,
                            colspan: 1,
                            titleWidth:60
                        }],true);
                    }


                    // tmp += this.buildRow([
                    //     {
                    //         title: 'Đội xe:',
                    //         value: (this.DeviceGroup == null)? ' ' : this.DeviceGroup,
                    //         colspan: 1,
                    //         titleWidth:60
                    //     },
                    //     {
                    //         title: 'Loại xe:',
                    //         value: `<span>${this.Model}</span>`,
                    //         colspan: 1,
                    //         titleWidth:60
                    //     }
                    // ]);

                    // tmp += this.buildRow([
                    //     {
                    //         title: 'Sở GTVT:',
                    //         value: (this.Sgtvt == null)? ' ' : this.Sgtvt,
                    //         colspan: 1,
                    //         titleWidth:60
                    //     },
                    //
                    //
                    // ]);
                    // tmp += this.buildRow([{
                    //     title: 'Điện áp:',
                    //     value: `${this.AdapterDienAp(this.Power)} V`,
                    //     colspan: 1,
                    //     titleWidth:60
                    // },
                    //     {
                    //         title: 'GSM:',
                    //         value: hienthimucsong,
                    //         colspan: 1,
                    //         titleWidth:60
                    //     }
                    // ]);
                    tmp += this.buildRow([
                        {
                            title: 'Vận tốc:',
                            value: `<span class="text-danger">${this.Speed} Km/h</span>`,
                            colspan: 1,
                            titleWidth:60
                        },
                        {
                            title: 'GPS:',
                            value: `<span>${this.Gps? 'Có':'Không'}</span>`,
                            colspan: 1,
                            titleWidth:60
                        },

                    ],true);
                    tmp += this.buildRow([{
                        title: 'Chìa khóa:',
                        value: this.KeyStatus?switchon:switchoff,
                        colspan: 1,
                        titleWidth:60
                    },
                        {
                            title: 'Máy lạnh:',
                            value: this.KeyStatus?(this.AirMachine?switchon:switchoff):switchoff,
                            colspan: 1,
                            titleWidth:60
                        }
                    ],true);

                    //a tí yêu cầu ko hiển thị 2 thông tin đồng hồ và thông tin cuốc ngày 16/09/2017
                    // console.log(this.cfg.global.Account.GetValue());

                    if(this.acccauhinh.CauHinhDoi !== null && this.acccauhinh.CauHinhDoi !== ""){
                        var data = JSON.parse(this.acccauhinh.CauHinhDoi);
                        if(data.trangthai !== undefined){
                            if(data.trangthai){
                                tmp += this.buildRow([{
                                    title: '<span class="text-danger">THÔNG TIN ĐỒNG HỒ</span>',
                                    value: '',
                                    colspan: 2,
                                    titleWidth:150
                                }]);


                                tmp += this.buildRow([{
                                    title: 'Km Rỗng:',
                                    value: `${this.AdapterMeterToKm(this.TotalSession0Km)} Km`,
                                    colspan: 1,
                                    titleWidth:60
                                },


                                ]);
                                tmp += this.buildRow([{
                                    title: 'Hồng ngoại:',
                                    value: this.HongNgoai? 'Có Khách':'Không Khách',
                                    colspan: 2,
                                    titleWidth:120
                                }]);

                                tmp += this.buildRow([{
                                    title: 'Km có khách (DH):',
                                    value: `${this.AdapterMeterToKm(this.TotalKmUse)} Km`,
                                    colspan: 2,
                                    titleWidth:120
                                }]);
                                tmp += this.buildRow([{
                                    title: 'Km có khách (GPS):',
                                    value: `${this.AdapterMeterToKm(this.TongKMCoKhachGps)} Km`,
                                    colspan: 2,
                                    titleWidth:120
                                }]);

                                // tmp += this.buildRow([
                                //     {
                                //         title: 'Tổng cuốc:',
                                //         value: `${this.AdapterMoney(this.TotalSession)} cuốc`,
                                //         colspan: 1,
                                //         titleWidth:70
                                //     }
                                // ]);

                                // tmp += this.buildRow([{
                                //     title: 'Tổng tiền:',
                                //     value: `${this.AdapterMoney(this.TotalMoney)} VNĐ`,
                                //     colspan: 2,
                                //     titleWidth:70
                                // },
                                // ]);
                                tmp += this.buildRow([
                                    {
                                        title: 'Tổng cuốc:',
                                        value: `${this.AdapterMoney(this.TotalSession)} cuốc`,
                                        colspan: 2,
                                        titleWidth:160
                                    }
                                ]);



                                tmp += this.buildRow([{
                                    title: 'Tổng tiền:',
                                    value: `${this.AdapterMoney(this.TotalMoney)} VNĐ`,
                                    colspan: 2,
                                    titleWidth:160
                                },
                                ]);

                                // tmp += this.buildRow([]);
                                // tmp += this.buildRow([{
                                //     title: 'Hồng ngoại:',
                                //     value: this.HongNgoai? 'Có Khách':'Không Khách',
                                //     colspan: 2,
                                //     titleWidth:70
                                // },
                                // ]);
                                if(this.MeterStatus){
                                    tmp += this.buildRow([{
                                        title: '<span class="text-danger">THÔNG TIN CUỐC</span>',
                                        value: '',
                                        colspan: 2,
                                        titleWidth:150
                                    }]);
                                    tmp += this.buildRow([{
                                        title: 'Km cuốc đang chạy:',
                                        value: `${this.AdapterMeterToKm(this.CurrentMeterKm)} Km`,
                                        colspan: 2,
                                        titleWidth:160
                                    }]);
                                    tmp += this.buildRow([{
                                        title: 'Tiền đang chạy:',
                                        value: `${this.AdapterMoney(this.MoneySession)} VNĐ`,
                                        colspan: 2,
                                        titleWidth:160
                                    }]);
                                    tmp += this.buildRow([{
                                        title: 'Thời gian chờ:',
                                        value: this.ThoiGianCho,
                                        colspan: 2,
                                        titleWidth:160
                                    }]);
                                    tmp += this.buildRow([{
                                        title: 'Thời gian đón khách:',
                                        value: this.ThoiGianNhanKhach,
                                        colspan: 2,
                                        titleWidth:160
                                    }]);
                                }
                            }
                        }else{
                            tmp += this.buildRow([{
                                title: '<span class="text-danger">THÔNG TIN ĐỒNG HỒ</span>',
                                value: '',
                                colspan: 2,
                                titleWidth:150
                            }]);


                            tmp += this.buildRow([{
                                title: 'Km Rỗng:',
                                value: `${this.AdapterMeterToKm(this.TotalSession0Km)} Km`,
                                colspan: 1,
                                titleWidth:60
                            }
                            ]);
                            tmp += this.buildRow([{
                                title: 'Hồng ngoại:',
                                value: this.HongNgoai? 'Có Khách':'Không Khách',
                                colspan: 2,
                                titleWidth:120
                            }]);
                            tmp += this.buildRow([{
                                title: 'Km có khách (DH):',
                                value: `${this.AdapterMeterToKm(this.TotalKmUse)} Km`,
                                colspan: 2,
                                titleWidth:160
                            }]);
                            tmp += this.buildRow([{
                                title: 'Km có khách (GPS):',
                                value: `${this.AdapterMeterToKm(this.TongKMCoKhachGps)} Km`,
                                colspan: 2,
                                titleWidth:160
                            }]);

                            // tmp += this.buildRow([
                            //     {
                            //         title: 'Tổng cuốc:',
                            //         value: `${this.AdapterMoney(this.TotalSession)} cuốc`,
                            //         colspan: 1,
                            //         titleWidth:70
                            //     }
                            // ]);

                            // tmp += this.buildRow([{
                            //     title: 'Tổng tiền:',
                            //     value: `${this.AdapterMoney(this.TotalMoney)} VNĐ`,
                            //     colspan: 2,
                            //     titleWidth:70
                            // },
                            // ]);
                            tmp += this.buildRow([
                                {
                                    title: 'Tổng cuốc:',
                                    value: `${this.AdapterMoney(this.TotalSession)} cuốc`,
                                    colspan: 2,
                                    titleWidth:160
                                }
                            ]);



                            tmp += this.buildRow([{
                                title: 'Tổng tiền:',
                                value: `${this.AdapterMoney(this.TotalMoney)} VNĐ`,
                                colspan: 2,
                                titleWidth:160
                            },
                            ]);

                            // tmp += this.buildRow([]);
                            // tmp += this.buildRow([{
                            //     title: 'Hồng ngoại:',
                            //     value: this.HongNgoai? 'Có Khách':'Không Khách',
                            //     colspan: 2,
                            //     titleWidth:70
                            // },
                            // ]);
                            if(this.MeterStatus){
                                tmp += this.buildRow([{
                                    title: '<span class="text-danger">THÔNG TIN CUỐC</span>',
                                    value: '',
                                    colspan: 2,
                                    titleWidth:150
                                }]);
                                tmp += this.buildRow([{
                                    title: 'Km cuốc đang chạy:',
                                    value: `${this.AdapterMeterToKm(this.CurrentMeterKm)} Km`,
                                    colspan: 2,
                                    titleWidth:160
                                }]);
                                tmp += this.buildRow([{
                                    title: 'Tiền đang chạy:',
                                    value: `${this.AdapterMoney(this.MoneySession)} VNĐ`,
                                    colspan: 2,
                                    titleWidth:160
                                }]);
                                tmp += this.buildRow([{
                                    title: 'Thời gian chờ:',
                                    value: this.ThoiGianCho,
                                    colspan: 2,
                                    titleWidth:160
                                }]);
                                tmp += this.buildRow([{
                                    title: 'Thời gian đón khách:',
                                    value: this.ThoiGianNhanKhach,
                                    colspan: 2,
                                    titleWidth:160
                                }]);
                            }
                        }


                    }else{
                        tmp += this.buildRow([{
                            title: '<span class="text-danger">THÔNG TIN ĐỒNG HỒ</span>',
                            value: '',
                            colspan: 2,
                            titleWidth:150
                        }]);


                        tmp += this.buildRow([{
                            title: 'Km Rỗng:',
                            value: `${this.AdapterMeterToKm(this.TotalSession0Km)} Km`,
                            colspan: 1,
                            titleWidth:60
                        },


                        ]);
                        tmp += this.buildRow([{
                            title: 'Hồng ngoại:',
                            value: this.HongNgoai? 'Có Khách':'Không Khách',
                            colspan: 2,
                            titleWidth:120
                        }]);
                        tmp += this.buildRow([{
                            title: 'Km có khách (DH):',
                            value: `${this.AdapterMeterToKm(this.TotalKmUse)} Km`,
                            colspan: 2,
                            titleWidth:120
                        }]);
                        tmp += this.buildRow([{
                            title: 'Km có khách (GPS):',
                            value: `${this.AdapterMeterToKm(this.TongKMCoKhachGps)} Km`,
                            colspan: 2,
                            titleWidth:120
                        }]);

                        // tmp += this.buildRow([
                        //     {
                        //         title: 'Tổng cuốc:',
                        //         value: `${this.AdapterMoney(this.TotalSession)} cuốc`,
                        //         colspan: 1,
                        //         titleWidth:70
                        //     }
                        // ]);

                        // tmp += this.buildRow([{
                        //     title: 'Tổng tiền:',
                        //     value: `${this.AdapterMoney(this.TotalMoney)} VNĐ`,
                        //     colspan: 2,
                        //     titleWidth:70
                        // },
                        // ]);
                        tmp += this.buildRow([
                            {
                                title: 'Tổng cuốc:',
                                value: `${this.AdapterMoney(this.TotalSession)} cuốc`,
                                colspan: 2,
                                titleWidth:120
                            }
                        ]);



                        tmp += this.buildRow([{
                            title: 'Tổng tiền:',
                            value: `${this.AdapterMoney(this.TotalMoney)} VNĐ`,
                            colspan: 2,
                            titleWidth:120
                        },
                        ]);

                        // tmp += this.buildRow([]);
                        // tmp += this.buildRow([{
                        //     title: 'Hồng ngoại:',
                        //     value: this.HongNgoai? 'Có Khách':'Không Khách',
                        //     colspan: 2,
                        //     titleWidth:70
                        // },
                        // ]);
                        if(this.MeterStatus){
                            tmp += this.buildRow([{
                                title: '<span class="text-danger">THÔNG TIN CUỐC</span>',
                                value: '',
                                colspan: 2,
                                titleWidth:150
                            }]);
                            tmp += this.buildRow([{
                                title: 'Km cuốc đang chạy:',
                                value: `${this.AdapterMeterToKm(this.CurrentMeterKm)} Km`,
                                colspan: 2,
                                titleWidth:120
                            }]);
                            tmp += this.buildRow([{
                                title: 'Tiền đang chạy:',
                                value: `${this.AdapterMoney(this.MoneySession)} VNĐ`,
                                colspan: 2,
                                titleWidth:120
                            }]);
                            tmp += this.buildRow([{
                                title: 'TG chờ:',
                                value: this.ThoiGianCho,
                                colspan: 2,
                                titleWidth:120
                            }]);
                            tmp += this.buildRow([{
                                title: 'TG đón khách:',
                                value: this.ThoiGianNhanKhach,
                                colspan: 2,
                                titleWidth:120
                            }]);
                        }
                    }


                    tmp += this.buildRow([{
                        title: '<span class="text-danger">THÔNG TIN TÀI XẾ</span>',
                        value: '',
                        colspan: 2,
                        titleWidth:120
                    }]);

                    tmp += this.buildRow([{
                        title: 'Tài xế:',
                        value: this.DriverName,
                        colspan: 2,
                        titleWidth:120
                    }, ]);
                    tmp += this.buildRow([{
                        title: 'GPLX:',
                        value: this.Gplx,
                        colspan: 2,
                        titleWidth:120
                    }]);
                    tmp += this.buildRow([{
                        title: 'SĐT:',
                        value: (this.DriverPhoneNumber == null)?'':this.DriverPhoneNumber,
                        colspan: 2,
                        titleWidth:120
                    }]);

                    tmp += this.buildRow([{
                        title: 'TG liên tục:',
                        value: this.AdapterTime(this.TgLaiXe),
                        colspan: 2,
                        titleWidth:120
                    }]);
                    tmp += this.buildRow([{
                        title: 'TG trong ngày:',
                        value: this.AdapterTime(this.TongThoiGian),
                        colspan: 2,
                        titleWidth:120
                    }]);
                    if(this.Speed == 0){
                        tmp += this.buildRow([{
                            title: 'TG dừng:',
                            value: this.ThoiGianDung,
                            colspan: 2,
                            titleWidth:120
                        }]);
                    }

                    // tmp += this.buildRow([{
                    //     title: '<span class="text-danger">Vị trí</span>',
                    //     value: '',
                    //     colspan: 2,
                    //     titleWidth:120
                    // }]);
                    this.cfg.$http.get(`http://geocode.adsun.vn/Geocode/GetAddress?lat=${this.latlng.lat()}&lng=${this.latlng.lng()}`).then((data:any)=> {

                        this.address = data.data.Address;

                        // bùa vị trí
                        // if (this.latlng.lat() <= 10.8280213 &&this.latlng.lat() >= 10.8271361 &&this.latlng.lng()>= 106.711809 && this.latlng.lng() <= 106.7137885)
                        //     this.address="79 quốc lộ 13, Hiệp Bình Chánh, Thủ Đức, Hồ Chí Minh, Vietnam";

                        tmp += this.buildRow([{
                            title: 'Vị trí:',
                            value: this.address !== undefined ? this.address: '',
                            colspan: 2,
                            titleWidth:70
                        }],true);

                        tmp += this.buildRow([{
                            title: 'Cập nhật:',
                            value: this.AdapterDate(this.ClientSend),
                            colspan: 2,
                            titleWidth:70
                        }],true);
                        // tmp += this.buildRow([{
                        //     title: 'TG hết phí sử dụng TB:',
                        //     value: `<span class="text-danger">${this.AdapterDateNoTime(this.EndTime)}</span>`,
                        //     colspan: 2,
                        //     titleWidth:150
                        // }]);
                        tmp += '</table>';
                        defer.resolve(tmp);

                    });


                    // if (this.isLatlngModify) {
                    //     this.cfg.$http.get(`http://geocode.adsun.vn/Geocode/GetAddress?lat=${this.latlng.lat()}&lng=${this.latlng.lng()}`).then((data:any)=> {
                    //         // console.log();
                    //         this.address = data.data.Address;
                    //
                    //         // bùa vị trí
                    //         // if (this.latlng.lat() <= 10.8280213 &&this.latlng.lat() >= 10.8271361 &&this.latlng.lng()>= 106.711809 && this.latlng.lng() <= 106.7137885)
                    //         //     this.address="79 quốc lộ 13, Hiệp Bình Chánh, Thủ Đức, Hồ Chí Minh, Vietnam";
                    //
                    //         tmp += this.buildRow([{
                    //             title: 'Vị trí:',
                    //             value: this.address !== undefined ? this.address: '',
                    //             colspan: 2,
                    //             titleWidth:70
                    //         }]);
                    //
                    //         tmp += this.buildRow([{
                    //             title: 'Cập nhật:',
                    //             value: this.AdapterDate(this.ClientSend),
                    //             colspan: 2,
                    //             titleWidth:70
                    //         }]);
                    //         // tmp += this.buildRow([{
                    //         //     title: 'TG hết phí sử dụng TB:',
                    //         //     value: `<span class="text-danger">${this.AdapterDateNoTime(this.EndTime)}</span>`,
                    //         //     colspan: 2,
                    //         //     titleWidth:150
                    //         // }]);
                    //         tmp += '</table>';
                    //         defer.resolve(tmp);
                    //
                    //     });
                    // } else {
                    //     tmp += this.buildRow([{
                    //         title: 'Vị trí:',
                    //         value: this.address !== undefined ? this.address: '',
                    //         colspan: 2,
                    //         titleWidth:70
                    //     }]);
                    //     tmp += this.buildRow([{
                    //         title: 'Cập nhật:',
                    //         value: this.AdapterDate(this.ClientSend),
                    //         colspan: 2,
                    //         titleWidth:70
                    //     }]);
                    //     // tmp += this.buildRow([{
                    //     //     title: 'TG hết phí sử dụng TB:',
                    //     //     value: `<span class="text-danger">${this.AdapterDateNoTime(this.EndTime)}</span>`,
                    //     //     colspan: 2,
                    //     //     titleWidth:150
                    //     // }]);
                    //     tmp += '</table>';
                    //     defer.resolve(tmp);
                    //
                    // }

                    return defer.promise;
                }


                private buildInfoDeviceMoreMap():Q.IPromise<string>{

                    var defer = this.cfg.q.defer<string>();
                    this.cfg.$http.get(`http://geocode.adsun.vn/Geocode/GetAddress?lat=${this.latlng.lat()}&lng=${this.latlng.lng()}`).then((data:any)=> {
                        // console.log();
                        this.address = data.data.Address;
                          var contentstring:string = `<div><p>Vận tốc: <span>${this.Speed}</span></p><p>Vị trí: <span>${this.address}</span></p><p>Cập nhật: <span>${this.AdapterDate(this.ClientSend)}</span></p></div>`
                      
                        defer.resolve(contentstring);
                    });

                    return defer.promise;

                }


                public AdapterTime(value:number){
                    var tmp = '';
                    if(value < 60){
                        if(value < 10){
                            tmp = `00:0${value}:00`;
                        }else{
                            tmp = `00:${value}:00`;
                        }

                    }else{

                        var min:number = (value%60);
                        var hour:number = (value - min)/60;
                        if(hour > 10){
                            if(min > 10){
                                tmp = `${hour}:${min}:00`;
                            }else{
                                tmp = `${hour}:0${min}:00`;
                            }

                        }else{
                            if(min > 10){
                                tmp = `0${hour}:${min}:00`;
                            }else{
                                tmp = `0${hour}:0${min}:00`;
                            }
                        }
                    }
                    return tmp;
                }
                private AdapterMoney(value:number){
                    // var tmp:number = 0;
                    Sugar.Number.setOption('decimal',',');
                    Sugar.Number.setOption('thousands','.');
                    return Sugar.Number(value).format(0);
                }
                private AdapterMeterToKm(value:any){
                    Sugar.Number.setOption('decimal',',');
                    Sugar.Number.setOption('thousands','.');
                    return Sugar.Number(value/1000).format(1);
                }
                private AdapterDienAp(value:number){
                    Sugar.Number.setOption('decimal',',');
                    Sugar.Number.setOption('thousands','.');
                    return Sugar.Number(value).format(1);
                }
                /*Hàm reset giá trị trên menu phải*/
                // public resettozero(type:string) {
                //     var ReportHost:string = 'http://route1.adsun.vn/';
                //     switch (type) {
                //         case 'DaoLop':
                //             var req = {
                //                 method: 'PUT',
                //                 url: ReportHost + `api/Maintenance/ResetMaintenanceBySerial?companyId=${this.companyId}&serial=${this.serial}&optionName=DaoLop`,
                //             };
                //             this.cfg.$http(req).then((rep:any)=> {
                //
                //                 this.MenuRight.KmDaoLopCurrent = 0;
                //                 this.check_right = true;
                //                 this.updateInfo(true);
                //             });
                //
                //             break;
                //         case 'ThayLocDau':
                //             var req = {
                //                 method: 'PUT',
                //                 url: ReportHost + `api/Maintenance/ResetMaintenanceBySerial?companyId=${this.companyId}&serial=${this.serial}&optionName=ThayLocDau`,
                //             };
                //             this.cfg.$http(req).then((rep:any)=> {
                //
                //                 this.MenuRight.KmThayLocDauCurrent = 0;
                //                 this.check_right = true; // thực hiện đi cập nhật lại menu phải
                //                 this.updateInfo(true);
                //             });
                //
                //             break;
                //         case 'ThayVo':
                //             var req = {
                //                 method: 'PUT',
                //                 url: ReportHost + `api/Maintenance/ResetMaintenanceBySerial?companyId=${this.companyId}&serial=${this.serial}&optionName=ThayVo`,
                //             };
                //             this.cfg.$http(req).then((rep:any)=> {
                //
                //                 this.MenuRight.KmThayVoCurrent = 0;
                //                 this.check_right = true;
                //                 this.updateInfo(true);
                //             });
                //             break;
                //         case 'ThayNhot':
                //             var req = {
                //                 method: 'PUT',
                //                 url: ReportHost + `api/Maintenance/ResetMaintenanceBySerial?companyId=${this.companyId}&serial=${this.serial}&optionName=ThayNhot`,
                //             };
                //             this.cfg.$http(req).then((rep:any)=> {
                //
                //                 this.MenuRight.KmThayNhotCurrent = 0;
                //                 this.check_right = true;
                //                 this.updateInfo(true);
                //             });
                //             break;
                //         case 'ThayLocGio':
                //             var req = {
                //                 method: 'PUT',
                //                 url: ReportHost + `api/Maintenance/ResetMaintenanceBySerial?companyId=${this.companyId}&serial=${this.serial}&optionName=ThayLocGio`,
                //             };
                //             this.cfg.$http(req).then((rep:any)=> {
                //
                //                 this.MenuRight.KmThayLocGioCurrent = 0;
                //                 this.check_right = true;
                //                 this.updateInfo(true);
                //             });
                //             break;
                //         case 'ThayLocNhot':
                //             var req = {
                //                 method: 'PUT',
                //                 url: ReportHost + `api/Maintenance/ResetMaintenanceBySerial?companyId=${this.companyId}&serial=${this.serial}&optionName=ThayLocNhot`,
                //             };
                //             this.cfg.$http(req).then((rep:any)=> {
                //
                //                 this.MenuRight.KmThayLocNhotCurrent = 0;
                //                 this.check_right = true;
                //                 this.updateInfo(true);
                //             });
                //             break;
                //     }
                // }

                private buildInfoDeviceRight():Q.IPromise<string> {
                    var defer = this.cfg.q.defer<string>();

                    // var result:IMaintenance;
                    // if (this.check_right) {
                    //     this.cfg.$http.get(`http://route1.adsun.vn/api/Maintenance/MaintenanceReportBySerial?companyId=${this.companyId}&serial=${this.serial}`).then((rep:any)=> {
                    //         console.log(rep);
                    //         result = rep.data.MaintenanceReportList[0];
                    //         this.MenuRight.KmDaoLopCurrent = result.KmDaoLopCurrent;
                    //         this.MenuRight.KmDaoLopLimit = result.KmDaoLopLimit;
                    //         this.MenuRight.KmThayLocDauCurrent = result.KmThayLocDauCurrent;
                    //         this.MenuRight.KmThayLocDauLimit = result.KmThayLocDauLimit;
                    //         this.MenuRight.KmThayVoCurrent = result.KmThayVoCurrent;
                    //         this.MenuRight.KmThayVoLimit = result.KmThayVoLimit;
                    //         this.MenuRight.KmThayNhotCurrent = result.KmThayNhotCurrent;
                    //         this.MenuRight.KmThayNhotLimit = result.KmThayNhotLimit;
                    //         this.MenuRight.KmThayLocGioCurrent = result.KmThayLocGioCurrent;
                    //         this.MenuRight.KmThayLocGioLimit = result.KmThayLocGioLimit;
                    //         this.MenuRight.KmThayLocNhotCurrent = result.KmThayLocNhotCurrent;
                    //         this.MenuRight.KmThayLocNhotLimit = result.KmThayLocNhotLimit;
                    //
                    //         var html_right = `<table class='divTable' style='width: 320px; height: 380px;'>`;
                    //         html_right += this.buildRow([{
                    //             title: 'Số hiệu:',
                    //             value: this.id,
                    //             colspan: 1,
                    //             titleWidth:70
                    //         }, {
                    //             title: 'Biển số:',
                    //             value: this.Bs,
                    //             colspan: 1,
                    //             titleWidth:70
                    //         }]);
                    //
                    //         html_right += this.buildRow([{
                    //             title: 'Đội xe:',
                    //             value: this.group,
                    //             colspan: 1,
                    //             titleWidth:70
                    //         }, {
                    //             title: 'Loại xe:',
                    //             value: this.modeltype,
                    //             colspan: 1,
                    //             titleWidth:70
                    //         }]);
                    //
                    //         html_right += this.buildRow([{
                    //             title: 'Số ĐT TB:',
                    //             value: this.phone,
                    //             colspan: 1,
                    //             titleWidth:70
                    //         }, {
                    //             title: 'SN:',
                    //             value: this.serial,
                    //             colspan: 1,
                    //             titleWidth:70
                    //         }]);
                    //         html_right += this.buildRow([{
                    //             title: 'Tài khoản sim:',
                    //             value: this.money,
                    //             colspan: 2,
                    //             titleWidth:100
                    //         }]);
                    //         html_right += this.buildRow([{
                    //             title: 'Doanh nghiệp:',
                    //             value: this.displayName,
                    //             colspan: 2,
                    //             titleWidth:100
                    //         }]);
                    //         html_right += this.buildRow([{
                    //             title: 'Đảo lốp:',
                    //             value: this.MenuRight.KmDaoLopCurrent + "/" + this.MenuRight.KmDaoLopLimit,
                    //             colspan: 1,
                    //             titleWidth:70
                    //         },
                    //             {
                    //                 title:'',
                    //                 value:"<button class='btn btn-danger' onclick='globalOnclick(\"" + this.serial + "\",\"DaoLop\")'>Reset</button>",
                    //                 colspan: 1,
                    //             }]);
                    //         html_right += this.buildRow([{
                    //             title: 'Thay lốp:',
                    //             value: this.MenuRight.KmThayVoCurrent + "/" + this.MenuRight.KmThayVoLimit,
                    //             colspan: 1,
                    //             titleWidth:70
                    //         },
                    //             {
                    //                 title:'',
                    //                 value:"<button class='btn btn-danger' onclick='globalOnclick(\"" + this.serial + "\",\"ThayVo\")'>Reset</button>",
                    //                 colspan: 1,
                    //             }]);
                    //         html_right += this.buildRow([{
                    //             title: 'Thay nhớt:',
                    //             value: this.MenuRight.KmThayNhotCurrent + "/" + this.MenuRight.KmThayNhotLimit,
                    //             colspan: 1,
                    //             titleWidth:70
                    //         },
                    //             {
                    //                 title:'',
                    //                 value:"<button class='btn btn-danger' onclick='globalOnclick(\"" + this.serial + "\",\"ThayNhot\")'>Reset</button>",
                    //                 colspan: 1,
                    //             }]);
                    //         html_right += this.buildRow([{
                    //             title: 'Thay lọc nhớt:',
                    //             value: this.MenuRight.KmThayLocNhotCurrent + "/" + this.MenuRight.KmThayLocNhotLimit,
                    //             colspan: 1,
                    //             titleWidth:70
                    //         },
                    //             {
                    //                 title:'',
                    //                 value:"<button class='btn btn-danger' onclick='globalOnclick(\"" + this.serial + "\",\"ThayLocNhot\")'>Reset</button>",
                    //                 colspan: 1,
                    //             }]);
                    //         html_right += this.buildRow([{
                    //             title: 'Thay lọc dầu:',
                    //             value: this.MenuRight.KmThayLocDauCurrent + "/" + this.MenuRight.KmThayLocDauLimit,
                    //             colspan: 1,
                    //             titleWidth:70
                    //         },
                    //             {
                    //                 title:'',
                    //                 value:"<button class='btn btn-danger' onclick='globalOnclick(\"" + this.serial + "\",\"ThayLocDau\")'>Reset</button>",
                    //                 colspan: 1,
                    //             }]);
                    //         html_right += this.buildRow([{
                    //             title: 'Thay lọc gió:',
                    //             value: this.MenuRight.KmThayLocGioCurrent + "/" + this.MenuRight.KmThayLocGioLimit,
                    //             colspan: 1,
                    //             titleWidth:70
                    //         },
                    //             {
                    //                 title:"<button class='btn btn-danger' onclick='globalOnclick(\"" + this.serial + "\",\"ThayLocGio\")'>Reset</button>",
                    //                 value:"",
                    //                 colspan: 1,
                    //             }]);
                    //         html_right += '</table>';
                    //         this.check_right = false;
                    //         defer.resolve(html_right);
                    //     });
                    //
                    // }
                    // else {
                    //     this.MenuRight.KmDaoLopCurrent = result.KmDaoLopCurrent;
                    //     this.MenuRight.KmDaoLopLimit = result.KmDaoLopLimit;
                    //     this.MenuRight.KmThayLocDauCurrent = result.KmThayLocDauCurrent;
                    //     this.MenuRight.KmThayLocDauLimit = result.KmThayLocDauLimit;
                    //     this.MenuRight.KmThayVoCurrent = result.KmThayVoCurrent;
                    //     this.MenuRight.KmThayVoLimit = result.KmThayVoLimit;
                    //     this.MenuRight.KmThayNhotCurrent = result.KmThayNhotCurrent;
                    //     this.MenuRight.KmThayNhotLimit = result.KmThayNhotLimit;
                    //     this.MenuRight.KmThayLocGioCurrent = result.KmThayLocGioCurrent;
                    //     this.MenuRight.KmThayLocGioLimit = result.KmThayLocGioLimit;
                    //     this.MenuRight.KmThayLocNhotCurrent = result.KmThayLocNhotCurrent;
                    //     this.MenuRight.KmThayLocNhotLimit = result.KmThayLocNhotLimit;
                    //     var html_right = `<table class='divTable' style='width: 320px; height: 380px;'>`;
                    //     html_right += this.buildRow([{
                    //         title: 'Số hiệu:',
                    //         value: this.id,
                    //         colspan: 1,
                    //         titleWidth:70
                    //     }, {
                    //         title: 'Biển số:',
                    //         value: this.Bs,
                    //         colspan: 1,
                    //         titleWidth:70
                    //     }]);
                    //
                    //     html_right += this.buildRow([{
                    //         title: 'Đội xe:',
                    //         value: this.group,
                    //         colspan: 1,
                    //         titleWidth:70
                    //     }, {
                    //         title: 'Loại xe:',
                    //         value: this.modeltype,
                    //         colspan: 1,
                    //         titleWidth:70
                    //     }]);
                    //
                    //     html_right += this.buildRow([{
                    //         title: 'Số ĐT TB:',
                    //         value: this.phone,
                    //         colspan: 1,
                    //         titleWidth:70
                    //     }, {
                    //         title: 'SN:',
                    //         value: this.serial,
                    //         colspan: 1,
                    //         titleWidth:70
                    //     }]);
                    //     html_right += this.buildRow([{
                    //         title: 'Tài khoản sim:',
                    //         value: this.money,
                    //         colspan: 2,
                    //         titleWidth:100
                    //     }]);
                    //     html_right += this.buildRow([{
                    //         title: 'Doanh nghiệp:',
                    //         value: this.displayName,
                    //         colspan: 2,
                    //         titleWidth:100
                    //     }]);
                    //     html_right += this.buildRow([{
                    //         title: 'Đảo lốp:',
                    //         value: this.MenuRight.KmDaoLopCurrent + "/" + this.MenuRight.KmDaoLopLimit,
                    //         colspan: 1,
                    //         titleWidth:70
                    //     },
                    //         {
                    //             title:'',
                    //             value:"<button class='btn btn-danger' onclick='globalOnclick(\"" + this.serial + "\",\"DaoLop\")'>Reset</button>",
                    //             colspan: 1,
                    //         }]);
                    //     html_right += this.buildRow([{
                    //         title: 'Thay lốp:',
                    //         value: this.MenuRight.KmThayVoCurrent + "/" + this.MenuRight.KmThayVoLimit,
                    //         colspan: 1,
                    //     },
                    //         {
                    //             title:'',
                    //             value:"<button class='btn btn-danger' onclick='globalOnclick(\"" + this.serial + "\",\"ThayVo\")'>Reset</button>",
                    //             colspan: 1,
                    //         }]);
                    //     html_right += this.buildRow([{
                    //         title: 'Thay nhớt:',
                    //         value: this.MenuRight.KmThayNhotCurrent + "/" + this.MenuRight.KmThayNhotLimit,
                    //         colspan: 1,
                    //         titleWidth:70
                    //     },
                    //         {
                    //             title:'',
                    //             value:"<button class='btn btn-danger' onclick='globalOnclick(\"" + this.serial + "\",\"ThayNhot\")'>Reset</button>",
                    //             colspan: 1,
                    //         }]);
                    //     html_right += this.buildRow([{
                    //         title: 'Thay lọc nhớt:',
                    //         value: this.MenuRight.KmThayLocNhotCurrent + "/" + this.MenuRight.KmThayLocNhotLimit,
                    //         colspan: 1,
                    //         titleWidth:70
                    //     },
                    //         {
                    //             title:'',
                    //             value:"<button class='btn btn-danger' onclick='globalOnclick(\"" + this.serial + "\",\"ThayLocNhot\")'>Reset</button>",
                    //             colspan: 1,
                    //         }]);
                    //     html_right += this.buildRow([{
                    //         title: 'Thay lọc dầu:',
                    //         value: this.MenuRight.KmThayLocDauCurrent + "/" + this.MenuRight.KmThayLocDauLimit,
                    //         colspan: 1,
                    //         titleWidth:70
                    //     },
                    //         {
                    //             title:'',
                    //             value:"<button class='btn btn-danger' onclick='globalOnclick(\"" + this.serial + "\",\"ThayLocDau\")'>Reset</button>",
                    //             colspan: 1,
                    //         }]);
                    //     html_right += this.buildRow([{
                    //         title: 'Thay lọc gió:',
                    //         value: this.MenuRight.KmThayLocGioCurrent + "/" + this.MenuRight.KmThayLocGioLimit,
                    //         colspan: 1,
                    //         titleWidth:70
                    //     },
                    //         {
                    //             title:"<button class='btn btn-danger' onclick='globalOnclick(\"" + this.serial + "\",\"ThayLocGio\")'>Reset</button>",
                    //             value:"",
                    //             colspan: 1,
                    //         }]);
                    //     html_right += '</table>';
                    //     defer.resolve(html_right);
                    // }
                    // var endtime:any = this.GetDateTimeCurrent();
                    // var starttime_30:any = this.GetDateTimeBefore30min();
                    // var starttime_60:any = this.GetDateTimeBefore60min();
                    // console.log(endtime);


                    var html_right = `<table class='divTable' style='width: 320px; height: 180px;'>`;
                    html_right += this.buildRow([{
                        title: 'Số tài:',
                        value: `<span class="text-danger">${this.SoHieu}</span>`,
                        colspan: 1,
                        titleWidth:70
                    }, {
                        title: 'Biển số:',
                        value: `<span class="text-danger">${this.Bs}</span>`,
                        colspan: 1,
                        titleWidth:70
                    }]);

                    html_right += this.buildRow([{
                        title: 'Đội xe:',
                        value: this.DeviceGroup,
                        colspan: 1,
                        titleWidth:70
                    }, {
                        title: 'Loại xe:',
                        value: this.Model,
                        colspan: 1,
                        titleWidth:70
                    }]);

                    html_right += this.buildRow([{
                        title: 'Số ĐT TB:',
                        value: this.Phone,
                        colspan: 1,
                        titleWidth:70
                    }, {
                        title: 'Serial:',
                        value: this.serial,
                        colspan: 1,
                        titleWidth:70
                    }]);
                    html_right += this.buildRow([{
                        title: 'Tài khoản sim:',
                        value: this.Money,
                        colspan: 2,
                        titleWidth:100
                    }]);
                    html_right += this.buildRow([{
                        title: 'Số phút',
                        value: `<input class="form-control" type="number" name="number" id="sophut" min="5" max="1400" >`,
                        colspan: 1,
                        titleWidth:70
                    },{
                        title: `<button class="btn btn-success" onclick="xemhanhtrinh(${this.CompanyId},${this.Serial},$('#sophut').val())">Xem hành trình</button>`,
                        value: '',
                        colspan: 1,
                        titleWidth:70
                    }
                        // {
                        //     title: `<a class="btn btn-success" href="/hanh-trinh-taxi/${this.CompanyId}/${this.Serial}/${starttime_60}/${endtime}" target="_blank" style="font-size: 12px;padding: 5px">Hành trình 60 phút</a>`,
                        //     value: '',
                        //     colspan: 1,
                        //     titleWidth:70
                        // }
                    ]);


                    html_right += '</table>';
                    defer.resolve(html_right);

                    return defer.promise;
                }


                // public GetDateTimeCurrent(){
                //     let starttime:any = new Date();
                //     let hournow:any = starttime.getHours() > 9? starttime.getHours(): `0${starttime.getHours()}`;
                //     let minnow:any = starttime.getMinutes() > 9? starttime.getMinutes(): `0${starttime.getMinutes()}`;
                //
                //     let daynow:any = starttime.getDate() > 9? starttime.getDate(): `0${starttime.getDate()}`;
                //     let monthnow:any = (starttime.getMonth() + 1) > 9? starttime.getMonth() + 1: `0${starttime.getMonth() + 1}`;
                //     let yearnow:any = starttime.getFullYear();
                //
                //     let tmp = `${hournow}:${minnow}:00 ${monthnow}-${daynow}-${yearnow}`;
                //     return tmp;
                // }
                // public GetDateTimeBefore30min(){
                //     let starttime:any = new Date();
                //     starttime.setMinutes(starttime.getMinutes() - 30);
                //
                //     let hournow:any = starttime.getHours() > 9? starttime.getHours(): `0${starttime.getHours()}`;
                //     let minnow:any = starttime.getMinutes() > 9? starttime.getMinutes(): `0${starttime.getMinutes()}`;
                //
                //     let daynow:any = starttime.getDate() > 9? starttime.getDate(): `0${starttime.getDate()}`;
                //     let monthnow:any = (starttime.getMonth() + 1) > 9? starttime.getMonth() + 1: `0${starttime.getMonth() + 1}`;
                //     let yearnow:any = starttime.getFullYear();
                //
                //     let tmp = `${hournow}:${minnow}:00 ${monthnow}-${daynow}-${yearnow}`;
                //     return tmp;
                // }
                // public GetDateTimeBefore60min(){
                //     let starttime:any = new Date();
                //     starttime.setMinutes(starttime.getMinutes() - 60);
                //
                //     let hournow:any = starttime.getHours() > 9? starttime.getHours(): `0${starttime.getHours()}`;
                //     let minnow:any = starttime.getMinutes() > 9? starttime.getMinutes(): `0${starttime.getMinutes()}`;
                //
                //     let daynow:any = starttime.getDate() > 9? starttime.getDate(): `0${starttime.getDate()}`;
                //     let monthnow:any = (starttime.getMonth() + 1) > 9? starttime.getMonth() + 1: `0${starttime.getMonth() + 1}`;
                //     let yearnow:any = starttime.getFullYear();
                //
                //     let tmp = `${hournow}:${minnow}:00 ${monthnow}-${daynow}-${yearnow}`;
                //     return tmp;
                // }


                /*lấy thông tin hình anh tùy theo mỗi trạng thái thiết bị*/
                private getIconTaxi():any {
                    $(`canvas#c_${this.serial}`).removeClass('xebantaxi');
                    if (this.LostGsm) {

                        return '../../assets/images/carIcon/break_car.png';
                    } else {
                        if(!this.MeterOk){
                            //trạng thái có đồng hồ hay không
                            return '../../assets/images/carIcon/matdongho.png';
                        }else{
                            //trạng thái có đồng hồ
                            if(!this.Gps){
                                // if(this.OverSpeed){
                                //     $(`canvas#c_${this.serial}`).addClass('xequatocdo');
                                // }
                                return '../../assets/images/carIcon/Lostgps_car.png';
                            }else{
                                if (this.MeterStatus) {
                                    if(this.OverSpeed){
                                        $(`canvas#c_${this.serial}`).addClass('xequatocdo');
                                        return '../../assets/images/carIcon/run_car.png';
                                    }else{
                                        $(`canvas#c_${this.serial}`).removeClass('xequatocdo');
                                        return '../../assets/images/carIcon/run_car.png';
                                    }
                                    //nếu có khách
                                    //return '../../assets/images/carIcon/run_car.png';
                                } else{
                                    //nếu ko khách
                                    if(this.IsBusy){
                                        $('canvas.xetarget').removeClass('xetarget');
                                        // $(`canvas#c_${this.serial}`).addClass('xequatocdo');
                                        return '../../assets/images/carIcon/Stop_car.png';
                                    }else{
                                        if (this.Speed !== 0) {
                                            //nếu vận tốc khác không và không có khách
                                            return '../../assets/images/carIcon/Stop_car.png';
                                        } else {
                                            if(this.KeyStatus){
                                                //xe dừng
                                                return '../../assets/images/carIcon/money_car.png';
                                            }else if (!this.KeyStatus) {
                                                //xe đỗ
                                                return '../../assets/images/carIcon/Lock_car.png';
                                            }

                                        }
                                    }

                                }


                            }

                        }

                    }

                    // if(this.lostgsm){
                    //     return '../../assets/images/carIcon/break_car.png';
                    // } else if(!this.statusGps){
                    //     return '../../assets/images/carIcon/Lostgps_car.png';
                    // }else if(!this.dungDo){
                    //     return '../../assets/images/carIcon/run_car.png';
                    // }
                    // else if(!this.trangThaiMay){
                    //     return '../../assets/images/carIcon/Lock_car.png';
                    // }
                    // else if(this.dungDo){
                    //     return '../../assets/images/carIcon/Stop_car.png';
                    // }else if(this.isOverSpeed){
                    //     return '../../assets/images/carIcon/b_car.gif';
                    // }else
                    //     // return '../../assets/images/carIcon/run_car.png';
                    //     console.log('chưa co icon truong hop ngoai le');

                }


                private getIconTaxiSun():any {
                    $(`canvas#c_${this.serial}`).removeClass('xebantaxi');
                    if (this.LostGsm) {

                        return '../../assets/images/carIcon/break_car.png';
                    } else {
                        if(!this.MeterOk){
                            //trạng thái có đồng hồ hay không
                            return '../../assets/images/carIcon/matdongho.png';
                        }else{
                            //trạng thái có đồng hồ
                            if(!this.Gps){
                                // if(this.OverSpeed){
                                //     $(`canvas#c_${this.serial}`).addClass('xequatocdo');
                                // }
                                return '../../assets/images/carIcon/Lostgps_car.png';
                            }else{
                                if (this.MeterStatus) {
                                    if(this.OverSpeed){
                                        $(`canvas#c_${this.serial}`).addClass('xequatocdo');
                                        return '../../assets/images/carIcon/run_car.png';
                                    }else{
                                        $(`canvas#c_${this.serial}`).removeClass('xequatocdo');
                                        return '../../assets/images/carIcon/run_car.png';
                                    }
                                    //nếu có khách
                                    //return '../../assets/images/carIcon/run_car.png';
                                } else{
                                    //nếu ko khách
                                    if(this.IsBusy){
                                        $('canvas.xetarget').removeClass('xetarget');
                                        // $(`canvas#c_${this.serial}`).addClass('xequatocdo');
                                        return '../../assets/images/carIcon/Stop_car.png';
                                    }else{
                                        if (this.SoGhe === 4 || this.SoGhe === 5 || this.SoGhe === 0) {
                                            return '../../assets/images/carIcon/Lock_car.png';
                                        } else if (this.SoGhe === 7) {
                                            return '../../assets/images/carIcon/Stop_car.png';
                                        } else {
                                            return '../../assets/images/carIcon/money_car.png';
                                        }
                                    }

                                }


                            }

                        }

                    }

                    // if(this.lostgsm){
                    //     return '../../assets/images/carIcon/break_car.png';
                    // } else if(!this.statusGps){
                    //     return '../../assets/images/carIcon/Lostgps_car.png';
                    // }else if(!this.dungDo){
                    //     return '../../assets/images/carIcon/run_car.png';
                    // }
                    // else if(!this.trangThaiMay){
                    //     return '../../assets/images/carIcon/Lock_car.png';
                    // }
                    // else if(this.dungDo){
                    //     return '../../assets/images/carIcon/Stop_car.png';
                    // }else if(this.isOverSpeed){
                    //     return '../../assets/images/carIcon/b_car.gif';
                    // }else
                    //     // return '../../assets/images/carIcon/run_car.png';
                    //     console.log('chưa co icon truong hop ngoai le');

                }

                public setMap(map:google.maps.Map) {
                    if (map == null)
                        super.setMap(map);
                    else if (map.getBounds().contains(this.latlng) && this.getMap() == null)//kiểm tra xem xe có nằm trong khu vực hiển thị hay không
                        super.setMap(map);
                    //else
                    //if(this.getMap()!=null)
                    //    super.setMap(null);
                }


            }
        }
    }
}
