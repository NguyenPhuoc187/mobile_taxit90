/**
 * Created by phamn on 21/05/2016.
 */
/// <reference path="area.lib.ts" />
module App {
    export module Shared {
        export module Library {
            export class AreaManage {
                map:any;
                mainMap:any;
                areaArray:any;
                newDesign:any;
                companyId:number;
                isLoadComplete:boolean;
                listenerArray:any;
                isShow:boolean;
                q:any;
                areaHighwayArray:any;
                constructor(map:google.maps.Map,$q:ng.IQService, _companyId:number, public $http: ng.IHttpService, public global: App.Shared.GlobalService) {
                    this.mainMap = map;
                    this.q = $q;
                    this.areaArray = [];
                    this.areaHighwayArray = [];
                    this.companyId = _companyId;
                    this.isLoadComplete = false;
                    this.listenerArray = [];
                    this.isShow = false;
                    this.global.ReportHost = global.ReportHost;
                    //this.funAcc = funAcc;

                }

                addListener(id:any, name:any, callback:any) {
                    var check = true;
                    angular.forEach(this.listenerArray, function (it) {
                        if (it.id == id && it.name == name)
                            check = false;
                    });
                    if (check)
                        this.listenerArray.push({id: id, name: name, callback: callback});
                };

                //vẽ vùng
                areaDraw() {
                    //var $this=this;
                    var a:any = new App.Shared.Library.Area(this.mainMap);
                    a.onShow = this.areaAdd.bind(this);
                    a.onDelete = this.areaDelete.bind(this);
                    a.infoArea();
                    this.newDesign = a;

                };


                //vẽ vùng cao tốc
                areaDraw_Highway(){
                    var a: any = new App.Shared.Library.Area(this.mainMap);
                    a.onShow = this.areaAdd_HighWay.bind(this);
                    a.onDelete = this.areaDelete.bind(this);
                    a.infoArea();
                    this.newDesign = a;
                }

                //load vùng
                public load(companyId:any) {
                    this.clear();

                    this.companyId = companyId;
                    var defer = this.q.defer();
                    /*làm sạch danh sách chứa*/
                    /*lấy thông tin từ server*/
                    this.$http.get(this.global.ReportHost + `api/CheckZone/GetCheckZoneByCompany?companyId=${this.companyId}`).then((rep:any)=>{
                        // console.log(rep);
                        this.clear();
                        this.isLoadComplete = false;
                        /*add vào danh sách*/
                        angular.forEach(rep.data.CheckZoneList,(d:any)=> {
                            var p = new App.Shared.Library.Area(this.mainMap);
                            p.onDelete = this.areaDelete.bind(this);
                            var arrPoint:any = [];
                            angular.forEach(d.Points, function (po) {
                                arrPoint.push(new google.maps.LatLng(po.Lat, po.Lng));
                            });
                            p.add(arrPoint);
                            p.addInfoArea(d.Name, d.Description, d.MaxSpeed, d.Id);
                            p.hide();
                            this.areaArray.push({Id: d.Id, Name: d.Name, MaxSpeed: d.MaxSpeed, value: p});
                            // console.log(this.areaArray);
                        });
                        this.isLoadComplete = true;
                        activeListenerEvent(this);
                        defer.resolve(this.areaArray);
                    });

                    return defer.promise;
                   
                };

                public load_highway(){
                    var defer = this.q.defer();
                    /*làm sạch danh sách chứa*/
                    /*lấy thông tin từ server*/
                    this.$http.get('http://geocode.adsun.vn/api/CheckZone/GetAllCheckZone').then((rep:any)=>{
                        // console.log(rep);
                        this.clear();
                        this.isLoadComplete = false;
                        /*add vào danh sách*/
                        angular.forEach(rep.data.ListCheckZone,(d:any)=> {
                            var p = new App.Shared.Library.Area(this.mainMap);
                            p.onDelete = this.areaDelete_HighWay.bind(this);
                            var arrPoint:any = [];
                            angular.forEach(d.Points, function (po) {
                                arrPoint.push(new google.maps.LatLng(po.Lat, po.Lng));
                            });
                            p.add(arrPoint);
                            p.addInfoArea(d.Name, d.Description, d.MaxSpeed, d.Id);
                            p.show();
                            this.areaArray.push({Id: d.Id, Name: d.Name, MaxSpeed: d.MaxSpeed, value: p});

                        });
                        this.isLoadComplete = true;
                        activeListenerEvent(this);
                        defer.resolve(this.areaArray);
                    });

                    return defer.promise;
                }

                //hiển thị toàn bộ vùng
                show(val:any) {
                    this.isShow = val;
                    angular.forEach(this.areaArray,(a:any)=> {
                        // console.log(a);
                        if (val)
                            a.value.show();
                        else
                            a.value.hide();
                    })
                };

                //xoá vùng
                clear() {
                    while (this.areaArray.length > 0) {
                        var p:any = this.areaArray.pop().value;
                        p.aDelete();
                        p.dispose();
                    }
                };

                cancelDesign() {
                    if (this.newDesign != undefined) {
                        this.newDesign.delete();
                        this.newDesign = undefined;
                    }
                };

                //lấy danh sách vùng
                public getAllArea() {
                    return this.areaArray;
                };

                areaDelete(a:any) {
                    /*call back server*/
                    bootbox.dialog({
                        title: "<h3 class='text-center'>Xoá vùng</h3>",
                        message: "Ban có muốn xoá: " + a.name,
                        buttons: {
                            success: {
                                label: "<span class='glyphicon glyphicon-ok'></span>" + "&nbspĐồng ý",
                                className: "btn-success text-center",
                                callback: ()=>{
                                    a.aDelete();
                                    a.dispose();
                                    angular.forEach(this.areaArray,(val:any, i:any)=>{
                                        if (val.Id === a.id) {
                                            this.callbackDeleteArea(a.id);
                                        }
                                    });

                                }
                            },
                            main: {
                                label: "<span class='glyphicon glyphicon-remove'></span>" + "&nbspHuỷ",
                                className: "btn-danger text-center",
                                callback: function () {

                                }
                            }
                        }
                    });

                };

            //thêm thông tin vùng
                areaAdd(a:any) {
                    //var $this = this;
                    this.areaShowFrom(a).then((req:any)=> {
                        // var data_req = {
                        //     Name: this.funAcc.Name,
                        //     Description: this.funAcc.Description,
                        //     CompanyId: this.funAcc.CompanyId,
                        //     MaxSpeed: this.funAcc.MaxSpeed,
                        //     MaxDevice: this.funAcc.MaxDevice,
                        //     Points: this.funAcc.Points
                        // };
                        this.$http.post(this.global.ReportHost + 'api/CheckZone/AddCheckZone', req).then((data:any)=>{
                            // console.log(data);
                            a.addInfoArea(data.Name, data.Description, data.MaxSpeed , data.Id);
                            this.areaArray.push({Id: data.Id, MaxDevice: data.MaxDevice, value: a, slXe: 0});

                        });
                    });
                };
                //Xóa vùng cao tốc
                areaDelete_HighWay(value:any) {
                    /*call back server*/
                    bootbox.dialog({
                        title: "<h3 class='text-center'>Xoá vùng</h3>",
                        message: "Ban có muốn xoá: " + value.name,
                        buttons: {
                            success: {
                                label: "<span class='glyphicon glyphicon-ok'></span>" + "&nbspĐồng ý",
                                className: "btn-success text-center",
                                callback: ()=>{
                                    value.aDelete();
                                    value.dispose();
                                    angular.forEach(this.areaArray,(val:any, i:any)=>{
                                        if (val.Id === value.id) {
                                            this.callbackDeleteAreaHighway(value.id);
                                        }
                                    });

                                }
                            },
                            main: {
                                label: "<span class='glyphicon glyphicon-remove'></span>" + "&nbspHuỷ",
                                className: "btn-danger text-center",
                                callback: function () {

                                }
                            }
                        }
                    });

                };

                //Thêm vùng cao tốc
                areaAdd_HighWay(area:any){
                    this.areaShowFrom_Highway(area).then((data:any)=>{
                        this.$http.post('http://geocode.adsun.vn/api/CheckZone/AddCheckZone', data).then((rep:any)=>{
                            if(rep.data.Status == 1){
                                area.addInfoArea(rep.Name ,rep.Description, rep.MaxSpeed , rep.Id );
                                this.$http.get(this.global.ReportHost + 'api/Tool/LoadZoneSpeed').then((rep:any)=>{
                                   console.log('load vung cao toc moi ok');
                                });
                            }else{
                                bootbox.dialog({
                                    title: "Thông Báo",
                                    message: 'Tạo vùng cao tốc mới không thành công',
                                    buttons: {
                                        cancle: {
                                            label: "Quay lại",
                                            className: "btn-danger",
                                        }
                                    },
                                });
                            }

                        });
                    })
                };
                //xoá vùng trên server
                callbackDeleteArea(id:any){

                    this.$http.delete(this.global.ReportHost + `api/CheckZone/DeleteCheckZone?idCheckZone=${id}&companyId=${this.companyId}`).then((data:any)=>{
                        // console.log(data);
                        //this.isLoadComplete = false;
                        //this.areaArray.splice(index, 1);
                        //this.ul.message.warning('xoá thành công');
                        //this.isLoadComplete = true;

                        activeListenerEvent(this);
                    });

                };
                callbackDeleteAreaHighway(id:any){
                    this.$http.delete(`http://geocode.adsun.vn/api/CheckZone/DeleteCheckZone?idCheckZone=${id}`).then((rep:any)=>{
                        activeListenerEvent(this);
                    });
                }
                // // lấy thông tin của mảng vùng theo id của row khi click
                // public where(where:(area:any)=>boolean):Array<any> {
                //     var result = Array<any>();
                //     angular.forEach(this.getAllArea(), function (area) {
                //         if (where(area))
                //             result.push(area);
                //     });
                //     return result;
                // }

                public get(Name:any) {
                    var result:any;
                    angular.forEach(this.areaArray, function (obj) {
                        // console.log(obj);
                        if (obj.Name == Name)
                            result = obj.value;
                    });
                    return result;
                    //console.log(result);
                };

                checkCarInArea(carArray:any) {
                    angular.forEach(this.areaArray,(ar)=> {
                        ar.value.checkCarInArea(carArray);
                    });
                    activeListenerEvent(this);
                };

                //hiển thị show
                areaShowFrom(data:any) {
                    //var $this = this;
                    var defer = this.q.defer();
                    var companyId = this.companyId;
                    bootbox.dialog({
                        title: "<h3 class='text-center'>Thêm Vùng</h3>",
                        message: buildHtmlArea(null),
                        buttons: {
                            success: {
                                label: "<span class='glyphicon glyphicon-ok'></span>" + "&nbspĐồng ý",
                                className: "btn-success",
                                callback: function () {
                                    var arraypoint:any = [];
                                    // console.log(data.polyline.j);
                                    angular.forEach(data.polyline.b, function (d:any) {
                                        var dt:any = {};
                                        dt.Lat = d.lat();
                                        dt.Lng = d.lng();
                                        dt.Address = 'null';
                                        arraypoint.push(dt);
                                    });
                                    //console.log(companyId);
                                    var result = {
                                        CompanyId: companyId,
                                        Points: arraypoint,
                                        Name: $('#manageArea-idName').val(),
                                        Description: $('#manageArea-description').val(),
                                        MaxSpeed: $('#manageArea-speed').val(),
                                        MaxDevice: $('#manageArea-device').val()
                                    };
                                    defer.resolve(result);
                                }
                            },
                            cancle: {
                                label: "<span class='glyphicon glyphicon-remove'></span>" + "&nbspHủy",
                                className: "btn-danger",
                                callback: ()=>{
                                    defer.reject();
                                    this.newDesign.polygons.setMap(null);
                                    //this.areaDelete = (a:any) => {
                                    //
                                    //    a.aDelete();
                                    //    a.dispose();
                                    //}

                                }
                            }
                        }
                    });
                    return defer.promise;
                }

                areaShowFrom_Highway(data:any) {
                    var defer = this.q.defer();
                    bootbox.dialog({
                        title: "<h3 class='text-center'>Thêm Vùng</h3>",
                        message: buildHtmlArea_Highway(null),
                        buttons: {
                            success: {
                                label: "<span class='glyphicon glyphicon-ok'></span>" + "&nbspĐồng ý",
                                className: "btn-success",
                                callback: function () {
                                    var arraypoint:any = [];
                                    console.log(data.polyline.j);
                                    angular.forEach(data.polyline.b, (d:any)=> {
                                        var result_data:any = {};
                                        result_data.Lat = d.lat();
                                        result_data.Lng = d.lng();
                                        result_data.Address = 'null';
                                        arraypoint.push(result_data);
                                    });
                                    //console.log(companyId);
                                    var result = {
                                        Points: arraypoint,
                                        Name: $('#manageAreaHighway-idName').val(),
                                        Description: $('#manageAreaHighway-description').val(),
                                        MaxSpeed: $('#manageAreaHighway-speed').val(),
                                    };
                                    defer.resolve(result);
                                }
                            },
                            cancle: {
                                label: "<span class='glyphicon glyphicon-remove'></span>" + "&nbspHủy",
                                className: "btn-danger",
                                callback: ()=>{
                                    defer.reject();
                                    this.newDesign.polygons.setMap(null);
                                }
                            }
                        }
                    });
                    return defer.promise;
                }


            }
            export function activeListenerEvent(manage:any) {
                angular.forEach(manage.listenerArray,(ls:any)=> {
                    if (ls.name == "updateArea")
                        ls.callback(manage);
                });
            }

            function buildHtmlArea(infoArea:any) {
                var html = "";
                html = "<form class='form-horizontal bucket-form' role='form'>" +
                        "<div class='form-group'>" +
                            "<label class='col-sm-3 control-label'>Tên vùng:</label>" +
                            "<div class='col-sm-6'>" +
                                "<input type='text' placeholder='Nhập tên vùng' class='form-control' id='manageArea-idName'>" +
                            "</div>" +
                        "</div>" +
                        "<div class='form-group'>" +
                            "<label class='col-sm-3 control-label'>Vận tốc tối đa:</label>" +
                            "<div class='col-sm-6'>" +
                            "<input type='text' placeholder='Nhập vận tốc' class='form-control' id='manageArea-speed'>" +
                            "</div>" +
                        "</div>" +
                        "<div class='form-group'>" +
                            "<label class='col-sm-3 control-label'>Mô tả:</label>" +
                            "<div class='col-sm-6'>" +
                                "<input type='text' placeholder='Nhập mô tả' class='form-control' id='manageArea-description'>" +
                            "</div>" +
                        "</div>" +
                        "<div class='form-group'>" +
                            "<label class='col-sm-3 control-label'>Thiết bị:</label>" +
                            "<div class='col-sm-6'>" +
                                "<input type='text' placeholder='Nhập thiết bị' class='form-control' id='manageArea-device'>" +
                            "</div>" +
                        "</div>" +
                        "</form>";
                return html;
            }
            function buildHtmlArea_Highway(infoArea:any){
                var html = "";
                html = "<form class='form-horizontal bucket-form' role='form'>" +
                            "<div class='form-group'>" +
                                "<label class='col-sm-3 control-label'>Tên vùng:</label>" +
                                "<div class='col-sm-6'>" +
                                    "<input type='text' placeholder='Nhập tên vùng' class='form-control' id='manageAreaHighway-idName'>" +
                                "</div>" +
                            "</div>" +
                            "<div class='form-group'>" +
                                "<label class='col-sm-3 control-label'>Vận tốc tối đa:</label>" +
                                "<div class='col-sm-6'>" +
                                    "<input type='text' placeholder='Nhập vận tốc' class='form-control' id='manageAreaHighway-speed'>" +
                                "</div>" +
                            "</div>" +
                            "<div class='form-group'>" +
                                "<label class='col-sm-3 control-label'>Mô tả:</label>" +
                                "<div class='col-sm-6'>" +
                                    "<input type='text' placeholder='Nhập mô tả' class='form-control' id='manageAreaHighway-description'>" +
                                "</div>" +
                            "</div>" +
                        "</form>";
                return html;
            }
        }
    }
}


