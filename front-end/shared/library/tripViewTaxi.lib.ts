/**
 * Created by NPPRO on 28/12/2016.
 */

module App{
    export module Shared{
        export module Library{
            export class TripViewTaxi {
                public static $inject =  ['$http'];
                private tripEventInfo:any;
                private drawTrip:any;
                private ListPointStop:any;
                private _listpointdontrakhach:any;
                private map:any;
                private oldLatLng:any;
                private oldCurrentMoney:any;
                private http:any;
                private infoglobal:any;
                constructor(config:CarConfig){
                    this.tripEventInfo=[];/*thông tin sự kiện trên chặng đường đã đi*/
                    this.drawTrip =[];
                    this.ListPointStop=[];
                    this._listpointdontrakhach = [];
                    this.map=config.map;
                    this.http = config;
                    this.infoglobal = new google.maps.InfoWindow();
                }

                push=(val:any)=> {

                    if (this.oldLatLng != undefined) {
                        if(this.oldCurrentMoney == null || this.oldCurrentMoney == undefined){
                            this.oldCurrentMoney = val.CurentMoneySession;
                        }
                        // console.log(val);
                        var iconsetngs = {
                            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                            scale: 2
                        };
                        console.log();
                        var chuoi=[{
                            repeat: '300px', //CHANGE THIS VALUE TO CHANGE THE DISTANCE BETWEEN ARROWS
                            icon: iconsetngs,
                            offset: '50%'}];
                        if(val.speed <5 || (val.index % 3 != 0))
                        {
                            chuoi=null;
                        }
                        var line:any = new google.maps.Polyline({
                            geodesic: true,
                            strokeColor: '#f80003',
                            strokeOpacity: 0.7,
                            strokeWeight: 5,
                            icons: chuoi
                        });
                        //
                        // var line_cokhach = new google.maps.Polyline({
                        //     geodesic: true,
                        //     strokeColor: 'blue',
                        //     strokeOpacity: 0.7,
                        //     strokeWeight: 4,
                        //     icons: chuoi
                        // });

                        //tạo hiệu ứng cho animation google map
                        // this.animateCircle(line);
                        //StatusDevice : trạng thái có khách hay không khách
                        if(val.StatusDevice){
                            console.log('status chạy khách');
                            line.strokeColor = '#1F3A93';
                            line.setMap(this.map);
                            var path = line.getPath();
                            if(val.CurentMoneySession !== 0 && this.oldCurrentMoney == 0){
                                line.strokeColor = '#f80003';
                                line.setMap(this.map);
                                path = line.getPath();
                                this.buildpointdonkhach(val,'donkhach');
                            }
                            this.oldCurrentMoney = val.CurentMoneySession;

                        }else{
                            line.strokeColor = '#f80003';
                            line.setMap(this.map);
                            path = line.getPath();
                            if(val.CurentMoneySession == 0 && this.oldCurrentMoney !== 0){
                                // console.log('diểm trả khách');
                                // console.log(val);
                                this.buildpointdonkhach(val,'trakhach');
                            }
                            this.oldCurrentMoney = val.CurentMoneySession;
                        }

                        // var path = line.getPath();
                        path.push(this.oldLatLng);
                        path.push(val.latLng);


                        if(val.pointEnd!=null)
                        {

                            // if(val.pointEnd.status==0) {
                            //     // console.log('điểm dừng');
                            //     // console.log(val);
                            //     var markerStop = new google.maps.Marker({
                            //         position: val.pointEnd.latLng,
                            //         map: this.map,
                            //         icon: '../../assets/images/carIcon/Stop.png',
                            //         title: "Dừng: "+val.pointEnd.title
                            //     });
                            //     markerStop.setMap(this.map);
                            //     this.ListPointStop.push(markerStop);
                            // }
                            if(val.pointEnd.status==1) {
                                // console.log('điểm đỗ');
                                // console.log(val);
                                var markerStop = new google.maps.Marker({
                                    position: val.pointEnd.latLng,
                                    map: this.map,
                                    icon: '../../assets/images/carIcon/Off.png',
                                    title: "Dừng đỗ: "+val.pointEnd.title
                                });
                                markerStop.setMap(this.map);
                                this.ListPointStop.push(markerStop);
                            }

                        }
                        else{
                            // console.log(val);
                            this.ListPointStop.push(null);

                        }





                        this.drawTrip.push(line);




                        // if(val.StatusDevice){
                        //     if(val.CurentMoneySession !== 0 && this.oldCurrentMoney == 0){
                        //         var markerDonkhach = new google.maps.Marker({
                        //             position: val.pointEnd.latLng,
                        //             map: this.map,
                        //             icon: '../../assets/images/carIcon/donkhach.png',
                        //             title: "Đón khách: "+val.pointEnd.title
                        //         });
                        //         markerDonkhach.setMap(this.map);
                        //         this._listpointdontrakhach.push(markerDonkhach);
                        //
                        //
                        //     }
                        //     // else{
                        //     //     this.oldCurrentMoney = val.CurentMoneySession;
                        //     //     console.log("test lại")
                        //     // }
                        //     this.oldCurrentMoney = val.CurentMoneySession;
                        //     // this.oldCurrentMoney = val.CurentMoneySession;
                        //     //xét trường hợp đang có khách - điểm dừng hoặc đỗ sẽ là điểm đón trả khách
                        //
                        // }else{
                        //     //trạng thái không khách
                        //     if(val.CurentMoneySession == 0 && this.oldCurrentMoney !== 0){
                        //         var markerTraKhach = new google.maps.Marker({
                        //             position: val.pointEnd.latLng,
                        //             map: this.map,
                        //             icon: '../../assets/images/carIcon/trakhach.png',
                        //             title: "Trả khách: "+val.pointEnd.title
                        //         });
                        //         markerTraKhach.setMap(this.map);
                        //         this._listpointdontrakhach.push(markerTraKhach);
                        //     }
                        // }
                        var contentString = '';

                        // line.addListener('click',(e:any)=>{
                        //
                        //     console.log(e);
                        //     console.log(val);
                        //     console.log('latmouse',val.latLng.lat());
                        //     console.log('latmouse',e.latLng.lat());
                        //
                        //     this.http.$http.get(`http://geocode.adsun.vn/Geocode/GetAddress?lat=${e.latLng.lat()}&lng=${e.latLng.lng()}`).then((rep:any)=>{
                        //         if(rep.data.Status == 1){
                        //             contentString = `<div><p>Thời điểm: ${val.TimeUpdate}</p><p>Địa chỉ: ${rep.data.Address}</p></div>`;
                        //             infowindow.setContent(contentString);
                        //             infowindow.setPosition(e.latLng);
                        //             infowindow.open(this.map);
                        //
                        //         }else{
                        //             contentString = `<div><p>Thời điểm: ${val.TimeUpdate}</p></div>`;
                        //             infowindow.setContent(contentString);
                        //             infowindow.setPosition(e.latLng);
                        //             infowindow.open(this.map);
                        //         }
                        //     });
                        //
                        // });
                        line.addListener('click', (e:any)=>{
                            contentString = `<div><p>Thời điểm: ${val.TimeUpdate}</p></div>`;
                            this.infoglobal.setContent(contentString);
                            this.infoglobal.setPosition(e.latLng);
                            this.infoglobal.open(this.map);
                        });


                    }
                    this.oldLatLng = val.latLng;
                };
                length=()=> {
                    return this.drawTrip.length;
                };

                pop=()=> {
                    var line = this.drawTrip.pop();
                    var point=this.ListPointStop.pop();

                    var tmp = line.getPath();
                    this.oldLatLng = tmp.getAt(0);
                    line.setMap(null);
                    if(point!=null) {
                        point.setMap(null);
                    }

                    // if(point_donkhach !== null){
                    //     point_donkhach.setMap(null);
                    // }
                    //delete line;
                };

                current=(index:any)=> {
                    if (index > this.drawTrip.length)
                        return false;
                    for (var i = 0; i < this.drawTrip.length - index; i++) {
                        this.pop();
                    }
                };

                clear=()=> {
                    var len = this.drawTrip.length;
                    for (var i = 0; i < len; i++) {
                        this.pop();
                    }
                    this.oldLatLng = undefined;
                    this.clear_pointdonkhach();
                };
                clear_pointdonkhach(){
                    $.each(this._listpointdontrakhach,(ind:number,val:any)=>{
                        val.setMap(null);
                    });
                }
                dispose=()=> {
                    this.clear();
                };

                buildpointdonkhach(value:any,tmp:string){
                    var contentString = '';

                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });


                    this.http.$http.get(`http://geocode.adsun.vn/Geocode/GetAddress?lat=${value.latLng.lat()}&lng=${value.latLng.lng()}`).then((rep:any)=>{
                        if(rep.data.Status == 1){

                            switch (tmp){
                                case 'donkhach':
                                    contentString = `<div><h6 class="text-center">Đón khách</h6><p>Thời điểm: ${value.TimeUpdate}</p><p>Địa chỉ: ${rep.data.Address}</p></div>`;
                                    infowindow.setContent(contentString);
                                    var markerDonkhach = new google.maps.Marker({
                                        position: value.latLng,
                                        map: this.map,
                                        icon: '../../assets/images/carIcon/donkhach.png',
                                        title: `Đón khách`
                                    });
                                    markerDonkhach.addListener('click', function() {
                                        infowindow.open(this.map, markerDonkhach);
                                    });
                                    markerDonkhach.setMap(this.map);
                                    this._listpointdontrakhach.push(markerDonkhach);
                                    break;
                                case 'trakhach':
                                    contentString = `<div><h6 class="text-center">Trả khách</h6><p>Thời điểm: ${value.TimeUpdate}</p><p>Địa chỉ: ${rep.data.Address}</p></div>`;
                                    infowindow.setContent(contentString);
                                    var markerTraKhach = new google.maps.Marker({
                                        position: value.latLng,
                                        map: this.map,
                                        icon: '../../assets/images/carIcon/trakhach.png',
                                        title: `Trả khách: Thời điểm ${value.TimeUpdate} Địa điểm: ${rep.data.Address}`
                                    });
                                    markerTraKhach.addListener('click', function() {
                                        infowindow.open(this.map, markerTraKhach);
                                    });
                                    markerTraKhach.setMap(this.map);
                                    this._listpointdontrakhach.push(markerTraKhach);
                                    break;
                            }

                            // console.log(rep);

                        }
                    });


                }

                // getAddress(lat:number,lng:number):string{
                //     let result:string = '';
                //     this.http.$http.get(`http://geocode.adsun.vn/Geocode/GetAddress?lat=${lat}&lng=${lng}`).then((rep:any)=>{
                //         if(rep.data.Status == 1){
                //             // console.log(rep);
                //              result = rep.data.Address;
                //         }
                //     });
                //     return result;
                //     // return 'name';
                // }



            }

        }
    }
}