/**
 * Created by NPPRO on 23/11/2016.
 */
module App{
    export module Shared{
        export module Library{
            export class PointTimdothatlac extends Point{
                constructor(map_:google.maps.Map,rad:any, pos:any, id:any, name:any,_data:any,address:any){
                    super(map_,rad,pos,id,name,_data);
                    this.marker = new google.maps.Marker({
                        position:this.map.getCenter(),
                        title: this.name,
                        // map: this.map,
                        icon:'../../assets/images/map/iconPointCricle.png',
                        // labelContent: this.name,
                        // labelAnchor: new google.maps.Point(22, 0),
                        // labelClass: "labelContentCircle", // the CSS class for the label
                        // labelStyle: {opacity: 0.75},
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                    }) ;
                    this.cricle = new google.maps.Circle({
                        center: this.map.getCenter(),
                        radius: this.radius,
                        strokeColor: "#FF6B6B",
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: "#FF6B6B",
                        fillOpacity: 0.35,
                        // map: this.map
                    });
                }
                // marker = {
                //     position: this.map.getCenter(),
                //     title: this.name,
                //     map: this.map,
                //     icon:'../../assets/images/map/iconPointCricle.png',
                //     // labelContent: this.name,
                //     // labelAnchor: new google.maps.Point(22, 0),
                //     // labelClass: "labelContentCircle", // the CSS class for the label
                //     // labelStyle: {opacity: 0.75},
                //     draggable: true
                // };
                hidemenu(){
                    this.marker.setMap(this.map);
                    this.cricle.setMap(this.map);
                    // this.menu.setMap(null);
                    this.marker.setDraggable(false);

                }
                show(){
                    var infowindow = new google.maps.InfoWindow({
                        content: this.marker.getTitle(),
                        position: this.marker.getPosition()
                    });

                    this.marker.setMap(this.map);
                    infowindow.open(this.map, this.marker);
                    // this.marker.addListener('click', function() {
                    //     infowindow.open(this.map, this.marker);
                    // });
                    this.cricle.setMap(this.map);
                    // /infowindow.setMap(this.map);
                    this.initialize();
                }

                initialize(){
                    /*thong tin menu*/
                    this.menu=this.createMenu();
                    this.menu.setMapInstance(this.map);
                    /*thông tin điểm (tên hiển thị trên bản đồ)*/

                    /*cài đặt các event*/

                    /*event left click của marker (hiển thị bảng thông tin điểm )*/
                    this.eventClickMarker = google.maps.event.addListener(this.marker, 'click',(ev:any)=>{

                    });

                    // this.eventRightClickMarker = google.maps.event.addListener(this.cricle, 'rightclick',(ev:any)=>{
                    //     this.menu.show(ev.latLng);
                    // });

                    this.eventdragend=google.maps.event.addListener(this.marker, 'drag',(ev:any)=>{
                        this.cricle.setCenter(ev.latLng);
                        //alert(ev.latLng);
                    });
                    this.eventdrag=google.maps.event.addListener(this.marker, 'dragend',(ev:any)=>{
                        this.cricle.setCenter(ev.latLng);
                        this.positionDrag=ev.latLng;
                        this.position = ev.latLng;
                        // this.onEdit(this);
                        // alert(ev.latLng)
                    });

                    //gọi sự kiện cho cho menu circle
                    // this.menu.menuEventTrigger= (name:any)=>{
                    //     switch (name) {
                    //         case 'editPointCircle':
                    //             this.onEdit(this);
                    //             break;
                    //         case 'deleteCircle':
                    //             this.onDelete(this);
                    //             break;
                    //         case 'hideCircle':
                    //             this.hide();
                    //             break;
                    //     }
                    //     this.menu.hide();
                    // };
                }
            }
        }

    }

}