/**
 * Created by Tuantv on 05/18/2016.
 */
/// <reference path="../library.ts"/>
module App{
    export module Shared{
        export module Library{
            export class CarManagerTrip {
                protected cfg:CarConfig;
                private listArray:Array<any>;
                private onChange:(event:any)=>any;// sự kiện xảy ra khi thay đổi record
                private current=0;/*lưu trữ vị trí hiện tại khi cho xe chạy trong hành trình*/
                private autoPlay:any;
                private mode:any;
                public totalCarKmTrip:number;// tổng km hành trình theo xung
                public totalRecord:number;// tổng recode hành trìhh
                private carObject:any;
                private tripView:any;
                private speedPlay:any;
                private lockDraw:boolean;
                private listStop:Array<any>;
                public xematchiakhoa:boolean;
                //this.carObject = new carUtility.baseCar(this.cfg);// đối tượng hiển thị trên bản đồ
                //this.tripView = new carUtility.tripView(this.cfg.map);/*vẽ hành trình lên bản đồ*/
                constructor(config:CarConfig){
                    this.listArray=[];
                    this.listStop=[];
                    this.cfg = config;
                    this.onChange=undefined;// sự kiện xảy ra khi thay đổi record
                    this.carObject = new App.Shared.Library.BaseCar(this.cfg);// đối tượng hiển thị trên bản đồ
                    this.tripView = new App.Shared.Library.TripView(this.cfg);/*vẽ hành trình lên bản đồ*/
                    this.current=0;/*lưu trữ vị trí hiện tại khi cho xe chạy trong hành trình*/
                    this.autoPlay;
                    this.mode;
                    this.totalCarKmTrip=0;// tổng km hành trình theo xung
                    this.totalRecord=0;// tổng recode hành trình
                    this.speedPlay=1000;
                    this.lockDraw=false;
                }
                /*load thông tin hành trình từ trên api xuống*/
                /*load thông tin hành trình từ trên api xuống*/
                load = (data:any,serial:string,CarId:any)=> {
                    var defer:any;
                    if (this.cfg.q != undefined)
                        defer = this.cfg.q.defer();
                    var self = this;
                    this.totalRecord = 0;
                    this.totalCarKmTrip = 0;
                    this.clear();
                    this.pause();
                    this.listStop=[];
                    var tongkmCardi = 0;
                    var tongkmGpsdi = 0;
                    // kiểm tra record đầu tiên cho thời gian dừng đỗ
                    var checkrecoder=false;
                    var timeStartStop="";
                    var latLng:any;
                    var oldLatLng:any;
                    var latLngcurren:any;
                    var StatusStopStart:any;
                    var indexPaint:any;
                    //lưu km đầu tiên
                    var kmStart:any;
                    var tongkmtichluy:any;
                    //lưu tọa độ để loại bỏ lúc tắt máy
                    var latLngPaint:any;
                    // kiểm tra xem tất cả record nó có mất chìa khóa thiệt ko
                    var checkxematchiakhoa=false;
                    angular.forEach(data, function (record, idx) {
                        if(record.MachineStatus)
                        checkxematchiakhoa=record.MachineStatus;

                    });
                    angular.forEach(data, function (record, idx) {
                        if(idx==0)
                        {
                            kmStart=record.TotalDistance;
                            tongkmtichluy=0;
                        }
                        else{
                            tongkmtichluy=record.TotalDistance-kmStart;
                        }

                        if (self.listArray.length > 0) {
                            tongkmCardi = record.TotalCarKm - self.listArray[0].totalCarKm;
                            tongkmGpsdi = record.TotalGpsKm - self.listArray[0].totalGpsKm;
                        }
                        var angle = 0;
                        latLngcurren=new google.maps.LatLng(record.Location.Lat, record.Location.Lng);
                        if (oldLatLng ==null || oldLatLng ==undefined) {
                            angle = 0;
                            oldLatLng = latLngcurren;
                        }
                        else {
                            try{

                            angle = google.maps.geometry.spherical.computeHeading(oldLatLng,latLngcurren);
                            oldLatLng=latLngcurren;
                                // console.log(angle);
                            }
                            catch(ex){
                                console.log('lỗi không lấy được tọa độ');
                                angle=record.Angle;
                            }
                        }

                        if(record.Location.Lat!=0) {
                            var imageurl="../../assets/images/carIcon/run_car.png";
                            if (record.GpsStatus == false) {
                                imageurl = '../../assets/images/carIcon/Lostgps_car.png';
                            }
                            else {
                                if (record.Speed <= 0 || record.MachineStatus==false) {
                                    imageurl = '../../assets/images/carIcon/Stop_car.png';
                                }
                                else {
                                    imageurl = '../../assets/images/carIcon/run_car.png';
                                }

                            }
                            //kiểm tra vận tốc và mở máy để lưu tọa độ
                            if(record.Speed>0 && record.MachineStatus==true)
                            {
                                latLngPaint =new google.maps.LatLng(record.Location.Lat, record.Location.Lng);
                            }
                            if(record.Speed==0 && record.MachineStatus==true)
                            {
                                if(checkrecoder==false ){
                                    latLngPaint =new google.maps.LatLng(record.Location.Lat, record.Location.Lng);
                                }
                                else{
                                    latLngPaint =latLngPaint;
                                }
                            }
                            if(record.MachineStatus==false)
                            {

                                if(checkrecoder==false ){
                                    latLngPaint =new google.maps.LatLng(record.Location.Lat, record.Location.Lng);
                                }
                                else{
                                    latLngPaint =latLngPaint;
                                }


                            }

                            self.listArray.push({
                                serial: serial,
                                carId: CarId,
                                latLng: latLngPaint,
                                angle: angle,
                                speed: record.Speed,
                                Distance: tongkmtichluy,
                                totalCarKm: record.TotalDistance,
                                gpsStatus: record.GpsStatus?"Có":"Không",
                                doorStatus: record.DoorStatus?"Đóng":"Mở",
                                freezerStatus: record.AirMachineStatus?"Mở":"Tắt",
                                fuel: record.Fuel,
                                powerBattery: record.Power,
                                MachineStatus:record.MachineStatus?"Mở":"Tắt",
                                tempernature: record.Tempernature,
                                timeUpdate: self.AdapterDate(record.TimeUpdate),
                                kmCarGo: tongkmCardi,
                                kmGpsGo: tongkmGpsdi,
                                index: self.listArray.length,
                                imageData:imageurl,
                                Address:record.Location.Address,
                                pointEnd:null
                            });
                            //xet các điểm dừng
                            var Pointend:Array<any>=[];
                            if(record.Speed==0 && record.MachineStatus==true)
                            {
                                if(checkrecoder==false ){
                                    StatusStopStart=0;
                                    indexPaint=self.listArray.length-1;
                                    checkrecoder=true;
                                    timeStartStop=record.TimeUpdate;
                                    latLng =new google.maps.LatLng(record.Location.Lat, record.Location.Lng);
                                }
                            }
                            if(record.Speed>0 && record.MachineStatus==true)
                            {
                                if(checkrecoder==true ){
                                    checkrecoder=false;
                                    var timeSpane=self.ChangeTimeSpan(timeStartStop,record.TimeUpdate);
                                    if(timeSpane >0) {
                                        self.listStop.push({latLng: latLng, title:  timeSpane+" Phút. Thời điểm: "+self.AdapterDate(timeStartStop),status:StatusStopStart,index:indexPaint})
                                    }

                                }

                            }
                            if(record.MachineStatus==false)
                            {

                                if(checkrecoder==false ){
                                    StatusStopStart=1;
                                    indexPaint=self.listArray.length-1;
                                    if(checkxematchiakhoa)
                                    checkrecoder=true;
                                    timeStartStop=record.TimeUpdate;
                                    latLng =new google.maps.LatLng(record.Location.Lat, record.Location.Lng);
                                }
                                else{
                                    StatusStopStart=1;
                                }


                            }

                        }
                    });

                    angular.forEach(self.listStop, function (value, iex) {
                                self.listArray[value.index].pointEnd=value;
                    });

                    var distance_tmp=0;
                    var latlng_tmp:any=null;
                    var lostKeyCount=0;
                    angular.forEach(data,function (r, i) {
                        var tmp=new google.maps.LatLng(r.Location.Lat, r.Location.Lng);
                       if(latlng_tmp!=null){
                           distance_tmp+=google.maps.geometry.spherical.computeDistanceBetween (tmp, latlng_tmp);
                       }
                       latlng_tmp=tmp;
                        if(!r.MachineStatus)
                            lostKeyCount++;
                    });
                    if(lostKeyCount==data.length&&distance_tmp>2000)
                        this.xematchiakhoa=true;
                    else this.xematchiakhoa=false;

                    self.setMode.bind(self);
                    self.setMode("system");
                    self.totalCarKmTrip = self.listArray[self.listArray.length - 1].totalCarKm - self.listArray[0].totalCarKm;
                    self.totalRecord = self.listArray.length;
                    if(defer!=undefined)
                    {
                        defer.resolve(self);
                    }
                    if(defer!=undefined)
                        return defer.promise;
                    return undefined;

                };
                //hàm chuyển thời gian
                AdapterDate=(time:any)=>{
                    var year:string = time.slice(0,4);
                    var month:string = time.slice(5,7);
                    var date:string = time.slice(8,10);

                    var min:string = time.slice(11,19);

                    var tt:any = min +' ' +date + '/' + month + '/' + year ;
                    return tt;
                }
                //hàm tính thời gian
                ChangeTimeSpan=(dateS:any,dateE:any)=> {
                    var date1 = new Date(dateS);
                    var date2 = new Date(dateE);
                    var diff = date2.getTime() - date1.getTime();

                    var days = Math.floor(diff / (1000 * 60 * 60 * 24));
                    diff -= days * (1000 * 60 * 60 * 24);

                    var hours = Math.floor(diff / (1000 * 60 * 60));
                    diff -= hours * (1000 * 60 * 60);

                    var mins = Math.floor(diff / (1000 * 60));
                    diff -= mins * (1000 * 60);

                    var seconds = Math.floor(diff / (1000));
                    diff -= seconds * (1000);


                    var resuflt:any = (hours*60) + mins;
                    return resuflt;
                }
                setMode=(mode:any)=> {

                            this.tripView.clear();
                            if (this.listArray.length > 0) {
                                this.carObject.latlng = this.listArray[0].latLng;
                                this.carObject.iconInfo.icon.src = '../../assets/images/carIcon/Stop_car.png';
                                this.carObject.serial = this.listArray[0].serial;
                                this.carObject.deviceLabel = this.listArray[0].carId;
                                this.carObject.iconInfo.angle = this.listArray[0].angle;
                                //this.carObject.buildCarViewHtml();
                                this.carObject.setMap(this.cfg.map);
                                this.tripView.push(this.listArray[0]);
                            }
                            this.current = 0;

                };
             clear=()=> {
                 this.pause();
                 this.tripView.clear();

                 this.carObject.setMap(null);
                 var len = this.listArray.length;
                 for (var i = 0; i < len; i++)
                     this.listArray.pop();
             };
                //chức năng next
             next=()=> {
                 this.current++;
                 if (this.current >= this.listArray.length) {
                     this.current = this.listArray.length - 1;
                     this.pause();
                 }
                 this.draw(this, "next", 0);
             };
                //chức năng prev
                prev=()=> {
                    this.current--;
                    if (this.current <= 0)
                        this.current = 0;
                    this.draw(this, "prev", 0);
                };
                //chức năng dừng (pause)
                pause=()=> {
                    if (this.autoPlay != undefined) {
                        clearInterval(this.autoPlay);
                        this.autoPlay = undefined;
                    }
                };
                // chức năng play
                play=()=> {

                    this.pause();
                    var self = this;
                    this.autoPlay = setInterval(function () {
                        self.next();
                    }, this.speedPlay)
                };
                setSpeed=(s:any)=>{
                    // this.speedPlay = s <= 100 ? 100 : s;
                    this.speedPlay = s;
                    if (this.autoPlay != undefined)
                        this.play();
                };
                seek=(index:any)=> {
                    if (index > this.listArray.length || index < 0) return false;
                    this.current = index;
                    this.tripView.clear();
                    this.draw(this, "seek", index);
                };
                replay=()=> {
                    this.current = 0;
                    this.tripView.clear();
                    this.draw(this, "seek", 0);
                };
                getCurrentRecord=()=> {
                    return this.listArray.length > this.current ? this.listArray[this.current] : null;
                };

                getPrev1Record=()=> {
                    return this.listArray.length > this.current ? this.listArray[this.current-1] : null;
                };
                //ham onchange
                onchangeRecode=(obj:any)=> {
                    return obj;
                };
                //hàm lấy hình
                //getimage=(speed:number,gps:boolean)=>{
                //    var urlimage = '';
                //    if (gps == false) {
                //        urlimage = '../../assets/images/carIcon/Lostgps_car.png';
                //    }
                //    else {
                //        if (speed <= 0) {
                //            urlimage = '../../assets/images/carIcon/Stop_car.png';
                //        }
                //        else {
                //            urlimage = '../../assets/images/carIcon/run_car.png';
                //        }
                //
                //    }
                //    return urlimage;
                //}
                private pct = 0;
                private numpart = 30;
                private start:any = new Date().getMilliseconds();
                private timer:any;
                animation(obj:any,current:any,steplat:number,steplng:number){

                    var lat = obj.carObject.latlng.lat() + steplat;
                    var lng = obj.carObject.latlng.lng() + steplng;

                    //if( t < 1) {
                        //console.log('chay animation');
                        obj.carObject.latlng = new google.maps.LatLng(lat,lng);
                        obj.carObject.iconInfo.icon.src = current.imageData;
                        obj.carObject.iconInfo.angle = current.angle;
                        if (obj.carObject.getMap() == null) obj.carObject.setMap(obj.cfg.map);
                        obj.carObject.rotate();
                        obj.carObject.draw();

                }
                draw(obj:any,to:any,seek:any){
                    while (this.lockDraw);
                    this.lockDraw = true;
                    try {
                        /*call hàm event */
                        console.log("Băt đầu 1 record : "+new Date().getMilliseconds());
                        this.start = new Date().getMilliseconds();
                        var current = obj.getCurrentRecord();
                        var old=obj.current>1?obj.getPrev1Record():null;
                        if (current == undefined || current == null) obj.pause();
                        if (obj.onChange != undefined)
                            obj.onChange(current);


                        if(old!=null){
                            obj.carObject.latlng = old.latLng;
                        }
                        else {
                            this.lockDraw = false;
                            return;
                        }
                        var linear = function linear(time:number, start:number, change:number, duration:number) {
                            return change * (time / duration) + start;
                        };
                        var easingDurationRatio = linear(this.speedPlay/(this.numpart),0, 1, 1000);
                        var steplat=(current.latLng.lat() - obj.carObject.latlng.lat())*easingDurationRatio;
                        var steplng=(current.latLng.lng() - obj.carObject.latlng.lng())*easingDurationRatio;
                        this.animation(obj,current,steplat,steplng);
                        var count=0;
                        clearInterval(this.timer);
                        this.timer = setInterval(()=>{
                            this.animation(obj,current,steplat,steplng);
                                console.log(count++);
                                if(count >=this.speedPlay/(this.numpart) ) clearInterval(this.timer);
                        },this.speedPlay/(this.numpart));

                        // obj.carObject.latlng = current.latLng;
                        // obj.carObject.iconInfo.icon.src = current.imageData;
                        // obj.carObject.iconInfo.angle = current.angle;
                        // if (obj.carObject.getMap() == null) obj.carObject.setMap(obj.cfg.map);
                        // obj.carObject.rotate();
                        // obj.carObject.draw();

                        // var timeloop = this.speedPlay/500;
                        // var timefunction = (new Date().getMilliseconds()) - this.start;
                        // var t = timefunction / 500;
                        // var easingDurationRatio = $.easing['swing'](t, timefunction, 0, 1, 500);
                        //
                        // var lat = obj.carObject.latlng.lat() + (current.latLng.lat() - obj.carObject.latlng.lat())*t;
                        // var lng = obj.carObject.latlng.lng() + (current.latLng.lng() - obj.carObject.latlng.lng())*t;
                        //
                        // obj.carObject.latlng = new google.maps.LatLng(lat,lng);
                        // obj.carObject.iconInfo.icon.src = current.imageData;
                        // obj.carObject.iconInfo.angle = current.angle;
                        // if (obj.carObject.getMap() == null) obj.carObject.setMap(obj.cfg.map);
                        // obj.carObject.rotate();
                        // obj.carObject.draw();



                        // while (t < 1){
                        //
                        // }
                        // if(obj.carObject.latlng !== undefined){
                        //     if(obj.carObject.latlng !== current.latLng){
                        //         if(timer !== undefined) clearInterval(timer);
                        //         // if (window.requestAnimationFrame) {
                        //         //     marker.AT_animationHandler = window.requestAnimationFrame(function() {animateStep(marker, startTime)});
                        //         // } else {
                        //         //     marker.AT_animationHandler = setTimeout(function() {animateStep(marker, startTime)}, 17);
                        //         // }
                        //         var timer = setInterval(()=>{
                        //             // this.pct += 0.002;
                        //
                        //             var timefunction = (new Date()).getMilliseconds() - this.start;
                        //             var t = timefunction / 500;
                        //
                        //
                        //             var easingDurationRatio = $.easing['swing'](t, timefunction, 0, 1, 1000);
                        //
                        //             var lat = obj.carObject.latlng.lat() + (current.latLng.lat() - obj.carObject.latlng.lat())*easingDurationRatio;
                        //             var lng = obj.carObject.latlng.lng() + (current.latLng.lng() - obj.carObject.latlng.lng())*easingDurationRatio;
                        //             obj.carObject.latlng = new google.maps.LatLng(lat,lng);
                        //             obj.carObject.iconInfo.icon.src = current.imageData;
                        //             obj.carObject.iconInfo.angle = current.angle;
                        //             if (obj.carObject.getMap() == null) obj.carObject.setMap(obj.cfg.map);
                        //             obj.carObject.rotate();
                        //             obj.carObject.draw();
                        //             if(t >= 1) clearInterval(timer)
                        //
                        //         },40);
                        //     }else{
                        //         return;
                        //     }
                        // }else{
                        //     obj.carObject.latlng = current.latLng;
                        //     obj.carObject.iconInfo.icon.src = current.imageData;
                        //     obj.carObject.iconInfo.angle = current.angle;
                        //     if (obj.carObject.getMap() == null) obj.carObject.setMap(obj.cfg.map);
                        //     obj.carObject.rotate();
                        //     obj.carObject.draw();
                        // }

                        switch (to) {
                            case "next":
                                obj.tripView.push(current);
                                break;
                            case "prev":
                                obj.tripView.pop();
                                break;
                            case "seek":
                                if (obj.tripView.length() > seek)
                                    obj.tripView.current(seek);
                                else {
                                    for (var i = obj.tripView.length(); i < seek; i++) {
                                        obj.tripView.push(obj.listArray[i]);
                                    }
                                }
                                break;
                        }
                    }
                    catch (message) {

                    }
                    this.lockDraw = false;

                }

            }

        }
    }
}