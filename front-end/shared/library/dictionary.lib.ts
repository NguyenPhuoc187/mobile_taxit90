/**
 * Created by user on 04/26/2016.
 */
module App{
    export module Shared{
        export module Library{
            export interface IDictionary<TKey,TValue>{
                Add(key:TKey,val:TValue):void;
                Remove(key:TKey):void;
                ContainKey(key:TKey):boolean;
                Values():Array<TValue>;
                Get(key:TKey):TValue;
                Key():Array<TKey>;
                Clear():void;
            }
            export interface IElement<Tkey,TValue>{
                Key:Tkey;
                Value:TValue;
            }

            export class Dictionary<TKey,TValue> implements IDictionary<TKey,TValue>{
                Key():Array<TKey> {
                    var result=Array<TKey>();
                    $.each(this._array,(idx:number,val:IElement<TKey,TValue>)=>{
                       result.push(val.Key);
                    });
                    return result;
                }
                Add(key:TKey, val:TValue):void {
                    if(this.ContainKey(key))
                        throw "Already key added";
                    this._array.push({Key:key,Value:val});
                }

                Remove(key:TKey):void {
                    var index=this.IndexOf(key);
                    if(index>-1)
                        this._array.splice(index,1);
                }

                ContainKey(key:TKey):boolean {
                    return this.IndexOf(key)>-1;
                }

                Values():Array<TValue> {
                    var result=Array<TValue>();
                    $.each(this._array,(idx:number,val:IElement<TKey,TValue>)=>{
                        result.push(val.Value);
                    });
                    return result;
                }

                Get(key:TKey):TValue {
                    var index=this.IndexOf(key);
                    if(index==-1) return null;
                    return this._array[index].Value;
                }

                Clear():void {
                    while (this._array.length>0){
                        this._array.pop();
                    }
                }

                private IndexOf(key:TKey):number{
                    return Dictionary.IndexOf(this._array,(m:IElement<TKey,TValue>)=>{return m.Key==key;});
                }

                private static IndexOf(source:Array<any>,fun:(val:any)=>boolean){
                    for(var i=0;i<source.length;i++){
                        if(fun(source[i])){
                            return i;
                        }
                    }
                    return -1;
                }
                private  _array =Array<IElement<TKey,TValue>>();


            }
        }
    }
}
