function splitgif(imggif, action, tmpCanvas)
{
	var hdr;
    var frame = null; 
	var transparency = null;
    var delay = null;
    var disposalMethod = null;
    var lastDisposalMethod = null;
	var frames = [];
	
	var doGet = function() {
      var h = new XMLHttpRequest();
      h.overrideMimeType('text/plain; charset=x-user-defined');
      h.onload = function(e) {
        //doLoadProgress(e);
        // TODO: In IE, might be able to use h.responseBody instead of overrideMimeType.
        stream = new Stream(h.responseText);
        parseGIF(stream, handler);
      };
      
      h.onerror = function() { };
      h.open('GET', imggif.src, true);
      h.send();
    };
	var withProgress= function(fn, draw)
	{
		return function(block){ fn(block);};
	};
	
	
	
	var clear = function() {
      transparency = null;
      delay = null;
      lastDisposalMethod = disposalMethod;
      disposalMethod = null;
      frame = null;
      //frame = tmpCanvas.getContext('2d');
    };
	
	
	
	 var doNothing = function(){};
	 
	 var doImg = function(img) {
	 //var tmpCanvas=document.getElementById('ctest');
      if (!frame) frame = tmpCanvas.getContext('2d');
      var ct = img.lctFlag ? img.lct : hdr.gct; // TODO: What if neither exists?

      var cData = frame.getImageData(img.leftPos, img.topPos, img.width, img.height);

      img.pixels.forEach(function(pixel, i) {
        // cData.data === [R,G,B,A,...]
        if (transparency !== pixel) { // This includes null, if no transparency was defined.
          cData.data[i * 4 + 0] = ct[pixel][0];
          cData.data[i * 4 + 1] = ct[pixel][1];
          cData.data[i * 4 + 2] = ct[pixel][2];
          cData.data[i * 4 + 3] = 255; // Opaque.
        } else {
          // TODO: Handle disposal method properly.
          // XXX: When I get to an Internet connection, check which disposal method is which.
          if (lastDisposalMethod === 2 || lastDisposalMethod === 3) {
            cData.data[i * 4 + 3] = 0; // Transparent.
            // XXX: This is very very wrong.
          } else {
            // lastDisposalMethod should be null (no GCE), 0, or 1; leave the pixel as it is.
            // assert(lastDispsalMethod === null || lastDispsalMethod === 0 || lastDispsalMethod === 1);
            // XXX: If this is the first frame (and we *do* have a GCE),
            // lastDispsalMethod will be null, but we want to set undefined
            // pixels to the background color.
          }
        }
      });
      frame.putImageData(cData, img.leftPos, img.topPos);
      // We could use the on-page canvas directly, except that we draw a progress
      // bar for each image chunk (not just the final image).
      //ctx.putImageData(cData, img.leftPos, img.topPos);
    };
	
	var doGCE = function(gce) {
      pushFrame();
      clear();
      transparency = gce.transparencyGiven ? gce.transparencyIndex : null;
      delay = gce.delayTime;
      disposalMethod = gce.disposalMethod;
      // We don't have much to do with the rest of GCE.
    };
	
	 var doHdr = function(_hdr) {
      hdr = _hdr;
    };
	
	var pushFrame = function() {
	    if (!frame) return;
	    var data=frame.getImageData(0, 0, hdr.width, hdr.height);
	    frames.push({
	        data: data,
	        url: tmpCanvas.toDataURL(),
	        delay: delay
	    });
    };
	
	var handler = {
      hdr: withProgress(doHdr),
      gce: withProgress(doGCE),
      com: withProgress(doNothing), // I guess that's all for now.
      app: {
       // TODO: Is there much point in actually supporting iterations?
        NETSCAPE: withProgress(doNothing)
      },
      img: withProgress(doImg, true),
      eof: function(block) {
        //toolbar.style.display = '';
          pushFrame();

          action(frames);
       
      }
    };
	doGet();
}