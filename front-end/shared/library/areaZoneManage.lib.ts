/**
 * Created by phamn on 01/11/2016.
 */
/// <reference path="areaCustomZone.lib.ts" />
module App{
    export module Shared{
        export module Library{

            export class AreaCustomZoneManage extends AreaManage{
                companyId:number;
                beginTime:string;
                endTime:string;
                areacurent:any;
                senddatazone:(item:any)=>any;
                constructor(map:google.maps.Map,$q:ng.IQService, _companyId:number, public $http: ng.IHttpService, public global: App.Shared.GlobalService){
                    super(map,$q,_companyId,$http,global);
                    // this.companyId = _companyId;
                    // this.beginTime = _beginTime;
                    // this.endTime = _endTime;
                }
                //vẽ vùng
                areaDraw() {
                    //var $this=this;
                    if(this.areacurent !== undefined){
                        this.areaDelete();
                    }
                    var data_zone:any = new App.Shared.Library.AreaCustomZone(this.mainMap);
                    data_zone.infoArea();
                    data_zone.onShow = this.areaAdd.bind(this);
                    this.areacurent = data_zone;

                };
                areaAdd(a:any){
                    this.areaShowFrom(a).then((req:any)=> {
                        console.log(req);
                        var item:any = req;
                        if (this.senddatazone != undefined && this.senddatazone != null) {
                            this.senddatazone(item);
                        }
                    });
                }

                areaShowFrom(data:any) {

                    //var $this = this;
                    var defer = this.q.defer();

                    var arraypoint:any = [];
                    // console.log(data.polyline.j);
                    angular.forEach(data.polyline.b, function (d:any) {
                        var dt:any = {};
                        dt.Lat = d.lat();
                        dt.Lng = d.lng();
                        dt.Address = 'null';
                        arraypoint.push(dt);
                    });
                    defer.resolve(arraypoint);
                    // var companyId = this.companyId;
                    return defer.promise;
                }
                areaGetData(area:any){
                    var defer = this.q.defer();
                    var arraypoint:any = [];
                    angular.forEach(area.polyline.b, function (d:any) {
                        var dt:any = {};
                        dt.Lat = d.lat();
                        dt.Lng = d.lng();
                        dt.Address = 'null';
                        arraypoint.push(dt);
                    });
                    var result = arraypoint;
                    // console.log(arraypoint);
                    defer.resolve(result);
                }
                areaDelete(){
                    if(this.areacurent !== undefined){
                        this.areacurent.polygons.setMap(null);
                    }
                }
                updateParametter(_companyId:number, beginTime:string, endTime:string){
                    this.companyId = _companyId;
                    this.beginTime = beginTime;
                    this.endTime = endTime;
                }
                //note: hàm lấy data sau khi thành công gửi dữ liệu
                sendDataZone(){
                    return this.senddatazone;
                }
                    //xoá vùng
            }
        }

    }
}