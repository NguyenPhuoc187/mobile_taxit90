/**
 * Created by phamn on 21/04/2016.
 */

module App{
    export module Shared {
        export module Library{
                export interface IExportExcel {
                    tableHtml: string;
                    infoTopleftHtml: string;
                    infoTopRightHtml: string;
                    infoHeaderHtml: string;
                    infoHeaderChildrenHtml: string;
                    infoColumnHtml: Array<any>;
                    infoRowHtml: Array<any>;
                    infoBotomLeftHtml: string;
                    infoBottomRightHtml: string;

                }
                export interface IStyle{
                    font_size?: any;
                    color?: any;
                    width?: any;
                    font_style?: any;
                    border?: any;
                    border_style?: any;
                    border_bottom?: any;
                    border_color?: any;
                    border_left?: any;
                    align_content?: any;
                    background_color?: any;
                    display?: any;
                    margin?: any;
                    padding?: any;
                    text_align?: any;
                    transform?: any;
                    z_index?: any;
                    transition?: any;
                    border_image?: any;
                    icon?: any;
                    position?: any;
                    box_shadow?: any;
                    font_family?: any;
                    background_image?: any;
                    font_weight?: any;

                }
                // export interface IInfoExport{
                //     content: any;
                //     getStyle: IStyle;
                //     countrow: any;
                // }
                export interface IExportExcellScope extends ng.IScope{
                    
                }
                export class ExportExcell{
                    constructor(){
                        this.a={
                            tableHtml: "",
                        infoTopleftHtml: "",
                        infoTopRightHtml: "",
                        infoHeaderHtml: "",
                        infoHeaderChildrenHtml: "",
                        infoColumnHtml: Array<any>(),
                        infoRowHtml: Array<any>(),
                        infoBotomLeftHtml: "",
                        infoBottomRightHtml: ""
                        };
                    }
                    a: IExportExcel;
                    public buildStyle(getStyle:IStyle):any {

                        var style = getStyle;
                        var result = "style='";
                        if(style!==null){
                            if(style.font_size!==undefined){
                                result+='font-size:'+style.font_size+'px;';
                            }
                            if(style.color!==undefined){
                                result+='color:'+style.color+';';
                            }
                            if(style.width!==undefined){
                                result+='width:'+style.width+';';
                            }
                            if(style.font_style!==undefined){
                                result+='font-style:'+style.font_style+';';
                            }
                            if(style.font_weight!==undefined){
                                result+='font-weight:'+style.font_weight+';';
                            }
                            if(style.border!==undefined){
                                result+='border:'+style.border+';';
                            }
                            if(style.border_style!==undefined){
                                result+='border-style:'+style.border_style+';';
                            }
                            if(style.border_bottom!==undefined){
                                result+='border-bottom:'+style.border_bottom+';';
                            }
                            if(style.border_color!==undefined){
                                result+='border-color:'+style.border_color+';';
                            }
                            if(style.border_left!==undefined){
                                result+='border_left:'+style.border_left+';';
                            }
                            if(style.align_content!==undefined){
                                result+='align-content:'+style.align_content+';';
                            }
                            if(style.background_color!==undefined){
                                result+='background-color:'+style.background_color+';';
                            }
                            if(style.display!==undefined){
                                result+='display:'+style.display+';';
                            }
                            if(style.margin!==undefined){
                                result+='margin:'+style.margin+';';
                            }
                            if(style.padding!==undefined){
                                result+='padding:'+style.padding+';';
                            }
                            if(style.text_align!==undefined){
                                result+='text-align:'+style.text_align+';';
                            }
                            if(style.transform!==undefined){
                                result+='transform:'+style.transform+';';
                            }
                            if(style.z_index!==undefined){
                                result+='z-index:'+style.z_index+';';
                            }
                            if(style.transition!==undefined){
                                result+='transition:'+style.transition+';';
                            }
                            if(style.border_image!==undefined){
                                result+='border-image:'+style.border_image+';';
                            }
                            if(style.icon!==undefined){
                                result+='icon:'+style.icon+';';
                            }
                            if(style.position!==undefined){
                                result+='position:'+style.position+';';
                            }
                            if(style.box_shadow!==undefined){
                                result+='box-shadow:'+style.box_shadow+';';
                            }
                            if(style.font_family!==undefined){
                                result+='font-family:'+style.font_family+';';
                            }
                            if(style.background_image!==undefined){
                                result+='background-image:'+style.background_image+';';
                            }
                        }
                        result+="'";
                        return result;
                    };

                    public AddInfoTopLeft(content:any,countrow:any,getStyle:IStyle){
                        try{
                            var style = this.buildStyle(getStyle==null?this.GetStyleTopLeftDefault():getStyle);
                            this.a.infoTopleftHtml = "<td colspan='"+ countrow+"' "+style+'>'+content+'</td>';
                        }
                        catch (e){
                            console.error("AddInfoTopLeft: "+e.result);
                        }
                    };

                    public GetStyleTopLeftDefault():IStyle {
                        var result= {
                            color:'#D14719',
                            font_size:'12px',
                            text_align:'left'
                        };
                        return result;
                    };
                    //tạo style default cho tiêu đề BottomLeft
                    public GetStyleBottomLeftDefault ():IStyle{
                    var result= {
                        color:'#D14719',
                        font_size:'12px',
                        text_align:'left'
                    };
                    return result;
                    };
                    //tạo style default cho tiêu đề BottomRight
                    public  GetStyleBottomRightDefault ():IStyle{
                        var result= {
                            color:'#D14719',
                            font_size:'12px',
                            text_align:'right'
                        };
                        return result;

                     };
                    //tạo style default cho tiêu đề TopRight
                    public  GetStyleTopRightDefault ():IStyle{
                        var result= {
                            color:'#D14719',
                            font_size:'12px',
                            font_weight:'600',
                            text_align:'right'
                        };
                        return result;
                    };
                    public GetStyleInfoHeaderDefault ():IStyle{
                        var result= {
                            color:'black',
                            text_align:'center',
                            font_size:15,
                            font_weight:'500',
                            // border: 'thin solid black'
                        };
                        return result;
                     };
                    // khai báo style mặc định cho tiêu đề từng cột
                    public GetStyleInfoColumnDefault ():IStyle {
                        var result= {
                            color:'black',
                            text_align:'center',
                            font_size:'11px',
                            font_weight:'400',
                            background_color:'#9DAFC1',
                            width:'auto',
                            border: 'thin solid black'
                        };
                        return result;
                    };
                    // khai báo style mặc định cho nội dung
                    public GetStyleInfoRowDefault ():IStyle {
                        var result= {
                            color:'Navy',
                            font_size:'15px',
                            text_align: 'left',
                            border: 'thin solid black'
                        };
                        return result;
                    };
                    public  AddInfoTopRight (content:any,countrow:any,getStyle:IStyle):void{

                            try{

                                var style=this.buildStyle(getStyle== null?this.GetStyleTopRightDefault():getStyle);

                                this.a.infoTopRightHtml="<td colspan='"+countrow+"'  "+style+'>'+content+'</td>';
                            }
                            catch(e){
                                console.error("AddInfoTopRight: "+e.result);
                            }
                    };
                    public AddInfoHeader (content:any,countrow:any,getStyle:IStyle):void{

                        try{

                            var style=this.buildStyle(getStyle==null?this.GetStyleInfoHeaderDefault():getStyle);

                            this.a.infoHeaderHtml="<td colspan='"+countrow+"' "+style+'>'+content+'</td>';
                        }
                        catch(e){
                            console.error("AddInfoHeader: "+e.result);
                        }
                    };

                    public AddInfoHeaderChildren (content:any,countrow:any,getStyle:IStyle):void{

                        try{

                            var style=this.buildStyle(getStyle==null?this.GetStyleInfoHeaderDefault():getStyle);

                            this.a.infoHeaderChildrenHtml="<td colspan='"+countrow+"' "+style+'>'+content+'</td>';
                        }
                        catch(e){
                            console.error("AddInfoHeaderChildren: "+e.result);
                        }
                    };

                    public AddInfoColumn (content:any,getStyle:IStyle):void{

                        try{

                            var style=this.buildStyle(getStyle==null?this.GetStyleInfoColumnDefault():getStyle);

                            this.a.infoColumnHtml.push("<td "+style+'>'+content+'</td>');
                        }
                        catch(e){
                            console.error("AddInfoColumn: "+e.result);
                        }
                    };
                    public  AddInfoRow (data:Array<any>,getStyle:IStyle):void{

                        try{
                            /*convert json to array*/
                            var arr = $.map(data, function(el:any) { return el; });

                            var style=this.buildStyle(getStyle==null?this.GetStyleInfoRowDefault():getStyle);
                            var content="";
                            for(var i=0;i<arr.length;i++){
                                content+="<td " + style + ">"+arr[i]+"</td>";
                            }
                            // this.a.infoRowHtml.push("<tr "+style+'>'+content+'</tr>');
                            this.a.infoRowHtml.push('<tr>'+content+'</tr>');
                        }
                        catch(e){
                            console.error("AddInfoRow: "+e.result);
                        }
                    };
                    public AddInfoBottomLeft(content:any,countrow:any,getStyle:IStyle):void{

                        try{

                            var style=this.buildStyle(getStyle==null?this.GetStyleBottomLeftDefault():getStyle);

                            this.a.infoBotomLeftHtml="<td colspan='"+countrow+"' "+style+'>'+content+'</td>';
                        }
                        catch(e){
                            console.error("AddInfoBottomLeft: "+e.result);
                        }

                     };
                    public  AddInfoBottomRight (content:any,countrow:any,getStyle:IStyle):void{

                        try{

                            var style=this.buildStyle(getStyle==null?this.GetStyleBottomRightDefault():getStyle);

                            this.a.infoBottomRightHtml="<td colspan='"+countrow+"'  "+style+'>'+content+'</td>';
                        }
                        catch(e){
                            console.error("AddInfoBottomRight: "+e.result);
                        }

                     };
                    public  buildTable (id:any):void{

                        var tableHtml="<table id='t_"+id+"'>";
                        /*add top*/
                        // tableHtml+="<tr>"+this.a.infoTopleftHtml+this.a.infoTopRightHtml+"</tr>";
                        /*add header*/
                        tableHtml+="<caption>"+this.a.infoHeaderHtml+"</caption>";
                        tableHtml+="<caption>"+this.a.infoHeaderChildrenHtml+"</caption>";

                        /*add column*/
                        tableHtml+="<tr>";
                        for(var i=0;i<this.a.infoColumnHtml.length;i++)
                            tableHtml+=this.a.infoColumnHtml[i];
                        tableHtml+="</tr>";

                        /*add data*/

                        for(var i=0;i<this.a.infoRowHtml.length;i++)
                            tableHtml+=this.a.infoRowHtml[i];

                        /*add bottom*/
                        tableHtml+="<tr>"+this.a.infoBotomLeftHtml+this.a.infoBottomRightHtml+"</tr>";
                        tableHtml+="</table>";

                        /*add to page*/
                        $('#'+id).html(tableHtml);

                };


                }


        }
    }
}