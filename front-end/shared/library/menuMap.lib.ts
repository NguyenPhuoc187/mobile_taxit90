/**
 * Created by phamn on 19/05/2016.
 */
/// <reference path="dictionary.lib.ts" />
/// <reference path="car.lib.ts" />
/// <reference path="contextmenu.lib.ts" />
/// <reference path="BaseCar.lib.ts" />
module App{
    export module Shared{
        export module Library{
            export class MenuMap extends google.maps.OverlayView {
                public contextMenu:any;
                public contextHighwayMenu:any;
                public contextTollboothMenu:any;
                protected cfg:any;

                constructor(config:any) {
                    super();
                    this.cfg = config;
                    //this.contextMenu = App.ContextMenu(null,null,null);
                    this.contextMenu = new ContextMenu(this.createMenuOption());
                    this.contextHighwayMenu = new ContextMenu(this.createMenuOption_MapHighWay());
                    this.contextTollboothMenu = new ContextMenu(this.createMenuOption_TollboothMap());
                    this.initEventOnMenu_MapHighWay();
                    this.initEventOnMenu_MapTollbooth();
                    this.initEventOnMenu();

                }
                initEventOnMenu_MapHighWay(){
                    this.contextHighwayMenu.menuEventTrigger = (name:string, point:google.maps.LatLng) =>{
                        switch (name) {

                            //thêm vùng
                            case 'addArea':
                            {
                                if(document.getElementById("menu_item_addArea").innerHTML === 'Thêm vùng') {
                                    document.getElementById("menu_item_addArea").innerHTML = 'Hủy vùng';
                                    this.cfg.manageArea.areaDraw_Highway();
                                }
                                else{
                                    document.getElementById("menu_item_addArea").innerHTML = 'Thêm vùng';
                                    this.cfg.manageArea.cancelDesign();
                                }
                                break;

                            }
                            case 'backCenter':
                            {
                                this.cfg.map.setCenter(point);
                                break;
                            }
                            case 'get_directions_click':
                            {
                                $("#search_location").hide();
                                $("#get_direction").show();
                                $('#refeshbutton_getdirection').click(()=>{
                                    $('#origin-input').val('');
                                    $('#destination-input').val('');
                                });
                                $('#closebutton_getdirection').click(()=>{
                                    this.cfg.getdirection.Cleardirection();
                                    $("#get_direction").hide();
                                });
                                this.cfg.getdirection.Getdirection();
                                break;

                            }
                            case 'clear_directions_click':
                            {
                                this.cfg.getdirection.Cleardirection();
                                $("#get_direction").hide();
                                break;
                            }
                        }

                        this.contextMenu.hide();
                    };
                }
                initEventOnMenu_MapTollbooth(){
                    this.contextTollboothMenu.menuEventTrigger = (name:string, point:google.maps.LatLng) =>{
                        switch (name) {

                            //thêm vùng
                            // case 'addArea':
                            // {
                            //     if(document.getElementById("menu_item_addArea").innerHTML === 'Thêm vùng') {
                            //         document.getElementById("menu_item_addArea").innerHTML = 'Hủy vùng';
                            //         this.cfg.manageArea.areaDraw_Highway();
                            //     }
                            //     else{
                            //         document.getElementById("menu_item_addArea").innerHTML = 'Thêm vùng';
                            //         this.cfg.manageArea.cancelDesign();
                            //     }
                            //     break;
                            //
                            // }
                            case 'addPoint':
                            {
                                this.cfg.managePoint.newPoint(point);
                                break;
                            }
                            case 'backCenter':
                            {
                                this.cfg.map.setCenter(point);
                                break;
                            }
                            case 'get_directions_click':
                            {
                                $("#search_location").hide();
                                $("#get_direction").show();
                                $('#refeshbutton_getdirection').click(()=>{
                                    $('#origin-input').val('');
                                    $('#destination-input').val('');
                                });
                                $('#closebutton_getdirection').click(()=>{
                                    this.cfg.getdirection.Cleardirection();
                                    $("#get_direction").hide();
                                });
                                this.cfg.getdirection.Getdirection();
                                break;

                            }
                            case 'clear_directions_click':
                            {
                                this.cfg.getdirection.Cleardirection();
                                $("#get_direction").hide();
                                break;
                            }
                        }

                        this.contextTollboothMenu.hide();
                    };
                }
                createMenuOption_MapHighWay() {
                    var contextMenuOptions:any = {};
                    contextMenuOptions.classNames = {menu: 'context_menu', menuSeparator: 'context_menu_separator'};
                    var menuItems = Array<any>();
                    // menuItems.push({className: 'context_menu_item', eventName: 'searchCar', label: 'Tìm xe'});
                    // menuItems.push({
                    //     className: 'context_menu_item',
                    //     eventName: 'searchCarInPoint',
                    //     label: 'Tìm xe trong khu vưc',
                    //     id: 'menu_item_searchCarInPoint'
                    // });
                    // menuItems.push({className: 'context_menu_item', eventName: 'viewLocation', label: 'Xem toạ độ'});
                    // menuItems.push({className: 'context_menu_item', eventName: 'viewAddress', label: 'Xem địa chỉ'});
                    // menuItems.push({className: '', eventName: 'hr', label: '<li role="separator" class="divider"></li>',});
                    // menuItems.push({
                    //     className: 'context_menu_item',
                    //     eventName: 'countDistance',
                    //     label: 'Đo khoảng cách',
                    //     id: 'menu_item_distance'
                    // });

                    //menuItems.push({className: 'context_menu_item', eventName: 'addPoint', label: 'Thêm điểm'});
                    menuItems.push({
                        className: 'context_menu_item',
                        eventName: 'addArea',
                        label: '<span><i class="icon-add"></i> Thêm vùng</span>',
                        id: 'menu_item_addArea'
                    });
                    // menuItems.push({className: '', eventName: 'hr', label: '<li role="separator" class="divider"></li>',});
                    menuItems.push({className: 'context_menu_item', eventName: 'get_directions_click', label: 'Chỉ đường', id:'getDirectionsItem'});
                    menuItems.push({className: 'context_menu_item', eventName: 'clear_directions_click', label: 'Xóa chỉ đường', id:'clearDirectionsItem'});
                    menuItems.push({className: '', eventName: 'hr', label: '<li role="separator" class="divider" style=" pointer-events: none;cursor: default;opacity:0.6;"></li>'});
                    menuItems.push({
                        className: 'context_menu_item',
                        eventName: 'backCenter',
                        label: 'Quay về trung tâm'
                    });
                    menuItems.push({className: 'context_menu_item', eventName: 'clear', label: 'Làm sạch bản đồ'});
                    contextMenuOptions.menuItems = menuItems;
                    return contextMenuOptions;
                }

                initEventOnMenu() {
                    this.contextMenu.menuEventTrigger = (name:string, point:google.maps.LatLng) =>{
                        switch (name) {
                            // case 'searchCar':
                            //     break;
                            // case 'searchCarInPoint':
                            //     break;
                            //case 'clearMap': clearMap();
                            //    break;
                            case 'viewLocation':
                            {
                                var infoLocation = 'Vĩ độ: ' + point.lat() + '</br>'+'Kinh độ: ' + point.lng();
                                this.cfg.managePoint.showInfo(infoLocation, 'Toạ độ hiện tại');
                                break;
                            }
                            case 'viewAddress':
                            {
                                this.cfg.managePoint.getAddress(point).then((mess:any)=>{
                                    this.cfg.managePoint.showInfo(mess, 'Kết quả tìm được');
                                });
                                break;
                            }
                            case 'countDistance':
                            {
                                //todo : commit thiếu đoạn này
                                if (document.getElementById("menu_item_distance").innerHTML === '<span><i class="icon-move-horizontal"></i> Đo khoảng cách</span>') {
                                    console.log('chay do khoang cach 1 lan');
                                    $("#distance_top").show();
                                    document.getElementById("menu_item_distance").innerHTML = '<span><i class="icon-eraser"></i>Hủy đo</span> ';
                                    this.cfg.manageMapDistance.createPolyline();
                                }
                                else {
                                    $("#distance_top").hide();
                                    console.log('chay do khoang cach 1 lan');
                                    document.getElementById("menu_item_distance").innerHTML = '<span><i class="icon-move-horizontal"></i> Đo khoảng cách</span>';
                                    this.cfg.manageMapDistance.deleteDistance();
                                }
                                break;
                            }
                            // thêm điểm
                            case 'addPoint':
                            {
                                this.cfg.managePoint.newPoint(point);
                                break;
                            }
                            //
                            case 'clear':
                            {
                                this.cfg.manageCarTrip.clear();
                                break;
                            }
                            //thêm vùng
                            case 'addArea':
                            {
                                if(document.getElementById("menu_item_addArea").innerHTML === '<span><i class="icon-add"></i> Thêm vùng</span>') {
                                document.getElementById("menu_item_addArea").innerHTML = '<span><i class="icon-eraser"></i>Hủy vùng</span> ';
                                this.cfg.manageArea.areaDraw();
                                }
                                else{
                                document.getElementById("menu_item_addArea").innerHTML = '<span><i class="icon-add"></i> Thêm vùng</span>';
                                this.cfg.manageArea.cancelDesign();
                                }
                                break;

                            }
                            case 'backCenter':
                            {
                                this.cfg.map.setCenter(point);
                                break;
                            }
                            case 'get_directions_click':
                            {
                                $("#search_location").hide();
                                $("#get_direction").show();
                                $('#refeshbutton_getdirection').click(()=>{
                                    $('#origin-input').val('');
                                    $('#destination-input').val('');
                                });
                                $('#closebutton_getdirection').click(()=>{
                                    this.cfg.getdirection.Cleardirection();
                                    $("#get_direction").hide();
                                });
                                this.cfg.getdirection.Getdirection();
                                break;

                            }
                            case 'directions_origin_click':
                            {
                                // console.log(point);
                                // window.open(`https://www.google.com/maps/dir///@${point.lat()},${point.lng()},15z/data=!4m2!4m1!3e0`, '_blank');
                                // window.open('https://maps.google.com', '_blank');
                                window.open(`https://www.google.com/maps/dir/${point.lat()},${point.lng()}//@${point.lat()},${point.lng()},12z/data=!4m2!4m1!3e0`, '_blank');
                                break;
                            }
                            // case 'directions_destination_click':
                            // {
                            //     this.cfg.getdirection.DirectionDestination(point);
                            //     document.getElementById('getDirectionsItem').style.display='block';
                            //     break;
                            // }
                            case 'clear_directions_click':
                            {
                                this.cfg.getdirection.Cleardirection();
                                $("#get_direction").hide();
                                break;
                            }
                            case 'search_directions':
                            {
                                $("#search_location").show();
                                $("#get_direction").hide();
                                this.cfg.getdirection.Searchdirection();
                                $('#searchdirections').click(()=>{
                                    $("#get_direction").show();
                                    $("#search_location").hide();
                                    this.cfg.getdirection.Getdirection();
                                });

                            }


                        }

                        this.contextMenu.hide();
                    };
                }
                createMenuOption() {
                    var contextMenuOptions:any = {};
                    contextMenuOptions.classNames = {menu: 'context_menu', menuSeparator: 'context_menu_separator'};
                    var menuItems = Array<any>();
                    // menuItems.push({className: 'context_menu_item', eventName: 'searchCar', label: 'Tìm xe'});
                    // menuItems.push({
                    //     className: 'context_menu_item',
                    //     eventName: 'searchCarInPoint',
                    //     label: 'Tìm xe trong khu vưc',
                    //     id: 'menu_item_searchCarInPoint'
                    // });
                    menuItems.push({className: 'context_menu_item', eventName: 'viewLocation', label: '<span><i class="icon-location4"></i> Xem toạ độ</span> '});
                    menuItems.push({className: 'context_menu_item', eventName: 'viewAddress', label: '<span><i class="icon-address-book"></i> Xem địa chỉ</span>'});
                    menuItems.push({
                        className: 'context_menu_item',
                        eventName: 'countDistance',
                        label: '<span><i class="icon-move-horizontal"></i> Đo khoảng cách</span>',
                        id: 'menu_item_distance'
                    });
                    menuItems.push({className: 'context_menu_item', eventName: 'directions_origin_click', label: '<span><i class="icon-direction"></i> Chỉ đường</span>', id:'getDirectionsItem'});
                    menuItems.push({className: '', eventName: 'hr', label: '<li role="separator" class="divider"></li>',});
                    menuItems.push({className: 'context_menu_item', eventName: 'addPoint', label: '<span><i class="icon-add"></i> Thêm điểm</span>'});
                    menuItems.push({
                        className: 'context_menu_item',
                        eventName: 'addArea',
                        label: '<span><i class="icon-add"></i> Thêm vùng</span>',
                        id: 'menu_item_addArea'
                    });

                    // menuItems.push({className: 'context_menu_item', eventName: 'search_directions', label: 'Tìm địa chỉ', id:'searchdirection'});
                    // menuItems.push({className: 'context_menu_item', eventName: 'get_directions_click', label: '<span><i class="icon-direction"></i> Chỉ đường</span>', id:'getDirectionsItem'});
                    // menuItems.push({className: 'context_menu_item', eventName: 'clear_directions_click', label: '<span><i class="icon-eraser2"></i> Xóa chỉ đường</span>', id:'clearDirectionsItem'});
                    menuItems.push({className: '', eventName: 'hr', label: '<li role="separator" class="divider" style=" pointer-events: none;cursor: default;opacity:0.6;"></li>'});
                    menuItems.push({
                        className: 'context_menu_item',
                        eventName: 'backCenter',
                        label: '<span><i class="icon-forward"></i> Quay về trung tâm</span>'
                    });
                    menuItems.push({className: 'context_menu_item', eventName: 'clear', label: '<span><i class="icon-loop3"></i> Làm sạch bản đồ</span>'});
                    contextMenuOptions.menuItems = menuItems;
                    return contextMenuOptions;
                }

                createMenuOption_TollboothMap(){
                    var contextMenuOptions:any = {};
                    contextMenuOptions.classNames = {menu: 'context_menu', menuSeparator: 'context_menu_separator'};
                    var menuItems = Array<any>();
                    // menuItems.push({className: 'context_menu_item', eventName: 'searchCar', label: 'Tìm xe'});
                    // menuItems.push({
                    //     className: 'context_menu_item',
                    //     eventName: 'searchCarInPoint',
                    //     label: 'Tìm xe trong khu vưc',
                    //     id: 'menu_item_searchCarInPoint'
                    // });
                    // menuItems.push({className: 'context_menu_item', eventName: 'viewLocation', label: 'Xem toạ độ'});
                    // menuItems.push({className: 'context_menu_item', eventName: 'viewAddress', label: 'Xem địa chỉ'});
                    // menuItems.push({className: '', eventName: 'hr', label: '<li role="separator" class="divider"></li>',});
                    // menuItems.push({
                    //     className: 'context_menu_item',
                    //     eventName: 'countDistance',
                    //     label: 'Đo khoảng cách',
                    //     id: 'menu_item_distance'
                    // });

                    menuItems.push({className: 'context_menu_item', eventName: 'addPoint', label: 'Thêm điểm'});
                    // menuItems.push({
                    //     className: 'context_menu_item',
                    //     eventName: 'addArea',
                    //     label: 'Thêm vùng',
                    //     id: 'menu_item_addArea'
                    // });
                    // menuItems.push({className: '', eventName: 'hr', label: '<li role="separator" class="divider"></li>',});
                    menuItems.push({className: 'context_menu_item', eventName: 'get_directions_click', label: 'Chỉ đường', id:'getDirectionsItem'});
                    menuItems.push({className: 'context_menu_item', eventName: 'clear_directions_click', label: 'Xóa chỉ đường', id:'clearDirectionsItem'});
                    menuItems.push({className: '', eventName: 'hr', label: '<li role="separator" class="divider" style=" pointer-events: none;cursor: default;opacity:0.6;"></li>'});
                    menuItems.push({
                        className: 'context_menu_item',
                        eventName: 'backCenter',
                        label: 'Quay về trung tâm'
                    });
                    menuItems.push({className: 'context_menu_item', eventName: 'clear', label: 'Làm sạch bản đồ'});
                    contextMenuOptions.menuItems = menuItems;
                    return contextMenuOptions;
                }
            }
        }
    }
}