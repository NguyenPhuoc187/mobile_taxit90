
/**
 * Created by user on 04/27/2016.
 */
/// <reference path="dictionary.lib.ts" />
/// <reference path="car.lib.ts" />
/// <reference path="BaseCar.lib.ts" />
module App{
    export module Shared{
        export module Library{

            export var GlobalCarManager:CarManager;

            export interface MapConfig{
                map:google.maps.Map|any;
                q:ng.IQService;
            }
            export interface CarConfig extends MapConfig{
                $http:ng.IHttpService;
                global?:GlobalService;
            }
            export class CarManager {
                /*Kiểm tra thông tin load dữ liệu từ server*/
                public isOk:boolean=false;
                private cfg:CarConfig;
                private allCars:IDictionary<string,CarView>;
                private listenerArray:Array<any>;
                private eventBoundChange:google.maps.MapsEventListener;
                private connection:any=null;
                private xhttp = new XMLHttpRequest();
                constructor(config:CarConfig) {
                    this.cfg = config ;
                    this.allCars = new Dictionary<string,CarView>();
                    this.listenerArray = Array<any>();
                    this.installEvent();

                }
                public sum:number =0;
                public xechay:number = 0;
                private statictis:any={
                    CarForAll:0,
                    CarForDrive:0,
                    CarForStop:0,
                    CarForShut:0,
                    CarLostGps:0,
                    CarLostSign:0,
                    CarOverSpeed:0,

                };
                public onStatictis:(st:any)=>void;
                public ConnectSocket(companyId:number,grId:number){
                    if(this.cfg.global!=null&&this.cfg.global!=undefined)
                    {
                        if(this.connection!=null)
                            this.connection.stop();
                        this.connection = $.hubConnection(this.cfg.global.ReportHost);

                        this.connection.qs = {
                            'token': this.cfg.global.Token.GetValue(),
                            'companyId':companyId,
                            'groupId':grId
                        };
                        var proxy = this.connection.createHubProxy('DeviceStatusHub');
                        var seft=this;
                        // đếm số lượng trạng thái xe
                        proxy.on("Update", (car:any)=>{
                            seft.onUpdateDevice(car);
                        });
                        console.log(this.connection);
                        this.connection.start().then(function () {
                            console.log('Connect Ok');

                            return proxy;

                        }).fail(function () {
                            console.error('Connect Fail');
                        });
                        // this.connection.disconnected((t:any)=>{
                        //     console.log('disconect');
                        //     location.reload();
                        //     // setTimeout(function() {
                        //     //     this.connection.start().then(function () {
                        //     //         console.log('Connect Ok');
                        //     //
                        //     //         return proxy;
                        //     //
                        //     //     }).fail(function () {
                        //     //         console.error('Connect Fail');
                        //     //     });
                        //     // }, 5000); // Restart connection after 5 seconds.
                        //     // this.connection.reconnecting();
                        //
                        // });

                    }
                }

                private onUpdateDevice(value:any){
                    if (this.allCars.ContainKey(value.Serial)) {
                        var oldvalue = this.allCars.Get(value.Serial);
                        // (tmp, value);

                        let newvalue = {
                            isOverSpeed: value.isOverSpeed, /*xe có quá tốc độ hay ko?*/
                            Bs: value.Bs, /*Biển số xe*/
                            statusCar: value.statusCar, /* trạng thái xe*/
                            speed: value.speed, /*Tốc độ xe*/
                            kmOnDay: value.KmNgay, /*số km trong ngày*/
                            dienAp: value.dienAp, /*thông số Điện áp*/
                            statusGps: value.statusGps, /*Trạng thái Gps*/
                            trangThaiMay: value.trangThaiMay, /*Trạng thái máy*/
                            trangThaiMayLanh: value.trangThaiMayLanh, /*Trạng thái máy lạnh*/
                            soGtvt: value.soGtvt, /*Tên sở giao thông vận tải*/
                            kmCuoc: value.kmCuoc, /*Km cuốc*/
                            mucXang: value.mucXang, /*mức xăng*/
                            mucSong: value.mucSong, /*Mức sóng - cường độ trường*/
                            cuaXe: value.cuaXe, /*Trạng thái đóng mở của xe*/
                            dungDo: value.dungDo, /*trạng thái dừng đỗ*/
                            Id: value.Id, /*id xe*/
                            Gplx: value.Gplx, /*Giấy phép lái xe*/
                            nhietDo: value.nhietDo, /*nhiệt độ*/
                            address: value.address, /*Địa chỉ*/
                            timeUpdate: value.timeUpdate, /*Thời gian cập nhật*/
                            tglxtn: value.tglxtn, /*Thời gian lái xe liên tục*/
                            tglxlt: value.tglxlt, /*Thời gian lái xe trong ngày*/
                            nameDriver: value.nameDriver, /*Tên tài xế*/
                            qtgtn: value.qtgtn, /*Quá thời gian liên tục*/
                            qtglt: value.qtglt, /*quá thời gian trong ngày*/
                            moCua: value.moCua, /*số lần mở cửa*/
                            Location: {
                                Lat: value.Location.Lat,
                                Lng: value.Location.Lng,
                                Address: ''
                            },
                            Serial: value.Serial,
                            bienSo: value.Bienso,
                            Angle: value.Angle,
                            type: value.type,
                            pausecount: value.pausecount,
                            opendoorcount: value.opendoorcount,
                            overspeedcount: value.overspeedcount,
                            vin: value.vin,
                            money: value.money,
                            phone: value.phone,
                            modeltype: value.modeltype,
                            statusplusmemory: value.plusmemory,
                            sheats: value.sheat,
                            lostgsm: value.lostgsm,
                            group: value.group,
                            typeValue: value.typeValue,
                            displayName: '',
                            EndTime: value.EndTime,
                            MessageAlert:value.MessageAlert,
                            TimePause:value.TimePause,
                            PauseValue:value.PauseValue,
                            groupId:value.groupId,
                            timeUpdateconvert: value.timeUpdateconvert,
                        };
                        this.FilterStateCars(oldvalue,newvalue);
                        oldvalue.load(newvalue);
                        // tmp.load({
                        //     isOverSpeed: value.isOverSpeed, /*xe có quá tốc độ hay ko?*/
                        //     Bs: value.Bs, /*Biển số xe*/
                        //     statusCar: value.statusCar, /* trạng thái xe*/
                        //     speed: value.speed, /*Tốc độ xe*/
                        //     kmOnDay: value.KmNgay, /*số km trong ngày*/
                        //     dienAp: value.dienAp, /*thông số Điện áp*/
                        //     statusGps: value.statusGps, /*Trạng thái Gps*/
                        //     trangThaiMay: value.trangThaiMay, /*Trạng thái máy*/
                        //     trangThaiMayLanh: value.trangThaiMayLanh, /*Trạng thái máy lạnh*/
                        //     soGtvt: value.soGtvt, /*Tên sở giao thông vận tải*/
                        //     kmCuoc: value.kmCuoc, /*Km cuốc*/
                        //     mucXang: value.mucXang, /*mức xăng*/
                        //     mucSong: value.mucSong, /*Mức sóng - cường độ trường*/
                        //     cuaXe: value.cuaXe, /*Trạng thái đóng mở của xe*/
                        //     dungDo: value.dungDo, /*trạng thái dừng đỗ*/
                        //     Id: value.Id, /*id xe*/
                        //     Gplx: value.Gplx, /*Giấy phép lái xe*/
                        //     nhietDo: value.nhietDo, /*nhiệt độ*/
                        //     address: value.address, /*Địa chỉ*/
                        //     timeUpdate: value.timeUpdate, /*Thời gian cập nhật*/
                        //     tglxtn: value.tglxtn, /*Thời gian lái xe liên tục*/
                        //     tglxlt: value.tglxlt, /*Thời gian lái xe trong ngày*/
                        //     nameDriver: value.nameDriver, /*Tên tài xế*/
                        //     qtgtn: value.qtgtn, /*Quá thời gian liên tục*/
                        //     qtglt: value.qtglt, /*quá thời gian trong ngày*/
                        //     moCua: value.moCua, /*số lần mở cửa*/
                        //     Location: {
                        //         Lat: value.Location.Lat,
                        //         Lng: value.Location.Lng,
                        //         Address: ''
                        //     },
                        //     Serial: value.Serial,
                        //     bienSo: value.Bienso,
                        //     Angle: value.Angle,
                        //     type: value.type,
                        //     pausecount: value.pausecount,
                        //     opendoorcount: value.opendoorcount,
                        //     overspeedcount: value.overspeedcount,
                        //     vin: value.vin,
                        //     money: value.money,
                        //     phone: value.phone,
                        //     modeltype: value.modeltype,
                        //     statusplusmemory: value.plusmemory,
                        //     sheats: value.sheat,
                        //     lostgsm: value.lostgsm,
                        //     group: value.group,
                        //     typeValue: value.typeValue,
                        //     displayName: '',
                        //     EndTime: value.EndTime,
                        //     MessageAlert:value.MessageAlert,
                        //     TimePause:value.TimePause,
                        //     PauseValue:value.PauseValue,
                        // });

                          this.processonlyCarViewMap(oldvalue);
                        //this.processCarViewOnMap();
                    }

                }
                public FilterStateCars(oldvalue:any, newvalue:any){
                    //với trường hợp xe chạy
                    // var count_xe_chay = this.getCarForDrive(newvalue.groupId).length;
                    if(!newvalue.lostgsm){
                        // không mất liên lạc
                        if(oldvalue.dungDo !== newvalue.dungDo){
                            if(!newvalue.dungDo){
                                this.statictis.CarForDrive ++;
                                this.statictis.CarForStop --;
                            }else{
                                this.statictis.CarForDrive --;
                                this.statictis.CarForStop ++;
                            }
                            // count_xe_chay = count_xe_chay + 1;
                        }
                    }
                    this.statictis.carnew = newvalue;
                    // this.statictis.CarForDrive = count_xe_chay;
                    // this.xechay = count_xe_chay;
                   // console.log(this.statictis.xechay);

                    if(this.onStatictis!=undefined)
                        this.onStatictis(this.statictis);

                }

                // public CountXedangchay(){
                //     return this.xechay;
                // }


                private installEvent() {
                    if (this.eventBoundChange != null)
                        google.maps.event.removeListener(this.eventBoundChange);
                    if (this.cfg.map !== undefined)
                        this.eventBoundChange = google.maps.event.addListener(this.cfg.map, "bounds_changed", ()=> {
                            this.processCarViewOnMap();
                        });
                }
                private processonlyCarViewMap(car: any){
                    if (this.cfg.map !== undefined) {
                        var bound = this.cfg.map.getBounds();
                        if(bound==undefined) return;
                        if (bound.contains(car.latlng)&&!car.ForceHide) {
                            if (car.getMap() == null) {
                                car.setMap(this.cfg.map);
                                // if(car.viewInfo() != null){
                                //     car.moveinfo();
                                // }
                            }
                            car.rotate();
                        }
                        else {
                            if (car.getMap() != null) {
                                car.setMap(null);
                            }
                        }
                    }
                }

                public processCarViewOnMap():void {
                    if (this.cfg.map !== undefined) {
                        var bound = this.cfg.map.getBounds();
                        if(bound==undefined) return;
                        angular.forEach(this.allCars.Values(), (car)=> {
                            if (bound.contains(car.latlng)&&!car.ForceHide) {
                                if (car.getMap() == null) {
                                    car.setMap(this.cfg.map);
                                    // if(car.viewInfo() != null){
                                    //     car.moveinfo();
                                    // }
                                }
                                car.rotate();
                            }
                            else {
                                if (car.getMap() != null) {
                                    car.setMap(null);
                                }
                            }

                        });
                    }
                }

                clearAllCars() {
                    angular.forEach(this.getAllCars(), (c)=> {
                        c.setMap(null);
                        c.removeinfo();
                        
                    });
                    this.allCars.Clear();
                };


                
                
                private defer:any=null;
                /*Load thông tin thiết bị từ server về*/
                public getAllCarByCompanyId(companyId:number, $http:ng.IHttpService, global: App.Shared.GlobalService, grid: App.Shared.Directives.GridviewControl, companyName:string, groupId:number):angular.IPromise<boolean> {
                    //if(this.defer==null) -- bỏ đoạn này để defer đc tạo mới liên tục
                    this.defer=this.cfg.q.defer<boolean>();
                    if(this.isOk==true) {
                        console.log('dang lay danh sach xe ! doi xi');
                        this.defer.resolve(false);
                        return this.defer.promise;
                    }
                    else {
                        this.isOk = true;
                        if(this.xhttp==null)
                           this.xhttp = new XMLHttpRequest();
                       if(this.xhttp.onreadystatechange==null||this.xhttp.onreadystatechange==undefined)
                            this.xhttp.onreadystatechange = ()=> {
                                if (this.xhttp.readyState == 4 && this.xhttp.status == 200) {
                                    var data = JSON.parse(this.xhttp.responseText);
                                    $.each(data.Datas, (index:number, value:any) => {
                                        try {
                                            value.tt = value.timeUpdate;
                                            // value.qtglt = 0;
                                            value.Angle += 176;

                                            var acc:any = global.Account.GetValue();
                                            if (this.allCars.ContainKey(value.Serial)) {


                                                var tmp = this.allCars.Get(value.Serial);
                                                tmp.load({
                                                    isOverSpeed: value.isOverSpeed, /*xe có quá tốc độ hay ko?*/
                                                    Bs: value.Bs, /*Biển số xe*/
                                                    statusCar: value.statusCar, /* trạng thái xe*/
                                                    speed: value.speed, /*Tốc độ xe*/
                                                    kmOnDay: value.KmNgay, /*số km trong ngày*/
                                                    dienAp: value.dienAp, /*thông số Điện áp*/
                                                    statusGps: value.statusGps, /*Trạng thái Gps*/
                                                    trangThaiMay: value.trangThaiMay, /*Trạng thái máy*/
                                                    trangThaiMayLanh: value.trangThaiMayLanh, /*Trạng thái máy lạnh*/
                                                    soGtvt: value.soGtvt, /*Tên sở giao thông vận tải*/
                                                    kmCuoc: value.kmCuoc, /*Km cuốc*/
                                                    mucXang: value.mucXang, /*mức xăng*/
                                                    mucSong: value.mucSong, /*Mức sóng - cường độ trường*/
                                                    cuaXe: value.cuaXe, /*Trạng thái đóng mở của xe*/
                                                    dungDo: value.dungDo, /*trạng thái dừng đỗ*/
                                                    Id: value.Id, /*id xe*/
                                                    Gplx: value.Gplx, /*Giấy phép lái xe*/
                                                    nhietDo: value.nhietDo, /*nhiệt độ*/
                                                    address: value.address, /*Địa chỉ*/
                                                    timeUpdate: value.timeUpdate, /*Thời gian cập nhật*/
                                                    tglxtn: value.tglxtn, /*Thời gian lái xe liên tục*/
                                                    tglxlt: value.tglxlt, /*Thời gian lái xe trong ngày*/
                                                    nameDriver: value.nameDriver, /*Tên tài xế*/
                                                    qtgtn: value.qtgtn, /*Quá thời gian liên tục*/
                                                    qtglt: value.qtglt, /*quá thời gian trong ngày*/
                                                    moCua: value.moCua, /*số lần mở cửa*/
                                                    Location: {
                                                        Lat: value.Location.Lat,
                                                        Lng: value.Location.Lng,
                                                        Address: ''
                                                    },
                                                    Serial: value.Serial,
                                                    bienSo: value.Bienso,
                                                    Angle: value.Angle,
                                                    type: value.type,
                                                    pausecount: value.pausecount,
                                                    opendoorcount: value.opendoorcount,
                                                    overspeedcount: value.overspeedcount,
                                                    vin: value.vin,
                                                    money: value.money,
                                                    phone: value.phone,
                                                    modeltype: value.modeltype,
                                                    statusplusmemory: value.plusmemory,
                                                    sheats: value.sheat,
                                                    lostgsm: value.lostgsm,
                                                    group: value.group,
                                                    typeValue: value.typeValue,
                                                    displayName: acc.DisplayName,
                                                    EndTime: value.EndTime,
                                                    MessageAlert:value.MessageAlert,
                                                    TimePause:value.TimePause,
                                                    PauseValue:value.PauseValue,
                                                    groupId:value.groupId,
                                                    timeUpdateconvert: value.timeUpdateconvert
                                                });

                                            } else {
                                                var car:any = new Shared.Library.CarView(this.cfg);
                                                car.companyId = companyId;
                                                car.companyName = companyName;
                                                car.load({
                                                    isOverSpeed: value.isOverSpeed, /*xe có quá tốc độ hay ko?*/
                                                    Bs: value.Bs, /*Biển số xe*/
                                                    statusCar: value.statusCar, /* trạng thái xe*/
                                                    speed: value.speed, /*Tốc độ xe*/
                                                    kmOnDay: value.KmNgay, /*số km trong ngày*/
                                                    dienAp: value.dienAp, /*thông số Điện áp*/
                                                    statusGps: value.statusGps, /*Trạng thái Gps*/
                                                    trangThaiMay: value.trangThaiMay, /*Trạng thái máy*/
                                                    trangThaiMayLanh: value.trangThaiMayLanh, /*Trạng thái máy lạnh*/
                                                    soGtvt: value.soGtvt, /*Tên sở giao thông vận tải*/
                                                    kmCuoc: value.kmCuoc, /*Km cuốc*/
                                                    mucXang: value.mucXang, /*mức xăng*/
                                                    mucSong: value.mucSong, /*Mức sóng - cường độ trường*/
                                                    cuaXe: value.cuaXe, /*Trạng thái đóng mở của xe*/
                                                    dungDo: value.dungDo, /*trạng thái dừng đỗ*/
                                                    Id: value.Id, /*id xe*/
                                                    Gplx: value.Gplx, /*Giấy phép lái xe*/
                                                    nhietDo: value.nhietDo, /*nhiệt độ*/
                                                    address: value.address, /*Địa chỉ*/
                                                    timeUpdate: value.timeUpdate, /*Thời gian cập nhật*/
                                                    tglxtn: value.tglxtn, /*Thời gian lái xe liên tục*/
                                                    tglxlt: value.tglxlt, /*Thời gian lái xe trong ngày*/
                                                    nameDriver: value.nameDriver, /*Tên tài xế*/
                                                    qtgtn: value.qtgtn, /*Quá thời gian liên tục*/
                                                    qtglt: value.qtglt, /*quá thời gian trong ngày*/
                                                    moCua: value.moCua, /*số lần mở cửa*/
                                                    Location: {
                                                        Lat: value.Location.Lat,
                                                        Lng: value.Location.Lng,
                                                        Address: ''
                                                    },
                                                    Serial: value.Serial,
                                                    Bienso: value.Bienso,
                                                    Angle: value.Angle,
                                                    tt: value.tt,
                                                    type: value.type,
                                                    pausecount: value.pausecount,
                                                    opendoorcount: value.opendoorcount,
                                                    overspeedcount: value.overspeedcount,
                                                    vin: value.vin,
                                                    money: value.money,
                                                    phone: value.phone,
                                                    modeltype: value.modeltype,
                                                    statusplusmemory: value.plusmemory,
                                                    sheats: value.sheat,
                                                    lostgsm: value.lostgsm,
                                                    group: value.group,
                                                    typeValue: value.typeValue,
                                                    displayName: acc.DisplayName,
                                                    EndTime: value.EndTime,
                                                    MessageAlert:value.MessageAlert,
                                                    TimePause:value.TimePause,
                                                    PauseValue:value.PauseValue,
                                                    groupId:value.groupId,
                                                    timeUpdateconvert: value.timeUpdateconvert
                                                });
                                                //console.log(`add device : ${car.Bs}`);
                                                this.allCars.Add(car.serial, car);
                                                car.CallbackOnClick = ()=> {
                                                    $.each(this.getAllCars(), (idx:number, val:CarView)=> {
                                                        val.removeinfo();
                                                    });
                                                }
                                            }
                                        } catch (e) {
                                            console.log('loi trang thai');
                                        }
                                        // this.statictis.CarForAll=this.getCarForGroup(groupId).length;
                                        this.statictis.CarForDrive=this.getCarForDrive(groupId).length;
                                        this.statictis.CarForStop=this.getCarForStop(groupId).length;
                                        this.statictis.CarForShut=this.getCarForShut(groupId).length;
                                        this.statictis.CarLostSign=this.getCarLostSign(groupId).length;
                                        this.statictis.CarOverSpeed=this.getCarOverSpeed(groupId).length;
                                        this.statictis.CarLostGps=this.getCarLostGps(groupId).length;

                                    });
                                    //
                                    this.processCarViewOnMap();
                                    ///*load thông tin từ server*/
                                    ///*load lên bản đồ*/
                                    this.isOk = false;
                                    //// grid.data = rep.data.Datas;
                                    ////grid.refresh();
                                    delete this.xhttp.responseText;
                                    delete this.xhttp.response;
                                    delete this.xhttp.onreadystatechange;
                                    delete this.xhttp;
                                    this.xhttp = null;
                                    data=null;
                                    this.defer.resolve(true);
                                }

                                if(this.xhttp!=undefined&&this.xhttp!=null&&this.xhttp.status==401)// hết hạn token
                                        window.location.href = "/login.html";
                            };
                        //todo: mở lên khi server up lên route 1
                        if(groupId !== -1){
                            this.xhttp.open("GET", global.ReportHost + `api/Device/GetDeviceStatusByGroupId?companyId=${companyId}&groupId=${groupId}`, false);

                        }else{
                            this.xhttp.open("GET", global.ReportHost + `api/Device/GetDeviceStatusByCompanyId?companyId=${companyId}`, false);
                        }
                        // this.xhttp.open("GET", global.ReportHost + `api/Device/GetDeviceStatusByCompanyId?companyId=${companyId}`, false);
                        this.xhttp.setRequestHeader("token", global.Token.GetValue<string>());
                      //  this.xhttp.responseType = 'json';
                        this.xhttp.setRequestHeader("Content-Type","application/json");
                        this.xhttp.setRequestHeader("Accept","application/json");
                       // this.xhttp.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                        this.xhttp.send(null);
                        //delete this.xhttp;
                        //this.xhttp = null;
                        //$http.get(global.ReportHost + `api/Device/GetDeviceStatusByCompanyId?companyId=${companyId}`,{cache :false}).then((rep:any)=> {
                        //    this.isOk = false;
                        //    this.defer.resolve(true);
                        //});

                    }
                    return this.defer.promise;
                }

                public getAllCars() {
                    return this.allCars.Values();
                }


                public resetOption(serial:string,type:string){
                    var car=this.allCars.Get(serial);
                    if(car!=null||car!=undefined){
                        car.resettozero(type);
                    }
                }
                   // carDistanceView (id,subtime){
                   // var car=this.allCars.get(id);
                   // if(car !=null){
                   //     /*tính thời gian cần hiển thị */
                   //     /*thời gian hiện tại + subtime*/
                   //     var end=new Date();
                   //
                   //     var begin=new Date();
                   //     begin.setTime(end.getTime()+subtime*60*1000);
                   //     this.cfg.manageCarTrip.load(id
                   //         ,(begin.getMonth() +1 )+"/"+begin.getDate()+"/"+begin.getFullYear()+" "+begin.getHours()+":"+begin.getMinutes()+":"+begin.getSeconds(),
                   //         (end.getMonth()+1)+"/"+end.getDate()+"/"+end.getFullYear()+" "+end.getHours()+":"+end.getMinutes()+":"+end.getSeconds()
                   //     );
                   // }

                //};
                //TODO: thêm các hàm thông kê ở đây
                public where(where:(car:CarView)=>boolean):Array<CarView> {
                    var result = Array<CarView>();
                    angular.forEach(this.getAllCars(), function (car) {
                        if (where(car))
                            result.push(car);
                    });
                    return result;
                }

                public getCarContainInbounds(bounds:google.maps.LatLngBounds) {
                    if (bounds == null || bounds == undefined)
                        return [];
                    return this.where(function (c) {
                        return bounds.contains(c.latlng);
                    });
                }

                //xe quá tốc độ
                public getCarOverSpeed(groupId:number) {
                    if(groupId !== -1){
                        return this.where((car:CarView)=> {
                            if(car.groupId === groupId){
                                return car.isOverSpeed && !car.lostgsm;
                            }

                        });
                    }else{
                        return this.where((car:CarView)=> {
                            return car.isOverSpeed && !car.lostgsm;
                        });
                    }

                }
                /*xe mất liên lạc*/
                public getCarLostSign(groupId:number) {
                    if(groupId !== -1){
                        return this.where((car:CarView)=> {
                            if(car.groupId === groupId){
                                return car.lostgsm;
                            }

                        });
                    }else{
                        return this.where((car:CarView)=> {
                            return car.lostgsm;
                        });
                    }

                }
                //lấy danh sách xe theo gps
                public getCarLostGps(groupId:number) {
                    if(groupId !== -1){
                        return this.where((car:CarView)=> {
                            if(car.groupId === groupId){
                                return !car.statusGps;
                            }

                        });
                    }else{
                        return this.where((car:CarView)=> {
                            return !car.statusGps;
                        });
                    }

                }

                // lấy xe theo Biển số
                public getCarForBs(bs:string) {
                    return this.where((car:CarView)=> {
                        return car.Bs == bs;
                    });
                }

                // lấy xe theo serial
                public getCarForSerial(ser:string) {
                    return this.where((car:CarView)=> {
                        return car.serial == ser;
                    });
                }

                // lấy xe theo tắt máy
                public getCarForShut(groupId:number) {
                    if(groupId !== -1){
                        return this.where((car:CarView)=> {
                            if(car.groupId === groupId){
                                return !car.trangThaiMay && !car.lostgsm;
                            }

                        });
                    }else{
                        return this.where((car:CarView)=> {
                            return !car.trangThaiMay && !car.lostgsm;
                        });
                    }
                }
                // lấy xe theo di chuyển
                public getCarForDrive(groupId:number) {
                    if(groupId !== -1){
                        return this.where((car:CarView)=> {
                            if(car.groupId === groupId){
                                return !car.dungDo && !car.lostgsm;
                            }

                        });
                    }else{
                        return this.where((car:CarView)=> {
                            return !car.dungDo && !car.lostgsm;
                        });
                    }

                }
                // lấy xe theo dừng đổ
                public getCarForStop(groupId:number) {
                    if(groupId !== -1){
                        return this.where((car:CarView)=> {
                            if(car.groupId == groupId){
                                return car.dungDo && !car.lostgsm;
                            }

                        });
                    }else{
                        return this.where((car:CarView)=> {
                            return car.dungDo && !car.lostgsm;
                        });
                    }

                }
                // lấy xe theo id xe
                public getCarForId(ser:string) {
                    return this.where((car:CarView)=> {
                        return car.id == ser;
                    });
                }
                public getCarForGroup(groupId:number){
                    if(groupId !== -1){
                        return this.where((car:CarView)=>{
                            return car.groupId == groupId;
                        });
                    }else{
                        return this.allCars.Values();
                    }
                }
                public CreateMenu(menu:any){

                }
                public cartrip(x:number){
                    alert('hello');
                }
                public getAlertMessage(){
                    return this.where((car:CarView)=>{
                        return car.MessageAlert !== null;
                    });
                }

            }
        }
    }
}