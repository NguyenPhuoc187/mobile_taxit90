/**
 * Created by phamn on 21/05/2016.
 */
/// <reference path="markerwithlabel.lib.ts" />

module App{
    export module Shared{
        export module Library{
            export class Point {
                name:string;
                radius:any;
                position:any;
                id:any;
                map:any;
                menu:any;
                value:any;
                marker:any;
                cricle:any;
                positionDrag:any;
                countInPoint:any;
                eventRightClickMarker:any;
                eventClickMarker:any;
                eventdrag:any;
                eventdragend:any;

                onDelete:any;
                onEdit:any;
                contentpoint:any;

                infowindow: any;

                constructor(map_:google.maps.Map,rad:any, pos:any, id:any, name:any,_data:any){
                    this.map = map_;
                    this.name = name;
                    this.radius=rad;
                    this.position=pos;
                    this.id=id;
                    this.value=_data;
                    this.countInPoint=0;
                    this.infowindow = new google.maps.InfoWindow();
                    this.marker = new google.maps.Marker({
                        position:this.position,
                        title: this.name,
                        // map: this.map,
                        icon:'../../assets/images/map/iconPointCricle.png',

                        // labelContent: this.name,
                        // labelAnchor: new google.maps.Point(22, 0),
                        // labelClass: "labelContentCircle", // the CSS class for the label
                        // labelStyle: {opacity: 0.75},
                        draggable: true
                    }) ;
                    this.cricle = new google.maps.Circle({
                        center: this.position,
                        radius: this.radius,
                        strokeColor: "black",
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: "gray",
                        fillOpacity: 0.35,
                        // map: this.map
                    });
                }
                createMenu(){
                    var contextMenu_circle:any = {};
                    contextMenu_circle.classNames = { menu: 'context_menu', menuSeparator: 'context_menu_separator' };
                    var menuItems_circle:any = [];
                    menuItems_circle.push({ className: 'context_menu_item', eventName: 'editPointCircle', label: 'Chỉnh sửa' });
                    menuItems_circle.push({ className: 'context_menu_item', eventName: 'deleteCircle', label: 'Xoá điểm' });
                    menuItems_circle.push({ className: 'context_menu_item', eventName: 'hideCircle', label: 'Ẩn điểm' });
                    contextMenu_circle.menuItems = menuItems_circle;

                    return new ContextMenu(contextMenu_circle);
                }
                /*cài đặt các thông tin của 1 điểm*/
                public initialize(){
                    /*tạo thông tin marker*/
                    // this.marker = new google.maps.Marker({
                    //     position:this.position,
                    //     title: this.name,
                    //     // map: this.map,
                    //     icon:'../../assets/images/map/iconPointCricle.png',
                    //     //labelContent: this.name,
                    //     //labelAnchor: new google.maps.Point(22, 0),
                    //     //labelClass: "labelContentCircle", // the CSS class for the label
                    //     //labelStyle: {opacity: 0.75},
                    //     draggable: true
                    // }) ;
                    /*thông tin bán kính*/
                    // this.cricle = {
                    //     center: this.position,
                    //     radius: this.radius,
                    //     strokeColor: "#FF0000",
                    //     strokeOpacity: 0.8,
                    //     strokeWeight: 2,
                    //     fillColor: "#FF0000",
                    //     fillOpacity: 0.35,
                    //     map: this.map
                    // };

                    /*thong tin menu*/
                    this.menu=this.createMenu();
                    this.menu.setMapInstance(this.map);
                    /*thông tin điểm (tên hiển thị trên bản đồ)*/

                    /*cài đặt các event*/

                    /*event left click của marker (hiển thị bảng thông tin điểm )*/
                    this.eventClickMarker = google.maps.event.addListener(this.marker, 'click',(ev:any)=>{
                        var showinfoWindowPoint = new google.maps.InfoWindow({content: this.contentpoint});
                        showinfoWindowPoint.setPosition(this.position);
                        showinfoWindowPoint.open(this.map);
                    });

                    this.eventRightClickMarker = google.maps.event.addListener(this.cricle, 'rightclick',(ev:any)=>{
                        this.menu.show(ev.latLng);
                    });

                    this.eventdragend=google.maps.event.addListener(this.marker, 'drag',(ev:any)=>{
                        this.cricle.setCenter(ev.latLng);
                        //alert(ev.latLng);
                    });
                    this.eventdrag=google.maps.event.addListener(this.marker, 'dragend',(ev:any)=>{
                        this.cricle.setCenter(ev.latLng);
                        this.positionDrag=ev.latLng;
                        this.onEdit(this);
                        // alert(ev.latLng)
                    });

                    //gọi sự kiện cho cho menu circle
                    this.menu.menuEventTrigger= (name:any)=>{
                        switch (name) {
                            case 'editPointCircle':
                                this.onEdit(this);
                                break;
                            case 'deleteCircle':
                                this.onDelete(this);
                                break;
                            case 'hideCircle':
                                this.hide();
                                break;
                        }
                        this.menu.hide();
                    };

                    /*thông tin xử lý phần menu*/

                    /*   sự kiện xoá*/

                }
                /*hiển thị hoặc ẩn thông tin menu*/
                public show(){
                    this.marker.setMap(this.map);
                    this.cricle.setMap(this.map);
                    this.initialize();
                }
                public focus(){

                    this.map.setCenter(this.position);
                    this.map.setZoom(16);
                    this.showinfowindow();
                }
                /*ản điểm */
                public hide(){
                    this.marker.setMap(null);
                    this.cricle.setMap(null);
                }
                /*chỉnh sửa lại điểm cần chọn*/
                editPointCircle(titlle:any,rad:any){
                    /*event right click của marker (hiển thị menu)*/

                    this.marker.setTitle(titlle);
                    this.marker.setOptions({labelContent:titlle});
                    this.cricle.setRadius(rad);
                 }

                /*giải phóng tài nguyên*/
                dispose(){
                    delete this.marker;
                    delete this.cricle;
                    google.maps.event.removeListener(this.eventRightClickMarker);
                    google.maps.event.removeListener(this.eventClickMarker);
                    google.maps.event.removeListener(this.eventdragend);
                    google.maps.event.removeListener(this.eventdrag);
                }
                // tính số lượng xe trên điểm
                contains(latlng:any){
                    return this.cricle.getBounds().contains(latlng);
                };
                checkCarInPoint(arrayCar:any){
                    this.countInPoint=0;
                    angular.forEach(arrayCar,(car:any)=>{
                        if(this.contains(car.latlng))
                            this.countInPoint++;
                    });
                }

                showinfowindow(){
                    this.contentpoint='Tên: '+ `<span style="font-weight: 600" class="text-primary">${this.name}</span>` + '</br>' +
                        'Bán kính: ' +this.radius+'m' +'</br>';

                    this.infowindow.setContent(this.contentpoint);
                    // this.infowindow.setPosition(this.position);
                    this.infowindow.open(this.map, this.marker);
                }
                closeinfo(){
                    this.infowindow.close();
                }

            }
        }
    }
}




