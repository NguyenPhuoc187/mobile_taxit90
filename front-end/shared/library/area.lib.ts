/**
 * Created by phamn on 20/05/2016.
 */

module App{
    export module Shared{
        export module Library{
            export class Area{

                protected name:any;
                description:any;
                speed:any;
                poly:any=null;
                id:any;
                map:any ;
                menu:any;
                carContains:number;
                polygons:any;
                markers: any;
                contentArea:string;

                polyline:any;
                eventClickMap:any;
                eventChangeMouse:any;
                eventClickPolyline:any;
                eventClickPolygon:any;
                eventRightClickPolygon:any;
                onShow:any;
                onDelete:any;
                infoWindowArea:any;
                constructor(map_:google.maps.Map){
                    this.map = map_;
                    this.name = '';

                }
                /*Khai báo thông tin menu khi click vào vùng*/
                createMenuArea(){
                    var contextMenu_Area:any = {};
                    contextMenu_Area.classNames = { menu: 'context_menu', menuSeparator: 'context_menu_separator' };
                    var menuItems_Area:any = [];
                    menuItems_Area.push({ className: 'context_menu_item', eventName: 'deleteArea', label: 'Xoá vùng' });
                    menuItems_Area.push({ className: 'context_menu_item', eventName: 'hideArea', label: 'Ẩn vùng' });
                    contextMenu_Area.menuItems = menuItems_Area;
                    return new ContextMenu(contextMenu_Area);
                };

                //cài đặt thông tin vùng
                infoArea(){
                    this.eventChangeMouse=google.maps.event.addListener(this.map,'mousemove',(event:any) =>{
                        this.map.setOptions({ draggableCursor: 'crosshair' });
                    });
                    var polyOptions = {
                        strokeColor: 'black',
                        strokeOpacity: 1.0,
                        strokeWeight: 3
                    };
                    this.poly = new google.maps.Polyline(polyOptions);
                    this.poly.setMap(this.map);

                    var firstClick=false;
                    // Add a listener for the click event
                    this.eventClickMap=google.maps.event.addListener(this.map, 'click',(event:any)=>{

                        if(!firstClick){firstClick=true ; return;}
                        this.polyline = this.poly.getPath();

                        // Because path is an MVCArray, we can simply append a new coordinate
                        // and it will automatically appear.
                        this.polyline.push(event.latLng);

                    });

                    this.eventClickPolyline=google.maps.event.addListener(this.poly,'click',(event:any)=>{
                        // giải phóng sự kiện click map
                        this.add(this.polyline);
                        google.maps.event.removeListener(this.eventClickMap);
                        //giải phóng sự kiện đổi con trỏ chuột
                        google.maps.event.removeListener(this.eventChangeMouse);
                        // đổi trạng thái chuột về mặc định
                        this.map.setOptions({ draggableCursor: 'default' });
                        document.getElementById("menu_item_addArea").innerHTML = 'Thêm vùng';
                        // hiển thị bảng thêm điểm
                        this.poly.setMap(null);
                        this.show();
                        this.onShow(this);
                    });

                }
                add(polyline:any){
                    this.menu=this.createMenuArea();
                    this.menu.setMapInstance(this.map);
                    this.polygons = new google.maps.Polygon({
                        paths: polyline,
                        strokeColor: 'black',
                        strokeOpacity: 0.8,
                        strokeWeight: 3,
                        fillColor: 'gray',
                        fillOpacity: 0.35
                    });

                    this.infoWindowArea = new google.maps.InfoWindow();
                    this.eventClickPolygon=google.maps.event.addListener(this.polygons,'click',(eve:any) =>{
                        this.infoWindowArea.setPosition(eve.latLng);
                        this.infoWindowArea.open(this.map);
                    });
                    //bắt sự kiện nhấn chuột phải hiển thị menu delete
                    this.eventRightClickPolygon=google.maps.event.addListener(this.polygons,'rightclick',(event:any)=>{
                        this.menu.show(event.latLng);
                    });
                    this.menu.menuEventTrigger= (name:any) =>{
                        switch (name) {
                            case 'deleteArea':
                                this.onDelete(this);
                                break;
                            case 'hideArea':
                                this.hide();
                                break;
                        }
                        this.menu.hide();
                    };
                }
                getBounds(){
                    var bounds = new google.maps.LatLngBounds();
                    var paths=this.polygons.getPath();
                    angular.forEach(paths,(p:any)=>{
                        bounds.extend(p);
                    });
                    return bounds;
                };

                //show vùng
                public show(){
                    
                    this.polygons.setMap(this.map);
                };
                //focus map toi vung
                public focus(){
                    var showinfoWindowArea = new google.maps.InfoWindow({content: this.contentArea});
                    this.map.setCenter(this.polygons.getPath().getAt(1));
                    this.map.setZoom(12);
                    showinfoWindowArea.setPosition(this.polygons.getPath().getAt(1));
                    showinfoWindowArea.open(this.map);
                    // this.infoWindowArea.open(this.map);
                };


            // an vung
                hide(){
                if(this.infoWindowArea != undefined){
                    this.infoWindowArea.setMap(null);
                }
                this.polygons.setMap(null);
                };
                addInfoArea(name:string,description:string,speed:any,id:any){
                    this.name=name;
                    this.description=description;
                    this.speed=speed;
                    this.id=id;
                    this.contentArea='Tên: '+ this.name+ '</br>' +
                        'Tốc độ: ' +this.speed+'Km/h' +'</br>' +
                        'Mô tả: ' +this.description;
                    this.infoWindowArea.setContent(this.contentArea);
                };
                //huỷ vẻ vùng
                delete(){
                this.poly.setMap(null);
                google.maps.event.removeListener(this.eventRightClickPolygon);
                google.maps.event.removeListener(this.eventClickPolygon);
                google.maps.event.removeListener(this.eventClickMap);
                google.maps.event.removeListener(this.eventChangeMouse);
                this.map.setOptions({ draggableCursor: 'default' });
                };
                /*giải phóng tài nguyên*/
                dispose(){
                delete this.poly;
                delete this.polygons;
                google.maps.event.removeListener(this.eventRightClickPolygon);
                google.maps.event.removeListener(this.eventClickPolygon);
                };
                /*xoá vùng*/
                aDelete(){
                this.hide();
                }

                contains(latlng:any){
                    return this.getBounds().contains(latlng);
                }

                checkCarInArea(arrayCar:any){
                    this.carContains=0;
                    angular.forEach(arrayCar,(car:any)=>{
                        if(this.contains(car.latlng))
                            this.carContains++;
                    });
                }


            }
        }
    }
}




