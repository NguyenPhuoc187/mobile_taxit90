/**
 * Created by user on 04/12/2016.
 */

///<reference path="../../tstyping/angular-ui-router/angular-ui-router.d.ts"/>
module App {
    export module Shared {


        export class SysRouterProvider implements ng.IServiceProvider {
            public static $inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider'];

            public $get:($location:ng.ILocationService,
                         $rootScope:ng.IRootScopeService,
                         $state:ng.ui.IStateService,
                         $browser:ng.IBrowserService,global:IGlobalService)=>IRouterHelper;

            constructor(public $locationProvider:ng.ILocationProvider,
                        public $stateProvider:ng.ui.IStateProvider,
                        public $urlRouterProvider:ng.ui.IUrlRouterProvider) {
                function supportsHistoryApi() {
                    return !!(window.history && history.pushState);
                }

                if (supportsHistoryApi()) {
                    $locationProvider.html5Mode(true);
                    $locationProvider.hashPrefix("!");
                }

                this.$get = router;// ['$locationProvider', function ($locationProvider){}];//($location, $rootScope, $state, $browser, $stateProvider, $urlRouterProvider);

                function router($location:ng.ILocationService,
                                $rootScope:ng.IRootScopeService,
                                $state:ng.ui.IStateService,
                                $browser:ng.IBrowserService,global:IGlobalService):IRouterHelper {


                    return new SysRouter($location, $rootScope, $state, $browser, $stateProvider, $urlRouterProvider,global);
                }

                router.$inject = ['$location', '$rootScope', '$state', '$browser','global'];
            }
        }

        export interface IRouterHelper {
            configureStates(cfg:IConfig, otherwisePath?:string): void;
            getStates():ng.ui.IState[];
        }

        export interface IRouterConfigInfo extends ng.ui.IState {
            controller: Function | string | Array<string | Function>|any;
            url: string | ng.ui.IUrlMatcher;
            templateUrl: string | { (params:ng.ui.IStateParamsService): string };
        }

        export interface IConfig {
            name: string;
            config: IRouterConfigInfo;
        }


        export class SysRouter implements IRouterHelper {
            hasOtherwise = false;
            //config = {
            //    docTitle: undefined,
            //    resolveAlways: {}
            //};
            constructor(public $location:ng.ILocationService,
                        public $rootScope:ng.IRootScopeService,
                        public $state:ng.ui.IStateService,
                        public $browser:ng.IBrowserService,
                        public $stateProvider:ng.ui.IStateProvider,
                        public $urlRouterProvider:ng.ui.IUrlRouterProvider,
                        public global:IGlobalService
            ) {
                this.handleRoutingErrors();
                /*event báo lỗi nếu router sai*/
                this.updateDocTitle();
                /*event cập nhật title khi có sự kiện change page*/
                this.checkRedirectController();
                //alert("abc");

            }

            public configureStates(cfg:IConfig, otherwisePath:string) {
                this.$stateProvider.state(cfg.name, cfg.config);
                if (otherwisePath && !this.hasOtherwise) {
                    this.hasOtherwise = true;
                    this.$urlRouterProvider.otherwise(otherwisePath);
                }

            }

            private handleRoutingErrors() {
                this.$rootScope.$on("$stateChangeError",
                    (event:ng.IAngularEvent, toState:any, toParams:any, fromState:any, fromParams:any, error:any) => {
                        var destination = (toState &&
                            (toState.title || toState.name || toState.loadedTemplateUrl)) ||
                            "unknown target";
                        var msg = `Error routing to ${destination}. ${error.data || ""}.${error.statusText || ""}: ${error.status || ""}`;
                        console.error(msg);
                        //this.$location.path("/");
                    }
                );
            }

            public getStates() {
                return this.$state.get();
            }

            private updateDocTitle() {
                var sef=this;
                this.$rootScope.$on("$stateChangeSuccess",
                    (event:ng.IAngularEvent, toState:any, toParams:any, fromState:any, fromParams:any) => {
                        /*Kiểm tra quyền sử dụng của tài khoản ở đây*/
                        //this.$rootScope. = toState.title; // data bind to <title>
                        sef.global.PageTite.SetValue(toState.tag.title);
                        sef.global.MenuSelect.SetValue(toState.name);
                    }
                );

            }

            // kiểm tra thông tin của controller trước khi cho phép truy cập
            private checkRedirectController() {
                var onRouteChangeOff = this.$rootScope.$on("$stateChangeStart", routeChange);
                var requestState = "";

                var sef=this;
                function routeChange(event:any, toState:any, toParams:any) {
                    sef.global.PageTite.SetValue(toState.tag.title);
                    sef.global.MenuSelect.SetValue(toState.name);
                    return;
                }
            }
        }

        AdModule.provider('sysRouter', SysRouterProvider);


    }
}