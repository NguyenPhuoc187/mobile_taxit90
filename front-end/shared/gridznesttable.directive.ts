/**
 * Created by phamn on 19/09/2016.
 */
    /// <reference path="./library.ts" />

module App{
    export module Shared{
        export module Directives{
            export interface IGridNestViewBehavior extends IGridViewBehavior{

            }
            export interface IEventNest extends IGridViewEvent{
                onExpand ?:(item:any)=>void;
            }
            export interface IDataNestTableSetting extends IDataTableSetting{
                initRowDetails?: any;
                rowDetails?: boolean;
                rowExpand?:(event:any) =>void;
            }
            export interface IGridNestControl extends IGridViewControl{
                rowdetail:any;
                event_nested: IEventNest;

            }
            export interface IGridNestControlScope extends IGridViewScope{
                id: IGridNestControl;
                dataNestTableSettings: IDataNestTableSetting;
                nestedControl:IGridNestControl;
                checkBox: any;

            }
            export class GridNestTableControl implements IGridNestControl{
                rowdetail:any;
                isExport: boolean;
                filename_excel: string;

                refresh:()=>void;
                clear:()=>void;
                clearSelection:()=>void;
                render:()=>void;
                id: string;
                isFilter:boolean;
                iseditable:boolean;
                isFilterMode: string;
                selectionModeinfo:string;
                columnsHeight:number;
                isAltRows:boolean;
                isshowAggregates:boolean;
                //isCreateHtml:boolean;
                constructor(name: string) {
                    this.id = name;
                    this.behavior= {
                        isEdit: false,
                        isDelete: false,
                        isNew:false,
                        isDetail:false,
                        isRFID: false,
                        isCheckSelect: false,
                    };
                    this.event = {};
                    this.event_nested = {};
                    this.columns = [];
                    this.columnGroups = [];
                    this.data = [];

                    //this.isCreateHtml=true;
                }

                behavior: IGridNestViewBehavior;
                columns: any[];
                columnGroups: any[];
                height:any;
                data: any[];
                event: IGridViewEvent;
                event_nested: IEventNest;
                datasource: string;
                enablebinding: (val: boolean) => void;
                dataEdit: any;
                selectRowInfo:any;
                showcolumn?:any;
            }
            export class GridNestTableController extends GridViewController{
                constructor($scope: IGridNestControlScope, $http: ng.IHttpService,$window:ng.IWindowService, $compile:ng.ICompileService) {
                    super($scope,$http,$window,$compile);

                    console.log('log table');
                    $scope.gridInstance = {};
                    //console.log($scope.id);

                    $scope.control = $scope.id;
                    $scope.nestedControl = $scope.id as GridNestTableControl;

                    //console.log($scope.control);
                    //var grid,deleteButtons, editButtons;

                    //$scope.createWidget=$scope.control.isCreateHtml;

                    // chuyển ngôn ngữ các từ mặc định của grid-view
                    var getLocalization:any = ()=>{
                        var localizationobj:any = {
                            pagergotopagestring: "Trang:",
                            pagershowrowsstring: " Hiển thị:",
                            pagerrangestring: " / ",
                            pagerpreviousbuttonstring: "Quay lại",
                            pagernextbuttonstring: "Tiếp theo",
                            pagerfirstbuttonstring: "Đầu tiên",
                            pagerlastbuttonstring: "Cuối cùng",
                            filterapplystring: "Đồng ý",
                            filtercancelstring: "Hủy",
                            filterclearstring: "Xóa tìm kiếm",
                            filterstring: "<p class='text-success' style='margin-left: 19px'>Tìm kiếm nâng cao</p>",
                            filtersearchstring: "Tìm kiếm:",
                            filterstringcomparisonoperators: ['Không lọc', 'Tất cả', 'Dữ liệu', 'Dữ liệu(theo ký tự)',
                                'Không trùng với dữ liệu', 'Không trùng với dữ liệu(theo ký tự)', 'Ký tự bắt đầu',
                                'Ký tự cuối', 'Bằng giá trị'],
                            filternumericcomparisonoperators: ['Bằng', 'Không Bằng', 'Nhỏ hơn', 'Nhỏ hơn hoặc bằng', 'Lớn hơn', 'Lớn hơn hoặc bằng', 'null', 'not null'],
                            filterdatecomparisonoperators: ['Bằng', 'Không Bằng', 'Nhỏ hơn', 'Nhỏ hơn hoặc bằng', 'Lớn hơn', 'Lớn hơn hoặc bằng', 'null', 'not null'],
                            filterbooleancomparisonoperators: ['equal', 'not equal'],
                            validationstring: "Entered value is not valid",
                            emptydatastring: "Không có dữ liệu để hiển thị",
                            filterselectstring: "Select Filter",
                            loadtext: "Đợi trong giây lát....",
                            clearstring: "Xóa",
                            todaystring: "Hôm nay",
                        };
                        return localizationobj;

                    };
                    //chi tiết của từng row


                    var grid:any;

                    $scope.dataNestTableSettings =
                    {

                        width: "100%",
                        rowDetails:true,
                        created:(args)=> {
                            grid = args.instance;
                            // grid.columnsHeight
                        },
                        initRowDetails: $scope.nestedControl.rowdetail,
                        height:$scope.nestedControl.height,
                        pageable: true,
                        pagerMode: 'advanced',
                        showAggregates: $scope.nestedControl.isshowAggregates,
                        altRows : $scope.nestedControl.isAltRows,
                        filterable:$scope.nestedControl.isFilter,
                        filterMode: $scope.nestedControl.isFilterMode,
                        enableHover: true,
                        pagerButtonsCount: 10,
                        scrollBarSize:10,

                        pageSizeOptions: ['20', '50','100', '200','500','1000'],
                        pageSize: 20,
                        editable: $scope.nestedControl.iseditable,
                        enableBrowserSelection: true, // bặt chế độ copy cho grid-view
                        columnsResize: true, // bật chế độ thay đổi kích thước cho cột
                        columns: [],
                        columnGroups: [],
                        columnsHeight:$scope.nestedControl.columnsHeight,
                        sortable: true,
                        localization: getLocalization(),
                        // exportSettings: {},
                        selectionMode: $scope.nestedControl.selectionModeinfo,
                        autoRowHeight:true,
                        // selectionmode: 'checkbox',
                        //selectionMode: 'singleRow',
                        rendered:()=>{
                            //return;
                            try {
                                $("." + "addIcon_" + $scope.id.id).bind('click', function(e){
                                    e.preventDefault();
                                    //$('#myModal').modal('show');
                                });
                                $("." + "editIcon_" + $scope.id.id).bind('click', function(e){
                                    e.preventDefault();

                                });
                                $("." + "deleteIcon_" + $scope.id.id).bind('click', function(e){
                                    e.preventDefault();
                                    //alert('abv');
                                    //$('#myModal').modal('show');
                                });
                                $("." + "detailIcon_" + $scope.id.id).bind('click', function(e){
                                    e.preventDefault();
                                    //alert('abv');
                                    //$('#myModal').modal('show');
                                });
                                $("." + "RFIDIcon_" + $scope.id.id).bind('click', function(e){
                                    e.preventDefault();
                                });
                                $("." + "addIcon_" + $scope.id.id).on('click', function(event:any){
                                    //var rowIndex = parseInt(event.currentTarget.getAttribute('data-row'));
                                    //if (isNaN(rowIndex)) {
                                    //    return;
                                    //}
                                    //var old = dataCfg.localData[rowIndex];
                                    //console.log(old);
                                    if($scope.nestedControl.event_nested != null)
                                        $scope.nestedControl.event_nested.onNew();
                                });
                                $("." + "editIcon_" + $scope.id.id).on('click', function (event) {
                                    var rowIndex = parseInt(event.currentTarget.getAttribute('data-row'));
                                    if (isNaN(rowIndex)) {
                                        return;
                                    }
                                    var old=dataCfg.localData[rowIndex];
                                    //console.log(old);
                                    if( $scope.nestedControl.event_nested!=null)
                                        $scope.nestedControl.event_nested.onEdit(old,old);
                                    //dataCfg.localData.splice(rowIndex,1);

                                    // $scope.dataTableSettings.apply('updateBoundData'); // update maunual du lieu moi
                                    //grid.endRowEdit(rowIndex, true);
                                });

                                $("." + "deleteIcon_" + $scope.id.id).click(function (event) {

                                    var rowIndex = parseInt(event.currentTarget.getAttribute('data-row'));
                                    //console.log(rowIndex);
                                    if (isNaN(rowIndex)) {
                                        return;
                                    }
                                    var old=dataCfg.localData[rowIndex];
                                    if( $scope.nestedControl.event_nested!=null){
                                        $scope.nestedControl.event_nested.onDelete(old);
                                    }

                                });
                                $("." + "detailIcon_" + $scope.id.id).on('click', function(event:any){
                                    var rowIndex = parseInt(event.currentTarget.getAttribute('data-row'));
                                    if (isNaN(rowIndex)) {
                                        return;
                                    }
                                    var old=dataCfg.localData[rowIndex];
                                    if($scope.nestedControl.event != null){
                                        $scope.nestedControl.event.onDetail(old);
                                    }

                                });
                                $("." + "RFIDIcon_" + $scope.id.id).on('click', function(event:any){
                                    var rowIndex = parseInt(event.currentTarget.getAttribute('data-row'));
                                    if (isNaN(rowIndex)) {
                                        return;
                                    }
                                    var old=dataCfg.localData[rowIndex];
                                    if($scope.nestedControl.event != null){
                                        $scope.nestedControl.event.onRFID(old);
                                    }

                                });
                            }
                            catch(e){

                            }
                        },
                        bindingComplete: () => {
                            if( $scope.nestedControl.event.onBindingComplete != null){
                                $scope.nestedControl.event.onBindingComplete();
                            }
                        },
                        rowDoubleClick: (event) => {
                            try {
                                var args = event.args;
                                // row data.
                                var row = args.row;

                                if($scope.nestedControl.event != null){
                                    $scope.nestedControl.event.onDoubleClick(row);
                                }
                            } catch(e) {

                            }
                        },
                        rowClick: (event) => {
                            try {
                                var args = event.args;
                                // row data.
                                var row = args.row;

                                if($scope.nestedControl.event != null){
                                    $scope.nestedControl.event.onClick(row);
                                }
                            } catch(e) {

                            }
                        },
                        rowExpand: (event) => {
                            try {
                                var args = event.args;
                                // row data.
                                var row = args.row;

                                if($scope.nestedControl.event_nested != null){
                                    $scope.nestedControl.event_nested.onExpand(row);
                                }
                            } catch(e) {

                            }
                        }
                    };
                    //xử lý select row
                    $scope.nestedControl.selectRowInfo = () =>{
                        // gets selected row indexes. The method returns an Array of indexes.
                        var selection = grid.getSelection();
                        return selection;
                    };

                    // Xử lý khi nhấn nút export sang excel

                    $scope.excelExportClick = () => {

                        var today = new Date(),
                            time = today.toTimeString().split(':').join('').substr(0, 4),
                            timestamp = getDate('dd-mm-yyyy', today);
                        var title= $scope.nestedControl.filename_excel;
                        var data=  $scope.nestedControl.data;
                        var nameheader:Array<any> = [];//$scope.columns;
                        // if(data.length===0) {
                        //     ul.message.warning("Chưa có dữ liệu để xuất excel");
                        //     return;
                        // }
                        angular.forEach($scope.dataNestTableSettings.columns,function(c){
                            nameheader.push(c);//headerlenght++;
                        });

                        var exportExcel = new App.Shared.Library.ExportExcell();
                        exportExcel.AddInfoHeader(title,nameheader.length,null);
                        exportExcel.AddInfoTopRight("ANH DUONG COMPANY",nameheader.length,null);
                        for(var i=0;i<nameheader.length;i++){
                            //if(nameheader[i].visible)
                            exportExcel.AddInfoColumn(nameheader[i].text,null);
                        }

                        for(var i=0;i<data.length;i++) {
                            exportExcel.AddInfoRow(mergeExcelData(data[i]),null);
                        }
                        exportExcel.buildTable('exportable');
                        var blob = new Blob([document.getElementById('exportable').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8"
                        });

                        saveAs(blob, title+"_"+timestamp+".xls");
                        /*Hàm mergeexcell*/
                        function mergeExcelData(data:any){
                            var result:any={};
                            angular.forEach($scope.dataNestTableSettings.columns,function(c){
                                //if(c.visible==true) {
                                if(data[c.dataField]==null) data[c.dataField]="";
                                if (result[c.dataField] == undefined && data != null && data != undefined) {
                                    result[c.dataField] = data[c.dataField];
                                }
                                //}
                            });
                            console.debug(result);
                            return result;
                        }
                        function getDate (mode:any, userdate:any) {
                            var dte = userdate || new Date(),
                                d = dte.getDate().toString(),
                                m = (dte.getMonth() + 1).toString(),
                                yyyy = dte.getFullYear().toString(),
                                dd = (d.length < 2) ? '0' + d : d,
                                mm = (m.length < 2) ? '0' + m : m,
                                yy = yyyy.substring(2, 4);
                            switch (mode) {
                                case 'dd-mm-yyyy': return dd + '-' + mm + '-' + yyyy;
                                case 'yyyymmdd': return yyyy + mm + dd;
                                default: return dte;
                            }
                        }
                    };

                    /*Xử lý nút in*/
                    $scope.print = () => {
                        grid.exportSettings = {fileName: null} ;
                        var gridContent = grid.exportData('html');
                        var newWindow = window.open('', '', 'width=800, height=500'),
                            document = newWindow.document.open(),
                            pageContent =
                                '<!DOCTYPE html>' +
                                '<html>' +
                                '<head>' +
                                '<meta charset="utf-8" />' +
                                '<title>jqwidgets Grid</title>' +
                                '</head>' +
                                '<body>' + gridContent + '</body></html>';
                        document.write(pageContent);
                        document.close();
                        newWindow.print();
                    };


                    var dataCfg = {
                        dataFields: Array<any>(),
                        dataType: 'array',
                        localData : Array<any>()
                    };
                    var updatingSelection = false;
                    var updatingSelectionFromGrid = false;


                    // $scope.dataNestTableSettings.columns.push({
                    //     text: "",
                    //     width:40,
                    //     cellsAlign: 'center',
                    //     align: "center",
                    //     columnType: 'none',
                    //     editable: false,
                    //     sortable: false,
                    //     filterable:false,
                    //     dataField: null,
                    //     cellsRenderer: function (row:number, column:any, value:any) {
                    //         // render custom column.
                    //         return "<a class='" + "editIcon_" + $scope.id.id +"' href='#' id='" + "editIcon_" + $scope.id.id +"' data-row='" + row + "'><i style='font-size:1.5em' class=' glyphicon glyphicon-edit text-success' title='Cập Nhật'></i></a> ";
                    //     }});


                    //todo: phải thêm id vào để phân biệt và xác định khi kick từng grid-view. lưu ý thêm id vào
                    if($scope.id.behavior.isNew === true && $scope.id.behavior.isEdit === true && $scope.id.behavior.isDelete === true ) {
                        $scope.dataNestTableSettings.columns.push({
                            text: "<a  class='" + "addIcon_" + $scope.id.id +" tooltitle_grid glyphicon glyphicon-plus text-center ' href='#' data-original-title='Thêm mới' data-toggle='tooltip' data-placement='bottom' title='Thêm mới' style='font-size: 1.5em; color: #65C3DF'></a> ",
                            width:80,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            editable: false,
                            sortable: false,
                            filterable:false,
                            dataField: null,
                            cellsRenderer: function (row:number, column:any, value:any) {
                                // render custom column.
                                return "<a class='" + "editIcon_" + $scope.id.id +"' id='" + "editIcon_" + $scope.id.id +"' href='#' data-row='" + row + "'><i style='font-size:1.5em' class=' glyphicon glyphicon-edit text-success' title='Cập Nhật'></i></a> " +
                                    "<a class='" + "deleteIcon_" + $scope.id.id +"' id='" + "deleteIcon_" + $scope.id.id +"' href='#' data-row='" + row + "'><i style='font-size:1.5em' class='glyphicon glyphicon-remove text-danger' title='Xóa'></i></a>" ;
                            }});
                    }
                    if($scope.id.behavior.isNew === true && $scope.id.behavior.isEdit === true && $scope.id.behavior.isDelete === false ) {
                        $scope.dataNestTableSettings.columns.push({
                            text: "<a  class='" + "addIcon_" + $scope.id.id +" tooltitle_grid glyphicon glyphicon-plus text-center ' href='#' data-original-title='Thêm mới' data-toggle='tooltip' data-placement='bottom' title='Thêm mới' style='font-size: 1.5em; color: #65C3DF'></a> ",
                            width:80,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            editable: false,
                            sortable: false,
                            filterable:false,
                            dataField: null,
                            cellsRenderer: function (row:number, column:any, value:any) {
                                // render custom column.
                                return "<a class='" + "editIcon_" + $scope.id.id +"' href='#' id='" + "editIcon_" + $scope.id.id +"' data-row='" + row + "'><i style='font-size:1.5em' class=' glyphicon glyphicon-edit text-success' title='Cập Nhật'></i></a> ";
                            }});
                    }
                    if($scope.id.behavior.isNew === false && $scope.id.behavior.isEdit === true && $scope.id.behavior.isDelete === false ) {
                        $scope.dataNestTableSettings.columns.push({
                            text: "",
                            width:40,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            editable: false,
                            sortable: false,
                            filterable:false,
                            dataField: null,
                            cellsRenderer: function (row:number, column:any, value:any) {
                                // render custom column.
                                return "<a class='" + "editIcon_" + $scope.id.id +"' href='#' id='" + "editIcon_" + $scope.id.id +"' data-row='" + row + "'><i style='font-size:1.5em' class=' glyphicon glyphicon-edit text-success' title='Cập Nhật'></i></a> ";
                            }});
                    }
                    if($scope.id.behavior.isNew === false && $scope.id.behavior.isEdit === true && $scope.id.behavior.isDelete === true ) {
                        $scope.dataNestTableSettings.columns.push({
                            text: "",
                            width:80,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            filterable:false,
                            editable: false,
                            sortable: false,
                            dataField: null,
                            cellsRenderer: function (row:number, column:any, value:any) {
                                // render custom column.
                                return "<a class='" + "editIcon_" + $scope.id.id +"' href='#' id='" + "editIcon_" + $scope.id.id +"' data-row='" + row + "'><i style='font-size:1.5em' class=' glyphicon glyphicon-edit text-success' title='Cập Nhật'></i></a> " +
                                    "<a class='" + "deleteIcon_" + $scope.id.id +"' id='" + "deleteIcon_" + $scope.id.id +"' href='#' data-row='" + row + "'><i style='font-size:1.5em' class='glyphicon glyphicon-remove text-danger' title='Xóa'></i></a>" ;;
                            }});
                    }
                    if($scope.id.behavior.isDetail === true){
                        $scope.dataNestTableSettings.columns.push({
                            text: "Chi tiết nhiên liệu",
                            width:150,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            editable: false,
                            sortable: false,
                            filterable:false,
                            dataField: null,
                            cellsRenderer: function (row:number, column:any, value:any) {
                                // render custom column.
                                return "<a class='" + "detailIcon_" + $scope.id.id +"' href='#' data-row='" + row + "'><button class='btn btn-info'>Hiển thị</button></a>";
                            }});
                    }
                    /*Chức năng RFID của tài xế*/
                    if($scope.id.behavior.isRFID === true){
                        $scope.dataNestTableSettings.columns.push({
                            text: "Ghi thẻ/Thiết lập tx",
                            width:110,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            editable: false,
                            sortable: false,
                            filterable:false,
                            dataField: null,
                            cellsRenderer: function (row:number, column:any, value:any) {
                                // render custom column.
                                return "<a class='" + "RFIDIcon_" + $scope.id.id +"' href='#' data-row='" + row + "'><button class='btn btn-info'>Ghi thẻ</button></a>";
                            }});
                    }
                    $scope.nestedControl.columnGroups.forEach(value => {
                        var text_ = value.text == undefined ?'group':value.text;
                        var name_ = value.name == undefined ?'group':value.name;

                        $scope.dataNestTableSettings.columnGroups.push({
                            text: text_,
                            name: name_,
                            align: 'center',
                            className: value.className,
                        });
                    });
                    $scope.dataNestTableSettings.columnGroups.push({
                        text: '',
                        name: '',
                        align: 'center',
                    });
                    $scope.nestedControl.columns.forEach(val=> {
                        var width_ = val.width;
                        var class_ = val.className;
                        if (val.width == undefined)
                            width_ = 'auto';
                        var columngroup_ = val.columngroup== undefined ? undefined:val.columngroup;

                        $scope.dataNestTableSettings.columns.push({
                            text: val.text,
                            dataField: val.dataField,
                            width: width_,
                            align: 'center',
                            aggregates:val.aggregates,
                            aggregatesRenderer:
                            val.aggregatesRenderer,
                            filterable:val.filterable,
                            className: class_,
                            cellClassName: val.cellClassName,
                            hidden:val.hidden,
                            columngroup: columngroup_
                        });

                        dataCfg.dataFields.push({ name: val.dataField, type: val.type })

                    });


                    dataCfg.localData = $scope.nestedControl.data;
                    $scope.nestedControl.refresh=()=>{
                        //delete dataCfg.localData;
                        //dataCfg.localData = $scope.control.data;
                        ////$scope.dataTableSettings.source.setSource(dataCfg);
                        //delete  $scope.dataTableSettings.source;
                        //$scope.dataTableSettings.source=null;
                        //$scope.dataTableSettings.source = new $.jqx.dataAdapter(dataCfg);
                        $scope.dataNestTableSettings.source._source.localData = $scope.nestedControl.data;
                        $scope.dataNestTableSettings.source._source.localdata = $scope.nestedControl.data;
                        if($scope.dataNestTableSettings.apply!=undefined) {
                            $scope.dataNestTableSettings.apply('render');
                            $scope.dataNestTableSettings.apply('clearFilters');

                            //$scope.dataTableSettings.apply('updateBoundData');
                            // update maunual du lieu moi
                            // $scope.dataTableSettings.apply('refresh'); // update maunual du lieu moi
                            // update maunual du lieu moi
                        }
                        //$scope.dataTableSettings.source.dataBind();
                        //if(grid.isBindingCompleted()){
                        //    grid.updateBoundData();
                        //}
                        //delete dataCfg.localData;

                        //$scope.dataTableSettings.source.setSource(dataCfg);
                    };
                    $scope.nestedControl.clear = () =>{
                        $scope.dataNestTableSettings.apply('clear');
                    };
                    // $scope.nestedControl.render = ()=>{
                    //     $scope.dataTableSettings.apply('render');
                    // };
                    $scope.nestedControl.clearSelection = ()=>{
                        $scope.dataNestTableSettings.apply('clearSelection');
                    };
                    $scope.dataNestTableSettings.source = new $.jqx.dataAdapter(dataCfg,{autoBind: true,loadComplete:()=>{
                        //if(grid.isBindingCompleted()){
                        //    grid.updateBoundData();
                        //}
                    }});
                    // $scope.dataTableSettings.apply('updateBoundData');
                    // Xử lý khi nhấn nút print

                    // $scope.id.behavior.isEdit = false;
                    // $scope.id.behavior.isDelete = false ;
                    // $scope.id.behavior.isNew = false;
                    // $scope.id.behavior.isDetail = false;
                }
            }
            export function gridnesttableDirective($templateCache:ng.ITemplateCacheService, $compile:ng.ICompileService, $http:ng.IHttpService): ng.IDirective {
                //If you use transclude;
                //'element' then you have to use replace: true,
                //for somewhat of an explanation see this issue.
                //If you use transclude: true the you can use whatever value you want for replace depending
                //on the end result you want with respect to the original HTML node on which the directive
                //was applied (whether it will still exist or not).
                return {
                    // bindToController: true,
                    controller: ['$scope','$http', GridNestTableController],
                    name: "gridNesttable",
                    restrict: "EA",
                    //transclude: true,// cho phép include đoán html nằm trong thẻ grid-view
                    templateUrl:'./shared/gridnesttable.html',
                    scope: {
                        id:"="
                    },
                    replace: true// không được phép xóa element nằm trong thẻ grid-view
                }
            }
            AdModule.directive('gridNesttable',gridnesttableDirective);
        }
    }
}