

/**
 * Created by user on 04/12/2016.
 */

/// <reference path="../../tstyping/angularjs/angular-cookies.d.ts" />
/// <reference path="Entity/account.e.ts" />

module App{
    export module Shared{
        import IState = angular.ui.IState;
        import IMenu = App.Components.IMenu;
        import IAccount = App.Shared.Entity.IAccount;
        import IRole = App.Shared.Entity.IRole;
        export class VariableWatch{
            private _value:any;
            public Watch:(ne:any)=>void;
            private Notify():void{
                if(this.Watch!=null){
                    try{
                        this.Watch(this._value);
                    }
                    catch (e){
                        console.log(`Watch variable error : ${e}`);
                    }
                }
            }
            public SetValue(val:any){
                this._value=val;
                this.Notify();
            }
            public GetValue<T>():T{
                return this._value;
            }
        }
        export interface IGlobalService{
            PageTite:VariableWatch;
            MenuSelect:VariableWatch;
            Token:VariableWatch;
            Parent:VariableWatch;
            Account:VariableWatch;
            GetMenu: VariableWatch;
            GetTmp: VariableWatch;
            AuthHost:string;
            ReportHost: string;
            CustomDaterange: any;
            SourceLoaiHinhKinhDoanh: any;
            SourceSoGTVT:any;
            GridViewMode: string;
            SingleDate:any;
            IndexReadly:VariableWatch;


        }
        export class GlobalService implements IGlobalService{
            IndexReadly:VariableWatch;
            public AuthHost:string;
            public GridViewMode:string;
            public ReportHost: string;
            public Version: string;
            public CustomDaterange:any;
            public SingleDate: any;
            public SourceLoaiHinhKinhDoanh: any;
            public SourceSoGTVT:any;
            public MenuSelect:VariableWatch;
            public PageTite:VariableWatch;
            public Token:VariableWatch;
            public Account:VariableWatch;
            public GetMenu: VariableWatch;
            public GetTmp: VariableWatch;
            public Parent: VariableWatch;

            //hàm lấy thời gian mặc định
            public getnowdaynotime(){
                var now = new Date();
                var Minute = ("0" + now.getMinutes()).slice(-2);
                // var Hours = ("0" + now.getHours()).slice(-2);
                var day_after = ("0" + (now.getDate()-1)).slice(-2);
                var day_now = ("0" + now.getDate()).slice(-2);
                var month = ("0" + (now.getMonth() + 1)).slice(-2);
                var today:any = (day_after) + "/" + (month) + "/" + now.getFullYear()+ " - " +  (day_now) + "/" + (month) + "/" + now.getFullYear();
                return today;
            }
            public  getnowday() {
                var now = new Date();
                var Minute = ("0" + now.getMinutes()).slice(-2);
                // var Hours = ("0" + now.getHours()).slice(-2);
                var day = ("0" + now.getDate()).slice(-2);
                var month = ("0" + (now.getMonth() + 1)).slice(-2);
                var today:any = "00:00 " + (day) + "/" + (month) + "/" + now.getFullYear() + " - " + "23:59" + " " + (day) + "/" + (month) + "/" + now.getFullYear();
                return today;
            }
            public  getnowday_default() {
                var now = new Date();
                var Minute = ("0" + now.getMinutes()).slice(-2);
                var Hours = ("0" + now.getHours()).slice(-2);
                var day = ("0" + now.getDate()).slice(-2);
                var month = ("0" + (now.getMonth() + 1)).slice(-2);
                var today:any = Hours + ":" + Minute + " " + (month) + "/" + (day) + "/" + now.getFullYear();
                return today;
            }
            public getnowsingleday(){
                var now = new Date();
                var Minute = ("0" + now.getMinutes()).slice(-2);
                var Hours = ("0" + now.getHours()).slice(-2);
                var day = ("0" + now.getDate()).slice(-2);
                var month = ("0" + (now.getMonth() + 1)).slice(-2);
                var today:any = day + "/" + month + "/" + now.getFullYear();
                return today;
            }
            //hàm chuyen thời gian trên daterange picker
            public ChangeTime(time:any){
                var year:string = time.slice(12,16);
                var month:string = time.slice(9,11);
                var date:string = time.slice(6,8);

                var hour:string = time.slice(0,5);

                var tt:any = hour +' ' +month + '/' + date + '/' + year ;
                return tt;
            }

            /*Hàm chuyển thời gian post lên server*/
            public ChangeTime_Server(time:any){
                var year:string = time.slice(15);
                var month:string = time.slice(12,14);
                var date:string = time.slice(9,11);
                var hour:string = time.slice(0,8);
                var tt:any = hour +' ' +month + '/' + date + '/' + year ;
                return tt;
            }
            //hàm chuyển thời gian
            public AdapterDate(time:any){
                var year:string = time.slice(0,4);
                var month:string = time.slice(5,7);
                var date:string = time.slice(8,10);
                var min:string = time.slice(11,19);
                var tt:any = min +' ' +date + '/' + month + '/' + year ;
                return tt;
            }
            //hàm chuyển thời gian
            public AdapterDate_Chart(time:any){
                // var tt = moment(time).format('HH:mm dd/MM/yyyy');
                var year:string = time.slice(0,4);
                var month:string = time.slice(5,7);
                var date:string = time.slice(8,10);
                var min:string = time.slice(11,16);
                // var tt:any = min +' ' +date + '/' + month + '/' + year ;
                var tt:any = min;
                return tt;
            }
            public AdapterSignDate(time:any){
                var year:string = time.slice(0,4);
                var month:string = time.slice(5,7);
                var date:string = time.slice(8,10);
                var tt:any = date + '/' + month + '/' + year ;
                return tt;
            }
            public ChangeSignDate_Server(time:any){
                var year:string = time.slice(6);
                var month:string = time.slice(3,5);
                var date:string = time.slice(0,2);
                var tt:any = month + '/' + date + '/' + year ;
                return tt;
            }
            /*Hàm chuyển dữ liệu chọn xe thành mảng serial để post lên server*/
            public AdapterSerial (serial:any) {
                var tmp:any = serial;
                var result:any = [];
                for (var i = 0; i < tmp.length; i++) {
                    result.push(tmp[i].originalItem.Serial);
                }

                return result;
            }
            public AdapterIDDriver (Id:any){
                var tmp: any = Id;
                var result: any = [];
                for(var i =0; i< tmp.length; i++){
                    result.push(tmp[i].originalItem.Id);
                }
                return result;
            }
            public AdpterChangeLoaiHinhKinhDoanh(value:any){
                var Type_mess:string = '';
                switch (value){
                    case 'Car':
                        Type_mess = 'Xe khách tuyến cố định';
                        break;
                    case 'VanTaiBus':
                        Type_mess = 'Vận tải hành khách bằng xe bus';
                        break;
                    case 'VanTaiKhach':
                        Type_mess = 'Vận tải hành khách theo hợp đồng';
                        break;
                    case 'VanChuyenKhach':
                        Type_mess = 'Vận chuyển khách du lịch bằng ô tô';
                        break;
                    case 'VanChuyenHang':
                        Type_mess = 'Vận tải hàng hóa bằng container';
                        break;
                    case 'XeTai':
                        Type_mess = 'Xe tải';
                        break;
                    case 'Taxi':
                        Type_mess = 'Taxi';
                        break;
                    case 'TaxiTai':
                        Type_mess = 'Taxi tải';
                        break;
                    case 'XeDauKeo':
                        Type_mess = 'Xe đầu kéo';
                        break;
                }
                return Type_mess;
            }
            // hàm chuyển số điện thoại
            public ApdaterChangePhone(value:any){
                var Phone_mess:string = '';
                if(value == null){
                    Phone_mess = '';
                }else {
                    switch (value.slice(0, 1)) {
                        case '+':
                            Phone_mess = `0${value.slice(3)}`;
                            break;
                        case '8':
                            Phone_mess = `0${value.slice(2)}`;
                            break;
                        case ' ':
                            Phone_mess = `0${value.slice(3)}`;
                            break;
                        default:
                            Phone_mess = `0${value}`;
                            break;
                    }
                }
                return Phone_mess;
            }

            public ChangeTimeGetLink(time:any){
                var year:string = time.slice(12);
                var month:string = time.slice(6,8);
                var date:string = time.slice(9,11);

                var hour:string = time.slice(0,5);

                var tt:any = hour +' ' +month + '-' + date + '-' + year ;
                return tt;
            }
            public ChangeLocation(value:any){
                if(value !== null){
                    value.Position = `<span>${value.Lng.toFixed(6)} ; ${ value.Lat.toFixed(6)}</span>`;
                }else{
                    value.Position = '';
                }
                return value.Position;
            }

            public BuildNewMapCar(idmap:string, map:any){
                // đăng ký map thần thánh
                var adMap=new AdMap.map(idmap,map);

                // đăng ký hình ảnh của xe
                // xe có khách
                var imgCokhach=new Image();
                imgCokhach.src='../assets/images/carIcon/run_car.png';
                adMap.registerImage('cokhach',imgCokhach);

                var imgmatdongho=new Image();
                imgmatdongho.src='../assets/images/carIcon/matdongho.png';
                adMap.registerImage('matdongho',imgmatdongho);


                var imgLostgps=new Image();
                imgLostgps.src='../assets/images/carIcon/Lostgps_car.png';
                adMap.registerImage('lostgps',imgLostgps);

                var imgStop_car=new Image();
                imgStop_car.src='../assets/images/carIcon/Stop_car.png';
                adMap.registerImage('kokhach',imgStop_car);

                var imgmoney_car=new Image();
                imgmoney_car.src='../assets/images/carIcon/money_car.png';
                adMap.registerImage('xedung',imgmoney_car);

                var imgLock_car=new Image();
                imgLock_car.src='../assets/images/carIcon/Lock_car.png';
                adMap.registerImage('xedo',imgLock_car);


                var imgbreak_car=new Image();
                imgbreak_car.src='../assets/images/carIcon/break_car.png';
                adMap.registerImage('lostgsm',imgbreak_car);

                var rungifimg = new Image();
                rungifimg.src='../assets/images/carIcon/runtarget.png';

                var xedunghgif = new Image();
                xedunghgif.src='../assets/images/carIcon/1942506.png';


                // var imggif = new Image();
                // imggif.src='../assets/images/carIcon/runtarget.gif';

                // var imggif = new Image();
                // imggif.src='../assets/images/carIcon/1913774.gif';

                // canvas hứng hình gif cho xe có khách được target
                var canvas= document.createElement('canvas');
                var imggif_cartarget:any = [];
                splitgif(rungifimg,(frames:any)=>{
                    for(var i =0; i< frames.length;i++){
                        imggif_cartarget[i] = new Image();
                        imggif_cartarget[i].width = 32;
                        imggif_cartarget[i].height =32;
                        imggif_cartarget[i].index =i;
                        imggif_cartarget[i].frame = frames.length;
                        imggif_cartarget[i].onload=function ( ){
                            // console.log(this.src);
                            adMap.registerImage('cartarget'+this.index,this);
                        };
                        imggif_cartarget[i].src = frames[i].url;
                    }
                    // frames.forEach((f:any,i:number)=>{
                    //     var imggif_cartarget = new Image();
                    //     imggif_cartarget.src = f.url;
                    //     adMap.registerImage(`cartarget${i}`,imggif_cartarget);
                    // })
                },canvas);

                var canvas_xedung= document.createElement('canvas');
                var imggif_xedung:any = [];
                splitgif(xedunghgif,(frames:any)=>{
                    for(var i =0; i< frames.length;i++){
                        imggif_xedung[i] = new Image();
                        imggif_xedung[i].width = 32;
                        imggif_xedung[i].height =32;
                        imggif_xedung[i].index =i;
                        imggif_xedung[i].frame = frames.length;
                        imggif_xedung[i].onload=function ( ){
                            // console.log(this.src);
                            adMap.registerImage('xedungtarget'+this.index,this);
                        };
                        imggif_xedung[i].src = frames[i].url;
                    }
                    // frames.forEach((f:any,i:number)=>{
                    //     var imggif_cartarget = new Image();
                    //     imggif_cartarget.src = f.url;
                    //     adMap.registerImage(`cartarget${i}`,imggif_cartarget);
                    // })
                },canvas_xedung);


                //xe mất lost gps target
                var xelostgpsgif = new Image();
                xelostgpsgif.src='../assets/images/carIcon/lostgpstarget.png';
                var canvas_xelostgps= document.createElement('canvas');
                var imggif_xelostgps:any = [];
                splitgif(xelostgpsgif,(frames:any)=>{
                    for(var i =0; i< frames.length;i++){
                        imggif_xelostgps[i] = new Image();
                        imggif_xelostgps[i].width = 32;
                        imggif_xelostgps[i].height =32;
                        imggif_xelostgps[i].index =i;
                        imggif_xelostgps[i].frame = frames.length;
                        imggif_xelostgps[i].onload=function ( ){
                            // console.log(this.src);
                            adMap.registerImage('xelostgpstarget'+this.index,this);
                        };
                        imggif_xelostgps[i].src = frames[i].url;
                    }
                    // frames.forEach((f:any,i:number)=>{
                    //     var imggif_cartarget = new Image();
                    //     imggif_cartarget.src = f.url;
                    //     adMap.registerImage(`cartarget${i}`,imggif_cartarget);
                    // })
                },canvas_xelostgps);



                // lost gsm
                var xelostgsmgif = new Image();
                xelostgsmgif.src='../assets/images/carIcon/lostgsmtarget.png';
                var canvas_xelostgsm = document.createElement('canvas');
                var imggif_xelostgsm:any = [];
                splitgif(xelostgsmgif,(frames:any)=>{
                    for(var i =0; i< frames.length;i++){
                        imggif_xelostgsm[i] = new Image();
                        imggif_xelostgsm[i].width = 32;
                        imggif_xelostgsm[i].height =32;
                        imggif_xelostgsm[i].index =i;
                        imggif_xelostgsm[i].frame = frames.length;
                        imggif_xelostgsm[i].onload=function ( ){
                            // console.log(this.src);
                            adMap.registerImage('xelosttarget'+this.index,this);
                        };
                        imggif_xelostgsm[i].src = frames[i].url;
                    }
                    // frames.forEach((f:any,i:number)=>{
                    //     var imggif_cartarget = new Image();
                    //     imggif_cartarget.src = f.url;
                    //     adMap.registerImage(`cartarget${i}`,imggif_cartarget);
                    // })
                },canvas_xelostgsm);



                //trạng thái xe đỗ

                var xedogif = new Image();
                xedogif.src='../assets/images/carIcon/locktarget.png';
                var canvas_xedo = document.createElement('canvas');
                var imggif_xedo:any = [];
                splitgif(xedogif,(frames:any)=>{
                    for(var i =0; i< frames.length;i++){
                        imggif_xedo[i] = new Image();
                        imggif_xedo[i].width = 32;
                        imggif_xedo[i].height =32;
                        imggif_xedo[i].index =i;
                        imggif_xedo[i].frame = frames.length;
                        imggif_xedo[i].onload=function ( ){
                            // console.log(this.src);
                            adMap.registerImage('xedotarget'+this.index,this);
                        };
                        imggif_xedo[i].src = frames[i].url;
                    }
                    // frames.forEach((f:any,i:number)=>{
                    //     var imggif_cartarget = new Image();
                    //     imggif_cartarget.src = f.url;
                    //     adMap.registerImage(`cartarget${i}`,imggif_cartarget);
                    // })
                },canvas_xedo);


                // xe không khách
                var xekhongkhachgif = new Image();
                xekhongkhachgif.src='../assets/images/carIcon/Stop_cartarget.png';
                var canvas_xekhongkhach = document.createElement('canvas');
                var imggif_xekhongkhach:any = [];
                splitgif(xekhongkhachgif,(frames:any)=>{
                    for(var i =0; i< frames.length;i++){
                        imggif_xekhongkhach[i] = new Image();
                        imggif_xekhongkhach[i].width = 32;
                        imggif_xekhongkhach[i].height =32;
                        imggif_xekhongkhach[i].index =i;
                        imggif_xekhongkhach[i].frame = frames.length;
                        imggif_xekhongkhach[i].onload=function ( ){
                            // console.log(this.src);
                            adMap.registerImage('xekhongkhachtarget'+this.index,this);
                        };
                        imggif_xekhongkhach[i].src = frames[i].url;
                    }
                    // frames.forEach((f:any,i:number)=>{
                    //     var imggif_cartarget = new Image();
                    //     imggif_cartarget.src = f.url;
                    //     adMap.registerImage(`cartarget${i}`,imggif_cartarget);
                    // })
                },canvas_xekhongkhach);


                // xe mất đồng hồ
                var xematdonghogif = new Image();
                xematdonghogif.src='../assets/images/carIcon/matdonghotarget.png';
                var canvas_xematdongho = document.createElement('canvas');
                var imggif_xematdongho:any = [];
                splitgif(xematdonghogif,(frames:any)=>{
                    for(var i =0; i< frames.length;i++){
                        imggif_xematdongho[i] = new Image();
                        imggif_xematdongho[i].width = 32;
                        imggif_xematdongho[i].height =32;
                        imggif_xematdongho[i].index =i;
                        imggif_xematdongho[i].frame = frames.length;
                        imggif_xematdongho[i].onload=function ( ){
                            // console.log(this.src);
                            adMap.registerImage('xematdonghotarget'+this.index,this);
                        };
                        imggif_xematdongho[i].src = frames[i].url;
                    }
                    // frames.forEach((f:any,i:number)=>{
                    //     var imggif_cartarget = new Image();
                    //     imggif_cartarget.src = f.url;
                    //     adMap.registerImage(`cartarget${i}`,imggif_cartarget);
                    // })
                },canvas_xematdongho);



                // xe quá tốc độ
                var xequatocdogif = new Image();
                xequatocdogif.src='../assets/images/carIcon/quatocdo.png';
                var canvas_xequatocdo = document.createElement('canvas');
                var imggif_xequatocdo:any = [];
                splitgif(xequatocdogif,(frames:any)=>{
                    for(var i =0; i< frames.length;i++){
                        imggif_xequatocdo[i] = new Image();
                        imggif_xequatocdo[i].width = 32;
                        imggif_xequatocdo[i].height =32;
                        imggif_xequatocdo[i].index =i;
                        imggif_xequatocdo[i].frame = frames.length;
                        imggif_xequatocdo[i].onload=function ( ){
                            // console.log(this.src);
                            adMap.registerImage('xequatocdotarget'+this.index,this);
                        };
                        imggif_xequatocdo[i].src = frames[i].url;
                    }
                    // frames.forEach((f:any,i:number)=>{
                    //     var imggif_cartarget = new Image();
                    //     imggif_cartarget.src = f.url;
                    //     adMap.registerImage(`cartarget${i}`,imggif_cartarget);
                    // })
                },canvas_xequatocdo);

                //trạng thái xe sos

                var xesosgif = new Image();
                xesosgif.src='../assets/images/carIcon/carsostarget.png';
                var canvas_xesos = document.createElement('canvas');
                var imggif_xesos:any = [];
                splitgif(xesosgif,(frames:any)=>{
                    for(var i =0; i< frames.length;i++){
                        imggif_xesos[i] = new Image();
                        imggif_xesos[i].width = 32;
                        imggif_xesos[i].height =32;
                        imggif_xesos[i].index =i;
                        imggif_xesos[i].frame = frames.length;
                        imggif_xesos[i].onload=function ( ){
                            // console.log(this.src);
                            adMap.registerImage('xesostarget'+this.index,this);
                        };
                        imggif_xesos[i].src = frames[i].url;
                    }
                    // frames.forEach((f:any,i:number)=>{
                    //     var imggif_cartarget = new Image();
                    //     imggif_cartarget.src = f.url;
                    //     adMap.registerImage(`cartarget${i}`,imggif_cartarget);
                    // })
                },canvas_xesos);



                // đăng ký background
                function getBs(){
                    return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEkAAAAcCAIAAACiUgXPAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAA8SURBVFhH7c9BDQAgDAAxDGJ0AvdGR8klNdBzZ3/VzdTN1M3UzdTN1M3UzdTN1M3UzdTN1M3UzfTvbfYBPFBhLo+q6m0AAAAASUVORK5CYII=';
                    // return 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAUEBAQEAwUEBAQGBQUGCA0ICAcHCBALDAkNExAUExIQEhIUFx0ZFBYcFhISGiMaHB4fISEhFBkkJyQgJh0gISD/2wBDAQUGBggHCA8ICA8gFRIVICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICD/wAARCAAUAFADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDBooor9VPwIKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//Z';
                }
                var bsBr=new Image();
                bsBr.src=getBs();
                adMap.registerBackground(bsBr);

                return adMap;
            }

            public FilterMenuWithAccount(permision:Array<any>):Array<IState> {
                var account = this.Account.GetValue<IAccount>();
                var menus:any = this.GetTmp.GetValue();
                if (account.Level == 0) return menus;
                var result = Array<IState>();
                $.each(this.GetTmp.GetValue(), (indx:number, val:IState)=> {
                    if (permision != null &&
                        permision.filter((m:any)=> {
                            return m.Name == val.name;
                        }).length > 0)
                        result.push(val);
                });
                return result;
            }

            public BuilRootMenu(menus:any,parentmenu:any){
                var menu = Array<IMenu>();
                menus.forEach((m:any)=> {
                    if (m.name !== "") {
                        if (( m.parent == 'undefined' || m.parent == null )) {
                            var tmp:IMenu;
                            tmp = {
                                Text: m.tag.title,
                                Child: Array<IMenu>(),
                                Link: m.url.toString(),
                                Style: m.tag.icon,
                                Name: m.name
                                // content: $sce.trustAsHtml(m.settings.before + m.title),
                                // nav: m.settings.nav
                            };
                            menu.push(tmp);
                            parentmenu.push(tmp);
                        }
                    }
                });
                function MenuIndexOfName(name:any):number{
                    for (var i = 0; i < parentmenu.length; i++) {
                        if (parentmenu[i].Name === name)
                            return i;
                    }
                    return -1;
                }
                menus.forEach((m:any)=> {
                    if (m.name !== "") {
                        if (m.parent !== null || m.parent != 'undefined') {
                            var parentIndex = MenuIndexOfName(m.parent);

                            var tmp:IMenu = {
                                Text: m.tag.title,
                                Child: Array<IMenu>(),
                                Link: m.url.toString(),
                                Style: m.tag.icon,
                                Name: m.name,
                                Parent: parentmenu[parentIndex]
                                //content: $sce.trustAsHtml(m.settings.before+m.title+ m.settings.after ),
                                //nav: m.settings.nav
                            };
                            if (parentIndex > -1) {
                                parentmenu[parentIndex].Child.push(tmp)
                            }

                            parentmenu.push(tmp);
                        }
                    }
                });
                return menu;
            }

            public BuildMenuChild(menus:any, parentmenu:Array<IMenu>){
                function MenuIndexOfName(name:any):number{
                    for (var i = 0; i < parentmenu.length; i++) {
                        if (parentmenu[i].Name === name)
                            return i;
                    }
                    return -1;
                }
                menus.forEach((m:any)=> {
                    if (m.name !== "") {
                        if (m.parent !== null || m.parent != 'undefined') {
                            var parentIndex = MenuIndexOfName(m.parent);

                            var tmp:IMenu = {
                                Text: m.tag.title,
                                Child: Array<IMenu>(),
                                Link: m.url.toString(),
                                Style: m.tag.icon,
                                Name: m.name,
                                Parent: parentmenu[parentIndex]
                                //content: $sce.trustAsHtml(m.settings.before+m.title+ m.settings.after ),
                                //nav: m.settings.nav
                            };
                            if (parentIndex > -1) {
                                parentmenu[parentIndex].Child.push(tmp)
                            }

                            parentmenu.push(tmp);
                        }
                    }
                });
            }

            private RandomHost():string{
                var t = Math.floor(Math.random() *100);
                var index = t%5;
                var arrayhost = ['http://routetaxi1.adsun.vn/','http://routetaxi2.adsun.pro.vn/','http://routetaxi3.adsun.net.vn/','http://route1.adsun.vn/','http://route2.adsun.pro.vn/'];
                return arrayhost[index];
            }
            // public static $inject = ['Notification'];
            constructor(){

                this.PageTite=new VariableWatch();
                this.MenuSelect=new VariableWatch();
                this.Token=new VariableWatch();
                this.Account=new VariableWatch();
                this.GetMenu=new VariableWatch();
                this.GetTmp=new VariableWatch();
                this.Parent=new VariableWatch();
                this.IndexReadly=new VariableWatch();
                this.AuthHost= 'http://auth.adsun.vn/';
                // this.ReportHost = 'http://routedev.adsun.vn/'; // route1 có thể thay đổi khi trường họp server quá tải
                this.ReportHost = 'http://systemroute.adsun.vn/'; // route1 có thể thay đổi khi trường họp server quá tải
                // this.ReportHost = this.RandomHost(); // route1 có thể thay đổi khi trường họp server quá tải
                this.GridViewMode = 'custom'; // chế độ custom thì datatable có thể copy từng cột
                this.Version = '2.0.20';
                this.CustomDaterange = {
                    timePicker: true,
                    format: 'HH:mm DD/MM/YYYY',
                    timePicker12Hour: false,
                    timePickerIncrement:1,
                    opens:'left',
                    dateLimit: {
                        days: 4
                    },
                    locale: { cancelLabel: 'Hủy',
                        applyLabel: 'Đồng ý',
                        fromLabel:'Từ ngày',
                        toLabel:'Đến ngày',
                        daysOfWeek: ['<label class="text-danger">CN</label>', 'T2', 'T3', 'T4', 'T5', 'T6','T7'],
                        monthNames: ['Tháng 1 - ', 'Tháng 2 - ', 'Tháng 3 - ', 'Tháng 4 - ', 'Tháng 5 - ', 'Tháng 6 - ', 'Tháng 7 - ', 'Tháng 8 - ', 'Tháng 9 - ', 'Tháng 10 - ', 'Tháng 11 - ', 'Tháng 12 - '],
                        firstDay: 1
                    },
                    maxDate: '23:59' + new Date().getDate() + '/' + (new Date().getMonth()+1) + '/' + new Date().getFullYear()
                };
                this.SingleDate = {
                    singleDatePicker: true,
                    showDropdowns: true,
                    format: 'DD/MM/YYYY',
                    opens:'left',
                    drops: 'up',
                    locale: { cancelLabel: 'Hủy',
                        applyLabel: 'Đồng ý',
                        fromLabel:'Từ ngày',
                        toLabel:'Đến ngày',
                        daysOfWeek: ['<label class="text-danger">CN</label>', 'T2', 'T3', 'T4', 'T5', 'T6','T7'],
                        monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
                        firstDay: 1

                    },

                };
                this.SourceLoaiHinhKinhDoanh = [
                    {Text:"100:Xe khách tuyến cố định",Type:"Car"},
                    {Text:"200:Vận tải hành khách bằng xe bus",Type:"VanTaiBus"},
                    {Text:"300:Vận tải hành khách theo hợp đồng",Type:"VanTaiKhach"},
                    {Text:"400:Vận chuyển khách du lịch bằng ô tô",Type:"VanChuyenKhach"},
                    {Text:"500:Vận tải hàng hóa bằng container",Type:"VanChuyenHang"},
                    {Text:"600: Xe tải",Type:"XeTai"},
                    {Text:"700:Taxi",Type:"Taxi"},
                    {Text:"800:Taxi tải",Type:"TaxiTai"},
                    {Text:"900:Xe đầu kéo",Type:"XeDauKeo"}
                ];
                this.SourceSoGTVT = [
                    {Id:61, Name:'Hà Nội'},{Id:62, Name:'Hồ Chí Minh'},{Id:59, Name:'Đà Nẵng'},{Id:58, Name:'Cần Thơ'},
                    {Id:0, Name:'An Giang'},{Id:1, Name:'Bà Rịa - Vũng Tàu'},{Id:2, Name:'Bắc Giang'},{Id:3, Name:'Bắc Kạn'},{Id:4, Name:'Bạc Liêu'},{Id:5, Name:'Bắc Ninh'},
                    {Id:6, Name:'Bến Tre'},{Id:7, Name:'Bình Định'},{Id:8, Name:'Bình Dương'},{Id:9, Name:'Bình Phước'},{Id:10, Name:'Bình Thuận'},{Id:11, Name:'Cà Mau'},
                    {Id:12, Name:'Cao Bằng'},{Id:13, Name:'Đắk Lắk'},{Id:14, Name:'Đắk Nông'},{Id:15, Name:'Điện Biên'},{Id:16, Name:'Đồng Nai'},{Id:17, Name:'Đồng Tháp'},
                    {Id:18, Name:'Gia Lai'},{Id:19, Name:'Hà Giang'},{Id:20, Name:'Hà Nam'},{Id:60, Name:'Hải Phòng'},{Id:21, Name:'Hà Tĩnh'},{Id:22, Name:'Hải Dương'},{Id:23, Name:'Hậu Giang'},
                    {Id:24, Name:'Hòa Bình'},{Id:25, Name:'Hưng Yên'},{Id:26, Name:'Khánh Hòa'},{Id:27, Name:'Kiên Giang'},{Id:28, Name:'Kon Tum'},{Id:29, Name:'Lai Châu'},
                    {Id:30, Name:'Lâm Đồng'},{Id:31, Name:'Lạng Sơn'},{Id:32, Name:'Lào Cai'},{Id:33, Name:'Long An'},{Id:34, Name:'Nam Định'},{Id:35, Name:'Nghệ An'},
                    {Id:36, Name:'Ninh Bình'},{Id:37, Name:'Ninh Thuận'},{Id:38, Name:'Phú Thọ'},{Id:57, Name:'Phú Yên'},{Id:39, Name:'Quảng Bình'},{Id:40, Name:'Quảng Nam'},{Id:41, Name:'Quảng Ngãi'},
                    {Id:42, Name:'Quảng Ninh'},{Id:43, Name:'Quảng Trị'},{Id:44, Name:'Sóc Trăng'},{Id:45, Name:'Sơn La'},{Id:46, Name:'Tây Ninh'},{Id:47, Name:'Thái Bình'},
                    {Id:48, Name:'Thái Nguyên'},{Id:49, Name:'Thanh Hóa'},{Id:50, Name:'Thừa Thiên Huế'},{Id:51, Name:'Tiền Giang'},{Id:52, Name:'Trà Vinh'},{Id:53, Name:'Tuyên Quang'},
                    {Id:54, Name:'Vĩnh Long'},{Id:55, Name:'Vĩnh Phúc'},{Id:56, Name:'Yên Bái'},

                ];

            }
        }

        AdModule.service('global',GlobalService);
    }
}
