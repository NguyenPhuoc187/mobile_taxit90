/**
 * Created by phamn on 18/04/2016.
 */
/// <reference path="../../tstyping/bootstrap/bootstrap.d.ts" />
/// <reference path="../../tstyping/bootbox/bootbox.d.ts" />
/// <reference path="../../tstyping/angularjs/angular.d.ts" />
/// <reference path="../../tstyping/FileSaver/FileSaver.d.ts" />

/// <reference path="./library.ts" />


/**
 * Directive :
 * Khi đặt tên nếu trong tên có ký tư viết hoa thì ngoài element phải thêm ký tự - để phân cách
 * vd name : gridView => <grid-view/>
 */

///<reference path="./library/exportexcel.lib.ts"/>

module App{
    export module Shared{
        export module Directives {
            export interface IGridViewBehavior {
                isEdit: boolean;
                isNew: boolean;
                isDelete: boolean;
                isDetail: boolean;
                isRFID: boolean;
                isCheckSelect:boolean;
            }
            export interface IGridViewEvent {
                onNew?: (item?: any) => void;
                onDelete?: (item: any) => void;
                onEdit?: (old: any, ne: any) => void;
                onDetail?:(item: any) => void;
                onCheck?:(item:any)=>void;
                onBindingComplete?:()=>void;
                onDoubleClick?:(item: any) => void;
                onClick?:(item: any) => void;
                onRFID?:(item:any) => void;

            }
            export interface IGridViewControl {
                id: string;
                behavior: IGridViewBehavior;
                columns: Array<any>;
                columnGroups?:Array<any>;
                data: Array<any>;
                height?:any;


                render():void;
                refresh():void;
                refreshupdate():void;
                clear():void;
                clearcompile():void;
                showcolumn?(column:string):void;
                clearSelection():void;
                event: IGridViewEvent;
                datasource: string;
                enablebinding: (val: boolean) => void;
                dataEdit:any;
                //isCreateHtml:boolean;
                filename_excel: string;
                info_excel: any;
                isExport: boolean;
                isFilter:boolean;
                iscolumresize:boolean;
                iseditable:boolean;
                isAltRows:boolean;
                //ví dụ: filterMode: 'simple'
                isFilterMode: string;
                isshowAggregates: boolean;
                selectionModeinfo: string;
                columnsHeight: number;
                selectRowInfo(): any;

            }
            export interface IDataTableSetting {
                width?: any;
                scrollBarSize?:number;
                pageable?: boolean;
                filterable?:boolean;
                filterMode?: string;
                height?:string;
                pageSizeOptions?:Array<any>;
                pageSize?:number;
                altRows?:boolean;
                pagerButtonsCount?: number;
                source?: any;
                columnsResize?: boolean;
                autoRowHeight?:boolean
                pagerMode?:string;
                columnsHeight?:number;
                columns?: Array<any>;
                columnGroups?:Array<any>;
                editable?: boolean;
                enableBrowserSelection?:boolean;
                apply?:(val?:any, val_2?:any)=>void;
                sortable?: boolean;
                created?:(args?:any) => void;
                rendered?:() =>any;
                // cellsRenderer

                bindingComplete?:() => void;
                rowDoubleClick?:(event:any) =>any;
                rowClick?:(event:any) =>any;
                showAggregates?: boolean;
                localization?: any;
                selectionMode?: any;
                // selectionmode:any;
                enableHover?:boolean;
                filter?:(event:any)=>any;

            }
            export interface IGridViewScope extends ng.IScope {
                newClick: () => void;
                editClick: (item: any) => void;
                delClick: (item: any) => void;
                id: IGridViewControl;
                control: IGridViewControl;
                getDialogId:()=>string;
                getDialogIdFull: () => string;
                dataTableSettings: IDataTableSetting;
                createWidget:boolean;
                gridInstance:any;
                excelExportClick?:() => void;
                print?:() =>void;
                bing:() =>void;
                datafilterview:any;
                compiletemplate:() => any;
            }

            export class GridviewControl implements IGridViewControl {
                isExport: boolean;
                filename_excel: string;
                info_excel: any;
                refresh:()=>void;
                refreshupdate:()=>void;
                clear:()=>void;
                clearcompile:()=>void;
                showcolumn:(column:string)=>void;
                clearSelection:()=>void;
                render:()=>void;
                id: string;
                isFilter:boolean;
                iscolumresize:boolean;
                iseditable:boolean;
                isFilterMode: string;
                selectionModeinfo:string;
                columnsHeight:number;
                isAltRows:boolean;
                isshowAggregates:boolean;
                //isCreateHtml:boolean;
                constructor(name: string) {
                    this.id = name;
                    this.behavior= {
                        isEdit: false,
                        isDelete: false,
                        isNew:false,
                        isDetail:false,
                        isRFID: false,
                        isCheckSelect:false,
                    };
                    this.event = {};
                    this.columns = [];
                    this.columnGroups = [];
                    this.data = [];

                    //this.isCreateHtml=true;
                }

                behavior: IGridViewBehavior;
                columns: any[];
                columnGroups: any[];
                height:any;
                data: any[];
                event: IGridViewEvent;
                datasource: string;
                enablebinding: (val: boolean) => void;
                dataEdit: any;
                selectRowInfo:any;
            }
            export interface IGridAttribute extends ng.IAttributes {
            }
            export class GridViewController {
                public static $name: string = "gridView";
                constructor($scope: IGridViewScope, $http: ng.IHttpService, $window:ng.IWindowService, $compile:ng.ICompileService) {
                    this.Init($scope,$http,$window,$compile);
                }


                public Init($scope: IGridViewScope,$http: ng.IHttpService, $window:ng.IWindowService, $compile:ng.ICompileService):void{

                    //$scope.dataTableSettings = {};
                    $scope.gridInstance = {};
                    //console.log($scope.id);

                    $scope.control = $scope.id;



                    //console.log($scope.control);
                    //var grid,deleteButtons, editButtons;

                    //$scope.createWidget=$scope.control.isCreateHtml;

                    // $('#toggle').click(function (event) {
                    //
                    //    $scope.id.refresh();
                    //     event.stopPropagation();
                    // });

                    // chuyển ngôn ngữ các từ mặc định của grid-view
                    var getLocalization:any = ()=>{
                        var localizationobj:any = {
                            pagergotopagestring: "Trang:",
                            decimalseparator: ',',
                            thousandsseparator: '.',
                            pagershowrowsstring: " Hiển thị:",
                            pagerrangestring: " / ",
                            pagerpreviousbuttonstring: "Quay lại",
                            pagernextbuttonstring: "Tiếp theo",
                            pagerfirstbuttonstring: "Đầu tiên",
                            pagerlastbuttonstring: "Cuối cùng",
                            filterapplystring: "Đồng ý",
                            filtercancelstring: "Hủy",
                            filterclearstring: "Xóa tìm kiếm",
                            filterstring: "<p class='text-success' style='margin-left: 19px'>Tìm kiếm nâng cao</p>",
                            filtersearchstring: "Tìm kiếm:",
                            filterstringcomparisonoperators: ['Không lọc', 'Tất cả', 'Dữ liệu', 'Dữ liệu(theo ký tự)',
                                'Không trùng với dữ liệu', 'Không trùng với dữ liệu(theo ký tự)', 'Ký tự bắt đầu',
                                'Ký tự cuối', 'Bằng giá trị'],
                            filternumericcomparisonoperators: ['Bằng', 'Không Bằng', 'Nhỏ hơn', 'Nhỏ hơn hoặc bằng', 'Lớn hơn', 'Lớn hơn hoặc bằng', 'null', 'not null'],
                            filterdatecomparisonoperators: ['Bằng', 'Không Bằng', 'Nhỏ hơn', 'Nhỏ hơn hoặc bằng', 'Lớn hơn', 'Lớn hơn hoặc bằng', 'null', 'not null'],
                            filterbooleancomparisonoperators: ['equal', 'not equal'],
                            validationstring: "Entered value is not valid",
                            emptydatastring: "Không có dữ liệu để hiển thị",
                            filterselectstring: "Select Filter",
                            loadtext: "Đợi trong giây lát....",
                            clearstring: "Xóa",
                            todaystring: "Hôm nay",
                        };
                        return localizationobj;

                    };
                    var grid:any;

                    $scope.dataTableSettings =
                        {
                            created: function(args)
                            {
                                grid = args.instance;
                                // console.log(grid);
                                // grid.columnsHeight
                            },
                            width: "100%",
                            height:$scope.control.height,
                            pageable: true,
                            pagerMode: 'default',
                            showAggregates: $scope.control.isshowAggregates,
                            altRows : $scope.control.isAltRows,
                            filterable:$scope.control.isFilter,
                            filterMode: $scope.control.isFilterMode,
                            enableHover: true,
                            pagerButtonsCount: 10,
                            scrollBarSize:10,
                            pageSizeOptions: ['20', '50','100', '200','500','1000'],
                            pageSize: 20,
                            editable: $scope.control.iseditable,
                            enableBrowserSelection: true, // bặt chế độ copy cho grid-view
                            columnsResize: ($scope.control.iscolumresize == undefined)?false:false, // bật chế độ thay đổi kích thước cho cột
                            columns: [],
                            columnGroups: [],
                            columnsHeight:$scope.control.columnsHeight,
                            sortable: true,
                            localization: getLocalization(),
                            selectionMode: $scope.control.selectionModeinfo,
                            autoRowHeight:true,
                            // selectionmode: 'checkbox',
                            //selectionMode: 'singleRow',
                            rendered:()=>{
                                //return;
                                try {
                                    $("." + "addIcon_" + $scope.id.id).bind('click', function(e:any){
                                        e.preventDefault();
                                        //$('#myModal').modal('show');
                                    });
                                    $("." + "editIcon_" + $scope.id.id).bind('click', function(e:any){
                                        e.preventDefault();

                                    });
                                    $("." + "checkIcon_" + $scope.id.id).bind('click', function(e:any){
                                        e.preventDefault();

                                    });
                                    $("." + "deleteIcon_" + $scope.id.id).bind('click', function(e:any){
                                        e.preventDefault();
                                        //alert('abv');
                                        //$('#myModal').modal('show');
                                    });
                                    $("." + "detailIcon_" + $scope.id.id).bind('click', function(e:any){
                                        e.preventDefault();
                                        //alert('abv');
                                        //$('#myModal').modal('show');
                                    });
                                    $("." + "RFIDIcon_" + $scope.id.id).bind('click', function(e:any){
                                        e.preventDefault();
                                    });
                                    $("." + "addIcon_" + $scope.id.id).on('click', function(event:any){
                                        //var rowIndex = parseInt(event.currentTarget.getAttribute('data-row'));
                                        //if (isNaN(rowIndex)) {
                                        //    return;
                                        //}
                                        //var old = dataCfg.localData[rowIndex];
                                        //console.log(old);
                                        if($scope.control.event != null)
                                            $scope.control.event.onNew();
                                    });
                                    $("." + "editIcon_" + $scope.id.id).on('click', function (event:any) {
                                        var rowIndex = parseInt(event.currentTarget.getAttribute('data-row'));
                                        if (isNaN(rowIndex)) {
                                            return;
                                        }
                                        var old=dataCfg.localData[rowIndex];
                                        //console.log(old);
                                        if( $scope.control.event!=null)
                                            $scope.control.event.onEdit(old,old);
                                        //dataCfg.localData.splice(rowIndex,1);

                                        // $scope.dataTableSettings.apply('updateBoundData'); // update maunual du lieu moi
                                        //grid.endRowEdit(rowIndex, true);
                                    });

                                    $("." + "deleteIcon_" + $scope.id.id).click(function (event:any) {

                                        var rowIndex = parseInt(event.currentTarget.getAttribute('data-row'));
                                        //console.log(rowIndex);
                                        if (isNaN(rowIndex)) {
                                            return;
                                        }
                                        var old=dataCfg.localData[rowIndex];
                                        if( $scope.control.event!=null){
                                            $scope.control.event.onDelete(old);
                                        }

                                    });
                                    $("." + "detailIcon_" + $scope.id.id).on('click', function(event:any){
                                        var rowIndex = parseInt(event.currentTarget.getAttribute('data-row'));
                                        if (isNaN(rowIndex)) {
                                            return;
                                        }
                                        var old=dataCfg.localData[rowIndex];
                                        if($scope.control.event != null){
                                            $scope.control.event.onDetail(old);
                                        }

                                    });
                                    $("." + "checkIcon_" + $scope.id.id).click(function(event:any){
                                        console.log(event.currentTarget);
                                        var rowIndex = parseInt(event.currentTarget.getAttribute('data-row'));
                                        // if($(this).checked == true){
                                        //     grid.selectRow(rowIndex);
                                        // }else{
                                        //     grid.clearSelection();
                                        // }
                                        if(this.checked){
                                            grid.selectRow(rowIndex);
                                        }else{
                                            grid.unselectRow(rowIndex);
                                        }
                                        // if( $(this).is(':checked') ){
                                        //     // $(this).attr('checked', '');
                                        //     grid.selectRow(rowIndex);
                                        //     // $(this).attr('checked','');
                                        //
                                        // }else{
                                        //     grid.clearSelection();
                                        // }

                                        // var rowIndex = parseInt(event.currentTarget.getAttribute('data-row'));
                                        // if (isNaN(rowIndex)) {
                                        //     return;
                                        // }
                                        // console.log(rowIndex);
                                        //
                                        // var old=dataCfg.localData[rowIndex];
                                        // grid.selectRow(parseInt(rowIndex));
                                        // if($scope.control.event != null){
                                        //     $scope.control.event.onRFID(old);
                                        // }

                                    });
                                    $("." + "RFIDIcon_" + $scope.id.id).on('click', function(event:any){
                                        var rowIndex = parseInt(event.currentTarget.getAttribute('data-row'));
                                        if (isNaN(rowIndex)) {
                                            return;
                                        }
                                        var old=dataCfg.localData[rowIndex];

                                        if($scope.control.event != null){
                                            $scope.control.event.onRFID(old);
                                        }

                                    });
                                }
                                catch(e){

                                }
                            },
                            bindingComplete: () => {
                                if( $scope.control.event.onBindingComplete != null){
                                    $scope.control.event.onBindingComplete();
                                }
                            },
                            rowDoubleClick: (event) => {
                                try {
                                    var args = event.args;
                                    // row data.
                                    var row = args.row;

                                    if($scope.control.event != null){
                                        $scope.control.event.onDoubleClick(row);
                                    }
                                } catch(e) {

                                }
                            },
                            rowClick: (event) => {
                                try {
                                    var args = event.args;
                                    // row data.
                                    var row = args.row;

                                    if($scope.control.event != null){
                                        $scope.control.event.onClick(row);
                                    }
                                } catch(e) {

                                }
                            },
                            filter:(event)=>{
                                $scope.datafilterview = event.args.owner.dataViewRecords;

                            }
                        };
                    //xử lý select row
                    $scope.control.selectRowInfo = () =>{
                        // gets selected row indexes. The method returns an Array of indexes.
                        var selection = grid.getSelection();
                        return selection;
                    };
                    $('#toggle').click(function () {
                        $scope.control.refresh();
                    });
                    // this.$scope.click_open_menu = () => {
                    //
                    // };

                    // Xử lý khi nhấn nút export sang excel

                    // $scope.excelExportClick = () => {
                    //
                    //     var today = new Date(),
                    //         time = today.toTimeString().split(':').join('').substr(0, 4),
                    //         timestamp = getDate('dd-mm-yyyy', today);
                    //     var title= $scope.control.filename_excel;
                    //     // console.log($scope.dataTableSettings);
                    //     // console.log($scope.control);
                    //     var data:any=[];
                    //     if($scope.datafilterview !== null || $scope.datafilterview !== undefined){
                    //         data = $scope.datafilterview;
                    //         // for(var i=0;i<$scope.dataTableSettings.columns.length;i++){
                    //         //     if($scope.dataTableSettings.columns[i].hidden == true){
                    //         //         for(var j =0; j< data.length;j++){
                    //         //
                    //         //         }
                    //         //     }
                    //         // }
                    //
                    //     }else{
                    //
                    //         data = $scope.control.data;
                    //         console.log(data);
                    //     }
                    //     var nameheader:Array<any> = [];//$scope.columns;
                    //     // if(data.length===0) {
                    //     //     ul.message.warning("Chưa có dữ liệu để xuất excel");
                    //     //     return;
                    //     // }
                    //
                    //     angular.forEach($scope.dataTableSettings.columns,function(c){
                    //         // xét cột nào có thuộc tính ẩn
                    //         if(c.hidden !== true){
                    //             nameheader.push(c);//headerlenght++;
                    //         }
                    //     });
                    //
                    //     /*
                    //      function xuất excel mới
                    //      * */
                    //     var hex = new Array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
                    //
                    //
                    //     function escapeHTML(val:any) {
                    //         var x = document.getElementById("from_text");
                    //
                    //
                    //         var preescape = "" + val;
                    //         var escaped = "";
                    //
                    //         var i = 0;
                    //         var brEscape = false;//document.getElementById('br').checked;
                    //         var tagEscape = false;//document.getElementById('tag').checked;
                    //         var others = true;//document.getElementById('other').checked;
                    //         var nbsp = false;//document.getElementById('nbsp').checked;
                    //         for (i = 0; i < preescape.length; i++) {
                    //             var p = preescape.charAt(i);
                    //
                    //             if (others) p = "" + escapeCharx(p);
                    //             if (tagEscape) p = "" + escapeTags(p);
                    //             if (brEscape) p = "" + escapeBR(p);
                    //             if (nbsp) p = "" + escapeNBSP(p);
                    //
                    //             escaped = escaped + p;
                    //         }
                    //         return escaped;
                    //         //x = document.getElementById("to_text");
                    //         //x.value = escaped;
                    //     }
                    //
                    //
                    //     function escapeBR(original:any) {
                    //         var thechar = original.charCodeAt(0);
                    //
                    //         switch (thechar) {
                    //             case 10: return "<br/>";  //newline
                    //             case '\r': break;
                    //         }
                    //         return original;
                    //     }
                    //
                    //     function escapeNBSP(original:any) {
                    //         var thechar = original.charCodeAt(0);
                    //         switch (thechar) {
                    //             case 32: return "&nbsp;";  //space
                    //         }
                    //         return original;
                    //     }
                    //
                    //
                    //     function escapeTags(original:any) {
                    //         var thechar = original.charCodeAt(0);
                    //         switch (thechar) {
                    //             case 60: return "&lt;";  //<
                    //             case 62: return "&gt;";  //>
                    //             case 34: return "&quot;";  //"
                    //         }
                    //         return original;
                    //
                    //     }
                    //
                    //     function escapeCharx(original:any) {
                    //         var found = true;
                    //         var thechar = original.charCodeAt(0);
                    //         switch (thechar) {
                    //             case 38: return "&amp;";
                    //             case 198: return "&AElig;";
                    //             case 193: return "&Aacute;";
                    //             case 194: return "&Acirc;";
                    //             case 192: return "&Agrave;";
                    //             case 197: return "&Aring;";
                    //             case 195: return "&Atilde;";
                    //             case 196: return "&Auml;";
                    //             case 199: return "&Ccedil;";
                    //             case 208: return "&ETH;";
                    //             case 201: return "&Eacute;";
                    //             case 202: return "&Ecirc;";
                    //             case 200: return "&Egrave;";
                    //             case 203: return "&Euml;";
                    //             case 205: return "&Iacute;";
                    //             case 206: return "&Icirc;";
                    //             case 204: return "&Igrave;";
                    //             case 207: return "&Iuml;";
                    //             case 209: return "&Ntilde;";
                    //             case 211: return "&Oacute;";
                    //             case 212: return "&Ocirc;";
                    //             case 210: return "&Ograve;";
                    //             case 216: return "&Oslash;";
                    //             case 213: return "&Otilde;";
                    //             case 214: return "&Ouml;";
                    //             case 222: return "&THORN;";
                    //             case 218: return "&Uacute;";
                    //             case 219: return "&Ucirc;";
                    //             case 217: return "&Ugrave;";
                    //             case 220: return "&Uuml;";
                    //             case 221: return "&Yacute;";
                    //             case 225: return "&aacute;";
                    //             case 226: return "&acirc;";
                    //             case 230: return "&aelig;";
                    //             case 224: return "&agrave;";
                    //             case 229: return "&aring;";
                    //             case 227: return "&atilde;";
                    //             case 228: return "&auml;";
                    //             case 231: return "&ccedil;";
                    //             case 233: return "&eacute;";
                    //             case 234: return "&ecirc;";
                    //             case 232: return "&egrave;";
                    //             case 240: return "&eth;";
                    //             case 235: return "&euml;";
                    //             case 237: return "&iacute;";
                    //             case 238: return "&icirc;";
                    //             case 236: return "&igrave;";
                    //             case 239: return "&iuml;";
                    //             case 241: return "&ntilde;";
                    //             case 243: return "&oacute;";
                    //             case 244: return "&ocirc;";
                    //             case 242: return "&ograve;";
                    //             case 248: return "&oslash;";
                    //             case 245: return "&otilde;";
                    //             case 246: return "&ouml;";
                    //             case 223: return "&szlig;";
                    //             case 254: return "&thorn;";
                    //             case 250: return "&uacute;";
                    //             case 251: return "&ucirc;";
                    //             case 249: return "&ugrave;";
                    //             case 252: return "&uuml;";
                    //             case 253: return "&yacute;";
                    //             case 255: return "&yuml;";
                    //             case 162: return "&cent;";
                    //             default:
                    //                 found = false;
                    //                 break;
                    //         }
                    //         if (!found) {
                    //             if (thechar > 127) {
                    //                 var c = thechar;
                    //                 var a4 = c % 16;
                    //                 c = Math.floor(c / 16);
                    //                 var a3 = c % 16;
                    //                 c = Math.floor(c / 16);
                    //                 var a2 = c % 16;
                    //                 c = Math.floor(c / 16);
                    //                 var a1 = c % 16;
                    //                 //	alert(a1);
                    //                 return "&#x" + hex[a1] + hex[a2] + hex[a3] + hex[a4] + ";";
                    //             }
                    //             else {
                    //                 return original;
                    //             }
                    //         }
                    //     }
                    //
                    //
                    //     /**
                    //      * kết thúc
                    //      * */
                    //
                    //     var exportExcel = new App.Shared.Library.ExportExcell();
                    //     exportExcel.AddInfoHeader(title,nameheader.length,null);
                    //     exportExcel.AddInfoTopRight("ANH DUONG COMPANY",nameheader.length,null);
                    //     for(var i=0;i<nameheader.length;i++){
                    //         //if(nameheader[i].visible)
                    //         exportExcel.AddInfoColumn(nameheader[i].text,null);
                    //     }
                    //
                    //     for(var i=0;i<data.length;i++) {
                    //         exportExcel.AddInfoRow(mergeExcelData(data[i]),null);
                    //     }
                    //     exportExcel.buildTable('exportable');
                    //     // var blob = new Blob([document.getElementById('exportable').innerHTML], {
                    //     //     type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8"
                    //     // });
                    //
                    //     // saveAs(blob, title+"_"+timestamp+".xls");
                    //
                    //
                    //     var tableToExcel = (function() {
                    //
                    //         var a:any = document.createElement("a");
                    //         document.body.appendChild(a);
                    //         a.style = "display: none";
                    //
                    //         var uri = 'data:application/vnd.ms-excel;base64,',
                    //             template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head>' +
                    //                 `<meta http-equiv=Content-Type content="text/html; charset=windows-1252 ;filename=${title}_${timestamp}.xls">` +
                    //                 '<!--[if gte mso 9]><?xml version="1.0" encoding="UTF-8" standalone="yes"?><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
                    //             base64 = function(s:any) { return window.btoa(unescape(encodeURIComponent(s))) },
                    //             format = function(s:any, c:any) { return s.replace(/{(\w+)}/g, function(m:any, p:any) { return c[p]; }) };
                    //         return function(table:any, name:any, fileName:string) {
                    //             if (!table.nodeType) table = document.getElementById(table);
                    //             // var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML };
                    //             var ctx = { worksheet: name || 'Worksheet', table: escapeHTML(table.innerHTML) };
                    //             //var tmp = format(template, ctx);
                    //             // window.location.href = uri + base64(format(template, ctx));
                    //             var url = uri + base64(format(template, ctx));
                    //
                    //             a.href = url;
                    //             a.download = fileName;
                    //             a.click();
                    //
                    //             window.URL.revokeObjectURL(url);
                    //             // document.body.remove(a);
                    //
                    //
                    //
                    //
                    //             // window.open(url, '_blank');
                    //
                    //
                    //             // $window.location.href = url;
                    //             // location.href = url;
                    //             // var blob = new Blob([base64(format(template, ctx))],{
                    //             //     type: "data:application/vnd.ms-excel"
                    //             // });
                    //             // saveAs(blob, title+"_"+timestamp+".xls");
                    //             // var blobURL = window.URL.createObjectURL(blob);
                    //             // return blobURL;
                    //
                    //             //saveContent(url,"abcas.xls");
                    //
                    //             // var blob = new Blob(['base64,'+base64(format(template, ctx))], {
                    //             //     type: "data:application/vnd.ms-excel"
                    //             // });
                    //             //
                    //             // saveAs(blob, title+"_"+timestamp+".xls");
                    //             // var fileName = `${title}_${timestamp}.xls`;
                    //             // var _window = window.open(url,'_blank','donwload');
                    //             // _window.document.execCommand('SaveAs', true, fileName);
                    //             // _window.close();
                    //
                    //
                    //         }
                    //     })();
                    //     tableToExcel('t_exportable',"",`${title}_${timestamp}`);
                    //
                    //     // var blob = new Blob([document.getElementById('exportable').innerHTML], {
                    //     //     type: "application/ms-excel;charset=utf-8"
                    //     // });
                    //
                    //     // saveAs(blob, title+"_"+timestamp+".xls");
                    //
                    //     /*Hàm mergeexcell*/
                    //     function mergeExcelData(data:any){
                    //         var result:any={};
                    //         angular.forEach($scope.dataTableSettings.columns,function(c){
                    //             if(c.hidden!==true){
                    //                 if(data[c.dataField]==null) data[c.dataField]="";
                    //                 if (result[c.dataField] == undefined && data != null && data != undefined) {
                    //                     result[c.dataField] = data[c.dataField];
                    //                 }
                    //             }
                    //         });
                    //         // console.debug(result);
                    //         return result;
                    //     }
                    //     function getDate (mode:any, userdate:any) {
                    //         var dte = userdate || new Date(),
                    //             d = dte.getDate().toString(),
                    //             m = (dte.getMonth() + 1).toString(),
                    //             yyyy = dte.getFullYear().toString(),
                    //             dd = (d.length < 2) ? '0' + d : d,
                    //             mm = (m.length < 2) ? '0' + m : m,
                    //             yy = yyyy.substring(2, 4);
                    //         switch (mode) {
                    //             case 'dd-mm-yyyy': return dd + '-' + mm + '-' + yyyy;
                    //             case 'yyyymmdd': return yyyy + mm + dd;
                    //             default: return dte;
                    //         }
                    //     }
                    // };

                    /*Xử lý nút in*/
                    $scope.excelExportClick = () => {
                        var today = new Date(),
                            time = today.toTimeString().split(':').join('').substr(0, 4),
                            timestamp = getDate('dd-mm-yyyy', today);
                        var title= $scope.control.filename_excel;
                        var info:any= $scope.control.info_excel;
                        // console.log($scope.dataTableSettings);
                        // console.log($scope.control);
                        var data:any=[];
                        if($scope.datafilterview !== null || $scope.datafilterview !== undefined){
                            data = $scope.datafilterview;
                            // for(var i=0;i<$scope.dataTableSettings.columns.length;i++){
                            //     if($scope.dataTableSettings.columns[i].hidden == true){
                            //         for(var j =0; j< data.length;j++){
                            //
                            //         }
                            //     }
                            // }

                        }else{

                            data = $scope.control.data;

                        }
                        var nameheader:Array<any> = [];//$scope.columns;
                        // if(data.length===0) {
                        //     ul.message.warning("Chưa có dữ liệu để xuất excel");
                        //     return;
                        // }

                        angular.forEach($scope.dataTableSettings.columns,function(c){
                            // xét cột nào có thuộc tính ẩn
                            if(c.hidden !== true && c.text !=="Ghi thẻ/Thiết lập tx"){
                                nameheader.push(c);//headerlenght++;
                            }

                        });

                        /*
                         function xuất excel mới
                         * */
                        var hex = new Array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');


                        function escapeHTML(val:any) {
                            var x = document.getElementById("from_text");


                            var preescape = "" + val;
                            var escaped = "";

                            var i = 0;
                            var brEscape = false;//document.getElementById('br').checked;
                            var tagEscape = false;//document.getElementById('tag').checked;
                            var others = true;//document.getElementById('other').checked;
                            var nbsp = false;//document.getElementById('nbsp').checked;
                            for (i = 0; i < preescape.length; i++) {
                                var p = preescape.charAt(i);

                                if (others) p = "" + escapeCharx(p);
                                if (tagEscape) p = "" + escapeTags(p);
                                if (brEscape) p = "" + escapeBR(p);
                                if (nbsp) p = "" + escapeNBSP(p);

                                escaped = escaped + p;
                            }
                            return escaped;
                            //x = document.getElementById("to_text");
                            //x.value = escaped;
                        }


                        function escapeBR(original:any) {
                            var thechar = original.charCodeAt(0);

                            switch (thechar) {
                                case 10: return "<br/>";  //newline
                                case '\r': break;
                            }
                            return original;
                        }

                        function escapeNBSP(original:any) {
                            var thechar = original.charCodeAt(0);
                            switch (thechar) {
                                case 32: return "&nbsp;";  //space
                            }
                            return original;
                        }


                        function escapeTags(original:any) {
                            var thechar = original.charCodeAt(0);
                            switch (thechar) {
                                case 60: return "&lt;";  //<
                                case 62: return "&gt;";  //>
                                case 34: return "&quot;";  //"
                            }
                            return original;

                        }

                        function escapeCharx(original:any) {
                            var found = true;
                            var thechar = original.charCodeAt(0);
                            switch (thechar) {
                                case 38: return "&amp;";
                                case 198: return "&AElig;";
                                case 193: return "&Aacute;";
                                case 194: return "&Acirc;";
                                case 192: return "&Agrave;";
                                case 197: return "&Aring;";
                                case 195: return "&Atilde;";
                                case 196: return "&Auml;";
                                case 199: return "&Ccedil;";
                                case 208: return "&ETH;";
                                case 201: return "&Eacute;";
                                case 202: return "&Ecirc;";
                                case 200: return "&Egrave;";
                                case 203: return "&Euml;";
                                case 205: return "&Iacute;";
                                case 206: return "&Icirc;";
                                case 204: return "&Igrave;";
                                case 207: return "&Iuml;";
                                case 209: return "&Ntilde;";
                                case 211: return "&Oacute;";
                                case 212: return "&Ocirc;";
                                case 210: return "&Ograve;";
                                case 216: return "&Oslash;";
                                case 213: return "&Otilde;";
                                case 214: return "&Ouml;";
                                case 222: return "&THORN;";
                                case 218: return "&Uacute;";
                                case 219: return "&Ucirc;";
                                case 217: return "&Ugrave;";
                                case 220: return "&Uuml;";
                                case 221: return "&Yacute;";
                                case 225: return "&aacute;";
                                case 226: return "&acirc;";
                                case 230: return "&aelig;";
                                case 224: return "&agrave;";
                                case 229: return "&aring;";
                                case 227: return "&atilde;";
                                case 228: return "&auml;";
                                case 231: return "&ccedil;";
                                case 233: return "&eacute;";
                                case 234: return "&ecirc;";
                                case 232: return "&egrave;";
                                case 240: return "&eth;";
                                case 235: return "&euml;";
                                case 237: return "&iacute;";
                                case 238: return "&icirc;";
                                case 236: return "&igrave;";
                                case 239: return "&iuml;";
                                case 241: return "&ntilde;";
                                case 243: return "&oacute;";
                                case 244: return "&ocirc;";
                                case 242: return "&ograve;";
                                case 248: return "&oslash;";
                                case 245: return "&otilde;";
                                case 246: return "&ouml;";
                                case 223: return "&szlig;";
                                case 254: return "&thorn;";
                                case 250: return "&uacute;";
                                case 251: return "&ucirc;";
                                case 249: return "&ugrave;";
                                case 252: return "&uuml;";
                                case 253: return "&yacute;";
                                case 255: return "&yuml;";
                                case 162: return "&cent;";
                                default:
                                    found = false;
                                    break;
                            }
                            if (!found) {
                                if (thechar > 127) {
                                    var c = thechar;
                                    var a4 = c % 16;
                                    c = Math.floor(c / 16);
                                    var a3 = c % 16;
                                    c = Math.floor(c / 16);
                                    var a2 = c % 16;
                                    c = Math.floor(c / 16);
                                    var a1 = c % 16;
                                    //	alert(a1);
                                    return "&#x" + hex[a1] + hex[a2] + hex[a3] + hex[a4] + ";";
                                }
                                else {
                                    return original;
                                }
                            }
                        }


                        var exportExcel = new App.Shared.Library.ExportExcell();
                        exportExcel.AddInfoHeader(`${title}`,nameheader.length,{
                            color:'black',
                            text_align: 'center',
                            width:'auto',
                            font_size:25,

                        });
                        if(info !== undefined && info !== null){
                            exportExcel.AddInfoHeaderChildren(`Từ ${info.Starttime} đến ${info.Endtime} <br/> Đơn vị kinh doanh vận tải: ${localStorage.getItem('accinfo')}<br/> Biển số: ${info.Bs}`,nameheader.length,null);
                        }

                        // exportExcel.AddInfoTopRight("CÔNG TY CỔ PHẦN ĐIỆN TỬ VIỄN THÔNG ÁNH DƯƠNG",nameheader.length,null);
                        for(var i=0;i<nameheader.length;i++){
                            //if(nameheader[i].visible)
                            exportExcel.AddInfoColumn(nameheader[i].text,null);
                        }

                        for(var i=0;i<data.length;i++) {
                            exportExcel.AddInfoRow(mergeExcelData(data[i]),{
                                color:'black',
                                border: 'thin solid black',
                                text_align: 'left',
                                width:'auto',
                                font_size:'11px',

                            });
                        }
                        exportExcel.buildTable('exportable');
                        function mergeExcelData(data:any){
                            var result:any={};
                            angular.forEach($scope.dataTableSettings.columns,function(c){
                                if(c.hidden!==true){
                                    if(data[c.dataField]==null) data[c.dataField]="";
                                    if (result[c.dataField] == undefined && data != null && data != undefined) {
                                        console.log('chay excel');

                                        if(c.cellsFormat !==undefined && c.cellsFormat === 'dd/MM/yyyy'){
                                            //todo: chuyển cột nào được định dạng ngày tháng thì chuyển sang dạng string hết để hiện thị ra excel cho đúng
                                            result[c.dataField] = AdapterDate(data[c.dataField]);
                                            console.log(data[c.dataField]);
                                        }else if(c.cellsFormat !==undefined && c.cellsFormat === 'HH:mm:ss dd/MM/yyyy'){
                                            result[c.dataField] = AdapterDateTime(data[c.dataField]);
                                        }else if(c.cellsFormat !==undefined && c.cellsFormat === 'HH:mm dd/MM/yyyy'){
                                            result[c.dataField] = AdapterDateTime(data[c.dataField]);
                                        }
                                        else{
                                            if(data[c.dataField] === '<span class="icon-checkbox-checked"></span>'){
                                                result[c.dataField] = 'Có';
                                            }else if(data[c.dataField] === '<span class="icon-checkbox-unchecked"></span>'){
                                                result[c.dataField] = 'Không';
                                            }else{
                                                result[c.dataField] = data[c.dataField];
                                            }
                                        }

                                    }
                                }
                            });
                            // console.debug(result);
                            return result;
                        }
                        function AdapterDateTime(time:any){
                            var year:string = time.getFullYear();
                            var month:string = (time.getMonth()+1 >= 10)?time.getMonth() + 1: `0${time.getMonth() + 1}`;
                            var date:string = (time.getDate() >= 10)?time.getDate():`0${time.getDate()}`;
                            var min:string = `${(time.getHours() >= 10)?time.getHours():`0${time.getHours()}`} : ${(time.getMinutes()>= 10)?time.getMinutes():`0${time.getMinutes()}`}`;
                            var tt:any = min +' ' +date + '/' + month + '/' + year ;
                            return tt;
                        }

                        function AdapterDate(time:any){
                            var year:string = time.getFullYear();
                            var month:string = (time.getMonth() + 1 >= 10)?time.getMonth() + 1: `0${time.getMonth() + 1}`;
                            var date:string = (time.getDate() >= 10)?time.getDate():`0${time.getDate()}`;
                            // var min:string = `${(time.getHours() >= 10)?time.getHours():`0${time.getHours()}`} : ${time.getMinutes()}`;
                            var tt:any = date + '/' + month + '/' + year ;
                            return tt;
                        }
                        function getDate (mode:any, userdate:any) {
                            var dte = userdate || new Date(),
                                d = dte.getDate().toString(),
                                m = (dte.getMonth() + 1).toString(),
                                yyyy = dte.getFullYear().toString(),
                                dd = (d.length < 2) ? '0' + d : d,
                                mm = (m.length < 2) ? '0' + m : m,
                                yy = yyyy.substring(2, 4);
                            switch (mode) {
                                case 'dd-mm-yyyy': return dd + '-' + mm + '-' + yyyy;
                                case 'yyyymmdd': return yyyy + mm + dd;
                                default: return dte;
                            }
                        }
                        var blob = new Blob([escapeHTML("<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns='http://www.w3.org/TR/REC-html40'><meta http-equiv='content-type' content='application/vnd.ms-excel; charset=UTF-8'><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>Sheet0</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body>"+document.getElementById('exportable').innerHTML + "</body></html>")], {
                            type: "application/octet-stream"
                        });
                        saveAs(blob, `${title}_${timestamp}.xls`);
                    };


                    var dataCfg = {
                        dataFields: Array<any>(),
                        dataType: 'array',
                        localData : Array<any>()
                    };
                    if($scope.id.behavior.isCheckSelect == true){
                        $scope.dataTableSettings.columns.push({
                            text: "Chọn",
                            width:40,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            editable: false,
                            sortable: false,
                            filterable:false,
                            dataField: null,
                            // renderer: (text:string, align:any, height:any)=> {
                            //     var checkBox = "<div id='checkbox' style='z-index: 999; margin: 5px; margin-left: 30px; margin-top: 8px; margin-bottom: 3px;'>";
                            //     checkBox += "</div>";
                            //     return checkBox;
                            // },
                            // rendered:(element:any, align:any, height:any)=> {
                            //     $scope.checkBox = new jqxCheckBox(element);
                            //     $scope.checkBox.on('change',(event:any)=> {
                            //         if (!updatingSelectionFromGrid) {
                            //             var args = event.args;
                            //             var rows = grid.getRows();
                            //             updatingSelection = true;
                            //             if (args.checked) {
                            //                 for (var i = 0; i < rows.length; i++) {
                            //                     grid.selectRow(i);
                            //                 }
                            //             }
                            //             else {
                            //                 grid.clearSelection();
                            //             }
                            //             updatingSelection = false;
                            //         }
                            //     });
                            //     return true;
                            // },
                            // row - row's index.
                            // column - column's data field.
                            // value - cell's value.
                            // rowData - rendered row's object.
                            cellsRenderer: (row:any, column:any, value:any, rowData:any)=> {
                                // console.log(rowData);
                                var selection = grid.getSelection();
                                if (value == null) value = false;
                                //var chk =$("<div></div>");
                                var chk = $("<input type='checkbox' class='" + "checkIcon_" + $scope.id.id +"' data-row='" + row + "' style='width:17px; height:17px'/>");
                                for(var i = 0; i< selection.length;i++){

                                }
                                $.each(selection,(ind:number,val:any)=>{
                                    if(val.uid === rowData.uid){
                                        value = true;
                                    }
                                });
                                if (value == true) {
                                    // var rowIndex = parseInt(event.currentTarget.getAttribute('data-row'));
                                    // console.log(rowIndex);
                                    $(chk).val(value);
                                    $(chk).attr('checked', '');
                                    // var rowIndex = parseInt(event.currentTarget.getAttribute('data-row'));
                                    // grid.selectRow(rowIndex);
                                    // grid.selectRow(rowIndex);
                                    // $scope.control.event.onCheck = (rowIndex:any)=>{
                                    //     grid.selectRow(rowIndex);
                                    // };
                                } else{
                                    $(chk).val(value);
                                    // grid.unselectRow();
                                }


                                /*$(chk).jqxCheckBox({width: '100%', height: '100%'});
                                 if(typeof value !== 'undefined'){
                                 if(value == true){
                                 $(chk).jqxCheckBox('check');
                                 }else{
                                 $(chk).jqxCheckBox('uncheck');
                                 }
                                 }
                                 return chk[0].innerHTML;*/
                                return chk[0].outerHTML;

                            },
                            createEditor:(element:any, row:any, cellvalue:any, editor:any, cellText:any, width:any, height:any)=> {

                                $(editor).css("margin-left", "4px");
                                $(editor).css("margin-top", "7px");
                                editor.jqxCheckBox({
                                    height: height,
                                    width: width
                                });
                                // /*if(typeof cellvalue !== 'undefined'){
                                //  if(cellvalue == true){
                                //  editor.jqxCheckBox('check');
                                //  }else{
                                //  editor.jqxCheckBox('uncheck');
                                //  }
                                //  }*/

                            },
                            initEditor:(row:any, cellvalue:any, editor:any, celltext:any, width:any, height:any)=> {

                                editor.jqxCheckBox({
                                    height: height,
                                    width: width
                                });

                                if (!cellvalue == true) {
                                    editor.jqxCheckBox('check');
                                    console.log(row);
                                    grid.selectRow(row);
                                } else {
                                    editor.jqxCheckBox('uncheck');
                                }

                            },
                            getEditorValue:(row:any, cellvalue:any, editor:any)=> {

                                return editor.val(); //jqxCheckBox('checked');

                            }
                        });
                    }



                    //todo: phải thêm id vào để phân biệt và xác định khi kick từng grid-view. lưu ý thêm id vào
                    if($scope.id.behavior.isNew === true && $scope.id.behavior.isEdit === true && $scope.id.behavior.isDelete === true ) {
                        $scope.dataTableSettings.columns.push({
                            text: "<a  class='" + "addIcon_" + $scope.id.id +" tooltitle_grid glyphicon glyphicon-plus text-center ' href='#' data-original-title='Thêm mới' data-toggle='tooltip' data-placement='bottom' title='Thêm mới' style='font-size: 1.5em; color: #65C3DF'></a> ",
                            width:80,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            editable: false,
                            sortable: false,
                            filterable:false,
                            dataField: null,
                            cellsRenderer: function (row:number, column:any, value:any) {
                                // render custom column.
                                return "<a class='" + "editIcon_" + $scope.id.id +"' id='" + "editIcon_" + $scope.id.id +"' href='#' data-row='" + row + "'><i style='font-size:1.5em' class=' glyphicon glyphicon-edit text-success' title='Cập Nhật'></i></a> " +
                                    "<a class='" + "deleteIcon_" + $scope.id.id +"' id='" + "deleteIcon_" + $scope.id.id +"' href='#' data-row='" + row + "'><i style='font-size:1.5em' class='glyphicon glyphicon-remove text-danger' title='Xóa'></i></a>" ;
                            }});
                    }
                    if($scope.id.behavior.isNew === true && $scope.id.behavior.isEdit === true && $scope.id.behavior.isDelete === false ) {
                        $scope.dataTableSettings.columns.push({
                            text: "<a  class='" + "addIcon_" + $scope.id.id +" tooltitle_grid glyphicon glyphicon-plus text-center ' href='#' data-original-title='Thêm mới' data-toggle='tooltip' data-placement='bottom' title='Thêm mới' style='font-size: 1.5em; color: #65C3DF'></a> ",
                            width:80,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            editable: false,
                            sortable: false,
                            filterable:false,
                            dataField: null,
                            cellsRenderer: function (row:number, column:any, value:any) {
                                // render custom column.
                                return "<a class='" + "editIcon_" + $scope.id.id +"' href='#' id='" + "editIcon_" + $scope.id.id +"' data-row='" + row + "'><i style='font-size:1.5em' class=' glyphicon glyphicon-edit text-success' title='Cập Nhật'></i></a> ";
                            }});
                    }
                    if($scope.id.behavior.isNew === true && $scope.id.behavior.isEdit === false && $scope.id.behavior.isDelete === true ) {
                        $scope.dataTableSettings.columns.push({
                            text: "<a  class='" + "addIcon_" + $scope.id.id +" tooltitle_grid glyphicon glyphicon-plus text-center ' href='#' data-original-title='Thêm mới' data-toggle='tooltip' data-placement='bottom' title='Thêm mới' style='font-size: 1.5em; color: #65C3DF'></a> ",
                            width:80,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            editable: false,
                            sortable: false,
                            filterable:false,
                            dataField: null,
                            cellsRenderer: function (row:number, column:any, value:any) {
                                // render custom column.
                                return "<a class='" + "deleteIcon_" + $scope.id.id +"' id='" + "deleteIcon_" + $scope.id.id +"' href='#' data-row='" + row + "'><i style='font-size:1.5em' class='glyphicon glyphicon-remove text-danger' title='Xóa'></i></a>" ;
                            }});
                    }
                    if($scope.id.behavior.isNew === false && $scope.id.behavior.isEdit === true && $scope.id.behavior.isDelete === false ) {
                        $scope.dataTableSettings.columns.push({
                            text: "",
                            width:30,
                            pinned:true,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            editable: false,
                            sortable: false,
                            filterable:false,
                            dataField: null,
                            cellsRenderer: function (row:number, column:any, value:any) {
                                // render custom column.
                                return "<a class='" + "editIcon_" + $scope.id.id +"' href='#' id='" + "editIcon_" + $scope.id.id +"' data-row='" + row + "'><i style='font-size:1.5em' class=' glyphicon glyphicon-edit text-success' title='Cập Nhật'></i></a> ";
                            }});
                    }
                    if($scope.id.behavior.isNew === false && $scope.id.behavior.isEdit === true && $scope.id.behavior.isDelete === true ) {
                        $scope.dataTableSettings.columns.push({
                            text: "",
                            width:55,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            filterable:false,
                            editable: false,
                            sortable: false,
                            dataField: null,
                            cellsRenderer: function (row:number, column:any, value:any) {
                                // render custom column.
                                return "<a class='" + "editIcon_" + $scope.id.id +"' href='#' id='" + "editIcon_" + $scope.id.id +"' data-row='" + row + "'><i style='font-size:1.5em' class=' glyphicon glyphicon-edit text-success' title='Cập Nhật'></i></a> " +
                                    "<a class='" + "deleteIcon_" + $scope.id.id +"' id='" + "deleteIcon_" + $scope.id.id +"' href='#' data-row='" + row + "'><i style='font-size:1.5em' class='glyphicon glyphicon-remove text-danger' title='Xóa'></i></a>" ;
                            }});
                    }
                    if($scope.id.behavior.isNew === false && $scope.id.behavior.isEdit === false && $scope.id.behavior.isDelete === true ) {
                        $scope.dataTableSettings.columns.push({
                            text: "",
                            width:30,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            filterable:false,
                            editable: false,
                            sortable: false,
                            dataField: null,
                            pinned:true,
                            cellsRenderer: function (row:number, column:any, value:any) {
                                // render custom column.
                                return "<a class='" + "deleteIcon_" + $scope.id.id +"' id='" + "deleteIcon_" + $scope.id.id +"' href='#' data-row='" + row + "'><i style='font-size:1.5em' class='glyphicon glyphicon-remove text-danger' title='Xóa'></i></a>" ;
                            }});
                    }
                    if($scope.id.behavior.isDetail === true){
                        $scope.dataTableSettings.columns.push({
                            text: "Chi tiết nhiên liệu",
                            width:150,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            editable: false,
                            sortable: false,
                            filterable:false,
                            dataField: null,
                            cellsRenderer: function (row:number, column:any, value:any) {
                                // render custom column.
                                return "<a class='" + "detailIcon_" + $scope.id.id +"' href='#' data-row='" + row + "'><button class='btn btn-info'>Hiển thị</button></a>";
                            }});
                    }
                    /*Chức năng RFID của tài xế*/
                    if($scope.id.behavior.isRFID === true){
                        $scope.dataTableSettings.columns.push({
                            text: "Ghi thẻ RFID",
                            width:90,
                            cellsAlign: 'center',
                            align: "center",
                            columnType: 'none',
                            editable: false,
                            sortable: false,
                            filterable:false,
                            dataField: null,
                            cellsRenderer: function (row:number, column:any, value:any) {
                                // render custom column.
                                return "<a class='" + "RFIDIcon_" + $scope.id.id +"' href='#' data-row='" + row + "'><button class='btn btn-info'>Ghi thẻ</button></a>";
                            }});
                    }
                    $scope.control.columnGroups.forEach(value => {
                        var text_ = value.text == undefined ?'group':value.text;
                        var name_ = value.name == undefined ?'group':value.name;
                        var parentGroup_ = value.parentGroup == undefined ? '': value.parentGroup;

                        $scope.dataTableSettings.columnGroups.push({
                            text: text_,
                            name: name_,
                            align: 'center',
                            parentGroup: parentGroup_,
                            className: value.className,
                        });
                    });

                   $scope.dataTableSettings.columnGroups.push({
                       text: '',
                       name: '',
                       align: 'center',
                   });
                   $scope.control.columns.forEach(val=> {
                       var width_ = val.width;
                       var class_ = val.className;
                       if (val.width == undefined)
                           width_ = 'auto';
                       var columngroup_ = val.columngroup== undefined ? undefined:val.columngroup;
                       var cellsFormat_ = val.cellsFormat == undefined ? null: val.cellsFormat;
                       var cellsAlign_ = val.cellsAlign == undefined? 'left':val.cellsAlign;
                       $scope.dataTableSettings.columns.push({
                           text: val.text,
                           dataField: val.dataField,
                           width: width_,
                           align: 'center',
                           aggregates:val.aggregates,
                           aggregatesRenderer:
                           val.aggregatesRenderer,
                           filterable:val.filterable,
                           className: class_,
                           cellClassName: val.cellClassName,
                           hidden:val.hidden,
                           columngroup: columngroup_,
                           cellsFormat: cellsFormat_,
                           cellsAlign:cellsAlign_,
                           pinned: val.pinned
                       });
                       dataCfg.dataFields.push({ name: val.dataField, type: val.type })
                    });


                    dataCfg.localData = $scope.control.data;
                    $scope.control.refresh=()=>{
                        //delete dataCfg.localData;
                        //dataCfg.localData = $scope.control.data;
                        ////$scope.dataTableSettings.source.setSource(dataCfg);
                        //delete  $scope.dataTableSettings.source;
                        //$scope.dataTableSettings.source=null;
                        //$scope.dataTableSettings.source = new $.jqx.dataAdapter(dataCfg);

                        $scope.dataTableSettings.source._source.localData = $scope.control.data;
                        $scope.dataTableSettings.source._source.localdata = $scope.control.data;
                        if($scope.dataTableSettings.apply!=undefined) {
                            $scope.dataTableSettings.apply('render');
                            $scope.dataTableSettings.apply('clearFilters');

                            //$scope.dataTableSettings.apply('updateBoundData');
                            // update maunual du lieu moi
                            // $scope.dataTableSettings.apply('refresh'); // update maunual du lieu moi
                            // update maunual du lieu moi
                        }
                        //$scope.dataTableSettings.source.dataBind();
                        //if(grid.isBindingCompleted()){
                        //    grid.updateBoundData();
                        //}
                        //delete dataCfg.localData;

                        //$scope.dataTableSettings.source.setSource(dataCfg);
                    };

                    $scope.control.refreshupdate = ()=>{
                        // $scope.dataTableSettings.source._source.localData = $scope.control.data;
                        // $scope.dataTableSettings.source._source.localdata = $scope.control.data;
                        if($scope.dataTableSettings.apply!=undefined) {
                            // $scope.dataTableSettings.apply('render');
                            // $scope.dataTableSettings.apply('clearFilters');
                            console.log($($scope.dataTableSettings));
                            // $scope.dataTableSettings.apply('applyFilters');

                            $scope.dataTableSettings.apply('updateBoundData');
                            // update maunual du lieu moi
                            // $scope.dataTableSettings.apply('refresh'); // update maunual du lieu moi
                            // update maunual du lieu moi
                        }
                    };

                    $scope.control.clear = () =>{
                        $scope.dataTableSettings.apply('clear');
                        // $scope.compiletemplate();
                    };
                    $scope.control.clearcompile = () =>{
                        // $scope.dataTableSettings.apply('clear');
                        $scope.compiletemplate();
                    };

                    /**
                     * Hàm grid-view chung hiển thị cột
                     */
                    $scope.control.showcolumn = (showcolumnstr) =>{
                        // $scope.dataTableSettings.apply('hidecolumn', hidecolumnstr);
                        grid.showColumn(showcolumnstr);
                    };

                    $scope.control.render = ()=>{
                        // $scope.dataTableSettings.apply('render');
                        $scope.dataTableSettings.apply('destroy');
                    };
                    $scope.control.clearSelection = ()=>{
                        $scope.dataTableSettings.apply('clearSelection');
                    };
                    $scope.dataTableSettings.source = new $.jqx.dataAdapter(dataCfg,{autoBind: true,loadComplete:()=>{
                        //if(grid.isBindingCompleted()){
                        //    grid.updateBoundData();
                        //}
                    },filter:(f:any,data:any)=>{
                        console.log('filter');
                        console.log(f);
                        console.log(data);
                    }});
                    // $scope.dataTableSettings.apply('updateBoundData');
                    // Xử lý khi nhấn nút print

                    $scope.id.behavior.isEdit = false;
                    $scope.id.behavior.isDelete = false ;
                    $scope.id.behavior.isNew = false;
                    $scope.id.behavior.isDetail = false;
                    $scope.id.behavior.isCheckSelect = false;


                    $scope.compiletemplate = () => {
                        //var dataCfg = {};
                        $scope.dataTableSettings={};
                        var el = angular.element('<jqx-data-table jqx-settings="dataTableSettings" ></jqx-data-table><div ng-hide="control.isExport"> ');

                        $('#basic').html(el);
                        var compiled = $compile(el);
                        var element = compiled($scope);

                        this.Init($scope,$http,$window,$compile);

                        // grid.render();
                    };


                }
            }

            export function gridviewDirective($templateCache:ng.ITemplateCacheService, $compile:ng.ICompileService, $http:ng.IHttpService,$window: ng.IWindowService): ng.IDirective {
                //If you use transclude;
                //'element' then you have to use replace: true,
                //for somewhat of an explanation see this issue.
                //If you use transclude: true the you can use whatever value you want for replace depending
                //on the end result you want with respect to the original HTML node on which the directive
                //was applied (whether it will still exist or not).
                return {
                    // bindToController: true,
                    controller: ['$scope','$http','$window','$compile', GridViewController],
                    name: "gridView",
                    restrict: "EA",
                    //transclude: true,// cho phép include đoán html nằm trong thẻ grid-view
                    templateUrl:'./shared/gridview.html',
                    scope: {
                        //= gán giá trị hoặc tham chiếu , @ chỉ gán giá trị ,& gán các callback
                        //columns: "=",
                        //enablebinding: "=",// Khi load xong datasource thì phải gọi thằng này để grid view tự động cập nhật dữ liệu,
                        //datasource: "@",// Tên của datasource được lưu trên server
                        //onNew:"&",//Thêm mới
                        //onEdit:"&",//Sửa
                        //onDelete: "&",//Xóa
                        id:"="
                    },
                    replace: true// không được phép xóa element nằm trong thẻ grid-view
                }
            }
            AdModule.directive('gridView',gridviewDirective);

        }
    }
}
