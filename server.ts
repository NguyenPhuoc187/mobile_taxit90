/**
 * Created by user on 04/08/2016.
 * Chứa các thông tin cấu hình cho backend
 */
/// <reference path="tstyping/express/express.d.ts" />
/// <reference path="tstyping/serve-favicon/serve-favicon.d.ts" />
/// <reference path="tstyping/morgan/morgan.d.ts" />
/// <reference path="tstyping/body-parser/body-parser.d.ts" />
/// <reference path="tstyping/method-override/method-override.d.ts" />

import express = require("express");
import http = require("http");
import path = require("path");
var favicon = require('serve-favicon');
import logger = require('morgan');
import bodyParse=require('body-parser');
import methodOverride=require('method-override');

var app = express();
var port = process.env.PORT || 3800 ;
var environment = process.env.NODE_ENV;
// all environments
app.set("port", process.env.PORT || 3800);

// reconfig http location
//app.set("views", path.join(__dirname, "views"));

//app.set("view engine", "jade");
// thiết lập icon trên thanh tiêu đề
app.use(favicon(__dirname + '/front-end/assets/images/favicon.ico'));
app.use(logger("dev"));
app.use(bodyParse.json());
app.use(bodyParse.urlencoded());
app.use(methodOverride());


console.log('PORT=' + port);
console.log('NODE_ENV=' + environment);

switch (environment){
    default:
        console.log('** DEV **');
        // app.use(express.static('./.front-end/'));
        app.use(express.static('./front-end/'));
        app.use(express.static('./bower-components'));
        app.use(express.static('./'));
       // app.use('/*', express.static('./front-end/index.html'));
        app.use((req: express.Request, res: express.Response, next:any) => {
            res.set('Location', '/#!' + req.path)
                .status(301).send({});
        });

        break;
}

app.listen(port, function() {
    console.log('Express server listening on port ' + port);
    console.log('env = ' + app.get('env') +
        '\n__dirname = ' + __dirname  +
        '\nprocess.cwd = ' + process.cwd());
});
